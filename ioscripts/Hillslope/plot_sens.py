###############################################################################
## MAIN SECTION : different plots using results.txt of simulation samples ##
###############################################################################

import numpy as np
import os.path
import time
import matplotlib.pyplot as plt
import pylab
import matplotlib.image as mpimg
import plotly.plotly as py
import math
import os
import pandas as pd
import matplotlib.cm as cm

# Domaine
#m2
aire = 5000

# Directory containing the pfb results of the PF simulation

base_dir = r'/homelocal5/home2/rogivues/auto_sens_analysis'

""" Lecture fichier resultats """

results_tab = np.loadtxt("results.txt")

res_df = pd.DataFrame(results_tab, columns = ['sample','H1perm','Precipitation','Runoff','Evap','WSC','Qr','Err'])


""" figure 2d  Qr=f(Ks)"""

#~ figure = res_df.plot(x='H1perm',y='Qr', title = 'Figure : Runoff coefficient plotted against H1 permeability')
#~ figure.set(xlabel="H1 permeability",ylabel="Qr = R/P")
#~ plt.show()

"""figure 3d Qr = f(H1.Ks,Precipitation)"""
####scatter plot####

#~ fig,ax = plt.subplots()
#~ ax.scatter(res_df.H1perm,res_df.Precipitation,c=res_df.Qr,cmap=cm.jet_r, marker = ',')
#~ leg = plt.cm.ScalarMappable(cmap=cm.jet_r)
#~ leg.set_array([0.,1.])
#~ fig.colorbar(leg)

####colormap plot####

x = np.array(res_df.H1perm)
x.sort()
y = res_df.Precipitation.unique()

Qr_array = np.zeros((len(x),len(y)))
Qr_array[:]=np.nan

for ii in range(len(res_df)) :
    K = res_df.H1perm[ii]
    P = res_df.Precipitation[ii]
    Qr = res_df.Qr[ii]
    for i in range(len(x)) :
        for j in range(len(y)):
            Ki = x[i]
            Pj = y[j]
            if Ki==K and P==Pj :
                Qr_array[i,j]= Qr

Qr_array[0,0]=0.5
            
fig,ax = plt.subplots()
ax.pcolormesh(x,y,Qr_array.T,cmap=cm.jet_r, shading='gouraud')
CS = ax.contour(x,y,Qr_array.T, colors = 'k', levels = [0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7])
ax.clabel(CS, inline=1, fontsize=12)
#~ ax.set_xscale('log')

leg = plt.cm.ScalarMappable(cmap=cm.jet_r)
leg.set_array([0.,1.])

cbar = fig.colorbar(leg)
cbar.set_label('   Cr', rotation=0, fontsize = 15)

ax.set_xlabel('Ks(H1 m/s)', fontsize = 15)
ax.set_ylabel('Precipitation (mm)', fontsize = 15)

plt.title('Cr = f (P,Ks(H1))', fontsize = 15)




