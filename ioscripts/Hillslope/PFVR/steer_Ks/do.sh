#!/bin/bash

rm -rf results
mkdir results
cd results

#-----------------------------------------------------------------------------
# Prepare input files
#-----------------------------------------------------------------------------
cp ../forcagePF.200707.30mn_updated.txt.0 ./forcagePF.txt.0
cp ../lai.dat ./lai.dat
cp ../sai.dat ./sai.dat
cp ../z0m.dat ./z0m.dat
cp ../displa.dat ./displa.dat
cp ../veg_map.pfb ./veg_map.pfb
cp ../drv_clmin.dat.0 ./drv_clmin.dat
cp ../drv_vegm.dat ./drv_vegm.dat
cp ../drv_vegp.dat ./drv_vegp.dat
cp ../dgridZ.pfb ./dgridZ.pfb
cp ../hillslope.tcl ./hillslope.tcl

#cp ../analyzer.py ./
cp ../analyzer_kvar.py ./analyzer.py
#cp ../analyzer_plot.py ./analyzer.py
cp ../parflowvr.py ./

rm $PROBLEMNAME*.out.*
P=1
Q=1
R=1
PROBLEMNAME=hillslope
export DAEMONFLAGS=" -s 3G "
$PARFLOW_DIR/bin/parflowvr/doMPI.sh $PROBLEMNAME $P $Q $R
$PARFLOW_DIR/bin/parflowvr/undist.tcl veg_map.pfb

echo .
echo .
echo ================== TESTS: ===================
#$PARFLOW_DIR/bin/parflowvr/compare_nc.py hillslope_sens.out.00010.nc good/hillslope_sens.out.00010.nc &&
#$PARFLOW_DIR/bin/parflowvr/compare_nc.py hillslope_sens.out.00020.nc good/hillslope_sens.out.00020.nc &&
echo passed! || echo XXXXXXXXXXXXXX: not passed!
