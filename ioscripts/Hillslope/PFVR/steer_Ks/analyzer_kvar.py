#!/usr/local/bin/ipython2 -u -i

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import pandas as pd
import glob, os
from matplotlib.animation import FuncAnimation
import matplotlib.animation as animation
from matplotlib.dates import MonthLocator, DateFormatter
from matplotlib.transforms import Affine2D
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from scipy import interpolate
import copy

"""local imports"""
import pypfAnalyzer as pfa
import PFlibs
from PFlibs import io_pfb as io
from PFlibs import io_tcl as iotcl


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def read_Forcings(simdir = './',date_init='1/1/2007 00:00:00'):
    forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
    forc = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')
    forc.rename(columns=forcing_headers,inplace=True)
    forc.index=pd.date_range(date_init, periods=len(forc), freq='30min')
    return forc


def exponential_decrease_with_cumulative_rainfall(cums, ks, ks_factor=5, cum_min=100, threshold=150, tau=10):
    """ From GQ:
    decreasing efolding as a function of cumulative precip.
    cum_min (mm) corresponds to the cumulative precip leading to the first weeding (sarclage)
    threshold (mm) gives weeding recurrence
    """
    def f(c):
        if c < cum_min:
            return ks_min
        else:
            r = np.mod((c + (threshold - cum_min)), threshold)
        return ks_min + (ks_max - ks_min) * np.exp(-r/tau)

    ks_min = ks
    ks_max = ks * ks_factor

    return cums.apply(f)


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

"""
Problem : variables are sent using dumpinterval in .tcl: when this one is too small (ie every time step), while the process
should be non-blocking (flowvr waits for python to send a message) it seems actually to fast as compared to python plots and crashes rapidly...
workaround: solve the non blocking issue or use overlandsum instead (faster, lighter)
"""


""" SETUP the Ks to steer"""
forc = read_Forcings()
tcl = iotcl.TclLines(filename = 'hillslope.tcl')              
geomDic = tcl.get_keyval_matching_pattern('Geom.*.Perm.Value')
Ks = np.float(geomDic['Geom.H1.Perm.Value'])/ 3600 #m/hr -> m/s
forc['P'] *= 60*30 # mm/s -> mm/30mn 
forc['Ks'] = exponential_decrease_with_cumulative_rainfall(np.cumsum(forc['P']), ks = Ks, ks_factor=5, cum_min=100, threshold=150, tau=10)


def onGridMessage(_arr, _gm):
    """
    arr.shape = [nz,ny,nx]
    """
    #deepcopy doesn't work
    arr = copy.copy(_arr)
    gm = copy.copy(_gm)

    #print('\n\n\n')
    print(gm.time)
    global forc

    if gm.variable == pfa.VARIABLE_PERMEABILITY_X :
        arr[-4:-1,:,:] = forc['Ks'][np.int(gm.time)] * 3600 #m/s -> m/hr
        arr[-1,:,:] = forc['Ks'][np.int(gm.time)] * 3600 #m/s -> m/hr
        pfa.SendSteer(pfa.ACTION_SET, pfa.VARIABLE_PERMEABILITY_X,
            gm.ix, gm.iy, gm.iz, arr) 
    if gm.variable == pfa.VARIABLE_PERMEABILITY_Y:
        arr[-4:-1,:,:] = forc['Ks'][np.int(gm.time)] * 3600 #m/s -> m/hr
        arr[-1,:,:] = forc['Ks'][np.int(gm.time)] * 3600 #m/s -> m/hr
        pfa.SendSteer(pfa.ACTION_SET, pfa.VARIABLE_PERMEABILITY_Y,
            gm.ix, gm.iy, gm.iz, arr) 
    if gm.variable == pfa.VARIABLE_PERMEABILITY_Z:
        arr[-4:-1,:,:] = forc['Ks'][np.int(gm.time)] * 3600 #m/s -> m/hr
        arr[-1,:,:] = forc['Ks'][np.int(gm.time)] * 3600 #m/s -> m/hr
        pfa.SendSteer(pfa.ACTION_SET, pfa.VARIABLE_PERMEABILITY_Z,
            gm.ix, gm.iy, gm.iz, arr) 
    print("check")


print("setting it up!")
pfa.SetGridMessageParser(onGridMessage)


print("running!!")
pfa.run(['t', 'zweiundvierzig'])


