#!/usr/local/bin/ipython2 -u -i

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import pandas as pd
import glob, os
from matplotlib.animation import FuncAnimation
import matplotlib.animation as animation
from matplotlib.dates import MonthLocator, DateFormatter
from matplotlib.transforms import Affine2D
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from scipy import interpolate
import copy

"""local imports"""
import pypfAnalyzer as pfa
import PFlibs
from PFlibs import io_pfb as io
from PFlibs import io_tcl as iotcl


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def read_Forcings(simdir = './',date_init='1/1/2007 00:00:00'):
    forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
    forc = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')
    forc.rename(columns=forcing_headers,inplace=True)
    forc.index=pd.date_range(date_init, periods=len(forc), freq='30min')
    return forc


def exponential_decrease_with_cumulative_rainfall(cums, ks, ks_factor=5, cum_min=100, threshold=150, tau=10):
    """ From GQ:
    decreasing efolding as a function of cumulative precip.
    cum_min (mm) corresponds to the cumulative precip leading to the first weeding (sarclage)
    threshold (mm) gives weeding recurrence
    """
    def f(c):
        if c < cum_min:
            return ks_min
        else:
            r = np.mod((c + (threshold - cum_min)), threshold)
        return ks_min + (ks_max - ks_min) * np.exp(-r/tau)

    ks_min = ks
    ks_max = ks * ks_factor

    return cums.apply(f)


def plot_snpsht(arr,txt):
    """ plot a snapshot 
    for keeping the graph open, follow: 
    https://stackoverflow.com/questions/11874767/how-do-i-plot-in-real-time-in-a-while-loop-using-matplotlib
    """
    global fig,ax,y,y_edges,z,z2,z3,ny,nz,dz,slope,ncells_Z_withslope,ndiv_dz_step,ncells_Z

    """
    #adapt the plot to the actual slope: 
    # 1) interpolate on a finer matrix, the same dimensions as original, define the finness, as a function of the slope
    # 2) place each column on a larger dimension (in Z) matrix by shifting each column depending on slope
    """
    satur_int1 = np.zeros((ny,np.size(z2)))
    #f1 = interpolate.interp2d(z[0:-1],y, np.reshape(np.swapaxes(arr[:,::-1,0],0,1),(ny,24)),kind='linear')
    f1 = interpolate.interp2d(z[1::],y, np.reshape(np.swapaxes(arr[:,::-1,0],0,1),(ny,24)),kind='linear')
    satur_int1[:,:]=f1(z2,y)
    satur_int_slope1 = np.empty((ny,np.size(z3)))
    satur_int_slope1[:]=np.nan
    zdeniv = np.zeros(ny)
    zdeniv[1:]=-10*slope
    for i in range(ny):
        satur_int_slope1[i,(np.int(ncells_Z_withslope))-(i*ndiv_dz_step)-ncells_Z:(ncells_Z_withslope)-(i*ndiv_dz_step)] = satur_int1[i,:]
    satur_int_slopem1 = np.ma.masked_where(np.isnan(satur_int_slope1),satur_int_slope1)
    cmap=cm.jet_r
    pcol1 = ax.pcolor(y_edges,np.append(z3,z3[-1]),np.swapaxes(satur_int_slopem1[:,:],0,1),cmap=cmap, shading = 'gouraud')
    """if you d'ont want it tilted:"""
    #pcol1 = ax.pcolor(y_edges,z,arr[:,::-1,0],cmap=cmap, shading = 'gouraud')

    ax.set_xlim([y[0],y[-1]])
    #ax.set_ylim([0,nz*dz]]) # without slope
    ax.set_ylim([0,deniv_tot+nz*dz])
    #t=ax.text(y[3],20,txt,fontsize=18,fontweight='bold',color='w') # without slope
    t=ax.text(y[1],0,txt,fontsize=18,fontweight='bold',color='k')
    fig.canvas.restore_region(background)
    # redraw just the points
    ax.draw_artist(pcol1)
    ax.draw_artist(t)
    # fill in the axes rectangle
    fig.canvas.blit(ax.bbox)
    return ax

def plot_WT(arr,txt):
    """ plot a snapshot of Water Table """
    global fig, ax, y, z,vardz
    #lines1, = ax.plot(y,arr[0,::-1,0]+vardz[0]/2,'k',linewidth=2) # without slope
    lines1, = ax.plot(y,-y*slope+deniv_tot+arr[0,::-1,0]+vardz[0]/2,'k',linewidth=2) # without slope
    ax.set_xlim([y[0],y[-1]])
    #ax.set_ylim([0,nz*dz]]) # without slope
    ax.set_ylim([0,deniv_tot+nz*dz])
    ax.set_ylabel('WT depth (m)', fontsize=11, color = 'b')
    #fig.canvas.restore_region(background)
    # redraw just the points
    ax.draw_artist(lines1)
    # fill in the axes rectangle
    fig.canvas.blit(ax.bbox)
    return ax

def plot_Q(a,txt):
    """ plot a snapshot of Water Table """
    global fig, ax1
    #lines2,=ax1.plot(pd.date_range('2007/1/1 00:00:00',periods=len(a),freq='D'),a,'b',linewidth = 2)
    lines2,=ax1.plot(pd.date_range('2007/1/1 00:00:00',periods=len(a),freq='H'),a,'b',linewidth = 2)
    # this allows to start back from scratch and not having all plots to pile up:
    fig.canvas.restore_region(background1)
    # redraw just the points
    ax1.draw_artist(lines2)
    # fill in the axes rectangle: that will use (display) the former defined axes (ticks, label and so), even if the current axes are different
    fig.canvas.blit(ax1.bbox)
    return ax1


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

"""
Problem : variables are sent using dumpinterval in .tcl: when this one is too small (ie every time step), while the process
should be non-blocking (flowvr waits for python to send a message) it seems actually to fast as compared to python plots and crashes rapidly...
workaround: solve the non blocking issue or use overlandsum instead (faster, lighter)
"""


#nz=24
tcl = iotcl.TclLines(filename = 'hillslope.tcl')              
nzdic = tcl.get_keyval_matching_pattern('dzScale.nzListNumber')
nz = [int(val) for key, val in nzdic.items()][0]
dz = [float(val) for key, val in tcl.get_keyval_matching_pattern('ComputationalGrid.DZ').items()][0]
print('dz')
print(dz)
#yslope,h = io.PFB_read('./Yslope.pfb',1)
#vardz,h = io.PFB_read('var_dz.pfb',1)
#nz = h['nz']

"""Get the actual vardz from a tcl:"""
tcl = iotcl.TclLines(filename = 'hillslope.tcl')              
vardzDic = tcl.get_keyval_matching_pattern('Cell.*.dzScale.Value')
vardz = np.zeros(len(vardzDic))
for key,val in vardzDic.items(): 
    i = int(key.split('.')[1])
    vardz[i] = val

z = np.append([0],np.cumsum(vardz))
y = np.linspace(0,490,50)
y_edges = np.linspace(0,500,51)
slope = 0.05
dy = 10.0
ny = 50
"""
# in case you want to adapt the plot to the actual slope: 
# 1) interpolate on a finer matrix, the same dimensions as original, define the finness, as a function of the slope
# 2) place each column on a larger dimension (in Z) matrix by shifting each column depending on slope
"""
dz_step = slope * dy
print('dz_step: %f (topographic diff betw one cell and the next one)'%dz_step)
ndiv_dz_step = 5
print('ndiv_dz_step: %d (resolution to discretize this topo diff)'%ndiv_dz_step)
Ztot = nz*dz-0.
ncells_Z = np.int(Ztot / (dz_step/ndiv_dz_step) +1)
print('new number of cells (Ztot / (dz_step/ndiv_dz_step): %d'%ncells_Z)
deniv_tot = slope * (np.max(y)-np.min(y))
print('total deniv: %f'%deniv_tot)
ncells_Z_withslope = np.int(ncells_Z + (ny-1) * ndiv_dz_step)
print('number of cells with slope (ncells_Z + (ny-1) * ndiv_dz_step): %d'%ncells_Z_withslope)
z2 = np.linspace(0,nz*dz,np.int(ncells_Z))
z3 = np.linspace(0,nz*dz + np.int(dy*ny*slope) ,np.int(ncells_Z_withslope))
yy,zz = np.meshgrid(y,z)


""" SETUP the initial plot """
#fig, [ax] = plt.subplots(1,1,figsize=(18,4))
fig, [ax,ax1] = plt.subplots(1,2,figsize=(18,4))
cmap=cm.jet_r
#cmap = cm.viridis
#cmap=plt.cm.viridis, norm=mpl.colors.BoundaryNorm(np.arange(0,1,0.1), plt.cm.viridis.N)
cmap.set_bad('w',1.)
ax.set_xlim([y[0],y[-1]])
#ax.set_ylim([0,24]) # without slope
ax.set_ylim([0,deniv_tot+nz*dz])
fig.axes[0].invert_yaxis()
#pcol1 = ax.pcolor(y_edges,z,np.random.random([len(z),len(y)]),cmap=cmap, shading = 'gouraud',transform= rot + base)
ax.margins(x=0)
ax.margins(y=0)
leg = plt.cm.ScalarMappable(cmap=cmap)
leg.set_array([0.,1.])
cbar = fig.colorbar(leg, ax = ax,orientation='vertical')
cbar.set_label('Saturation',fontsize=14)

"""SETUP a plot for streamflow"""
ax1.set_ylim([0,10])
ax1.set_xlim([pd.datetime(2007,1,1),pd.datetime(2007,12,31)])
ax1.set_ylabel('water height (mm)',fontsize=9)
ax1.xaxis.set_major_locator(MonthLocator(bymonth=range(1, 13), bymonthday=1, interval=1))
ax1.xaxis.set_major_formatter(DateFormatter("%b"))


plt.show(False)
#plt.draw()
#plt.pause(1)
#cache the background
background = fig.canvas.copy_from_bbox(ax.bbox)
background1 = fig.canvas.copy_from_bbox(ax1.bbox)

""" SETUP the Ks to steer"""
forc = read_Forcings()
geomDic = tcl.get_keyval_matching_pattern('Geom.*.Perm.Value')
Ks = np.float(geomDic['Geom.H1.Perm.Value'])/ 3600 #m/hr -> m/s
forc['P'] *= 60*30 # mm/s -> mm/30mn 
forc['Ks'] = exponential_decrease_with_cumulative_rainfall(np.cumsum(forc['P']), ks = Ks, ks_factor=5, cum_min=100, threshold=150, tau=10)



global Qs
Qs=[]

def onGridMessage(_arr, _gm):
    """
    arr.shape = [nz,ny,nx]
    """
    arr = copy.copy(_arr)
    gm = copy.copy(_gm)


    print('\n\n\n\n\n\n\n\n')
    print(gm.time)
    global fig,ax,ax1,y,z,Qs,forc

    if gm.variable == pfa.VARIABLE_SATURATION:
        ax=plot_snpsht(arr,str(round(gm.time)))

    if gm.variable == pfa.VARIABLE_PRESSURE:
        print(arr[0,:,0])
        print(arr.shape)
        ax=plot_WT(arr,str(round(gm.time)))
        Qs.append(arr[-1,0,0]*1000)#convert to mm
        print(Qs)
        print(arr[:,0,0])
        Qs[Qs<0]=np.nan
        ax1=plot_Q(Qs,str(round(gm.time)))
    print("Epic cool grid message! Let's do some stuff now!")
#    t = pfa.StampLog()
#    t.stamp_name = 't'
#    t.value = gm.time
#    print(gm.variable)
#    b = pfa.StampLog()
#    b.stamp_name = 'zweiundvierzig'
#    b.value = 42.0
#    pfa.SendLog([t,b])

    #r = 1 + random.random()* 0.2 - 0.4
    #arr *= r
#    if gm.time >= 96.:
#        pfa.SendSteer(pfa.ACTION_SET, pfa.VARIABLE_PERMEABILITY_Z,
#            gm.ix, gm.iy, gm.iz, np.zeros(np.shape(arr))+0.0001)    
#        pfa.SendSteer(pfa.ACTION_SET, pfa.VARIABLE_PRESSURE,
#            gm.ix, gm.iy, gm.iz, np.zeros(np.shape(arr))+1.0)    

#    pfa.SendSteer(pfa.ACTION_SET, gm.variable,
#            gm.ix, gm.iy, gm.iz, arr)
    if gm.variable == pfa.VARIABLE_PERMEABILITY_X :
        arr[-4:-1,:,:] = forc['Ks'][np.int(gm.time)] * 3600 #m/s -> m/hr
        arr[-1,:,:] = forc['Ks'][np.int(gm.time)] * 3600 #m/s -> m/hr
        pfa.SendSteer(pfa.ACTION_SET, pfa.VARIABLE_PERMEABILITY_X,
            gm.ix, gm.iy, gm.iz, arr) 
    if gm.variable == pfa.VARIABLE_PERMEABILITY_Y:
        arr[-4:-1,:,:] = forc['Ks'][np.int(gm.time)] * 3600 #m/s -> m/hr
        arr[-1,:,:] = forc['Ks'][np.int(gm.time)] * 3600 #m/s -> m/hr
        pfa.SendSteer(pfa.ACTION_SET, pfa.VARIABLE_PERMEABILITY_Y,
            gm.ix, gm.iy, gm.iz, arr) 
    if gm.variable == pfa.VARIABLE_PERMEABILITY_Z:
        arr[-4:-1,:,:] = forc['Ks'][np.int(gm.time)] * 3600 #m/s -> m/hr
        arr[-1,:,:] = forc['Ks'][np.int(gm.time)] * 3600 #m/s -> m/hr
        pfa.SendSteer(pfa.ACTION_SET, pfa.VARIABLE_PERMEABILITY_Z,
            gm.ix, gm.iy, gm.iz, arr) 
    print("check")


print("setting it up!")
pfa.SetGridMessageParser(onGridMessage)


print("running!!")
pfa.run(['t', 'zweiundvierzig'])


