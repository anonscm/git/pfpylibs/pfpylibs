#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    compare WT profiles in Bele : simulation 2D vs data

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import xarray as xr
import datetime
import os,copy

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb
from procsycz import readDataAMMA as rdA
from procsycz import procGeodata_Gdal

plt.close('all')

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Hillslope/bele_nalo_tests/NCout/test_11yrs_2020_05_29'
simuname1 = 'test_11yrs'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Hillslope/bele_nalo_tests/NCout/test_rip_11yrs_2020_05_29'
simuname2 = 'test_rip_11yrs'
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def find_grid_location(xx,yy,x,y):
    """matches the simulation grid given by xx and yy created by meshgrid and a given location (x,y):
    finds the closest match
    """
    dist = np.sqrt((np.round(xx)- x)**2 + (np.round(yy) - y)**2)
    ind_min = np.where(dist == dist.min())   
    return ind_min[1][0],ind_min[0][0],np.round(xx[ind_min])[0],np.round(yy[ind_min])[0]
    
def xtract_WTs(P,x,y,z):
    """Extract WT time series at each depth from an xarray P at locations x and y and given the depth profile z"""
    Ptmp=P.succion.sel(longitude=x,latitude=y)    
    #note that in pfoutputs.py:
    #profile[0] = base of domain. if the criteria is p < 0 => 0, you realize there are some weird points at the bottom
    #of perched layers with p>0 but < self.dz ... so as if the cell is not entirely saturated but p>0 ... 
    #profile = np.array([0. if self.press_array[xloc,yloc,i,t]<= self.dz_mult_arr[xloc,yloc,i]*self.dz/2 else 1. for i in range(self.nz)])
    #so?:
    #~ tmp2=(tmp-np.tile((P1.var_dz[:,50,50]*3).data,(354,1))/2>=0)
    #~ tmp2=(tmp>=0)
    tmp=np.zeros(np.shape(Ptmp.data))
    tmp[Ptmp.data>=0]=1
    for i in range(np.shape(tmp)[1]-1):
        tmp[:,i+1]=(tmp[:,i]+tmp[:,i+1])*tmp[:,i+1]
        # a=[1,1,1,0,0,0,1,1,0,1,0,0,0,1] => [1, 2, 3, 0, 0, 0, 1, 2, 0, 1, 0, 0, 0, 1]
        tmp[tmp!=1.]=np.nan
    #~ z=np.cumsum(P.var_dz[::-1,ysim_ind,xsim_ind]*3) -  P1.var_dz[::-1,ysim_ind,xsim_ind]*3/2
    WTs =np.tile(z[::-1],(np.shape(Ptmp.data)[0],1)) -(tmp*Ptmp.data) 
    return pd.DataFrame({z.data[::-1][depth]:pd.Series(WTs[:,depth],index=P.time.data) for depth in range(len(z))})    

def get_WT_simulation_results(P,stadic):
    """fill in the station dictionnary with WTs at stations locations calculated from """
    
    for stationname,sta in stadic.items():
        print(sta.name)
        z=np.cumsum(P.var_dz[::-1,sta.ysim_ind,sta.xsim_ind]*1) -  P.var_dz[::-1,sta.ysim_ind,sta.xsim_ind]*1/2
        sta.wts=xtract_WTs(P,sta.xsim,sta.ysim,z)
        stadic[stationname]=sta
    return stadic
    
def plot_WT_time_series(stadic,simdir='',savefig=True):
    """"""
    fig,ax =plt.subplots(nrows=int(np.ceil(len(stadic)/3)),ncols = 3,figsize=(15,20), squeeze=True,sharex=True,sharey=True)

    i=0
    j=0
    k=0

    for stationname,sta in stadic.items():
        # interpolation on sim + daily approach:
        obs = sta.WT.dropna().sort_index(axis=0)
        #~ simu = sta.WTsim.dropna().sort_index(axis=0)
        #~ simu = sta.wts[0].dropna().sort_index(axis=0)
        simu = sta.wts.iloc[:,0].dropna().sort_index(axis=0)
        #~ obs = obs.groupby(obs.index).mean()
        obs = obs.groupby(obs.index).mean().rename('obs')
        
        #~ obs_int=pd.concat([obs,simu],axis=1).interpolate(method='slinear',limit=50)[0]
        obs_int=pd.concat([obs,simu],axis=1).interpolate(method='slinear',limit=50)['obs']
        obs_intD = obs_int.resample('D').mean()
        simuD = simu.resample('D').mean()
        merge=pd.concat([obs_intD.rename('obs'),simuD.rename('sim')],axis=1)
        #~ obs_intD.name = 'obs'
        #~ simuD.name = 'sim'
        #~ merge=pd.concat([obs_intD,simuD],axis=1)
        kept = merge.dropna()
        res = obj.kge(kept['obs'].values, kept['sim'].values, return_all=True)
        print(res)  
      

        #2 columns version:
        #~ if i>=int(np.ceil(len(stadic)/2)):
            #~ j=1
            #~ i=0
        if (k>=int(np.ceil(len(stadic)/3))) & (k<2*int(np.ceil(len(stadic)/3))):
            j=1
            i=k-int(np.ceil(len(stadic)/3))
        elif k>=2*int(np.ceil(len(stadic)/3)):
            j=2
            i=k-2*int(np.ceil(len(stadic)/3))        
            
            
        #~ ax[i][j].plot(sta.WT.loc['2007-1-1':'2007-12-31'].dropna(),'k')
        ax[i][j].plot(sta.WT.dropna().sort_index(axis=0),'k')
        
        #~ ax[i][j].plot(sta.WTsim.loc['2007-1-1':'2007-12-31'].dropna(),'r')
        #~ ax[i][j].plot(sta.WTsim.dropna().sort_index(axis=0),'r')
        #~ for k in range(sim.nz):
        for k2 in range(len(sta.wts.columns)):
            #~ ax[i][j].plot(sta.wts[k].dropna().sort_index(axis=0),'r')
            ax[i][j].plot(sta.wts.iloc[:,k2].dropna().sort_index(axis=0),'r')
        ax[i][j].plot(obs_intD,'b')
                
        
        #~ ax[i].plot(sta.WTsim.loc['2007-1-1':'2007-12-31'],'r')
        if j==0: ax[i][j].set_ylabel('WTD(m)')

        if i==0: ax[i][j].legend(['obs','sim'],fontsize=8,loc='upper left',ncol=2)
        #~ ax[i,j].set_xlim([datetime.datetime(2002,1,1),datetime.datetime(2011,12,31)])
        ax[i,j].set_xlim([datetime.datetime(2004,1,1),datetime.datetime(2007,12,31)])
        ax[i,j].set_ylim([-0.5,25])
        #~ ax[i,j].text(datetime.datetime(2004,3,25),18,r'%s: $r^2 = %2.2f$ KGE = %2.2f'%(stationname,res[1]*res[1],res[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})
        ax[i,j].text(datetime.datetime(2004,3,25),18,r'%s'%stationname,FontSize=10,bbox = {'facecolor':'white', 'alpha':0.7})
           
        ax[i,j].tick_params(axis='x', which='both', labelbottom=False, labeltop=False)
        if j ==0: ax[i,j].tick_params(axis='y', which='both', labelright=False, labelleft=True)
        if j ==1: ax[i,j].tick_params(axis='y', which='both', labelright=False, labelleft=False)
        if j ==2: ax[i,j].tick_params(axis='y', which='both', labelright=True, labelleft=False)
        if j ==0: ax[i,j].tick_params(axis='both', which='major', bottom=False,top=False,right=False,left=True)
        if j ==1: ax[i,j].tick_params(axis='both', which='major', bottom=False,top=False,right=False,left=False)
        if j ==2: ax[i,j].tick_params(axis='both', which='major', bottom=False,top=False,right=True,left=False)
        i+=1
        k+=1
        
    #~ for i in range(len(stadic)):
    fig.subplots_adjust(bottom=0.06, top =0.98,left=0.05,right =0.96,wspace=0.0, hspace=0.000)
        #~ fig.axes[i].invert_yaxis() # if share_y = True this may not work : all axes now behave as if their were one. For instance, when you invert one of them, you affect all 
    fig.axes[0].invert_yaxis() #if share_y=True            
    ax[0,0].tick_params(axis='both', which='major', bottom=False,top=True,right=False,left=True)
    ax[0,1].tick_params(axis='both', which='major', bottom=False,top=True,right=False,left=False)
    ax[0,2].tick_params(axis='both', which='major', bottom=False,top=True,right=True,left=False)
    ax[int(np.ceil(len(stadic)/3))-1,0].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)
    ax[int(np.ceil(len(stadic)/3))-1,1].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)
    ax[int(np.ceil(len(stadic)/3))-2,2].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)
    ax[int(np.ceil(len(stadic)/3))-2,2].tick_params(axis='x', which='major', bottom=True, labeltop=False)
    fig.autofmt_xdate()
    #~ ax[int(np.ceil(len(stadic)/3))-1,2].set_axis_off()

    if savefig:
        plt.savefig(os.sep.join([simdir,'WTD.png']),dpi=400,format='png')
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
"""Read in Data"""
######### Oueme
#~ WTOu= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/WTOu.csv')
#~ WTOu= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/processed_data/WTOu_added_uncheckeddata_12h.csv')
######### oueme transect (belle, naloh, etc)
WTOut= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/processed_data/WT_Oueme_transect_smoothedDailyMean.csv')
WTOut = WTOut.set_index(WTOut.columns[0])
WTOut.index = pd.to_datetime(WTOut.index,format='%Y-%m-%d %H:%M:%S')
WTOut.index.rename('Date',inplace=True)
WTOut.drop(['Nalo_P034_20_2013-2015','Nalo_P190_20_2013-2015'],axis=1,inplace=True)
WTOut = WTOut.where(WTOut<1000,np.nan)   
#~ stations_to_keep = ['NALO-P034-10','NALO-P190-11','NALO-P500-10','Bele_P0099_120','Bele_P0192_120','Bele_P0312_100','Bele_P0688_22','Bele_P0968_24','Bele_P1250_21']
#~ stations_to_keep = ['Bele_P0099_120','Bele_P0192_120','Bele_P0312_100','Bele_P0688_22','Bele_P0968_24','Bele_P1250_21']
stations_to_keep = ['Bele_P0099_120','Bele_P0192_120','Bele_P0312_100','Bele_P0464_100','Bele_P0688_22','Bele_P0968_24','Bele_P1250_21']

#~ WT=pd.concat([WTOu,WTOut],axis=1)
WT=WTOut

WT=-WT

######### METADATA
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/WTOuSta.csv')
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/WTOuSta_added_uncheckeddata_12h.csv')
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/WTOuSta_added_uncheckeddata_12h_added_precip_from_AC.csv')
WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/processed_data/WTOuSta_added_uncheckeddata_12h_added_precip_from_AC.csv')
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/WTOuSta_added_uncheckeddata_12h_15Dmedianmov_window_added_precip_from_AC.csv')
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/WTOuSta_smoothedDailyMin.csv')
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/WTOuSta_smoothedDailyMin_P001deg_1day.csv')
WTOuSta = WTOuSta.set_index(WTOuSta.columns[0])

WTOuSta = WTOuSta.loc[[col for col in WT.columns if (col  in WTOuSta.index)],:]    

WTOuSta.loc['NALO-P005-12',['x','y']]=[1.6048,9.7469]
WTOuSta.loc['NALO-P034-02',['x','y']]=[1.6049,9.7466]
WTOuSta.loc['NALO-P034-10',['x','y']]=[1.6049,9.7466]
WTOuSta.loc['NALO-P190-02',['x','y']]=[1.6053,9.7454]
WTOuSta.loc['NALO-P190-11',['x','y']]=[1.6053,9.7454]
WTOuSta.loc['NALO-P500-2',['x','y']]=[1.6064,9.743]
WTOuSta.loc['NALO-P500-10',['x','y']]=[1.6064,9.743]
WTOuSta.loc['NALO-P500-18',['x','y']]=[1.6064,9.743]


WTOuSta.loc['Bele_P0099_120','z']=403.35-1.35
WTOuSta.loc['Bele_P0192_120','z']=408.65-0.76
WTOuSta.loc['Bele_P0312_100','z']=412.56-0.8
WTOuSta.loc['Bele_P0464_100','z']=416.59-0.8
WTOuSta.loc['Bele_P0688_22','z']=423.4-0.8
WTOuSta.loc['Bele_P0968_24','z']=431.68-0.8
WTOuSta.loc['Bele_P1250_21','z']=428.9-0.93
WTOuSta.loc['NALO-P005-12','z']=427.41
WTOuSta.loc['NALO-P034-02','z']=430.17
WTOuSta.loc['NALO-P034-10','z']=430.13
WTOuSta.loc['NALO-P190-02','z']=439.52
WTOuSta.loc['NALO-P190-11','z']=439.64
WTOuSta.loc['NALO-P500-2','z']=446.3
WTOuSta.loc['NALO-P500-10','z']=446.3
WTOuSta.loc['NALO-P500-18','z']=446.32


WTOuSta.loc['Bele_P0099_120','dist_riv']=99
WTOuSta.loc['Bele_P0192_120','dist_riv']=192
WTOuSta.loc['Bele_P0312_100','dist_riv']=312
WTOuSta.loc['Bele_P0464_100','dist_riv']=464
WTOuSta.loc['Bele_P0688_22','dist_riv']=688
WTOuSta.loc['Bele_P0968_24','dist_riv']=968
WTOuSta.loc['Bele_P1250_21','dist_riv']=1250
WTOuSta.loc['NALO-P005-12','dist_riv']=5
WTOuSta.loc['NALO-P034-02','dist_riv']=34
WTOuSta.loc['NALO-P034-10','dist_riv']=34
WTOuSta.loc['NALO-P190-02','dist_riv']=190
WTOuSta.loc['NALO-P190-11','dist_riv']=190
WTOuSta.loc['NALO-P500-2','dist_riv']=500
WTOuSta.loc['NALO-P500-10','dist_riv']=500
WTOuSta.loc['NALO-P500-18','dist_riv']=500

""" Find the location of data in the PF framework"""
P = xr.open_dataset(os.sep.join([simdir1,''.join([simuname1,'_press.nc'])]),decode_times=False)
[xx,yy]=np.meshgrid(P.longitude,P.latitude)
xx = np.round(xx)
yy = np.round(yy)

stadic ={}
#create a dictionnary of station objects
for stationname in WT.columns:
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    sta.WT=WT[stationname]

    #~ WTOuSta['xsim_ind'],WTOuSta['ysim_ind'],WTOuSta['xsim'],WTOuSta['ysim'] = zip(*WTOuSta.apply(lambda col: find_grid_location(xx,yy,col.x,col.y),axis=1))
    WTOuSta['xsim_ind'],WTOuSta['ysim_ind'],WTOuSta['xsim'],WTOuSta['ysim'] = zip(*WTOuSta.apply(lambda col: find_grid_location(xx,yy,col.dist_riv*0,col.dist_riv),axis=1))
    #~ sta.x=WTOuSta.loc[stationname,'x'] 
    #~ sta.y=WTOuSta.loc[stationname,'y'] 
    #~ sta.xsim=WTOuSta.loc[stationname,'xsim']  
    #~ sta.ysim=WTOuSta.loc[stationname,'ysim']
    #~ sta.xsim_ind=WTOuSta.loc[stationname,'xsim_ind']  
    #~ sta.ysim_ind=WTOuSta.loc[stationname,'ysim_ind']
    sta.x=0.
    sta.y=WTOuSta.loc[stationname,'dist_riv']
    sta.xsim=WTOuSta.loc[stationname,'xsim']  
    sta.ysim=WTOuSta.loc[stationname,'ysim']
    sta.xsim_ind=WTOuSta.loc[stationname,'xsim_ind']  
    sta.ysim_ind=WTOuSta.loc[stationname,'ysim_ind']
    sta.dist_riv=WTOuSta.loc[stationname,'dist_riv']
    stadic[stationname]=sta
    
stadic['Bele_P0099_120'].z=403.35-1.35
stadic['Bele_P0192_120'].z=408.65-0.76
stadic['Bele_P0312_100'].z=412.56-0.8
stadic['Bele_P0464_100'].z=416.59-0.8
stadic['Bele_P0688_22'].z=423.4-0.8
stadic['Bele_P0968_24'].z=431.68-0.8
stadic['Bele_P1250_21'].z=428.9-0.93
stadic['NALO-P005-12'].z=427.41
stadic['NALO-P034-02'].z=430.17
stadic['NALO-P034-10'].z=430.13
stadic['NALO-P190-02'].z=439.52
stadic['NALO-P190-11'].z=439.64
stadic['NALO-P500-2'].z=446.3
stadic['NALO-P500-10'].z=446.3
stadic['NALO-P500-18'].z=446.32


""" Get DEM:"""
DEM_filename='/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/Oueme_merit_f3_m1_newriv_DEM.pfb'
DEM = np.swapaxes(iopfb.PFB_read(DEM_filename,0)[:,:,0],0,1)

""" Read in sim outputs using xarray"""
P1 = xr.open_dataset(os.sep.join([simdir1,''.join([simuname1,'_press.nc'])]),decode_times=False)
tsim1 = pd.date_range('1/1/2005 00:00:00', periods=len(P1.time), freq='D')
# for saturation & pression
#~ tsim_full = np.insert(tsim,0,tsim[0]- (tsim[1]-tsim[0])) #for satur and press
P1 = P1.assign_coords(time = pd.to_datetime(tsim1)) #for satur and press

P1['veg_map']=P1.veg_map.where(P1.veg_map!=7,1) 
P1['veg_map']=P1.veg_map.where(P1.veg_map!=8,2) 
P1['veg_map']=P1.veg_map.where(P1.veg_map!=19,3) 
P1['veg_map'] = P1.veg_map.where(P1.mask.sel(depth=0)==1,np.nan)          


P2 = xr.open_dataset(os.sep.join([simdir2,''.join([simuname2,'_press.nc'])]),decode_times=False)
tsim2 = pd.date_range('1/1/2005 00:00:00', periods=len(P2.time), freq='D')
# for saturation & pression
#~ tsim_full = np.insert(tsim,0,tsim[0]- (tsim[1]-tsim[0])) #for satur and press
P2 = P2.assign_coords(time = pd.to_datetime(tsim2)) #for satur and press


"""
plot time series
"""
#~ z=np.cumsum(P1.var_dz[::-1,0,0]*1) -  P1.var_dz[::-1,0,0]*1/2
z=np.cumsum(P1.dz_mult[::-1,0,0]*1) -  P1.dz_mult[::-1,0,0]*1/2

stadic['Bele_P1250_21'].wts=xtract_WTs(P1,stadic['Bele_P1250_21'].xsim,stadic['Bele_P1250_21'].ysim,z)

# 2D altidude plot
#~ ax1=WT[stations_to_keep].apply(lambda x: x-WTOuSta.loc[x.name,'Z']).plot()   
ax1=WT[stations_to_keep].apply(lambda x: WTOuSta.loc[x.name,'z']-x).plot()   
#~ ax1.invert_yaxis()
# 3D altitude x time plot:
# https://stackoverflow.com/questions/42677160/matplotlib-3d-scatter-plot-date
from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure(figsize=plt.figaspect(0.5))

"""Bele"""
t=[ (d-WT.index[0]).days for d in WT.index]
#~ fig = plt.figure()
#~ ax = fig.add_subplot(111, projection='3d')
ax = fig.add_subplot(1, 2, 1, projection='3d')
ax.plot(np.zeros(len(t))+99,t,WTOuSta.loc['Bele_P0099_120','z']-WT['Bele_P0099_120'].data,'r')
ax.plot(np.zeros(len(t))+192,t,WTOuSta.loc['Bele_P0192_120','z']-WT['Bele_P0192_120'].data,'m')
ax.plot(np.zeros(len(t))+312,t,WTOuSta.loc['Bele_P0312_100','z']-WT['Bele_P0312_100'].data,'y')
ax.plot(np.zeros(len(t))+464,t,WTOuSta.loc['Bele_P0464_100','z']-WT['Bele_P0464_100'].data,'y')
ax.plot(np.zeros(len(t))+688,t,WTOuSta.loc['Bele_P0688_22','z']-WT['Bele_P0688_22'].data,'c')
ax.plot(np.zeros(len(t))+968,t,WTOuSta.loc['Bele_P0968_24','z']-WT['Bele_P0968_24'].data,'b')
ax.plot(np.zeros(len(t))+1250,t,WTOuSta.loc['Bele_P1250_21','z']-WT['Bele_P1250_21'].data,'g')
dist_to_riv=[99,192,312,464,688,968,1250]
depth=np.array([12.51,12.3,10.04,10,22.7,24.43,20.76])
crepine=np.array([2,2,2,2,5.5,7.73,3.83])
surf_elev=np.array([WTOuSta.loc['Bele_P0099_120','z'],WTOuSta.loc['Bele_P0192_120','z'],
WTOuSta.loc['Bele_P0312_100','z'],WTOuSta.loc['Bele_P0464_100','z'],WTOuSta.loc['Bele_P0688_22','z'],
WTOuSta.loc['Bele_P0968_24','z'],WTOuSta.loc['Bele_P1250_21','z']])

ax.plot(dist_to_riv,np.zeros(len(dist_to_riv))+np.mean(t),surf_elev,'k')
ax.plot(dist_to_riv,np.zeros(len(dist_to_riv))+np.mean(t),surf_elev-depth,'k+')
ax.plot(dist_to_riv,np.zeros(len(dist_to_riv))+np.mean(t),surf_elev-(depth-crepine),'k+')
dt = [ pd.Timedelta(d,unit='D') + WT.index[0]  for d in  ax.get_yticks()]
#~ dtStr = [d.isoformat() for d in dt]
dtStr = [d.strftime('%Y-%m-%d') for d in dt]
ax.set_yticklabels(dtStr)
ax.set_xlabel('length to river (m)')
ax.set_zlabel('elevation')
ax.set_xlim([0,1300])
zlim=ax.get_zlim()

"""Nalo"""
t=[ (d-WT.index[0]).days for d in WT.index]
#~ fig = plt.figure()
#~ ax = fig.add_subplot(111, projection='3d')
ax = fig.add_subplot(1, 2, 2, projection='3d')
WTOuSta.loc['NALO-P005-12','z']=427.41
WTOuSta.loc['NALO-P034-02','z']=430.17
WTOuSta.loc['NALO-P034-10','z']=430.13
WTOuSta.loc['NALO-P190-02','z']=439.52
WTOuSta.loc['NALO-P190-11','z']=439.64
WTOuSta.loc['NALO-P500-2','z']=446.3
WTOuSta.loc['NALO-P500-10','z']=446.3
WTOuSta.loc['NALO-P500-18','z']=446.32

plt.plot(np.zeros(len(t))+5,t,WTOuSta.loc['NALO-P005-12','z']-WT['NALO-P005-12'].data,'r')
plt.plot(np.zeros(len(t))+34,t,WTOuSta.loc['NALO-P034-02','z']-WT['NALO-P034-02'].data,'m')
plt.plot(np.zeros(len(t))+34,t,WTOuSta.loc['NALO-P034-10','z']-WT['NALO-P034-10'].data,'y')
plt.plot(np.zeros(len(t))+190,t,WTOuSta.loc['NALO-P190-02','z']-WT['NALO-P190-02'].data,'y')
plt.plot(np.zeros(len(t))+190,t,WTOuSta.loc['NALO-P190-11','z']-WT['NALO-P190-11'].data,'c')
plt.plot(np.zeros(len(t))+500,t,WTOuSta.loc['NALO-P500-2','z']-WT['NALO-P500-2'].data,'b')
plt.plot(np.zeros(len(t))+500,t,WTOuSta.loc['NALO-P500-10','z']-WT['NALO-P500-10'].data,'g')
plt.plot(np.zeros(len(t))+500,t,WTOuSta.loc['NALO-P500-18','z']-WT['NALO-P500-18'].data,'g')
dist_to_riv=[5,34,34,190,190,500,500,500]
depth=np.array([12.0,2.13,10.27,1.98,9.98,1.93,9.28,16.72])
surf_elev=np.array([WTOuSta.loc['NALO-P005-12','z'],WTOuSta.loc['NALO-P034-02','z'],
WTOuSta.loc['NALO-P034-10','z'],WTOuSta.loc['NALO-P190-02','z'],WTOuSta.loc['NALO-P190-11','z'],
WTOuSta.loc['NALO-P500-2','z'],WTOuSta.loc['NALO-P500-10','z'],WTOuSta.loc['NALO-P500-18','z']])

plt.plot(dist_to_riv,np.zeros(len(dist_to_riv))+np.mean(t),surf_elev,'k')
plt.plot(dist_to_riv,np.zeros(len(dist_to_riv))+np.mean(t),surf_elev-depth,'k+')
dt = [ pd.Timedelta(d,unit='D') + WT.index[0]  for d in  ax.get_yticks()]
#~ dtStr = [d.isoformat() for d in dt]
dtStr = [d.strftime('%Y-%m-%d') for d in dt]
ax.set_yticklabels(dtStr)
ax.set_xlabel('length to river (m)')
ax.set_zlabel('elevation')
ax.set_xlim([0,1300])
ax.set_zlim(ax.get_zlim()[0],ax.get_zlim()[0]+zlim[1]-zlim[0])

"""
Asssign simulation restults to stadic
"""
stadic1=copy.deepcopy(stadic)
for stationname in WT.columns:
    stadic1[stationname].wts=xtract_WTs(P1,stadic1[stationname].xsim,stadic1[stationname].ysim,z)

stadic2=copy.deepcopy(stadic)
for stationname in WT.columns:
    stadic2[stationname].wts=xtract_WTs(P2,stadic2[stationname].xsim,stadic2[stationname].ysim,z)

"""
create a pd dataframe for simulated WT at obs locations
"""
WTsim1 = pd.DataFrame()
for stationname in WT.columns:
    if 'Bele' in stationname:    
        WTsim1[stationname]=stadic1[stationname].wts.iloc[:,0].dropna().sort_index(axis=0)
WTsim2 = pd.DataFrame()
for stationname in WT.columns:
    if 'Bele' in stationname:    
        WTsim2[stationname]=stadic2[stationname].wts.iloc[:,0].dropna().sort_index(axis=0)


"""
plot WTD - amp relationships
"""


fig,ax = plt.subplots(1,1,figsize = [7,7])
bele_stations=[station for station in WT.mean().index if 'Bele' in station]
bele_stations.pop(np.where(np.array(bele_stations)=='Bele_P0464_100')[0][0]) 
tmp=(WT.resample('Y').max()- WT.resample('Y').min()).mean()
plt.scatter(WT.mean().loc[bele_stations],tmp.loc[bele_stations],c='g')

nalohou_stations=[station for station in WT.mean().index if 'NALO' in station]
nalohou_stations=['NALO-P005-12','NALO-P034-10','NALO-P190-11','NALO-P500-18']
plt.scatter(WT.mean().loc[nalohou_stations],tmp.loc[nalohou_stations],c='r')

#add simulations:
tmp2=(WTsim1.resample('Y').max()- WTsim1.resample('Y').min()).mean()
tmp3=(WTsim2.resample('Y').max()- WTsim2.resample('Y').min()).mean()
plt.scatter(WTsim1.mean().loc[bele_stations],tmp2.loc[bele_stations],c='b')
plt.scatter(WTsim2.mean().loc[bele_stations],tmp3.loc[bele_stations],c='c')


#~ plt.legend(['Trees (belefoungou)','Herbaceous (Nalohou)'])
plt.legend(['Trees (belefoungou)','Herbaceous (Nalohou)','trees sim','trees+rip sim'])
ax.set_xlabel('WTD (m)',fontsize=14)
ax.set_ylabel('amplitude (m)',fontsize=14)

"""Plot 3D again bele obs vs sim"""

fig = plt.figure(figsize=plt.figaspect(0.5))

"""Bele"""
t=[ (d-WT.index[0]).days for d in WT.index]
#~ fig = plt.figure()
#~ ax = fig.add_subplot(111, projection='3d')
ax = fig.add_subplot(1, 2, 1, projection='3d')
ax.plot(np.zeros(len(t))+99,t,WTOuSta.loc['Bele_P0099_120','z']-WT['Bele_P0099_120'].data,'r')
ax.plot(np.zeros(len(t))+192,t,WTOuSta.loc['Bele_P0192_120','z']-WT['Bele_P0192_120'].data,'m')
ax.plot(np.zeros(len(t))+312,t,WTOuSta.loc['Bele_P0312_100','z']-WT['Bele_P0312_100'].data,'y')
ax.plot(np.zeros(len(t))+464,t,WTOuSta.loc['Bele_P0464_100','z']-WT['Bele_P0464_100'].data,'y')
ax.plot(np.zeros(len(t))+688,t,WTOuSta.loc['Bele_P0688_22','z']-WT['Bele_P0688_22'].data,'c')
ax.plot(np.zeros(len(t))+968,t,WTOuSta.loc['Bele_P0968_24','z']-WT['Bele_P0968_24'].data,'b')
ax.plot(np.zeros(len(t))+1250,t,WTOuSta.loc['Bele_P1250_21','z']-WT['Bele_P1250_21'].data,'g')
dist_to_riv=[99,192,312,464,688,968,1250]
depth=np.array([12.51,12.3,10.04,10,22.7,24.43,20.76])
crepine=np.array([2,2,2,2,5.5,7.73,3.83])
surf_elev=np.array([WTOuSta.loc['Bele_P0099_120','z'],WTOuSta.loc['Bele_P0192_120','z'],
WTOuSta.loc['Bele_P0312_100','z'],WTOuSta.loc['Bele_P0464_100','z'],WTOuSta.loc['Bele_P0688_22','z'],
WTOuSta.loc['Bele_P0968_24','z'],WTOuSta.loc['Bele_P1250_21','z']])

ax.plot(dist_to_riv,np.zeros(len(dist_to_riv))+np.mean(t),surf_elev,'k')
ax.plot(dist_to_riv,np.zeros(len(dist_to_riv))+np.mean(t),surf_elev-depth,'k+')
ax.plot(dist_to_riv,np.zeros(len(dist_to_riv))+np.mean(t),surf_elev-(depth-crepine),'k+')
dt = [ pd.Timedelta(d,unit='D') + WT.index[0]  for d in  ax.get_yticks()]
#~ dtStr = [d.isoformat() for d in dt]
dtStr = [d.strftime('%Y-%m-%d') for d in dt]
ax.set_yticklabels(dtStr)
ax.set_xlabel('length to river (m)')
ax.set_zlabel('elevation')
ax.set_xlim([0,1300])
zlim=ax.get_zlim()

"""Bele sim"""
#minimum z of data
minz=WTOuSta.loc['Bele_P0099_120','z']

t=[ (d-WTsim1.index[0]).days for d in WTsim1.index]
#~ fig = plt.figure()
#~ ax = fig.add_subplot(111, projection='3d')
ax = fig.add_subplot(1, 2, 2, projection='3d')

ax.plot(np.zeros(len(t))+stadic1['Bele_P0099_120'].ysim,t,minz+P1.slope_y.mean().data*stadic1['Bele_P0099_120'].ysim-WTsim1['Bele_P0099_120'].values,'r')
ax.plot(np.zeros(len(t))+stadic1['Bele_P0192_120'].ysim,t,minz+P1.slope_y.mean().data*stadic1['Bele_P0192_120'].ysim-WTsim1['Bele_P0192_120'].values,'m')
ax.plot(np.zeros(len(t))+stadic1['Bele_P0312_100'].ysim,t,minz+P1.slope_y.mean().data*stadic1['Bele_P0312_100'].ysim-WTsim1['Bele_P0312_100'].values,'y')
ax.plot(np.zeros(len(t))+stadic1['Bele_P0464_100'].ysim,t,minz+P1.slope_y.mean().data*stadic1['Bele_P0464_100'].ysim-WTsim1['Bele_P0464_100'].values,'y')
ax.plot(np.zeros(len(t))+stadic1['Bele_P0688_22'].ysim,t,minz+P1.slope_y.mean().data*stadic1['Bele_P0688_22'].ysim-WTsim1['Bele_P0688_22'].values,'c')
ax.plot(np.zeros(len(t))+stadic1['Bele_P0968_24'].ysim,t,minz+P1.slope_y.mean().data*stadic1['Bele_P0968_24'].ysim-WTsim1['Bele_P0968_24'].values,'b')
ax.plot(np.zeros(len(t))+stadic1['Bele_P1250_21'].ysim,t,minz+P1.slope_y.mean().data*stadic1['Bele_P1250_21'].ysim-WTsim1['Bele_P1250_21'].values,'g')
dist_to_riv=[stadic1['Bele_P0099_120'].ysim,stadic1['Bele_P0192_120'].ysim,stadic1['Bele_P0312_100'].ysim,
stadic1['Bele_P0464_100'].ysim,stadic1['Bele_P0688_22'].ysim,stadic1['Bele_P0968_24'].ysim,
stadic1['Bele_P1250_21'].ysim]
surf_elev=np.array([minz+P1.slope_y.mean().data*stadic1['Bele_P0099_120'].ysim,
minz+P1.slope_y.mean().data*stadic1['Bele_P0192_120'].ysim,minz+P1.slope_y.mean().data*stadic1['Bele_P0312_100'].ysim,
minz+P1.slope_y.mean().data*stadic1['Bele_P0464_100'].ysim,minz+P1.slope_y.mean().data*stadic1['Bele_P0688_22'].ysim,
minz+P1.slope_y.mean().data*stadic1['Bele_P0968_24'].ysim,minz+P1.slope_y.mean().data*stadic1['Bele_P1250_21'].ysim])

plt.plot(dist_to_riv,np.zeros(len(dist_to_riv))+np.mean(t),surf_elev,'k')
#~ plt.plot(dist_to_riv,np.zeros(len(dist_to_riv))+np.mean(t),surf_elev-depth,'k+')
dt = [ pd.Timedelta(d,unit='D') + WTsim1.index[0]  for d in  ax.get_yticks()]
#~ dtStr = [d.isoformat() for d in dt]
dtStr = [d.strftime('%Y-%m-%d') for d in dt]
ax.set_yticklabels(dtStr)
ax.set_xlabel('length to river (m)')
ax.set_zlabel('elevation')
ax.set_xlim([0,1300])
ax.set_zlim(ax.get_zlim()[0],ax.get_zlim()[0]+zlim[1]-zlim[0])



"""
compare time series observated and simulated
"""

fig,ax = plt.subplots(1,1)

for stationname in WT.columns:
    if 'Bele' in stationname:    
        stadic1[stationname].wts=xtract_WTs(P1,stadic1[stationname].xsim,stadic1[stationname].ysim,z)
        #~ WT[stationname].plot(ax=ax)
        ax.plot(WT[stationname])
        color = ax.get_lines()[-1].get_color()
        tmp=stadic1[stationname].wts
        for k2 in range(len(stadic1[stationname].wts.columns)):
            #~ ax.plot(stadic1[stationname].wts.iloc[:,k2].dropna().sort_index(axis=0).loc[datetime.datetime(2005,1,1):datetime.datetime(2006,12,22)],'r')
            ax.plot(stadic1[stationname].wts.iloc[:,k2].dropna().sort_index(axis=0),c=color)
ax.invert_yaxis()

def plot_WTs(stadic,stationname,ax):
    tmp=stadic[stationname].wts
    for k2 in range(len(stadic[stationname].wts.columns)):
        #~ ax.plot(stadic1[stationname].wts.iloc[:,k2].dropna().sort_index(axis=0).loc[datetime.datetime(2005,1,1):datetime.datetime(2006,12,22)],'r')
        ax.plot(stadic[stationname].wts.iloc[:,k2].dropna().sort_index(axis=0),'r')
    return ax

fig,ax = plt.subplots(7,1,sharex=True)

ax[0].plot(WT['Bele_P0099_120'],'k')
ax[0] = plot_WTs(stadic1,'Bele_P0099_120',ax[0])

ax[1].plot(WT['Bele_P0192_120'],'k')
ax[1] = plot_WTs(stadic1,'Bele_P0192_120',ax[1])

ax[2].plot(WT['Bele_P0312_100'],'k')
ax[2] = plot_WTs(stadic1,'Bele_P0312_100',ax[2])

ax[3].plot(WT['Bele_P0464_100'],'k')
ax[3] = plot_WTs(stadic1,'Bele_P0464_100',ax[3])

ax[4].plot(WT['Bele_P0688_22'],'k')
ax[4] = plot_WTs(stadic1,'Bele_P0688_22',ax[4])

ax[5].plot(WT['Bele_P0968_24'],'k')
ax[5] = plot_WTs(stadic1,'Bele_P0968_24',ax[5])

ax[6].plot(WT['Bele_P1250_21'],'k')
ax[6] = plot_WTs(stadic1,'Bele_P1250_21',ax[6])

for a in ax:
    a.invert_yaxis()
    a.set_ylabel('WTD (m)', fontsize=9)

ax[0].legend(('obs','sim'),loc='upper right',fontsize=7,ncol=6)

fig.subplots_adjust(bottom=0.05, top =0.98,left=0.13,right =0.93,wspace=0.04, hspace=0.001)


"""P2"""
fig,ax = plt.subplots(7,1,sharex=True)

ax[0].plot(WT['Bele_P0099_120'],'k')
ax[0] = plot_WTs(stadic2,'Bele_P0099_120',ax[0])

ax[1].plot(WT['Bele_P0192_120'],'k')
ax[1] = plot_WTs(stadic2,'Bele_P0192_120',ax[1])

ax[2].plot(WT['Bele_P0312_100'],'k')
ax[2] = plot_WTs(stadic2,'Bele_P0312_100',ax[2])

ax[3].plot(WT['Bele_P0464_100'],'k')
ax[3] = plot_WTs(stadic2,'Bele_P0464_100',ax[3])

ax[4].plot(WT['Bele_P0688_22'],'k')
ax[4] = plot_WTs(stadic2,'Bele_P0688_22',ax[4])

ax[5].plot(WT['Bele_P0968_24'],'k')
ax[5] = plot_WTs(stadic2,'Bele_P0968_24',ax[5])

ax[6].plot(WT['Bele_P1250_21'],'k')
ax[6] = plot_WTs(stadic2,'Bele_P1250_21',ax[6])

for a in ax:
    a.invert_yaxis()
    a.set_ylabel('WTD (m)', fontsize=9)

ax[0].legend(('obs','sim'),loc='upper right',fontsize=7,ncol=6)

fig.subplots_adjust(bottom=0.05, top =0.98,left=0.13,right =0.93,wspace=0.04, hspace=0.001)

""""""
ax=WT[stations_to_keep].plot()        
ax.invert_yaxis()
tmp=stadic1['Bele_P1250_21'].wts
for k2 in range(len(stadic['Bele_P1250_21'].wts.columns)):
    ax.plot(stadic['Bele_P1250_21'].wts.iloc[:,k2].dropna().sort_index(axis=0).loc[datetime.datetime(2005,1,1):datetime.datetime(2006,12,22)],'r')
            
tmp=stadic1['Bele_P0099_120'].wts
for k2 in range(len(tmp.columns)):
    ax.plot(tmp.iloc[:,k2].dropna().sort_index(axis=0).loc[datetime.datetime(2005,1,1):datetime.datetime(2006,12,22)],'k')

tmp=wts        
for k2 in range(len(tmp.columns)):
    ax.plot(tmp.iloc[:,k2].dropna().sort_index(axis=0).loc[datetime.datetime(2005,1,1):datetime.datetime(2006,12,22)],'g')
               



P2 = xr.open_dataset(os.sep.join([simdir2,''.join([simuname2,'_press.nc'])]),decode_times=False)
tsim2 = pd.date_range('1/1/2005 00:00:00', periods=len(P2.time), freq='D')
# for saturation & pression
#~ tsim_full = np.insert(tsim,0,tsim[0]- (tsim[1]-tsim[0])) #for satur and press
P2 = P2.assign_coords(time = pd.to_datetime(tsim2)) #for satur and press
stadic2=get_WT_simulation_results(P2,copy.deepcopy(stadic))
plot_WT_time_series(stadic=stadic2,simdir=simdir2,savefig=True)

P3 = xr.open_dataset(os.sep.join([simdir3,''.join([simuname3,'_press.nc'])]),decode_times=False)
tsim3 = pd.date_range('1/1/2005 00:00:00', periods=len(P3.time), freq='D')
P3 = P3.assign_coords(time = pd.to_datetime(tsim3)) #for satur and press

stadic3=get_WT_simulation_results(P3,copy.deepcopy(stadic))
plot_WT_time_series(stadic=stadic3,simdir=simdir3,savefig=True)

P4 = xr.open_dataset(os.sep.join([simdir4,''.join([simuname4,'_press.nc'])]),decode_times=False)
tsim4 = pd.date_range('1/1/2005 00:00:00', periods=len(P4.time), freq='D')
P4 = P4.assign_coords(time = pd.to_datetime(tsim4)) #for satur and press
stadic4=get_WT_simulation_results(P4,copy.deepcopy(stadic))
plot_WT_time_series(stadic=stadic4,simdir=simdir4,savefig=True)


P1m=P1.where(P1.mask>0).sel(time=slice('2006-01-01', '2006-12-20'))
P2m=P2.where(P2.mask>0).sel(time=slice('2006-01-01', '2006-12-20'))
P3m=P3.where(P3.mask>0).sel(time=slice('2006-01-01', '2006-12-20'))
P4m=P4.where(P4.mask>0).sel(time=slice('2006-01-01', '2006-12-20'))
tot_thickness1= np.array(P1.var_dz[::-1,50,50]*3).cumsum()[-1] - P1.var_dz[0,50,50]*3/2
tot_thickness2= np.array(P2.var_dz[::-1,50,50]*3).cumsum()[-1] - P2.var_dz[0,50,50]*3/2
tot_thickness3= np.array(P3.var_dz[::-1,50,50]*3).cumsum()[-1] - P3.var_dz[0,50,50]*3/2
tot_thickness4= np.array(P4.var_dz[::-1,50,50]*3).cumsum()[-1] - P4.var_dz[0,50,50]*3/2


"""Plot maps: mean WTD"""
#~ fig,ax = plt.subplots(1,2,figsize = [7,7])
fig,ax = plt.subplots(2,2,figsize = [7,7])
for a in ax.flat: a.set_aspect(1)
#~ (tot_thickness - P1m.succion[:,0,:,:].mean(dim='time')).plot(ax=ax[0],robust=True,vmin=0,vmax=10,cbar_kwargs={'label':"WTD (m)"},add_labels=False)
#~ tmp = (tot_thickness - P2m.succion[:,0,:,:].mean(dim='time')).plot(ax=ax[1],robust=True,vmin=0,vmax=10,cbar_kwargs={'label':"WTD (m)"},add_labels=False)
ax1 = (tot_thickness1 - P1m.succion[:,0,:,:].mean(dim='time')).plot(ax=ax[0][0],robust=True,vmin=0,vmax=5,add_labels=False,add_colorbar=False)
ax2 = (tot_thickness2 - P2m.succion[:,0,:,:].mean(dim='time')).plot(ax=ax[0][1],robust=True,vmin=0,vmax=5,add_labels=False,add_colorbar=False)
ax3 = (tot_thickness3 - P3m.succion[:,0,:,:].mean(dim='time')).plot(ax=ax[1][0],robust=True,vmin=0,vmax=5,add_labels=False,add_colorbar=False)
ax4 = (tot_thickness4 - P4m.succion[:,0,:,:].mean(dim='time')).plot(ax=ax[1][1],robust=True,vmin=0,vmax=5,add_labels=False,add_colorbar=False)


#control colorbar
fig.colorbar(ax4, ax=ax.ravel().tolist(),label="WTD (m)")
#or:
#~ cbar_ax = fig.add_axes([0.15, 0.05, 0.7, 0.05])
#~ fig.colorbar(ax1, cax=cbar_ax,orientation='horizontal',label="WTD (m)")
for i,a in enumerate(ax.flat): a.set_xticklabels('');a.set_yticklabels('');a.set_title('simulation %d'%i)

ax[0][0].set_title(short1)
ax[0][1].set_title(short2)
ax[1][0].set_title(short3)
ax[1][1].set_title(short4)

#~ plt.gcf().subplots_adjust(bottom=0.05, top =0.95,left=0.15, hspace=0.02,wspace=0.2)

plt.savefig(os.sep.join([simdir1,'Figure_compare_WT_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir2,'Figure_compare_WT_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir3,'Figure_compare_WT_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir4,'Figure_compare_WT_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')


"""Plot maps: amplitude WTD"""
#~ fig,ax = plt.subplots(1,2,figsize = [7,7])
fig,ax = plt.subplots(2,2,figsize = [7,7])
for a in ax.flat: a.set_aspect(1)
#~ (tot_thickness - P1m.succion[:,0,:,:].mean(dim='time')).plot(ax=ax[0],robust=True,vmin=0,vmax=10,cbar_kwargs={'label':"WTD (m)"},add_labels=False)
#~ tmp = (tot_thickness - P2m.succion[:,0,:,:].mean(dim='time')).plot(ax=ax[1],robust=True,vmin=0,vmax=10,cbar_kwargs={'label':"WTD (m)"},add_labels=False)
ax1 = (P1m.succion[:,0,:,:].max(dim='time') - P1m.succion[:,0,:,:].min(dim='time')).plot(ax=ax[0][0],robust=True,vmin=0,vmax=5,add_labels=False,add_colorbar=False)
ax2 = (P2m.succion[:,0,:,:].max(dim='time') - P2m.succion[:,0,:,:].min(dim='time')).plot(ax=ax[0][1],robust=True,vmin=0,vmax=5,add_labels=False,add_colorbar=False)
ax3 = (P3m.succion[:,0,:,:].max(dim='time') - P3m.succion[:,0,:,:].min(dim='time')).plot(ax=ax[1][0],robust=True,vmin=0,vmax=5,add_labels=False,add_colorbar=False)
ax4 = (P4m.succion[:,0,:,:].max(dim='time') - P4m.succion[:,0,:,:].min(dim='time')).plot(ax=ax[1][1],robust=True,vmin=0,vmax=5,add_labels=False,add_colorbar=False)


#control colorbar
fig.colorbar(ax4, ax=ax.ravel().tolist(),label="WTD amplitude (m)")
#or:
#~ cbar_ax = fig.add_axes([0.15, 0.05, 0.7, 0.05])
#~ fig.colorbar(ax1, cax=cbar_ax,orientation='horizontal',label="WTD (m)")
for i,a in enumerate(ax.flat): a.set_xticklabels('');a.set_yticklabels('');a.set_title('simulation %d'%i)

ax[0][0].set_title(short1)
ax[0][1].set_title(short2)
ax[1][0].set_title(short3)
ax[1][1].set_title(short4)

#~ plt.gcf().subplots_adjust(bottom=0.05, top =0.95,left=0.15, hspace=0.02,wspace=0.2)

plt.savefig(os.sep.join([simdir1,'Figure_compare_WTamp_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir2,'Figure_compare_WTamp_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir3,'Figure_compare_WTamp_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir4,'Figure_compare_WTamp_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')



"""tmp"""
amp = (P1m.succion[:,0,:,:].max(dim='time') - P1m.succion[:,0,:,:].min(dim='time'))
depth = (tot_thickness1 - P1m.succion[:,0,:,:].mean(dim='time'))

amp2 = (P2m.succion[:,0,:,:].max(dim='time') - P2m.succion[:,0,:,:].min(dim='time')) 
depth2 = (tot_thickness2 - P2m.succion[:,0,:,:].mean(dim='time'))  

amp3 = (P3m.succion[:,0,:,:].max(dim='time') - P3m.succion[:,0,:,:].min(dim='time')) 
depth3 = (tot_thickness3 - P3m.succion[:,0,:,:].mean(dim='time'))   

amp4 = (P4m.succion[:,0,:,:].max(dim='time') - P4m.succion[:,0,:,:].min(dim='time')) 
depth4 = (tot_thickness4 - P4m.succion[:,0,:,:].mean(dim='time'))   

fig,ax = plt.subplots(1,1,figsize = [7,7])
#~ ax.scatter(amp,depth,c='k')
ax.scatter(amp.where(P1.veg_map==1,np.nan),depth.where(P1.veg_map==1,np.nan),c='g')#'Trees'
ax.scatter(amp.where(P1.veg_map==2,np.nan),depth.where(P1.veg_map==2,np.nan),c='y')#'Open Savanna'
ax.scatter(amp.where(P1.veg_map==3,np.nan),depth.where(P1.veg_map==3,np.nan),c='r')#'Herbaceous'
ax.scatter(amp2,depth2,c='b')
ax.scatter(amp3,depth3,c='r')
ax.scatter(amp4,depth4,c='g')

"""Plot hists"""
fig,ax = plt.subplots(1,1,figsize = [7,7])
(tot_thickness1 - P1m.succion[:,0,:,:].mean(dim='time')).plot.hist(ax=ax,bins=np.arange(0,5,0.2),density=True,histtype='step',color='r',label=short1)   
(tot_thickness2 - P2m.succion[:,0,:,:].mean(dim='time')).plot.hist(ax=ax,bins=np.arange(0,5,0.2),density=True,histtype='step',color='b',label=short2)   
(tot_thickness3 - P3m.succion[:,0,:,:].mean(dim='time')).plot.hist(ax=ax,bins=np.arange(0,5,0.2),density=True,histtype='step',color='m',label=short3)   
(tot_thickness4 - P4m.succion[:,0,:,:].mean(dim='time')).plot.hist(ax=ax,bins=np.arange(0,5,0.2),density=True,histtype='step',color='c',label=short4)   
plt.legend()
ax.set_xlabel('WTD (m)',fontsize=14)
ax.set_ylabel('frequency',fontsize=14)

plt.savefig(os.sep.join([simdir1,'Figure_compare_WT_hist_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir2,'Figure_compare_WT_hist_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir3,'Figure_compare_WT_hist_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir4,'Figure_compare_WT_hist_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')

"""Plot hists amplitude"""
fig,ax = plt.subplots(1,1,figsize = [7,7])
(P1m.succion[:,0,:,:].max(dim='time') - P1m.succion[:,0,:,:].min(dim='time')).plot.hist(ax=ax,bins=np.arange(0,5,0.2),density=True,histtype='step',color='r',label=short1)   
(P2m.succion[:,0,:,:].max(dim='time') - P2m.succion[:,0,:,:].min(dim='time')).plot.hist(ax=ax,bins=np.arange(0,5,0.2),density=True,histtype='step',color='b',label=short2)   
(P3m.succion[:,0,:,:].max(dim='time') - P3m.succion[:,0,:,:].min(dim='time')).plot.hist(ax=ax,bins=np.arange(0,5,0.2),density=True,histtype='step',color='m',label=short3)   
(P4m.succion[:,0,:,:].max(dim='time') - P4m.succion[:,0,:,:].min(dim='time')).plot.hist(ax=ax,bins=np.arange(0,5,0.2),density=True,histtype='step',color='c',label=short4)   
plt.legend()
ax.set_xlabel('WTD amplitude (m)',fontsize=14)
ax.set_ylabel('frequency',fontsize=14)

plt.savefig(os.sep.join([simdir1,'Figure_compare_WTamp_hist_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir2,'Figure_compare_WTamp_hist_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir3,'Figure_compare_WTamp_hist_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir4,'Figure_compare_WTamp_hist_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')


