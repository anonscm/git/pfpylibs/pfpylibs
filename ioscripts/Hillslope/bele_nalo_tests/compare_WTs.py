#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Compare 2 simulations: WT

    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime
import os,glob
import time
from netCDF4 import Dataset

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
plt.close('all')

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


def getSimWT(simdir,simuname='ref',xloc=0,yloc=0):
    """
    make a function of all the reading part
    """
    sim = PFout.PFout(simdir,'hillslope')
    sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
    sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')

    """Read PF outputs"""
    start_time = time.time()
    with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
        sim.satur_array = np.transpose(ds.variables['saturation'][:],[3,2,1,0])
    with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
        sim.press_array = np.transpose(ds.variables['succion'][:],[3,2,1,0])

    """ Read constant files"""
    ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
    #watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
    #~ sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,2)
    sim.dz_mult_arr = np.swapaxes(ds.variables['dz_mult'][:],0,2)
    sim.mannings_arr = np.zeros((sim.nx,sim.ny))+5e-6
    sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,1)
    #~ sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
    #~ sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
    sim.slope_x_arr = np.swapaxes(ds.variables['slope_x'][:],0,1)
    sim.slope_y_arr = np.swapaxes(ds.variables['slope_y'][:],0,1)
    sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,2)
    sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,2)
    #~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))
    sim.dz = np.diff(ds.variables['depth'][:])[0]
    sim.z = np.cumsum(sim.dz_mult_arr[0,0,::-1]*sim.dz) - sim.dz_mult_arr[0,0,::-1]*sim.dz/2
    sim.z = np.round(sim.z*1000)/1000

    """ extract a T-D slice """
    xloc=xloc
    yloc = yloc

    domain_thickness = np.round(np.cumsum(sim.dz_mult_arr[0,0,::-1]*sim.dz)[-1]*1000)/1000
    depth_last_cell = sim.z[-1]
    WT = domain_thickness - sim.press_array[xloc,yloc,0,:]
    WT = depth_last_cell - sim.press_array[xloc,yloc,0,:]

    """ Get All Water tables (perched)"""
    #first get a mask of the base layers of each saturated (p>0) contiguous (over Z) unit
    maskWT=np.zeros([sim.nz,sim.n_t])
    for t in range(sim.n_t):
        profile = np.zeros(sim.nz)
        #profile[0] = base of domain. if the criteria is p < 0 => 0, you realize there are some weird points at the bottom
        #of perched layers with p>0 but < sim.dz ... so as if the cell is not entirely saturated but p>0 ... 
        profile = np.array([0. if sim.press_array[xloc,yloc,i,t]<= sim.dz_mult_arr[xloc,yloc,i]*sim.dz else 1. for i in range(sim.nz)])
        for i in range(sim.nz-1):
            profile[i+1]=(profile[i]+profile[i+1])*profile[i+1]
        # a=[1,1,1,0,0,0,1,1,0,1,0,0,0,1] => [1, 2, 3, 0, 0, 0, 1, 2, 0, 1, 0, 0, 0, 1]
        profile[profile!=1.]=np.nan
        maskWT[:,t] = profile

    WTs = np.resize(sim.z[::-1],[sim.n_t,sim.nz]).T - (maskWT*sim.press_array[xloc,yloc,:,:])
    return WTs

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##


xloc=0
yloc=30
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Hillslope/bele_nalo_tests/NCout/test_2020_05_29'
simuname = 'test'
WTs1 = getSimWT(simdir,simuname,xloc,yloc)

simdir = r'/home/hectorb/PARFLOW/PROJECTS/Hillslope/bele_nalo_tests/NCout/test_rip_2020_05_29'
simuname = 'test_rip'
WTs2 = getSimWT(simdir,simuname,xloc,yloc)

xloc=0
yloc=0
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Hillslope/bele_nalo_tests/NCout/test_2020_05_29'
simuname = 'test'
WTs1_side = getSimWT(simdir,simuname,xloc,yloc)
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Hillslope/bele_nalo_tests/NCout/test_rip_2020_05_29'
simuname = 'test_rip'
WTs2_side = getSimWT(simdir,simuname,xloc,yloc)


""" Read Forcings: """
forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
simdir = simdir
forc = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')
forc.rename(columns=forcing_headers,inplace=True)
forc.index=pd.date_range('1/1/2006 00:00:00', periods=len(forc), freq='30min')


""" Read Forcings: """
simdir = simdir
forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
#### WATCH out, the separator is space in this file, no pb with CLM??
forc2 = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')
forc2.rename(columns=forcing_headers,inplace=True)
forc2.index=pd.date_range('1/1/2006 00:00:00', periods=len(forc2), freq='30min')


""" Plot Figure
2 panels 
"""
tsim1 = pd.date_range('1/1/2006 00:00:00', periods=np.shape(WTs1)[1], freq='D')
tsim2 = pd.date_range('1/1/2006 00:00:00', periods=np.shape(WTs2)[1], freq='D')

#~ fig, ax = plt.subplots(2, 1,figsize=(20,10))
#A4 in inches is 8.3 * 11.7
fig, ax = plt.subplots(2,1,figsize=(8,4))
tsim = pd.date_range('1/1/2006 00:00:00', periods=np.shape(WTs1)[1], freq='D')

""" Upper panel"""
for i in range(np.shape(WTs1)[0]):
    lines1, = ax[0].plot(tsim1,WTs1[i,:],'k',linewidth=0.7)
for i in range(np.shape(WTs2)[0]):
    lines2, = ax[0].plot(tsim2,WTs2[i,:],'r',linewidth=0.5)
 
ax[0].set_xlim([datetime.datetime(2006,1,1),max(tsim)])
ax[0].set_ylim([0,22])
ax[0].legend((lines1,lines2),('sim1','sim2'),loc='lower right',fontsize=7,ncol=6)
ax[0].set_ylabel('WT depth (m)', fontsize=9, color = 'k')
ax[0].tick_params(axis='both', which='major', labelsize=9,bottom='on',top='on',right='on',left='on')
ax[0].tick_params(axis='x', which='both', labelbottom='off', labeltop='off')
ax[0].text(datetime.datetime(2006,1,20),0.6,r'a)',fontsize=9)


""" Lower Panel """
for i in range(np.shape(WTs1_side)[0]):
    lines1, = ax[1].plot(tsim1,WTs1_side[i,:],'k',linewidth=0.7)
for i in range(np.shape(WTs2_side)[0]):
    lines2, = ax[1].plot(tsim2,WTs2_side[i,:],'r',linewidth=0.5)
ax[1].set_xlim([datetime.datetime(2006,1,1),max(tsim1)])
ax[1].set_ylim([0,22])
ax[1].legend((lines1,lines2),('sim1','sim2'),loc='upper right',fontsize=7,ncol=6)
ax[1].tick_params(axis='both', which='major', labelsize=9,bottom='on',top='on',right='on',left='on')
ax[1].set_ylabel('WT depth (m)', fontsize=9, color = 'k')
ax[1].tick_params(axis='y', which='both', labelleft='on', labelright='off')
ax[1].text(datetime.datetime(2006,1,20),0.6,r'b)',fontsize=9)

ax2 = ax[1].twinx()
ax2.plot(forc['P'].resample('D').sum()*30*60,'b')
ax2.plot(forc2['P'].resample('D').sum()*30*60,'r')
ax2.set_ylim([0,190])
ax2.set_yticks([0,20,40,60])
ax2.yaxis.set_label_position("right")
ax2.tick_params(axis='y',colors='b',labelsize=9)
ax2.set_ylabel('Precip (mm/d)', fontsize=9, color = 'b')

for i in range(2):
    fig.axes[i].invert_yaxis()
fig.subplots_adjust(bottom=0.05, top =0.98,left=0.07,right =0.93,wspace=0.04, hspace=0.001)

simdir =simdir

plt.savefig(os.sep.join([simdir,'Figure_compare_WT_outlet.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir,'Figure_compare_WT_outlet.pdf']),dpi=400,format='pdf')
