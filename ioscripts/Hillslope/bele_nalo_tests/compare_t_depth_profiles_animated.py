#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Extract time - depth slices at given location from single simulation

    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import datetime
import pandas as pd
from netCDF4 import Dataset
import os,glob
import time
import matplotlib.cm as cm
from scipy import interpolate
from matplotlib.animation import FuncAnimation
import matplotlib.animation as animation
"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout

plt.close("all")

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Hillslope/NCout/test_2020_05_29'
simuname = 'test'

daynumber = 0

"""
CLM_var is a dictionnary where keys are local variable names as in PFCLM
values are lists of string[filename, PFCLM variable name, long name, units]
"""
CLM_var={'surf_press':['','h_top','Surface pressure','m']}

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


def getSim(simdir,simuname='ref',xloc=0,yloc=0):
    """
    make a function of all the reading part
    """
    sim = PFout.PFout(simdir,'hillslope')
    sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
    sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_h_top.nc'])]),filetype='CLM')

    """Read PF outputs"""
    start_time = time.time()
    with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
        sim.satur_array = np.transpose(ds.variables['saturation'][:],[3,2,1,0])
    with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
        sim.press_array = np.transpose(ds.variables['succion'][:],[3,2,1,0])
    """Read CLM outputs"""
    """ add the filename in the dic"""        
    for k,val in CLM_var.items():
        CLM_var[k][0]=os.sep.join([simdir,''.join([simuname,'_'+val[1]+'.nc'])])    
        
    for k,val in CLM_var.items():
        try:
            with Dataset(val[0], 'r') as ds:
                print('read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
                exec('sim.%s=np.swapaxes(ds.variables[\'%s\'][:],0,2)'%(k,val[1]))        
        except FileNotFoundError:
                print('CANNOT read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
    print("--- %d seconds --- read all PFCLM outputs from NCs" % (time.time() - start_time))

    """ Read constant files"""
    ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
    #watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
    #~ sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,2)
    sim.dz_mult_arr = np.swapaxes(ds.variables['dz_mult'][:],0,2)
    sim.mannings_arr = np.zeros((sim.nx,sim.ny))+5.6e-6
    sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,1)
    sim.mask_arr = np.transpose(ds.variables['mask'][:])
    topmask = sim.mask_arr[:,:,0]
    MASK=topmask>0     
    #~ sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
    #~ sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
    sim.slope_x_arr = np.swapaxes(ds.variables['slope_x'][:],0,1)
    sim.slope_y_arr = np.swapaxes(ds.variables['slope_y'][:],0,1)
    sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,2)
    sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,2)
    #~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))
    sim.dz = np.diff(ds.variables['depth'][:])[0]
    sim.z = np.cumsum(sim.dz_mult_arr[0,0,::-1]*sim.dz) - sim.dz_mult_arr[0,0,::-1]*sim.dz/2
    sim.z = np.round(sim.z*1000)/1000
    # need to hard code the following because for 2D simulation dx = nan which sets Q to 0
    #~ sim.dx = 10.0
    sim.dx = 20.0

    return sim,MASK
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
simuname1 = 'hillslope'
#~ simdir1 = r'/homelocal5/phyrev/_PARFLOW/PF_lectures/hillslope_course/hillslope2018/test/NCoutputs/hillslope_2018_11_28'
simdir1 = r'/home/hectorb/PARFLOW/PF_lectures/hillslope_course/hillslope2019/test/NCoutputs/hillslope_2018_11_28'

simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Hillslope/bele_nalo_tests/NCout/test_2020_05_29'
simuname1 = 'test'
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Hillslope/bele_nalo_tests/NCout/test_11yrs_2020_05_29'
simuname1 = 'test_11yrs'
sim,MASK = getSim(simdir1,simuname1)

simuname2 = 'hillslope2'
#~ simdir2 = r'/homelocal5/phyrev/_PARFLOW/PF_lectures/hillslope_course/hillslope2018/test/NCoutputs/hillslope3_2018_11_28'
simdir2 = r'/home/hectorb/PARFLOW/PF_lectures/hillslope_course/hillslope2019/test/NCoutputs/hillslope2_2018_11_28'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Hillslope/bele_nalo_tests/NCout/test_rip_2020_05_29'
simuname2 = 'test_rip'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Hillslope/bele_nalo_tests/NCout/test_rip_11yrs_2020_05_29'
simuname2 = 'test_rip_11yrs'
sim2,MASK = getSim(simdir2,simuname2)

slope=0.03
catchmentsurf=np.sum(MASK)*sim.dx*sim.dy

sim.calculate_runoff_from_press(x_outlet = 0,y_outlet = 0)
sim.surfQ = pd.DataFrame(sim.surfQ/catchmentsurf)
sim.surfQ.index=pd.date_range('1/1/2006 00:00:00', periods=sim.n_t_clm, freq='H')
sim.surfQ = sim.surfQ.resample('D').sum()

sim2.calculate_runoff_from_press(x_outlet = 0,y_outlet = 0)
sim2.surfQ = pd.DataFrame(sim2.surfQ/catchmentsurf)
sim2.surfQ.index=pd.date_range('1/1/2006 00:00:00', periods=sim.n_t_clm, freq='H')
sim2.surfQ = sim2.surfQ.resample('D').sum()

domain_thickness=sim.dz_mult_arr[0,0,:].sum()*sim.dz
# pressure at the bottom cell is obtained at half the cell thickness:
offset_for_WTD = domain_thickness - sim.dz_mult_arr[0,0,0]*sim.dz/2
WTD=offset_for_WTD-sim.press_array[0,::-1,0,:]
#~ WTD[WTD<0]=0
WTD2=offset_for_WTD-sim2.press_array[0,::-1,0,:]
#~ WTD2[WTD2<0]=0
y = sim.y
dy = sim.dy
ny = sim.ny
z = [0.01,0.02,0.03,0.04,0.09,0.18,0.27,0.36,0.45,0.58,0.74,0.9,1.06,1.22,1.42,1.66,1.9,2.14,2.38,4.65,8.95,13.25,17.55,21.58]
z = np.cumsum(sim.dz_mult_arr[0,0,::-1])
#adapt the plot to the actual slope: 
# 1) interpolate on a finer matrix, the same dimensions as original, define the finness, as a function of the slope
# 2) place each column on a larger dimension (in Z) matrix by shifting each column depending on slope
dz_step = slope * dy
print('dz_step: %f (topographic diff betw one cell and the next one)'%dz_step)
ndiv_dz_step = 5
print('ndiv_dz_step: %d (resolution to discretize this topo diff)'%ndiv_dz_step)
Ztot = 24.0-0.
ncells_Z = np.int(Ztot / (dz_step/ndiv_dz_step) +1)
print('new number of cells (Ztot / (dz_step/ndiv_dz_step): %d'%ncells_Z)
deniv_tot = slope * (np.max(y)-np.min(y))
print('total deniv: %f'%deniv_tot)
ncells_Z_withslope = np.int(ncells_Z + (ny-1) * ndiv_dz_step)
print('number of cells with slope (ncells_Z + (ny-1) * ndiv_dz_step): %d'%ncells_Z_withslope)

#~ z2 = np.linspace(0,24,101)
z2 = np.linspace(0,24,np.int(ncells_Z))
#~ z3 = np.linspace(0,24 * 2 ,201)
z3 = np.linspace(0,24 + np.int(dy*ny*slope) ,np.int(ncells_Z_withslope))

satur_int1 = np.zeros((1,sim.ny,np.size(z2),sim.n_t))
satur_int2 = np.zeros((1,sim.ny,np.size(z2),sim.n_t))
yy,zz = np.meshgrid(y,z)
for i in range(sim.n_t):
    #~ tmp = np.reshape(sim.satur_array[0,:,:,i],(sim.ny,sim.nz))    
    tmp = np.reshape(sim.satur_array[0,:,::-1,i],(sim.ny,sim.nz))    
    #~ tmp2 = np.reshape(sim2.satur_array[0,:,:,i],(sim.ny,sim.nz))        
    tmp2 = np.reshape(sim2.satur_array[0,:,::-1,i],(sim.ny,sim.nz))        
    f1 = interpolate.interp2d(z,y,tmp,kind='linear')
    f2 = interpolate.interp2d(z,y,tmp2,kind='linear')
    #~ f = interpolate.interp2d(zz,yy,tmp,kind='linear')
    satur_int1[0,:,:,i]=f1(z2,y)
    satur_int2[0,:,:,i]=f2(z2,y)

satur_int_slope1 = np.empty((1,sim.ny,np.size(z3),sim.n_t))
satur_int_slope1[:]=np.nan
satur_int_slope2 = np.empty((1,sim.ny,np.size(z3),sim.n_t))
satur_int_slope2[:]=np.nan
zdeniv = np.zeros(sim.ny)
#~ zdeniv[1:]=-10*slope
zdeniv[1:]=-20*slope

for i in range(sim.ny):
    #~ satur_int_slope1[0,i,200-(i*2)-101:200-i*2,:] = satur_int1[0,i,:,:]
    #~ satur_int_slope2[0,i,200-(i*2)-101:200-i*2,:] = satur_int2[0,i,:,:]
    satur_int_slope1[0,i,(np.int(ncells_Z_withslope))-(i*ndiv_dz_step)-ncells_Z:(ncells_Z_withslope)-(i*ndiv_dz_step),:] = satur_int1[0,i,:,:]
    satur_int_slope2[0,i,(np.int(ncells_Z_withslope))-(i*ndiv_dz_step)-ncells_Z:(ncells_Z_withslope)-(i*ndiv_dz_step),:] = satur_int2[0,i,:,:]
    
#~ satur_slope = np.nan((1,ny,np.size(z2),n_t))

satur_int_slopem1 = np.ma.masked_where(np.isnan(satur_int_slope1),satur_int_slope1)
satur_int_slopem2 = np.ma.masked_where(np.isnan(satur_int_slope2),satur_int_slope2)

fig, (ax1, ax2) = plt.subplots(2, 2,figsize=(20,8))

cmap=cm.jet_r
cmap.set_bad('w',1.)
#~ pcol1 = ax1[0].pcolormesh(np.array(y),np.array(z),sim.satur_array[0,:,:,0].T,cmap=cmap, shading = 'gouraud')
pcol1 = ax1[0].pcolormesh(np.array(y),np.array(z3),satur_int_slopem1[0,::-1,:,daynumber].T,cmap=cmap, shading = 'gouraud')
plt1 = ax1[0].plot(y,WTD[:,daynumber]-np.cumsum(zdeniv),'k-')
fig.axes[0].invert_yaxis()
ax1[0].margins(x=0)
ax1[0].margins(y=0)
leg = plt.cm.ScalarMappable(cmap=cmap)
leg.set_array([0.,1.])
cbar2 = fig.colorbar(leg, ax = ax1[0])
cbar2.ax.set_title('saturation')
ax1[0].set_ylabel('Depth (m)', fontsize=16, color = 'k')

#~ pcol2 = ax2[0].pcolormesh(np.array(y),np.array(z),sim2.satur_array[0,:,:,0].T,cmap=cmap, shading = 'gouraud')
pcol2 = ax2[0].pcolormesh(np.array(y),np.array(z3),satur_int_slopem2[0,::-1,:,daynumber].T,cmap=cmap, shading = 'gouraud')
plt2 = ax2[0].plot(y,WTD2[:,daynumber]-np.cumsum(zdeniv),'k-')
fig.axes[2].invert_yaxis()
ax2[0].margins(x=0)
ax2[0].margins(y=0)
leg = plt.cm.ScalarMappable(cmap=cmap)
leg.set_array([0.,1.])
cbar = fig.colorbar(leg, ax = ax2[0])
cbar.ax.set_title('saturation')
ax2[0].set_ylabel('Depth (m)', fontsize=16, color = 'k')
ax2[0].set_xlabel('Y (m)', fontsize=16, color = 'k')

ylim = 0.1
ax1[1] = sim.surfQ.plot(ax = ax1[1])
ax1[1].set_ylabel('mm/day', fontsize=16)
ax1[1].set_ylim(0,ylim)
ann = ax1[1].annotate("",xy=(sim.surfQ.index[daynumber], sim.surfQ[0][daynumber]), xycoords='data',
    xytext=(sim.surfQ.index[daynumber],32), textcoords='data',
    arrowprops=dict(arrowstyle="->",
    connectionstyle="arc3"))


ax2[1] = sim2.surfQ.plot(ax = ax2[1])
ax2[1].set_ylabel('mm/day', fontsize=16)
ax2[1].set_ylim(0,ylim)
ann2 = ax2[1].annotate("",xy=(sim2.surfQ.index[daynumber], sim2.surfQ[0][daynumber]), xycoords='data',
    xytext=(sim2.surfQ.index[daynumber],32), textcoords='data',
    arrowprops=dict(arrowstyle="->",
    connectionstyle="arc3"))


# define animation:
def update(i):
    global plt1,plt2,pcol1,pcol2, ann, ann2
    label = 'timestep {0}'.format(i)
    print(label)
    # Update the line and the axes (with a new xlabel). Return a tuple of
    # "artists" that have to be redrawn for this frame.
    ann.remove()
    ann2.remove()
    plt1[0].remove()
    plt2[0].remove()
    pcol1.remove()
    pcol2.remove()
    #~ pcol1 = ax1[0].pcolormesh(np.array(y),np.array(z),sim.satur_array[0,::-1,:,i].T,cmap=cmap, shading = 'gouraud')
    pcol1 = ax1[0].pcolormesh(np.array(y),np.array(z3),satur_int_slopem1[0,::-1,:,i].T,cmap=cmap, shading = 'gouraud')
    plt1 = ax1[0].plot(y,WTD[:,i]-np.cumsum(zdeniv),'k-')
    #~ pcol2 = ax2[0].pcolormesh(np.array(y),np.array(z),sim2.satur_array[0,::-1,:,i].T,cmap=cmap, shading = 'gouraud')
    pcol2 = ax2[0].pcolormesh(np.array(y),np.array(z3),satur_int_slopem2[0,::-1,:,i].T,cmap=cmap, shading = 'gouraud')
    plt2 = ax2[0].plot(y,WTD2[:,i]-np.cumsum(zdeniv),'k-')
    ann = ax1[1].annotate("",xy=(sim.surfQ.index[i], sim.surfQ[0][i]), xycoords='data',
        xytext=(sim.surfQ.index[i],32), textcoords='data',
        arrowprops=dict(arrowstyle="->",
        connectionstyle="arc3"))
    ann2 = ax2[1].annotate("",xy=(sim2.surfQ.index[i], sim2.surfQ[0][i]), xycoords='data',
        xytext=(sim2.surfQ.index[i],32), textcoords='data',
        arrowprops=dict(arrowstyle="->",
        connectionstyle="arc3"))
    return (ax1[0],ax1[1],ax2[0],ax2[1])


#animate
Writer = animation.writers['ffmpeg']
#~ writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)
writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=5000)

anim = FuncAnimation(fig, update, frames=np.arange(0, 720, 10), interval=20, repeat=False)
#~ anim.save(os.sep.join([simdir,'compare_%s_and_%s.mp4'%(simuname1,simuname2)]), writer=writer)
plt.show()

