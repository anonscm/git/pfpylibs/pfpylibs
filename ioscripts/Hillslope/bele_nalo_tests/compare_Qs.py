#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    Compare 2 simulations: Streamflow
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os,glob
from netCDF4 import Dataset
import time
import datetime
from matplotlib.dates import MonthLocator, DateFormatter
#import locale
#locale.setlocale(locale.LC_ALL,'en_US.UTF-8')

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout

plt.close("all")


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

statsDay =False

"""
CLM_var is a dictionnary where keys are local variable names as in PFCLM
values are lists of string[filename, PFCLM variable name, long name, units]
"""
CLM_var={'surf_press':['','h_top','Surface pressure','m']}

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def minimalist_xldate_as_datetime(xldate, datemode):
    # datemode: 0 for 1900-based, 1 for 1904-based
    return (
        datetime.datetime(1899, 12, 30)
        + datetime.timedelta(days=xldate + 1462 * datemode)
        )
        
def roundDateTime(shakyDT,nearest_minutes=1):
	"""
	"""
	shakyDT += datetime.timedelta(minutes=nearest_minutes/2)
	shakyDT -= datetime.timedelta(minutes=shakyDT.minute % nearest_minutes,
                         seconds=shakyDT.second,
                         microseconds=shakyDT.microsecond)
	return shakyDT
	

def getSimQ(simdir,simuname='ref',x_outlet=0,y_outlet=0):    
    """make a function of all the reading part"""
    
    """ add the filename in the dic"""        
    for k,val in CLM_var.items():
        CLM_var[k][0]=os.sep.join([simdir,''.join([simuname,'_'+val[1]+'.nc'])])    
        
    #### Read & process simulation outputs:
    sim = PFout.PFout(simdir,'hillslope')
    sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
    sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_h_top.nc'])]),filetype='CLM')

    """Read PF outputs"""
    start_time = time.time()
    with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
        sim.satur_array = ds.variables['saturation'][:]
    with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
        sim.press_array = ds.variables['succion'][:] 

    """Read CLM outputs"""
    for k,val in CLM_var.items():
        try:
            with Dataset(val[0], 'r') as ds:
                print('read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
                exec('sim.%s=np.swapaxes(ds.variables[\'%s\'][:],0,2)'%(k,val[1]))        
        except FileNotFoundError:
                print('CANNOT read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
    print("--- %d seconds --- read all PFCLM outputs from NCs" % (time.time() - start_time))

    """ Read constant files"""
    ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
    #watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
    sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,1)
    sim.mannings_arr = np.zeros((sim.nx,sim.ny))+5e-6
    sim.mask_arr = np.transpose(ds.variables['mask'][:])
    topmask = sim.mask_arr[:,:,0]
    MASK=topmask>0 
    """MASK is a nx x ny boolean mask of the domain
    When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
    (as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
    """    
    sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
    sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
    sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,1)
    sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,1)

    """ Read Forcings: """
    #~ forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
    #~ forc = pd.read_csv(os.sep.join([simdir,'forcagePF.txt.0']),header=None,sep='\t')
    #~ forc = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')
    #~ forc.rename(columns=forcing_headers,inplace=True)
    #~ forc.index=pd.date_range('1/1/2006 00:00:00', periods=len(forc), freq='30min')
    #~ sim.P=sum(forc['P'])*30*60


    """Create CLM output DataFrames: """
    dsclm=pd.DataFrame()
    # need to hard code the following because for 2D simulation dx = nan which sets Q to 0
    sim.dx = 10.0
    sim.calculate_runoff_from_press(x_outlet = x_outlet,y_outlet = y_outlet)
    dsclm['Q']=sim.surfQ
    catchmentsurf=np.sum(MASK)*sim.dx*sim.dy
    catchmentsurf=np.sum(MASK[:,0:-3])*sim.dx*sim.dy
    dsclm.index=pd.date_range('1/1/2006 00:00:00', periods=sim.n_t_clm, freq='H')

    return dsclm,catchmentsurf


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
simuname = 'hillslope'
#~ simdir = r'/homelocal5/phyrev/_PARFLOW/PF_lectures/hillslope_course/hillslope2018/veg/NCoutputs/hillslope_2018_11_28'
simdir = r'/home/hectorb/PARFLOW/PF_lectures/hillslope_course/hillslope2019/test/NCoutputs/hillslope_2018_11_28'
d1,catchmentsurf = getSimQ(simdir,simuname,x_outlet=0,y_outlet=0)

simuname = 'hillslope2'
#~ simdir = r'/homelocal5/phyrev/_PARFLOW/PF_lectures/hillslope_course/hillslope2018/veg/NCoutputs/hillslope3_2018_11_28'
simdir = r'/home/hectorb/PARFLOW/PF_lectures/hillslope_course/hillslope2019/test/NCoutputs/hillslope2_2018_11_28'
d2,catchmentsurf = getSimQ(simdir,simuname,x_outlet=0,y_outlet=0)

"""PLOT"""
#A4 in inches is 8.3 * 11.7
fig,ax = plt.subplots(1,1,figsize=(8,2), squeeze=True)
yr = 2007
#~ threshold=0.1 #"""m3/hr"""
simu1 = d1['Q'][d1.index.year==yr]
#~ simu1[simu1<=threshold]=np.nan
simu2 = d2['Q'][d2.index.year==yr]
#~ simu2[simu2<=threshold]=np.nan

""" in case you want it in days"""
#~ simu1 = simu1.resample('D').sum()
#~ simu2= simu2.resample('D').sum()

# every month
months = MonthLocator(bymonth=range(1, 13), bymonthday=1, interval=1)
monthsFmt = DateFormatter("%b")

ax.plot(simu1,'k',linewidth=0.5)
ax.plot(simu2,'r',linewidth=0.5)

ax.set_ylim([0.05,100])
#~ ax.set_yscale('log')
#~ ax.set_ylabel('$m^3/day$',fontsize=9)
ax.set_ylabel('$m^3$',fontsize=9)
ax.legend(['sim1','sim2'],loc='lower left',ncol=5,fontsize=9)    
ax.xaxis.set_major_locator(months)
ax.xaxis.set_major_formatter(monthsFmt)
ax.tick_params(axis='both', which='major', labelsize=9)

fig.subplots_adjust(bottom=0.1, top =0.99,left=0.07,right=0.99, hspace=0.001)

simdir = simdir
#~ plt.savefig(os.sep.join([simdir,'Q_comp_%d.png'%yr]),dpi=400,format='png')
#~ plt.savefig(os.sep.join([simdir,'Q_comp_%d.pdf'%yr]),dpi=400,format='pdf')

