"""
- Read overland sum pfb output file
- create hydrograph for selected cell
Created on Mon Sep 29 11:47:42 2014

@author: jgilbert
modified by savannah
"""

def PFB_read(pfb_file_path, flag):
    import struct
    import numpy
    #print("**This code DOES handle subgrids**")
    
    hdr_dict = {}
    
    with open(pfb_file_path,'rb') as pfbf:
        hdr_dict['len_x'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['len_y'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['len_z'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['nx'] = struct.unpack('>i', pfbf.read(4))[0]
        hdr_dict['ny'] = struct.unpack('>i', pfbf.read(4))[0]
        hdr_dict['nz'] = struct.unpack('>i', pfbf.read(4))[0]
        hdr_dict['dx'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['dy'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['dz'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['ns'] = struct.unpack('>i', pfbf.read(4))[0]
        ns = hdr_dict['ns']
        #print(ns)
        nx = hdr_dict['nx']
        ny = hdr_dict['ny']
        nz = hdr_dict['nz']
        
        if nx == 0:
            nx = int(raw_input("What should the NX be?"))
            hdr_dict['nx'] = nx
        if ny == 0:
            ny = int(raw_input("What should the NY be?"))
            hdr_dict['ny'] = ny
        if nz ==0:
            nz =int(raw_input("What should the NZ be?"))
            hdr_dict['nz'] = nz
        
        len_x = hdr_dict['len_x']
        #print "len_x, len_y, len_z, nx ny nz --> %f %f %f %i %i %i" %(len_x, len_x, len_x, nx, ny, nz)
        
        pfb_array = numpy.zeros((nx, ny, nz))
        
        for subgrids in range(ns):
            hdr_dict['ix'] = struct.unpack('>i', pfbf.read(4))[0]
            ix = hdr_dict['ix']
            hdr_dict['iy'] = struct.unpack('>i', pfbf.read(4))[0]
            iy = hdr_dict['iy']
            #print(iy)
            hdr_dict['iz'] = struct.unpack('>i', pfbf.read(4))[0]
            iz = hdr_dict['iz']
            hdr_dict['nx2'] = struct.unpack('>i', pfbf.read(4))[0]
            nx2 = hdr_dict['nx2']
            hdr_dict['ny2'] = struct.unpack('>i', pfbf.read(4))[0]
            ny2 = hdr_dict['ny2']
            #print(ny2)
            hdr_dict['nz2'] = struct.unpack('>i', pfbf.read(4))[0]
            nz2 = hdr_dict['nz2']
            hdr_dict['rx'] = struct.unpack('>i', pfbf.read(4))[0]
            rx = hdr_dict['rx']
            hdr_dict['ry'] = struct.unpack('>i', pfbf.read(4))[0]
            ry = hdr_dict['ry']
            hdr_dict['rz'] = struct.unpack('>i', pfbf.read(4))[0]
            rz = hdr_dict['rz']
            #print("ix iy iz nx2 ny2 nz2 rx ry rz --> %i %i %i %i %i %i %i %i %i\n" %(ix,iy, iz, nx2, ny2, nz2, rx,ry, rz))
            for lays in range(nz2):
                for rows in range(ny2):
                    for cols in range(nx2):
                        val = struct.unpack('>d', pfbf.read(8))[0]
                        #q = raw_input(val)
                        pfb_array[(cols+ix), (rows + iy), (nz2-(lays + iz)-1)] = val
                        #pfb_array[(cols+ix-1), (nx2-(iy)-1), (nz2-(lays + iz)-1)] = val
                        #pfb_array[(rows + iy-1), (cols+ix-1), (nz2-(lays + iz)-1)] = val
                        #print val
                        
    if flag ==1:
        return(pfb_array, hdr_dict)
    else:
        return(pfb_array)

###############################################################################
## MAIN SECTION : Select Files (pfb) and Select what you want done with them ##
###############################################################################

import numpy as np
import os.path
import time
import matplotlib.pyplot as plt
import pylab
import matplotlib.image as mpimg
import plotly.plotly as py
import math
import os
import pandas as pd
import matplotlib.cm as cm

from matplotlib.animation import FuncAnimation

# Print the full arrays and tupples
np.set_printoptions(threshold=np.nan)

# Set number of time steps from output to use
n_t = 366
#~ n_t = 731

dump_int = 24

n_t_clm = 8760
#~ n_t_clm = 17520
dump_int_clm = 1



"""converter"""
#~ conv = 15
#~ conv_clm = 900
conv = 1
# from mm/s to mm/hr
conv_clm = 1 * 60 * 60


# Domaine
#m2
aire = 5000

cwd = os.getcwd()
# Directory containing the pfb results of the PF simulation

#~ RT_dir = r'../../hillslope2017'
#~ RT_dir = r'../../hillslope2017/SAINTSULPICE'
#~ RT_dir = r'../../hillslope2017/OUINE'
RT_dir = r'./'

#~ base_dir = '/'.join([RT_dir,'results'])
#~ base_dir = '/'.join([RT_dir,'results_perm'])
#~ base_dir = '/'.join([RT_dir,'results5'])
base_dir = '/'.join([RT_dir,'results3'])

print("Found base directory...")

# Defining the file names of the outputs
#~ runnam = r'hillslope_sens'
runnam = r'hillslope'

prop_base = os.path.join(base_dir, '.'.join([runnam, "out"]))

# Use out_type and out_base to chose the property
out_type = r'overlandsum'
out_base = os.path.join(base_dir, '.'.join([runnam,"out",out_type]))

# Get format of property arrays with the geometry : overland flow and pressure
if os.path.isfile('.'.join([prop_base,"press",format(0,"05.0F"),"pfb"])):
    
    [test_array, hdr] =  PFB_read('.'.join([prop_base,"press",format(0,"05.0F"),"pfb"]),1)      
    
    nz = hdr['nz']
    nx = hdr['nx']
    ny = hdr['ny']
    dx = hdr['dx']
    dy = hdr['dy']
# +1 car 00000.pfb existe
    press_array = np.zeros((nx,ny,nz,n_t))
    satur_array = np.zeros((nx,ny,nz,n_t))   
    
    overlandsum_array = np.zeros((nx,ny,n_t-1))
    evaptranssum_array = np.zeros((nx,ny,n_t-1))
    evaptrans_array = np.zeros((nx,ny,n_t-1)) 
    
# 36 nombre de sorties clm    
    clm_output_array = np.zeros((nx,ny,36,n_t_clm))

    tmp_arr = np.zeros((nx,ny,nz))
#constant values
    if os.path.isfile('.'.join([prop_base,"dz_mult.pfb"])):
        dz_mult_fp = '.'.join([prop_base,"dz_mult.pfb"])
        dz_mult_arr = PFB_read(dz_mult_fp,0)
    else:
        dz_mult_arr = tmp_arr * 1
            
    if os.path.isfile('.'.join([prop_base,"mannings.pfb"])):    
        mannings_fp = '.'.join([prop_base,"mannings.pfb"])
        mannings_arr = PFB_read(mannings_fp,0)
    else:
        mannings_arr = tmp_arr + 5.5e-6
    
    mask_fp = '.'.join([prop_base,"mask.pfb"])
    mask_arr = PFB_read(mask_fp,0)
    
    
    #~ perm_x_fp = '.'.join([prop_base,"perm_x.pfb"])
    #~ perm_x_arr = PFB_read(perm_x_fp,0)
    #~ 
    #~ perm_y_fp = '.'.join([prop_base,"perm_y.pfb"])
    #~ perm_y_arr = PFB_read(perm_y_fp,0)
#~ 
    #~ perm_z_fp = '.'.join([prop_base,"perm_z.pfb"])
    #~ perm_z_arr = PFB_read(perm_z_fp,0)

    porosity_fp = '.'.join([prop_base,"porosity.pfb"])
    porosity_arr = PFB_read(porosity_fp,0)

    slope_x_fp = '.'.join([prop_base,"slope_x.pfb"])
    slope_x_arr = PFB_read(slope_x_fp,0)
    
    slope_y_fp = '.'.join([prop_base,"slope_y.pfb"])
    slope_y_arr = PFB_read(slope_y_fp,0)
    
    specific_storage_fp = '.'.join([prop_base,"specific_storage.pfb"])
    specific_storage_arr = PFB_read(specific_storage_fp,0)    
    

# Fill in property arrays with space and time dependant values
for t in range(n_t):

    # Get all time file directories
    press_fp = '.'.join([prop_base,"press",format(t*dump_int,"05.0F"),"pfb"])
    satur_fp = '.'.join([prop_base,"satur",format(t*dump_int,"05.0F"),"pfb"])

    # Read pfb
    press_arr = PFB_read(press_fp,0)
    satur_arr = PFB_read(satur_fp,0)
   
    #Fill arrays for each time output
    press_array[:,:,:,t]=press_arr[:,:,:]
    satur_array[:,:,:,t]=satur_arr[:,:,:]


for t in range(n_t-1):
    # Get all time file directories
    overlandsum_fp = '.'.join([prop_base,"overlandsum",format((t+1)*dump_int,"05.0F"),"pfb"])
    evaptranssum_fp = '.'.join([prop_base, "evaptranssum", format((t+1)*dump_int,"05.0F"),"pfb"])
    evaptrans_fp = '.'.join([prop_base, "evaptrans", format((t+1)*dump_int,"05.0F"),"pfb"])

    # Read pfb
    overlandsum_arr = PFB_read(overlandsum_fp,0)
    evaptranssum_arr = PFB_read(evaptranssum_fp,0)
    evaptrans_arr = PFB_read(evaptrans_fp,0)
    
    #Fill arrays for each time output
    overlandsum_array[:,:,t]= overlandsum_arr[:,:,0]
    evaptranssum_array[:,:,t]=evaptranssum_arr[:,:,0]
    evaptrans_array[:,:,t]=evaptrans_arr[:,:,0]

 

""" Runoff OEVRLANDSUM in mm """

#~ 
overland_runoff = []
overland_runoff_tmp = overlandsum_array[0,0,:]
for i in range(len(overland_runoff_tmp)) : overland_runoff.append(overland_runoff_tmp[i]*1000/aire)


""" Variation du stock d'eau dans le sol in mm"""

mailles_array = np.zeros((nx,ny,nz,n_t))

var_stock = []

for t in range(n_t):
    for i in range(ny):
        for j in range(nz):
            vol_maille = hdr['dx']*hdr['dy']*(hdr['dz']*dz_mult_arr[0,i,j])*porosity_arr[0,i,j]*satur_array[0,i,j,t]
            #~ if press_array[0,i,j,t] > 0 :
            if press_array[0,i,j,t] > -100000 :
                mailles_array[0,i,j,t]=vol_maille + press_array[0,i,j,t]*specific_storage_arr[0,0,0]*(satur_array[0,i,j,t]*hdr['dx']*hdr['dy']*hdr['dz']*dz_mult_arr[0,i,j])
            else :
                mailles_array[0,i,j,t]=vol_maille

for t in range(n_t-1):    
        vol_t_avant = np.sum(mailles_array[:,:,:,t])*1000/aire
        vol_t_apres = np.sum(mailles_array[:,:,:,t+1])*1000/aire
        diff = vol_t_apres-vol_t_avant
        var_stock.append(diff)
    

#stock simple en mm
stock_simple = [0]
for i in range(n_t):
    
    stock_simple.append(np.sum(mailles_array[:,:,:,i]))
    #~ stock_simple.append(np.sum(mailles_array[:,:,:,i])*1000/aire)

#var_stock[-1]=0
#~ var_vol = pd.Series(var_stock, index = dates_less)


var_tot = np.sum(mailles_array[:,:,:,n_t-1])*1000/aire - np.sum(mailles_array[:,:,:,0])*1000/aire

var_cum = sum(var_stock)

#~ plt.figure()
#~ plt.plot(var_vol, 'r')
#~ plt.xlabel('Temps')
#~ plt.ylabel('Volume mm')
#~ plt.title('Volume d eau dans le sol en fonction du temps')
#~ 




""" Lecture fichier de forcages """
#os.chdir("../hillslope_clm")
os.chdir(RT_dir)

forcages = np.loadtxt("forcagePF.200707.30mn_updated.txt.0")
#os.chdir("../hillslope_clm")
os.chdir(cwd)

SW = forcages[0:n_t_clm*2,0]
LW = forcages[0:n_t_clm*2,1]
Pluie = forcages[0:n_t_clm*2,2]
Temp = forcages[0:n_t_clm*2,3]
Vent = forcages[0:n_t_clm*2,4]
Neige = forcages[0:n_t_clm*2,5]
Press =forcages[0:n_t_clm*2,6]
Hum = forcages[0:n_t_clm*2,7]






""" Pluie m/hr puis en mm"""
#mm/s
P_sec = 0.0025

#P = sum(Pluie)*900
P = sum(Pluie)*60*30


""" PLOT using panda series"""
# create time series index for PF & CLM:
ts_pf = pd.date_range('1/1/2007', periods=n_t-1)
ts_forcings = pd.date_range('1/1/2007 00:00', freq='30min', periods=n_t_clm*2)

# create time series with associated index:
ts_ov = pd.Series(overland_runoff, index=ts_pf)
ts_ov_cumsum = pd.Series(np.cumsum(overland_runoff), index =ts_pf)
ts_stock = pd.Series(np.cumsum(var_stock), index=ts_pf)

ts_rain_cumsum = pd.Series(np.cumsum(Pluie*60*30), index = ts_forcings)
ts_rain = pd.Series(Pluie*60*30, index = ts_forcings)

# create daily average
ts_rain_d = ts_rain.resample('D').sum()


#create dataframe object for easy plotting
pf_out_cum = pd.DataFrame({'Q':ts_ov,
                        'cumsum(Q)':ts_ov_cumsum,
                        'S':ts_stock})
forcings_cum = pd.DataFrame({'cumsum(P)':ts_rain_cumsum})
#~ pf_out = pd.DataFrame({'Q':ts_ov,
                        #~ 'S':ts_stock,
                        #~ 'ETRday':ts_evap_d})
#~ pf_out = pd.DataFrame({'Q':ts_ov,
                        #~ 'S':ts_stock,
                        #~ 'ETRday':ts_evap_d,
                        #~ 'evap_grndday':ts_evap_ground_d,
                        #~ 'evap_vegday':ts_evap_veg_d})                        

#~ pf_out = pd.DataFrame({'Q':ts_ov,
                        #~ 'ETRday':ts_evap_d,
                        #~ 'evap_grndday':ts_evap_ground_d,
                        #~ 'evap_vegday':ts_evap_veg_d})                        
pf_out = pd.DataFrame({'Q':ts_ov})                        
                                                                                    
                        

#~ forcings = pd.DataFrame({'P':ts_rain})
forcings = pd.DataFrame({'P':ts_rain_d})

Q_P = pd.DataFrame({'Q':ts_ov,'P':ts_rain_d})

#~ plt.figure()                    
#~ ax = pf_out_cum.plot()
#~ clm_out_cum.plot(ax = ax)
#~ forcings_cum.plot(ax = ax)
#~ forcings_cum.plot(ax = ax, kind = 'bar')



plt.figure(figsize=(20,6))
#~ ax2 = pf_out.plot()
#~ plt.plot([Q_P.index.values[100], Q_P.index.values[100],Q_P.index.values[100]],[0,30,70])
#~ plt.plot(Q_P.index.values[100:101],[0,70])
plt.ylim(0,70)
#~ 
ax2 = Q_P.Q.plot()

ax2.annotate("",xy=(Q_P.index[100], Q_P['Q'][100]), xycoords='data',
  xytext=(Q_P.index[100],40), textcoords='data',
  arrowprops=dict(arrowstyle="->",
  connectionstyle="arc3"),
            )
#~ Q_P.Q.plot()
plt.ylabel('mm/day', fontsize=16)
plt.show()

#~ clm_out.plot(ax = ax2)
#~ forcings.plot(ax = ax2)
#~ ax2=forcings.plot(secondary_y=True, kind = 'bar',ax = ax2)
#~ ax2=Q_P.P.plot(secondary_y=True, kind = 'bar',ax = ax2)
#~ fig = ax2.get_figure()
#~ fig.axes[1].invert_yaxis()
#~ plt.rcParams["figure.figsize"] = (20,6)
#~ plt.ylabel('mm/day', fontsize=16)
#~ plt.savefig('./streamflow3.png',format='png')
#~ plt.savefig('../../hillslope2017/budget.png',format='png')

#~ plt.draw_if_interactive()
#~ plt.savefig('./streamflow.png',format='png')

#~ plt.plot(evap_tot)
#~ plt.show()


fig, (ax1, ax2) = plt.subplots(1, 2,figsize=(20,5))

y = [dy*y+dy/2 for y in range(ny)]
z = [0.01,0.02,0.03,0.04,0.09,0.18,0.27,0.36,0.45,0.58,0.74,0.9,1.06,1.22,1.42,1.66,1.9,2.14,2.38,4.65,8.95,13.25,17.55,21.58]
#~ fig,ax = plt.subplots(figsize=(20,5))

#~ ax.pcolormesh(np.array(y),np.array(z),satur_array[0,:,:,0].T,cmap=cm.jet_r, shading = 'gouraud')
ax1.pcolormesh(np.array(y),np.array(z),satur_array[0,::-1,:,183].T,cmap=cm.jet_r, shading = 'gouraud')
#~ ax.pcolormesh(np.array(y),np.array(z),satur_array[0,:,:,365].T,cmap=cm.jet_r, shading = 'gouraud')

#~ fig.gca().invert_yaxis()
fig.axes[0].invert_yaxis()
ax1.margins(x=0)
ax1.margins(y=0)
#~ ax.set_yscale('log')
leg = plt.cm.ScalarMappable(cmap=cm.jet_r)
leg.set_array([0.,1.])
fig.colorbar(leg, ax = ax1)

ax2 = Q_P.Q.plot()

ax2.annotate("",xy=(Q_P.index[100], Q_P['Q'][100]), xycoords='data',
  xytext=(Q_P.index[100],65), textcoords='data',
  arrowprops=dict(arrowstyle="->",
  connectionstyle="arc3"),
            )
#~ Q_P.Q.plot()
plt.ylabel('mm/day', fontsize=16)
plt.ylim(0,70)

plt.show()

########try to animate:

#plot first plot:

y = [dy*y+dy/2 for y in range(ny)]
z = [0.01,0.02,0.03,0.04,0.09,0.18,0.27,0.36,0.45,0.58,0.74,0.9,1.06,1.22,1.42,1.66,1.9,2.14,2.38,4.65,8.95,13.25,17.55,21.58]

fig, (ax1, ax2) = plt.subplots(1, 2,figsize=(20,5))
ax1.pcolormesh(np.array(y),np.array(z),satur_array[0,:,:,0].T,cmap=cm.jet_r, shading = 'gouraud')
fig.axes[0].invert_yaxis()
ax1.margins(x=0)
ax1.margins(y=0)
#~ ax.set_yscale('log')
leg = plt.cm.ScalarMappable(cmap=cm.jet_r)
leg.set_array([0.,1.])
fig.colorbar(leg, ax = ax1)

ax2 = Q_P.Q.plot()
ann = ax2.annotate("",xy=(Q_P.index[0], Q_P['Q'][0]), xycoords='data',
    xytext=(Q_P.index[0],65), textcoords='data',
    arrowprops=dict(arrowstyle="->",
    connectionstyle="arc3"))
ann_list[0:2]=[ann,ann]
plt.ylabel('mm/day', fontsize=16)
plt.ylim(0,70)

# define animation:
def update(i):
    label = 'timestep {0}'.format(i)
    print(label)
    # Update the line and the axes (with a new xlabel). Return a tuple of
    # "artists" that have to be redrawn for this frame.
    ann_list[0].remove()
    ax1.pcolormesh(np.array(y),np.array(z),satur_array[0,::-1,:,i].T,cmap=cm.jet_r, shading = 'gouraud')
    ann = ax2.annotate("",xy=(Q_P.index[i], Q_P['Q'][i]), xycoords='data',
        xytext=(Q_P.index[i],65), textcoords='data',
        arrowprops=dict(arrowstyle="->",
        connectionstyle="arc3"))
    ann_list[0:2]=[ann,ann]
    return (ax1,ax2)


#animate
anim = FuncAnimation(fig, update, frames=np.arange(0, 360, 10), interval=20, blit=True)
plt.show()
