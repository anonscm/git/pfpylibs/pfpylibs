"""
- Read overland sum pfb output file
- create hydrograph for selected cell
Created on Mon Sep 29 11:47:42 2014

@author: jgilbert
modified by savannah
"""

def PFB_read(pfb_file_path, flag):
    import struct
    import numpy
    #print("**This code DOES handle subgrids**")
    
    hdr_dict = {}
    
    with open(pfb_file_path,'rb') as pfbf:
        hdr_dict['len_x'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['len_y'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['len_z'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['nx'] = struct.unpack('>i', pfbf.read(4))[0]
        hdr_dict['ny'] = struct.unpack('>i', pfbf.read(4))[0]
        hdr_dict['nz'] = struct.unpack('>i', pfbf.read(4))[0]
        hdr_dict['dx'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['dy'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['dz'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['ns'] = struct.unpack('>i', pfbf.read(4))[0]
        ns = hdr_dict['ns']
        #print(ns)
        nx = hdr_dict['nx']
        ny = hdr_dict['ny']
        nz = hdr_dict['nz']
        
        if nx == 0:
            nx = int(raw_input("What should the NX be?"))
            hdr_dict['nx'] = nx
        if ny == 0:
            ny = int(raw_input("What should the NY be?"))
            hdr_dict['ny'] = ny
        if nz ==0:
            nz =int(raw_input("What should the NZ be?"))
            hdr_dict['nz'] = nz
        
        len_x = hdr_dict['len_x']
        #print "len_x, len_y, len_z, nx ny nz --> %f %f %f %i %i %i" %(len_x, len_x, len_x, nx, ny, nz)
        
        pfb_array = numpy.zeros((nx, ny, nz))
        
        for subgrids in range(ns):
            hdr_dict['ix'] = struct.unpack('>i', pfbf.read(4))[0]
            ix = hdr_dict['ix']
            hdr_dict['iy'] = struct.unpack('>i', pfbf.read(4))[0]
            iy = hdr_dict['iy']
            #print(iy)
            hdr_dict['iz'] = struct.unpack('>i', pfbf.read(4))[0]
            iz = hdr_dict['iz']
            hdr_dict['nx2'] = struct.unpack('>i', pfbf.read(4))[0]
            nx2 = hdr_dict['nx2']
            hdr_dict['ny2'] = struct.unpack('>i', pfbf.read(4))[0]
            ny2 = hdr_dict['ny2']
            #print(ny2)
            hdr_dict['nz2'] = struct.unpack('>i', pfbf.read(4))[0]
            nz2 = hdr_dict['nz2']
            hdr_dict['rx'] = struct.unpack('>i', pfbf.read(4))[0]
            rx = hdr_dict['rx']
            hdr_dict['ry'] = struct.unpack('>i', pfbf.read(4))[0]
            ry = hdr_dict['ry']
            hdr_dict['rz'] = struct.unpack('>i', pfbf.read(4))[0]
            rz = hdr_dict['rz']
            #print("ix iy iz nx2 ny2 nz2 rx ry rz --> %i %i %i %i %i %i %i %i %i\n" %(ix,iy, iz, nx2, ny2, nz2, rx,ry, rz))
            for lays in range(nz2):
                for rows in range(ny2):
                    for cols in range(nx2):
                        val = struct.unpack('>d', pfbf.read(8))[0]
                        #q = raw_input(val)
                        pfb_array[(cols+ix), (rows + iy), (nz2-(lays + iz)-1)] = val
                        #pfb_array[(cols+ix-1), (nx2-(iy)-1), (nz2-(lays + iz)-1)] = val
                        #pfb_array[(rows + iy-1), (cols+ix-1), (nz2-(lays + iz)-1)] = val
                        #print val
                        
    if flag ==1:
        return(pfb_array, hdr_dict)
    else:
        return(pfb_array)

###############################################################################
## MAIN SECTION : Select Files (pfb) and Select what you want done with them ##
###############################################################################

import numpy as np
import os.path
import time
import matplotlib.pyplot as plt
import pylab
import matplotlib.image as mpimg
import plotly.plotly as py
import math
import os
import pandas as pd
import matplotlib.cm as cm

# Print the full arrays and tupples
np.set_printoptions(threshold=np.nan)

# Set number of time steps from output to use
#~ n_t = 366
n_t = 731

dump_int = 24

#~ n_t_clm = 8760
n_t_clm = 17520
dump_int_clm = 1



"""converter"""
#~ conv = 15
#~ conv_clm = 900
conv = 1
# from mm/s to mm/hr
conv_clm = 1 * 60 * 60


# Domaine
#m2
aire = 5000

cwd = os.getcwd()
# Directory containing the pfb results of the PF simulation

#~ RT_dir = r'../../hillslope2017'
#~ RT_dir = r'../../hillslope2017/SAINTSULPICE'
#~ RT_dir = r'../../hillslope2017/OUINE'
RT_dir = r'./'

#~ base_dir = '/'.join([RT_dir,'results'])
#~ base_dir = '/'.join([RT_dir,'results_perm'])
#~ base_dir = '/'.join([RT_dir,'results5'])
base_dir = '/'.join([RT_dir,'results7'])

print("Found base directory...")

# Defining the file names of the outputs
#~ runnam = r'hillslope_sens'
runnam = r'hillslope'

prop_base = os.path.join(base_dir, '.'.join([runnam, "out"]))

# Use out_type and out_base to chose the property
out_type = r'overlandsum'
out_base = os.path.join(base_dir, '.'.join([runnam,"out",out_type]))

# Get format of property arrays with the geometry : overland flow and pressure
if os.path.isfile('.'.join([prop_base,"press",format(0,"05.0F"),"pfb"])):
    
    [test_array, hdr] =  PFB_read('.'.join([prop_base,"press",format(0,"05.0F"),"pfb"]),1)      
    
    nz = hdr['nz']
    nx = hdr['nx']
    ny = hdr['ny']
    dx = hdr['dx']
    dy = hdr['dy']
# +1 car 00000.pfb existe
    press_array = np.zeros((nx,ny,nz,n_t))
    satur_array = np.zeros((nx,ny,nz,n_t))   
    
    overlandsum_array = np.zeros((nx,ny,n_t-1))
    evaptranssum_array = np.zeros((nx,ny,n_t-1))
    evaptrans_array = np.zeros((nx,ny,n_t-1)) 
    
# 36 nombre de sorties clm    
    clm_output_array = np.zeros((nx,ny,36,n_t_clm))

    tmp_arr = np.zeros((nx,ny,nz))
#constant values
    if os.path.isfile('.'.join([prop_base,"dz_mult.pfb"])):
        dz_mult_fp = '.'.join([prop_base,"dz_mult.pfb"])
        dz_mult_arr = PFB_read(dz_mult_fp,0)
    else:
        dz_mult_arr = tmp_arr * 1
            
    if os.path.isfile('.'.join([prop_base,"mannings.pfb"])):    
        mannings_fp = '.'.join([prop_base,"mannings.pfb"])
        mannings_arr = PFB_read(mannings_fp,0)
    else:
        mannings_arr = tmp_arr + 5.5e-6
    
    mask_fp = '.'.join([prop_base,"mask.pfb"])
    mask_arr = PFB_read(mask_fp,0)
    
    
    #~ perm_x_fp = '.'.join([prop_base,"perm_x.pfb"])
    #~ perm_x_arr = PFB_read(perm_x_fp,0)
    #~ 
    #~ perm_y_fp = '.'.join([prop_base,"perm_y.pfb"])
    #~ perm_y_arr = PFB_read(perm_y_fp,0)
#~ 
    #~ perm_z_fp = '.'.join([prop_base,"perm_z.pfb"])
    #~ perm_z_arr = PFB_read(perm_z_fp,0)

    porosity_fp = '.'.join([prop_base,"porosity.pfb"])
    porosity_arr = PFB_read(porosity_fp,0)

    slope_x_fp = '.'.join([prop_base,"slope_x.pfb"])
    slope_x_arr = PFB_read(slope_x_fp,0)
    
    slope_y_fp = '.'.join([prop_base,"slope_y.pfb"])
    slope_y_arr = PFB_read(slope_y_fp,0)
    
    specific_storage_fp = '.'.join([prop_base,"specific_storage.pfb"])
    specific_storage_arr = PFB_read(specific_storage_fp,0)    
    

# Fill in property arrays with space and time dependant values
for t in range(n_t):

    # Get all time file directories
    press_fp = '.'.join([prop_base,"press",format(t*dump_int,"05.0F"),"pfb"])
    satur_fp = '.'.join([prop_base,"satur",format(t*dump_int,"05.0F"),"pfb"])

    # Read pfb
    press_arr = PFB_read(press_fp,0)
    satur_arr = PFB_read(satur_fp,0)
   
    #Fill arrays for each time output
    press_array[:,:,:,t]=press_arr[:,:,:]
    satur_array[:,:,:,t]=satur_arr[:,:,:]


for t in range(n_t-1):
    # Get all time file directories
    overlandsum_fp = '.'.join([prop_base,"overlandsum",format((t+1)*dump_int,"05.0F"),"pfb"])
    evaptranssum_fp = '.'.join([prop_base, "evaptranssum", format((t+1)*dump_int,"05.0F"),"pfb"])
    evaptrans_fp = '.'.join([prop_base, "evaptrans", format((t+1)*dump_int,"05.0F"),"pfb"])

    # Read pfb
    overlandsum_arr = PFB_read(overlandsum_fp,0)
    evaptranssum_arr = PFB_read(evaptranssum_fp,0)
    evaptrans_arr = PFB_read(evaptrans_fp,0)
    
    #Fill arrays for each time output
    overlandsum_array[:,:,t]= overlandsum_arr[:,:,0]
    evaptranssum_array[:,:,t]=evaptranssum_arr[:,:,0]
    evaptrans_array[:,:,t]=evaptrans_arr[:,:,0]

 

 
for t in range(n_t_clm):
    
    # Get all time file directories
    clm_output_fp = '.'.join([prop_base,"clm_output",format((t+1)*dump_int_clm,"05.0F"),"C.pfb"])

    # Read pfb
    clm_output_arr = PFB_read(clm_output_fp,0)

    #Fill arrays for each time output
    clm_output_array[:,:,:,t]= clm_output_arr[:,:,:]


""" Runoff première maille en fonction du temps MANNINGS in mm"""

S = slope_y_arr[0,0,0]
mannings_runoff = []
m = mannings_arr[0,0,0]

for i in range(n_t):    
    if press_array[0,0,0,i] >= 0 :
        mannings_runoff.append((dx/m)*(S**0.5)*(press_array[0,0,0,i])**(5./3.)*(1000/aire)*conv*dump_int)
    else :
        mannings_runoff.append(0.)

temps_hr = []
for i in range(n_t):

    val = i,
    temps_hr.append(val)

""" Runoff OEVRLANDSUM in mm """

#~ 
overland_runoff = []
overland_runoff_tmp = overlandsum_array[0,0,:]
for i in range(len(overland_runoff_tmp)) : overland_runoff.append(overland_runoff_tmp[i]*1000/aire)


""" Variation du stock d'eau dans le sol in mm"""

mailles_array = np.zeros((nx,ny,nz,n_t))

var_stock = []

for t in range(n_t):
    for i in range(ny):
        for j in range(nz):
            vol_maille = hdr['dx']*hdr['dy']*(hdr['dz']*dz_mult_arr[0,i,j])*porosity_arr[0,i,j]*satur_array[0,i,j,t]
            #~ if press_array[0,i,j,t] > 0 :
            if press_array[0,i,j,t] > -100000 :
                mailles_array[0,i,j,t]=vol_maille + press_array[0,i,j,t]*specific_storage_arr[0,0,0]*(satur_array[0,i,j,t]*hdr['dx']*hdr['dy']*hdr['dz']*dz_mult_arr[0,i,j])
            else :
                mailles_array[0,i,j,t]=vol_maille

for t in range(n_t-1):    
        vol_t_avant = np.sum(mailles_array[:,:,:,t])*1000/aire
        vol_t_apres = np.sum(mailles_array[:,:,:,t+1])*1000/aire
        diff = vol_t_apres-vol_t_avant
        var_stock.append(diff)
    

#stock simple en mm
stock_simple = [0]
for i in range(n_t):
    
    stock_simple.append(np.sum(mailles_array[:,:,:,i]))
    #~ stock_simple.append(np.sum(mailles_array[:,:,:,i])*1000/aire)

#var_stock[-1]=0
#~ var_vol = pd.Series(var_stock, index = dates_less)


var_tot = np.sum(mailles_array[:,:,:,n_t-1])*1000/aire - np.sum(mailles_array[:,:,:,0])*1000/aire

var_cum = sum(var_stock)

#~ plt.figure()
#~ plt.plot(var_vol, 'r')
#~ plt.xlabel('Temps')
#~ plt.ylabel('Volume mm')
#~ plt.title('Volume d eau dans le sol en fonction du temps')
#~ 


""" CLM outputs """
# latent heat W/m2
lh_tot = []
for i in range(n_t_clm) :
    lh_tot.append(clm_output_array[0,:,35,i].mean())

#outgoing lw radiation W/m2
lwrad_out = []
for i in range(n_t_clm) :
    lwrad_out.append(clm_output_array[0,:,34,i].mean())

# sensible heat W/m2
sh_tot = []
for i in range(n_t_clm) :
    sh_tot.append(clm_output_array[0,:,33,i].mean())

# ground heat flux W/m2
soil_grnd = []
for i in range(n_t_clm) :
    soil_grnd.append(clm_output_array[0,:,32,i].mean())

#total evapotranspiration mm/s
evap_tot=[]
for i in range(n_t_clm) :
    evap_tot.append(clm_output_array[0,:,31,i].mean()*conv_clm)

#ground evaporation without condesation mm/s
evap_ground = []
for i in range(n_t_clm) :
    evap_ground.append(clm_output_array[0,:,30,i].mean()*conv_clm)

#soil evaporation mm/s
evap_soil = []
for i in range(n_t_clm) :
    evap_soil.append(clm_output_array[0,:,29,i].mean()*conv_clm)

#vegetation evaporation + transpiration mm/s
evap_veg = []
for i in range(n_t_clm) :
    evap_veg.append(clm_output_array[0,:,28,i].mean()*conv_clm)

# vegeation transpiration mm/s
tran_veg = []
for i in range(n_t_clm) :
    tran_veg.append(clm_output_array[0,:,27,i].mean()*conv_clm)

# soil infiltration mm/s
infl = []
for i in range(n_t_clm) :
    infl.append(clm_output_array[0,:,26,i].mean()*conv_clm)




""" Lecture fichier de forcages """
#os.chdir("../hillslope_clm")
os.chdir(RT_dir)

forcages = np.loadtxt("forcagePF.200707.30mn_updated.txt.0")
#os.chdir("../hillslope_clm")
os.chdir(cwd)

SW = forcages[0:n_t_clm*2,0]
LW = forcages[0:n_t_clm*2,1]
Pluie = forcages[0:n_t_clm*2,2]
Temp = forcages[0:n_t_clm*2,3]
Vent = forcages[0:n_t_clm*2,4]
Neige = forcages[0:n_t_clm*2,5]
Press =forcages[0:n_t_clm*2,6]
Hum = forcages[0:n_t_clm*2,7]



WB = []

for i in range(n_t-1):
    
    #WB.append(Pluie[i]*900-evap_tot[i]-var_stock[i]-overland_runoff[i])
    WB.append(Pluie[i]*60*30-evap_tot[i]-var_stock[i]-overland_runoff[i])

WB_cum = np.cumsum(WB)


""" Pluie m/hr puis en mm"""
#mm/s
P_sec = 0.0025

#P = sum(Pluie)*900
P = sum(Pluie)*60*30


""" total """

R = sum(overland_runoff)
V = sum(var_stock)
E = sum(evap_tot)

print('\n The total rain is {} mm \n'.format(P)
)
print('The total overland runoff is {} mm \n'.format(sum(overland_runoff)))
print('The total mannings runoff is {} mm \n'.format(sum(mannings_runoff)))
print('The total evapotranspiration is {} mm \n'.format(sum(evap_tot)))
print('The total amount of water in the soil is {} mm \n'.format(var_tot))

#print('The total amount of water in the soil is {} mm (cumulative)\n'.format(var_cum))

#~ Sum = sum(mannings_runoff)+sum(evaptrans_series)+ var_tot


Sum_parflow = sum(overland_runoff)+ var_cum +sum(evap_tot)

print('Runoff coefficient  {} \n'.format(R/P))

print('TOTAL  Runoff, infl, evap water is {} mm \n'.format(Sum_parflow))

print('ERROR PARFLOW P-E-V-R : {} mm \n'.format(P-Sum_parflow))


#~ 
#~ print('\n RAIN {} mm \n'.format(P_tot)
#~ )
#~ print('OVERLAND RUNOFF {} mm \n'.format(sum(overland_runoff)))
#~ print('MANNINGS RUNOFF {} mm \n'.format(sum(mannings_runoff)))
#~ print('ETP PARFLOW mm \n'.format(sum(evaptrans_liste)))
#~ print('VAR STOCK {} mm \n'.format(var_tot))
#~ 
#~ 
#~ Sum_clm = sum(infl)+sum(evap_tot) 
#~ Sum_parflow = sum(overland_runoff)+ var_tot +sum(evap_tot)
#~ 
#~ 
#~ print('CLM TOTAL water is {} mm \n'.format(Sum_clm))
#~ print('PARFLOW and ~CLM~ TOTAL water is {} mm \n'.format(Sum_parflow))
#~ 
#~ 
#~ print('ERROR CLM {} mm \n'.format(P_tot-Sum_clm))
#~ print('ERROR PARFLOW {} mm \n'.format(P_tot-Sum_parflow))

""" PLOT using panda series"""
# create time series index for PF & CLM:
ts_pf = pd.date_range('1/1/2007', periods=n_t-1)
ts_clm = pd.date_range('1/1/2007 00:00', freq='1H', periods=n_t_clm)
ts_forcings = pd.date_range('1/1/2007 00:00', freq='30min', periods=n_t_clm*2)

# create time series with associated index:
ts_ov = pd.Series(overland_runoff, index=ts_pf)
ts_ov_cumsum = pd.Series(np.cumsum(overland_runoff), index =ts_pf)
ts_stock = pd.Series(np.cumsum(var_stock), index=ts_pf)
ts_evap_cumsum = pd.Series(np.cumsum(evap_tot), index=ts_clm)
ts_evap = pd.Series(evap_tot, index=ts_clm)
ts_evap_ground = pd.Series(evap_ground, index=ts_clm)
ts_evap_veg = pd.Series(evap_veg, index=ts_clm)

ts_rain_cumsum = pd.Series(np.cumsum(Pluie*60*30), index = ts_forcings)
ts_rain = pd.Series(Pluie*60*30, index = ts_forcings)

# create daily average
ts_evap_d = ts_evap.resample('D').sum()
ts_evap_ground_d = ts_evap_ground.resample('D').sum()
ts_evap_veg_d = ts_evap_veg.resample('D').sum()


#create dataframe object for easy plotting
pf_out_cum = pd.DataFrame({'Q':ts_ov,
                        'cumsum(Q)':ts_ov_cumsum,
                        'S':ts_stock})
clm_out_cum = pd.DataFrame({'cumsum(ETR)':ts_evap_cumsum})
forcings_cum = pd.DataFrame({'cumsum(P)':ts_rain_cumsum})
pf_out = pd.DataFrame({'Q':ts_ov,
                        'S':ts_stock,
                        'ETRday':ts_evap_d})
#~ pf_out = pd.DataFrame({'Q':ts_ov,
                        #~ 'S':ts_stock,
                        #~ 'ETRday':ts_evap_d,
                        #~ 'evap_grndday':ts_evap_ground_d,
                        #~ 'evap_vegday':ts_evap_veg_d})                        

pf_out = pd.DataFrame({'Q':ts_ov,
                        'ETRday':ts_evap_d,
                        'evap_grndday':ts_evap_ground_d,
                        'evap_vegday':ts_evap_veg_d})                        
                                          
                        
clm_out = pd.DataFrame({'ETR':ts_evap})

#~ clm_out = pd.DataFrame({'ETR':ts_evap,
                        #~ 'evap_grnd':ts_evap_ground,
                        #~ 'evap_veg':ts_evap_veg,
                        #~ 'tran_veg':ts_tran_veg})

forcings = pd.DataFrame({'P':ts_rain})

#~ plt.figure()                    
ax = pf_out_cum.plot()
clm_out_cum.plot(ax = ax)
forcings_cum.plot(ax = ax)


#~ plt.figure(figsize=(20,6))

ax2 = pf_out.plot()
clm_out.plot(ax = ax2)
forcings.plot(ax = ax2)
plt.rcParams["figure.figsize"] = (20,6)
plt.ylabel('mm/day', fontsize=16)
plt.savefig('../../hillslope2017/budget.png',format='png')

plt.show()

#~ plt.plot(evap_tot)
#~ plt.show()

"""plot T/E """

y = [dy*y+dy/2 for y in range(ny)]

#ground evaporation without condesation mm/s
evap_ground_yt = np.zeros((ny,n_t_clm))
evap_ground_yt = clm_output_array[0,:,30,:]*conv_clm

# vegeation transpiration mm/s
tran_veg_yt = np.zeros((ny,n_t_clm))
tran_veg_yt = clm_output_array[0,:,27,:]*conv_clm

evap_veg_yt = np.zeros((ny,n_t_clm))
evap_veg_yt = clm_output_array[0,:,28,:]*conv_clm

T_E = tran_veg_yt / evap_ground_yt

#~ np.reshape(evap_ground_yt
evap_ground_daily_yt = evap_ground_yt.reshape(ny,730,24).mean(axis=2)
tran_veg_daily_yt = tran_veg_yt.reshape(ny,730,24).mean(axis=2)

T_E_daily = tran_veg_daily_yt / evap_ground_daily_yt

ts_clm_daily = pd.date_range('1/1/2007', periods=n_t-1)

fig,ax = plt.subplots(figsize=(20,6))
#~ ax.pcolormesh(ts_clm, np.array(y),evap_ground_yt,cmap=cm.jet_r, shading = 'gouraud')
#~ ax.pcolormesh(ts_clm_daily, np.array(y),evap_ground_daily_yt,cmap=cm.jet_r, shading = 'gouraud')
ax.pcolormesh(ts_clm_daily, np.array(y),T_E_daily,cmap=cm.jet_r, shading = 'gouraud')

plt.ylabel('Y (m)', fontsize=16)
leg = plt.cm.ScalarMappable(cmap=cm.jet_r)
leg.set_array([np.min(T_E_daily),np.max(T_E_daily)])
fig.colorbar(leg)
plt.savefig('../../hillslope2017/T_E.png',format='png')

plt.show()

### Plot4

#~ for i in range(len(ts_clm_daily)):

for i in range(30):
    
    plt.plot(y,T_E_daily[:,i+365])

plt.ylabel('T / E', fontsize=16)
plt.xlabel('Y (m)', fontsize=16)

#~ plt.savefig('../../hillslope2017/T_E_DAYbyDAY.png',format='png')

plt.show()    

#~ z = [0.01,0.02,0.03,0.04,0.09,0.18,0.27,0.36,0.45,0.58,0.74,0.9,1.06,1.22,1.42,1.66,1.9,2.14,2.38,4.65,8.95,13.25,17.55,21.58]
#~ fig,ax = plt.subplots(figsize=(20,5))
#~ 
#~ #ax.pcolormesh(np.array(y),np.array(z),satur_array[0,:,:,183].T,cmap=cm.jet_r, shading = 'gouraud')
#~ ax.pcolormesh(np.array(y),np.array(z),satur_array[0,:,:,365].T,cmap=cm.jet_r, shading = 'gouraud')
#~ 
#~ fig.gca().invert_yaxis()
#~ ax.margins(x=0)
#~ ax.margins(y=0)
#~ ax.set_yscale('log')
#~ leg = plt.cm.ScalarMappable(cmap=cm.jet_r)
#~ leg.set_array([0.,1.])
#~ fig.colorbar(leg)
#plt.show()
