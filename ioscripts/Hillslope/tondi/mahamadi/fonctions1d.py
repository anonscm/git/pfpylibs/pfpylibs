
from glob import glob
import numpy as np
import os,glob,struct
import xarray as xr
import pandas as pd
import datetime as dt
#import hydrostats as hs
#import hydrostats.data as hd
#import hydrostats.visual as hv
from matplotlib import pyplot as plt
import HydroErr as he
import math
import scipy.io as sp
#import logging
#import PFlibs 
from PFlibs import PFoutputs as PFout
from netCDF4 import Dataset
from PFlibs import VGM
from matplotlib.lines import Line2D



def PFB_read(pfb_file_path, flag):

    #print("**This code DOES handle subgrids**")
    # flag = 1: return array + header
    # flag = 0: return array only
    
    hdr_dict = {}
    
    with open(pfb_file_path,'rb') as pfbf:
        hdr_dict['X0'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['Y0'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['Z0'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['nx'] = struct.unpack('>i', pfbf.read(4))[0]
        hdr_dict['ny'] = struct.unpack('>i', pfbf.read(4))[0]
        hdr_dict['nz'] = struct.unpack('>i', pfbf.read(4))[0]
        hdr_dict['dx'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['dy'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['dz'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['ns'] = struct.unpack('>i', pfbf.read(4))[0]
        ns = hdr_dict['ns']
        #print(ns)
        nx = hdr_dict['nx']
        ny = hdr_dict['ny']
        nz = hdr_dict['nz']
        
        if nx == 0:
            nx = int(raw_input("What should the NX be?"))
            hdr_dict['nx'] = nx
        if ny == 0:
            ny = int(raw_input("What should the NY be?"))
            hdr_dict['ny'] = ny
        if nz ==0:
            nz =int(raw_input("What should the NZ be?"))
            hdr_dict['nz'] = nz
        
        X0 = hdr_dict['X0']
        #print "len_x, len_y, len_z, nx ny nz --> %f %f %f %i %i %i" %(len_x, len_x, len_x, nx, ny, nz)
        
        pfb_array = np.zeros((nx, ny, nz))
        
        for subgrids in range(ns):
            hdr_dict['ix'] = struct.unpack('>i', pfbf.read(4))[0]
            ix = hdr_dict['ix']
            hdr_dict['iy'] = struct.unpack('>i', pfbf.read(4))[0]
            iy = hdr_dict['iy']
            #print(iy)
            hdr_dict['iz'] = struct.unpack('>i', pfbf.read(4))[0]
            iz = hdr_dict['iz']
            hdr_dict['nx2'] = struct.unpack('>i', pfbf.read(4))[0]
            nx2 = hdr_dict['nx2']
            hdr_dict['ny2'] = struct.unpack('>i', pfbf.read(4))[0]
            ny2 = hdr_dict['ny2']
            #print(ny2)
            hdr_dict['nz2'] = struct.unpack('>i', pfbf.read(4))[0]
            nz2 = hdr_dict['nz2']
            hdr_dict['rx'] = struct.unpack('>i', pfbf.read(4))[0]
            rx = hdr_dict['rx']
            hdr_dict['ry'] = struct.unpack('>i', pfbf.read(4))[0]
            ry = hdr_dict['ry']
            hdr_dict['rz'] = struct.unpack('>i', pfbf.read(4))[0]
            rz = hdr_dict['rz']
            #print "ix iy iz nx2 ny2 nz2 rx ry rz --> %i %i %i %i %i %i %i %i %i\n" %(ix,iy, iz, nx2, ny2, nz2, rx,ry, rz)
            for lays in range(nz2):
                for rows in range(ny2):
                    for cols in range(nx2):
                        val = struct.unpack('>d', pfbf.read(8))[0]
                        #q = raw_input(val)
                        pfb_array[(cols+ix), (rows + iy), (nz2-(lays + iz)-1)] = val
                        #pfb_array[(cols+ix-1), (nx2-(iy)-1), (nz2-(lays + iz)-1)] = val
                        #pfb_array[(rows + iy-1), (cols+ix-1), (nz2-(lays + iz)-1)] = val
                        #print val
                        
    if flag ==1:
        return(pfb_array, hdr_dict)
    else:
        return(pfb_array)

    
    

def etr_mil(dossier,filename):

        """identify files"""
        files = glob.glob(os.path.join(dossier,'*clm_output*.pfb'))
        files.sort()
        """reads in the first file to have the dimension & initialize"""
        d,h=PFB_read(files[0],1)
        evaptot_array = np.zeros([h['ny'],len(files)])
        evapveg_array = np.zeros([h['ny'],len(files)])
        evapsoil_array = np.zeros([h['ny'],len(files)])
        trans_array = np.zeros([h['ny'],len(files)])
        infl_array = np.zeros([h['ny'],len(files)])
        
        
        #print(d.shape)
        """reads in all files"""
        for t,f in enumerate(files):
            d,h=PFB_read(f,1)
            evaptot_array[:,t] = d[0,:,-5]*60*60
            evapveg_array[:,t] = d[0,:,-8]*60*60
            evapsoil_array[:,t] = d[0,:,-7]*60*60
            trans_array[:,t] = d[0,:,-9]*60*60
            infl_array[:,t] = d[0,:,-10]*60*60


        ds_etr = xr.Dataset({"evaptot": (("time","y"), np.swapaxes(evaptot_array,1,0)),
                         "evapveg": (("time","y"), np.swapaxes(evapveg_array,1,0)),
                         "evapsoil": (("time","y"), np.swapaxes(evapsoil_array,1,0)),
                         "trans": (("time","y"), np.swapaxes(trans_array,1,0)),
                        "infl": (("time","y"), np.swapaxes(infl_array,1,0))},
                            
            coords={
                "time": pd.date_range("2006-01-01", periods=len(files),freq='1H'),
                "reference_time": pd.Timestamp("2006-01-01")})

        ds_etr.evaptot.attrs['units']='mm/h'
        ds_etr.evapveg.attrs['units']='mm/h'
        ds_etr.evapsoil.attrs['units']='mm/h'
        ds_etr.trans.attrs['units']='mm/h'
        ds_etr.infl.attrs['units']='mm/h'
        
        #if os.path.exists(os.path.join(outputdir,fichier))==True :
         #   ds_etr.to_netcdf(os.path.join(outputdir,fichier),format="NETCDF4", mode='a')
            
        fichier = os.path.join(outputdir_mil,filename)
        if os.path.exists(fichier):
            os.remove(fichier)
        ds_etr.to_netcdf(fichier,format="NETCDF4") 
        
        return xr.open_dataset(fichier)
    


def etr_mil1(dossier,filename):

        """identify files"""
        files = glob.glob(os.path.join(dossier,'*clm_output*.pfb'))
        files.sort()
        """reads in the first file to have the dimension & initialize"""
        d,h=PFB_read(files[0],1)
        evaptot_array = np.zeros([h['ny'],len(files)])
        evapveg_array = np.zeros([h['ny'],len(files)])
        evapsoil_array = np.zeros([h['ny'],len(files)])
        trans_array = np.zeros([h['ny'],len(files)])
        infl_array = np.zeros([h['ny'],len(files)])
        
        
        #print(d.shape)
        """reads in all files"""
        for t,f in enumerate(files):
            d,h=PFB_read(f,1)
            evaptot_array[:,t] = d[0,:,-5]*60*60
            evapveg_array[:,t] = d[0,:,-8]*60*60
            evapsoil_array[:,t] = d[0,:,-7]*60*60
            trans_array[:,t] = d[0,:,-9]*60*60
            infl_array[:,t] = d[0,:,-10]*60*60


        ds_etr = xr.Dataset({"evaptot": (("time","y"), np.swapaxes(evaptot_array,1,0)),
                         "evapveg": (("time","y"), np.swapaxes(evapveg_array,1,0)),
                         "evapsoil": (("time","y"), np.swapaxes(evapsoil_array,1,0)),
                         "trans": (("time","y"), np.swapaxes(trans_array,1,0)),
                        "infl": (("time","y"), np.swapaxes(infl_array,1,0))},
                            
            coords={
                "time": pd.date_range("2006-01-01", periods=len(files),freq='1H'),
                "reference_time": pd.Timestamp("2006-01-01")})

        ds_etr.evaptot.attrs['units']='mm/h'
        ds_etr.evapveg.attrs['units']='mm/h'
        ds_etr.evapsoil.attrs['units']='mm/h'
        ds_etr.trans.attrs['units']='mm/h'
        ds_etr.infl.attrs['units']='mm/h'
        
        #if os.path.exists(os.path.join(outputdir,fichier))==True :
         #   ds_etr.to_netcdf(os.path.join(outputdir,fichier),format="NETCDF4", mode='a')
            
        #fichier = os.path.join(outputdir_mil,filename)
        #if os.path.exists(fichier):
            #os.remove(fichier)
        #ds_etr.to_netcdf(fichier,format="NETCDF4") 
        
        return ds_etr
    

def etr_mil2(dossier,filename):

        """identify files"""
        files = glob.glob(os.path.join(dossier,'*clm_output*.pfb'))
        files.sort()
        """reads in the first file to have the dimension & initialize"""
        d,h=PFB_read(files[0],1)
        evaptot_array = np.zeros([h['ny'],len(files)])
        evapveg_array = np.zeros([h['ny'],len(files)])
        evapsoil_array = np.zeros([h['ny'],len(files)])
        trans_array = np.zeros([h['ny'],len(files)])
        infl_array = np.zeros([h['ny'],len(files)])
        
        
        #print(d.shape)
        """reads in all files"""
        for t,f in enumerate(files):
            d,h=PFB_read(f,1)
            evaptot_array[:,t] = d[0,:,-5]*60*60
            evapveg_array[:,t] = d[0,:,-8]*60*60
            evapsoil_array[:,t] = d[0,:,-7]*60*60
            trans_array[:,t] = d[0,:,-9]*60*60
            infl_array[:,t] = d[0,:,-10]*60*60


        ds_etr = xr.Dataset({"evaptot": (("time","y"), np.swapaxes(evaptot_array,1,0)),
                         "evapveg": (("time","y"), np.swapaxes(evapveg_array,1,0)),
                         "evapsoil": (("time","y"), np.swapaxes(evapsoil_array,1,0)),
                         "trans": (("time","y"), np.swapaxes(trans_array,1,0)),
                        "infl": (("time","y"), np.swapaxes(infl_array,1,0))},
                            
            coords={
                "time": pd.date_range("2006-01-01", periods=len(files),freq='30min'),
                "reference_time": pd.Timestamp("2006-01-01")})

        ds_etr.evaptot.attrs['units']='mm/h'
        ds_etr.evapveg.attrs['units']='mm/h'
        ds_etr.evapsoil.attrs['units']='mm/h'
        ds_etr.trans.attrs['units']='mm/h'
        ds_etr.infl.attrs['units']='mm/h'
        
        #if os.path.exists(os.path.join(outputdir,fichier))==True :
         #   ds_etr.to_netcdf(os.path.join(outputdir,fichier),format="NETCDF4", mode='a')
            
        #fichier = os.path.join(outputdir_mil,filename)
        #if os.path.exists(fichier):
            #os.remove(fichier)
        #ds_etr.to_netcdf(fichier,format="NETCDF4") 
        
        return ds_etr



def etr_jach(dossier,filename):

        """identify files"""
        files = glob.glob(os.path.join(dossier,'*clm_output*.pfb'))
        files.sort()
        """reads in the first file to have the dimension & initialize"""
        d,h=PFB_read(files[0],1)
        evaptot_array = np.zeros([h['ny'],len(files)])
        evapveg_array = np.zeros([h['ny'],len(files)])
        evapsoil_array = np.zeros([h['ny'],len(files)])
        trans_array = np.zeros([h['ny'],len(files)])
        infl_array = np.zeros([h['ny'],len(files)])
        
        
        #print(d.shape)
        """reads in all files"""
        for t,f in enumerate(files):
            d,h=PFB_read(f,1)
            evaptot_array[:,t] = d[0,:,-5]*60*60
            evapveg_array[:,t] = d[0,:,-8]*60*60
            evapsoil_array[:,t] = d[0,:,-7]*60*60
            trans_array[:,t] = d[0,:,-9]*60*60
            infl_array[:,t] = d[0,:,-10]*60*60


        ds_etr = xr.Dataset({"evaptot": (("time","y"), np.swapaxes(evaptot_array,1,0)),
                         "evapveg": (("time","y"), np.swapaxes(evapveg_array,1,0)),
                         "evapsoil": (("time","y"), np.swapaxes(evapsoil_array,1,0)),
                         "trans": (("time","y"), np.swapaxes(trans_array,1,0)),
                        "infl": (("time","y"), np.swapaxes(infl_array,1,0))},
                            
            coords={
                "time": pd.date_range("2006-01-01", periods=len(files),freq='1H'),
                "reference_time": pd.Timestamp("2006-01-01")})

        ds_etr.evaptot.attrs['units']='mm/h'
        ds_etr.evapveg.attrs['units']='mm/h'
        ds_etr.evapsoil.attrs['units']='mm/h'
        ds_etr.trans.attrs['units']='mm/h'
        ds_etr.infl.attrs['units']='mm/h'
        
        #if os.path.exists(os.path.join(outputdir,fichier))==True :
         #   ds_etr.to_netcdf(os.path.join(outputdir,fichier),format="NETCDF4", mode='a')
            
        fichier = os.path.join(outputdir_jach,filename)
        if os.path.exists(fichier):
            os.remove(fichier)
        ds_etr.to_netcdf(fichier,format="NETCDF4") 
        
        return xr.open_dataset(fichier)






def en1_mil(dossier,filename):

    files = glob.glob(os.path.join(dossier,'*clm_output*.pfb'))
    files.sort()
    """reads in the first file to have the dimension & initialize"""
    d,h=PFB_read(files[0],1)
    latent = np.zeros([h['ny'],len(files)])
    sensible = np.zeros([h['ny'],len(files)])
    lwrad = np.zeros([h['ny'],len(files)])
    chalsol = np.zeros([h['ny'],len(files)])
    tsol = np.zeros([h['ny'],len(files)])
    t_csol = np.zeros([h['ny'],len(files)])
    surfalb = np.zeros([h['ny'],len(files)])
    #print(d.shape)
    """reads in all files"""
    for t,f in enumerate(files):
        d,h=PFB_read(f,1)
        latent[:,t] = d[0,:,-1]
        sensible[:,t] = d[0,:,-3]
        lwrad[:,t] = d[0,:,-2]
        chalsol[:,t] = d[0,:,-4]
        tsol[:,t] = d[0,:,-13]
        t_csol[:,t] = d[0,:,-14]
        surfalb[:,t] = d[0,:,-12]

    ds_en = xr.Dataset({"ch_latent": (("time","y"), np.swapaxes(latent,1,0)),
      "ch_sensible": (("time","y"), np.swapaxes(sensible,1,0)),
      "lwrad": (("time","y"), np.swapaxes( lwrad,1,0)),
      "chal_sol": (("time","y"), np.swapaxes(chalsol,1,0)),
      "tsol": (("time","y"), np.swapaxes(tsol,1,0)),
      "t_csol": (("time","y"), np.swapaxes(t_csol,1,0)),
      "surfalb": (("time","y"), np.swapaxes(surfalb,1,0))},
      coords={"y":np.arange(start = h['Y0'],stop = h['Y0']+h['ny']*h['dy'],step=h['dy']),
        "time": pd.date_range("2006-01-01", periods=len(files),freq='30min'),
        "reference_time": pd.Timestamp("2006-01-01")})

    ds_en.ch_latent.attrs['units']='W/m²'
    ds_en.ch_sensible.attrs['units']='W/m²'
    ds_en.lwrad.attrs['units']='W/m²'
    ds_en.chal_sol.attrs['units']='W/m²'
    ds_en.tsol.attrs['units']='Kelvin'
    ds_en.t_csol.attrs['units']='Kelvin'
    ds_en.surfalb.attrs['units']=''
    #ds_en.y.attrs['units']='Kelvin'


    #if os.path.exists(os.path.join(outputdir,fichier))==True :
     # ds_en.to_netcdf(os.path.join(outputdir,fichier),format="NETCDF4", mode='a')

    fichier = os.path.join(outputdir_mil,filename)
    if os.path.exists(fichier):
        os.remove(fichier)
    ds_en.to_netcdf(fichier,format="NETCDF4") 
    
    return xr.open_dataset(fichier)



def en1_jach(dossier,filename):

    files = glob.glob(os.path.join(dossier,'*clm_output*.pfb'))
    files.sort()
    """reads in the first file to have the dimension & initialize"""
    d,h=PFB_read(files[0],1)
    latent = np.zeros([h['ny'],len(files)])
    sensible = np.zeros([h['ny'],len(files)])
    lwrad = np.zeros([h['ny'],len(files)])
    chalsol = np.zeros([h['ny'],len(files)])
    tsol = np.zeros([h['ny'],len(files)])
    t_csol = np.zeros([h['ny'],len(files)])
    surfalb = np.zeros([h['ny'],len(files)])
    #print(d.shape)
    """reads in all files"""
    for t,f in enumerate(files):
        d,h=PFB_read(f,1)
        latent[:,t] = d[0,:,-1]
        sensible[:,t] = d[0,:,-3]
        lwrad[:,t] = d[0,:,-2]
        chalsol[:,t] = d[0,:,-4]
        tsol[:,t] = d[0,:,-13]
        t_csol[:,t] = d[0,:,-14]
        surfalb[:,t] = d[0,:,-12]

    ds_en = xr.Dataset({"ch_latent": (("time","y"), np.swapaxes(latent,1,0)),
      "ch_sensible": (("time","y"), np.swapaxes(sensible,1,0)),
      "lwrad": (("time","y"), np.swapaxes( lwrad,1,0)),
      "chal_sol": (("time","y"), np.swapaxes(chalsol,1,0)),
      "tsol": (("time","y"), np.swapaxes(tsol,1,0)),
      "t_csol": (("time","y"), np.swapaxes(t_csol,1,0)),
      "surfalb": (("time","y"), np.swapaxes(surfalb,1,0))},
      coords={"y":np.arange(start = h['Y0'],stop = h['Y0']+h['ny']*h['dy'],step=h['dy']),
        "time": pd.date_range("2006-01-01", periods=len(files),freq='1H'),
        "reference_time": pd.Timestamp("2006-01-01")})

    ds_en.ch_latent.attrs['units']='W/m²'
    ds_en.ch_sensible.attrs['units']='W/m²'
    ds_en.lwrad.attrs['units']='W/m²'
    ds_en.chal_sol.attrs['units']='W/m²'
    ds_en.tsol.attrs['units']='Kelvin'
    ds_en.t_csol.attrs['units']='Kelvin'
    ds_en.surfalb.attrs['units']=''
    #ds_en.y.attrs['units']='Kelvin'


    #if os.path.exists(os.path.join(outputdir,fichier))==True :
     # ds_en.to_netcdf(os.path.join(outputdir,fichier),format="NETCDF4", mode='a')

    fichier = os.path.join(outputdir_jach,filename)
    if os.path.exists(fichier):
        os.remove(fichier)
    ds_en.to_netcdf(fichier,format="NETCDF4") 

    return xr.open_dataset(fichier)




def pression_mil(dossier,filename):

        """identify files"""
        files = glob.glob(os.path.join(dossier,'*press.*.pfb'))
        files.sort()
        #skip initial condition
        files.pop(0)
        """reads in the first file to have the dimension & initialize"""
        d,h=PFB_read(files[0],1)
        #print(h)
        
        dz_mult_arr,h = PFB_read(glob.glob(os.path.join(dossier,'*dz_mult.pfb'))[0],1)
        vdz = dz_mult_arr[0,0,:]
        z = np.cumsum(vdz)-vdz/2

        pressure_array = np.zeros([h['nz'],len(files)])
        """reads in all files"""
        for t,f in enumerate(files):
            d,h=PFB_read(f,1)
            pressure_array[:,t] = d[0,0,:]
        
        ds = xr.Dataset({"press": (("time","z"), np.swapaxes(pressure_array,1,0))},
            coords={"z":-z, 
                "time": pd.date_range("2006-01-01", periods=len(files)),
                "reference_time": pd.Timestamp("2006-01-01")})

        ds.press.attrs['units']='m'
        ds.z.attrs['units']='meters'

        #ds.to_netcdf(os.path.join(datadir,"press.nc"),format="NETCDF4")

        
        #if os.path.exists(os.path.join(outputdir,fichier))==True :
            #ds.to_netcdf(os.path.join(outputdir,fichier),format="NETCDF4", mode='a')
            
        fichier = os.path.join(outputdir_mil,filename)
        if os.path.exists(fichier):
            os.remove(fichier)
        ds.to_netcdf(fichier,format="NETCDF4") 
        
        return xr.open_dataset(fichier)
    
    


def pression_jach(dossier,filename):

        """identify files"""
        files = glob.glob(os.path.join(dossier,'*press.*.pfb'))
        files.sort()
        #skip initial condition
        files.pop(0)
        """reads in the first file to have the dimension & initialize"""
        d,h=PFB_read(files[0],1)
        #print(h)
        
        dz_mult_arr,h = PFB_read(glob.glob(os.path.join(dossier,'*dz_mult.pfb'))[0],1)
        vdz = dz_mult_arr[0,0,:]
        z = np.cumsum(vdz)-vdz/2

        pressure_array = np.zeros([h['nz'],len(files)])
        """reads in all files"""
        for t,f in enumerate(files):
            d,h=PFB_read(f,1)
            pressure_array[:,t] = d[0,0,:]
        
        ds = xr.Dataset({"press": (("time","z"), np.swapaxes(pressure_array,1,0))},
            coords={"z":-z, 
                "time": pd.date_range("2006-01-01", periods=len(files)),
                "reference_time": pd.Timestamp("2006-01-01")})

        ds.press.attrs['units']='m'
        ds.z.attrs['units']='meters'

        #ds.to_netcdf(os.path.join(datadir,"press.nc"),format="NETCDF4")

        
        #if os.path.exists(os.path.join(outputdir,fichier))==True :
            #ds.to_netcdf(os.path.join(outputdir,fichier),format="NETCDF4", mode='a')
        fichier = os.path.join(outputdir_jach,filename)
        if os.path.exists(fichier):
            os.remove(fichier)
        ds.to_netcdf(fichier,format="NETCDF4")    
        
        return xr.open_dataset(fichier)




def saturation_mil(dossier,filename):

        """identify files"""
        files = glob.glob(os.path.join(dossier,'*satur.*.pfb'))
        files.sort()
        #skip initial condition
        files.pop(0)
        """reads in the first file to have the dimension & initialize"""
        d,h=PFB_read(files[0],1)
        #print(h)
        
        dz_mult_arr,h = PFB_read(glob.glob(os.path.join(dossier,'*dz_mult.pfb'))[0],1)
        vdz = dz_mult_arr[0,0,:]
        z = np.cumsum(vdz)-vdz/2
        
        satur_array = np.zeros([h['nz'],len(files)])
        #print(d.shape)
        """reads in all files"""
        for t,f in enumerate(files):
            d,h=PFB_read(f,1)
            satur_array[:,t] = d[0,0,:]

        ds = xr.Dataset({"satur": (("time","z"), np.swapaxes(satur_array,1,0))},
                        coords={"z":-z, 
                          "time": pd.date_range("2006-01-01", periods=len(files)),
                          "reference_time": pd.Timestamp("2000-01-01")})

        ds.satur.attrs['units']='m'
        ds.z.attrs['units']='meters'

        #ds.to_netcdf(os.path.join(datadir,"satur.nc"),format="NETCDF4")


        #if os.path.exists(os.path.join(outputdir,fichier))==True :
          #ds.to_netcdf(os.path.join(outputdir,fichier),format="NETCDF4", mode='a')

        #else:
        fichier = os.path.join(outputdir_mil,filename)
        if os.path.exists(fichier):
            os.remove(fichier)
        ds.to_netcdf(fichier,format="NETCDF4")    
        
        return xr.open_dataset(fichier)
    
    

def saturation_jach(dossier,filename):

        """identify files"""
        files = glob.glob(os.path.join(dossier,'*satur.*.pfb'))
        files.sort()
        #skip initial condition
        files.pop(0)
        """reads in the first file to have the dimension & initialize"""
        d,h=PFB_read(files[0],1)
        #print(h)
        
        dz_mult_arr,h = PFB_read(glob.glob(os.path.join(dossier,'*dz_mult.pfb'))[0],1)
        vdz = dz_mult_arr[0,0,:]
        z = np.cumsum(vdz)-vdz/2
        
        satur_array = np.zeros([h['nz'],len(files)])
        #print(d.shape)
        """reads in all files"""
        for t,f in enumerate(files):
            d,h=PFB_read(f,1)
            satur_array[:,t] = d[0,0,:]

        ds = xr.Dataset({"satur": (("time","z"), np.swapaxes(satur_array,1,0))},
                        coords={"z":-z, 
                          "time": pd.date_range("2006-01-01", periods=len(files)),
                          "reference_time": pd.Timestamp("2000-01-01")})

        ds.satur.attrs['units']='m'
        ds.z.attrs['units']='meters'

        #ds.to_netcdf(os.path.join(datadir,"satur.nc"),format="NETCDF4")


        #if os.path.exists(os.path.join(outputdir,fichier))==True :
          #ds.to_netcdf(os.path.join(outputdir,fichier),format="NETCDF4", mode='a')

        #else:
        fichier = os.path.join(outputdir_jach,filename)
        if os.path.exists(fichier):
            os.remove(fichier)
        ds.to_netcdf(fichier,format="NETCDF4")    
        
        return xr.open_dataset(fichier)
    
    

def lire_debit(dossier,filename):
    
    files = glob.glob(os.path.join(dossier,'*overlandsum.*.pfb'))
    files.sort()
    """reads in the first file to have the dimension & initialize"""
    d,h=PFB_read(files[0],1)
    #print(d.shape)
    #print(h)
    runoff = pd.Series(index=pd.date_range("2006-01-01", periods=len(files)))
    """reads in all files"""
    for t,f in enumerate(files):
        d,h=PFB_read(f,1)
        runoff.iloc[t] = np.sum(d[0,0,0])    
        
    return runoff
    

def plot_bilan(dfall, labels=None,  H="/", **kwargs):
    """Given a list of dataframes, with identical columns and index, create a clustered stacked bar plot. 
labels is a list of the names of the dataframe, used for the legend
title is a string for the title of the plot
H is the hatch used for identification of the different dataframe"""

    n_df = len(dfall)
    n_col = len(dfall[0].columns) 
    n_ind = len(dfall[0].index)
    fig = plt.figure(figsize=(15,8))
    axe = fig.add_subplot(111)

    for df in dfall : # for each data frame
        if isinstance(df, pd.DataFrame):
            axe = df.plot(kind="bar",
            linewidth=0,
            stacked=True,
            ax=axe,
            legend=False,
            grid=False,
            **kwargs)  # make bar plots
        else:
             axe = df.plot(kind="bar",
             linewidth=0,
             stacked=True,
             ax=axe,
             legend=False,
             grid=False,
	     color='b',
             **kwargs)  # make bar plots

    h,l = axe.get_legend_handles_labels() # get the handles we want to modify
    for i in range(0, n_df * n_col, n_col): # len(h) = n_col * n_df
        for j, pa in enumerate(h[i:i+n_col]):
            for rect in pa.patches: # for each index
                rect.set_x(rect.get_x() + 1 / float(n_df + 1) * i / float(n_col))
                rect.set_hatch(H * int(i / n_col)) #edited part     
                rect.set_width(1 / float(n_df + 1))

    axe.set_xticks((np.arange(0, 2 * n_ind, 2) + 1 / float(n_df + 1)) / 2.)
    axe.set_xticklabels(df.index, rotation = 0)

    # Add invisible data to add another legend
    n=[]        
    for i in range(n_df):
        n.append(axe.bar(0, 0, color="gray", hatch=H * i))

    l1 = axe.legend(h[:n_col], l[:n_col], loc=[1.02, 0.75])
    if labels is not None:
        l2 = plt.legend(n, labels, loc=[1.01, 0.1]) 
    axe.add_artist(l1)
    return axe



def valeurs_superieures(array, seuil):
    resultats = []
    for i in range(len(array)):
        if array[i] > seuil:
            resultats.append((i, array[i]))
    return resultats


import matplotlib.pyplot as plt
import numpy as np

def plot_distribution_dataframe(df):
    columns = df.columns
    num_columns = len(columns)

    fig, axes = plt.subplots(1, num_columns, figsize=(16, 6))  # Crée une seule ligne de sous-graphiques

    for i, column in enumerate(columns):
        data = df[column].dropna().tolist()
        if data:
            n, bins, patches = axes[i].hist(data, bins='auto', alpha=0.7, rwidth=0.85,log = False)
            axes[i].grid(axis='y', alpha=0.75)
            axes[i].set_xlabel('Valeurs')
            axes[i].set_ylabel('Fréquence')
            axes[i].set_title(f'Distribution des valeurs - {column}')

            # Affichage du nombre de valeurs dans chaque classe
            for j, patch in enumerate(patches):
                axes[i].text(patch.get_x() + patch.get_width() / 2, patch.get_height(),
                             int(n[j]), ha='center', va='bottom')

            # Affichage des intervalles des données
            bins_str = [f'{bins[k]:.4f} - {bins[k+1]:.4f}' for k in range(len(bins)-1)]
            #axes[i].text(0.5, -0.28, 'Intervalle des données:', ha='center', va='center', transform=axes[i].transAxes)
            axes[i].text(0.5, -0.3, '\n'.join(bins_str), ha='center', va='center', transform=axes[i].transAxes)

    plt.tight_layout()  # Ajuste automatiquement les espacements entre les sous-graphiques
    plt.show()
    



def plot_distribution_new(df, column_name):
    fig, ax = plt.subplots(figsize=(8, 6))
    ax.hist(df[column_name], bins=20, density=True, alpha=0.5)
    ax.set_xlabel(column_name)
    ax.set_ylabel('Densité')
    ax.legend()

    plt.tight_layout()
    plt.show()


def calc_retention(h,thr,ths,hg,n):
    """
    """
    return thr+(ths-thr)*(1+(h/hg)**n)**(-(1-1/n))
    
    
def calc_cond(h,Ks,alpha,n):
    """
    """
    m=1-1/n
    return Ks*((1-((h*alpha)**(n-1))/((1+(h*alpha)**n)**m))**2)/((1+(h*alpha)**n)**(m/2))




def concatenate_tcls(path_directory=None):
    v_list = []
    # Parcourir tous les dossiers et fichiers dans le répertoire racine
    for root, directories, files in os.walk(path_directory):
        # Parcourir les dossiers commençant par 'sim_'
        for directory in directories:
            if directory.startswith('sim_'):
                # Construire le chemin complet du dossier
                directory_path = os.path.join(root, directory)
                # Parcourir les fichiers avec l'extension .tcl dans le dossier
                for file in os.listdir(directory_path):
                    if file.endswith('.tcl'):
                        # Construire le chemin complet du fichier .tcl
                        file_path = os.path.join(directory_path, file)
                        # Appliquer la fonction populate_from_tcl au fichier .tcl
                        v = VGM.VGMps()
                        v=v.populate_from_tcl(file_path)
                        # Ajouter le résultat à la liste v_list
                        v_list.append(v)

    return v_list




def plot_hth_Kh_SA(v,save_path=None):
    """plot all retention curve and K(theta) curves after Sensivity Analysis"""

    h=np.concatenate([np.arange(0.001,0.049,0.001),np.arange(0.05,1,0.01),np.arange(1.1,500,0.1)]) 
    #h=np.concatenate([np.arange(0.001,0.09,0.001),np.arange(0.05,1,0.01),np.arange(1.1,500,0.1)])           
    
    colors = ['blue', 'red', 'green', 'purple', 'orange', 'cyan']  # Liste de couleurs à utiliser pour les courbes
    legend_labels = []

    fig, ax = plt.subplots(1, 2,figsize=(20,10))
    l1=[]
    l2=[]
    names=[]
    h1_color = 'blue'  # Couleur pour les courbes "H1"
    for key,val in v.items():
        th = calc_retention(h,thr = val['Poro']*val['Sr'],ths= val['Poro']*val['Ssat'],hg= 1/val['alpha'],n = val['n'])
        K = calc_cond(h,val['Ks'],alpha=val['alpha'],n = val['n'])
        if key.startswith('v'):
            key = 'H1'
            color = h1_color  # Utilisation de la couleur pour les courbes "H1"
        else:
            color = colors[(len(names)) % len(colors)]  # Sélection d'une couleur différente pour les autres courbes
        
        tmp1, = ax[0].semilogy(th, h, color=color)
        tmp2, = ax[1].semilogy(th, K, color=color)
        l1.append(tmp1)
        l2.append(tmp2)

        if key not in legend_labels:
            names.append(key)
            legend_labels.append(key)

    for i in range(len(names)):
        if names[i].startswith('v'):
            names[i] = 'H1'
    
    ## legende manuelle

    lines = [Line2D([0], [0], color=c) for c in colors]
    labels = ['H1', 'H2','H3','H4','H5']
    ax[0].legend(lines, labels,loc='upper right', fontsize=20, ncol=1)
    ax[1].legend(lines, labels,loc='upper right', fontsize=20, ncol=1)


    ax[0].set_xlim([0,0.45])
    #ax[0].set_xlim([0,0.9])
    ax[0].set_ylim([0.01,1000])
    ax[0].set_ylabel('|h| (m)', fontsize=20, color = 'k')
    ax[0].set_xlabel(r'$\theta$', fontsize=20, color = 'k')
    ax[0].tick_params(axis='both', which='major', labelsize=20)

    ax[1].set_ylim([0.00001,10])
    ax[1].set_xlim([0,0.45])
    ax[1].set_ylabel('K', fontsize=20, color = 'k')
    ax[1].set_xlabel(r'$\theta$', fontsize=20, color = 'k')
    ax[1].tick_params(axis='both', which='major', labelsize=20)

    #ax[0].legend(l1, names, loc='upper right', fontsize=20, ncol=1)
    #ax[1].legend(l2, names, loc='upper right', fontsize=20, ncol=1)
    if save_path:
        plt.savefig(save_path)

    plt.show()



def plot_hth_Kh_new(v,save_path=None):
        """plot all retention curve, K(theta) curves and save fig"""
        
        h=np.concatenate([np.arange(0.001,0.049,0.001),np.arange(0.05,1,0.01),np.arange(1.1,500,0.1)]) 
        #h=np.concatenate([np.arange(0.001,0.09,0.001),np.arange(0.05,1,0.01),np.arange(1.1,500,0.1)])           
        
        fig, ax = plt.subplots(1, 2,figsize=(20,10))
        l1=[]
        l2=[]
        names=[]
        for key,val in v.items():
            th = calc_retention(h,thr = val['Poro']*val['Sr'],ths= val['Poro']*val['Ssat'],hg= 1/val['alpha'],n = val['n'])
            K = calc_cond(h,val['Ks'],alpha=val['alpha'],n = val['n'])
            tmp1, = ax[0].semilogy(th,h)
            tmp2, = ax[1].semilogy(th,K)
            l1.append(tmp1)
            l2.append(tmp2)
            names.append(key)
            
        
        ax[0].set_xlim([0,0.45])
        #ax[0].set_xlim([0,0.9])
        ax[0].set_ylim([0.01,1000])
        ax[0].legend(l1,names,loc='upper right',fontsize=20,ncol=1)
        ax[0].set_ylabel('|h| (m)', fontsize=20, color = 'k')
        ax[0].set_xlabel(r'$\theta$', fontsize=20, color = 'k')
        ax[0].tick_params(axis='both', which='major', labelsize=20)
        
        ax[1].set_ylim([0.00001,10])
        ax[1].set_xlim([0,0.45])
        ax[1].legend(l2,names,loc='upper right',fontsize=20,ncol=1)
        ax[1].set_ylabel('K', fontsize=20, color = 'k')
        ax[1].set_xlabel(r'$\theta$', fontsize=20, color = 'k')
        ax[1].tick_params(axis='both', which='major', labelsize=20)

        if save_path:
            plt.savefig(save_path)

        #plt.show()
        
        
      


######################### Lucie RAPP

