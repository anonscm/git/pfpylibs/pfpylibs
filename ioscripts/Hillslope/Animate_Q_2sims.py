"""
Animate simulation results
"""
__author__     = "PHYREV team"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

import os, glob, shutil 

import PFlibs
import numpy as np
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pandas as pd
import rasterio
from Data_proc import readDataAMMA as rdA
import pyproj
from copy import copy
from copy import deepcopy
from matplotlib.animation import FuncAnimation
import matplotlib.animation as animation
from scipy import interpolate


#~ simdir1 = r'/homelocal5/phyrev/_PARFLOW/PF_lectures/hillslope_course/hillslope2017/GODART/results'
#~ simdir2 = r'/homelocal5/phyrev/_PARFLOW/PF_lectures/hillslope_course/hillslope2017/GODART/results2'
#~ simdir1 = r'/homelocal5/phyrev/_PARFLOW/PF_lectures/hillslope_course/hillslope2017/GODART/results_sahel'
#~ simdir2 = r'/homelocal5/phyrev/_PARFLOW/PF_lectures/hillslope_course/hillslope2017/GODART/results_soudan'
#~ simdir1 = r'/homelocal5/phyrev/_PARFLOW/PF_lectures/hillslope_course/hillslope2017/GODART/results_soudan_high2_steeper'
#~ simdir2 = r'/homelocal5/phyrev/_PARFLOW/PF_lectures/hillslope_course/hillslope2017/GODART/results_soudan_low2_steeper'
simdir1 = r'/home/hectorb/PARFLOW/PF_lectures/hillslope_course/hillslope2017/GODART/results_soudan_high2_steeper'
simdir2 = r'/home/hectorb/PARFLOW/PF_lectures/hillslope_course/hillslope2017/GODART/results_soudan_low2_steeper'

simuname = r'hillslope'

n_coupled_layers = 22
n_CLM_2Dlayers = 14

slope=0.03


""" Read & process simulation outputs:"""

sim = PFout.PFout(simdir1,simuname)
#~ sim.set_reading_parameters(n_t=366,n_t_clm=8760,n_t_forc = 8760*2, dump_int=24,dump_int_clm=1,conv=1,conv_clm=1 * 60 * 60, conv_forc= 1 * 60 * 30)
sim.set_reading_parameters(n_t=731,n_t_clm=17520,n_t_forc = 17520*2, dump_int=24,dump_int_clm=1,conv=1,conv_clm=1 * 60 * 60, conv_forc= 1 * 60 * 30)
sim.read_domain_dimensions_from_PFBs()

sim.read_allPFoutput_from_PFBs()
#~ sim.read_CLMoutput(nlayers = n_coupled_layers+n_CLM_2Dlayers)
#~ sim.extract_CLM_2Doutputs(nlayers = 16 +14 )
sim.read_allconstant_outputs()
sim.read_forcings()
sim.get_runoff_from_PF()
#~ sim.calculate_WSC()
#remove spinup:
sim.satur_array =np.delete(sim.satur_array,range(365),axis=3)
sim.press_array =np.delete(sim.press_array,range(365),axis=3)
sim.overland_runoff =np.delete(sim.overland_runoff,range(365))
sim.n_t=366
sim.n_t_forc = 8760*2


sim2 = PFout.PFout(simdir2,simuname)
#~ sim2.set_reading_parameters(n_t=366,n_t_clm=8760,n_t_forc = 8760*2, dump_int=24,dump_int_clm=1,conv=1,conv_clm=1 * 60 * 60, conv_forc= 1 * 60 * 30)
sim2.set_reading_parameters(n_t=731,n_t_clm=17520,n_t_forc = 17520*2, dump_int=24,dump_int_clm=1,conv=1,conv_clm=1 * 60 * 60, conv_forc= 1 * 60 * 30)
sim2.read_domain_dimensions_from_PFBs()
sim2.read_allPFoutput_from_PFBs()
sim2.get_runoff_from_PF()
#remove spinup:
sim2.satur_array =np.delete(sim2.satur_array,range(365),axis=3)
sim2.press_array =np.delete(sim2.press_array,range(365),axis=3)
sim2.overland_runoff =np.delete(sim2.overland_runoff,range(365))
sim2.n_t=366
sim2.n_t_forc = 8760*2
""" Create panda series """

# create time series index for PF & CLM:
ts_pf = pd.date_range('1/1/2007', periods=sim.n_t-1)
ts_forcings = pd.date_range('1/1/2007 00:00', freq='30min', periods=sim.n_t_forc)

# create time series with associated index:
ts_ov = pd.Series(sim.overland_runoff, index=ts_pf)
ts_ov2 = pd.Series(sim2.overland_runoff, index=ts_pf)

Q = pd.DataFrame({'Q':ts_ov})
Q2 = pd.DataFrame({'Q':ts_ov2})

""" Animate"""


domain_thickness=sim.dz_mult_arr[0,0,:].sum()*sim.dz
# pressure at the bottom cell is obtained at half the cell thickness:
offset_for_WTD = domain_thickness - sim.dz_mult_arr[0,0,-1]*sim.dz/2
WTD=offset_for_WTD-sim.press_array[0,::-1,-1,:]
#for some reason, water table seems to rise high above the surface!!!!! check that out
WTD[WTD<0]=0
WTD2=offset_for_WTD-sim2.press_array[0,::-1,-1,:]
#for some reason, water table seems to rise high above the surface!!!!! check that out
WTD2[WTD2<0]=0
y = sim.y
dy = sim.dy
ny = sim.ny
z = [0.01,0.02,0.03,0.04,0.09,0.18,0.27,0.36,0.45,0.58,0.74,0.9,1.06,1.22,1.42,1.66,1.9,2.14,2.38,4.65,8.95,13.25,17.55,21.58]
#adapt the plot to the actual slope: 
# 1) interpolate on a finer matrix, the same dimensions as original, define the finness, as a function of the slope
# 2) place each column on a larger dimension (in Z) matrix by shifting each column depending on slope
dz_step = slope * dy
print('dz_step: %f (topographic diff betw one cell and the next one)'%dz_step)
ndiv_dz_step = 5
print('ndiv_dz_step: %d (resolution to discretize this topo diff)'%ndiv_dz_step)
Ztot = 24.0-0.
ncells_Z = np.int(Ztot / (dz_step/ndiv_dz_step) +1)
print('new number of cells (Ztot / (dz_step/ndiv_dz_step): %d'%ncells_Z)
deniv_tot = slope * (np.max(y)-np.min(y))
print('total deniv: %f'%deniv_tot)
ncells_Z_withslope = np.int(ncells_Z + (ny-1) * ndiv_dz_step)
print('number of cells with slope (ncells_Z + (ny-1) * ndiv_dz_step): %d'%ncells_Z_withslope)

#~ z2 = np.linspace(0,24,101)
z2 = np.linspace(0,24,np.int(ncells_Z))
#~ z3 = np.linspace(0,24 * 2 ,201)
z3 = np.linspace(0,24 + np.int(dy*ny*slope) ,np.int(ncells_Z_withslope))

satur_int1 = np.zeros((1,sim.ny,np.size(z2),sim.n_t))
satur_int2 = np.zeros((1,sim.ny,np.size(z2),sim.n_t))
yy,zz = np.meshgrid(y,z)
for i in range(sim.n_t):
    tmp = np.reshape(sim.satur_array[0,:,:,i],(sim.ny,sim.nz))    
    tmp2 = np.reshape(sim2.satur_array[0,:,:,i],(sim.ny,sim.nz))        
    f1 = interpolate.interp2d(z,y,tmp,kind='linear')
    f2 = interpolate.interp2d(z,y,tmp2,kind='linear')
    #~ f = interpolate.interp2d(zz,yy,tmp,kind='linear')
    satur_int1[0,:,:,i]=f1(z2,y)
    satur_int2[0,:,:,i]=f2(z2,y)

satur_int_slope1 = np.empty((1,sim.ny,np.size(z3),sim.n_t))
satur_int_slope1[:]=np.nan
satur_int_slope2 = np.empty((1,sim.ny,np.size(z3),sim.n_t))
satur_int_slope2[:]=np.nan
zdeniv = np.zeros(sim.ny)
zdeniv[1:]=-10*slope

for i in range(sim.ny):
    #~ satur_int_slope1[0,i,200-(i*2)-101:200-i*2,:] = satur_int1[0,i,:,:]
    #~ satur_int_slope2[0,i,200-(i*2)-101:200-i*2,:] = satur_int2[0,i,:,:]
    satur_int_slope1[0,i,(np.int(ncells_Z_withslope))-(i*ndiv_dz_step)-ncells_Z:(ncells_Z_withslope)-(i*ndiv_dz_step),:] = satur_int1[0,i,:,:]
    satur_int_slope2[0,i,(np.int(ncells_Z_withslope))-(i*ndiv_dz_step)-ncells_Z:(ncells_Z_withslope)-(i*ndiv_dz_step),:] = satur_int2[0,i,:,:]
    
#~ satur_slope = np.nan((1,ny,np.size(z2),n_t))

satur_int_slopem1 = np.ma.masked_where(np.isnan(satur_int_slope1),satur_int_slope1)
satur_int_slopem2 = np.ma.masked_where(np.isnan(satur_int_slope2),satur_int_slope2)

fig, (ax1, ax2) = plt.subplots(2, 2,figsize=(20,8))

cmap=cm.jet_r
cmap.set_bad('w',1.)
#~ pcol1 = ax1[0].pcolormesh(np.array(y),np.array(z),sim.satur_array[0,:,:,0].T,cmap=cmap, shading = 'gouraud')
pcol1 = ax1[0].pcolormesh(np.array(y),np.array(z3),satur_int_slopem1[0,::-1,:,0].T,cmap=cmap, shading = 'gouraud')
plt1 = ax1[0].plot(y,WTD[:,0]-np.cumsum(zdeniv),'k-')
fig.axes[0].invert_yaxis()
ax1[0].margins(x=0)
ax1[0].margins(y=0)
leg = plt.cm.ScalarMappable(cmap=cmap)
leg.set_array([0.,1.])
cbar2 = fig.colorbar(leg, ax = ax1[0])
cbar2.ax.set_title('saturation')
ax1[0].set_ylabel('Depth (m)', fontsize=16, color = 'k')

#~ pcol2 = ax2[0].pcolormesh(np.array(y),np.array(z),sim2.satur_array[0,:,:,0].T,cmap=cmap, shading = 'gouraud')
pcol2 = ax2[0].pcolormesh(np.array(y),np.array(z3),satur_int_slopem2[0,::-1,:,0].T,cmap=cmap, shading = 'gouraud')
plt2 = ax2[0].plot(y,WTD2[:,0]-np.cumsum(zdeniv),'k-')
fig.axes[2].invert_yaxis()
ax2[0].margins(x=0)
ax2[0].margins(y=0)
leg = plt.cm.ScalarMappable(cmap=cmap)
leg.set_array([0.,1.])
cbar = fig.colorbar(leg, ax = ax2[0])
cbar.ax.set_title('saturation')
ax2[0].set_ylabel('Depth (m)', fontsize=16, color = 'k')
ax2[0].set_xlabel('Y (m)', fontsize=16, color = 'k')

ax1[1] = Q.Q.plot(ax = ax1[1])
ax1[1].set_ylabel('mm/day', fontsize=16)
ax1[1].set_ylim(0,35)
ann = ax1[1].annotate("",xy=(Q.index[0], Q['Q'][0]), xycoords='data',
    xytext=(Q.index[0],32), textcoords='data',
    arrowprops=dict(arrowstyle="->",
    connectionstyle="arc3"))


ax2[1] = Q2.Q.plot(ax = ax2[1])
ax2[1].set_ylabel('mm/day', fontsize=16)
ax2[1].set_ylim(0,35)
ann2 = ax2[1].annotate("",xy=(Q2.index[0], Q2['Q'][0]), xycoords='data',
    xytext=(Q2.index[0],32), textcoords='data',
    arrowprops=dict(arrowstyle="->",
    connectionstyle="arc3"))



# define animation:
def update(i):
    global plt1,plt2,pcol1,pcol2, ann, ann2
    label = 'timestep {0}'.format(i)
    print(label)
    # Update the line and the axes (with a new xlabel). Return a tuple of
    # "artists" that have to be redrawn for this frame.
    ann.remove()
    ann2.remove()
    plt1[0].remove()
    plt2[0].remove()
    pcol1.remove()
    pcol2.remove()
    #~ pcol1 = ax1[0].pcolormesh(np.array(y),np.array(z),sim.satur_array[0,::-1,:,i].T,cmap=cmap, shading = 'gouraud')
    pcol1 = ax1[0].pcolormesh(np.array(y),np.array(z3),satur_int_slopem1[0,::-1,:,i].T,cmap=cmap, shading = 'gouraud')
    plt1 = ax1[0].plot(y,WTD[:,i]-np.cumsum(zdeniv),'k-')
    #~ pcol2 = ax2[0].pcolormesh(np.array(y),np.array(z),sim2.satur_array[0,::-1,:,i].T,cmap=cmap, shading = 'gouraud')
    pcol2 = ax2[0].pcolormesh(np.array(y),np.array(z3),satur_int_slopem2[0,::-1,:,i].T,cmap=cmap, shading = 'gouraud')
    plt2 = ax2[0].plot(y,WTD2[:,i]-np.cumsum(zdeniv),'k-')
    ann = ax1[1].annotate("",xy=(Q.index[i], Q['Q'][i]), xycoords='data',
        xytext=(Q.index[i],32), textcoords='data',
        arrowprops=dict(arrowstyle="->",
        connectionstyle="arc3"))
    ann2 = ax2[1].annotate("",xy=(Q2.index[i], Q2['Q'][i]), xycoords='data',
        xytext=(Q2.index[i],32), textcoords='data',
        arrowprops=dict(arrowstyle="->",
        connectionstyle="arc3"))
    return (ax1[0],ax1[1],ax2[0],ax2[1])


#animate
Writer = animation.writers['ffmpeg']
#~ writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)
writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=5000)

anim = FuncAnimation(fig, update, frames=np.arange(0, 360, 10), interval=20, repeat=False)
#~ anim.save('slopes_soudanian_yr4_steeper.mp4', writer=writer)
plt.show()



