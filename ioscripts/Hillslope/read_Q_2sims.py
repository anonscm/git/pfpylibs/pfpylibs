"""
- Read overland sum pfb output file
- create hydrograph for selected cell
Created on Mon Sep 29 11:47:42 2014

@author: jgilbert
modified by savannah
"""

def PFB_read(pfb_file_path, flag):
    import struct
    import numpy
    #print("**This code DOES handle subgrids**")
    
    hdr_dict = {}
    
    with open(pfb_file_path,'rb') as pfbf:
        hdr_dict['len_x'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['len_y'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['len_z'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['nx'] = struct.unpack('>i', pfbf.read(4))[0]
        hdr_dict['ny'] = struct.unpack('>i', pfbf.read(4))[0]
        hdr_dict['nz'] = struct.unpack('>i', pfbf.read(4))[0]
        hdr_dict['dx'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['dy'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['dz'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['ns'] = struct.unpack('>i', pfbf.read(4))[0]
        ns = hdr_dict['ns']
        #print(ns)
        nx = hdr_dict['nx']
        ny = hdr_dict['ny']
        nz = hdr_dict['nz']
        
        if nx == 0:
            nx = int(raw_input("What should the NX be?"))
            hdr_dict['nx'] = nx
        if ny == 0:
            ny = int(raw_input("What should the NY be?"))
            hdr_dict['ny'] = ny
        if nz ==0:
            nz =int(raw_input("What should the NZ be?"))
            hdr_dict['nz'] = nz
        
        len_x = hdr_dict['len_x']
        #print "len_x, len_y, len_z, nx ny nz --> %f %f %f %i %i %i" %(len_x, len_x, len_x, nx, ny, nz)
        
        pfb_array = numpy.zeros((nx, ny, nz))
        
        for subgrids in range(ns):
            hdr_dict['ix'] = struct.unpack('>i', pfbf.read(4))[0]
            ix = hdr_dict['ix']
            hdr_dict['iy'] = struct.unpack('>i', pfbf.read(4))[0]
            iy = hdr_dict['iy']
            #print(iy)
            hdr_dict['iz'] = struct.unpack('>i', pfbf.read(4))[0]
            iz = hdr_dict['iz']
            hdr_dict['nx2'] = struct.unpack('>i', pfbf.read(4))[0]
            nx2 = hdr_dict['nx2']
            hdr_dict['ny2'] = struct.unpack('>i', pfbf.read(4))[0]
            ny2 = hdr_dict['ny2']
            #print(ny2)
            hdr_dict['nz2'] = struct.unpack('>i', pfbf.read(4))[0]
            nz2 = hdr_dict['nz2']
            hdr_dict['rx'] = struct.unpack('>i', pfbf.read(4))[0]
            rx = hdr_dict['rx']
            hdr_dict['ry'] = struct.unpack('>i', pfbf.read(4))[0]
            ry = hdr_dict['ry']
            hdr_dict['rz'] = struct.unpack('>i', pfbf.read(4))[0]
            rz = hdr_dict['rz']
            #print("ix iy iz nx2 ny2 nz2 rx ry rz --> %i %i %i %i %i %i %i %i %i\n" %(ix,iy, iz, nx2, ny2, nz2, rx,ry, rz))
            for lays in range(nz2):
                for rows in range(ny2):
                    for cols in range(nx2):
                        val = struct.unpack('>d', pfbf.read(8))[0]
                        #q = raw_input(val)
                        pfb_array[(cols+ix), (rows + iy), (nz2-(lays + iz)-1)] = val
                        #pfb_array[(cols+ix-1), (nx2-(iy)-1), (nz2-(lays + iz)-1)] = val
                        #pfb_array[(rows + iy-1), (cols+ix-1), (nz2-(lays + iz)-1)] = val
                        #print val
                        
    if flag ==1:
        return(pfb_array, hdr_dict)
    else:
        return(pfb_array)

###############################################################################
## MAIN SECTION : Select Files (pfb) and Select what you want done with them ##
###############################################################################

import numpy as np
import os.path
import time
import matplotlib.pyplot as plt
import pylab
import matplotlib.image as mpimg
import plotly.plotly as py
import math
import os
import pandas as pd
import matplotlib.cm as cm
from scipy import interpolate
from matplotlib.animation import FuncAnimation

# Print the full arrays and tupples
np.set_printoptions(threshold=np.nan)

# Set number of time steps from output to use
n_t = 366
#~ n_t = 731

dump_int = 24

n_t_clm = 8760
#~ n_t_clm = 17520
dump_int_clm = 1



"""converter"""
#~ conv = 15
#~ conv_clm = 900
conv = 1
# from mm/s to mm/hr
conv_clm = 1 * 60 * 60


# Domaine
#m2
aire = 5000

cwd = os.getcwd()
# Directory containing the pfb results of the PF simulation

#~ RT_dir = r'../../hillslope2017'
#~ RT_dir = r'../../hillslope2017/SAINTSULPICE'
#~ RT_dir = r'../../hillslope2017/OUINE'
RT_dir = r'./'

#~ base_dir = '/'.join([RT_dir,'results'])
#~ base_dir = '/'.join([RT_dir,'results_perm'])
#~ base_dir = '/'.join([RT_dir,'results5'])
base_dir = '/'.join([RT_dir,'results'])

print("Found base directory...")

# Defining the file names of the outputs
#~ runnam = r'hillslope_sens'
runnam = r'hillslope'

prop_base = os.path.join(base_dir, '.'.join([runnam, "out"]))

# Use out_type and out_base to chose the property
out_type = r'overlandsum'
out_base = os.path.join(base_dir, '.'.join([runnam,"out",out_type]))

# Get format of property arrays with the geometry : overland flow and pressure
if os.path.isfile('.'.join([prop_base,"press",format(0,"05.0F"),"pfb"])):
    
    [test_array, hdr] =  PFB_read('.'.join([prop_base,"press",format(0,"05.0F"),"pfb"]),1)      
    
    nz = hdr['nz']
    nx = hdr['nx']
    ny = hdr['ny']
    dx = hdr['dx']
    dy = hdr['dy']
# +1 car 00000.pfb existe
    press_array = np.zeros((nx,ny,nz,n_t))
    satur_array = np.zeros((nx,ny,nz,n_t))   
    
    overlandsum_array = np.zeros((nx,ny,n_t-1))
    evaptranssum_array = np.zeros((nx,ny,n_t-1))
    evaptrans_array = np.zeros((nx,ny,n_t-1)) 
    
# 36 nombre de sorties clm    
    clm_output_array = np.zeros((nx,ny,36,n_t_clm))

    tmp_arr = np.zeros((nx,ny,nz))
#constant values
    if os.path.isfile('.'.join([prop_base,"dz_mult.pfb"])):
        dz_mult_fp = '.'.join([prop_base,"dz_mult.pfb"])
        dz_mult_arr = PFB_read(dz_mult_fp,0)
    else:
        dz_mult_arr = tmp_arr * 1
            
    if os.path.isfile('.'.join([prop_base,"mannings.pfb"])):    
        mannings_fp = '.'.join([prop_base,"mannings.pfb"])
        mannings_arr = PFB_read(mannings_fp,0)
    else:
        mannings_arr = tmp_arr + 5.5e-6
    
    mask_fp = '.'.join([prop_base,"mask.pfb"])
    mask_arr = PFB_read(mask_fp,0)
    
    
    #~ perm_x_fp = '.'.join([prop_base,"perm_x.pfb"])
    #~ perm_x_arr = PFB_read(perm_x_fp,0)
    #~ 
    #~ perm_y_fp = '.'.join([prop_base,"perm_y.pfb"])
    #~ perm_y_arr = PFB_read(perm_y_fp,0)
#~ 
    #~ perm_z_fp = '.'.join([prop_base,"perm_z.pfb"])
    #~ perm_z_arr = PFB_read(perm_z_fp,0)

    porosity_fp = '.'.join([prop_base,"porosity.pfb"])
    porosity_arr = PFB_read(porosity_fp,0)

    slope_x_fp = '.'.join([prop_base,"slope_x.pfb"])
    slope_x_arr = PFB_read(slope_x_fp,0)
    
    slope_y_fp = '.'.join([prop_base,"slope_y.pfb"])
    slope_y_arr = PFB_read(slope_y_fp,0)
    
    specific_storage_fp = '.'.join([prop_base,"specific_storage.pfb"])
    specific_storage_arr = PFB_read(specific_storage_fp,0)    
    

# Fill in property arrays with space and time dependant values
for t in range(n_t):

    # Get all time file directories
    press_fp = '.'.join([prop_base,"press",format(t*dump_int,"05.0F"),"pfb"])
    satur_fp = '.'.join([prop_base,"satur",format(t*dump_int,"05.0F"),"pfb"])

    # Read pfb
    press_arr = PFB_read(press_fp,0)
    satur_arr = PFB_read(satur_fp,0)
   
    #Fill arrays for each time output
    press_array[:,:,:,t]=press_arr[:,:,:]
    satur_array[:,:,:,t]=satur_arr[:,:,:]
    

for t in range(n_t-1):
    # Get all time file directories
    overlandsum_fp = '.'.join([prop_base,"overlandsum",format((t+1)*dump_int,"05.0F"),"pfb"])
    evaptranssum_fp = '.'.join([prop_base, "evaptranssum", format((t+1)*dump_int,"05.0F"),"pfb"])
    evaptrans_fp = '.'.join([prop_base, "evaptrans", format((t+1)*dump_int,"05.0F"),"pfb"])

    # Read pfb
    overlandsum_arr = PFB_read(overlandsum_fp,0)
    evaptranssum_arr = PFB_read(evaptranssum_fp,0)
    evaptrans_arr = PFB_read(evaptrans_fp,0)
    
    #Fill arrays for each time output
    overlandsum_array[:,:,t]= overlandsum_arr[:,:,0]
    evaptranssum_array[:,:,t]=evaptranssum_arr[:,:,0]
    evaptrans_array[:,:,t]=evaptrans_arr[:,:,0]

 

""" Runoff OEVRLANDSUM in mm """

#~ 
overland_runoff = []
overland_runoff_tmp = overlandsum_array[0,0,:]
for i in range(len(overland_runoff_tmp)) : overland_runoff.append(overland_runoff_tmp[i]*1000/aire)


# SIMulation 2
base_dir = '/'.join([RT_dir,'results2'])
runnam = r'hillslope'
prop_base = os.path.join(base_dir, '.'.join([runnam, "out"]))
press_array2 = np.zeros((nx,ny,nz,n_t))
satur_array2 = np.zeros((nx,ny,nz,n_t)) 
overlandsum_array2 = np.zeros((nx,ny,n_t-1))

# Fill in property arrays with space and time dependant values
for t in range(n_t):

    # Get all time file directories
    press_fp = '.'.join([prop_base,"press",format(t*dump_int,"05.0F"),"pfb"])
    satur_fp = '.'.join([prop_base,"satur",format(t*dump_int,"05.0F"),"pfb"])

    # Read pfb
    press_arr = PFB_read(press_fp,0)
    satur_arr = PFB_read(satur_fp,0)
   
    #Fill arrays for each time output
    press_array2[:,:,:,t]=press_arr[:,:,:]
    satur_array2[:,:,:,t]=satur_arr[:,:,:]
    

for t in range(n_t-1):
    # Get all time file directories
    overlandsum_fp = '.'.join([prop_base,"overlandsum",format((t+1)*dump_int,"05.0F"),"pfb"])

    # Read pfb
    overlandsum_arr = PFB_read(overlandsum_fp,0)
    
    #Fill arrays for each time output
    overlandsum_array2[:,:,t]= overlandsum_arr[:,:,0]

 

""" Runoff OEVRLANDSUM in mm """

#~ 
overland_runoff2 = []
overland_runoff_tmp2 = overlandsum_array2[0,0,:]
for i in range(len(overland_runoff_tmp2)) : overland_runoff2.append(overland_runoff_tmp2[i]*1000/aire)
    

""" Variation du stock d'eau dans le sol in mm"""

mailles_array = np.zeros((nx,ny,nz,n_t))

var_stock = []

for t in range(n_t):
    for i in range(ny):
        for j in range(nz):
            vol_maille = hdr['dx']*hdr['dy']*(hdr['dz']*dz_mult_arr[0,i,j])*porosity_arr[0,i,j]*satur_array[0,i,j,t]
            #~ if press_array[0,i,j,t] > 0 :
            if press_array[0,i,j,t] > -100000 :
                mailles_array[0,i,j,t]=vol_maille + press_array[0,i,j,t]*specific_storage_arr[0,0,0]*(satur_array[0,i,j,t]*hdr['dx']*hdr['dy']*hdr['dz']*dz_mult_arr[0,i,j])
            else :
                mailles_array[0,i,j,t]=vol_maille

for t in range(n_t-1):    
        vol_t_avant = np.sum(mailles_array[:,:,:,t])*1000/aire
        vol_t_apres = np.sum(mailles_array[:,:,:,t+1])*1000/aire
        diff = vol_t_apres-vol_t_avant
        var_stock.append(diff)
    

#stock simple en mm
stock_simple = [0]
for i in range(n_t):
    
    stock_simple.append(np.sum(mailles_array[:,:,:,i]))
    #~ stock_simple.append(np.sum(mailles_array[:,:,:,i])*1000/aire)

#var_stock[-1]=0
#~ var_vol = pd.Series(var_stock, index = dates_less)


var_tot = np.sum(mailles_array[:,:,:,n_t-1])*1000/aire - np.sum(mailles_array[:,:,:,0])*1000/aire

var_cum = sum(var_stock)

#~ plt.figure()
#~ plt.plot(var_vol, 'r')
#~ plt.xlabel('Temps')
#~ plt.ylabel('Volume mm')
#~ plt.title('Volume d eau dans le sol en fonction du temps')
#~ 




""" Lecture fichier de forcages """
#os.chdir("../hillslope_clm")
os.chdir(RT_dir)

forcages = np.loadtxt("forcagePF.200707.30mn_updated.txt.0")
#os.chdir("../hillslope_clm")
os.chdir(cwd)

SW = forcages[0:n_t_clm*2,0]
LW = forcages[0:n_t_clm*2,1]
Pluie = forcages[0:n_t_clm*2,2]
Temp = forcages[0:n_t_clm*2,3]
Vent = forcages[0:n_t_clm*2,4]
Neige = forcages[0:n_t_clm*2,5]
Press =forcages[0:n_t_clm*2,6]
Hum = forcages[0:n_t_clm*2,7]






""" Pluie m/hr puis en mm"""
#mm/s
P_sec = 0.0025

#P = sum(Pluie)*900
P = sum(Pluie)*60*30


""" PLOT using panda series"""
# create time series index for PF & CLM:
ts_pf = pd.date_range('1/1/2007', periods=n_t-1)
ts_forcings = pd.date_range('1/1/2007 00:00', freq='30min', periods=n_t_clm*2)

# create time series with associated index:
ts_ov = pd.Series(overland_runoff, index=ts_pf)
ts_ov2 = pd.Series(overland_runoff2, index=ts_pf)

ts_ov_cumsum = pd.Series(np.cumsum(overland_runoff), index =ts_pf)
ts_stock = pd.Series(np.cumsum(var_stock), index=ts_pf)

ts_rain_cumsum = pd.Series(np.cumsum(Pluie*60*30), index = ts_forcings)
ts_rain = pd.Series(Pluie*60*30, index = ts_forcings)

# create daily average
ts_rain_d = ts_rain.resample('D').sum()


#create dataframe object for easy plotting
pf_out_cum = pd.DataFrame({'Q':ts_ov,
                        'cumsum(Q)':ts_ov_cumsum,
                        'S':ts_stock})
forcings_cum = pd.DataFrame({'cumsum(P)':ts_rain_cumsum})
#~ pf_out = pd.DataFrame({'Q':ts_ov,
                        #~ 'S':ts_stock,
                        #~ 'ETRday':ts_evap_d})
#~ pf_out = pd.DataFrame({'Q':ts_ov,
                        #~ 'S':ts_stock,
                        #~ 'ETRday':ts_evap_d,
                        #~ 'evap_grndday':ts_evap_ground_d,
                        #~ 'evap_vegday':ts_evap_veg_d})                        

#~ pf_out = pd.DataFrame({'Q':ts_ov,
                        #~ 'ETRday':ts_evap_d,
                        #~ 'evap_grndday':ts_evap_ground_d,
                        #~ 'evap_vegday':ts_evap_veg_d})                        
pf_out = pd.DataFrame({'Q':ts_ov})                        
                                                                                    
                        

#~ forcings = pd.DataFrame({'P':ts_rain})
forcings = pd.DataFrame({'P':ts_rain_d})

Q_P = pd.DataFrame({'Q':ts_ov,'P':ts_rain_d})
Q_P2 = pd.DataFrame({'Q':ts_ov2,'P':ts_rain_d})

#~ plt.figure()                    
#~ ax = pf_out_cum.plot()
#~ clm_out_cum.plot(ax = ax)
#~ forcings_cum.plot(ax = ax)
#~ forcings_cum.plot(ax = ax, kind = 'bar')



plt.figure(figsize=(20,6))
#~ ax2 = pf_out.plot()
#~ plt.plot([Q_P.index.values[100], Q_P.index.values[100],Q_P.index.values[100]],[0,30,70])
#~ plt.plot(Q_P.index.values[100:101],[0,70])
plt.ylim(0,70)
#~ 
ax2 = Q_P.Q.plot()

ax2.annotate("",xy=(Q_P.index[100], Q_P['Q'][100]), xycoords='data',
  xytext=(Q_P.index[100],40), textcoords='data',
  arrowprops=dict(arrowstyle="->",
  connectionstyle="arc3"),
            )
#~ Q_P.Q.plot()
plt.ylabel('mm/day', fontsize=16)
plt.show()

#~ clm_out.plot(ax = ax2)
#~ forcings.plot(ax = ax2)
#~ ax2=forcings.plot(secondary_y=True, kind = 'bar',ax = ax2)
#~ ax2=Q_P.P.plot(secondary_y=True, kind = 'bar',ax = ax2)
#~ fig = ax2.get_figure()
#~ fig.axes[1].invert_yaxis()
#~ plt.rcParams["figure.figsize"] = (20,6)
#~ plt.ylabel('mm/day', fontsize=16)
#~ plt.savefig('./streamflow3.png',format='png')
#~ plt.savefig('../../hillslope2017/budget.png',format='png')

#~ plt.draw_if_interactive()
#~ plt.savefig('./streamflow.png',format='png')

#~ plt.plot(evap_tot)
#~ plt.show()


domain_thickness=dz_mult_arr[0,0,:].sum()*hdr['dz']
# pressure at the bottom cell is obtained at half the cell thickness:
offset_for_WTD = domain_thickness - dz_mult_arr[0,0,-1]*hdr['dz']/2

WTD=offset_for_WTD-press_array[0,::-1,-1,:]
#for some reason, water table seems to rise high above the surface!!!!! check that out
WTD[WTD<0]=0


y = [dy*y+dy/2 for y in range(ny)]
z = [0.01,0.02,0.03,0.04,0.09,0.18,0.27,0.36,0.45,0.58,0.74,0.9,1.06,1.22,1.42,1.66,1.9,2.14,2.38,4.65,8.95,13.25,17.55,21.58]
#~ fig,ax = plt.subplots(figsize=(20,5))
#adapt the plot to the actual slope: 
# 1) interpolate on a finer matrix, the same dimensions as original, define the finness, as a function of the slope
# 2) place each column on a larger dimension (in Z) matrix by shifting each column depending on slope
slope=0.05
dz_step = slope * dy
print('dz_step: %f (topographic diff betw one cell and the next one)'%dz_step)
ndiv_dz_step = 5
print('ndiv_dz_step: %d (resolution to discretize this topo diff)'%ndiv_dz_step)
Ztot = 24.0-0.
ncells_Z = Ztot / (dz_step/ndiv_dz_step) +1
print('new number of cells (Ztot / (dz_step/ndiv_dz_step): %d'%ncells_Z)
deniv_tot = slope * (np.max(y)-np.min(y))
print('total deniv: %f'%deniv_tot)
ncells_Z_withslope = np.int(ncells_Z + (ny-1) * ndiv_dz_step)
print('number of cells with slope (ncells_Z + (ny-1) * ndiv_dz_step): %d'%ncells_Z_withslope)

#~ z2 = np.linspace(0,24,101)
z2 = np.linspace(0,24,ncells_Z)
#~ z3 = np.linspace(0,24 * 2 ,201)
z3 = np.linspace(0,24 + dy*ny*slope ,ncells_Z_withslope)
satur_int = np.zeros((1,ny,np.size(z2),n_t))

yy,zz = np.meshgrid(y,z)
    
for i in range(n_t):
    tmp = np.reshape(satur_array[0,:,:,i],(ny,nz))    
    #~ tmp = np.reshape(np.log10(np.abs(press_array[0,:,:,i])),(ny,nz))
    f = interpolate.interp2d(z,y,tmp,kind='linear')
    #~ f = interpolate.interp2d(zz,yy,tmp,kind='linear')
    satur_int[0,:,:,i]=f(z2,y)
    #~ satur_int[0,:,:,i]=f(z2,y)

satur_int_slope = np.empty((1,ny,np.size(z3),n_t))
satur_int_slope[:]=np.nan
zdeniv = np.zeros(ny)
zdeniv[1:]=-10*slope

for i in range(ny):
    #~ satur_int_slope[0,i,200-(i*2)-101:200-i*2,:] = satur_int[0,i,:,:]
    #~ print((ncells_Z_withslope)-(i*ndiv_dz_step)-ncells_Z)
    #~ print((ncells_Z_withslope)-(i*ndiv_dz_step))
    satur_int_slope[0,i,(ncells_Z_withslope)-(i*ndiv_dz_step)-ncells_Z:(ncells_Z_withslope)-(i*ndiv_dz_step),:] = satur_int[0,i,:,:]
#~ satur_slope = np.nan((1,ny,np.size(z2),n_t))

satur_int_slopem = np.ma.masked_where(np.isnan(satur_int_slope),satur_int_slope)

fig, (ax1, ax2) = plt.subplots(1, 2,figsize=(20,5))
cmap=cm.jet_r
cmap.set_bad('w',1.)
#~ ax.pcolormesh(np.array(y),np.array(z),satur_array[0,:,:,0].T,cmap=cm.jet_r, shading = 'gouraud')
#~ ax1.pcolormesh(np.array(y),np.array(z),satur_array[0,::-1,:,183].T,cmap=cm.jet_r, shading = 'gouraud')
#~ ax1.pcolormesh(np.array(y),np.array(z2),satur_int[0,::-1,:,183].T,cmap=cm.jet_r, shading = 'gouraud')
ax1.pcolormesh(np.array(y),np.array(z3),satur_int_slopem[0,::-1,:,183].T,cmap=cmap, shading = 'gouraud')
ax1.plot(y,WTD[:,183]-np.cumsum(zdeniv),'k-')

#~ ax.pcolormesh(np.array(y),np.array(z),satur_array[0,:,:,365].T,cmap=cm.jet_r, shading = 'gouraud')

#~ fig.gca().invert_yaxis()
fig.axes[0].invert_yaxis()
ax1.margins(x=0)
ax1.margins(y=0)
#~ ax.set_yscale('log')
leg = plt.cm.ScalarMappable(cmap=cmap)
leg.set_array([0.,1.])
#~ leg.set_array([-1.,0.])
fig.colorbar(leg, ax = ax1)
ax1.set_ylabel('Depth (m)', fontsize=16, color = 'k')
ax1.set_xlabel('Y (m)', fontsize=16, color = 'k')

ax2 = Q_P.Q.plot()

ax2.annotate("",xy=(Q_P.index[183], Q_P['Q'][183]), xycoords='data',
  xytext=(Q_P.index[183],65), textcoords='data',
  arrowprops=dict(arrowstyle="->",
  connectionstyle="arc3"),
            )
#~ Q_P.Q.plot()
plt.ylabel('mm/day', fontsize=16)
plt.ylim(0,70)

plt.show()

########try to animate:

#plot first plot:

domain_thickness=dz_mult_arr[0,0,:].sum()*hdr['dz']
# pressure at the bottom cell is obtained at half the cell thickness:
offset_for_WTD = domain_thickness - dz_mult_arr[0,0,-1]*hdr['dz']/2
WTD=offset_for_WTD-press_array[0,::-1,-1,:]
#for some reason, water table seems to rise high above the surface!!!!! check that out
WTD[WTD<0]=0
WTD2=offset_for_WTD-press_array2[0,::-1,-1,:]
#for some reason, water table seems to rise high above the surface!!!!! check that out
WTD2[WTD2<0]=0

y = [dy*y+dy/2 for y in range(ny)]
z = [0.01,0.02,0.03,0.04,0.09,0.18,0.27,0.36,0.45,0.58,0.74,0.9,1.06,1.22,1.42,1.66,1.9,2.14,2.38,4.65,8.95,13.25,17.55,21.58]


fig, (ax1, ax2) = plt.subplots(2, 2,figsize=(20,5))
pcol1 = ax1[0].pcolormesh(np.array(y),np.array(z),satur_array[0,:,:,0].T,cmap=cm.jet_r, shading = 'gouraud')
plt1 = ax1[0].plot(y,WTD[:,0],'r-')
#~ plt_list[0:2]=[plt1,plt1]

fig.axes[0].invert_yaxis()
ax1[0].margins(x=0)
ax1[0].margins(y=0)
#~ ax.set_yscale('log')
leg = plt.cm.ScalarMappable(cmap=cm.jet_r)
leg.set_array([0.,1.])
fig.colorbar(leg, ax = ax1[0])
ax1[0].set_ylabel('Depth (m)', fontsize=16, color = 'k')
ax1[0].set_xlabel('Y (m)', fontsize=16, color = 'k')

pcol2 = ax2[0].pcolormesh(np.array(y),np.array(z),satur_array2[0,:,:,0].T,cmap=cm.jet_r, shading = 'gouraud')
plt2 = ax2[0].plot(y,WTD2[:,0],'r-')
#~ plt_list2[0:2]=[plt2,plt2]

fig.axes[2].invert_yaxis()
ax2[0].margins(x=0)
ax2[0].margins(y=0)
#~ ax.set_yscale('log')
leg = plt.cm.ScalarMappable(cmap=cm.jet_r)
leg.set_array([0.,1.])
fig.colorbar(leg, ax = ax2[0])
ax2[0].set_ylabel('Depth (m)', fontsize=16, color = 'k')
ax2[0].set_xlabel('Y (m)', fontsize=16, color = 'k')
ann_list=[]
ann_list2=[]

ax1[1] = Q_P.Q.plot(ax = ax1[1])
ann = ax1[1].annotate("",xy=(Q_P.index[0], Q_P['Q'][0]), xycoords='data',
    xytext=(Q_P.index[0],65), textcoords='data',
    arrowprops=dict(arrowstyle="->",
    connectionstyle="arc3"))
ann_list[0:2]=[ann,ann]
plt.ylabel('mm/day', fontsize=16)
plt.ylim(0,70)

ax2[1] = Q_P2.Q.plot(ax = ax2[1])
ann2 = ax2[1].annotate("",xy=(Q_P2.index[0], Q_P2['Q'][0]), xycoords='data',
    xytext=(Q_P2.index[0],65), textcoords='data',
    arrowprops=dict(arrowstyle="->",
    connectionstyle="arc3"))
ann_list2[0:2]=[ann2,ann2]
plt.ylabel('mm/day', fontsize=16)
plt.ylim(0,70)


# define animation:
def update(i):
    global plt1,plt2,pcol1,pcol2
    label = 'timestep {0}'.format(i)
    print(label)
    # Update the line and the axes (with a new xlabel). Return a tuple of
    # "artists" that have to be redrawn for this frame.
    ann_list[0].remove()
    ann_list2[0].remove()
    plt1[0].remove()
    plt2[0].remove()
    pcol1.remove()
    pcol2.remove()
    #~ plt_list[0].remove()
    #~ plt2_list2[0].remove()
    pcol1 = ax1[0].pcolormesh(np.array(y),np.array(z),satur_array[0,::-1,:,i].T,cmap=cm.jet_r, shading = 'gouraud')
    plt1 = ax1[0].plot(y,WTD[:,i],'r-')
    pcol2 = ax2[0].pcolormesh(np.array(y),np.array(z),satur_array2[0,::-1,:,i].T,cmap=cm.jet_r, shading = 'gouraud')
    plt2 = ax2[0].plot(y,WTD2[:,i],'r-')
    #~ plt_list[0:2]=[plt1,plt1]
    #~ plt_list2[0:2]=[plt2,plt2]
    ann = ax1[1].annotate("",xy=(Q_P.index[i], Q_P['Q'][i]), xycoords='data',
        xytext=(Q_P.index[i],65), textcoords='data',
        arrowprops=dict(arrowstyle="->",
        connectionstyle="arc3"))
    ann_list[0:2]=[ann,ann]
    ann2 = ax2[1].annotate("",xy=(Q_P2.index[i], Q_P2['Q'][i]), xycoords='data',
        xytext=(Q_P2.index[i],65), textcoords='data',
        arrowprops=dict(arrowstyle="->",
        connectionstyle="arc3"))
    ann_list2[0:2]=[ann2,ann2]
    return (ax1[0],ax1[1],ax2[0],ax2[1])


#animate
#~ anim = FuncAnimation(fig, update, frames=np.arange(0, 360, 10), interval=20, blit=True)
anim = FuncAnimation(fig, update, frames=np.arange(0, 360, 10), interval=20)
plt.show()
