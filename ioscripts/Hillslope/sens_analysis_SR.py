"""
Read and write tcl files for parflow
"""
__author__     = "PHYREV team"
__copyright__  = "Copyright 2017"
__license__    = "GNU GPL"

import os, glob, shutil 

import PFlibs
import numpy as np
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pandas as pd

##################################
#### set root directories:
##################################

testdir = r'/home/rogivues/auto_sens_analysis/'
tclfile = '/'.join([testdir,'simu.tcl'])

##################################
#### define number of simulations, sample parameters and define sim names:
##################################

nsim_rain = 10
nsim_ks = 10
simunames = [os.path.join(testdir, "sample__{0}".format(i)) for i in range(nsim_rain*nsim_ks)]

# for changing Ks of H1

#~ simus = PF.Simus()
#~ for ii, irundir in enumerate(simunames):
    #~ k = "Geom.H1.Perm.Value"
    #v = (ii +1) * 1e-5
    #~ v = 5*10**(-(6+ii*0.1))
    #~ 
    #~ # m/s => m / min 
    #~ v = v * 60
    #~ dic = {k: v}
    #~ print(dic)
    #~ simus = simus.add_simu(irundir, **dic)
    
#for changing rain forcings

#~ simus = PF.Simus()
#~ for ii, irundir in enumerate(simunames):
    #~ k = "Solver.CLM.MetFileName"
    #~ v = [("forcages_{0}0.txt.0".format(ii+1))]
    #~ if ii == 9 : v=95
    #~ dic = {k: v}
    #~ print(dic)
    #~ simus = simus.add_simu(irundir, **dic)


"""how to""" 

k=0
kpluie = "Solver.CLM.MetFileName"
kks = "Geom.H1.Perm.Value"

simus = PF.Simus()

for i in range(nsim_rain):
    vpluie = [("forcages_{0}0.txt.0".format(i+1))]
    if i == 9 : vpluie="forcages_95.txt.0"
    
    for j in range(nsim_ks):
         v_ks = 5e-7 + ((5e-5-5e-7)/(nsim_ks-1))*j

         dic = {kpluie:vpluie,kks:v_ks}

         irundir= '/'.join([testdir,'sample____%d'%k])
         simus = simus.add_simu(irundir, **dic)
         k+=1
    


##################################
#### simulations initialization:
##################################
#~ simus.create_rundirs()
#~ simus.create_new_tcls(tclfile)
#~ simus.cp_allinputfiles(tclfile)

##################################
#### run:
##################################
#~ simus.run_simulations()

##################################
#### for processing forcages_10 becomes integer 10
##################################

#~ simus = PF.Simus()
#~ for ii, irundir in enumerate(simunames):
    #~ k = "Solver.CLM.MetFileName"
    #~ v = 10*(ii+1)
    #~ if ii == 9 : v=95
    #~ dic = {k: v}
    #~ print(dic)
    #~ simus = simus.add_simu(irundir, **dic)

##################################
#### outputs initialization:
##################################
folder_pattern = os.path.join(testdir,'sample*','results')
tcl_pattern = 'simu.tcl'
simus_out = PF.Simus()
simus_out = simus_out.populate_simus_from_patterns(folder_pattern, tcl_ref = tclfile, tcl_pattern = tcl_pattern)



##################################
#### Read & process all outputs:
##################################


simus_out = simus_out.sort_index(by = ["Solver.CLM.MetFileName","Geom.H1.Perm.Value"])


simus_out['H1perm'] = np.nan
simus_out['Precipitation'] = np.nan
simus_out['Runoff']=pd.Series(np.nan,index=simus_out.index)
simus_out['Evap']=pd.Series(np.nan,index=simus_out.index)
simus_out['WSC']=pd.Series(np.nan,index=simus_out.index)
simus_out['Qr']=pd.Series(np.nan,index=simus_out.index)
simus_out['Err']=pd.Series(np.nan,index=simus_out.index)


for i in range(len(simus_out)):
    simdir = simus_out.index[i]
    sim = PFout.PFout(simdir,'hillslope')
    # warning, current simulations are in m/mn (not hardcoded in the main code...)
    sim.set_reading_parameters(n_t=2689,n_t_clm=2688,n_t_forc = 2688, dump_int=1,dump_int_clm=1,conv=1,conv_clm=1 * 60 * 15, conv_forc= 1 * 60 * 15)
    sim.read_domain_dimensions_from_PFBs()
    

    sim.read_allPFoutput_from_PFBs()
    sim.read_CLMoutput()
    sim.extract_CLM_2Doutputs()
    sim.read_allconstant_outputs()
    ii=i//10
    if ii <9 : sim.read_forcings(forcing_file = "../forcages_{0}.txt.0".format((ii+1)*10))
    if ii == 9 : sim.read_forcings(forcing_file = "../forcages_{0}.txt.0".format(95))
    sim.calculate_runoff()
    sim.calculate_WSC()
    sim.calculate_Qr()
    
    H1perm = sim.perm_x_arr[0,0,0]
    P = sim.P
    R = sum(sim.overland_runoff)
    E = sum(sim.evap_tot)
    WSC = sim.var_tot
    Qr = R/P
    Err = P-R-E-WSC
    
    simus_out['H1perm'].loc[simdir] = H1perm
    simus_out['Precipitation'].loc[simdir] = P
    simus_out['Runoff'].loc[simdir] = R
    simus_out['Evap'].loc[simdir] = E
    simus_out['WSC'].loc[simdir] = WSC
    simus_out['Qr'].loc[simdir] = Qr
    simus_out['Err'].loc[simdir] = Err
    print(simus_out.values[i])
    print(Qr)
    #~ 


""" write results in text file """

for i in range(len(simus_out)):
    with open("results.txt", "a") as results_file:
        results_file.write("{0} ".format(i))
        results_file.write("%.7f " % simus_out['H1perm'].iloc[i])
        results_file.write("%.2f " % simus_out['Precipitation'].iloc[i])
        results_file.write("%.2f " % simus_out['Runoff'].iloc[i])
        results_file.write("%.2f " % simus_out['Evap'].iloc[i])
        results_file.write("%.2f " % simus_out['WSC'].iloc[i])
        results_file.write("%.2f " % simus_out['Qr'].iloc[i])
        results_file.write("%.0e\n" % simus_out['Err'].iloc[i])

##################################
#### Read & process one single simulation
##################################

#~ sim1 = PFout.PFout(simus_out.index[2],'hillslope_sens')
# warning, current simulations are in m/mn (not hardcoded in the main code...)
#~ sim1.set_reading_parameters(n_t=481,n_t_clm=480,n_t_forc=480,dump_int=1,dump_int_clm=1,conv=1,conv_clm=1 * 60 * 15, conv_forc= 1 * 60 * 15)
#~ sim1.read_domain_dimensions_from_PFBs()
#~ sim1.read_allPFoutput_from_PFBs()
#~ sim1.read_CLMoutput()
#~ sim1.extract_CLM_2Doutputs()
#~ sim1.read_allconstant_outputs()
#~ sim1.read_forcings()
#~ sim1.calculate_runoff()
#~ sim1.calculate_WSC()
#~ sim1.print_budget()
#~ sim1.create_panda_series_for_plot()
#~ sim1.create_some_panda_series(start_date = "1/1/2007", start_hour = "00:00", freq_PF = "15min", freq_clm = "15min", freq_forc = "15min")



##################################
##### plots:
##################################

# create daily average
#~ sim1.ts_evap_d = sim1.ts_evap.resample('D').sum()
#~ sim1.ts_evap_ground_d = sim1.ts_evap_ground.resample('D').sum()
#~ sim1.ts_evap_veg_d = sim1.ts_evap_veg.resample('D').sum()


#create dataframe object for easy plotting
#~ sim1.pf_out_cum = pd.DataFrame({'Q':sim1.ts_ov,
                        #~ 'cumsum(Q)':sim1.ts_ov_cumsum,
                        #~ 'S':sim1.ts_stock})
#~ sim1.clm_out_cum = pd.DataFrame({'cumsum(ETR)':sim1.ts_evap_cumsum})
#~ sim1.forcings_cum = pd.DataFrame({'cumsum(P)':sim1.ts_rain_cumsum})
#~ sim1.pf_out = pd.DataFrame({'Q':sim1.ts_ov,
                        #~ 'S':sim1.ts_stock,
                        #~ 'ETRday':sim1.ts_evap_d})
#~ sim1.pf_out = pd.DataFrame({'Q':sim1.ts_ov,
                        #~ 'S':sim1.ts_stock})                        

#~ sim1.pf_out = pd.DataFrame({'Q':sim1.ts_ov,
                        #~ 'ETRday':sim1.ts_evap_d,
                        #~ 'evap_grndday':sim1.ts_evap_ground_d,
                        #~ 'evap_vegday':sim1.ts_evap_veg_d})                        
                                          
                        
#~ sim1.clm_out = pd.DataFrame({'ETR':sim1.ts_evap})

#~ clm_out = pd.DataFrame({'ETR':sim1.ts_evap,
                        #~ 'evap_grnd':sim1.ts_evap_ground,
                        #~ 'evap_veg':sim1.ts_evap_veg,
                        #~ 'tran_veg':sim1.ts_tran_veg})
#~ 
#~ sim1.forcings = pd.DataFrame({'P':sim1.ts_rain})

###########" PLot time series:

#~ plt.figure()                    
#~ ax = sim1.pf_out_cum.plot()
#~ sim1.clm_out_cum.plot(ax = ax)
#~ sim1.forcings_cum.plot(ax = ax)


#~ plt.figure(figsize=(20,6))

#~ ax2 = sim1.pf_out.plot()
#~ sim1.clm_out.plot(ax = ax2)
#~ sim1.forcings.plot(ax = ax2)
#~ plt.rcParams["figure.figsize"] = (20,6)
#~ plt.ylabel('mm/day', fontsize=16)
#~ plt.savefig('../../hillslope2017/budget.png',format='png')

#~ plt.show()

#~ plt.plot(evap_tot)
#~ plt.show()

########################## plot (Y,t) variables:

#~ y = [sim1.dy*y+sim1.dy/2 for y in range(sim1.ny)]

#~ #ground evaporation without condesation mm/s
#~ evap_ground_yt = np.zeros((sim1.ny,sim1.n_t_clm))
#~ evap_ground_yt = sim1.clm_output_array[0,:,30,:]*sim1.conv_clm
#~ 
#~ # vegeation transpiration mm/s
#~ tran_veg_yt = np.zeros((sim1.ny,sim1.n_t_clm))
#~ tran_veg_yt = sim1.clm_output_array[0,:,27,:]*sim1.conv_clm
#~ 
#~ evap_veg_yt = np.zeros((sim1.ny,sim1.n_t_clm))
#~ evap_veg_yt = sim1.clm_output_array[0,:,28,:]*sim1.conv_clm
#~ 
#~ T_E = tran_veg_yt / evap_ground_yt
#~ 
#np.reshape(evap_ground_yt
#~ evap_ground_daily_yt = evap_ground_yt.reshape(sim1.ny,20,24).mean(axis=2)
#~ tran_veg_daily_yt = tran_veg_yt.reshape(sim1.ny,20,24).mean(axis=2)
#~ 
#~ T_E_daily = tran_veg_daily_yt / evap_ground_daily_yt
#~ 
#ts_clm_daily = pd.date_range('1/1/2007', periods=sim1.n_t-1)
#~ ts_clm_daily = pd.date_range('1/1/2007', periods=20)
#~ ts_clm = pd.date_range('1/1/2007',freq = "15min", periods=sim1.n_t_clm)
#~ 
#~ fig,ax = plt.subplots(figsize=(20,6))
#ax.pcolormesh(ts_clm_daily, np.array(y),evap_ground_daily_yt,cmap=cm.jet_r, shading = 'gouraud')
#~ ax.pcolormesh(ts_clm, np.array(y),evap_ground_yt,cmap=cm.jet_r, shading = 'gouraud')
#ax.pcolormesh(ts_clm_daily, np.array(y),T_E_daily,cmap=cm.jet_r, shading = 'gouraud')
#~ 
#~ plt.ylabel('Y (m)', fontsize=16)
#~ leg = plt.cm.ScalarMappable(cmap=cm.jet_r)
#~ leg.set_array([np.min(T_E_daily),np.max(T_E_daily)])
#~ fig.colorbar(leg)
#~ # plt.savefig('../../hillslope2017/T_E.png',format='png')
#~ 
#~ plt.show()
#~ 
#~ for i in range(len(ts_clm_daily)):
    #~ plt.plot(y,T_E_daily[:,i])
#~ 
#~ plt.ylabel('T / E', fontsize=16)
#~ plt.xlabel('Y (m)', fontsize=16)
#~ 
#~ # plt.savefig('../../hillslope2017/T_E_DAYbyDAY.png',format='png')
#~ 
#~ plt.show()    



    
