#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Calculate budget from a single simulation
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import datetime
import pandas as pd
from netCDF4 import Dataset
import os
import time
import glob

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout

plt.close("all")

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
simuname = 'hillslope2'
#~ simdir = r'/homelocal5/phyrev/_PARFLOW/PF_lectures/hillslope_course/hillslope2018/veg/NCoutputs/hillslope2_2018_11_28'
simdir = r'/home/hectorb/PARFLOW/PF_lectures/hillslope_course/hillslope2019/test/NCoutputs/hillslope2_2018_11_28'

"""
CLM_var is a dictionnary where keys are local variable names as in PFCLM
values are lists of string[filename, PFCLM variable name, long name, units]
"""
#~ CLM_var={'evap_tot':['','qflx_evap_tot','total evapotranspiration','mm/s'],\
        #~ 'evap_soil':['','qflx_evap_soi','soil evaporation','mm/s'],\
        #~ 'evap_veg':['','qflx_evap_veg','vegetation evaporation + transpiration','mm/s'],\
        #~ 'surf_press':['','h_top','Surface pressure','m'],\
        #~ }
CLM_var={'evap_tot':['','qflx_evap_tot','total evapotranspiration','mm/s'],\
        'surf_press':['','h_top','Surface pressure','m'],\
        }
""" add the filename in the dic"""        
for k,val in CLM_var.items():
    CLM_var[k][0]=os.sep.join([simdir,''.join([simuname,'_'+val[1]+'.nc'])])

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def plot_clustered_stacked(dfall, labels=None,  H="/", **kwargs):
    """Given a list of dataframes, with identical columns and index, create a clustered stacked bar plot. 
labels is a list of the names of the dataframe, used for the legend
title is a string for the title of the plot
H is the hatch used for identification of the different dataframe"""

    n_df = len(dfall)
    n_col = len(dfall[0].columns) 
    n_ind = len(dfall[0].index)
    axe = plt.subplot(111)

    for df in dfall : # for each data frame
        if isinstance(df, pd.DataFrame):
            axe = df.plot(kind="bar",
            linewidth=0,
            stacked=True,
            ax=axe,
            legend=False,
            grid=False,
            **kwargs)  # make bar plots
        else:
             axe = df.plot(kind="bar",
             linewidth=0,
             stacked=True,
             ax=axe,
             legend=False,
             grid=False,
	     color='b',
             **kwargs)  # make bar plots

    h,l = axe.get_legend_handles_labels() # get the handles we want to modify
    for i in range(0, n_df * n_col, n_col): # len(h) = n_col * n_df
        for j, pa in enumerate(h[i:i+n_col]):
            for rect in pa.patches: # for each index
                rect.set_x(rect.get_x() + 1 / float(n_df + 1) * i / float(n_col))
                rect.set_hatch(H * int(i / n_col)) #edited part     
                rect.set_width(1 / float(n_df + 1))

    axe.set_xticks((np.arange(0, 2 * n_ind, 2) + 1 / float(n_df + 1)) / 2.)
    axe.set_xticklabels(df.index, rotation = 0)

    # Add invisible data to add another legend
    n=[]        
    for i in range(n_df):
        n.append(axe.bar(0, 0, color="gray", hatch=H * i))

    l1 = axe.legend(h[:n_col], l[:n_col], loc=[1.01, 0.5])
    if labels is not None:
        l2 = plt.legend(n, labels, loc=[1.01, 0.1]) 
    axe.add_artist(l1)
    return axe

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
#### Read & process simulation outputs:
sim = PFout.PFout(simdir,'hillslope')

"""NetCDF"""
sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_qflx_evap_tot.nc'])]),filetype='CLM')

"""Read PF outputs"""
start_time = time.time()
with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
    sim.satur_array = np.transpose(ds.variables['saturation'][:],[3,2,1,0])
    #~ topmask=np.swapaxes(ds.variables['top_mask'][:],0,1)

with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
    sim.press_array = np.transpose(ds.variables['succion'][:],[3,2,1,0])

"""Read CLM outputs"""
for k,val in CLM_var.items():
    try:
        with Dataset(val[0], 'r') as ds:
            print('read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
            exec('sim.%s=np.swapaxes(ds.variables[\'%s\'][:],0,2)'%(k,val[1]))        
    except FileNotFoundError:
            print('CANNOT read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
print("--- %d seconds --- read all PFCLM outputs from NCs" % (time.time() - start_time))

""" Read constant files"""
ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
#watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,2)
sim.mannings_arr = np.zeros((sim.nx,sim.ny))+5.6e-6
sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,2)
"""MASK is a nx x ny boolean mask of the domain
When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
(as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
"""
MASK=sim.mask_arr[:,:,0]>0 
sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,2)
sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,2)
#~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))
sim.dz = np.diff(ds.variables['depth'][:])[0]
sim.z = np.cumsum(sim.dz_mult_arr[0,0,::-1]*sim.dz) - sim.dz_mult_arr[0,0,::-1]*sim.dz/2
sim.z = np.round(sim.z*1000)/1000


""" Read Forcings: """
forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
forc = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')

forc.rename(columns=forcing_headers,inplace=True)
forc.index=pd.date_range('1/1/2006 00:00:00', periods=len(forc), freq='30min')
sim.P=sum(forc['P'])*30*60

"""Prepare Budg datadfram"""
Budg = pd.DataFrame()

""" Get Q"""
# need to hard code the following because for 2D simulation dx = nan which sets Q to 0
sim.dx = 10
sim.calculate_runoff_from_press(x_outlet = 0,y_outlet = 0)
Budg['Q'] = sim.surfQ
catchmentsurf=np.sum(MASK)*sim.dx*sim.dy

""" convert from m3/h to mm/h"""
Budg['Q']=Budg['Q']*1000./catchmentsurf
Budg.index=pd.date_range('1/1/2006 00:00:00', periods=sim.n_t_clm, freq='H')
Budg['P']=(forc['P']*30.*60.).resample('H').sum()
Budg = Budg.resample('D').sum()

""" Get ET"""
Budg['ET']=np.reshape(sim.evap_tot*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[MASK].mean(axis=0)


""" Calculate storage """
stock = np.zeros(sim.n_t)
for t in range(sim.n_t):
    # underground storage
    stock[t] = (sim.dx*sim.dy*sim.dz*sim.dz_mult_arr*sim.porosity_arr*sim.satur_array[:,:,:,t]).sum(axis=0).sum(axis=0).sum(axis=0)
    # poroelastic storage
    tmp = (sim.dx*sim.dy*sim.dz*sim.dz_mult_arr*sim.specific_storage_arr*sim.satur_array[:,:,:,t]*sim.press_array[:,:,:,t])
    # Veery curious... so the specific storage is actually applied in PF even for < pressure: the following need be commented:
    ##~ tmp[tmp<0]=0.
    stock[t]+=tmp.sum(axis=0).sum(axis=0).sum(axis=0)
    # surface storage
    tmp = sim.dx*sim.dy*sim.press_array[:,:,-1,t]
    tmp[tmp<0]=0
    stock[t]+=tmp.sum(axis=0).sum(axis=0)
stock = stock*1000./catchmentsurf
stock = stock-stock[0]

Budg['WSC']  = np.delete(stock,0)
Budg = Budg.sort_index()
Budg.to_csv(os.sep.join([simdir,'Budg.csv']))


""" Prepare yearly budget """
BudgYR = Budg.resample('A').sum()
#~ BudgYR = Budg.resample('Y').sum()
PYR=BudgYR['P']
BudgYR2 = BudgYR.drop('P',axis = 1)
for i, y in enumerate(Budg.index.year.unique()):
    if i ==0:
         BudgYR2['WSC'][BudgYR2.index.year==y] = Budg['WSC'][Budg.index.year==y][-1]
    else:
         BudgYR2['WSC'][BudgYR2.index.year==y] = Budg['WSC'][Budg.index.year==y][-1] - Budg['WSC'][Budg.index.year==y-1][-1] 
	 

BudgYR2.index=BudgYR2.index.year
PYR.index = PYR.index.year
BudgYR2=BudgYR2[['ET','Q','WSC']] #reorder
ax=plot_clustered_stacked([BudgYR2,PYR])
ax.set_ylabel('mm')
plt.savefig(os.sep.join([simdir,'Budg.png']),dpi=400,format='png')
    
yr=[yr for yr in BudgYR2.index]

budgerr = [PYR[PYR.index==yr] - (BudgYR2[BudgYR2.index==yr]).sum(axis=1) for yr in BudgYR2.index]
B=pd.DataFrame()
B['P']=PYR
B['ET']=BudgYR2['ET']
B['Q']=BudgYR2['Q']
B['WSC']=BudgYR2['WSC']
