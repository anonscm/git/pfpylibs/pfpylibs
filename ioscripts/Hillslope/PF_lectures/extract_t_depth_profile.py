#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Extract time - depth slices at given location from single simulation

    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import datetime
import pandas as pd
from netCDF4 import Dataset
import os,glob
import time
import matplotlib.cm as cm
"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout

plt.close("all")

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
simuname = 'hillslope2'
#~ simdir = r'/homelocal5/phyrev/_PARFLOW/PF_lectures/hillslope_course/hillslope2018/veg/NCoutputs/hillslope2_2018_11_28'
simdir = r'/home/hectorb/PARFLOW/PF_lectures/hillslope_course/hillslope2019/test/NCoutputs/hillslope2_2018_11_28'

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

### Read & process simulation outputs:
sim = PFout.PFout(simdir,'hillslope')
sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_eflx_lh_tot.nc'])]),filetype='CLM')

"""Read PF outputs"""
start_time = time.time()
with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
    sim.satur_array = np.transpose(ds.variables['saturation'][:],[3,2,1,0])
    topmask=np.swapaxes(ds.variables['top_mask'][:],0,1)
"""MASK is a nx x ny boolean mask of the domain
When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
(as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
"""
MASK=topmask>0 
with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
    sim.press_array = np.transpose(ds.variables['succion'][:],[3,2,1,0])

""" Read constant files"""
ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
#watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,2)
sim.mannings_arr = np.zeros((sim.nx,sim.ny))+5e-6
sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,1)
sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,2)
sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,2)
sim.dz = np.diff(ds.variables['depth'][:])[0]
sim.z = np.cumsum(sim.dz_mult_arr[0,0,::-1]*sim.dz) - sim.dz_mult_arr[0,0,::-1]*sim.dz/2
sim.z = np.round(sim.z*1000)/1000


""" Read Forcings: """
forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
#~ forc = pd.read_csv(os.sep.join([simdir,'forcagePF.txt.0']),header=None,sep='\t')
forc = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')

forc.rename(columns=forcing_headers,inplace=True)
forc.index=pd.date_range('1/1/2006 00:00:00', periods=len(forc), freq='30min')
sim.P=sum(forc['P'])*30*60

""" extract a T-D slice """
""" show how it matches well to BF when xloc = 7-1 instead of 8-1 !"""
xloc=0
yloc =49
yloc =20
TD = sim.satur_array[xloc,yloc,:,:]
newz = np.arange(0,24,0.02)
TD2=np.zeros([len(newz),sim.n_t])
for t in range(sim.n_t):
    TD2[:,t] = np.interp(newz,sim.z,TD[::-1,t])
    
domain_thickness = np.round(np.cumsum(sim.dz_mult_arr[0,0,::-1]*sim.dz)[-1]*1000)/1000
depth_last_cell = sim.z[-1]
WT = domain_thickness - sim.press_array[xloc,yloc,0,:]
WT = depth_last_cell - sim.press_array[xloc,yloc,0,:]


""" Get All Water tables (perched)"""
#first get a mask of the base layers of each saturated (p>0) contiguous (over Z) unit
maskWT=np.zeros([sim.nz,sim.n_t])
for t in range(sim.n_t):
    profile = np.zeros(sim.nz)
    #profile[0] = base of domain. if the criteria is p < 0 => 0, you realize there are some weird points at the bottom
    #of perched layers with p>0 but < sim.dz ... so as if the cell is not entirely saturated but p>0 ... 
    profile = np.array([0. if sim.press_array[xloc,yloc,i,t]<= sim.dz_mult_arr[xloc,yloc,i]*sim.dz else 1. for i in range(sim.nz)])
    for i in range(sim.nz-1):
        profile[i+1]=(profile[i]+profile[i+1])*profile[i+1]
    # a=[1,1,1,0,0,0,1,1,0,1,0,0,0,1] => [1, 2, 3, 0, 0, 0, 1, 2, 0, 1, 0, 0, 0, 1]
    profile[profile!=1.]=np.nan
    maskWT[:,t] = profile

WTs = np.resize(sim.z[::-1],[sim.n_t,sim.nz]).T - (maskWT*sim.press_array[xloc,yloc,:,:])


""" Plot """
tsim = pd.date_range('1/1/2006 00:00:00', periods=sim.n_t, freq='D')
fig, ax = plt.subplots(1, 1,figsize=(20,10))
ax.pcolormesh(tsim,newz,TD2,cmap=cm.jet_r, shading = 'gouraud')
#~ ax.plot(tsim,WT,'r')
for i in range(sim.nz):
    ax.plot(tsim,WTs[i,:],'r')


ax.set_xlim([datetime.datetime(2006,1,1),max(tsim)])

fig.axes[0].invert_yaxis()
leg = plt.cm.ScalarMappable(cmap=cm.jet_r)
leg.set_array([0.,1.])
cb = fig.colorbar(leg, ax = ax)
cb.set_label('Saturation',fontsize=14)

ax.set_ylabel('Depth (m)', fontsize=16, color = 'k')
ax.tick_params(axis='both', which='major', labelsize=16)

#~ plt.savefig(os.sep.join([simdir,'WT.png']),dpi=400,format='png')
