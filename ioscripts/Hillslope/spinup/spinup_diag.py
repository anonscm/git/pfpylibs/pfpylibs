#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Diag script for a simple sahelian hillslope spinup run

    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
import pandas as pd
from scipy import interpolate
import matplotlib as mpl
from matplotlib.animation import FuncAnimation
import matplotlib.animation as animation
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
simuname = 'spinup'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Hillslope/spinup_test/results/'
slope=0.01

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

sim = PFout.PFout(simdir,simuname)

sim.set_reading_parameters(n_t=20,n_t_clm=0,n_t_forc = 0, dump_int=1,dump_int_clm=1,conv=1,conv_clm=1 * 60 * 60, conv_forc= 1 * 60 * 30)


sim.read_domain_dimensions_from_PFBs()
sim.read_allPFoutput_from_PFBs()
sim.read_allconstant_outputs()
#~ sim.read_forcings()
#~ sim.calculate_runoff(x_outlet = 7,y_outlet = -1)
#~ sim.calculate_WSC()


# create time series index for PF & CLM:
ts_pf = pd.date_range('1/1/2007', periods=sim.n_t-1, freq = '100D')


""" Animate"""


domain_thickness=sim.dz_mult_arr[0,0,:].sum()*sim.dz
# pressure at the bottom cell is obtained at half the cell thickness:
offset_for_WTD = domain_thickness - sim.dz_mult_arr[0,0,-1]*sim.dz/2
WTD=offset_for_WTD-sim.press_array[0,::-1,-1,:]
#for some reason, water table seems to rise high above the surface!!!!! check that out
WTD[WTD<0]=0
y = sim.y
dy = sim.dy
ny = sim.ny
z = [0.01,0.02,0.03,0.04,0.09,0.18,0.27,0.36,0.45,0.58,0.74,0.9,1.06,1.22,1.42,1.66,1.9,2.14,2.38,4.65,8.95,13.25,17.55,21.58]
z = np.cumsum(sim.dz_mult_arr[0][0])

#adapt the plot to the actual slope: 
# 1) interpolate on a finer matrix, the same dimensions as original, define the finness, as a function of the slope
# 2) place each column on a larger dimension (in Z) matrix by shifting each column depending on slope
dz_step = slope * dy
print('dz_step: %f (topographic diff betw one cell and the next one)'%dz_step)
ndiv_dz_step = 5
ndiv_dz_step = 100
print('ndiv_dz_step: %d (resolution to discretize this topo diff)'%ndiv_dz_step)
Ztot = 24.0-0.
Ztot = 10.0-0.
ncells_Z = np.int(Ztot / (dz_step/ndiv_dz_step) +1)
print('new number of cells (Ztot / (dz_step/ndiv_dz_step): %d'%ncells_Z)
deniv_tot = slope * (np.max(y)-np.min(y))
print('total deniv: %f'%deniv_tot)
ncells_Z_withslope = np.int(ncells_Z + (ny-1) * ndiv_dz_step)
print('number of cells with slope (ncells_Z + (ny-1) * ndiv_dz_step): %d'%ncells_Z_withslope)

#~ z2 = np.linspace(0,24,101)
z2 = np.linspace(0,24,np.int(ncells_Z))
z2 = np.linspace(0,10,np.int(ncells_Z))
#~ z3 = np.linspace(0,24 * 2 ,201)
z3 = np.linspace(0,24 + np.int(dy*ny*slope) ,np.int(ncells_Z_withslope))
z3 = np.linspace(0,10 + np.int(dy*ny*slope) ,np.int(ncells_Z_withslope))

satur_int1 = np.zeros((1,sim.ny,np.size(z2),sim.n_t))
yy,zz = np.meshgrid(y,z)
for i in range(sim.n_t):
    tmp = np.reshape(sim.satur_array[0,:,:,i],(sim.ny,sim.nz))    
    f1 = interpolate.interp2d(z,y,tmp,kind='linear')
    #~ f = interpolate.interp2d(zz,yy,tmp,kind='linear')
    satur_int1[0,:,:,i]=f1(z2,y)

satur_int_slope1 = np.empty((1,sim.ny,np.size(z3),sim.n_t))
satur_int_slope1[:]=np.nan
zdeniv = np.zeros(sim.ny)
zdeniv[1:]=-10*slope

for i in range(sim.ny):
    #~ satur_int_slope1[0,i,200-(i*2)-101:200-i*2,:] = satur_int1[0,i,:,:]
    #~ satur_int_slope2[0,i,200-(i*2)-101:200-i*2,:] = satur_int2[0,i,:,:]
    satur_int_slope1[0,i,(np.int(ncells_Z_withslope))-(i*ndiv_dz_step)-ncells_Z:(ncells_Z_withslope)-(i*ndiv_dz_step),:] = satur_int1[0,i,:,:]
    
#~ satur_slope = np.nan((1,ny,np.size(z2),n_t))

satur_int_slopem1 = np.ma.masked_where(np.isnan(satur_int_slope1),satur_int_slope1)

fig, ax = plt.subplots(1, 1,figsize=(20,8))

cmap=cm.jet_r
cmap.set_bad('w',1.)
#~ pcol1 = ax1[0].pcolormesh(np.array(y),np.array(z),sim.satur_array[0,:,:,0].T,cmap=cmap, shading = 'gouraud')
pcol1 = ax.pcolormesh(np.array(y),np.array(z3),satur_int_slopem1[0,::-1,:,0].T,cmap=cmap, shading = 'gouraud')
plt1 = ax.plot(y,WTD[:,0]-np.cumsum(zdeniv),'k-')
fig.axes[0].invert_yaxis()
ax.margins(x=0)
ax.margins(y=0)
leg = plt.cm.ScalarMappable(cmap=cmap)
leg.set_array([0.,1.])
cbar2 = fig.colorbar(leg, ax = ax)
cbar2.ax.set_title('saturation')
ax.set_ylabel('Depth (m)', fontsize=16, color = 'k')



# define animation:
def update(i):
    global plt1,pcol1
    label = 'timestep {0}'.format(i)
    print(label)
    # Update the line and the axes (with a new xlabel). Return a tuple of
    # "artists" that have to be redrawn for this frame.
    plt1[0].remove()
    pcol1.remove()
    #~ pcol1 = ax1[0].pcolormesh(np.array(y),np.array(z),sim.satur_array[0,::-1,:,i].T,cmap=cmap, shading = 'gouraud')
    pcol1 = ax.pcolormesh(np.array(y),np.array(z3),satur_int_slopem1[0,::-1,:,i].T,cmap=cmap, shading = 'gouraud')
    plt1 = ax.plot(y,WTD[:,i]-np.cumsum(zdeniv),'k-')


    return ax


#animate
Writer = animation.writers['ffmpeg']
#~ writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)
writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=5000)

#~ anim = FuncAnimation(fig, update, frames=np.arange(0, 360, 10), interval=20, repeat=False)
anim = FuncAnimation(fig, update, frames=np.arange(0, 20, 1), interval=20, repeat=False)
#~ anim.save('slopes_soudanian_yr4_steeper.mp4', writer=writer)
plt.show()




