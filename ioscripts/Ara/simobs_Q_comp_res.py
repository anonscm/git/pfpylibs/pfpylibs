"""
    PFPYLIBS - IOSCRIPTS

    Anaylse PF outputs for Ara at # resolutions: Streamflow
    Compare different resolutions
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os,glob
from netCDF4 import Dataset
import time
import datetime
from matplotlib.dates import MonthLocator, DateFormatter
import locale
locale.setlocale(locale.LC_ALL,'en_US.UTF-8')
import pyproj
import xarray as xr

"""local imports:"""
from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout

plt.close("all")


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
class simulation(object):
    """just a single simulation"""
    def __init__(self,simuname='',simdir='',resolution=0.0):
        self.simuname = simuname
        self.simdir =simdir
        self.resolution = resolution
        
#~ resolutions = [30,500,1000]
resolutions = [30,100,500,1000]
sims={}
for r in resolutions:
    if r == 30:
        #~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/NCout/ara30_2019_08_26'
        simdir = r'/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/NCout/ara30_2019_08_27'
        simuname = 'ara30'
        x_outlet = 151
        y_outlet = 64
    elif r == 100:
        #~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/NCout/ara100_2019_08_24'
        #~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/NCout/ara100_2019_08_27'
        simdir = r'/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/NCout/ara100_2019_08_28'
        simuname = 'ara100'
        x_outlet = 46   #46 in visit using classeur
        y_outlet = 19    #19 in visit
    elif r == 500:
        #~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/NCout/ara500_2018_10_09'
        #~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/NCout/ara500_2019_08_23'
        simdir = r'/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/NCout/ara500_2019_08_28'
        simuname = 'ara500'
        x_outlet = 9
        y_outlet = 3    
    elif r == 1000:
        #~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/NCout/ara1000_2018_10_09'
        #~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/NCout/ara1000_2019_08_23'
        simdir = r'/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/NCout/ara1000_2019_08_28'
        simuname = 'ara1000'
        x_outlet = 4
        y_outlet = 2
    sims[r] = simulation(simuname = simuname, simdir = simdir, resolution = r)
    sims[r].x_outlet = x_outlet
    sims[r].y_outlet = y_outlet
    
statsDay =True
#~ statsDay =False
#~ method = 'D'
method = 'H'


##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def minimalist_xldate_as_datetime(xldate, datemode):
    # datemode: 0 for 1900-based, 1 for 1904-based
    return (
        datetime.datetime(1899, 12, 30)
        + datetime.timedelta(days=xldate + 1462 * datemode)
        )
        
def roundDateTime(shakyDT,nearest_minutes=1):
	"""
	"""
	shakyDT += datetime.timedelta(minutes=nearest_minutes/2)
	shakyDT -= datetime.timedelta(minutes=shakyDT.minute % nearest_minutes,
                         seconds=shakyDT.second,
                         microseconds=shakyDT.microsecond)
	return shakyDT
	
def pdinterpolate(ts, datetime_index):
    """could decide to fill nans !
    but not adapted for calculating scores?
    To be tested"""
    x = pd.concat([ts, pd.Series(index=datetime_index)])
    #~ return x.groupby(x.index).first().sort_index().fillna(method="ffill")[datetime_index]
    return x.groupby(x.index).first().sort_index().interpolate('linear')[datetime_index]
    #~ return x.groupby(x.index).first().sort_index()[datetime_index]

def getSimQ(simdir,simuname='ref',x_outlet = 0, y_outlet = 0):    
    """  """
    
    """
    CLM_var is a dictionnary where keys are local variable names as in PFCLM
    values are lists of string[filename, PFCLM variable name, long name, units]
    """
    CLM_var={'surf_press':['','h_top','Surface pressure','m']}
    """ add the filename in the dic"""        
    for k,val in CLM_var.items():
        CLM_var[k][0]=os.sep.join([simdir,''.join([simuname,'_'+val[1]+'.nc'])])

    
    #### Read & process simulation outputs:
    sim = PFout.PFout(simdir,'doro')
    sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
    sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_h_top.nc'])]),filetype='CLM')

    """Read PF outputs"""
    start_time = time.time()
    with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
        sim.satur_array = ds.variables['saturation'][:]

    with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
        sim.press_array = ds.variables['succion'][:] 

    """Read CLM outputs"""
    for k,val in CLM_var.items():
        try:
            with Dataset(val[0], 'r') as ds:
                print('read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
                exec('sim.%s=np.swapaxes(ds.variables[\'%s\'][:],0,2)'%(k,val[1]))        
        except FileNotFoundError:
                print('CANNOT read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
    print("--- %d seconds --- read all PFCLM outputs from NCs" % (time.time() - start_time))

    """ Read constant files"""
    ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
    #watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
    sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,1)
    sim.mannings_arr = np.zeros((sim.nx,sim.ny))+1e-6
    sim.mask_arr = np.transpose(ds.variables['mask'][:])
    topmask = sim.mask_arr[:,:,0]
    MASK=topmask>0 
    """MASK is a nx x ny boolean mask of the domain
    When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
    (as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
    """

    sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
    sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
    sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,1)
    sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,1)
    vegmap=np.swapaxes(ds.variables['veg_map'][:],0,1)
    #~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))

    """ Read Forcings: """
    forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
    #~ forc = pd.read_csv(os.sep.join([simdir,'forcagePF.txt.0']),header=None,sep='\t')
    forc = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')
    forc.rename(columns=forcing_headers,inplace=True)
    forc.index=pd.date_range('1/1/2005 00:00:00', periods=len(forc), freq='30min')
    sim.P=sum(forc['P'])*30*60


    """Create CLM output DataFrames: """
    dsclm=pd.DataFrame()
    sim.calculate_runoff_from_press(x_outlet = x_outlet,y_outlet = y_outlet)
    dsclm['Q']=sim.surfQ
    catchmentsurf=np.sum(MASK)*sim.dx*sim.dy
    print('catchment surface : %i'%catchmentsurf)
    cnts,bins = np.histogram(vegmap*topmask,np.arange(0,20,1))
    print('n pixels trees: %i; n pixels herb:  %i'%(cnts[8],cnts[18]))
    print('ratio trees / n pixels vegetated : %0.2f'%(cnts[8]/(cnts[8]+cnts[18])))


    
    # to compare obs witj sim runoff
    #~ catchmentsurf=np.sum(MASK[:,0:-3])*sim.dx*sim.dy

    #~ """ move from m3/s (hr?) to mm/h"""
    dsclm['Q']=dsclm['Q']*1000/catchmentsurf
    #~ """ move from m3/(hr?) to m3/s"""
    #~ dsclm['Q'] = dsclm['Q']/3600.0
    dsclm.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t_clm, freq='H')
    dsclm['P']=(forc['P']*3600).resample('H').sum()

    #~ dsclmD_ref=pd.DataFrame()
    #~ for val in dsclm.columns:
        #~ if val=='Q': dsclmD_ref[val] = dsclm[val].resample('D').sum()
        #~ if val=='P': dsclmD_ref[val] = dsclm[val].resample('D').sum()
        #~ else: dsclmD_ref[val] = dsclm[val].resample('D').mean()

    #~ dsclmD=pd.DataFrame()

    #~ dsclmD['P'] = dsclm['P'].resample('D').sum()
    #~ dsclmD['Q'] = dsclm['Q'].resample('D').sum()
    #~ dsclmD.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
    
    return dsclm
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

"""Get streamflow data"""
#~ """ Parshall data"""
#~ filename=r'/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/analyse/Data/flow_parshall_2006_2012.txt'
#~ df = pd.read_csv(filename,header=None, sep='\t')
#~ df.rename(columns={0:'date',1:'Qobs',2:'QBFobs'},inplace=True)
#~ df['date']=df['date'].apply(lambda x: minimalist_xldate_as_datetime(x, 0))
#~ df = df.set_index('date')
#~ """ from m3/s to mm/hr"""
#~ dfmms=df*1000./120000.
#~ df=df*3600.*1000./120000.

""" Ara data """
rt_dir = r'/home/hectorb/DATA/streamflow/AMMA_Benin/Donga'
pre_pattern = 'CL.Run_Od-'
suf_pattern = '.csv'

#~ pre_pattern = 'CL.Run_O-'
station_list = {'ARA_PONT':2 ,'DONGA_PONT':3,'DONGA_ROUTE_DE_KOLOKONDE':3}
station_list = {'BORI':1,'COTE_238':1,'IGBOMAKORO':1,'SANI_A_SANI':1,'SARMANGA':1,'TEBOU':1,'WEWE':1,'AFFON_PONT':1,'AGUIMO':1,'AVAL-SANI':1,'BAREROU':1,'BETEROU':1}
station_list = {'ARA_PONT':2}
station_list= {k: 2 for k, v in station_list.items()}
#first col is time then daily average then hourly
proj = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')

df = pd.DataFrame()
stadic ={}
for stationname,data_column in station_list.items():
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    """m3/s"""
    sta.read_Q(filepattern, data_col = data_column)
    #~ sta.Q = sta.Q*sta.Q.index.to_series().diff().dt.total_seconds().fillna(0)
    """m3/s -> m3/hr"""
    #~ sta.Q = sta.Q*3600.
    df[stationname]=sta.Q
    print(sta.name)
    filenames = glob.glob(filepattern)    
    sta.read_latlon_from_Qfile(filenames[0])
    sta.x, sta.y = pyproj.transform(geo_system,proj,sta.lon, sta.lat)
    print(sta.lon)
    print(sta.lat)
    print(sta.x)
    print(sta.y)
    stadic[stationname] = sta

df['Qobs']=df['ARA_PONT']
"""m3/s -> mm/hr"""
df['Qobs'] = df['Qobs']*3600*1000/13000000
#~ df.resample('5min').bfill()
""" from mm/s to mm/time step """
#~ df['Qobs']=df['Qobs']*df.index.to_series().diff().dt.total_seconds().fillna(0)

for resol,simu in sims.items():
    print('simulation with resolution : %d'%resol)
    sims[resol].Q = getSimQ(sims[resol].simdir,sims[resol].simuname,x_outlet=sims[resol].x_outlet,y_outlet=sims[resol].y_outlet)


"""PLOT"""
    
#~ fig,ax = plt.subplots(nrows=len(np.arange(2008,2013,1)),figsize=(15,10),sharex=True, squeeze=True)
#~ fig,ax = plt.subplots(nrows=len(np.arange(2008,2013,1)),figsize=(20,10), squeeze=True)
fig,ax = plt.subplots(nrows=len(np.arange(2005,2007,1)),figsize=(20,10), squeeze=True)
#~ for i,yr in enumerate(np.arange(2008,2013,1)):
for i,yr in enumerate(np.arange(2005,2007,1)):
        """
        in matlab:
        unique obs where retained , then interpolated on simulated timesteps
        nans set to 0; then stats where calculated on mean daily flow 
        
        other option:
        - Round the observations times at the hour time step
        - for each hour which has more than 1 obs: get the obs as the mean
        - simulation output is at a fixed time step: drop timesteps where obs is not available
        - calculate stats on remaining couples
        
        """
    
        print(yr)
        obs = df['Qobs'][df.index.year==yr]
        #obs = obs[(obs.index.month>=6) & (obs.index.month<=11) ]
        #obs = dfmms['Qobs'][df.index.year==yr]
        for resol,simu in sims.items():
            simu = simu.Q['Q'][simu.Q.index.year==yr]
        
        if statsDay==True:
            # interpolation on sim + daily approach:
            obs = obs.groupby(obs.index).mean()
            obs_int=pd.concat([obs,simu],axis=1).interpolate(method='linear')['Qobs']
            obs_intD = obs_int.resample('D').mean()
            simuD = simu.resample('D').mean()
            merge=pd.concat([obs_intD,simuD],axis=1)
            #
        else:
            # Interpolation on the closest available obs - sim couples:
            #simuD = simu.resample('D').asfreq()
            #simuD = dsclm['Q'][dsclm.index.hour==12]
            simu = simu[(simu.index.month>=6) & (simu.index.month<=11)]
            new_dates = obs.index.round('H')
            obs2=obs
            obs2.index = new_dates
            obs2 = obs2.groupby(obs2.index).mean()
            merge=pd.concat([obs2,simu],axis=1)
            #merge=pd.concat([obs2,simuD],axis=1)
        
        kept = merge.dropna()
        res = obj.kge(kept['Qobs'].values, kept['Q'].values, return_all=True)
        print(res)        
        
        
        ax[i].plot(obs,'k',linewidth=3.0)
        #~ ax[i].plot(simu.resample(method).mean(),'r',linewidth=1)
        for resol,simu in sims.items():
            ax[i].plot(simu.Q['Q'].resample(method).mean(),linewidth=1)

        #~ ax[i].set_ylim([0,0.99])
        ax[i].set_ylim([0,2.5])
        ax[i].set_xlim([datetime.datetime(yr,6,15), datetime.datetime(yr,11,15)])
        # every month
        months = MonthLocator(bymonth=range(7, 12), bymonthday=1, interval=1)
        monthsFmt = DateFormatter("%b")
        ax[i].xaxis.set_major_locator(months)
        ax[i].xaxis.set_major_formatter(monthsFmt)
        ax[i].set_ylabel('mm/h',FontSize=20)
        #~ ax[i].set_ylabel('$m3/s',FontSize=20)
        #~ ax[i].plot(kept['Qobs'],linestyle='', marker='o', color='g',markersize=2)
        #~ ax[i].plot(kept['Q'],linestyle='', marker='o', color='g',markersize=2)        
        ax[i].text(datetime.datetime(yr,10,25),0.65,r'$r^2 = %2.2f$'%(res[1]*res[1]),FontSize=17)
        ax[i].text(datetime.datetime(yr,10,25),0.5,r'KGE = %2.2f'%res[0],FontSize=17)
        ax[i].text(datetime.datetime(yr,10,25),1.8,r'%d'%yr,FontSize = 17, fontweight='bold')
        ax[i].tick_params(axis='both', which='major', labelsize=20)
leg = ['obs']
for resol,simu in sims.items():
    leg.append(str(resol))

ax[0].legend(leg,loc='upper left',fontsize=19)    

#~ fig.subplots_adjust(bottom=0.05, top =0.95, hspace=0.001)
fig.subplots_adjust(bottom=0.05, top =0.99,left = 0.05,right =0.99, hspace=0.001)
#~ if statsDay==True:
    #~ plt.savefig(os.sep.join([simdir,'Figure_obsim_Q_statsDay.png']),dpi=400,format='png')
#~ else:
    #~ plt.savefig(os.sep.join([simdir,'Figure_obsim_Q_statsAllPts.png']),dpi=400,format='png')




""" ALTERNATIVE WAY, USE XARRAY"""

#~ ds = xr.open_dataset(os.sep.join([sims[100].simdir,''.join([sims[100].simuname,'_press.nc'])]),decode_times=False)
