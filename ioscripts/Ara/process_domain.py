# coding: utf-8
"""
    PFPYLIBS - IOSCRIPT - Ara

    Prepare the domain for various spatial resolution

    in August 2019: change the vardz 

    @copyright: 2019 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2019"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import rasterio
import pyproj
import matplotlib.pyplot as plt
import scipy.spatial as ann
import copy

"""local imports:"""
import kolinkelly as kk
import joestork as js
import PFlibs
from PFlibs import io_pfb
from PFlibs import drv_vegm_io as vegio
from procsycz import readDataAMMA as rdA
from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
from procsycz import procGeodata_Gdal
#kolinkelly is a module from waterbox that includes functionalities for NC manipulation


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

resolution = 500
#~ resolution = 1000
dz = 2.0


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

#### Open DEM:
#~ with rasterio.open("/home/hectorb/PARFLOW/PROJECTS/Ara/Ara/input/data/DEM/ara_MNT_100m_extent_342000_348400_1076000_1082400_mean_agreg_D4_pitfilled.tif") as src:
if resolution == 30: dem,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/preproc/DEM/ara_MNT_30m.tif",nodata_value = 65535)
if resolution == 100: dem,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/preproc/DEM/ara_MNT_100m_min_agreg.tif",nodata_value = 65535)
if resolution == 500: dem,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/preproc/DEM/ara_MNT_500m_min_agreg2.tif",nodata_value = 65535)
if resolution == 1000: dem,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/preproc/DEM/ara_MNT_1000m_min_agreg2.tif",nodata_value = 65535)
[lonlondem,latlatdem]=np.meshgrid(londem,latdem)
dem = dem[::-1,:]
dem[dem<0]=0


""" Prepare var dz :"""
#~ dz = 3.0
dz = 2.0
#~ v = np.array([0.09,0.39,0.75,0.75,1.2,1.8,12.9])
#~ vardz = v/dz 
#~ vardz3D = np.tile(vardz,(len(londem),len(latdem),7))
#08/2019:
v = np.array([0.08,0.32,0.08,0.08,0.32,0.64,0.88,0.32,0.32,0.64,0.64,1.2,1.8,3.6,19.08])
vardz = v/dz 
vardz3D = np.tile(vardz,(len(londem),len(latdem),len(v)))

""" If pfb writing is needed:"""
if resolution == 30:
    x0 = 342930.0
    y0 = 1076360.0
else:
    x0 = 342900.0
    y0 = 1076300.0

hdr ={'X0': x0,
 'Y0': y0,
 'Z0': 0.0,
 'dx': resolution,
 'dy': resolution,
 'dz': dz,
 'ix': 0,
 'iy': 0,
 'iz': 0,
 'ns': 1,
 'nx': len(londem),
 'nx2': len(londem),
 'ny': len(latdem),
 'ny2': len(latdem),
 'nz': len(v),
 'nz2': len(v),
 'rx': 1,
 'ry': 1,
 'rz': 1}


io_pfb.PFB_write(vardz3D[:,:,::-1], hdr, pfb_fp="/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/inputs/ara2019/vardz_Ara%d.pfb"%resolution)


""" Prepare dgridz :"""

zloc = np.cumsum(vardz*dz) - dz*vardz/2 #center of cells
final_grid = dem[:,:,None] - zloc[None,None,:]
initial_grid = np.zeros(np.shape(dem))[:,:,None] + np.arange(v.shape[0]-1,-1,-1)
dgridz = final_grid-initial_grid
dgridz  = np.swapaxes(dgridz,0,1)
io_pfb.PFB_write(dgridz[:,:,::-1], hdr, pfb_fp="/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/inputs/ara2019/dgridz_Ara%d.pfb"%resolution)

""" Prepare dem :"""
hdr2D = copy.copy(hdr)
hdr2D['nz']=1
hdr2D['nz2']=1
io_pfb.PFB_write(dem[:,:,None]*1., hdr2D, pfb_fp="/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/inputs/ara2019/dem_Ara%d.pfb"%resolution)

#~ v = np.linspace(0,6,7)
#~ orig_grid = np.tile(v,(len(latdem),len(londem),1))
#~ dest_grid = np.zeros(orig_grid.shape)
#~ for i in range(7):
    #~ dest_grid[:,:,i] = dem-v[i]
#~ dgridz = dest_grid-orig_grid
#~ io_pfb.PFB_write(np.swapaxes(dgridz[:,:,::-1],0,1), hdr, pfb_fp="/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/inputs/dgridz2019_Ara%d.pfb"%resolution)


""" Prepare domains: """
#~ d = np.array([1,1,2,2,3,3,3])
d = np.array([1,1,1,2,2,2,2,3,3,3,3,3,3,3,3])
d3D = np.tile(d,(len(londem),len(latdem),1))
PFlibs.io_pfb.PFB_write(d3D[:,:,::-1], hdr, pfb_fp="/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/inputs/ara2019/domains_Ara%d.pfb"%resolution)



