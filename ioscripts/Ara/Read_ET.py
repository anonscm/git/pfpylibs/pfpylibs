#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Anaylse PF outputs for Oueme: ET

    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os,glob
from netCDF4 import Dataset
import time
import datetime
import pyproj
import rasterio
from matplotlib.dates import MonthLocator, DateFormatter


"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
from procsycz import procGeodata_Gdal

plt.close("all")

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##


#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup_AH/oueme_2018_07_23'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_2018_08_29'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_Kapptot_exp30_CIconst_2018_09_04'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_Kapptot_exp30_Dz6_H20036_2018_09_04'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_Kapptot_exp30_Dz6_H1H2036_2018_09_06'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_Kapptot_gauss30_Dz6_H20036_2018_09_04'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_Kapptot_exp30_Dz6_H1H2036_6yrs_2018_09_07'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_2018_08_30'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_1layer_CIvar_2018_09_05'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_1layer_2018_08_30'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_domain18_2018_08_30'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H1H2036_6yrs_sandH2_2018_09_08'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H20036_CI6_2018_09_09'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H1H2036_CI6_6yrs_2018_09_10'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results__H1H2036_CI6_2018_09_08'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H20036_CI6_2018_09_11'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI6_2018_09_11'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H20036_CI6_6yrs_2018_09_12'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI6_5yrs_2018_09_13'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI4_P3_005_2018_09_15'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI4_P3_01_2018_09_17'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI4_P3_01_anis_2018_09_21'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_H336_CI4_P3_01_H4clay_2018_09_21'
simdir = r'/mnt/hectorb/equipes/phyrev/_PARFLOW/share/Amelie/DATA/ouem_out/Ciment_BH/oueme_Amelie_results_ZA_H30052_4yrs_2018_09_27'
#~ simdir = r'/mnt/hectorb/equipes/phyrev/_PARFLOW/share/Amelie/DATA/ouem_out/Ciment_BH/oueme_Amelie_results_ZA_4yrs_2018_09_27'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZAZFA_4yrs_2018_09_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZA_stat2_2018_09_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZAZFAlow_2018_09_26'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZA_2018_09_26'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZA_H30052_2018_09_26'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_H336_CI8_P3_01_anis_2018_09_26'

simuname = 'oueme'
#~ simuname = 'oueme_results'
#~ simuname = 'oueme_results_1layer_CIvar'
simuname = 'oueme_results_Kapptot_exp30_CIconst'
#~ simuname = 'oueme_results_Kapptot_exp30_Dz6_H20036'
#~ simuname = 'oueme_results_Kapptot_exp30_Dz6_H1H2036'
#~ simuname = 'oueme_results_Kapptot_gauss30_Dz6_H20036'
#~ simuname = 'oueme_results_Kapptot_exp30_Dz6_H1H2036_6yrs'
#~ simuname = 'oueme_results_H1H2036_6yrs_sandH2'
#~ simuname = 'oueme_results_H20036_CI6'
#~ simuname = 'oueme_results_H1H2036_CI6_6yrs'
simuname = 'oueme_results__H1H2036_CI6'
simuname = 'oueme_results_ZA_H20036_CI6'
simuname = 'oueme_results_ZA_H236_CI6'
#~ simuname = 'oueme_results_domain18'
simuname = 'oueme_results_H20036_CI6_6yrs'
simuname = 'oueme_results_ZA_H236_CI6_5yrs'
#~ simuname = 'oueme_results_ZA_H236_CI4_P3_01_anis'
#~ simuname = 'oueme_results_ZA_H236_H336_CI4_P3_01_H4clay'
#~ simuname = 'oueme_results_ZA_H236_CI4_P3_005'
#~ simuname = 'oueme_results_ZA_H236_CI4_P3_01'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2017/oueme2D_KH3high4_2016_12_22'
#~ simuname = 'oueme2D_KH3high4'
simuname = 'oueme_Amelie_results_ZA_H30052_4yrs'
#~ simuname = 'oueme_Amelie_results_ZA_4yrs'
#~ simuname = 'oueme_Amelie_results_ZAZFA_4yrs'
#~ simuname = 'oueme_Amelie_results_ZA_stat2'
#~ simuname = 'oueme_Amelie_results_ZAZFAlow'
#~ simuname = 'oueme_Amelie_results_ZA'
#~ simuname = 'oueme_Amelie_results_ZA_H30052'
#~ simuname = 'oueme_results_ZA_H236_H336_CI8_P3_01_anis'

n_coupled_layers = 5
n_CLM_2Dlayers = 14

statsDay =True
#~ statsDay =False

"""
CLM_var is a dictionnary where keys are local variable names as in PFCLM
values are lists of string[filename, PFCLM variable name, long name, units]
"""
CLM_var={'evap_tot':['','qflx_evap_tot','total evapotranspiration','mm/s']}
""" add the filename in the dic"""        
for k,val in CLM_var.items():
    CLM_var[k][0]=os.sep.join([simdir,''.join([simuname,'_'+val[1]+'.nc'])])  
    
vegclasses={1:'evergreen needleleaf forests',2:'evergreen broadleaf forests',\
			3:'deciduous needleleaf forests', 4: 'deciduous broadleaf forests',\
			5: 'mixed forests6 closed shrublands', 6:'closed shrublands',\
			7: 'open shrublands', 8: 'woody savannas',\
			9: 'savannas', 10: 'grasslands',\
			11: 'permanent wetlands', 12: 'croplands',\
			13: 'urban and built-up lands', 14: 'cropland / natural vegetation mosaics',\
			15: 'snow and ice', 16: 'barren or sparsely vegetated',\
			17: 'water bodies', 18: 'bare soil',\
			19: 'herbaceous'}

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


def getNaloET():
	"""
	Obtain Nalohou ET time series
	everything is hard coded here
	resample to get daily data
	"""
	
	"""Nalo /herbaceous"""
	filename=r'/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/inputs/forcage/recap_ETR_gapfill_2007-2010_Basile_modif_bh.csv'
	nalo=pd.read_csv(filename,sep=';',na_values=[-6999.0],header=None)
	#~ nalo=pd.read_csv(filename,sep=';',header=None)
	nalo.rename(columns={0:'date',1:'ETobs'},inplace=True)
	#~ nalo['date']=nalo['date'].apply(lambda x: pd.to_datetime(x))
	"""When reading time stamps from the file, this results in many badly placed points"""
	#~ nalo['date']=nalo['date'].apply(lambda x: datetime.datetime.strptime(x,'%d/%m/%y %H:%M'))
	nalo = nalo.set_index(pd.date_range('1/1/2007 00:00:00', periods=len(nalo), freq='30min'))
	nalo = nalo['ETobs']
	#~ nalo = nalo.set_index('date')
	nalo/=28.94
	naloD=nalo.resample('D').mean()
	naloD = naloD.sort_index()
	return naloD
	
def getBeleET():
	"""
	Obtain Bele ET time series
	everything is hard coded here
	resample to get daily data
	"""
	"""Bele / trees"""
	filename=r'/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/inputs/forcage/recap_ETR_gapfill_2007-2010_Basile_bele_modif_bh.csv'
	bele=pd.read_csv(filename,sep=';',na_values=[-6999.0],header=None)
	bele.rename(columns={0:'date',1:'ETobs'},inplace=True)
	bele['date']=bele['date'].apply(lambda x: pd.to_datetime(x))
	
	"""When reading time stamps from the file, this results in many badly placed points"""
	#~ bele['date']=bele['date'].apply(lambda x: datetime.datetime.strptime(x,'%d/%m/%y %H:%M'))
	bele = bele.set_index(pd.date_range('1/1/2008 00:00:00', periods=len(bele), freq='30min'))
	bele = bele['ETobs']
	#~ bele = bele.set_index('date')
	#~ bele/=28.94
	bele2= bele/28.94
	#~ bele2 = bele2.sort_index()
	
	beleD=bele2.resample('D').mean()
	tmp=bele2.resample('D').count()
	beleD[tmp<45]=np.nan  
	
	#~ fig,ax = plt.subplots(nrows=1,figsize=(15,10),sharex=True, squeeze=True)
	#~ ax.plot(bele2)
	#~ ax.plot(beleD)
	
	return beleD
	
def getLASET_and_oldECET_Nalohou():
	"""Get LAS data and older EC data"""
	rt_dir = r'/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/analyse/Data/data_flux/'
	suf_pattern = '.csv'
	pre_pattern = 'ETR_Obs_jour_Nalo'
	filepattern = glob.glob(os.path.join(rt_dir,'*'.join([pre_pattern,suf_pattern])))
	ETdata = pd.DataFrame()
	for f in filepattern:
		yr=int(f.split('_')[-1].split('.')[0])
		df = pd.read_csv(f, sep=';',na_values=[-6999])
		df.rename(columns={'Date_ET0':'date'},inplace=True)
		df['date']=df['date'].apply(lambda x: datetime.datetime.strptime(x,'%m/%d/%Y'))
		df = df.set_index('date')
		ETdata=pd.concat([ETdata,df[['LE_LAS','LE_EC']]])
	#~ ETdata/=28.94 
	ETdata = ETdata.sort_index()
	return ETdata



##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

  
#### Read & process simulation outputs:
sim = PFout.PFout(simdir,'doro')
"""NetCDF"""
sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_qflx_evap_tot.nc'])]),filetype='CLM')

"""
TMP !! 
"""
sim.n_t = 721

"""Read PF outputs"""
start_time = time.time()
with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
    sim.satur_array = ds.variables['saturation'][:]
    #~ topmask=np.swapaxes(ds.variables['top_mask'][:],0,1)
    vegmap=np.swapaxes(ds.variables['veg_map'][:],0,1)
    xslopes=np.swapaxes(ds.variables['xslope'][:],0,1)
# for some weird reason sometimes the mask aint in the nc... so pick up another one
ds=Dataset(r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZA_2018_09_26/oueme_Amelie_results_ZA_satur.nc', 'r')
topmask=np.swapaxes(ds.variables['top_mask'][:],0,1)

"""MASK is a nx x ny boolean mask of the domain
When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
(as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
"""
MASK=topmask>0 
with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
    sim.press_array = ds.variables['succion'][:] 

"""Read CLM outputs"""
for k,val in CLM_var.items():
    try:
        with Dataset(val[0], 'r') as ds:
            print('read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
            exec('sim.%s=np.swapaxes(ds.variables[\'%s\'][:],0,2)'%(k,val[1]))        
    except FileNotFoundError:
        print('CANNOT read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
print("--- %d seconds --- read all PFCLM outputs from NCs" % (time.time() - start_time))

""" Read constant files"""
ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
#watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,1)
sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,1)
sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,1)
sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,1)

# for some weird reason sometimes the mask aint in the nc... so pick up another one
#~ ds=Dataset(r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZA_2018_09_26/oueme_Amelie_results_ZA_satur.nc', 'r')
#~ sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,1)


"""Create CLM output DataFrames: """
#~ CLM_varsubset=['evap_tot','evap_ground','evap_soil','evap_veg','tran_veg']
CLM_varsubset=['evap_tot']


"""For the ET mean min max, remove bas-fond cells and left bank (Flux tower is on right bank)"""
vegmaprightbank=vegmap
#~ vegmaprightbank[0:8,:]=0
#~ vegmaprightbank[0:9,:]=0
dsclmD=pd.DataFrame()
dsclmD_min=pd.DataFrame()
dsclmD_max=pd.DataFrame()
dsclmDherb=pd.DataFrame()
dsclmDherb_min=pd.DataFrame()
dsclmDherb_max=pd.DataFrame()
dsclmDtrees=pd.DataFrame()
dsclmDtrees_min=pd.DataFrame()
dsclmDtrees_max=pd.DataFrame()   
for k in CLM_varsubset:
    exec('dsclmD[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[MASK].mean(axis=0)'%k)
    exec('dsclmD_min[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[MASK].min(axis=0)'%k)
    exec('dsclmD_max[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[MASK].max(axis=0)'%k)
    
    exec('dsclmDherb[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[(vegmap==19.) & (MASK)].mean(axis=0)'%k)
    exec('dsclmDherb_min[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[(vegmap==19.) & (MASK)].min(axis=0)'%k)
    exec('dsclmDherb_max[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[(vegmap==19.) & (MASK)].max(axis=0)'%k)
    
    exec('dsclmDtrees[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[(vegmap==7.) & (MASK)].mean(axis=0)'%k)
    exec('dsclmDtrees_min[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[(vegmap==7.) & (MASK)].min(axis=0)'%k)
    exec('dsclmDtrees_max[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[(vegmap==7.) & (MASK)].max(axis=0)'%k)
    
dsclmD.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
dsclmD_min.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
dsclmD_max.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')

dsclmDherb.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
dsclmDherb_min.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
dsclmDherb_max.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')

dsclmDtrees.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
dsclmDtrees_min.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
dsclmDtrees_max.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')

""" Get ET data: see readDataAMMA for hardwired paths to change if the data is elsewhere """
ETdata = getLASET_and_oldECET_Nalohou()
naloD = getNaloET()
beleD = getBeleET()
ETdata.rename(columns={'LE_EC':'ETobs'},inplace=True)
naloD=pd.concat([naloD,ETdata])
beleD=pd.concat([beleD,ETdata['LE_LAS']],axis=1)




#~ fig,ax = plt.subplots(nrows=5,figsize=(15,10),sharex=True, squeeze=True)
fig,ax = plt.subplots(nrows=2,figsize=(16,10),sharex=True, squeeze=True)

linessim,=ax[0].plot(dsclmDherb['evap_tot'])
ax[0].fill_between(dsclmDherb['evap_tot'].index,\
    np.reshape(np.array(dsclmDherb_min['evap_tot']),len(dsclmD_min),1),\
    np.reshape(np.array(dsclmDherb_max['evap_tot']),len(dsclmD_max),1),\
    alpha=0.5)
linesLASobs,=ax[0].plot(naloD['LE_LAS'],color='b',marker='+',markersize=4,linestyle='-',linewidth=0.5)
ax[0].plot(naloD['ETobs'],color='r',marker='+',markersize=4,linestyle='-',linewidth=0.5)
linesECobs,=ax[0].plot(naloD[0],color='r',marker='+',markersize=4,linestyle='-',linewidth=0.5)
ax[0].legend((linessim,linesECobs,linesLASobs),('sim: herbaceous','obs: EC','obs: LAS'),loc='upper left',ncol=3,fontsize=17)
ax[0].set_ylabel('mm/d',fontsize=17)
ax[0].set_xlim([datetime.datetime(2005,1,1), datetime.datetime(2010,12,31)])
#~ ax[0].set_ylim([0,7])
ax[0].set_ylim([0,7.9])
ax[0].tick_params(axis='both', which='major', labelsize=17)

linessim,=ax[1].plot(dsclmDtrees['evap_tot'])
ax[1].fill_between(dsclmDtrees['evap_tot'].index,\
    np.reshape(np.array(dsclmDtrees_min['evap_tot']),len(dsclmD_min),1),\
    np.reshape(np.array(dsclmDtrees_max['evap_tot']),len(dsclmD_max),1),\
    alpha=0.5)
linesLASobs,=ax[1].plot(beleD['LE_LAS'],color='b',marker='+',markersize=4,linestyle='-',linewidth=0.5)
linesECobs,=ax[1].plot(beleD['ETobs'],color='r',marker='+',markersize=4,linestyle='-',linewidth=0.5)
ax[1].legend((linessim,linesECobs,linesLASobs),('sim: trees','obs: EC','obs: LAS'),loc='upper left',ncol=3,fontsize=17)
ax[1].set_ylabel('mm/d',fontsize=17)
ax[1].set_xlim([datetime.datetime(2005,1,1), datetime.datetime(2010,12,31)])
#~ ax[1].set_ylim([0,7])
ax[1].set_ylim([0,7.9])
ax[1].tick_params(axis='both', which='major', labelsize=17)
fig.subplots_adjust(bottom=0.03, top =0.99, left =0.05, right =0.99,hspace=0.001)

plt.savefig(os.sep.join([simdir,'Figure_compare_ET.png']),dpi=400,format='png')

