#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Analyse WT outputs for ara simulation:
    compare several simulations with different resolutions

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime
import os, glob,copy,warnings
from collections import OrderedDict
import xarray as xr

"""local imports:"""
from procsycz import readDataAMMA as rdA
from procsycz import procGeodata_Gdal
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb

plt.close("all")
warnings.filterwarnings(action='once')


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
rt_dir = r'/home/hectorb/PARFLOW/PROJECTS/Ara/resolution2020/NCout/'
statsDay =True
#~ statsDay =False
#~ method = 'D'
method = 'H'
manning = 3e-6
slope_mode='KWE'

plot_yr = 2006

""" simu1 """
props1 = {}; props1['rt_dir'] = rt_dir

props1['res']=50

props1['NCdate'] = '2020_08_15'
props1['simuname'] = 'ara50'

props1['NCdate'] = '2020_08_16'
props1['simuname'] = 'ara50_KH20002'

props1['NCdate'] = '2020_08_23'
props1['simuname'] = 'ara50_man'

""" simu2 """
props2 = {}; props2['rt_dir'] = rt_dir
props2['res']=100

props2['NCdate'] = '2020_08_14'
props2['simuname'] = 'ara100'

props2['NCdate'] = '2020_08_16'
props2['simuname'] = 'ara100_KH20002'

props2['NCdate'] = '2020_08_23'
props2['simuname'] = 'ara100_man'


""" simu3 """
props3 = {}; props3['rt_dir'] = rt_dir
props3['res']=250

props3['NCdate'] = '2020_08_13'
props3['simuname'] = 'ara250'

props3['NCdate'] = '2020_08_16'
props3['simuname'] = 'ara250_KH20002'

props3['NCdate'] = '2020_08_16'
props3['simuname'] = 'ara250_KH236'

props3['NCdate'] = '2020_08_17'
props3['simuname'] = 'ara250_man'

props3['NCdate'] = '2020_08_23'
props3['simuname'] = 'ara250_man'

""" simu4 """
props4 = {}; props4['rt_dir'] = rt_dir
props4['res']=500

props4['NCdate'] = '2020_08_13'
props4['simuname'] = 'ara500'

props4['NCdate'] = '2020_08_16'
props4['simuname'] = 'ara500_KH20002'

props4['NCdate'] = '2020_08_16'
props4['simuname'] = 'ara500_KH236'

props4['NCdate'] = '2020_08_17'
props4['simuname'] = 'ara500_man'

props4['NCdate'] = '2020_08_23'
props4['simuname'] = 'ara500_man'

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def get_resolution_dependent_properties(props):
    """ get generic properties depending on simulation resolution"""
    
    NCdate = props['NCdate']
    simuname = props['simuname']
    rt_dir = props['rt_dir']
    if props['res'] == 20:
        props['simdir'] = rt_dir+'ara20/'+simuname+'_'+NCdate
    if props['res'] == 50:
        props['simdir'] = rt_dir+'ara50/'+simuname+'_'+NCdate
    if props['res'] == 100:
        props['simdir'] = rt_dir+'ara100/'+simuname+'_'+NCdate
    if props['res'] == 250:
        props['simdir'] = rt_dir+'ara250/'+simuname+'_'+NCdate
    if props['res'] == 500:
        props['simdir'] = rt_dir+'ara500/'+simuname+'_'+NCdate
    return copy.copy(props)



##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

""" get resolution dependent simulation properties"""
props1 = get_resolution_dependent_properties(props1)
props2 = get_resolution_dependent_properties(props2)
props3 = get_resolution_dependent_properties(props3)
props4 = get_resolution_dependent_properties(props4)

sims=OrderedDict() #ordered dict classes is like a dictionnary (a={}) but preserved the order in which items are assigned
sims['sim1'] = {'props':props1}
sims['sim2'] = {'props':props2}
sims['sim3'] = {'props':props3}
sims['sim4'] = {'props':props4}

for sim, simdata in sims.items(): 
    simdata['D'] = xr.open_dataset(os.sep.join([simdata['props']['simdir'],''.join([simdata['props']['simuname'],'_press.nc'])]),decode_times=False)
    tsim = pd.date_range('1/1/2005 00:00:00', periods=len(simdata['D'].time), freq='D')
    simdata['D'] = simdata['D'].assign_coords(time = pd.to_datetime(tsim)) #for satur and press
    simdata['Dm'] = simdata['D'].where(simdata['D'].mask>0).sel(time=slice('%d-01-01'%plot_yr, '%d-12-20'%plot_yr))
    simdata['tot_thickness']= np.array(simdata['D'].var_dz[::-1,0,0]*2).cumsum()[-1] - simdata['D'].var_dz[0,0,0]*2/2
    sims[sim] = simdata

figname_template='%dsims'%len(sims)
for sim, simdata in sims.items(): 
    figname_template=figname_template+'_%s'%simdata['props']['simuname']
outdirs = []
for sim, simdata in sims.items(): 
    outdir = os.sep.join([simdata['props']['simdir'],'compare_'+figname_template])
    simdata['props']['outdir'] = outdir
    outdirs.append(outdir)
    try: os.mkdir(outdir)
    except: print('directory already exist: %s'%outdir)

"""Plot maps: mean WTD"""
fig,ax = plt.subplots(int(np.ceil(len(sims)/2)),2,figsize = [7,7],sharex=True,sharey=True)
i=0;j=0
for sim, simdata in sims.items(): 
    p=(simdata['tot_thickness'] - simdata['Dm'].succion[:,0,:,:].mean(dim='time')).plot(ax=ax[j][i],robust=True,vmin=0,vmax=15,add_labels=False,add_colorbar=False)
    ax[j][i].set_title(simdata['props']['simuname'],loc='left',fontdict={'fontsize':14, 'fontweight':'bold'})
    ax[j][i].set_xticklabels('');ax[j][i].set_yticklabels('');ax[j][i].set_aspect(1)    
    if i>=1: j+=1; i=-1;        
    i+=1

#control colorbar
cb = fig.colorbar(p, ax=ax.ravel().tolist())
cb.set_label(label="WTD (m)",size=18)
fig.canvas.draw(); cb.ax.set_yticklabels([a.get_text() for a in cb.ax.get_yticklabels()],fontsize = 18)
#or:
#~ cbar_ax = fig.add_axes([0.15, 0.05, 0.7, 0.05])
#~ fig.colorbar(ax1, cax=cbar_ax,orientation='horizontal',label="WTD (m)")
for outdir in outdirs:
    plt.savefig(os.sep.join([outdir,'WTD_%s'%(figname_template)+'.png']),dpi=400,format='png')



"""Plot maps: amplitude WTD"""
fig,ax = plt.subplots(int(np.ceil(len(sims)/2)),2,figsize = [7,7],sharex=True,sharey=True)
i=0;j=0
for sim, simdata in sims.items(): 
    p = (simdata['Dm'].succion[:,0,:,:].max(dim='time') - simdata['Dm'].succion[:,0,:,:].min(dim='time')).plot(ax=ax[j][i],robust=True,vmin=0,vmax=8,add_labels=False,add_colorbar=False)
    ax[j][i].set_title(simdata['props']['simuname'],loc='left',fontdict={'fontsize':14, 'fontweight':'bold'})
    ax[j][i].set_xticklabels('');ax[j][i].set_yticklabels('');ax[j][i].set_aspect(1)    
    if i>=1: j+=1; i=-1;        
    i+=1

#control colorbar
cb = fig.colorbar(p, ax=ax.ravel().tolist())
cb.set_label(label="WT amplitude (m)",size=18)
fig.canvas.draw(); cb.ax.set_yticklabels([a.get_text() for a in cb.ax.get_yticklabels()],fontsize = 18)
#or:
#~ cbar_ax = fig.add_axes([0.15, 0.05, 0.7, 0.05])
#~ fig.colorbar(ax1, cax=cbar_ax,orientation='horizontal',label="WTD (m)")
for outdir in outdirs:
    plt.savefig(os.sep.join([outdir,'WT_amp_%s'%(figname_template)+'.png']),dpi=400,format='png')


"""Plot scatters: WT amplitude vs WTD"""
# find axes ranges
xmax = np.max([simdata['tot_thickness'] for sim, simdata in sims.items()])
ymax = xmax/2
fig,ax = plt.subplots(int(np.ceil(len(sims)/2)),2,figsize = [7,7])
i=0
j=0
for sim, simdata in sims.items(): 
    amp = (simdata['Dm'].succion[:,0,:,:].max(dim='time') - simdata['Dm'].succion[:,0,:,:].min(dim='time'))
    depth = (simdata['tot_thickness'] - simdata['Dm'].succion[:,0,:,:].mean(dim='time'))
    l1=ax[j][i].scatter(depth.where(simdata['Dm'].veg_map==7,np.nan),amp.where(simdata['Dm'].veg_map==7,np.nan),c='g',s=0.1)#'Trees'
    l2=ax[j][i].scatter(depth.where(simdata['Dm'].veg_map==8,np.nan),amp.where(simdata['Dm'].veg_map==8,np.nan),c='y',s=0.1)#'Open Savanna'
    l3=ax[j][i].scatter(depth.where(simdata['Dm'].veg_map==19,np.nan),amp.where(simdata['Dm'].veg_map==19,np.nan),c='r',s=0.1)#'Herbaceous'
    ax[j][i].set_xlim([0,xmax])
    ax[j][i].set_ylim([0,ymax])
    ax[j][i].legend(['Trees','Open Savannah','Herbaceous'])
    ax[j][i].set_xlabel('WTD (m)',fontsize=14)
    ax[j][i].set_ylabel('amplitude (m)',fontsize=14)
    ax[j][i].set_title(simdata['props']['simuname'],loc='left',fontdict={'fontsize':14, 'fontweight':'bold'})
    
    if i>=1: j+=1; i=-1;        
    i+=1

plt.gcf().subplots_adjust(bottom=0.07, top =0.95,left=0.07,right=0.98,hspace=0.3,wspace=0.2)
for outdir in outdirs:
    plt.savefig(os.sep.join([outdir,'scat WT_amp_vs_WTD_%s'%(figname_template)+'.png']),dpi=400,format='png')


"""Plot hists"""

fig,ax = plt.subplots(1,1,figsize = [7,7])
for sim, simdata in sims.items(): 
    (simdata['tot_thickness'] - simdata['Dm'].succion[:,0,:,:].mean(dim='time')).plot.hist(ax=ax,bins=np.arange(0,5,0.2),density=True,histtype='step',label=simdata['props']['simuname'])   

    plt.legend()
    ax.set_xlabel('WTD (m)',fontsize=14)
    ax.set_ylabel('frequency',fontsize=14)

for outdir in outdirs:
    plt.savefig(os.sep.join([outdir,'hist_WTD_%s'%(figname_template)+'.png']),dpi=400,format='png')

"""Plot hists amplitude"""

fig,ax = plt.subplots(1,1,figsize = [7,7])

for sim, simdata in sims.items(): 
    (simdata['Dm'].succion[:,0,:,:].max(dim='time') - simdata['Dm'].succion[:,0,:,:].min(dim='time')).plot.hist(ax=ax,bins=np.arange(0,5,0.2),density=True,histtype='step',label=simdata['props']['simuname'])   

    plt.legend()
    ax.set_xlabel('WT amplitude (m)',fontsize=14)
    ax.set_ylabel('frequency',fontsize=14)
for outdir in outdirs:
    plt.savefig(os.sep.join([outdir,'hist_WTD_%s'%(figname_template)+'.png']),dpi=400,format='png')

