#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Prepare vegetation map for Ara

    basically resample the cropped 20m DONGA LULC from I. Zin. 
    
    
    options to convert source classes to destination PF classes:
    this can be done before the interpolation (option #1) or after 
    (option #2), and changes significantly the statistics. 
    An option to correct the stats a posteriori is implemented, based on 
    a ranking of the stats used in each aggregate from the original 
    resolution (eg stats on 25 cells from 20 to 100m resolution change)
    As the ranking is drawn during the interpolation, so far it's much 
    easier to work on preconverted classes (option 1). 
    

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os,glob,copy
from netCDF4 import Dataset
import time
import datetime
import pyproj
import rasterio
import scipy.spatial as ann
from matplotlib.dates import MonthLocator, DateFormatter

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import drv_vegm_io as vegio
from PFlibs import io_pfb
from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
from procsycz import procGeodata_Gdal



##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

corr_veg_dist=True

savefigs=False
savefigs=True
savename='LC_respect_stats.png'
#~ savename='LC.png'
write_outputs=True

root_dir = '/home/hectorb/PARFLOW/PROJECTS/Ara/resolution2020/preproc/'

""" If pfb writing is needed:"""
h ={'X0': 343000.0,
 'Y0': 1076500.0,
 'Z0': 0.0,
 'dx': 20.0,
 'dy': 20.0,
 'dz': 2.0,
 'ix': 0,
 'iy': 0,
 'iz': 0,
 'ns': 1,
 'nx': 250,
 'nx2': 250,
 'ny': 250,
 'ny2': 250,
 'nz': 1,
 'nz2': 1,
 'rx': 1,
 'ry': 1,
 'rz': 1}

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def ann_tree(x, y, X, Y, nb=1):
    # generic tree function for a list of points (can originate from a structured grid or not)
    tree = ann.KDTree(np.array([(i,j) for i,j in zip(x, y)]))
    # res returns the (one, nb =1) closest (the cell) source point (x,y) from each destination point (X,Y)
    res = tree.query(np.array([(i,j) for i,j in zip(X, Y)]), nb)[1]
    return res


def progress(i, l):
    p = i/l*100
    if (p % 2) <= (100/l):
        print('...%s%%...'%int(p))
        
        
def agregate_raster(lonlonsource,latlatsource,source,lonlontarget,latlattarget,nb_nn):
    """
    aggregate from a lower resolution lonlonsource, latlatsource grid to a lonlontarget,latlattarget grid 
    using nearest neighbors statistics. Defaults is the dominant class. nb_nn is the number of nearest neighbor.
    """
    # Interpôlate on thiessen (voronoi cells), requires first to connect each point to its 2 closest neighbours (tree);
    # This is the triangulation. Then, cells can be viasualized by drawing the bissectrice of each segment.
    # then the interpolation on the destination set of points (or grid) is made by looking onto which cell is
    # each destination point
    # the tree is made to know for each point selected, which are the two closest points, then the 2 closest of
    # each of these two, etc... up to any chosen level.
       
    #flatten makes vector from a matrix
    tree = ann_tree(lonlonsource.flatten(), latlatsource.flatten(), lonlontarget.flatten(), latlattarget.flatten(), nb=nb_nn)

    nx=np.shape(lonlonsource)[1]
    l = len(tree)

    new_data = np.zeros((lonlontarget.size, 1))
    rank_tuples=list(np.zeros((lonlontarget.size, 1)))    
    if nb_nn==1:
        for s, p in enumerate(tree):
            progress(s, l)       
            i0 = p % nx
            j0 = p // nx
            new_data[s] =  source[j0,i0]        
    else:        
        for s, p in enumerate(tree):
            #s is index of the destination grid
            #p is index of the source grid
            progress(s, l)       
            datatmp = np.zeros((nb_nn, 1))
            k=0
            for pp in p: 
                # this is short to go from vector back to matrix using modulo (reste) and integer division
                i0 = pp % nx
                j0 = pp // nx
                datatmp[k] = source[j0,i0]
                k+=1
            
            val,counts=np.unique(datatmp,return_counts=True)
            new_data[s] = val[np.argmax(counts)]
            rank_tuples[s] = (val[np.argmax(counts)],val[0:3],counts[0:3])
    nx=np.shape(lonlontarget)[1]
    ny=np.shape(lonlontarget)[0]
    new_datadata = np.zeros(np.shape(lonlontarget))
    for i in range(ny):
        #~ vegveg[i,:] = np.reshape(veg2[i*nx:(i+1)*nx],(nx,1))
        new_datadata[i,:] = new_data[i*nx:(i+1)*nx].T
        #~ vegveg[i,:] = veg2[i*nx:(i+1)*nx]
    #~ vegveg []

    new_datadata=np.flipud(new_datadata)
    return new_datadata,rank_tuples
    
def aggregate_zins_on_grid(lonlonsource,latlatsource,source,grid_filename,nodata_value=-9999,nb_nn=1,mask_file='',corr_veg_dist=False,refstats=[],write_outputs=False,filename_root="",pfb_header={}):
    """
    meta function of agregate_raster to facilitate multiple aggregation at diffrernet resolution of zin veg data
    """
    grid,lon,lat = procGeodata_Gdal.readRasterWithGdal(grid_filename,nodata_value=nodata_value)
    [lonlon,latlat]=np.meshgrid(lon,lat)
    mask,lon2,lat2 = procGeodata_Gdal.readRasterWithGdal(mask_file,nodata_value=0)
    mask = np.flipud(mask)
    
    # Option 1: convert before interpolation
    source[source==0]=19
    source[source==1]=19 #settlements
    source[source==2]=19 #crops
    source[source==3]=19 #bare
    source[source==4]=19 #fallows
    source[source==5]=8  #shrublands
    source[source==6]=7 #trees
    source[source==7]=7 #trees
    
    
    vegveg,rank_tuples = agregate_raster(lonlonsource,latlatsource,source,lonlon,latlat,nb_nn=nb_nn)

    
    # Option 2: convert after interpolation
    #~ vegveg[vegveg==0]=np.nan
    #~ vegveg[vegveg==1]=19 #settlements
    #~ vegveg[vegveg==2]=19 #crops
    #~ vegveg[vegveg==3]=19 #bare
    #~ vegveg[vegveg==4]=19 #fallows
    #~ vegveg[vegveg==5]=8  #shrublands
    #~ vegveg[vegveg==6]=7 #trees
    #~ vegveg[vegveg==7]=7 #trees
    
    #option 1:
    vegveg[vegveg==0]=np.nan

    
    vegvegPF = copy.copy(vegveg)
    vegvegPF[np.isnan(vegvegPF)]=19
    vegvegPF[mask==0]=18
    
    
    #~ vegvegstats=copy.copy(vegveg)*1.
    vegvegstats=copy.copy(vegvegPF)*1.
    vegvegstats[mask==0]=np.nan

    vzin2,czin2 = np.unique(vegvegstats,return_counts=True)
    stats=(vzin2,czin2[0:3]/sum(czin2[0:3])*100)
    
    if corr_veg_dist:
        nx=np.shape(lonlon)[1]
        ny=np.shape(lonlon)[0]
        rdf = pd.DataFrame(copy.copy(rank_tuples),columns=['val','classes','counts'])           
        vegveg,vegvegPF,stats,rdf = correct_agregate_statistics(rdf = rdf,refstats = refstats,stats=stats,nx=nx,ny=ny,mask=mask)
        
    
    if write_outputs:
        vegveg_towrite = copy.copy(vegvegPF)
        [x2,y2,vegwrite]=vegio.vegveg_to_veg(lonlon,latlat,vegveg_towrite)
        #~ vegio.write_vegm(x2+h['dx']/2,y2+h['dx']/2,vegwrite,filename_root+'.dat')   
        vegio.write_vegm(x2,y2,vegwrite,filename_root+'.dat')    # OFFSET from half a cell not needed done in the function
        io_pfb.PFB_write(np.swapaxes(vegveg_towrite,0,1), h, pfb_fp=filename_root+'.pfb')
    return vegveg,vegvegPF,lonlon,latlat,stats,rank_tuples

def correct_agregate_statistics(rdf,refstats,stats,nx,ny,mask):
    """
    function to post-process agregate statistics when resampling to a 
    lower resolution, in order to get as close as possible to the 
    original (or any other) distribution. The function will iteratively
    change the state of final cells from the category that has too many
    cells wrt the reference to the category that has too few cells wrt 
    the reference. cells that have their state changed are those who 
    have the most 'target category' counts in the agregate statistics 
    but yet not enough to be categorized as such.
    
    inputs :
    - rdf = ranking dataframe. a pd.DataFrame with columns 'val', 
    'classes' and 'counts' and each line is a cell ordered from the 
    raster as in procGeodata_Gdal.readRasterWithGdal:
        - val : cell value (raster category) : int or float
        - classes : list of raster category found in the agregate stats
        - counts: list (same ordering as classes) of counts of subcells 
        found in each cell. 
        Typically classes and counts are obtained with
        classes,counts=np.unique(datatmp,return_counts=True)
    - refstats: target stats: a tuple of arrays: array 1 : classes
        array 2: percent occurence
    - stats : idem but the initial of the dataset to modify
    - nx : x dimension for the equivalent 2D array and the mask
    - ny : y dimension for the equivalent 2D array and the mask
    - mask: a boolean mask of 0 and 1's
     
    """

    ncells=sum(sum(mask))

    #prct diff with respect to ref for each class (sorted: classes 7,8,9)
    diff_prct = refstats[1][np.argsort(refstats[0])[0:3]] - stats[1][np.argsort(stats[0])[0:3]]
    #count diff with respect to ref for each class
    diff_count = np.round(diff_prct/100*ncells) 

    count_loop=0
    while sum(abs(diff_count))>len(diff_count):
        print('loop n° %d'%count_loop)
        #corresponding class
        class_to_fill = np.sort(stats[0])[np.argmax(diff_count)]   # necessarily positive  -or equal-  (one class has to lack counts wrt reference)
        class_to_empty = np.sort(stats[0])[np.argsort(diff_count)[0]] # necessarily negative  -or equal-  (one class has to be in excess of counts wrt reference)

        sub=rdf.loc[rdf.val==class_to_empty,:]  # keep only class_to_empty
        subsub=sub.loc[map(lambda x: True if class_to_fill in x else False, sub.classes),:]   # make sure there are class to fill available in those subsets

        # find the target classes with the most important counts of classes to fill
        subsub['counts_class_to_fill'] = subsub.apply(lambda x: x['counts'][np.argwhere(x['classes']==class_to_fill)][0][0],axis=1)  
        subsubsort = subsub.sort_values(by='counts_class_to_fill',ascending=False)

        nb_cells_to_swap = min([-sorted(diff_count)[0],sorted(diff_count)[-1]])    # either give all cells from the diff or less if abs(diff count) > max(diff_count) (in which case cells shouls also be given to the other class)

        new_vals = subsubsort.loc[subsubsort.val==class_to_empty,:].iloc[0:int(nb_cells_to_swap)] #subset only cells where third class is dominant
        new_vals['val']=class_to_fill # means we only change 'val' column and not counts and classes order (should we?). We could also add a flag
        rdf.loc[new_vals.index]=new_vals.loc[:,['val','classes','counts']]  

        #recalculate stats
        new_datadata = np.zeros([ny,nx])
        for i in range(ny):
            new_datadata[i,:] = np.array(rdf.val[i*nx:(i+1)*nx]).T
        new_datadata=np.flipud(new_datadata)

        new_datadata[new_datadata==0]=np.nan

        new_datadataPF = copy.copy(new_datadata)
        new_datadataPF[np.isnan(new_datadataPF)]=19
        new_datadataPF[mask==0]=18

        vegvegstats=copy.copy(new_datadataPF)*1.
        vegvegstats[mask==0]=np.nan
        vzin2,czin2 = np.unique(vegvegstats,return_counts=True)
        stats=(vzin2,czin2[0:3]/sum(czin2[0:3])*100)
        print('stats: \n')
        print(stats)

        #prct diff with respect to ref for each class (sorted: classes 7,8,9)
        diff_prct = refstats[1][np.argsort(refstats[0])[0:3]] - stats[1][np.argsort(stats[0])[0:3]]
        #count diff with respect to ref for each class
        diff_count = np.round(diff_prct/100*ncells) 
        count_loop+=1
        
    return new_datadata,new_datadataPF,stats,copy.copy(rdf)


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

veg,lonveg,latveg = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Ara/resolution2020/preproc/VEG/Data/SUPER_DONGA_LANDCOVER_20m_2005-2006_ara.tif",nodata_value = 0)
[lonlonveg,latlatveg]=np.meshgrid(lonveg,latveg)

# get stats:
vegt = veg*1. 
vegt[vegt==0]=np.nan
vegt[vegt==1]=19 #settlements
vegt[vegt==2]=19 #crops
vegt[vegt==3]=19 #bare
vegt[vegt==4]=19 #fallows
vegt[vegt==5]=8  #shrublands
vegt[vegt==6]=7 #trees
vegt[vegt==7]=7 #trees

vzin,czin = np.unique(vegt,return_counts=True) 
totzin = np.sum(czin[0:3]) 
statsref=(vzin,czin[0:3]/totzin *100)



#20m
filename=root_dir+"DEM/DEM_20m/elv_merit_Ara_20m_precarved_twice.tif"
mask_file=root_dir+"DEM/priorityflow_outputs_20m/ara20_pc_twice_first_pass_mask.out.tif"
out_filename=root_dir+"VEG/VEG_20m/drvvegm_zin_20m"
nn=250;dxy=20;h['X0']=343010.0;h['Y0']=1076510.0;h['nx']=nn;h['nx2']=nn;h['ny']=nn;h['ny2']=nn;h['dx']=dxy;h["dy"]=dxy
vegveg20,vegvegPF20,lonlon20,latlat20,stats20,rt20 = aggregate_zins_on_grid(lonlonveg,latlatveg,copy.copy(veg),grid_filename=filename,nodata_value=65535,
nb_nn=1,mask_file=mask_file,corr_veg_dist=False,refstats=[],write_outputs=write_outputs,filename_root=out_filename,pfb_header=h)
#~ vegio.read_vegm(out_filename+'.dat',plot=True,cmap = plt.cm.RdYlGn_r)
d,xx,yy,fig,ax,cb=io_pfb.plot_2D_PFB(out_filename+'.pfb',zlayer=0,logscale=False,colorlabel='LC classes',title='Land Cover',savefig=savefigs,savedir=root_dir+"VEG/VEG_20m",savename=savename,width=6.7,height=3.75,cmap='YlGn_r')

#50m
filename=root_dir+"DEM/DEM_50m/elv_merit_Ara_50m_cubicspline_precarved_twice.tif"
mask_file=root_dir+"DEM/priorityflow_outputs_50m/ara50_pc_cs_twice_first_pass_mask.out.tif"
out_filename=root_dir+"VEG/VEG_50m/drvvegm_zin_50m"
nn=100;dxy=50;h['X0']=343025.0;h['Y0']=1076525.0;h['nx']=nn;h['nx2']=nn;h['ny']=nn;h['ny2']=nn;h['dx']=dxy;h["dy"]=dxy
vegveg50,vegvegPF50,lonlon50,latlat50,stats50,rt50  = aggregate_zins_on_grid(lonlonveg,latlatveg,copy.copy(veg),grid_filename=filename,nodata_value=65535,
nb_nn=9,mask_file=mask_file,corr_veg_dist=corr_veg_dist,refstats=stats20,write_outputs=write_outputs,filename_root=out_filename,pfb_header=h)
#~ vegio.read_vegm(out_filename+'.dat',plot=True,cmap = plt.cm.RdYlGn_r)
d,xx,yy,fig,ax,cb=io_pfb.plot_2D_PFB(out_filename+'.pfb',zlayer=0,logscale=False,colorlabel='LC classes',title='Land Cover',savefig=savefigs,savedir=root_dir+"VEG/VEG_50m",savename=savename,width=6.7,height=3.75,cmap='YlGn_r')

#100m
filename=root_dir+"DEM/DEM_100m/elv_merit_Ara_100m_cubicspline_precarved_twice.tif"
mask_file=root_dir+"DEM/priorityflow_outputs_100m/ara100_pc_cs_twice_first_pass_mask.out.tif"
out_filename=root_dir+"VEG/VEG_100m/drvvegm_zin_100m"
nn=50;dxy=100;h['X0']=343050.0;h['Y0']=1076550.0;h['nx']=nn;h['nx2']=nn;h['ny']=nn;h['ny2']=nn;h['dx']=dxy;h["dy"]=dxy
vegveg100,vegvegPF100,lonlon100,latlat100,stats100,rt100 = aggregate_zins_on_grid(lonlonveg,latlatveg,copy.copy(veg),grid_filename=filename,nodata_value=65535,
nb_nn=25,mask_file=mask_file,corr_veg_dist=corr_veg_dist,refstats=stats20,write_outputs=write_outputs,filename_root=out_filename,pfb_header=h)
#~ vegio.read_vegm(out_filename+'.dat',plot=True,cmap = plt.cm.RdYlGn_r)
d,xx,yy,fig,ax,cb=io_pfb.plot_2D_PFB(out_filename+'.pfb',zlayer=0,logscale=False,colorlabel='LC classes',title='Land Cover',savefig=savefigs,savedir=root_dir+"VEG/VEG_100m",savename=savename,width=6.7,height=3.75,cmap='YlGn_r')

#250m
filename=root_dir+"DEM/DEM_250m/elv_merit_Ara_250m_cubicspline_precarved_agreg_min_twice.tif"
mask_file=root_dir+"DEM/priorityflow_outputs_250m/ara250_pc_twice_first_pass_mask.out.tif"  
out_filename=root_dir+"VEG/VEG_250m/drvvegm_zin_250m"
nn=20;dxy=250;h['X0']=343125.0;h['Y0']=1076625.0;h['nx']=nn;h['nx2']=nn;h['ny']=nn;h['ny2']=nn;h['dx']=dxy;h["dy"]=dxy
vegveg250,vegvegPF250,lonlon250,latlat250,stats250,rt250 = aggregate_zins_on_grid(lonlonveg,latlatveg,copy.copy(veg),grid_filename=filename,nodata_value=65535,
nb_nn=225,mask_file=mask_file,corr_veg_dist=corr_veg_dist,refstats=stats20,write_outputs=write_outputs,filename_root=out_filename,pfb_header=h)
#~ vegio.read_vegm(out_filename+'.dat',plot=True,cmap = plt.cm.RdYlGn_r)
d,xx,yy,fig,ax,cb=io_pfb.plot_2D_PFB(out_filename+'.pfb',zlayer=0,logscale=False,colorlabel='LC classes',title='Land Cover',savefig=savefigs,savedir=root_dir+"VEG/VEG_250m",savename=savename,width=6.7,height=3.75,cmap='YlGn_r')

#500m
filename=root_dir+"DEM/DEM_500m/elv_merit_Ara_500m_cubicspline_precarved_agreg_min_twice.tif"
mask_file=root_dir+"DEM/priorityflow_outputs_500m/ara500_pc_twice_first_pass_mask.out.tif"  
out_filename=root_dir+"VEG/VEG_500m/drvvegm_zin_500m"
nn=10;dxy=500;h['X0']=343250.0;h['Y0']=1076750.0;h['nx']=nn;h['nx2']=nn;h['ny']=nn;h['ny2']=nn;h['dx']=dxy;h["dy"]=dxy
vegveg500,vegvegPF500,lonlon500,latlat500,stats500,rt500 = aggregate_zins_on_grid(lonlonveg,latlatveg,copy.copy(veg),grid_filename=filename,nodata_value=65535,
nb_nn=625,mask_file=mask_file,corr_veg_dist=corr_veg_dist,refstats=stats20,write_outputs=write_outputs,filename_root=out_filename,pfb_header=h)
#~ vegio.read_vegm(out_filename+'.dat',plot=True,cmap = plt.cm.RdYlGn_r)
d,xx,yy,fig,ax,cb=io_pfb.plot_2D_PFB(out_filename+'.pfb',zlayer=0,logscale=False,colorlabel='LC classes',title='Land Cover',savefig=savefigs,savedir=root_dir+"VEG/VEG_500m",savename=savename,width=6.7,height=3.75,cmap='YlGn_r')

#1000m the watershed mask is not good yet
filename=root_dir+"DEM/DEM_1000m/elv_merit_Ara_1000m_cubicspline_precarved_agreg_min_twice.tif"
out_filename=root_dir+"VEG/VEG_1000m/drvvegm_zin_1000m"
nn=5;dxy=1000;h['X0']=343500.0;h['Y0']=1077000.0;h['nx']=nn;h['nx2']=nn;h['ny']=nn;h['ny2']=nn;h['dx']=dxy;h["dy"]=dxy
#~ vegveg1000,vegvegPF1000,lonlon1000,latlat1000,stats1000 = aggregate_zins_on_grid(lonlonveg,latlatveg,copy.copy(veg),grid_filename=filename,nodata_value=65535,nb_nn=2500,write_outputs=True,filename_root=out_filename,pfb_header=h)
#~ vegio.read_vegm(out_filename+'.dat',plot=True,cmap = plt.cm.RdYlGn_r)
#~ d,xx,yy,fig,ax,cb=io_pfb.plot_2D_PFB(out_filename+'.pfb',zlayer=0,logscale=False,colorlabel='LC classes',title='Land Cover',savefig=savefigs,savedir=root_dir+"",savename=savename,width=6.7,height=3.75,cmap='YlGn_r')








###### try adapt 50m resolution stats to 20m
""" the following works for as many classses as needed
#~ mask_file=root_dir+"priorityflow_outputs_50m/ara50_pc_cs_twice_first_pass_mask.out.tif"
#~ mask,lon2,lat2 = procGeodata_Gdal.readRasterWithGdal(mask_file,nodata_value=0)
mask_file=root_dir+"priorityflow_outputs_100m/ara100_pc_cs_twice_first_pass_mask.out.tif"
mask,lon2,lat2 = procGeodata_Gdal.readRasterWithGdal(mask_file,nodata_value=0)
mask = np.flipud(mask)
#~ rt = pd.DataFrame(copy.copy(rt50),columns=['val','classes','counts'])   
#~ rtb = pd.DataFrame(copy.copy(rt50),columns=['val','classes','counts'])   
rt = pd.DataFrame(copy.copy(rt100),columns=['val','classes','counts'])   
rtb = pd.DataFrame(copy.copy(rt100),columns=['val','classes','counts'])   
#~ rt50b=rt50
#~ lookup_table = {0:np.nan,1:19,2:19,3:19,4:19,5:8,6:9}      
#~ rt50b = [(np.array([lookup_table.get(x, x) for x in t[0]]),t[1]) for t in rt50b]
ncells=sum(sum(mask))

#prct diff with respect to ref for each class (sorted: classes 7,8,9)
#~ diff_prct = stats20[1][np.argsort(stats20[0])[0:3]] - stats50[1][np.argsort(stats50[0])[0:3]]
diff_prct = stats20[1][np.argsort(stats20[0])[0:3]] - stats100[1][np.argsort(stats100[0])[0:3]]
#count diff with respect to ref for each class
diff_count = np.round(diff_prct/100*ncells) 

count_loop=0
stats=stats50
while sum(abs(diff_count))>len(diff_count):
    print('loop n° %d'%count_loop)
    #corresponding class
    class_to_fill = np.sort(stats[0])[np.argmax(diff_count)]   # necessarily positive  -or equal-  (one class has to lack counts wrt reference)
    class_to_empty = np.sort(stats[0])[np.argsort(diff_count)[0]] # necessarily negative  -or equal-  (one class has to be in excess of counts wrt reference)

    sub=rtb.loc[rtb.val==class_to_empty,:]  # keep only class_to_empty
    subsub=sub.loc[map(lambda x: True if class_to_fill in x else False, sub.classes),:]   # make sure there are class to fill available in those subsets

    # find the target classes with the most important counts of classes to fill
    subsub['counts_class_to_fill'] = subsub.apply(lambda x: x['counts'][np.argwhere(x['classes']==class_to_fill)][0][0],axis=1)  
    subsubsort = subsub.sort_values(by='counts_class_to_fill',ascending=False)

    nb_cells_to_swap = min([-sorted(diff_count)[0],sorted(diff_count)[-1]])    # either give all cells from the diff or less if abs(diff count) > max(diff_count) (in which case cells shouls also be given to the other class)

    new_vals = subsubsort.loc[subsubsort.val==class_to_empty,:].iloc[0:int(nb_cells_to_swap)] #subset only cells where third class is dominant
    new_vals['val']=class_to_fill # means we only change 'val' column and not counts and classes order (should we?). We could also add a flag
    rtb.loc[new_vals.index]=new_vals.loc[:,['val','classes','counts']]  

    #recalculate stats
    #~ nx=np.shape(lonlon50)[1]
    #~ ny=np.shape(lonlon50)[0]
    #~ new_datadata = np.zeros(np.shape(lonlon50))
    nx=np.shape(lonlon100)[1]
    ny=np.shape(lonlon100)[0]
    new_datadata = np.zeros(np.shape(lonlon100))
    for i in range(ny):
        new_datadata[i,:] = np.array(rtb.val[i*nx:(i+1)*nx]).T
    new_datadata=np.flipud(new_datadata)

    new_datadata[new_datadata==0]=np.nan

    new_datadataPF = copy.copy(new_datadata)
    new_datadataPF[np.isnan(new_datadataPF)]=19
    new_datadataPF[mask==0]=18

    vegvegstats=copy.copy(new_datadataPF)*1.
    vegvegstats[mask==0]=np.nan
    vzin2,czin2 = np.unique(vegvegstats,return_counts=True)
    stats=(vzin2,czin2[0:3]/sum(czin2[0:3])*100)
    print('stats: \n')
    print(stats)

    #prct diff with respect to ref for each class (sorted: classes 7,8,9)
    diff_prct = stats20[1][np.argsort(stats20[0])[0:3]] - stats[1][np.argsort(stats[0])[0:3]]
    #count diff with respect to ref for each class
    diff_count = np.round(diff_prct/100*ncells) 
    count_loop+=1
    
"""


""" the following works for 3 classes
#~ mask_file=root_dir+"priorityflow_outputs_50m/ara50_pc_cs_twice_first_pass_mask.out.tif"
#~ mask,lon2,lat2 = procGeodata_Gdal.readRasterWithGdal(mask_file,nodata_value=0)
mask_file=root_dir+"priorityflow_outputs_100m/ara100_pc_cs_twice_first_pass_mask.out.tif"
mask,lon2,lat2 = procGeodata_Gdal.readRasterWithGdal(mask_file,nodata_value=0)
mask = np.flipud(mask)
#~ rt = pd.DataFrame(copy.copy(rt50),columns=['val','classes','counts'])   
#~ rtb = pd.DataFrame(copy.copy(rt50),columns=['val','classes','counts'])   
rt = pd.DataFrame(copy.copy(rt100),columns=['val','classes','counts'])   
rtb = pd.DataFrame(copy.copy(rt100),columns=['val','classes','counts'])   
#~ rt50b=rt50
#~ lookup_table = {0:np.nan,1:19,2:19,3:19,4:19,5:8,6:9}      
#~ rt50b = [(np.array([lookup_table.get(x, x) for x in t[0]]),t[1]) for t in rt50b]
ncells=sum(sum(mask))

#prct diff with respect to ref for each class (sorted: classes 7,8,9)
diff_prct = stats20[1][np.argsort(stats20[0])[0:3]] - stats50[1][np.argsort(stats50[0])[0:3]]
#count diff with respect to ref for each class
diff_count = np.round(diff_prct/100*ncells) 
#corresponding class
class_to_fill = np.sort(stats50[0])[np.argmax(diff_count)]   # necessarily positive  -or equal-  (one class has to lack counts wrt reference)
second_class = np.sort(stats50[0])[np.argsort(diff_count)[-2]]   # positive or negative count    
third_class = np.sort(stats50[0])[np.argsort(diff_count)[-3]]# necessarily negative  -or equal-  (one class has to be in excess of counts wrt reference)

sub=rtb.loc[(rtb.val==second_class) | (rtb.val==third_class),:]  # keep only classes 2 & 3 to decide which to swap
subsub=sub.loc[map(lambda x: True if class_to_fill in x else False, sub.classes),:]   # make sure there are class to fill available in those subsets

# find the target classes with the most important counts of classes to fill
subsub['counts_class_to_fill'] = subsub.apply(lambda x: x['counts'][np.argwhere(x['classes']==class_to_fill)][0][0],axis=1)  
subsubsort = subsub.sort_values(by='counts_class_to_fill',ascending=False)

#Starting HERE: now this seems specific to when there's only 3 classes !! (the rest shouldn't be?)
# start by the lowest diff count: it HAS to be negative and the one with the most cells to give

nb_cells_to_swap = min([-sorted(diff_count)[0],sorted(diff_count)[-1]])    # either give all cells from the diff or less if abs(diff count) > max(diff_count) (in which case cells shouls also be given to the other class)

new_vals = subsubsort.loc[subsubsort.val==third_class,:].iloc[0:int(nb_cells_to_swap)] #subset only cells where third class is dominant
new_vals['val']=class_to_fill # means we only change 'val' column and not counts and classes order (should we?). We could also add a flag
rtb.loc[new_vals.index]=new_vals.loc[:,['val','classes','counts']]  

#give the remaining cells (if any) to the other class
nb_cells_to_swap2 =  np.max([nb_cells_to_swap - max(diff_count) ,0])  
sub2=rtb.loc[rtb.val==third_class,:]  
subsub2=sub2.loc[map(lambda x: True if second_class in x else False, sub2.classes),:]   #maybe this line's not needed
subsub2['counts_class_to_fill'] = subsub2.apply(lambda x: x['counts'][np.argwhere(x['classes']==second_class)][0][0],axis=1)  
subsubsort2 = subsub2.sort_values(by='counts_class_to_fill',ascending=False)

new_vals2 = subsubsort2.loc[subsubsort2.val==third_class,:].iloc[0:int(nb_cells_to_swap2)]
new_vals2['val']=second_class # means we only change 'val' column and not counts and classes order (should we?). We could also add a flag
rtb.loc[new_vals2.index]=new_vals2.loc[:,['val','classes','counts']]  

# Else give the remaining cells from the other class to the class to fill
nb_cells_to_swap3 =  max(diff_count) - nb_cells_to_swap  
sub3=rtb.loc[rtb.val==second_class,:]  
subsub3=sub3.loc[map(lambda x: True if class_to_fill in x else False, sub3.classes),:]   
subsub3['counts_class_to_fill'] = subsub3.apply(lambda x: x['counts'][np.argwhere(x['classes']==class_to_fill)][0][0],axis=1)  
subsubsort3 = subsub3.sort_values(by='counts_class_to_fill',ascending=False)

new_vals3 = subsubsort3.loc[subsubsort3.val==second_class,:].iloc[0:int(nb_cells_to_swap3)]
new_vals3['val']=class_to_fill # means we only change 'val' column and not counts and classes order (should we?). We could also add a flag
rtb.loc[new_vals3.index]=new_vals3.loc[:,['val','classes','counts']]  

nx=np.shape(lonlon50)[1]
ny=np.shape(lonlon50)[0]
new_datadata = np.zeros(np.shape(lonlon50))
for i in range(ny):
    new_datadata[i,:] = np.array(rtb.val[i*nx:(i+1)*nx]).T
new_datadata=np.flipud(new_datadata)

new_datadata[new_datadata==0]=np.nan

new_datadataPF = copy.copy(new_datadata)
new_datadataPF[np.isnan(new_datadataPF)]=19
new_datadataPF[mask==0]=18

vegvegstats=copy.copy(new_datadataPF)*1.
vegvegstats[mask==0]=np.nan
vzin2,czin2 = np.unique(vegvegstats,return_counts=True)
stats=(vzin2,czin2[0:3]/sum(czin2[0:3])*100)

"""

#~ filename="/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/vegmap_tap_tmp_python.pfb"
#~ io_pfb.PFB_write(np.swapaxes(vegveg_towrite,0,1), h, pfb_fp=filename)
#~ d,xx,yy,fig,ax,cb=io_pfb.plot_2D_PFB(filename,zlayer=0,logscale=False,colorlabel='LC classes',title='Land Cover',savefig=False,savename='LCC.png',width=6.7,height=3.75,cmap='YlGn_r') 


#~ filename="/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/veg_map_dahu.pfb"
#~ d,xx,yy,fig,ax,cb=io_pfb.plot_2D_PFB(filename,zlayer=0,logscale=False,colorlabel='LC classes',title='Land Cover',savefig=False,savename='LCC.png',width=6.7,height=3.75,cmap='YlGn_r') 



