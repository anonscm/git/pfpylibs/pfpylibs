# coding: utf-8
"""
    PFPYLIBS - IOSCRIPT - Ara

    Prepare the domain for various spatial resolution
    for experiments starting in 2020

    in August 2019: change the vardz 
    

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2019"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import rasterio
import pyproj
import matplotlib.pyplot as plt
import scipy.spatial as ann
import copy

"""local imports:"""
import kolinkelly as kk
import joestork as js
import PFlibs
from PFlibs import io_pfb
from PFlibs import drv_vegm_io as vegio
from procsycz import readDataAMMA as rdA
from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
from procsycz import procGeodata_Gdal
#kolinkelly is a module from waterbox that includes functionalities for NC manipulation


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

resolution = 20
#~ resolution = 50
resolution = 100
resolution = 250
resolution = 500
resolution = 1000
dz = 2.0


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
root_dir = '/home/hectorb/PARFLOW/PROJECTS/Ara/resolution2020/preproc/DEM/'

#### Open DEM:
#~ with rasterio.open("/home/hectorb/PARFLOW/PROJECTS/Ara/Ara/input/data/DEM/ara_MNT_100m_extent_342000_348400_1076000_1082400_mean_agreg_D4_pitfilled.tif") as src:
if resolution == 20: 
    dem,londem,latdem = procGeodata_Gdal.readRasterWithGdal(root_dir+"/DEM_20m/elv_merit_Ara_20m_precarved_twice.tif",nodata_value = 65535)
    slopex,h = io_pfb.PFB_read(root_dir+'priorityflow_outputs_20m/ara20_pc_twice_carved_1m_slopex.pfb',1)
    slopey,h = io_pfb.PFB_read(root_dir+'priorityflow_outputs_20m/ara20_pc_twice_carved_1m_slopey.pfb',1)    
    x0 = 343010.0
    y0 = 1076510.0
if resolution == 50: 
    dem,londem,latdem = procGeodata_Gdal.readRasterWithGdal(root_dir+"/DEM_50m/elv_merit_Ara_50m_cubicspline_precarved_twice.tif",nodata_value = 65535)
    slopex,h = io_pfb.PFB_read(root_dir+'priorityflow_outputs_50m/ara50_cs_pc_twice_carved_1m_slopex.pfb',1)
    slopey,h = io_pfb.PFB_read(root_dir+'priorityflow_outputs_50m/ara50_cs_pc_twice_carved_1m_slopey.pfb',1)       
    x0 = 343025.0
    y0 = 1076525.0
if resolution == 100: 
    dem,londem,latdem = procGeodata_Gdal.readRasterWithGdal(root_dir+"/DEM_100m/elv_merit_Ara_100m_cubicspline_precarved_twice.tif",nodata_value = 65535)
    slopex,h = io_pfb.PFB_read(root_dir+'priorityflow_outputs_100m/ara100_cs_pc_twice_carved_1m_slopex.pfb',1)
    slopey,h = io_pfb.PFB_read(root_dir+'priorityflow_outputs_100m/ara100_cs_pc_twice_carved_1m_slopey.pfb',1)      
    x0 = 343050.0
    y0 = 1076550.0
if resolution == 250: 
    dem,londem,latdem = procGeodata_Gdal.readRasterWithGdal(root_dir+"/DEM_250m/elv_merit_Ara_250m_cubicspline_precarved_agreg_min_twice.tif",nodata_value = 65535)
    slopex,h = io_pfb.PFB_read(root_dir+'priorityflow_outputs_250m/ara250_pc_twice_carved_1m_slopex.pfb',1)
    slopey,h = io_pfb.PFB_read(root_dir+'priorityflow_outputs_250m/ara250_pc_twice_carved_1m_slopey.pfb',1)    
    x0 = 343125.0
    y0 = 1076625.0
if resolution == 500: 
    dem,londem,latdem = procGeodata_Gdal.readRasterWithGdal(root_dir+"/DEM_500m/elv_merit_Ara_500m_cubicspline_precarved_agreg_min_twice.tif",nodata_value = 65535)
    slopex,h = io_pfb.PFB_read(root_dir+'priorityflow_outputs_500m/ara500_pc_twice_carved_1m_slopex.pfb',1)
    slopey,h = io_pfb.PFB_read(root_dir+'priorityflow_outputs_500m/ara500_pc_twice_carved_1m_slopey.pfb',1)      
    x0 = 343250.0
    y0 = 1076750.0
[lonlondem,latlatdem]=np.meshgrid(londem,latdem)
#~ dem = dem[::-1,:]
#~ dem = dem[::-1,:]
#strange this is needed here: (2020)
dem = np.flipud(dem)
dem=np.swapaxes(dem,0,1)
dem[dem<0]=0


""" Prepare var dz :"""
#~ dz = 3.0
dz = 2.0
#~ v = np.array([0.09,0.39,0.75,0.75,1.2,1.8,12.9])
#~ vardz = v/dz 
#~ vardz3D = np.tile(vardz,(len(londem),len(latdem),7))
#08/2019:
#~ v = np.array([0.08,0.32,0.08,0.08,0.32,0.64,0.88,0.32,0.32,0.64,0.64,1.2,1.8,3.6,19.08])
#2020
v = np.array([0.08,0.32,0.08,0.08,0.32,0.64,0.88,0.32,0.32,0.64,1.2,1.8,3.6,5.4,8.32])
vardz = v/dz 
vardz3D = np.tile(vardz,(len(londem),len(latdem),len(v)))

""" If pfb writing is needed:"""
#~ x0 = 343000.0
#~ y0 = 1076500.0

hdr ={'X0': x0,
 'Y0': y0,
 'Z0': 0.0,
 'dx': resolution,
 'dy': resolution,
 'dz': dz,
 'ix': 0,
 'iy': 0,
 'iz': 0,
 'ns': 1,
 'nx': len(londem),
 'nx2': len(londem),
 'ny': len(latdem),
 'ny2': len(latdem),
 'nz': len(v),
 'nz2': len(v),
 'rx': 1,
 'ry': 1,
 'rz': 1}

hdr2D=copy.copy(hdr)
hdr2D['nz'] = 1
hdr2D['nz2'] = 1

io_pfb.PFB_write(vardz3D[:,:,::-1], hdr, pfb_fp=root_dir+"DEM_%dm/vardz_Ara%d.pfb"%(resolution,resolution))
io_pfb.PFB_write(slopex[:,:,::-1], hdr2D, pfb_fp=root_dir+"DEM_%dm/slopex_Ara%d.pfb"%(resolution,resolution))
io_pfb.PFB_write(slopey[:,:,::-1], hdr2D, pfb_fp=root_dir+"DEM_%dm/slopey_Ara%d.pfb"%(resolution,resolution))


""" Prepare dgridz :"""

zloc = np.cumsum(vardz*dz) - dz*vardz/2 #center of cells
final_grid = dem[:,:,None] - zloc[None,None,:]
initial_grid = np.zeros(np.shape(dem))[:,:,None] + np.arange(v.shape[0]-1,-1,-1)
dgridz = final_grid-initial_grid
dgridz  = np.swapaxes(dgridz,0,1)
io_pfb.PFB_write(dgridz[:,:,::-1], hdr, pfb_fp=root_dir+"DEM_%dm/dgridz_Ara%d.pfb"%(resolution,resolution))

""" Prepare dem :"""
hdr2D = copy.copy(hdr)
hdr2D['nz']=1
hdr2D['nz2']=1
filename = root_dir+"DEM_%dm/dem_Ara%d.pfb"%(resolution,resolution)
io_pfb.PFB_write(dem[:,:,None]*1., hdr2D, pfb_fp=filename)
d,xx,yy,fig,ax,cb=io_pfb.plot_2D_PFB(filename,zlayer=0,logscale=False,colorlabel='(m)',title='Elevation',savefig=True,savedir=root_dir+"DEM_%dm/"%resolution,savename='DEM_%d.png'%resolution,width=6.7,height=3.75,vmin=420,vmax=500) 

#~ v = np.linspace(0,6,7)
#~ orig_grid = np.tile(v,(len(latdem),len(londem),1))
#~ dest_grid = np.zeros(orig_grid.shape)
#~ for i in range(7):
    #~ dest_grid[:,:,i] = dem-v[i]
#~ dgridz = dest_grid-orig_grid
#~ io_pfb.PFB_write(np.swapaxes(dgridz[:,:,::-1],0,1), hdr, pfb_fp="/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/inputs/dgridz2019_Ara%d.pfb"%resolution)


""" Prepare domains: """
#~ d = np.array([1,1,2,2,3,3,3])
d = np.array([1,1,1,2,2,2,2,2,3,3,3,3,3,3,3])
d3D = np.tile(d,(len(londem),len(latdem),1))
PFlibs.io_pfb.PFB_write(d3D[:,:,::-1], hdr, pfb_fp=root_dir+"DEM_%dm/domains_Ara%d.pfb"%(resolution,resolution))



