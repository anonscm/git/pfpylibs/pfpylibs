#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Analyse streamflow outputs for ara simulation:
    compare several simulations with different resolutions

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import datetime
import matplotlib.pyplot as plt
from netCDF4 import Dataset
import os, glob, copy,time
import pyproj
import warnings
from collections import OrderedDict

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb
from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
from procsycz import procGeodata_Gdal
from de import de
from de import kge

plt.close("all")
warnings.filterwarnings(action='once')


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
rt_dir = r'/home/hectorb/PARFLOW/PROJECTS/Ara/resolution2020/NCout/'
statsDay =True
#~ statsDay =False
#~ method = 'D'
method = 'H'
manning = 3e-6
slope_mode='KWE'

""" simu1 """
props1 = {}; props1['rt_dir'] = rt_dir

props1['res']=50

props1['NCdate'] = '2020_08_15'
props1['simuname'] = 'ara50'

props1['NCdate'] = '2020_08_16'
props1['simuname'] = 'ara50_KH20002'

props1['NCdate'] = '2020_08_18'
props1['simuname'] = 'ara50_KH236'

props1['NCdate'] = '2020_08_21'
props1['simuname'] = 'ara50_man_2yrs'

props1['NCdate'] = '2020_08_23'
props1['simuname'] = 'ara50_man'

""" simu2 """
props2 = {}; props2['rt_dir'] = rt_dir
props2['res']=100

props2['NCdate'] = '2020_08_14'
props2['simuname'] = 'ara100'

props2['NCdate'] = '2020_08_16'
props2['simuname'] = 'ara100_KH20002'

props2['NCdate'] = '2020_08_17'
props2['simuname'] = 'ara100_KH236'

props2['NCdate'] = '2020_08_21'
props2['simuname'] = 'ara100_man_3yrs'

props2['NCdate'] = '2020_08_23'
props2['simuname'] = 'ara100_man'


""" simu3 """
props3 = {}; props3['rt_dir'] = rt_dir
props3['res']=250

props3['NCdate'] = '2020_08_13'
props3['simuname'] = 'ara250'

props3['NCdate'] = '2020_08_16'
props3['simuname'] = 'ara250_KH20002'

props3['NCdate'] = '2020_08_17'
props3['simuname'] = 'ara250_KH236'

props3['NCdate'] = '2020_08_21'
props3['simuname'] = 'ara250_man_3yrs'

props3['NCdate'] = '2020_08_23'
props3['simuname'] = 'ara250_man'

""" simu4 """
props4 = {}; props4['rt_dir'] = rt_dir
props4['res']=500

props4['NCdate'] = '2020_08_13'
props4['simuname'] = 'ara500'

props4['NCdate'] = '2020_08_16'
props4['simuname'] = 'ara500_KH20002'

props4['NCdate'] = '2020_08_16'
props4['simuname'] = 'ara500_KH236'

props4['NCdate'] = '2020_08_21'
props4['simuname'] = 'ara500_man_3yrs'

props4['NCdate'] = '2020_08_23'
props4['simuname'] = 'ara500_man'

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def get_resolution_dependent_properties(props):
    """ get generic properties depending on simulation resolution"""
    
    NCdate = props['NCdate']
    simuname = props['simuname']
    rt_dir = props['rt_dir']
    if props['res'] == 20:
        props['simdir'] = rt_dir+'ara20/'+simuname+'_'+NCdate
        props['x_outlet'] = 226-1
        props['y_outlet'] = 91-1
        props['manning'] = 1 # TO CHANGE
    if props['res'] == 50:
        props['simdir'] = rt_dir+'ara50/'+simuname+'_'+NCdate
        props['x_outlet'] = 89-1
        props['y_outlet'] = 36-1
        props['manning'] = 2.2e-5
    if props['res'] == 100:
        props['simdir'] = rt_dir+'ara100/'+simuname+'_'+NCdate
        props['x_outlet'] = 45-1
        props['y_outlet'] = 18-1
        props['manning'] = 1.4e-5
    if props['res'] == 250:
        props['simdir'] = rt_dir+'ara250/'+simuname+'_'+NCdate
        props['x_outlet'] = 18-1
        props['y_outlet'] = 7-1
        props['manning'] = 7.6e-6
    if props['res'] == 500:
        props['simdir'] = rt_dir+'ara500/'+simuname+'_'+NCdate
        props['x_outlet'] = 9-2
        props['y_outlet'] = 4-2
        props['manning'] = 4.8e-6
    return copy.copy(props)
    
def minimalist_xldate_as_datetime(xldate, datemode):
    # datemode: 0 for 1900-based, 1 for 1904-based
    return (
        datetime.datetime(1899, 12, 30)
        + datetime.timedelta(days=xldate + 1462 * datemode)
        )
        
def roundDateTime(shakyDT,nearest_minutes=1):
	"""
	"""
	shakyDT += datetime.timedelta(minutes=nearest_minutes/2)
	shakyDT -= datetime.timedelta(minutes=shakyDT.minute % nearest_minutes,
                         seconds=shakyDT.second,
                         microseconds=shakyDT.microsecond)
	return shakyDT
	

def pdinterpolate(ts, datetime_index):
    """could decide to fill nans !
    but not adapted for calculating scores?
    To be tested"""
    x = pd.concat([ts, pd.Series(index=datetime_index)])
    #~ return x.groupby(x.index).first().sort_index().fillna(method="ffill")[datetime_index]
    return x.groupby(x.index).first().sort_index().interpolate('linear')[datetime_index]
    #~ return x.groupby(x.index).first().sort_index()[datetime_index]

def get_kge_de_stats(obs,sim):
    """Function to agregate all stats calculation as proposed in https://github.com/schwemro/diag-eff"""
    objstats={}
    objstats['brel_mean'] = de.calc_brel_mean(obs,sim) 
    objstats['brel_res'] = de.calc_brel_res(obs,sim) 
    objstats['b_area'] = de.calc_bias_area(objstats['brel_res']) 
    objstats['temp_cor'] = de.calc_temp_cor(obs,sim) 
    objstats['eff_de'] = de.calc_de(obs,sim) 
    objstats['b_dir'] = de.calc_bias_dir(objstats['brel_res']) 
    objstats['phi'] = de.calc_bias_slope(objstats['b_area'],objstats['b_dir'])
    objstats['kge_beta'] = kge.calc_kge_beta(obs,sim)
    objstats['alpha'] = kge.calc_kge_alpha(obs,sim)
    objstats['gamma'] = kge.calc_kge_gamma(obs,sim)
    objstats['kge_r'] = kge.calc_temp_cor(obs,sim)
    objstats['kge'] = kge.calc_kge(obs,sim)
    return objstats
    
def get_streamflow_simulation_results(simdir,simuname,stadic,slope_mode = 'OF',manning=3e-6,manning_filename='whatever.pfb'):
    """
    get a pd DataFrame with Q for all stations (given in stadic)
    slope mode : 'OF' = 'Overland Flow' classical way of using the slopes straight to get Q
    'KWE' = 'Kinematic Wave Equation' using the flow direction to get the slopes to get Q
    """

    #### Read & process simulation outputs:
    sim = PFout.PFout(simdir,'ara')
    sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
    sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_h_top.nc'])]),filetype='CLM')
    sim.X0=np.min(sim.x)
    sim.Y0=np.min(sim.y)

    """Read PF outputs"""
    start_time = time.time()
    with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
        sim.satur_array = ds.variables['saturation'][:]
        #~ topmask=np.swapaxes(ds.variables['top_mask'][:],0,1)
    """MASK is a nx x ny boolean mask of the domain
    When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
    (as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
    """
    #~ MASK=topmask>0 
    with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
        sim.press_array = ds.variables['succion'][:] 

    """Read CLM outputs"""
    """
    CLM_var is a dictionnary where keys are local variable names as in PFCLM
    values are lists of string[filename, PFCLM variable name, long name, units]
    """
    CLM_var={'surf_press':['','h_top','Surface pressure','m']}
    """ add the filename in the dic"""        
    for k,val in CLM_var.items():
        CLM_var[k][0]=os.sep.join([simdir,''.join([simuname,'_'+val[1]+'.nc'])])
        
    for k,val in CLM_var.items():
        try:
            with Dataset(val[0], 'r') as ds:
                print('read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
                exec('sim.%s=np.swapaxes(ds.variables[\'%s\'][:],0,2)'%(k,val[1]))        
        except FileNotFoundError:
                print('CANNOT read %s \t\t\t\t\t from file %s'%(val[2],val[0]))


    """ Read constant files"""
    ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
    #~ ds=Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r')
    #watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
    sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,1)
    if manning==0:
        sim.mannings_arr = io.PFB_read(manning_filename,0)[:,:,0]
    else:
        sim.mannings_arr = np.zeros((sim.nx,sim.ny))+manning
    sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,2)
    sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
    sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
    sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,1)
    sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,1)
    #~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))
    ds.close()
    #xx_masked, yy_masked and data have shapes [ny,nx] and data[0,:] is the lowest latitude (bottom of map)
    Qsim = pd.DataFrame()
    #~ x = [sim.X0 + sim.dx*x+sim.dx/2 for x in range(sim.nx)]
    #~ y = [sim.Y0 + sim.dy*y+sim.dy/2 for y in range(sim.ny)]
    x = [sim.X0 + sim.dx*x for x in range(sim.nx)]
    y = [sim.Y0 + sim.dy*y for y in range(sim.ny)]

    dtime = time.time() - start_time
    temp_time = time.time()
    print("read all PFCLM outputs from NCs --- %d seconds --- " % (dtime))

    print("calculate streamflow" )

    
    if slope_mode == 'KWE':
        outflow = PFout.calculate_runoff_maps(htop = sim.surf_press,slopex=sim.slope_x_arr,slopey=sim.slope_y_arr,mannings=sim.mannings_arr,
        nx = sim.nx,ny = sim.ny,dx = sim.dx, dy = sim.dy, nt =sim.n_t_clm,mask=sim.mask_arr,KWE='Stan',large_array=True,epsilon = 1E-7)
    else:
       outflow = PFout.calculate_runoff_maps(htop = sim.surf_press,slopex=sim.slope_x_arr,slopey=sim.slope_y_arr,mannings=sim.mannings_arr,
        nx = sim.nx,ny = sim.ny,dx = sim.dx, dy = sim.dy, nt =sim.n_t_clm,mask=sim.mask_arr,KWE='UP',large_array=True,epsilon = 1E-7)
         
    for stationname,sta in stadic.items():
        print(sta.name)
        sim.surfQ = np.swapaxes(outflow,0,1)[sta.y_sim_ind,sta.x_sim_ind,:].flatten()
        """m3/hr (TO CHECK -> seems ok)"""
        sta.Qsim  = sim.create_panda_series(np.reshape(sim.surfQ,sim.n_t_clm),start_date = "1/1/2005", start_hour = "00:00", freq = "1H",periods = sim.n_t_clm)        
        sta.Qsim[abs(sta.Qsim)>10e10]=np.nan
        """m3/hr -> m3/s"""
        sta.Qsim = sta.Qsim/3600.0
        Qsim[stationname]=sta.Qsim        
        stadic[stationname] = sta

    dtime = time.time() - temp_time
    temp_time = time.time()
    print("calculate streamflow --- %d seconds --- " % (dtime))            
    
    return sim,stadic,Qsim,x,y, outflow
    

def plot_time_series(sims,stationname,plot_yrs,method='D',scale=False,scale_free=True,color_index=['r','b','g','c']):
    
    fig,ax =plt.subplots(nrows=len(plot_yrs),ncols = 1,figsize=(20,10), squeeze=True)
    i=0
    j=0
    #initiate kge de stat dics
    for sim, simdata in sims.items(): sims[sim]['KGE_de_stats']=OrderedDict() #ordered because key order (years) should be kept for score comparison at the end
    for i,yr in enumerate(plot_yrs):
        print('processing year %d...'%yr)
        obs = sims[[k for k in sims.keys()][0]]['stadic'][stationname].Q.dropna().sort_index(axis=0)
        obs = obs.groupby(obs.index).mean()[datetime.datetime(yr,1,1):datetime.datetime(yr,12,31)]
        for sim, simdata in sims.items(): 
            sims[sim]['Qsim'] = sims[sim]['stadic'][stationname].Qsim.dropna().sort_index(axis=0)[datetime.datetime(yr,1,1):datetime.datetime(yr,12,31)]
            obs_int=pd.concat([obs,sims[sim]['Qsim']],axis=1).interpolate(method='slinear',limit=50)[pd.concat([obs,sims[sim]['Qsim']],axis=1).columns[0]]
            obs_intD = obs_int.resample('D').mean()
            simuD = sims[sim]['Qsim'].resample('D').mean()
            merge=pd.concat([obs_intD.rename('obs'),simuD.rename('sim')],axis=1)
            kept = merge.dropna()
            sims[sim]['stats'] = obj.kge(kept['obs'].values, kept['sim'].values, return_all=True) 
            sims[sim]['KGE_de_stats'][yr] = get_kge_de_stats(kept['obs'].values, kept['sim'].values)
            print('scores 1 %s: '%sim)
            print('kge standard')
            for st in sims[sim]['stats']: print('%2.2f'%st,end='\t')
            print(' (kge, corr, std1/std2, mean1/mean2 )')  
            print('kge-de')
            kgedestats = [sims[sim]['KGE_de_stats'][yr]['kge'],sims[sim]['KGE_de_stats'][yr]['kge_r'],sims[sim]['KGE_de_stats'][yr]['alpha'],sims[sim]['KGE_de_stats'][yr]['kge_beta']]
            for st in kgedestats: print('%2.2f'%st,end='\t')
            print(' (kge, corr, std1/std2, mean1/mean2 )')              
            #~ else:
                #~ # Interpolation on the closest available obs - sim couples:
                #~ #simuD = simu.resample('D').asfreq()
                #~ #simuD = dsclm['Q'][dsclm.index.hour==12]
                #~ simu = simu[(simu.index.month>=6) & (simu.index.month<=11)]
                #~ new_dates = obs.index.round('H')
                #~ obs2=obs
                #~ obs2.index = new_dates
                #~ obs2 = obs2.groupby(obs2.index).mean()
                #~ merge=pd.concat([obs2,simu],axis=1)
                #~ #merge=pd.concat([obs2,simuD],axis=1)  
      
        if method=='H':
            ax[i].plot(obs_int.resample(method).mean(),'k')
        else:
            ax[i].plot(obs_intD.resample(method).mean(),'k')
            
        #Add an extra empty inseplot so that the legend item #2 is empty and 'obs' label stays alone (in both nsim=2 and 4)
        ax[i].plot([],[],color=(0,0,0,0))
        legend=['obs','   ']
        k=0
        for sim, simdata in sims.items(): 
            ax[i].plot(sims[sim]['stadic'][stationname].Qsim.dropna().sort_index(axis=0).resample(method).mean(),color_index[k])
            legend.append(sims[sim]['props']['simuname'])
            k+=1
        ax[i].set_ylabel('$m^{3}/s$')

        if i==0: ax[i].legend(legend,fontsize=10,loc='upper right',ncol=int(np.ceil(len(legend)/2)))
        
        ax[i].set_xlim([datetime.datetime(yr,4,1),datetime.datetime(yr,11,1)])
        if scale:
            ax[i].set_ylim([0,3.5])
            txtstr='year %d\n'%yr
            for sim, simdata in sims.items(): 
                res=sims[sim]['stats']
                txtstr=txtstr+'%s: $r^2 = %2.2f$ KGE = %2.2f\n'%(sims[sim]['props']['simuname'],res[1]*res[1],res[0])
            ax[i].text(datetime.datetime(yr,4,4),2.0,txtstr)
        
        elif scale_free:    
            txtstr='year %d\n'%yr
            for sim, simdata in sims.items(): 
                res=sims[sim]['stats']
                txtstr=txtstr+'%s: $r^2 = %2.2f$ KGE = %2.2f\n'%(sims[sim]['props']['simuname'],res[1]*res[1],res[0])
            ax[i].text(datetime.datetime(yr,4,4),ax[i].get_ylim()[1]-0.41*ax[i].get_ylim()[1],txtstr)
        else:            
            mask = (obs_intD.resample(method).mean().index >=datetime.datetime(yr,4,1)) & (obs_intD.resample(method).mean().index<datetime.datetime(yr,11,1))
            ax[i].set_ylim([0,obs_intD.resample(method).mean().loc[mask].max()+0.24*obs_intD.resample(method).mean().loc[mask].max()])
            txtstr='year %d\n'%yr
            for sim, simdata in sims.items(): 
                res=sims[sim]['stats']
                txtstr=txtstr+'%s: $r^2 = %2.2f$ KGE = %2.2f\n'%(sims[sim]['props']['simuname'],res[1]*res[1],res[0])
            ax[i].text(datetime.datetime(yr,4,4),ax[i].get_ylim()[1]-0.72*ax[i].get_ylim()[1],txtstr)
            
        ax[i].tick_params(axis='x', which='both', labelbottom=False, labeltop=False)
        ax[i].tick_params(axis='y', which='both', labelright=False, labelleft=True)
        ax[i].tick_params(axis='both', which='major', bottom=False,top=False,right=False,left=True)
        
    ax[0].tick_params(axis='both', which='both', bottom=False,top=True,right=False,left=True)
    ax[len(plot_yrs)-1].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)
    plt.gcf().subplots_adjust(bottom=0.05, top =0.95,left=0.2, hspace=0.001,wspace=0.001)
    
    figname_template='%dsims'%len(sims)
    for sim, simdata in sims.items(): 
        figname_template=figname_template+'_%s'%simdata['props']['simuname']
    for sim, simdata in sims.items(): 
        outdir = os.sep.join([simdata['props']['simdir'],'compare_'+figname_template])
        try: os.mkdir(outdir)
        except: print('directory already exist: %s'%outdir)
        
        if scale:
            plt.savefig(os.sep.join([outdir,'Q_%s_%s_%s'%(figname_template,stationname,method)+'.png']),dpi=400,format='png')
        elif scale_free:   
            plt.savefig(os.sep.join([outdir,'Q_%s_%s_%s'%(figname_template,stationname,method)+'_unscaled_free.png']),dpi=400,format='png')
        else:
            plt.savefig(os.sep.join([outdir,'Q_%s_%s_%s'%(figname_template,stationname,method)+'_unscaled.png']),dpi=400,format='png')
    return ax,sims

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

"""Get streamflow data"""

temp_time = time.time()
print("Load streamflow data" )

#~ """ Parshall data"""
#~ filename=r'/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/analyse/Data/flow_parshall_2006_2012.txt'
#~ df = pd.read_csv(filename,header=None, sep='\t')
#~ df.rename(columns={0:'date',1:'Qobs',2:'QBFobs'},inplace=True)
#~ df['date']=df['date'].apply(lambda x: minimalist_xldate_as_datetime(x, 0))
#~ df = df.set_index('date')
#~ """ from m3/s to mm/hr"""
#~ dfmms=df*1000./120000.
#~ df=df*3600.*1000./120000.

""" Ara data """
rt_dir = r'/home/hectorb/DATA/streamflow/AMMA_Benin/Donga'
pre_pattern = 'CL.Run_Od-'
suf_pattern = '.csv'
#~ station_list = {'ARA_PONT':2 ,'DONGA_PONT':3,'DONGA_ROUTE_DE_KOLOKONDE':3}
#~ station_list = {'BORI':1,'COTE_238':1,'IGBOMAKORO':1,'SANI_A_SANI':1,'SARMANGA':1,'TEBOU':1,'WEWE':1,'AFFON_PONT':1,'AGUIMO':1,'AVAL-SANI':1,'BAREROU':1,'BETEROU':1}
station_list = {'ARA_PONT':2}
station_list= {k: 2 for k, v in station_list.items()}
#first col is time then daily average then hourly
proj = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')

df = pd.DataFrame()
stadic ={}
for stationname,data_column in station_list.items():
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    """m3/s"""
    sta.read_Q(filepattern, data_col = data_column)
    #~ sta.Q = sta.Q*sta.Q.index.to_series().diff().dt.total_seconds().fillna(0)
    """m3/s -> m3/hr"""
    #~ sta.Q = sta.Q*3600.
    sta.Q.dropna(inplace = True)
    df[stationname]=sta.Q
    print(sta.name)
    filenames = glob.glob(filepattern)    
    sta.read_latlon_from_Qfile(filenames[0])
    sta.x, sta.y = pyproj.transform(geo_system,proj,sta.lon, sta.lat)
    print(sta.lon)
    print(sta.lat)
    print(sta.x)
    print(sta.y)
    stadic[stationname] = sta

df['Qobs']=df['ARA_PONT']
"""m3/s -> mm/hr"""
#~ df['Qobs'] = df['Qobs']*3600*1000/13000000
#~ df.resample('5min').bfill()
""" from mm/s to mm/time step """
#~ df['Qobs']=df['Qobs']*df.index.to_series().diff().dt.total_seconds().fillna(0)

dtime = time.time() - temp_time
temp_time = time.time()
print("Load streamflow data --- %d seconds --- " % (dtime))

""" get resolution dependent simulation properties"""
props1 = get_resolution_dependent_properties(props1)
props2 = get_resolution_dependent_properties(props2)
props3 = get_resolution_dependent_properties(props3)
props4 = get_resolution_dependent_properties(props4)

""" get streamflow simulated"""
sims=OrderedDict() #ordered dict classes is like a dictionnary (a={}) but preserved the order in which items are assigned
sims['sim1'] = {'props':props1}
sims['sim2'] = {'props':props2}
sims['sim3'] = {'props':props3}
sims['sim4'] = {'props':props4}


for sim, simdata in sims.items(): 
    stadic_tmp = copy.deepcopy(stadic)
    stationname = 'ARA_PONT'
    stadic_tmp[stationname].x_sim_ind = simdata['props']['x_outlet']
    stadic_tmp[stationname].y_sim_ind = simdata['props']['y_outlet']
    simdata['sim'],simdata['stadic'],Qsim,xtmp,ytmp,outflowtmp = get_streamflow_simulation_results(simdata['props']['simdir'],
    simdata['props']['simuname'],stadic_tmp,slope_mode='KWE',manning=simdata['props']['manning'])
    sims[sim] = simdata

#~ sims=OrderedDict() #ordered dict classes is like a dictionnary (a={}) but preserved the order in which items are assigned
#~ sims['sim1'] = {'sim':sim1,'stadic':stadic1,'props':props1}
#~ sims['sim2'] = {'sim':sim2,'stadic':stadic2,'props':props2}
#~ sims['sim3'] = {'sim':sim3,'stadic':stadic3,'props':props3}
#~ sims['sim4'] = {'sim':sim4,'stadic':stadic4,'props':props4}


""" Plot simulation results"""

plot_time_series(sims,stationname='ARA_PONT',plot_yrs=np.arange(2005,2009),method='D',scale=False,scale_free=True,color_index=['r','b','g','c'])
plot_time_series(sims,stationname='ARA_PONT',plot_yrs=np.arange(2005,2009),method='H',scale=False,scale_free=True,color_index=['r','b','g','c'])
plot_time_series(sims,stationname='ARA_PONT',plot_yrs=np.arange(2005,2009),method='D',scale=True,scale_free=False,color_index=['r','b','g','c'])
plot_time_series(sims,stationname='ARA_PONT',plot_yrs=np.arange(2005,2009),method='H',scale=True,scale_free=False,color_index=['r','b','g','c'])
plot_time_series(sims,stationname='ARA_PONT',plot_yrs=np.arange(2005,2009),method='D',scale=False,scale_free=False,color_index=['r','b','g','c'])
ax,sims2 = plot_time_series(sims,stationname='ARA_PONT',plot_yrs=np.arange(2005,2009),method='H',scale=False,scale_free=False,color_index=['r','b','g','c'])

brel_mean = np.array([[simdata['KGE_de_stats'][yr]['brel_mean'] for yr in simdata['KGE_de_stats'].keys()] for sim, simdata in sims2.items()]).flatten()
b_area =    np.array([[simdata['KGE_de_stats'][yr]['b_area'] for yr in simdata['KGE_de_stats'].keys()] for sim, simdata in sims2.items()]).flatten()
temp_cor =  np.array([[simdata['KGE_de_stats'][yr]['temp_cor'] for yr in simdata['KGE_de_stats'].keys()] for sim, simdata in sims2.items()]).flatten()
eff_de =    np.array([[simdata['KGE_de_stats'][yr]['eff_de'] for yr in simdata['KGE_de_stats'].keys()] for sim, simdata in sims2.items()]).flatten()
b_dir =     np.array([[simdata['KGE_de_stats'][yr]['b_dir'] for yr in simdata['KGE_de_stats'].keys()] for sim, simdata in sims2.items()]).flatten()
phi =       np.array([[simdata['KGE_de_stats'][yr]['phi'] for yr in simdata['KGE_de_stats'].keys()] for sim, simdata in sims2.items()]).flatten()
kge_beta =  np.array([[simdata['KGE_de_stats'][yr]['kge_beta'] for yr in simdata['KGE_de_stats'].keys()] for sim, simdata in sims2.items()]).flatten()
alpha =     np.array([[simdata['KGE_de_stats'][yr]['alpha'] for yr in simdata['KGE_de_stats'].keys()] for sim, simdata in sims2.items()]).flatten()
kge_r =     np.array([[simdata['KGE_de_stats'][yr]['kge_r'] for yr in simdata['KGE_de_stats'].keys()] for sim, simdata in sims2.items()]).flatten()
kge_eff =   np.array([[simdata['KGE_de_stats'][yr]['kge'] for yr in simdata['KGE_de_stats'].keys()] for sim, simdata in sims2.items()]).flatten()

figname_template='%dsims'%len(sims2)
for sim, simdata in sims2.items(): 
    figname_template=figname_template+'_%s'%simdata['props']['simuname']
for sim, simdata in sims2.items(): 
    outdir = os.sep.join([simdata['props']['simdir'],'compare_'+figname_template])

plt.close('all')

#~ fig2 = de.diag_polar_plot_multi(brel_mean,b_area,temp_cor,eff_de,b_dir,phi)
#~ fig2 = de.diag_polar_plot_multi(brel_mean[eff_de>-2],b_area[eff_de>-2],temp_cor[eff_de>-2],eff_de[eff_de>-2],b_dir[eff_de>-2],phi[eff_de>-2])
#~ fig2.savefig(os.sep.join([outdir,'scores_%s'%(figname_template)+'.png']),dpi=400,format='png')

#~ fig3 = kge.polar_plot_multi(kge_beta,alpha,kge_r,kge_eff)
#~ plt.savefig('/home/hectorb/test.png',dpi=400,format='png')
