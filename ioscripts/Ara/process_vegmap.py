
# coding: utf-8

import kolinkelly as kk
import joestork as js

import numpy as np
import pandas as pd
import rasterio
import pyproj
import matplotlib.pyplot as plt
import PFlibs
import copy
from PFlibs import io_pfb
from PFlibs import drv_vegm_io as vegio

import scipy.spatial as ann
from procsycz import readDataAMMA as rdA
from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
from procsycz import procGeodata_Gdal
#kolinkelly is a module from waterbox that includes functionalities for NC manipulation


###################################
## STEP 1: 
###################################
resolution = 1000
#~ resolution = 500
#~ resolution = 100
resolution = 30

#### Open DEM:
#~ with rasterio.open("/home/hectorb/PARFLOW/PROJECTS/Ara/Ara/input/data/DEM/ara_MNT_100m_extent_342000_348400_1076000_1082400_mean_agreg_D4_pitfilled.tif") as src:
if resolution == 30: dem,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/preproc/DEM/ara_MNT_30m.tif",nodata_value = 65535)
if resolution == 100: dem,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/preproc/DEM/ara_MNT_100m_min_agreg.tif",nodata_value = 65535)
if resolution == 500: dem,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/preproc/DEM/ara_MNT_500m_min_agreg2.tif",nodata_value = 65535)
if resolution == 1000: dem,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/preproc/DEM/ara_MNT_1000m_min_agreg2.tif",nodata_value = 65535)
[lonlondem,latlatdem]=np.meshgrid(londem,latdem)

#### Open MASK:
if resolution == 30: mask,lonmask,latmask = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/preproc/DEM/ara_MNT_30m_mask2.tif",nodata_value = 65535)
if resolution == 100: mask,lonmask,latmask = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/preproc/DEM/ara_MNT_100m_min_agreg_mask.tif",nodata_value = 65535)
if resolution == 500: mask,lonmask,latmask = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/preproc/DEM/ara_MNT_500m_min_agreg2_mask.tif",nodata_value = 65535)
if resolution == 1000: mask,lonmask,latmask = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/preproc/DEM/ara_MNT_1000m_min_agreg3_mask.tif",nodata_value = 65535)
[lonlonmask,latlatmask]=np.meshgrid(lonmask,latmask)

#### Open vegmap:
veg,lonveg,latveg = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/preproc/VEG/SUPER_DONGA_LANDCOVER_20m_2005-2006_cropped_ara.tif",nodata_value = 65535)
[lonlonveg,latlatveg]=np.meshgrid(lonveg,latveg)


def ann_tree(x, y, X, Y, nb=1):
    # generic tree function for a list of points (can originate from a structured grid or not)
    tree = ann.KDTree(np.array([(i,j) for i,j in zip(x, y)]))
    # res returns the (one, nb =1) closest (the cell) source point (x,y) from each destination point (X,Y)
    res = tree.query(np.array([(i,j) for i,j in zip(X, Y)]), nb)[1]
    return res

def progress(i, l):
    p = i/l*100
    if (p % 2) <= (100/l):
        print('...%s%%...'%int(p))
        
# Interpôlate on thiessen (voronoi cells), requires first to connect each point to its 2 closest neighbours (tree);
# This is the triangulation. Then, cells can be viasualized by drawing the bissectrice of each segment.
# then the interpolation on the destination set of points (or grid) is made by looking onto which cell is
# each destination point
# the tree is made to know for each point selected, which are the two closest points, then the 2 closest of
# each of these two, etc... up to any chosen level.
   
#flatten makes vector from a matrix
tree = ann_tree(lonlonveg.flatten(), latlatveg.flatten(), lonlondem.flatten(), latlatdem.flatten(), nb=1)

nx = len(lonveg)
l = len(tree)

veg2 = np.zeros((lonlondem.size, 1))

for s, p in enumerate(tree):
    #s is index of the destination grid
    #p is index of the source grid
    progress(s, l)       
            
    # this is short to go from vector back to matrix using modulo (reste) and integer division
    i0 = p % nx
    j0 = p // nx
                         
    veg2[s] = veg[j0,i0]  

nx=len(londem)
ny=len(latdem)
vegveg = np.zeros(np.shape(dem))
for i in range(ny):
    #~ vegveg[i,:] = np.reshape(veg2[i*nx:(i+1)*nx],(nx,1))
    vegveg[i,:] = veg2[i*nx:(i+1)*nx].T
#~ vegveg []

#~ crop_x_min = 8
#~ crop_x_max = np.size(londem) -6 
#~ crop_y_min = 5  
#~ crop_y_max = np.size(latdem) -13

#~ veg[veg==6]=10
#~ veg[veg==7]=10
#~ veg[veg<6]=2
#~ veg[veg==10]=1
vegveg[vegveg==6]=10
vegveg[vegveg==7]=10
vegveg[vegveg<6]=2
vegveg[vegveg==10]=1

cnts,bins = np.histogram(vegveg,[0,1.2,2])
print('n pixels trees: %i; n pixels herb:  %i'%(cnts[0],cnts[1]))
print('ratio trees / n pixels vegetated : %0.2f'%(cnts[0]/(cnts[0]+cnts[1])))
vegveg_towrite = copy.copy(vegveg)
vegveg_towrite[vegveg == 2] = 19
vegveg_towrite[vegveg == 1] = 8	
#~ [x,y,veg]=read_vegm(inputdrvvegm,plot=False)
#~ [x,y,veg]=read_vegm(inputdrvvegm,plot=True)
#~ [XX,YY,vegveg]=veg_to_vegveg(x,y,veg)
[x2,y2,veg2]=vegio.vegveg_to_veg(lonlondem,latlatdem,vegveg_towrite)
vegio.write_vegm(x2,y2,veg2,"/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/inputs/drv_vegm_Ara%d.dat"%resolution)



""" If pfb writing is needed:"""
if resolution == 30:
    x0 = 342930.0
    y0 = 1076360.0
else:
    x0 = 342900.0
    y0 = 1076300.0

hdr ={'X0': x0,
 'Y0': y0,
 'Z0': 0.0,
 'dx': resolution,
 'dy': resolution,
 'dz': 3.0,
 'ix': 0,
 'iy': 0,
 'iz': 0,
 'ns': 1,
 'nx': len(londem),
 'nx2': len(londem),
 'ny': len(latdem),
 'ny2': len(latdem),
 'nz': 1,
 'nz2': 1,
 'rx': 1,
 'ry': 1,
 'rz': 1}

io_pfb.PFB_write(np.swapaxes(vegveg_towrite,0,1), hdr, pfb_fp="/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/inputs/vegmap_Ara%d.pfb"%resolution)
#~ io_pfb.PFB_write(np.swapaxes(vegveg,0,1), hdr, pfb_fp="/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/inputs/vegmap_Ara100.pfb")
#~ io_pfb.PFB_write(np.swapaxes(vegveg,0,1), hdr, pfb_fp="/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/inputs/vegmap_Ara500.pfb")
#~ io_pfb.PFB_write(np.swapaxes(vegveg,0,1), hdr, pfb_fp="/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/inputs/vegmap_Ara1000.pfb")
#~ io_pfb.PFB_write(vegveg, hdr, pfb_fp="/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/preproc/VEG/drvvegm.pfb")



#### PLOT
fig = plt.figure(figsize=(16,8))
ax = fig.add_subplot(111)
ax.set_aspect(1)

p = ax.pcolormesh(lonlondem, latlatdem, dem[::-1,:], cmap="terrain", zorder=-1)
ax.plot([lonlondem[0,0], lonlondem[-1,-1], lonlondem[-1,-1], lonlondem[0,0], lonlondem[0,0]],
        [latlatdem[0,0], latlatdem[0,0], latlatdem[-1,-1], latlatdem[-1, -1], latlatdem[0,0]], 'r-' )
#~ ax.plot([lonlondem[crop_y_min,crop_x_min], lonlondem[crop_y_max,crop_x_max], lonlondem[crop_y_max,crop_x_max], lonlondem[crop_y_min,crop_x_min], lonlondem[crop_y_min,crop_x_min]],
        #~ [latlatdem[crop_y_min,crop_x_min], latlatdem[crop_y_min,crop_x_min], latlatdem[crop_y_max,crop_x_max], latlatdem[crop_y_max,crop_x_max], latlatdem[crop_y_min,crop_x_min]], 'r-' )        
#~ cb = plt.colorbar(p)
p2 = ax.pcolormesh(lonlonmask, latlatmask, mask[::-1,:], cmap='Greys', zorder=-1, alpha=0.1)
#~ p3 = ax.pcolormesh(lonlonveg, latlatveg, veg[::-1,:], cmap=plt.cm.get_cmap("YlGn_r",2), zorder=-1, alpha=0.7)
p3 = ax.pcolormesh(lonlondem, latlatdem, vegveg[::-1,:], cmap=plt.cm.get_cmap("YlGn_r",2), zorder=-1, alpha=0.7)
#~ p2 = ax.pcolormesh(xx-sim.dx/2, yy-sim.dy/2, vegmasked, cmap=plt.cm.get_cmap("YlGn_r",3), alpha=0.7)

cb = plt.colorbar(p3,ticks=np.linspace(0,7,8))
#~ cb.set_label('vegetation class',fontsize=20)
cb.ax.set_yticklabels(['Trees', 'Herbaceous'],fontsize = 14)
ax.set_xlabel('X utm (m)',fontsize=20)
ax.set_ylabel('Y utm (m)',fontsize=20)
#~ plt.pcolor(x_mask, y_mask, np.ma.masked_equal(mask,0), hatch='/', alpha=0.)
plt.show()
