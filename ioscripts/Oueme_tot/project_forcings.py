#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    Script for projecting .netcdf forcing data from geo coordinates to 
    cartesian coordinates matching a PF grid
    
    Worflow is as follows:
    - read in a netcdf forcing field (here Precip as example)
    - read in a domain mask as geotif (onbly the grid is used not the values)
    - convert both to iris cubes, and add some missing metadata such as 
    crs , units and standard names matching cf conventions
    - then regrid: here the regrid mapper can be calculated on a smaller grid in time:
    eg use yearly average Precip to calculate the regridder, then apply the regridder to the
    whole matrix (maybe this could be made better)
    - warning: some memory issues, check faro memory status with 'top'
    - then the projected + interpolated cube is converted to xarray for convenience
    - then writing of pfb files using time parsing is set up
    
    
    ###### NOTE: compatibility issues gdal vs. iris #######
    make a virtual env without gdal: 
    
    conda create -n env_iris python=3.7

    conda activate env_iris

    conda install ipython

    conda install xarray

    (conda remove rasterio si necessaire)

    pip install rasterio

    conda install -c conda-forge iris 
    ==> gdal still not available as such (don't try to conda install it)
    but at least we have rasterio which would fail otherwise
    
    #########################################################

    Regrid using iris tools:
    https://scitools-iris.readthedocs.io/en/latest/userguide/interpolation_and_regridding.html
    https://scitools.org.uk/iris/docs/v1.13.0/iris/iris/cube.html#iris.cube.Cube.regrid
    
    for display
    https://stackoverflow.com/questions/62346854/how-to-convert-projection-x-and-y-coordinate-in-netcdf-iris-cube-to-lat-lon
    
    

    @copyright: 2021 by Unknown <hectorb@faro>
    @license: GNU GPL, see COPYING for details.
"""

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import xarray as xr
import os
import iris
import iris.plot as iplt
import pyproj
import struct

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
forcings_folder = '/home/bodjrenr/Climat/Base_ERA5'
outfolder = '/homelocal5/phyrev/_PARFLOW/projects/Oueme/active/Oueme/forcings/FORC/'

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def PFB_write(pfarray, pfb_hdr_dict, pfb_fp):
    print("**This code does NOT handle subgrids**")
    with open(pfb_fp,'wb') as pfbf:
        pfbf.write(struct.pack('>d',pfb_hdr_dict['X0']))
        pfbf.write(struct.pack('>d',pfb_hdr_dict['Y0']))
        pfbf.write(struct.pack('>d',pfb_hdr_dict['Z0']))
        pfbf.write(struct.pack('>i',pfb_hdr_dict['nx']))
        pfbf.write(struct.pack('>i',pfb_hdr_dict['ny']))
        pfbf.write(struct.pack('>i',pfb_hdr_dict['nz']))
        pfbf.write(struct.pack('>d', pfb_hdr_dict['dx']))
        pfbf.write(struct.pack('>d', pfb_hdr_dict['dy']))
        pfbf.write(struct.pack('>d', pfb_hdr_dict['dz']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['ns']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['ix']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['iy']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['iz']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['nx']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['ny']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['nz']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['rx']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['ry']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['rz']))
            
        print ("wrote header")
        nx = pfb_hdr_dict['nx']
        ny = pfb_hdr_dict['ny']
        nz = pfb_hdr_dict['nz']
                
        for lays in range(nz):
            # repeat writing of layer 1 for Noah-WRF layers
            # 1, 2, & 3 (0.10 m, 0.30 m, + 0.60 m dz)
            #print("Writing layer %s") %(lays+1)
            for rows in range(ny):
                   for cols in range(nx):
                       if nz == 1:
                           val = pfarray[cols, rows]
                       else:
                           val = pfarray[cols, rows, lays]
                       pfbf.write(struct.pack('>d', val))
    print("Wrote pfb output")
    return(1)


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

""" read in some data and convert to iris """
print('lecture des donnees/conversion Iris.............. ')
# lire un fichier d'entree type
P = xr.open_dataset(os.sep.join([forcings_folder,'P_1.nc']))
# convert to iris
Pcfull = P.tp.to_iris()

# ex plots:
#~ iplt.plot(Pc[:,10,10])
#~ iplt.pcolormesh(Pc[0,:,:], norm=plt.Normalize(260, 300))
#~ iplt.pcolormesh(Pc[0,:,:])

# convert to iris an averaged, 2D, array:
Pc = P.tp.groupby('time.year').sum().mean(dim='year').to_iris()
#~ iplt.pcolormesh(Pc)


# lire une grille parflow en .tif (grille de destination)
mask = xr.open_rasterio('/homelocal5/phyrev/_PARFLOW/projects/Oueme/active/Oueme/DEM/Oueme_Nokoue_Mask.tif')
maskc = mask.to_iris()


""" assign metadata: THIS IS NEEDED ! """

#: iris.coord_systems.GeogCS(semi_major_axis=6378137.0, inverse_flattening=298.257223563) = WGS84... see https://github.com/SciTools/iris/blob/main/lib/iris/analysis/cartography.py crtl+f WGS
crs_aea = iris.coord_systems.AlbersEqualArea(latitude_of_projection_origin=11.35, longitude_of_central_meridian=-4.3, false_easting=0.0, false_northing=0.0, standard_parallels=(16.12,6.58), ellipsoid=iris.coord_systems.GeogCS(semi_major_axis=6378137.0, inverse_flattening=298.257223563))
crs_geo = iris.coord_systems.GeogCS(semi_major_axis=6378137.0, inverse_flattening=298.257223563)
Pc.coord('longitude').coord_system = crs_geo
Pc.coord('latitude').coord_system = crs_geo
Pc.coords('longitude')[0].standard_name='longitude'
Pc.coords('latitude')[0].standard_name='latitude'
Pcfull.coord('longitude').coord_system = crs_geo
Pcfull.coord('latitude').coord_system = crs_geo
Pcfull.coords('longitude')[0].standard_name='longitude'
Pcfull.coords('latitude')[0].standard_name='latitude'

maskc.coord('x').coord_system = crs_aea
maskc.coord('y').coord_system = crs_aea
maskc.coords('x')[0].standard_name='projection_x_coordinate'
maskc.coords('y')[0].standard_name='projection_y_coordinate'
maskc.coords('projection_x_coordinate')[0].units='metres'
maskc.coords('projection_y_coordinate')[0].units='metres'

print('lecture des donnees/conversion Iris.............. ok')


"""regrid: projection + interpolation)"""
print('regrille.............. ')

#~ PCaea,ext = iris.analysis.cartography.project(Pc, crs_aea)
# test : regrille seulement la carte 2D: 
Pc_aea = Pc.regrid(maskc,iris.analysis.Linear())

# regrille l'ensemble du netcdf: passe par un regridder pour ne pas refaire le mapping a chaque fois: 
# deux methodes: lineaire ou nearest: comparons les
regridder_nearest = iris.analysis.Nearest().regridder(Pc,maskc)
regridder_linear = iris.analysis.Linear().regridder(Pc,maskc)

print('regrille.............. mapping ok')

Pcfull_aea_near = regridder_nearest(Pcfull)
# reconverti en xarray (plus simple / immediat pour les manips):
p_aea_near = xr.DataArray().from_iris(Pcfull_aea_near)
# delete the large array when not needed anymore (to avoid memomry issues)
del Pcfull_aea_near
p_aea_nearY = p_aea_near.groupby('time.year').sum().mean(dim='year')

print('regrille.............. nearest ok')

Pcfull_aea_lin = regridder_linear(Pcfull)
del(Pcfull)
# reconverti en xarray (plus simple / immediat pour les manips):
p_aea_lin = xr.DataArray().from_iris(Pcfull_aea_lin)
del Pcfull_aea_lin
p_aea_linY = p_aea_lin.groupby('time.year').sum().mean(dim='year')

print('regrille.............. linear ok')


""" compare les moyennes annuelles avant / apres projection """
PY = P.tp.groupby('time.year').sum().mean(dim='year')

projAEA = pyproj.Proj("+proj=aea +lat_1=16.12 +lat_2=6.58 +lat_0=11.35 +lon_0=-4.3 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs")
projUTM = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')
xmin,ymin=pyproj.transform(projAEA,geo_system,p_aea_nearY.x.min(),p_aea_nearY.y.min())
xmax,ymax=pyproj.transform(projAEA,geo_system,p_aea_nearY.x.max(),p_aea_nearY.y.max())

#plot
fig,ax = plt.subplots(1,3,figsize=(17,5))
p_aea_nearY.plot(ax=ax[0],vmin=2.5,vmax=27)
ax[0].set_title('interpolation nearest neighbor')

PY.sel(longitude=slice(xmin,xmax),latitude=slice(ymax,ymin)).plot(ax=ax[1],vmin=2.5,vmax=27)
ax[1].set_title('ERA5 mean P')

p_aea_linY.plot(ax=ax[2],vmin=2.5,vmax=27)
ax[2].set_title('interpolation lineaire')

print('conversion xarray et plot.............. ok')

""" write PFBs """
print('save_pfb.............. ')

# divide by 10 days periods
NT = 30 # days
n_nt=np.floor(NT/10) # number of periods
#~ nt=10*48 # number of timesteps per period
nt=10*24 # number of timesteps per period
hout={}
hout['nx'] = 216
hout['ny'] = 420
hout['nz'] = nt
hout['nx2'] = 216
hout['ny2'] = 420
hout['nz2'] = nt
hout['dx'] = 1000
hout['dy'] = 1000
hout['dz'] = 1
hout['X0'] = 634500
hout['Y0'] = -539500
hout['Z0'] = 0
hout['ns'] = 1
hout['rx'] = 1000
hout['ry'] = 1000
hout['rz'] = 1
hout['ix'] = 634500
hout['iy'] = -539500
hout['iz'] = 0

for period in range(int(n_nt)):
    hout['Z0'] = period*nt+1
    pfbfilename = os.sep.join([outfolder,'APCP','Forc.APCP.%06d_to_%06d.pfb'%(period*nt+1,(period+1)*nt)])
    print(pfbfilename)
    
    Outdata = np.swapaxes(p_aea_lin.isel(time=slice(period*nt,(period+1)*nt)).data,0,2)
    PFB_write(Outdata,hout,pfbfilename)
print('save_pfb.............. ok')
