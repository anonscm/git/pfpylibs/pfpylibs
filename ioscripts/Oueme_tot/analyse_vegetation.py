#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS
    
    

    analyse vegetation maps to define LAI spatialization method
    
    PENSER A CHANGER CLASSE 8 -> 7 

    @copyright: 2022 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2022"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import xarray as xr
import rioxarray as rioxr
import datetime
import scipy.spatial as ann
import pyproj
import rasterio as rio
from rasterio.plot import show
import geopandas as gpd
from shapely.geometry import Point
import earthpy.plot as ep
import rasterstats as rs
import os,sys
import seaborn as sns

"""local imports:"""

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
# this is the Hansen 2000 map reprojected using AEA at 1km agregation using means. 
hansen = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/Hansen_2000_projete_Albers_1km.tif"
tappan = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/Oume_Nokoue_2000_lulc_1km_4classes.tif"
tappan2015 = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/Oume_Nokoue_2015_lulc_1km_4classes.tif"
ESA_CCI = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/PF_grid_ESA_CCI_tree_prop.tif"
LL_file = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/PF_grid_LLeroux_2000_tree_prop_masked.tif"
vegh_file = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/Forest_height_2019_benin_aea_1km_average.tif"
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##


hans = rioxr.open_rasterio(hansen)
hsdf = hans.to_dataframe(name='tree_percent')    


tap = rioxr.open_rasterio(tappan)
tapdf = tap.to_dataframe(name='veg_class')    

tap2 = rioxr.open_rasterio(tappan2015)
tapdf2 = tap2.to_dataframe(name='veg_class')    

esa = rioxr.open_rasterio(ESA_CCI)
esadf = esa.to_dataframe(name='tree_prct')   
 
ll = rioxr.open_rasterio(LL_file)
lldf = ll.to_dataframe(name='tree_prct')    

vegh = rioxr.open_rasterio(vegh_file)
veghdf = vegh.to_dataframe(name='tree_height')   

veg = pd.concat([hsdf,tapdf],axis=1)
veg = veg.drop('spatial_ref',axis=1)
veg[veg<0] = np.nan

veg2 = pd.concat([esadf,tapdf2],axis=1)
veg2 = veg2.drop('spatial_ref',axis=1)
veg2[veg2<0] = np.nan

veg3 = pd.concat([lldf,tapdf],axis=1)
veg3 = veg3.drop('spatial_ref',axis=1)
veg3[veg3<0] = np.nan

veg4 = pd.concat([veghdf,tapdf],axis=1)
veg4 = veg4.drop('spatial_ref',axis=1)
veg4[veg4<0] = np.nan
veg4[veg4>40] = np.nan

sns.pairplot(data=veg)

trees_8 = veg.loc[veg.veg_class==8,'tree_percent']
herb = veg.loc[veg.veg_class==19,'tree_percent']

trees_8_2 = veg2.loc[veg2.veg_class==8,'tree_prct']
herb_2 = veg2.loc[veg2.veg_class==19,'tree_prct']

trees_8_3 = veg3.loc[veg3.veg_class==8,'tree_prct']
herb_3 = veg3.loc[veg3.veg_class==19,'tree_prct']

trees_8_4 = veg4.loc[veg4.veg_class==8,'tree_height']
herb_4 = veg4.loc[veg4.veg_class==19,'tree_height']



fig, ax= plt.subplots(1,1)
sns.distplot(trees_8,kde=False,norm_hist=True,label='trees')
sns.distplot(herb,kde=False,norm_hist=True,label='herbaceous')
plt.legend(prop={'size': 12})
plt.xlabel('% trees')
plt.ylabel('Densite')


fig, ax= plt.subplots(1,1)
sns.kdeplot(trees_8,cumulative=True,label='trees')
sns.kdeplot(herb,cumulative=True,label='herbaceous')
plt.legend(prop={'size': 12})
plt.xlabel('% trees')
plt.ylabel('Densite')

from scipy.stats import ks_2samp
ks_2samp(trees_8.dropna().values,herb.dropna().values)
#The KS test report the maximum difference between the two cumulative distributions
# pvalue = 0: https://stackoverflow.com/questions/20530138/scipy-p-value-returns-0-0
# => KS statistics : value = 0.2 = max difference in density in the CDF (in vertical axis, see the kde plot)

#mean difference
diff_mean = trees_8.mean() - herb.mean()


fig, ax= plt.subplots(1,1)
sns.distplot(trees_8_2,kde=True,norm_hist=True,label='trees')
sns.distplot(herb_2,kde=True,norm_hist=True,label='herbaceous')
plt.legend(prop={'size': 12})
plt.xlabel('% trees')
plt.ylabel('Densite')


fig, ax= plt.subplots(1,1)
sns.kdeplot(trees_8_2,cumulative=True,label='trees')
sns.kdeplot(herb_2,cumulative=True,label='herbaceous')
plt.legend(prop={'size': 12})
plt.xlabel('% trees')
plt.ylabel('Densite')


diff_mean2 = trees_8_2.mean() - herb_2.mean()
diff_mean2
trees_8_2.mean()     
herb_2.mean()

fig, ax= plt.subplots(1,1)
sns.distplot(trees_8_3.dropna(),kde=True,norm_hist=True,label='trees')
sns.distplot(herb_3.dropna(),kde=True,norm_hist=True,label='herbaceous')
plt.legend(prop={'size': 12})
plt.xlabel('% trees')
plt.ylabel('Densite')


fig, ax= plt.subplots(1,1)
sns.kdeplot(trees_8_3,cumulative=True,label='trees')
sns.kdeplot(herb_3,cumulative=True,label='herbaceous')
plt.legend(prop={'size': 12})
plt.xlabel('% trees')
plt.ylabel('Densite')


diff_mean3 = trees_8_3.mean() - herb_3.mean()
diff_mean3
trees_8_3.mean()     
herb_3.mean()

"""Veggetation heught"""

fig, ax= plt.subplots(1,1)
sns.distplot(trees_8_4.dropna(),kde=True,norm_hist=True,label='trees')
sns.distplot(herb_4.dropna(),kde=True,norm_hist=True,label='herbaceous')
plt.legend(prop={'size': 12})
plt.xlabel('tree height (m)')
plt.ylabel('Densite')


fig, ax= plt.subplots(1,1)
sns.kdeplot(trees_8_4,cumulative=True,label='trees')
sns.kdeplot(herb_4,cumulative=True,label='herbaceous')
plt.legend(prop={'size': 12})
plt.xlabel('tree height (m)')
plt.ylabel('Densite')


diff_mean4 = trees_8_4.mean() - herb_4.mean()
diff_mean4
trees_8_4.mean()     
herb_4.mean()


hvegtree=pd.DataFrame({"hveg":trees_8_4.values,"lat":[ind[1] for ind in trees_8_4.index]}) 
lat_range = np.arange(hvegtree['lat'].min(),hvegtree['lat'].max(),20000)
labels = [str(a) for a in np.arange(hvegtree['lat'].min(),hvegtree['lat'].max()-20000,20000)/1000+10]

hvegtree['lat_range'] = pd.cut(hvegtree['lat'], lat_range,labels=labels)

lat_range2 = [hvegtree['lat'].min(),hvegtree['lat'].min()+(hvegtree['lat'].max()-hvegtree['lat'].min())/2,hvegtree['lat'].max()]
labels2 = ["%skm - %skm"%(hvegtree['lat'].min()/1000,hvegtree['lat'].min()/1000 + (hvegtree['lat'].max()-hvegtree['lat'].min())/2000),"%skm - %skm"%(hvegtree['lat'].min()/1000 +(hvegtree['lat'].max()-hvegtree['lat'].min())/2000,hvegtree['lat'].max()/1000)]

hvegtree['lat_range'] = pd.cut(hvegtree['lat'], lat_range,labels=labels)
hvegtree['lat_range2'] = pd.cut(hvegtree['lat'], lat_range2,labels=labels2)




plt.figure(figsize=(14,7))
ax = sns.boxplot(x="lat_range", y="hveg", data=hvegtree,showfliers=False, showmeans=True, palette="Set1")
tmp = np.unique(hvegtree.loc[:,['lat_range','hveg']].dropna(how = 'any').lat_range, return_counts=True)
xlabels = [t.get_text() for t in ax.get_xticklabels()]
for xindex in ax.get_xticks(): ax.text(xindex,ax.get_ylim()[1]-0.1*(ax.get_ylim()[1]-ax.get_ylim()[0]),str(tmp[1][list(tmp[0][:]).index(xlabels[xindex])]))    


plt.figure(figsize=(14,7))
ax = sns.boxplot(x="lat_range2", y="hveg", data=hvegtree,showfliers=False, showmeans=True, palette="Set1")
tmp = np.unique(hvegtree.loc[:,['lat_range2','hveg']].dropna(how = 'any').lat_range2, return_counts=True)
xlabels = [t.get_text() for t in ax.get_xticklabels()]
for xindex in ax.get_xticks(): ax.text(xindex,ax.get_ylim()[1]-0.1*(ax.get_ylim()[1]-ax.get_ylim()[0]),str(tmp[1][list(tmp[0][:]).index(xlabels[xindex])]))    


"""This section loads up a tiff of land cover, and a PF grid as shp 
obtained in QGIS from 'outil de création de vecteur' -> 	
'créer une grille', extraire l'extension depuis la couche cible parflow aea 1km
Then it applies zonal stats to each grid cell (which is a polygon in the shp)
Then it computes for instance the proportion of trees in the high resolution mapt
for each cell of the low resolution map.
"""
# Load the data
ESA20m_file = r"/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/ESACCI-LC-L4-LC10-Map-20m-P1Y-2016-v1.0_AO_V2_int_small_cut_aea.tif"
print('Read in...')
with rio.open(ESA20m_file) as src:
    data = src.read(1, masked=False,out_dtype='uint8')
    meta = src.profile
    #~ meta['nodata']=np.uint8(0) #otherwise it's 0.0 for strange reason, so then in rasterstats it will try to allocate a huge float64 array!
    extent = rio.plot.plotting_extent(src)
    
print('Read in:done')


grid_oueme = gpd.read_file(r"/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/oueme_grid.shp")
# Process the data
# https://pythonhosted.org/rasterstats/manual.html
print('process...')
#Note for large array: zonal_stats will at some point create a large np array with floats which may create problem
#~ ESA20m_stats = rs.zonal_stats(grid_oueme,data,nodata = meta['nodata'],
                                   #~ affine=meta['transform'],geojson_out=True,
                                   #~ copy_properties=True,
                                   #~ add_stats={'veg_hist': return_hist2})
ESA20m_stats = rs.zonal_stats(grid_oueme,data,nodata = meta['nodata'],
                                   affine=meta['transform'],
                                   categorical=True)

tree_prop = [e[1.]/sum(e.values()) if 1.0 in e.keys() else 0. for e in ESA20m_stats] 
grid_oueme['trees'] = tree_prop
grid_oueme.to_file(r"/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/oueme_grid_ESA_CCI_tree_prop.shp")




"""This section loads up a tiff of land cover, and a PF grid as shp 
obtained in QGIS from 'outil de création de vecteur' -> 	
'créer une grille', extraire l'extension depuis la couche cible parflow aea 1km
Then it applies zonal stats to each grid cell (which is a polygon in the shp)
Then it computes for instance the proportion of trees in the high resolution mapt
for each cell of the low resolution map.
"""
# Load the data
LL_file = r"/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/L_Leroux_Oueme_2000_aea.tif"
print('Read in...')
with rio.open(LL_file) as src:
    data = src.read(1, masked=False,out_dtype='uint8')
    meta = src.profile
    #~ meta['nodata']=np.uint8(0) #otherwise it's 0.0 for strange reason, so then in rasterstats it will try to allocate a huge float64 array!
    extent = rio.plot.plotting_extent(src)
    
print('Read in:done')


grid_oueme = gpd.read_file(r"/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/oueme_grid.shp")
# Process the data
# https://pythonhosted.org/rasterstats/manual.html
print('process...')
#Note for large array: zonal_stats will at some point create a large np array with floats which may create problem
#~ ESA20m_stats = rs.zonal_stats(grid_oueme,data,nodata = meta['nodata'],
                                   #~ affine=meta['transform'],geojson_out=True,
                                   #~ copy_properties=True,
                                   #~ add_stats={'veg_hist': return_hist2})
LL_stats = rs.zonal_stats(grid_oueme,data,nodata = meta['nodata'],
                                   affine=meta['transform'],
                                   categorical=True)

tree_prop_LL = [np.nan for e in range(len(LL_stats))]
for i,e in enumerate(LL_stats):
    if e:
        if not 0.0 in e.keys():
            if not 1.0 in e.keys():
                LL_stats[i][1.0] = 0.
            if not 4.0 in e.keys():
                LL_stats[i][4.0] = 0.

for i,e in enumerate(LL_stats):
    if e:
        if not 0.0 in e.keys():
            tree_prop_LL[i] = (e[1.0] + e[4.0])/sum(e.values())

        
grid_oueme['trees'] = tree_prop_LL
grid_oueme.to_file(r"/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/oueme_grid_LLeroux2000_tree_prop.shp")

