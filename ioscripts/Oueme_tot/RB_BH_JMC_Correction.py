def Code_corection (obs, rea, REA, f): #rea : réanalyse sur la période historique et sur des pixels proches des stations
    '''ce code a été développé par Bodjrènou, Cohard et Basile (2022) pour corriger principalement les distributions de pluie horaire ERA5
    Ce code, analogue à la méthode quantile empirique se base fondamentalement sur deux hypothèses qui sont : 
    -h1 : L’assimilation des pluies de X mm par la réanalyse est affectée par la même erreur dans le temps et dans l’espace. 
        Ex. : si la réanalyse simule mal les pluies de 5mm/h, l’erreur d’estimation demeure invariable dans le temps (jours, mois, saison, année, etc.), et sur toute la zone.
    -h2 : Les erreurs d’assimilation de la pluie ERA5 différent d’une modalité à une autre. 
        Ex. : l’assimilation des pluies entre [0 et 5], [10 et 15] ou même supérieures à 30mm n’est pas affectée par les mêmes erreurs. 
    hist : Compile des données horaires disponibles sur la région. Les données par stations sont présentées en colonne et les indice en datetime
    REA : Base ERA5 à corriger (en termes d'intensité)        
    freq : Degré de performance                            
    '''
    # Etape 1 : : Déterminations du coeff pour corriger 99,95% de l'échantillon de pluie
    s_ = []
    freq =np.arange(0,60, 1)
    for s in freq:          # s en mm
        h = len(obs.loc[obs <= s]) /len(obs)  *100
        s_.append(h)
    ss = pd.Series(s_, freq)
    s = ss.loc[ss > 99.95].index[0] ## hauteur de pluie à ce seuil (99,95) 
    n_ob = len(obs.loc[obs>s]) # Nombre de pluie supérieure à ce seuil dans les observations  
    c0 = s / rea.sort_values(ascending=False)[n_ob] # Facteur multiplicateur pour avoir un même échantillon supérieur à ce seuil  

    Data_c = rea*c0
    len(obs.loc[obs< s])/len(obs) *100 #vérification du nombre de réanalyse cotre observations    
    len(Data_c.loc[Data_c< s]) /len(obs) *100
    REA_c = REA * c0
    '''Ajustement sur la réanalyse '''
    # Etape 2 : Correction des pluie par ordre décroissant (f est le pas de temps)
    fr2 = np.arange(0, 2.01, 0.02) 
    for a in np.arange(0, s+0.001, f)[::-1] : #  a =10   
       k_ =[]
       for k in fr2[::-1]:
           er = k * Data_c
           if len(er.loc[er<=a].dropna()) < len(obs.loc[obs<=a].dropna()) :
               ''
           else:
               k_.append(k)
       c= np.max(k_)
       D_corr =  c * Data_c
       print( 'Prec<={:.1f} : E={:.2f} | O={:.2f}, k={:.2f} '.format(a, len(D_corr.loc[D_corr<=a].dropna()) /len(obs)*100 , len(obs.loc[obs<=a].dropna())/len(obs)*100 , c))
       Data_c = pd.Series( np.where(Data_c<=a, D_corr, Data_c))  ;   Data_c.index = obs.index
       # Etape 3 : correction de la réanalyse
       b1 = c* REA_c
       REA_c = b1.where(REA_c <= a , other= REA_c) 
       
    eror =(Data_c.loc[Data_c< s].sum() -obs.loc[obs< s].sum() )/ len(Data_c.loc[Data_c< s] )
    sup_value = obs.loc[obs> s].mean() - eror  
    Data_c.loc[Data_c> s]  =  sup_value      
    REA_c = REA_c.where(REA_c <=s, sup_value)
    return REA_c, Data_c

###############################################--------------------------------------------------------------------------------------------
#                                                           Ajustement des cumuls
###############################################--------------------------------------------------------------------------------------------

REA_c, dt_c = Code_corection (obs, rea, REA, f=0.1) #rea : réanalyse la période historique et sur des pixels proches des stations

ERA5_new = xr.open_dataarray("C:\Dossier\Doctorat\Base_ERA5\Prec_New\Prec_New.nc")
new_cor = xr.open_dataarray("C:\Dossier\Doctorat\Base_ERA5\Prec_New\Prec_New_cor_24-01-2022.nc")
ERA5_new = ERA5_new.where(ERA5_new>0, other = 0.000001) # logique ; évitons de diviser les nombres par zéro
new_cor = new_cor.where(new_cor>0, other = 0.000001)

lon = [i for i in ERA5_new.lon.values]
lat = [i for i in ERA5_new.lat.values]
year =[i for i in  pd.to_datetime( ERA5_new.time.to_dataframe().index ) .year.drop_duplicates()]

# Temps 1
year =[i for i in  pd.to_datetime( ERA5_new.time.to_dataframe().index ) .year.drop_duplicates()]
day = [ b.strftime("%Y-%m") for b in pd.to_datetime({'year':  ERA5_new.time.to_dataframe().index.year,'month': ERA5_new.time.to_dataframe().index.month, 'day': ERA5_new.time.to_dataframe().index.day}).drop_duplicates() ]
database = []
for i in range (len(lon)):
    datas =[]
    for k in range(len(lat)):
       temp= []
       for t in day :    
           print(i, k, t)
           y_new = ERA5_new.sel(time=t, lon= lon[i] , lat= lat[k]) .to_dataframe()
           y_cor = new_cor.sel(time=t, lon= lon[i] , lat= lat[k]) .to_dataframe()
           f_cor = y_new['Rainf'].mean()  /  y_cor['Rainf']. mean()
           y_cor['Rainf']  = y_cor['Rainf'] * f_cor 
           temp.append(y_cor)   
       lat_dt = pd.concat( temp, axis=0)
       datas.append(lat_dt)
    lon_dt = pd.concat( datas, axis=0)
    database.append(lon_dt)
database = pd.concat( database,  axis=0)
database = database .set_index([pd.to_datetime(database.index), database.lat, database.lon])['Rainf']
database = database.to_xarray()
database = database.where(database>=0.1, other = 0.00000)
