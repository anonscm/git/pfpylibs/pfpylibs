#-*- coding: utf-8 -*-

"""
    PFPYLIBS - ioscripts 

    Script made to create LAIs, SAIs, DISPLA and Z0M 
    
    1. scale ERA5 climatological LAI for low vegetation (lai_lv) 
    from a specified lai_min to the max observed in ERA5
    
    2. correct some pixels which have 0 LAI_lv all year long: interpolate 
    on closest non-zero
    => lai_lv_corr2
    
    3. calculate scaling ratio for yearly LAI amplitude: corr_ratio
     corr_ration is yearly ERA5 P sum (bias corrected) over mean ERA5 P 
     sum (bias corrected) for period 2000-2008 (period of LAI observed)
     apply the corr_ration to yearly LAI amplitudes:
     => LAIcorr_b
     
    4. Prepare SAI: for each pixel, 
        identify starting day of year for increasing SAI: index corresponding
        to LAI_max (choice made). 
        Identify starting day of year for decreasing SAI: 30 days before LAI min
        Identify day of year where SAI goes back to minimum value : index corresponding
        to LAI_min (choice made)
        
        loop over each pixel & time step and assign a code whether minimum SAI should be
        applied, increasing SAI, or decreasing SAI. In the latter, the code is actually
        the number of days after the start of decreasing so the slope of the decrease can
        be easily computed.
        
        Create SAI for herbaceous
        




    
    ###### NOTE: compatibility issues gdal vs. iris #######
    So,  in case iris is needed: 
    make a virtual env without gdal:     
    conda create -n env_iris python=3.7
    conda activate env_iris
    conda install ipython
    conda install pandas
    conda install xarray
    (conda remove rasterio si necessaire)
    pip install rasterio
    conda install -c conda-forge iris 
    conda install -c conda-forge/label/cf201901 iris
    ==> gdal still not available as such (don't try to conda install it)
    but at least we have rasterio which would fail otherwise

    OR !
    conda create -n env_iris -c conda-forge -c defaults python=3.7 ipython xarray iris
    to remove the env afterwards
    conda remove --name env_iris --all

    BEWARE in pers laptopt, think of changing python3 alias to point toward anaconda's python and not the native one'

    @copyright: 2022 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2022"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import xarray as xr
import copy
import os,struct
"""local imports:"""

plt.close()


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
LAI_file = "/home/hectorb/DATA/VEG/vegetation_TS/ERA5_LAI.nc"
P_file = "/home/hectorb/DATA/Precipitation/ERA5/Prec_New.nc"
outfolder = "/homelocal5/phyrev/_PARFLOW/projects/Oueme/active/Oueme/forcings/FORC/"
tappan = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/Oume_Nokoue_2000_lulc_1km_4classes.tif"


##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def PFB_write(pfarray, pfb_hdr_dict, pfb_fp):
    print("**This code does NOT handle subgrids**")
    with open(pfb_fp,'wb') as pfbf:
        pfbf.write(struct.pack('>d',pfb_hdr_dict['X0']))
        pfbf.write(struct.pack('>d',pfb_hdr_dict['Y0']))
        pfbf.write(struct.pack('>d',pfb_hdr_dict['Z0']))
        pfbf.write(struct.pack('>i',pfb_hdr_dict['nx']))
        pfbf.write(struct.pack('>i',pfb_hdr_dict['ny']))
        pfbf.write(struct.pack('>i',pfb_hdr_dict['nz']))
        pfbf.write(struct.pack('>d', pfb_hdr_dict['dx']))
        pfbf.write(struct.pack('>d', pfb_hdr_dict['dy']))
        pfbf.write(struct.pack('>d', pfb_hdr_dict['dz']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['ns']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['ix']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['iy']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['iz']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['nx']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['ny']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['nz']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['rx']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['ry']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['rz']))
            
        print ("wrote header")
        nx = pfb_hdr_dict['nx']
        ny = pfb_hdr_dict['ny']
        nz = pfb_hdr_dict['nz']
                
        for lays in range(nz):
            # repeat writing of layer 1 for Noah-WRF layers
            # 1, 2, & 3 (0.10 m, 0.30 m, + 0.60 m dz)
            #print("Writing layer %s") %(lays+1)
            for rows in range(ny):
                   for cols in range(nx):
                       if nz == 1:
                           val = pfarray[cols, rows]
                       else:
                           val = pfarray[cols, rows, lays]
                       pfbf.write(struct.pack('>d', val))
    print("Wrote pfb output")
    return(1)

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

# open dataset
lai = xr.open_dataset(LAI_file)
lai = lai.sel(latitude=slice(11,6),longitude=slice(0.5,4.))

# setup some parameters
laimin = 0.09                       # minimum LAI over herbaceous
saimin = 0.5                        # minimum SAI over herbaceous
lai_mean_after_corr_threshold = 0.5 # seuil à partir duquel ne plus interpoler avec du NN
lai_amp_after_corr_threshold = 0.5 # seuil à partir duquel ne plus interpoler avec du NN
# il faut virer des points qui n'ont absolument aucune amplitude. Sinon dans l'interpolation
# des poitns à 0, ces zones avec comportement étrange deviennent très étendues !

# apply lai correction (rescale over the full range down to lai_min)
lai_lv_corr = (lai.lai_lv - lai.lai_lv.min(dim='time')) * (lai.lai_lv.max(dim='time') - laimin)/(lai.lai_lv.max(dim='time')-lai.lai_lv.min(dim='time'))      


# plotting parameters to be kept all around to plot the simulation window only
xlim=[1.5,3.47]
ylim=[6.4,10.19]
aspect = np.diff(xlim)[0]/np.diff(ylim)[0]
#plot examples
#~ lai.lai_lv.mean(dim='time').plot(xlim=xlim,ylim=ylim,size=8,aspect=np.diff(xlim)[0]/np.diff(ylim)[0])
#~ lai.lai_lv.mean(dim='time').plot(xlim=xlim,ylim=ylim,figsize=(8*aspect,8)
#~ lai.lai_lv.mean(dim='time').plot(xlim=xlim,ylim=ylim,figsize=(6,8))
#~ lai.lai_hv.mean(dim='time').plot(xlim=xlim,ylim=ylim,figsize=(6,8))

""" Problem:  some lai have 0 amplitude (and are 0 all year long), so the resulting correction has nans
the solution is to interpolate nans using nearest neighbor but the easy solution
implemented in xarray has only 1d interpolation (interpolate_na)

-solution 1 : easy: use interpolate_na in xarray 1D using longitude
- solutin 2: convert to an iris cube, use
https://scitools.org.uk/iris/docs/v2.4.0/iris/iris/cube.html#iris.cube.Cube.interpolate
and
https://scitools.org.uk/iris/docs/v2.4.0/iris/iris/analysis.html#iris.analysis.Nearest
but this fails as NaN are interpolated as ... nans. the masked array in the cube is not really taken into account
- solutin 3: use good old scipy
https://stackoverflow.com/questions/37662180/interpolate-missing-values-2d-python
this works fine
"""
#~ lai_lv_corr.mean(dim='time').plot(xlim=xlim,ylim=ylim,figsize=(6,8),cmap='viridis')
#~ lai.lai_lv.min(dim='time').plot(xlim=xlim,ylim=ylim,figsize=(6,8),cmap='viridis')
#~ lai.lai_lv.max(dim='time').plot(xlim=xlim,ylim=ylim,figsize=(6,8),cmap='viridis')
# solution 1: 
#~ lai_lv_corr.interpolate_na(dim='longitude',method='nearest').min(dim='time').plot(xlim=xlim,ylim=ylim,figsize=(6,8),cmap='viridis')
# solution 2: 
""" does not work because does not take into account the mask of nans
import iris
laic = lai_lv_corr.to_iris()
crs_aea = iris.coord_systems.AlbersEqualArea(latitude_of_projection_origin=11.35, longitude_of_central_meridian=-4.3, false_easting=0.0, false_northing=0.0, standard_parallels=(16.12,6.58), ellipsoid=iris.coord_systems.GeogCS(semi_major_axis=6378137.0, inverse_flattening=298.257223563))
laic.coord('longitude').coord_system = crs_aea
laic.coord('latitude').coord_system = crs_aea
laic.coords('longitude')[0].standard_name='projection_x_coordinate'
laic.coords('latitude')[0].standard_name='projection_y_coordinate'
laic.coords('projection_x_coordinate')[0].units='metres'
laic.coords('projection_y_coordinate')[0].units='metres'

#identify nans: 
dstack = np.isnan(lai_lv_corr.mean(dim='time')).stack(c=['longitude','latitude'])
dstackfull = np.isnan(lai_lv_corr).stack(c=['longitude','latitude'])
dstack2 = dstack[dstack]
# create sample points for the iris interpolation: 
sample_points = [("projection_x_coordinate",[coord[0] for coord in dstack2.c.data]),\
("projection_y_coordinate",[coord[1] for coord in dstack.c.data])]

interpolated = laic.interpolate(sample_points,iris.analysis.Nearest())
tp = iris.analysis.interpolate.extract_nearest_neighbour(laic,sample_points)

"""

#~ lai.lai_lv.mean(dim='time').plot(xlim=xlim,ylim=ylim,figsize=(6,8),cmap='viridis')

# solution 3:
# use scipy interpolate to fill nans
lai_lv_corr2 = copy.deepcopy(lai_lv_corr)
from scipy import interpolate
tmpmean = copy.deepcopy(lai_lv_corr.mean(dim='time'))
tmpamp = copy.deepcopy(lai_lv_corr.max(dim='time') - lai_lv_corr.min(dim='time'))
tmpmean = tmpmean.where((tmpmean>lai_mean_after_corr_threshold) & (tmpamp>lai_amp_after_corr_threshold))
x = tmpmean.longitude.values
y = tmpmean.latitude.values
#mask invalid values
array = np.ma.masked_invalid(tmpmean.data)
xx, yy = np.meshgrid(x, y)
#get only the valid values
x1 = xx[~array.mask]
y1 = yy[~array.mask]
for i in range(len(lai_lv_corr.time)):
    tmp2 = copy.deepcopy(lai_lv_corr.isel(time=i))
    newarr = tmp2.data[~array.mask]
    GD1 = interpolate.griddata((x1, y1), newarr.ravel(),
                          (xx, yy),
                             method='nearest')
    lai_lv_corr2 = xr.where(lai_lv_corr2.coords['time']==lai_lv_corr2.coords['time'][i],GD1,lai_lv_corr2)
                       
lai_lv_corr2 = xr.where(lai_lv_corr2<laimin,laimin,lai_lv_corr2)

# control plot:
lai_lv_corr2.mean(dim='time').plot(xlim=xlim,ylim=ylim,figsize=(6,8),cmap='viridis')
(lai_lv_corr2.max(dim='time') - lai_lv_corr2.min(dim="time")).plot(xlim=xlim,ylim=ylim,figsize=(6,8),cmap='viridis')

# get ERA5 bias corrected Precip for period 2000-2008
p = xr.open_dataset(P_file)
PY = p.resample(time='Y').sum()
PYtmp = p.resample(time='Y').sum().sel(time=slice('2000-1-1','2008-12-31'))
PYref = PYtmp.Rainf.mean(dim='time')
PYref =PYref.rename({'lon':'longitude',"lat":'latitude'})
PY =PY.rename({'lon':'longitude',"lat":'latitude'})

PYref_int = PYref.interp_like(lai_lv_corr2)
PY2 = PY.rename({'time':'time_tmp'})
PY_int = PY2.interp_like(lai_lv_corr2).rename({'time_tmp':'time'})
corr_ratio = (PY_int/PYref_int)
corr_ratio.mean(dim='time').Rainf.plot(xlim=xlim,ylim=ylim,figsize=(6,8),cmap='viridis')

#https://xarray.pydata.org/en/stable/examples/monthly-means.html

# fill in the data by replicating daily LAI available for a climatological year
# and already pre processed and rescaled by the yearly P anomaly (P/Pref) for each ERA5 pixel

dP = corr_ratio.Rainf.data
dL = lai_lv_corr2.data
dt = pd.date_range(start='1981-1-1',end='2020-12-31')
laicorr = np.zeros((len(dt),dL.shape[1],dL.shape[2]))
startind = 0
for i,yr in enumerate(dt.year.unique()):
    yrlen = len(dt.year[dt.year==yr])
    laicorr[startind:startind+yrlen,:,:] = dL[0:yrlen,:,:] * dP[i,:,:]
    startind+=yrlen

LAIcorr_b = xr.DataArray(laicorr, coords=[dt, lai_lv_corr2.latitude, lai_lv_corr2.longitude], dims=["time", "latitude",'longitude'])
LAIcorr_b.mean(dim='time').plot(xlim=xlim,ylim=ylim,figsize=(6,8),cmap='viridis')

# prepare LAI haut
lai_hv_corr = lai.lai_hv.interpolate_na(dim='longitude',method='nearest').fillna(0)
lai_hv_corr.mean(dim='time').plot(xlim=xlim,ylim=ylim,figsize=(6,8),cmap='viridis')
dLh = lai_hv_corr.data
dt = pd.date_range(start='1981-1-1',end='2020-12-31')
laicorr_h = np.zeros((len(dt),dLh.shape[1],dLh.shape[2]))
startind = 0
for i,yr in enumerate(dt.year.unique()):
    yrlen = len(dt.year[dt.year==yr])
    laicorr_h[startind:startind+yrlen,:,:] = dLh[0:yrlen,:,:]
    startind+=yrlen
LAIcorr_h = xr.DataArray(laicorr_h, coords=[dt, lai_hv_corr.latitude, lai_hv_corr.longitude], dims=["time", "latitude",'longitude'])
LAIcorr_h.mean(dim='time').plot(xlim=xlim,ylim=ylim,figsize=(6,8),cmap='viridis')

#temporary save:
LAIcorr_h.to_netcdf("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/LAI_corr_h.nc")
LAIcorr_b.to_netcdf("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/LAI_corr_b.nc")
LAIcorr_b.isel(time = (LAIcorr_b.time.dt.year==2000)).to_netcdf("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/LAI_corr_b_2000.nc")
# prepare SAI: 

# take any year as LAI timing is climatological (but not the amplitude as we rescaled it.
dtmp1 =  copy.deepcopy(LAIcorr_b.sel(time=LAIcorr_b.time.dt.year.isin([2000])))
dtmp2 =  copy.deepcopy(dtmp1)
# argmax: If there are multiple maxima, the indices of the first one found will be returned.
# issue for instance in lat=8, lon=2)
# eg look at lai or directly
# maxLAIidx.sel(latitude=8,longitude=2,method='nearest')

# so reverse the data first
dtmp1.data = dtmp1.data[::-1,:,:]
maxLAIidx = 366-dtmp1.argmax(dim='time',skipna=True)
#dtmp2 = dtmp.reindex(time=list(reversed(dtmp.time)))

minSAIidx = dtmp2.argmin(dim='time',skipna=True)
minSAIidx = minSAIidx.where(minSAIidx>225,365+minSAIidx)
maxSAIidx = minSAIidx - 30


# add one year to allow interpolate last year and cut back afterwards
dt_sai = pd.date_range(start='1981-1-1',end='2021-12-31')

maxLAIidxs = np.zeros((len(dt_sai.year.unique()),dL.shape[1],dL.shape[2]))
maxSAIidxs = np.zeros((len(dt_sai.year.unique()),dL.shape[1],dL.shape[2]))
minSAIidxs = np.zeros((len(dt_sai.year.unique()),dL.shape[1],dL.shape[2]))

sai_code = np.zeros((len(dt_sai),dL.shape[1],dL.shape[2]))-2

startind=0
for i,yr in enumerate(dt_sai.year.unique()):
    yrlen = len(dt_sai.year[dt_sai.year==yr])
    maxLAIidxs[i,:,:] = maxLAIidx.data + startind
    minSAIidxs[i,:,:] = minSAIidx.data + startind
    maxSAIidxs[i,:,:] = maxSAIidx.data + startind
    startind+=yrlen

"""
code: 
-2 :        do nothing (0.5)
-1 :        increase of SAI: calculate SAI from LAI
1 to 30:    the decrease period of SAI: go from max LAI to 0.5 with increment given by number of days (the code) x slope
"""
    
for i in range(maxLAIidxs.shape[1]):
    for j in range(maxLAIidxs.shape[2]):
        for t,yr in enumerate(dt_sai.year.unique()):
            sai_code[maxLAIidxs[t,i,j].astype(int):maxSAIidxs[t,i,j].astype(int),i,j] =  - 1 # increase
            if t == len(dt_sai.year.unique()) - 1: # last year 
                ndays_last_year = len(dt_sai) - maxSAIidxs[t,i,j] 
                sai_code[maxSAIidxs[t,i,j].astype(int):minSAIidxs[t,i,j].astype(int),i,j] = np.arange(0,ndays_last_year) # decrease
            else:
                sai_code[maxSAIidxs[t,i,j].astype(int):minSAIidxs[t,i,j].astype(int),i,j] = np.arange(0,30) # decrease

sai_code_xr = xr.DataArray(sai_code[0:LAIcorr_b.shape[0]], coords=[dt, lai_hv_corr.latitude, lai_hv_corr.longitude], dims=["time", "latitude",'longitude'])

# this is the SAI calculation:
sai_b = xr.DataArray(0.5, coords=[dt, lai_hv_corr.latitude, lai_hv_corr.longitude], dims=["time", "latitude",'longitude'])
for i,yr in enumerate(dt_sai.year.unique()[0:-1]):
    curr_max = LAIcorr_b.isel(time = (LAIcorr_b.time.dt.year==yr)).max(dim='time')
    sai_b = xr.where((sai_code_xr==-1) &  (sai_code_xr.time.dt.year==yr),curr_max - LAIcorr_b + 0.5,sai_b)
    sai_b = xr.where((sai_code_xr>=0) &  (sai_code_xr.time.dt.year==yr),curr_max - sai_code_xr*(curr_max - 0.5)/30,sai_b)

# control plot: 
lat = 9
lon = 2

sai_b.sel(latitude=lat,longitude=lon,method='nearest').plot(label='sai')
LAIcorr_b.sel(latitude=lat,longitude=lon,method='nearest').plot(label='lai')
lai.lai_lv.sel(latitude=lat,longitude=lon,method='nearest').plot(label='lai orig')
plt.legend()

#temporary save:
sai_b.to_netcdf("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/SAI_corr_b.nc")
sai_b.isel(time = (sai_b.time.dt.year==2000)).to_netcdf("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/SAI_corr_b_2000.nc")

# HVEG: displa herbacees LAI +  SAI et trouver la relation pour que la moyenne climatique soit ~ 2.5m en hveg puis x 0.67
# 

hveg_b = sai_b + LAIcorr_b
# control plot: 
lat = 8
lon = 2

sai_b.sel(latitude=lat,longitude=lon,method='nearest').plot(label='sai')
LAIcorr_b.sel(latitude=lat,longitude=lon,method='nearest').plot(label='lai')
hveg_b.sel(latitude=lat,longitude=lon,method='nearest').plot(label='hveg')
plt.legend()
# la moyenne climatique est variable autour de 2.37 sur la gamme de latitude 7 à 11... donc ok
hveg_b.resample(time='Y').max().mean(dim='time').plot()
hveg_b.resample(time='Y').max().sel(latitude=slice(11,7)).mean()


# project LAI haut and bas sur PF grid:
# lire une grille parflow en .tif (grille de destination)
import rasterio
tappan2000 = xr.open_rasterio("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/Oume_Nokoue_2000_lulc_1km_4classes.tif")
tappan2000c = tappan2000.to_iris()

LAIh = LAIcorr_h.to_iris()
LAIb = LAIcorr_b.to_iris()
SAIb = sai_b.to_iris()
HVEGb = hveg_b.to_iris()

""" assign metadata: THIS IS NEEDED ! """

import iris
#: iris.coord_systems.GeogCS(semi_major_axis=6378137.0, inverse_flattening=298.257223563) = WGS84... see https://github.com/SciTools/iris/blob/main/lib/iris/analysis/cartography.py crtl+f WGS
crs_aea = iris.coord_systems.AlbersEqualArea(latitude_of_projection_origin=11.35, longitude_of_central_meridian=-4.3, false_easting=0.0, false_northing=0.0, standard_parallels=(16.12,6.58), ellipsoid=iris.coord_systems.GeogCS(semi_major_axis=6378137.0, inverse_flattening=298.257223563))
crs_geo = iris.coord_systems.GeogCS(semi_major_axis=6378137.0, inverse_flattening=298.257223563)
LAIh.coord('longitude').coord_system = crs_geo
LAIh.coord('latitude').coord_system = crs_geo
LAIh.coords('longitude')[0].standard_name='longitude'
LAIh.coords('latitude')[0].standard_name='latitude'
LAIh.coord('longitude').units = "degrees"
LAIh.coord('latitude').units = "degrees"

LAIb.coord('longitude').coord_system = crs_geo
LAIb.coord('latitude').coord_system = crs_geo
LAIb.coords('longitude')[0].standard_name='longitude'
LAIb.coords('latitude')[0].standard_name='latitude'
LAIb.coord('longitude').units = "degrees"
LAIb.coord('latitude').units = "degrees"

SAIb.coord('longitude').coord_system = crs_geo
SAIb.coord('latitude').coord_system = crs_geo
SAIb.coords('longitude')[0].standard_name='longitude'
SAIb.coords('latitude')[0].standard_name='latitude'
SAIb.coord('longitude').units = "degrees"
SAIb.coord('latitude').units = "degrees"

HVEGb.coord('longitude').coord_system = crs_geo
HVEGb.coord('latitude').coord_system = crs_geo
HVEGb.coords('longitude')[0].standard_name='longitude'
HVEGb.coords('latitude')[0].standard_name='latitude'
HVEGb.coord('longitude').units = "degrees"
HVEGb.coord('latitude').units = "degrees"


tappan2000c.coord('x').coord_system = crs_aea
tappan2000c.coord('y').coord_system = crs_aea
tappan2000c.coords('x')[0].standard_name='projection_x_coordinate'
tappan2000c.coords('y')[0].standard_name='projection_y_coordinate'
tappan2000c.coords('projection_x_coordinate')[0].units='metres'
tappan2000c.coords('projection_y_coordinate')[0].units='metres'

print('lecture des donnees/conversion Iris.............. ok')


"""regrid: projection + interpolation)"""
print('regrille.............. ')

#~ PCaea,ext = iris.analysis.cartography.project(Pc, crs_aea)
# test : regrille seulement la carte 2D: 
#~ LAIb_aea = LAIb.regrid(tappan2000c,iris.analysis.Linear())
#~ LAIh_aea = LAIh.regrid(tappan2000c,iris.analysis.Linear())

# regrille l'ensemble du netcdf: passe par un regridder pour ne pas refaire le mapping a chaque fois: 
# deux methodes: lineaire ou nearest: comparons les
#~ regridder_nearest = iris.analysis.Nearest().regridder(Pc,maskc)
regridder_linear_b = iris.analysis.Linear().regridder(LAIb,tappan2000c)
regridder_linear_h = iris.analysis.Linear().regridder(LAIh,tappan2000c)
regridder_linear_bsai = iris.analysis.Linear().regridder(SAIb,tappan2000c)
regridder_linear_bhveg = iris.analysis.Linear().regridder(HVEGb,tappan2000c)

print('regrille.............. mapping ok')

#~ Pcfull_aea_near = regridder_nearest(Pcfull)
# reconverti en xarray (plus simple / immediat pour les manips):
#~ p_aea_near = xr.DataArray().from_iris(Pcfull_aea_near)
# delete the large array when not needed anymore (to avoid memomry issues)
#~ del Pcfull_aea_near
#~ p_aea_nearY = p_aea_near.groupby('time.year').sum().mean(dim='year')

#~ print('regrille.............. nearest ok')

LAIb_aea_lin = regridder_linear_b(LAIb)
LAIh_aea_lin = regridder_linear_h(LAIh)
SAIb_aea_lin = regridder_linear_bsai(SAIb)
HVEGb_aea_lin = regridder_linear_bhveg(HVEGb)

# reconverti en xarray (plus simple / immediat pour les manips):
laib_aea = xr.DataArray().from_iris(LAIb_aea_lin)
laih_aea = xr.DataArray().from_iris(LAIh_aea_lin)
saib_aea = xr.DataArray().from_iris(SAIb_aea_lin)
hvegb_aea = xr.DataArray().from_iris(HVEGb_aea_lin)

#tappan2000 = xr.DataArray().from_iris(tappan2000c)

laib_aea.to_netcdf("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/LAI_corr_b_aea.nc")
laih_aea.to_netcdf("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/LAI_corr_h_aea.nc")
saib_aea.to_netcdf("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/SAI_corr_b_aea.nc")
hvegb_aea.to_netcdf("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/HVEG_corr_b_aea.nc")

#~ laib_aea.mean(dim='time').plot(figsize=(6,8),cmap='viridis')

#~ lai = xr.where(tappan2000.band==8,laib_aea*0.21+laih_aea*0.79,laib_aea*0.63+laih_aea*0.37)
#~ sai = xr.where(tappan2000.band==8,saib_aea*0.21+2*0.79,saib_aea*0.63+2*0.37)
#~ displa = xr.where(tappan2000.band==8,6*0.67,hvegb_aea*0.67)
#~ z0m = xr.where(tappan2000.band==8,6*0.097+0.17,hvegb_aea*0.097+0.17)

lai = xr.where(tappan2000==8,laib_aea*0.21+laih_aea*0.79,laib_aea*0.63+laih_aea*0.37)
sai = xr.where(tappan2000==8,saib_aea*0.21+2*0.79,saib_aea*0.63+2*0.37)
#~ del(laib_aea)
#~ del(laih_aea)
#~ del(saib_aea)

displa = xr.where(tappan2000==8,6*0.67,hvegb_aea*0.67)
z0m = xr.where(tappan2000==8,6*0.097+0.17,hvegb_aea*0.097+0.17)


#~ del(hvegb_aea)

#~ lai.to_netcdf("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/LAI_aea.nc")
#~ sai.to_netcdf("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/SAI_aea.nc")
#hveg.to_netcdf("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/HVEG_aea.nc")
#~ displa.to_netcdf("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/DISPLA_aea.nc")
#~ z0m.to_netcdf("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/ZOM_aea.nc")

print('regrille.............. linear ok')


# daily to hourly

""" write PFBs """
print('save_pfb.............. ')

# divide by 10 days periods
NT = 14610 # days
NT = 370 # days
n_nt=np.floor(NT/10) # number of periods
#~ nt=10*48 # number of timesteps per period
nt=10*24 # number of timesteps per period
hout={}
hout['nx'] = 216
hout['ny'] = 420
hout['nz'] = nt
hout['nx2'] = 216
hout['ny2'] = 420
hout['nz2'] = nt
hout['dx'] = 1000
hout['dy'] = 1000
hout['dz'] = 1
hout['X0'] = 634500
hout['Y0'] = -539500
hout['Z0'] = 0
hout['ns'] = 1
hout['rx'] = 1000
hout['ry'] = 1000
hout['rz'] = 1
hout['ix'] = 0
hout['iy'] = 0
hout['iz'] = 0

for period in range(int(n_nt)):
    hout['Z0'] = period*nt+1
    pfbfilename_lai = os.sep.join([outfolder,'LAI','Forc.LAI.%06d_to_%06d.pfb'%(period*nt+1,(period+1)*nt)])
    pfbfilename_sai = os.sep.join([outfolder,'SAI','Forc.SAI.%06d_to_%06d.pfb'%(period*nt+1,(period+1)*nt)])
    pfbfilename_displa = os.sep.join([outfolder,'DISPLA','Forc.DISPLA.%06d_to_%06d.pfb'%(period*nt+1,(period+1)*nt)])
    pfbfilename_z0m = os.sep.join([outfolder,'Z0M','Forc.Z0M.%06d_to_%06d.pfb'%(period*nt+1,(period+1)*nt)])
    print(pfbfilename_lai)
    
    PFB_write(np.swapaxes(lai.isel(time=slice(period*10,(period+1)*10+1)).resample(time='1H').interpolate('linear').isel(time=slice(0,-1)).data,0,2),hout,pfbfilename_lai)
    PFB_write(np.swapaxes(sai.isel(time=slice(period*10,(period+1)*10+1)).resample(time='1H').interpolate('linear').isel(time=slice(0,-1)).data,0,2),hout,pfbfilename_sai)
    PFB_write(np.swapaxes(displa.isel(time=slice(period*10,(period+1)*10+1)).resample(time='1H').interpolate('linear').isel(time=slice(0,-1)).data,0,2),hout,pfbfilename_displa)
    PFB_write(np.swapaxes(z0m.isel(time=slice(period*10,(period+1)*10+1)).resample(time='1H').interpolate('linear').isel(time=slice(0,-1)).data,0,2),hout,pfbfilename_z0m)
    #~ PFB_write(Outdata,hout,pfbfilename)
print('save_pfb.............. ok')

