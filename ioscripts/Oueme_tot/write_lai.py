#-*- coding: utf-8 -*-


__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2022"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import xarray as xr
import copy
import os,struct
"""local imports:"""

plt.close()
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

lai = xr.open_dataset("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/LAI_aea.nc")
#~ sai = xr.open_dataset("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/SAI_aea.nc")
#hveg.to_netcdf("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/HVEG_aea.nc")
#~ displa = xr.open_dataset("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/DISPLA_aea.nc")
#~ z0m = xr.open_dataset("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/ZOM_aea.nc")
outfolder = "/homelocal5/phyrev/_PARFLOW/projects/Oueme/active/Oueme/forcings/FORC/"

lai['lai'] = lai['__xarray_dataarray_variable__']
lai = lai.drop(['__xarray_dataarray_variable__'])
dat = lai.sel(time=slice('1981-01-01','1982-12-31'))

#~ sai['sai'] = sai['__xarray_dataarray_variable__']
#~ sai = sai.drop(['__xarray_dataarray_variable__'])
#~ dat = sai.sel(time=slice('1981-01-01','1982-12-31'))

#~ displa['displa'] = displa['__xarray_dataarray_variable__']
#~ displa = displa.drop(['__xarray_dataarray_variable__'])
#~ dat = displa.sel(time=slice('1981-01-01','1982-12-31'))

#~ z0m['z0m'] = z0m['__xarray_dataarray_variable__']
#~ z0m = z0m.drop(['__xarray_dataarray_variable__'])
#~ dat = z0m.sel(time=slice('1981-01-01','1982-12-31'))
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def PFB_write(pfarray, pfb_hdr_dict, pfb_fp):
    print("**This code does NOT handle subgrids**")
    with open(pfb_fp,'wb') as pfbf:
        pfbf.write(struct.pack('>d',pfb_hdr_dict['X0']))
        pfbf.write(struct.pack('>d',pfb_hdr_dict['Y0']))
        pfbf.write(struct.pack('>d',pfb_hdr_dict['Z0']))
        pfbf.write(struct.pack('>i',pfb_hdr_dict['nx']))
        pfbf.write(struct.pack('>i',pfb_hdr_dict['ny']))
        pfbf.write(struct.pack('>i',pfb_hdr_dict['nz']))
        pfbf.write(struct.pack('>d', pfb_hdr_dict['dx']))
        pfbf.write(struct.pack('>d', pfb_hdr_dict['dy']))
        pfbf.write(struct.pack('>d', pfb_hdr_dict['dz']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['ns']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['ix']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['iy']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['iz']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['nx']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['ny']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['nz']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['rx']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['ry']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['rz']))
            
        print ("wrote header")
        nx = pfb_hdr_dict['nx']
        ny = pfb_hdr_dict['ny']
        nz = pfb_hdr_dict['nz']
                
        for lays in range(nz):
            # repeat writing of layer 1 for Noah-WRF layers
            # 1, 2, & 3 (0.10 m, 0.30 m, + 0.60 m dz)
            #print("Writing layer %s") %(lays+1)
            for rows in range(ny):
                   for cols in range(nx):
                       if nz == 1:
                           val = pfarray[cols, rows]
                       else:
                           val = pfarray[cols, rows, lays]
                       pfbf.write(struct.pack('>d', val))
    print("Wrote pfb output")
    return(1)


print('regrille.............. linear ok')


# daily to hourly

""" write PFBs """
print('save_pfb.............. ')

# divide by 10 days periods
NT = 14610 # days
NT = 5*370 # days
n_nt=np.floor(NT/10) # number of periods
#~ nt=10*48 # number of timesteps per period
nt=10*24 # number of timesteps per period
hout={}
hout['nx'] = 216
hout['ny'] = 420
hout['nz'] = nt
hout['nx2'] = 216
hout['ny2'] = 420
hout['nz2'] = nt
hout['dx'] = 1000
hout['dy'] = 1000
hout['dz'] = 1
hout['X0'] = 634500
hout['Y0'] = -539500
hout['Z0'] = 0
hout['ns'] = 1
hout['rx'] = 1000
hout['ry'] = 1000
hout['rz'] = 1
hout['ix'] = 0
hout['iy'] = 0
hout['iz'] = 0

for period in range(int(n_nt)):
    hout['Z0'] = period*nt+1
    pfbfilename_lai = os.sep.join([outfolder,'LAI','Forc.LAI.%06d_to_%06d.pfb'%(period*nt+1,(period+1)*nt)])
    #~ pfbfilename_sai = os.sep.join([outfolder,'SAI','Forc.SAI.%06d_to_%06d.pfb'%(period*nt+1,(period+1)*nt)])
    #~ pfbfilename_displa = os.sep.join([outfolder,'DISPLA','Forc.DISPLA.%06d_to_%06d.pfb'%(period*nt+1,(period+1)*nt)])
    #~ pfbfilename_z0m = os.sep.join([outfolder,'Z0M','Forc.Z0M.%06d_to_%06d.pfb'%(period*nt+1,(period+1)*nt)])
    print(pfbfilename_lai)
    #~ print(pfbfilename_sai)
    #~ print(pfbfilename_displa)
    #~ print(pfbfilename_z0m)
    dat_array = lai.lai.isel(time=slice(period*10,(period+1)*10+1)).resample(time='1H').interpolate('linear').isel(time=slice(0,-1)).data
    #~ dat_array = sai.sai.isel(time=slice(period*10,(period+1)*10+1)).resample(time='1H').interpolate('linear').isel(time=slice(0,-1)).data
    #~ dat_array = displa.displa.isel(time=slice(period*10,(period+1)*10+1)).resample(time='1H').interpolate('linear').isel(time=slice(0,-1)).data
    #~ dat_array = z0m.z0m.isel(time=slice(period*10,(period+1)*10+1)).resample(time='1H').interpolate('linear').isel(time=slice(0,-1)).data
    PFB_write(np.swapaxes(dat_array,0,2),hout,pfbfilename_lai)
    #~ PFB_write(np.swapaxes(dat_array,0,2),hout,pfbfilename_sai)
    #~ PFB_write(np.swapaxes(dat_array,0,2),hout,pfbfilename_displa)
    #~ PFB_write(np.swapaxes(dat_array,0,2),hout,pfbfilename_z0m)
    #~ PFB_write(np.swapaxes(sai.isel(time=slice(period*nt,(period+1)*nt)).resample(time='1H').interpolate('linear').data,0,2),hout,pfbfilename_sai)
    #~ PFB_write(np.swapaxes(displa.isel(time=slice(period*nt,(period+1)*nt)).resample(time='1H').interpolate('linear').data,0,2),hout,pfbfilename_displa)
    #~ PFB_write(np.swapaxes(z0m.isel(time=slice(period*nt,(period+1)*nt)).resample(time='1H').interpolate('linear').data,0,2),hout,pfbfilename_z0m)
    #~ PFB_write(Outdata,hout,pfbfilename)
print('save_pfb.............. ok')
