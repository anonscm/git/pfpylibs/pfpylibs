#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    Open land cover map already processed, modify (veg class 8->7) then 
    write as pfb and drv_vegm

    @copyright: 2022 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2022"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import xarray as xr
import rioxarray as rioxr

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb
from PFlibs import PFpostproc




##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

tappan1975 = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/Oume_Nokoue_1975_lulc_1km_4classes.tif"
tappan2000 = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/Oume_Nokoue_2000_lulc_1km_4classes.tif"
tappan2013 = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme/processing/VEG/Oume_Nokoue_2015_lulc_1km_4classes.tif"

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##



tap75 = rioxr.open_rasterio(tappan1975)
tap00 = rioxr.open_rasterio(tappan2000)
tap13 = rioxr.open_rasterio(tappan2013)

## TODO check que les forçages soient OK avec une carte tappan avec des nans

