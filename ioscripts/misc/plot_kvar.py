#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    plot variable Ks

    @copyright: 2019 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2019"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import glob,os
import matplotlib.pyplot as plt


"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
simdir = '/home/hectorb/PARFLOW/PROJECTS/Hillslope/steering-Python/'
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def exponential_decrease_with_cumulative_rainfall(cums, ks, ks_factor=5, cum_min=100, threshold=150, tau=10):
    """ From GQ:
    decreasing efolding as a function of cumulative precip.
    cum_min (mm) corresponds to the cumulative precip leading to the first weeding (sarclage)
    threshold (mm) gives weeding recurrence
    """
    def f(c):
        if c < cum_min:
            return ks_min
        else:
            r = np.mod((c + (threshold - cum_min)), threshold)
        return ks_min + (ks_max - ks_min) * np.exp(-r/tau)

    ks_min = ks
    ks_max = ks * ks_factor

    return cums.apply(f)

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##


""" Read Forcings: """
forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
forc = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')
forc.rename(columns=forcing_headers,inplace=True)
forc.index=pd.date_range('1/1/2005 00:00:00', periods=len(forc), freq='30min')
forc['P'] = forc['P']*30*60


forc['Ks'] = exponential_decrease_with_cumulative_rainfall(np.cumsum(forc['P']), ks = 10**(-7), ks_factor=5, cum_min=100, threshold=150, tau=10)

fig = plt.figure(figsize=(16,8))
ax = fig.add_subplot(111)

#~ p = ax.plot(forc['Ks'])
p = ax.semilogy(forc['Ks'])
ax.set_ylabel('Ks (m/s)',fontsize=20)


