#-*- coding: utf-8 -*-
"""
    PFPYLIBS - ioscripts

    Utility code to swap vegetation classes in a work dir
    mostly used to swap veg class 7 (open shrublands) & 8 (woody savannah)
    which have been confusingly mixed (class 7 being used for trees)

    Either propose a new suffix to each file, or create a new subfolder and write the new files in there (or both)
    
    ATTENTION: this is actually NOT RELEVANT  because confusingly, 
    open shrublands has actually a denser wood cover 
    (it's closer to the evergreen needleleaf forest')
    so 7 is forest and 8 is savannah
    
    @copyright: 2019 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2019"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import datetime
import matplotlib.pyplot as plt
import os
import copy
"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb
from PFlibs import drv_vegm_io as vegmio

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2019/'
outdir = simdir
#~ outdir = os.sep.join([simdir,'vegetation_class_swapped/'])

filename_drv_vegm = 'drv_vegm_vshape.dat'
filename_drv_vegm = 'drv_vegm_treesvshape.dat'
filename_drv_vegm = 'drv_vegm_herbvshape.dat'

filename_vegmap = 'veg_map.pfb'
filename_vegmap = 'veg_map_trees.pfb'
filename_vegmap = 'veg_map_herb.pfb'

filename_lai = 'lai.dat'
filename_sai = 'sai.dat'
filename_displa = 'displa.dat'
filename_z0m = 'z0m.dat'


suffix = '_swap_7_8.'
#~ suffix = '.'


#~ plot_LAI = True
#~ plot_SAI = True
#~ plot_DISPLA = True
#~ plot_Z0M = True

plot_LAI = False
plot_SAI = False
plot_DISPLA = False
plot_Z0M = False

vegclasses={1:'evergreen needleleaf forests',2:'evergreen broadleaf forests',\
			3:'deciduous needleleaf forests', 4: 'deciduous broadleaf forests',\
			5: 'mixed forests6 closed shrublands', 6:'closed shrublands',\
			7: 'open shrublands', 8: 'woody savannas',\
			9: 'savannas', 10: 'grasslands',\
			11: 'permanent wetlands', 12: 'croplands',\
			13: 'urban and built-up lands', 14: 'cropland / natural vegetation mosaics',\
			15: 'snow and ice', 16: 'barren or sparsely vegetated',\
			17: 'water bodies', 18: 'bare soil',\
			19: 'herbaceous'}	
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

""" Change vegetation maps"""

out_filename_drv_vegm = suffix.join(filename_drv_vegm.split('.'))
out_filename_vegmap = suffix.join(filename_vegmap.split('.'))

#~ [x,y,veg]=vegmio.read_vegm(os.sep.join([simdir,filename]),plot=True)
[x,y,veg]=vegmio.read_vegm(os.sep.join([simdir,filename_drv_vegm]),plot=False)
data,hdr = io_pfb.PFB_read(os.sep.join([simdir,filename_vegmap]),1)

veg[veg == 7.] = 42
veg[veg == 8.] = 7
veg[veg == 42] = 8
[XX,YY,vegveg]=vegmio.veg_to_vegveg(x,y,veg)

# WRITE DRV_VEGM
vegmio.write_vegm(x,y,veg,os.sep.join([outdir,out_filename_drv_vegm]))
# WRITE VEG_MAP
io_pfb.PFB_write(np.swapaxes(vegveg,0,1), hdr, pfb_fp=os.sep.join([outdir,out_filename_vegmap]))


""" Change vegetation TS """

#LAI
out_filename_lai = suffix.join(filename_lai.split('.'))

df=pd.read_csv(os.sep.join([simdir,filename_lai]),header=None, delimiter=r"\s+")
tmp = copy.deepcopy(df[6])
df[6] = df[7]
df[7] = tmp
df.to_csv(outdir+out_filename_lai,float_format='%2.4f', sep=' ', header=False, index=False)

if plot_LAI:
    df.rename(columns=lambda x: x+1, inplace=True)
    df = df.drop(df.columns[np.append(np.arange(0,6),np.arange(8,18))],axis=1)
    df.rename(columns=lambda x: vegclasses[x], inplace=True)
    df.plot()
    plt.show()

#SAI
out_filename_sai = suffix.join(filename_sai.split('.'))

df=pd.read_csv(os.sep.join([simdir,filename_sai]),header=None, delimiter=r"\s+")
tmp = copy.deepcopy(df[6])
df[6] = df[7]
df[7] = tmp
df.to_csv(outdir+out_filename_sai,float_format='%2.4f', sep=' ', header=False, index=False)

if plot_SAI:
    df.rename(columns=lambda x: x+1, inplace=True)
    df = df.drop(df.columns[np.append(np.arange(0,6),np.arange(8,18))],axis=1)
    df.rename(columns=lambda x: vegclasses[x], inplace=True)
    df.plot()
    plt.show()

#displa
out_filename_displa = suffix.join(filename_displa.split('.'))

df=pd.read_csv(os.sep.join([simdir,filename_displa]),header=None, delimiter=r"\s+")
tmp = copy.deepcopy(df[6])
df[6] = df[7]
df[7] = tmp
df.to_csv(outdir+out_filename_displa,float_format='%2.4f', sep=' ', header=False, index=False)

if plot_DISPLA:
    df.rename(columns=lambda x: x+1, inplace=True)
    df = df.drop(df.columns[np.append(np.arange(0,6),np.arange(8,18))],axis=1)
    df.rename(columns=lambda x: vegclasses[x], inplace=True)
    df.plot()
    plt.show()

#Z0M
out_filename_z0m = suffix.join(filename_z0m.split('.'))

df=pd.read_csv(os.sep.join([simdir,filename_z0m]),header=None, delimiter=r"\s+")
tmp = copy.deepcopy(df[6])
df[6] = df[7]
df[7] = tmp
df.to_csv(outdir+out_filename_z0m,float_format='%2.4f', sep=' ', header=False, index=False)

if plot_Z0M:
    df.rename(columns=lambda x: x+1, inplace=True)
    df = df.drop(df.columns[np.append(np.arange(0,6),np.arange(8,18))],axis=1)
    df.rename(columns=lambda x: vegclasses[x], inplace=True)
    df.plot()
    plt.show()
