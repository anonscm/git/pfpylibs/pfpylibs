#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Anaylse PF outputs for Vshape paper


    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import pandas as pd
import time
import os,glob
import matplotlib.pyplot as plt
import datetime
import re
#Added for handling NC (to be moved to another library)
#~ import kolinkelly as kk
#~ import joestork as js
from netCDF4 import Dataset

"""local imports:"""
from procsycz import readDataAMMA as rdA
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def roundDateTime(shakyDT,nearest_minutes=1):
	"""
	"""
	shakyDT += datetime.timedelta(minutes=nearest_minutes/2)
	shakyDT -= datetime.timedelta(minutes=shakyDT.minute % nearest_minutes,
                         seconds=shakyDT.second,
                         microseconds=shakyDT.microsecond)
	return shakyDT
	
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
simuname = 'ref'
#~ simuname = 'ref_n13'
#~ simuname = 'sap'
#~ simuname = 'mix'
#~ simuname = 'noBF'
#~ simuname = 'herb'
#~ simuname = 'trees'

simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_2018_03_13'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_n13_2018_03_28'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/sap_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/mix_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/noBF_2018_03_29'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/herb_2018_03_13'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/trees_2018_03_29'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_2020_01_20'

n_coupled_layers = 13
n_CLM_2Dlayers = 14

"""
CLM_var is a dictionnary where keys are local variable names as in PFCLM
values are lists of string[filename, PFCLM variable name, long name, units]
"""
CLM_var={'lh_tot':['','eflx_lfh_tot','latent heat','W/m2'],\
        'lwrad_out':['','eflx_lwrad_out','outgoing lw radiation','W/m2'],\
        'sh_tot':['','eflx_sh_tot','sensible heat','W/m2'],\
        'soil_grnd':['','eflx_soil_grnd','ground heat flux','W/m2'],\
        'evap_tot':['','qflx_evap_tot','total evapotranspiration','mm/s'],\
        'evap_ground':['','qflx_evap_grnd','ground evaporation without condesation','mm/s'],\
        'evap_soil':['','qflx_evap_soi','soil evaporation','mm/s'],\
        'evap_veg':['','qflx_evap_veg','vegetation evaporation + transpiration','mm/s'],\
        'tran_veg':['','qflx_tran_veg','vegetation transpiration','mm/s'],\
        't_grnd':['','t_grnd','Ground temperature','K'],\
        'surf_press':['','h_top','Surface pressure','m'],\
        }
""" add the filename in the dic"""        
for k,val in CLM_var.items():
    CLM_var[k][0]=os.sep.join([simdir,''.join([simuname,'_'+val[1]+'.nc'])])
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

#### Read & process simulation outputs:
sim = PFout.PFout(simdir,'doro')

#sim.set_reading_parameters(n_t=2921,n_t_clm=70080,n_t_forc = 70080*2, dump_int=24,dump_int_clm=1,conv=1,conv_clm=1 * 60 * 60, conv_forc= 1 * 60 * 30)

"""
sim.read_domain_dimensions_from_PFBs()
sim.read_allPFoutput_from_PFBs()
sim.read_CLMoutput(nlayers = n_coupled_layers+n_CLM_2Dlayers)
#~ sim.extract_CLM_2Doutputs(nlayers = 16 +14 )
sim.read_allconstant_outputs()
sim.read_forcings()
sim.calculate_runoff(x_outlet = 7,y_outlet = -1)
#~ sim.calculate_WSC()
"""
"""NetCDF"""
sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_eflx_lh_tot.nc'])]),filetype='CLM')

"""Read PF outputs"""
start_time = time.time()
with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
    #~ sim.satur_array = ds.variables['saturation'][:]
    sim.satur_array = np.transpose(ds.variables['saturation'][:],[3,2,1,0])
    topmask=np.swapaxes(ds.variables['top_mask'][:],0,1)
    vegmap=np.swapaxes(ds.variables['veg_map'][:],0,1)
    xslopes=np.swapaxes(ds.variables['xslope'][:],0,1)
    
"""MASK is a nx x ny boolean mask of the domain
When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
(as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
"""
MASK=topmask>0 
with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
    sim.press_array = ds.variables['succion'][:] 

""" Read constant files"""
ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
#watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,2)
sim.mannings_arr = np.zeros((sim.nx,sim.ny))+5.5e-6
sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,1)
sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,2)
sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,1)
sim.dz = np.diff(ds.variables['depth'][:])[0]
sim.z = np.cumsum(sim.dz_mult_arr[0,0,::-1]*sim.dz) - sim.dz_mult_arr[0,0,::-1]*sim.dz/2
sim.z = np.round(sim.z*1000)/1000

#~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))
""" Read Forcings: """
forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
#~ forc = pd.read_csv(os.sep.join([simdir,'forcagePF.txt.0']),header=None,sep='\t')
forc = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')

forc.rename(columns=forcing_headers,inplace=True)
forc.index=pd.date_range('1/1/2005 00:00:00', periods=len(forc), freq='30min')
sim.P=sum(forc['P'])*30*60


""" Get SM data: """
rt_dir = r'/home/hectorb/DATA/TDR/CE.SW_Odc_BD_AMMA-CATCH_2018_03_27'
suf_pattern = '.csv'
station_list = {'NAH':['NALOHOU_500H']}
TDR=rdA.StaDic(name='TDR')
TDR.readTDRFiles_from_pre_suf(station_list,rt_dir,pre_pattern='CE.SW_Odc',suf_pattern='.csv')
dat=TDR['NAH'].tdr
dat[dat<0] = np.nan
dat = dat.sort_index()

"""get columns depths from their string value...."""
depths=[''.join(re.split(r'(\d+)', s)[0:-1]) for s in dat.columns]  
units=[''.join(re.split(r'(\d+)', s)[-1]) for s in dat.columns]                         
depths = [ float(val) if units[i]=='m' else float(val)/100 for i, val in enumerate(depths) ] 
dat = dat[dat.columns[np.argsort(depths)]]
depths = np.sort(depths)
ind_to_keep=[]
for ind, col in enumerate(dat.columns):
    if dat[col].isnull().sum() > 5*365*24*2:
        dat = dat.drop(col,axis=1)
    else:
        ind_to_keep.append(ind)
depths = depths[ind_to_keep]

""" remove 10cm depth because no according simulated depth..."""
dat = dat.drop(dat.columns[1],axis = 1)
depths = np.delete(depths,1)

""" Format simulated SM"""
SM=pd.DataFrame()
#~ for k, z in enumerate(sim.z):

for z in depths:
    zsim_ind = np.argmin(abs(sim.z-z))
    zsim=sim.z[zsim_ind]
    #~ SM[zsim] = sim.satur_array[:,sim.nz-1-zsim_ind,2,12]*sim.porosity_arr[12,2,sim.nz-1-zsim_ind]
    #~ SM[zsim] = sim.satur_array[13,0,sim.nz-1-zsim_ind,:]*sim.porosity_arr[13,0,sim.nz-1-zsim_ind]
    SM[zsim] = sim.satur_array[13,3,sim.nz-1-zsim_ind,:]*sim.porosity_arr[13,3,sim.nz-1-zsim_ind]

SM.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t, freq='D')
SM['P']=(forc['P']*30 * 60).resample('D').sum()

#~ SM =SM.drop(SM.columns[5:sim.nz],axis=1)


fig,ax = plt.subplots(nrows=len(SM.columns),figsize=(15,10),sharex=True, squeeze=True)

for i in range(len(ax)):
    if (SM.columns[i] == 'P'):
        #~ ax[i].plot(SM[SM.columns[i]])
        ax[i].bar(SM.index,SM[SM.columns[i]].values,width=1,facecolor='b')
        ax[i].legend(['Precipitation'])
        ax[i].set_ylabel('mm/d')
    else:
        ax[i].plot(dat[dat.columns[i]],'k')        
        ax[i].plot(SM[SM.columns[i]],'r')
        ax[i].legend([''.join(['obs: ',dat.columns[i]]),''.join(['sim: ',str(int(np.round(SM.columns[i]*100))),'cm'])],loc='upper left',ncol = 2)
        ax[i].set_ylabel('SM')
        ax[i].set_ylim([0, 0.39])
        #~ ax[i].set_xlim([min(SM.index), max(SM.index)])
        ax[i].set_xlim([datetime.datetime(2006,1,1), max(SM.index)])

fig.subplots_adjust(bottom=0.03, top =0.95, hspace=0.001)

#~ plt.savefig(os.sep.join([simdir,'SM.png']),dpi=400,format='png')
