#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Anaylse PF outputs for Vshape paper
    ET figure

    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import pandas as pd
import time
import os,glob
import matplotlib.pyplot as plt
import datetime
#Added for handling NC (to be moved to another library)
#~ import kolinkelly as kk
#~ import joestork as js
from netCDF4 import Dataset

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from procsycz import readDataAMMA as rdA

plt.close("all")

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def roundDateTime(shakyDT,nearest_minutes=1):
	"""
	"""
	shakyDT += datetime.timedelta(minutes=nearest_minutes/2)
	shakyDT -= datetime.timedelta(minutes=shakyDT.minute % nearest_minutes,
                         seconds=shakyDT.second,
                         microseconds=shakyDT.microsecond)
	return shakyDT

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

n_coupled_layers = 13
n_CLM_2Dlayers = 14

"""
CLM_var is a dictionnary where keys are local variable names as in PFCLM
values are lists of string[filename, PFCLM variable name, long name, units]
"""
CLM_var={'lh_tot':['','eflx_lfh_tot','latent heat','W/m2'],\
        'lwrad_out':['','eflx_lwrad_out','outgoing lw radiation','W/m2'],\
        'sh_tot':['','eflx_sh_tot','sensible heat','W/m2'],\
        'soil_grnd':['','eflx_soil_grnd','ground heat flux','W/m2'],\
        'evap_tot':['','qflx_evap_tot','total evapotranspiration','mm/s'],\
        'evap_ground':['','qflx_evap_grnd','ground evaporation without condesation','mm/s'],\
        'evap_soil':['','qflx_evap_soi','soil evaporation','mm/s'],\
        'evap_veg':['','qflx_evap_veg','vegetation evaporation + transpiration','mm/s'],\
        'tran_veg':['','qflx_tran_veg','vegetation transpiration','mm/s'],\
        't_grnd':['','t_grnd','Ground temperature','K'],\
        'surf_press':['','h_top','Surface pressure','m'],\
        }

    
vegclasses={1:'evergreen needleleaf forests',2:'evergreen broadleaf forests',\
			3:'deciduous needleleaf forests', 4: 'deciduous broadleaf forests',\
			5: 'mixed forests6 closed shrublands', 6:'closed shrublands',\
			7: 'open shrublands', 8: 'woody savannas',\
			9: 'savannas', 10: 'grasslands',\
			11: 'permanent wetlands', 12: 'croplands',\
			13: 'urban and built-up lands', 14: 'cropland / natural vegetation mosaics',\
			15: 'snow and ice', 16: 'barren or sparsely vegetated',\
			17: 'water bodies', 18: 'bare soil',\
			19: 'herbaceous'}	
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

def getSimET(simdir,simuname='ref'):    
    """make a function of all the reading part"""
    
    """ add the filename in the dic"""        
    for k,val in CLM_var.items():
        CLM_var[k][0]=os.sep.join([simdir,''.join([simuname,'_'+val[1]+'.nc'])])    
    #### Read & process simulation outputs:
    sim = PFout.PFout(simdir,'doro')
    """NetCDF"""
    sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
    sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_eflx_lh_tot.nc'])]),filetype='CLM')
    """Read PF outputs"""
    start_time = time.time()
    with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
        sim.satur_array = ds.variables['saturation'][:]
        topmask=np.swapaxes(ds.variables['top_mask'][:],0,1)
        vegmap=np.swapaxes(ds.variables['veg_map'][:],0,1)
        xslopes=np.swapaxes(ds.variables['xslope'][:],0,1)
    
    """MASK is a nx x ny boolean mask of the domain
    When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
    (as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
    """
    MASK=topmask>0 
    with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
        sim.press_array = ds.variables['succion'][:] 

    """Read CLM outputs"""
    for k,val in CLM_var.items():
        try:
            with Dataset(val[0], 'r') as ds:
                print('read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
                exec('sim.%s=np.swapaxes(ds.variables[\'%s\'][:],0,2)'%(k,val[1]))        
        except FileNotFoundError:
            print('CANNOT read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
    print("--- %d seconds --- read all PFCLM outputs from NCs" % (time.time() - start_time))

    """ Read constant files"""
    ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
    #watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
    sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,1)
    sim.mannings_arr = np.zeros((sim.nx,sim.ny))+5.5e-6
    sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,1)
    sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
    sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
    sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,1)
    sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,1)




    """Create CLM output DataFrames: """
    CLM_varsubset=['evap_tot','evap_ground','evap_soil','evap_veg','tran_veg']


    """For the ET mean min max, remove bas-fond cells and left bank (Flux tower is on right bank)"""
    vegmaprightbank=vegmap
    vegmaprightbank[0:8,:]=0
    vegmaprightbank[0:9,:]=0
    dsclmD=pd.DataFrame()
    dsclmD_min=pd.DataFrame()
    dsclmD_max=pd.DataFrame()
    dsclmDherb=pd.DataFrame()
    dsclmDherb_min=pd.DataFrame()
    dsclmDherb_max=pd.DataFrame()
    dsclmDtrees=pd.DataFrame()
    dsclmDtrees_min=pd.DataFrame()
    dsclmDtrees_max=pd.DataFrame()   
    for k in CLM_varsubset:
        exec('dsclmD[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[MASK].mean(axis=0)'%k)
        exec('dsclmD_min[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[MASK].min(axis=0)'%k)
        exec('dsclmD_max[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[MASK].max(axis=0)'%k)
        
        exec('dsclmDherb[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[(vegmaprightbank==19.) & (MASK)].mean(axis=0)'%k)
        exec('dsclmDherb_min[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[(vegmaprightbank==19.) & (MASK)].min(axis=0)'%k)
        exec('dsclmDherb_max[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[(vegmaprightbank==19.) & (MASK)].max(axis=0)'%k)
        
        exec('dsclmDtrees[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[(vegmaprightbank==7.) & (MASK)].mean(axis=0)'%k)
        exec('dsclmDtrees_min[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[(vegmaprightbank==7.) & (MASK)].min(axis=0)'%k)
        exec('dsclmDtrees_max[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[(vegmaprightbank==7.) & (MASK)].max(axis=0)'%k)
        
    dsclmD.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
    dsclmD_min.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
    dsclmD_max.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')

    dsclmDherb.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
    dsclmDherb_min.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
    dsclmDherb_max.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')

    dsclmDtrees.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
    dsclmDtrees_min.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
    dsclmDtrees_max.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
    return dsclmD,dsclmD_min,dsclmD_max,dsclmDherb,dsclmDherb_min,dsclmDherb_max,dsclmDtrees,dsclmDtrees_min,dsclmDtrees_max

simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_n13_2018_03_28'
simuname = 'ref_n13'

simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_slope_parshall_2018_08_22'
simuname = 'ref_slope_parshall'


[tot,totmin,totmax,refherb,refherbmin,refherbmax,reftrees,reftreesmin,reftreesmax] = getSimET(simdir,simuname)

simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/sap_2018_03_28'
simuname = 'sap'
[tot,totmin,totmax,sapherb,sapherbmin,sapherbmax,saptrees,saptreesmin,saptreesmax] = getSimET(simdir,simuname)


""" Get ET data: see readDataAMMA for hardwired paths to change if the data is elsewhere """
ETdata = rdA.getLASET_and_oldECET_Nalohou()
naloD = rdA.getNaloET()
beleD = rdA.getBeleET()
ETdata.rename(columns={'LE_EC':'ETobs'},inplace=True)
naloD=pd.concat([naloD,ETdata])
beleD=pd.concat([beleD,ETdata['LE_LAS']],axis=1)

""" Read Forcings: """
forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
#~ forc = pd.read_csv(os.sep.join([simdir,'forcagePF.txt.0']),header=None,sep='\t')
forc = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')
forc.rename(columns=forcing_headers,inplace=True)
forc.index=pd.date_range('1/1/2005 00:00:00', periods=len(forc), freq='30min')
P=(forc['P']*30*60).resample('D').sum()

""" Get VegForc """
LAI=pd.read_csv(r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/lai.dat',header=None, delimiter=r"\s+")
LAI.rename(columns=lambda x: x+1, inplace=True)
LAI = LAI[[7,19]]
#~ LAI.rename(columns=lambda x: vegclasses[x], inplace=True)
LAI['time']=pd.date_range('1/1/2005 00:00:00','1/1/2016 00:00:00', freq='30min')
LAI=LAI.set_index('time')

SAI=pd.read_csv(r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/sai.dat',header=None, delimiter=r"\s+")
SAI.rename(columns=lambda x: x+1, inplace=True)
SAI = SAI[[7,19]]

#~ SAI.rename(columns=lambda x: vegclasses[x], inplace=True)
SAI['time']=pd.date_range('1/1/2005 00:00:00','1/1/2016 00:00:00', freq='30min')
SAI=SAI.set_index('time')


#~ fig,ax = plt.subplots(nrows=5,figsize=(15,10),sharex=True, squeeze=True)
#~ fig,ax = plt.subplots(nrows=6,figsize=(15,13),sharex=True, squeeze=True)
#A4 in inches is 8.3 * 11.7
fig,ax = plt.subplots(nrows=6,figsize=(7.5,6.5),sharex=True, squeeze=True)

ax[0].plot(LAI[19],'g')
ax[0].plot(LAI[7],'g--')
ax[0].plot(SAI[19],'k')
ax[0].plot(SAI[7],'k--')

#~ ax[0].legend(['LAI herbaceous','LAI trees','SAI herbaceous','SAI trees'],loc='upper left',ncol=4,fontsize=17)
ax[0].legend(['LAI herbaceous','LAI trees','SAI herbaceous','SAI trees'],loc='upper left',ncol=4,fontsize=9)
#~ ax[0].set_ylabel('Index',fontsize=17)
ax[0].set_ylabel('Index',fontsize=9)
ax[0].set_xlim([datetime.datetime(2006,1,1), datetime.datetime(2010,12,31)])
ax[0].set_ylim([0,5])
#~ ax[0].text(datetime.datetime(2010,11,20),4.1,r'a)',FontSize=17)
ax[0].text(datetime.datetime(2010,11,20),4.25,r'a)',FontSize=9)
#~ ax[0].tick_params(axis='both', which='major', labelsize=17)
ax[0].tick_params(axis='both', which='major', labelsize=9)

linessim,=ax[1].plot(refherb['evap_tot'],linewidth=0.5)
ax[1].fill_between(refherb['evap_tot'].index,\
    np.reshape(np.array(refherbmin['evap_tot']),len(totmin),1),\
    np.reshape(np.array(refherbmax['evap_tot']),len(totmax),1),\
    alpha=0.5)
linesLASobs,=ax[1].plot(naloD['LE_LAS'],color='b',marker='+',markersize=1,linestyle='-',linewidth=0.2)
ax[1].plot(naloD['ETobs'],color='r',marker='+',markersize=1,linestyle='-',linewidth=0.2)
linesECobs,=ax[1].plot(naloD[0],color='r',marker='+',markersize=1,linestyle='-',linewidth=0.2)
#~ ax[1].legend((linessim,linesECobs,linesLASobs),('sim (ref): herbaceous','obs: EC','obs: LAS'),loc='upper left',ncol=3,fontsize=17)
ax[1].legend((linessim,linesECobs,linesLASobs),('sim (ref): herbaceous','obs: EC','obs: LAS'),loc='upper left',ncol=3,fontsize=9)
#~ ax[1].set_ylabel('mm/d',fontsize=17)
ax[1].set_ylabel('mm/d',fontsize=9)
ax[1].set_xlim([datetime.datetime(2006,1,1), datetime.datetime(2010,12,31)])
#~ ax[1].set_ylim([0,7])
ax[1].set_ylim([0,7.9])
#~ ax[1].text(datetime.datetime(2010,11,20),6.8,r'b)',FontSize=17)
ax[1].text(datetime.datetime(2010,11,20),6.8,r'b)',FontSize=9)
#~ ax[1].tick_params(axis='both', which='major', labelsize=17)
ax[1].tick_params(axis='both', which='major', labelsize=9)

linessim,=ax[2].plot(reftrees['evap_tot'],linewidth=0.5)
ax[2].fill_between(reftrees['evap_tot'].index,\
    np.reshape(np.array(reftreesmin['evap_tot']),len(totmin),1),\
    np.reshape(np.array(reftreesmax['evap_tot']),len(totmax),1),\
    alpha=0.5)
linesLASobs,=ax[2].plot(beleD['LE_LAS'],color='b',marker='+',markersize=1,linestyle='-',linewidth=0.2)
linesECobs,=ax[2].plot(beleD['ETobs'],color='r',marker='+',markersize=1,linestyle='-',linewidth=0.2)
#~ ax[2].legend((linessim,linesECobs,linesLASobs),('sim (ref): trees','obs: EC','obs: LAS'),loc='upper left',ncol=3,fontsize=17)
ax[2].legend((linessim,linesECobs,linesLASobs),('sim (ref): trees','obs: EC','obs: LAS'),loc='upper left',ncol=3,fontsize=9)
#~ ax[2].set_ylabel('mm/d',fontsize=17)
ax[2].set_ylabel('mm/d',fontsize=9)
ax[2].set_xlim([datetime.datetime(2006,1,1), datetime.datetime(2010,12,31)])
#~ ax[2].set_ylim([0,7])
ax[2].set_ylim([0,7.9])
#~ ax[2].text(datetime.datetime(2010,11,20),6.8,r'c)',FontSize=17)
ax[2].text(datetime.datetime(2010,11,20),6.8,r'c)',FontSize=9)
#~ ax[2].tick_params(axis='both', which='major', labelsize=17)
ax[2].tick_params(axis='both', which='major', labelsize=9)

linessim,=ax[3].plot(sapherb['evap_tot'],linewidth=0.5)
ax[3].fill_between(sapherb['evap_tot'].index,\
    np.reshape(np.array(sapherbmin['evap_tot']),len(totmin),1),\
    np.reshape(np.array(sapherbmax['evap_tot']),len(totmax),1),\
    alpha=0.5)
linesLASobs,=ax[3].plot(naloD['LE_LAS'],color='b',marker='+',markersize=1,linestyle='-',linewidth=0.2)
ax[3].plot(naloD['ETobs'],color='r',marker='+',markersize=1,linestyle='-',linewidth=0.2)
linesECobs,=ax[3].plot(naloD[0],color='r',marker='+',markersize=1,linestyle='-',linewidth=0.2)
#~ ax[3].legend((linessim,linesECobs,linesLASobs),('sim (saprolite): herbaceous','obs: EC','obs: LAS'),loc='upper left',ncol=3,fontsize=17)
ax[3].legend((linessim,linesECobs,linesLASobs),('sim (saprolite): herbaceous','obs: EC','obs: LAS'),loc='upper left',ncol=3,fontsize=9)
#~ ax[3].set_ylabel('mm/d',fontsize=17)
ax[3].set_ylabel('mm/d',fontsize=9)
ax[3].set_xlim([datetime.datetime(2006,1,1), datetime.datetime(2010,12,31)])
#~ ax[3].set_ylim([0,7])
ax[3].set_ylim([0,7.9])
#~ ax[3].text(datetime.datetime(2010,11,20),6.8,r'd)',FontSize=17)
ax[3].text(datetime.datetime(2010,11,20),6.8,r'd)',FontSize=9)
#~ ax[3].tick_params(axis='both', which='major', labelsize=17)
ax[3].tick_params(axis='both', which='major', labelsize=9)

linessim,=ax[4].plot(saptrees['evap_tot'],linewidth=0.5)
ax[4].fill_between(saptrees['evap_tot'].index,\
    np.reshape(np.array(saptreesmin['evap_tot']),len(totmin),1),\
    np.reshape(np.array(saptreesmax['evap_tot']),len(totmax),1),\
    alpha=0.5)
linesLASobs,=ax[4].plot(beleD['LE_LAS'],color='b',marker='+',markersize=1,linestyle='-',linewidth=0.2)
linesECobs,=ax[4].plot(beleD['ETobs'],color='r',marker='+',markersize=1,linestyle='-',linewidth=0.2)
#~ ax[4].legend((linessim,linesECobs,linesLASobs),('sim (saprolite): trees','obs: EC','obs: LAS'),loc='upper left',ncol=3,fontsize=17)
ax[4].legend((linessim,linesECobs,linesLASobs),('sim (saprolite): trees','obs: EC','obs: LAS'),loc='upper left',ncol=3,fontsize=9)
#~ ax[4].set_ylabel('mm/d',fontsize=17)
ax[4].set_ylabel('mm/d',fontsize=9)
ax[4].set_xlim([datetime.datetime(2006,1,1), datetime.datetime(2010,12,31)])
#~ ax[4].set_ylim([0,7])
ax[4].set_ylim([0,7.9])
#~ ax[4].text(datetime.datetime(2010,11,20),6.8,r'e)',FontSize=17)
ax[4].text(datetime.datetime(2010,11,20),6.8,r'e)',FontSize=9)
#~ ax[4].tick_params(axis='both', which='major', labelsize=17)
ax[4].tick_params(axis='both', which='major', labelsize=9)

#~ linesprecip,=ax[5].plot(P,color='b')
ax[5].bar(P.index,P.values,width=1,facecolor='b')
#~ ax[5].set_ylabel('mm/d',fontsize=17)
ax[5].set_ylabel('mm/d',fontsize=9)
#~ ax[5].legend(['Precipitation'],loc='upper left',fontsize=17)
ax[5].legend(['Precipitation'],loc='upper left',fontsize=9)
ax[5].set_xlim([datetime.datetime(2006,1,1), datetime.datetime(2010,12,31)])
ax[5].set_ylim([0,90])
#~ ax[5].tick_params(axis='both', which='major', labelsize=17)
ax[5].tick_params(axis='both', which='major', labelsize=9)
#~ ax[5].text(datetime.datetime(2010,11,20),75,r'f)',FontSize=17)
ax[5].text(datetime.datetime(2010,11,20),75,r'f)',FontSize=9)
#~ fig.subplots_adjust(bottom=0.03, top =0.95, hspace=0.001)
fig.subplots_adjust(bottom=0.05, top =0.99, left =0.07, right =0.99,hspace=0.001)

simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_n13_2018_03_28'

#~ plt.savefig(os.sep.join([simdir,'Figure_simobs_ET.png']),dpi=400,format='png')
#~ plt.savefig(os.sep.join([simdir,'Figure_simobs_ET.pdf']),dpi=400,format='pdf')

