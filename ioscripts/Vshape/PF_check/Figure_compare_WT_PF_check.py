#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    DESCRIPTION
    This script is made to intercompare two similar simulations to check for any difference in PF code
    e.g. when checking out a new pf version of when testing out some new features
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime
import os,glob
import time
from netCDF4 import Dataset

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
plt.close('all')


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
simuname = 'ref'
simuname = 'ref_n13'
#~ simuname = 'sap'
#~ simuname = 'mix'
#~ simuname = 'noBF'
#~ simuname = 'herb'
#~ simuname = 'trees'

simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_2018_03_13'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_n13_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/sap_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/mix_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/noBF_2018_03_29'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/herb_2018_03_13'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/trees_2018_03_29'

n_coupled_layers = 13
n_CLM_2Dlayers = 14
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

def getSimWT(simdir,simuname='ref',xloc=0,yloc=0):
    """
    make a function of all the reading part
    """
    sim = PFout.PFout(simdir,'doro')

    sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
    sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_eflx_lh_tot.nc'])]),filetype='CLM')

    """Read PF outputs"""
    start_time = time.time()
    with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
        sim.satur_array = np.transpose(ds.variables['saturation'][:],[3,2,1,0])
        topmask=np.swapaxes(ds.variables['top_mask'][:],0,1)
    """MASK is a nx x ny boolean mask of the domain
    When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
    (as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
    """
    MASK=topmask>0 
    with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
        sim.press_array = np.transpose(ds.variables['succion'][:],[3,2,1,0])

    """ Read constant files"""
    ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
    #watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
    sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,2)
    sim.mannings_arr = np.zeros((sim.nx,sim.ny))+5e-6
    sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,1)
    sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
    sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
    sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,2)
    sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,2)
    #~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))
    sim.dz = np.diff(ds.variables['depth'][:])[0]
    sim.z = np.cumsum(sim.dz_mult_arr[0,0,::-1]*sim.dz) - sim.dz_mult_arr[0,0,::-1]*sim.dz/2
    sim.z = np.round(sim.z*1000)/1000




    """ extract a T-D slice """
    # x= 140 y = 430 according to Matlab script:
    xloc=xloc
    yloc = yloc

    domain_thickness = np.round(np.cumsum(sim.dz_mult_arr[0,0,::-1]*sim.dz)[-1]*1000)/1000
    depth_last_cell = sim.z[-1]
    WT = domain_thickness - sim.press_array[xloc,yloc,0,:]
    WT = depth_last_cell - sim.press_array[xloc,yloc,0,:]


    """ Get All Water tables (perched)"""
    #first get a mask of the base layers of each saturated (p>0) contiguous (over Z) unit
    maskWT=np.zeros([sim.nz,sim.n_t])
    for t in range(sim.n_t):
        profile = np.zeros(sim.nz)
        #profile[0] = base of domain. if the criteria is p < 0 => 0, you realize there are some weird points at the bottom
        #of perched layers with p>0 but < sim.dz ... so as if the cell is not entirely saturated but p>0 ... 
        profile = np.array([0. if sim.press_array[xloc,yloc,i,t]<= sim.dz_mult_arr[xloc,yloc,i]*sim.dz else 1. for i in range(sim.nz)])
        for i in range(sim.nz-1):
            profile[i+1]=(profile[i]+profile[i+1])*profile[i+1]
        # a=[1,1,1,0,0,0,1,1,0,1,0,0,0,1] => [1, 2, 3, 0, 0, 0, 1, 2, 0, 1, 0, 0, 0, 1]
        profile[profile!=1.]=np.nan
        maskWT[:,t] = profile

    WTs = np.resize(sim.z[::-1],[sim.n_t,sim.nz]).T - (maskWT*sim.press_array[xloc,yloc,:,:])
    return WTs

# x= 140 y = 430 according to Matlab script:

xloc=8-1
xloc=7-1
yloc=23-1
simuname = 'ref_n13'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_n13_2018_03_28'
WTsref = getSimWT(simdir,simuname,xloc,yloc)

simuname = 'sap'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/sap_2018_03_28'
WTssap = getSimWT(simdir,simuname,xloc,yloc)

simuname = 'mix'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/mix_2018_03_28'
WTsmix = getSimWT(simdir,simuname,xloc,yloc)

simuname = 'noBF'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/noBF_2018_03_29'
WTsnoBF = getSimWT(simdir,simuname,xloc,yloc)

simuname = 'herb'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/herb_2018_03_13'
WTsherb = getSimWT(simdir,simuname,xloc,yloc)

simuname = 'trees'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/trees_2018_03_29'
WTstrees = getSimWT(simdir,simuname,xloc,yloc)


# x= 30 y = 0 according to Matlab script:

xloc=15-1
yloc=4-1
simuname = 'ref_n13'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_n13_2018_03_28'
WTsref_side = getSimWT(simdir,simuname,xloc,yloc)

#~ simuname = 'sap'
simuname = 'ref'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/sap_2018_03_28'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_2020_01_17'
WTssap_side = getSimWT(simdir,simuname,xloc,yloc)

simuname = 'mix'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/mix_2018_03_28'
WTsmix_side = getSimWT(simdir,simuname,xloc,yloc)

simuname = 'noBF'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/noBF_2018_03_29'
WTsnoBF_side = getSimWT(simdir,simuname,xloc,yloc)

simuname = 'herb'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/herb_2018_03_13'
WTsherb_side = getSimWT(simdir,simuname,xloc,yloc)

simuname = 'trees'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/trees_2018_03_29'
WTstrees_side = getSimWT(simdir,simuname,xloc,yloc)


""" Read Forcings: """
forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
forc = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')
forc.rename(columns=forcing_headers,inplace=True)
forc.index=pd.date_range('1/1/2005 00:00:00', periods=len(forc), freq='30min')

""" Plot """
tsim = pd.date_range('1/1/2005 00:00:00', periods=np.shape(WTsref)[1], freq='D')
#~ fig, ax = plt.subplots(3, 2,figsize=(20,10))
#A4 in inches is 8.3 * 11.7
fig, ax = plt.subplots(3, 2,figsize=(8,4))

for i in range(np.shape(WTsref)[0]):
    #~ lines1, = ax[0,0].plot(tsim,WTsref[i,:],'k',linewidth=2.0)
    lines1, = ax[0,0].plot(tsim,WTsref[i,:],'k',linewidth=0.7)
for i in range(np.shape(WTsref)[0]):
    #~ lines2, = ax[0,0].plot(tsim,WTsnoBF[i,:],'r',linewidth=1.0)
    lines2, = ax[0,0].plot(tsim,WTsnoBF[i,:],'r',linewidth=0.5)
ax[0,0].set_xlim([datetime.datetime(2007,1,1),max(tsim)])
ax[0,0].set_ylim([0,7.9])
#~ ax[0,0].legend((lines1,lines2),('ref','no I. Valley'),loc='lower left',fontsize=16,ncol=2)
#~ ax[0,0].set_ylabel('WT depth (m)', fontsize=16, color = 'k')
#~ ax[0,0].tick_params(axis='both', which='major', labelsize=16,bottom='on',top='on',right='on',left='on')
ax[0,0].legend((lines1,lines2),('ref','no I. Valley'),loc='lower left',fontsize=7,ncol=2)
ax[0,0].set_ylabel('WT depth (m)', fontsize=9, color = 'k')
ax[0,0].tick_params(axis='both', which='major', labelsize=9,bottom='on',top='on',right='on',left='on')
ax[0,0].tick_params(axis='x', which='both', labelbottom='off', labeltop='off')
ax[0,0].text(datetime.datetime(2007,1,20),0.6,r'a)',FontSize=9)

for i in range(np.shape(WTsref)[0]):
    #~ lines1, = ax[1,0].plot(tsim,WTsref[i,:],'k',linewidth=2.0)
    lines1, = ax[1,0].plot(tsim,WTsref[i,:],'k',linewidth=0.7)
for i in range(np.shape(WTsref)[0]):
    #~ lines2, = ax[1,0].plot(tsim,WTstrees[i,:],'r',linewidth=1.0)
    lines2, = ax[1,0].plot(tsim,WTstrees[i,:],'r',linewidth=0.5)
for i in range(np.shape(WTsref)[0]):
    lines3, = ax[1,0].plot(tsim,WTsherb[i,:],'b',linewidth=0.5)
ax[1,0].set_xlim([datetime.datetime(2007,1,1),max(tsim)])
ax[1,0].set_ylim([0,7.9])
#~ ax[1,0].legend((lines1,lines2,lines3),['ref','trees','herb'],loc='lower left',fontsize=16,ncol=3)
#~ ax[1,0].set_ylabel('WT depth (m)', fontsize=16, color = 'k')
#~ ax[1,0].tick_params(axis='both', which='major', labelsize=16,bottom='on',top='on',right='on',left='on')
ax[1,0].legend((lines1,lines2,lines3),['ref','trees','herb'],loc='lower left',fontsize=7,ncol=3)
ax[1,0].set_ylabel('WT depth (m)', fontsize=9, color = 'k')
ax[1,0].tick_params(axis='both', which='major', labelsize=9,bottom='on',top='on',right='on',left='on')
ax[1,0].tick_params(axis='x', which='both', labelbottom='off', labeltop='off')
ax[1,0].text(datetime.datetime(2007,1,20),0.6,r'b)',FontSize=9)

for i in range(np.shape(WTsref)[0]):
    #~ lines1, = ax[2,0].plot(tsim,WTsref[i,:],'k',linewidth=2.0)
    lines1, = ax[2,0].plot(tsim,WTsref[i,:],'k',linewidth=0.7)
for i in range(np.shape(WTsref)[0]):
    #~ lines2, = ax[2,0].plot(tsim,WTssap[i,:],'r',linewidth=1.0)
    lines2, = ax[2,0].plot(tsim,WTssap[i,:],'r',linewidth=0.5)
for i in range(np.shape(WTsref)[0]):
    lines3, = ax[2,0].plot(tsim,WTsmix[i,:],'b',linewidth=0.5)
ax[2,0].set_xlim([datetime.datetime(2007,1,1),max(tsim)])
ax[2,0].set_ylim([0,7.9])
#~ ax[2,0].set_ylabel('WT depth (m)', fontsize=16, color = 'k')
#~ ax[2,0].tick_params(axis='both', which='major', labelsize=16,bottom='on',top='on',right='on',left='on')
#~ ax[2,0].text(datetime.datetime(2007,1,20),0.6,r'c)',FontSize=18)
ax[2,0].set_ylabel('WT depth (m)', fontsize=9, color = 'k')
ax[2,0].tick_params(axis='both', which='major', labelsize=9,bottom='on',top='on',right='on',left='on')
ax[2,0].text(datetime.datetime(2007,1,20),0.6,r'c)',FontSize=9)

ax1 = ax[2,0].twinx()
ax1.plot(forc['P'].resample('D').sum()*30*60,'b')
ax1.set_ylim([0,190])
ax1.set_yticks([0,20,40,60])
ax1.yaxis.set_label_position("right")
ax1.tick_params(axis='y',colors='b')
#~ ax1.set_ylabel('Precip (mm/d)', fontsize=16, color = 'b')
ax1.tick_params(axis='both', which='major', labelright='off')

#~ ax1.legend((lines1,lines2,lines3),['ref','saprolite','mixed'],loc='lower left',fontsize=16,ncol=3)
ax1.legend((lines1,lines2,lines3),['ref','saprolite','mixed'],loc='lower left',fontsize=7,ncol=3)


for i in range(np.shape(WTsref)[0]):
    #~ lines1, = ax[0,1].plot(tsim,WTsref_side[i,:],'k',linewidth=2.0)
    lines1, = ax[0,1].plot(tsim,WTsref_side[i,:],'k',linewidth=0.7)
for i in range(np.shape(WTsref)[0]):
    #~ lines2, = ax[0,1].plot(tsim,WTsnoBF_side[i,:],'r',linewidth=1.0)
    lines2, = ax[0,1].plot(tsim,WTsnoBF_side[i,:],'r',linewidth=0.5)
ax[0,1].set_xlim([datetime.datetime(2007,1,1),max(tsim)])
ax[0,1].set_ylim([0,7.9])
#~ ax[0,1].legend((lines1,lines2),['ref','no I. Valley'],loc='upper right',fontsize=16,ncol=2)
#~ ax[0,1].tick_params(axis='both', which='major', labelsize=16,bottom='on',top='on',right='on',left='on')
ax[0,1].legend((lines1,lines2),['ref','no I. Valley'],loc='upper right',fontsize=7,ncol=2)
ax[0,1].tick_params(axis='both', which='major', labelsize=9,bottom='on',top='on',right='on',left='on')
ax[0,1].tick_params(axis='y', which='both', labelleft='off', labelright='on')
ax[0,1].tick_params(axis='x', which='both', labelbottom='off', labeltop='off')
ax[0,1].yaxis.set_label_position("right")
#~ ax[0,1].set_ylabel('WT depth (m)', fontsize=16, color = 'k')
ax[0,1].set_ylabel('WT depth (m)', fontsize=9, color = 'k')
ax[0,1].text(datetime.datetime(2007,1,20),0.6,r'd)',FontSize=9)


for i in range(np.shape(WTsref)[0]):
    #~ lines1, = ax[1,1].plot(tsim,WTsref_side[i,:],'k',linewidth=2.0)
    lines1, = ax[1,1].plot(tsim,WTsref_side[i,:],'k',linewidth=0.7)
for i in range(np.shape(WTsref)[0]):
    #~ lines2, = ax[1,1].plot(tsim,WTstrees_side[i,:],'r',linewidth=1.0)
    lines2, = ax[1,1].plot(tsim,WTstrees_side[i,:],'r',linewidth=0.5)
for i in range(np.shape(WTsref)[0]):
    lines3, = ax[1,1].plot(tsim,WTsherb_side[i,:],'b',linewidth=0.5)
ax[1,1].set_xlim([datetime.datetime(2007,1,1),max(tsim)])
ax[1,1].set_ylim([0,7.9])
#~ ax[1,1].legend((lines1,lines2,lines3),['ref','trees','herb'],loc='upper right',fontsize=16,ncol=3)
#~ ax[1,1].tick_params(axis='both', which='major', labelsize=16,bottom='on',top='on',right='on',left='on')
ax[1,1].legend((lines1,lines2,lines3),['ref','trees','herb'],loc='upper right',fontsize=7,ncol=3)
ax[1,1].tick_params(axis='both', which='major', labelsize=9,bottom='on',top='on',right='on',left='on')
ax[1,1].tick_params(axis='y', which='both', labelleft='off', labelright='on')
ax[1,1].tick_params(axis='x', which='both', labelbottom='off', labeltop='off')
ax[1,1].yaxis.set_label_position("right")
#~ ax[1,1].set_ylabel('WT depth (m)', fontsize=16, color = 'k')
#~ ax[1,1].text(datetime.datetime(2007,1,20),0.6,r'e)',FontSize=18)
ax[1,1].set_ylabel('WT depth (m)', fontsize=9, color = 'k')
ax[1,1].text(datetime.datetime(2007,1,20),0.6,r'e)',FontSize=9)


for i in range(np.shape(WTsref)[0]):
    #~ lines1, = ax[2,1].plot(tsim,WTsref_side[i,:],'k',linewidth=2.0)
    lines1, = ax[2,1].plot(tsim,WTsref_side[i,:],'k',linewidth=0.7)
for i in range(np.shape(WTsref)[0]):
    lines2, = ax[2,1].plot(tsim,WTssap_side[i,:],'r',linewidth=0.5,zorder=50)
for i in range(np.shape(WTsref)[0]):
    lines3, = ax[2,1].plot(tsim,WTsmix_side[i,:],'b',linewidth=0.5)
ax[2,1].set_xlim([datetime.datetime(2007,1,1),max(tsim)])
ax[2,1].set_ylim([0,7.9])
#~ ax[2,1].legend((lines1,lines2,lines3),['ref','saprolite','mixed'],loc='upper right',fontsize=16,ncol=3)
#~ ax[2,1].tick_params(axis='both', which='major', labelsize=16,bottom='on',top='on',right='on',left='on')
ax[2,1].legend((lines1,lines2,lines3),['ref','saprolite','mixed'],loc='upper right',fontsize=7,ncol=3)
ax[2,1].tick_params(axis='both', which='major', labelsize=9,bottom='on',top='on',right='on',left='on')
ax[2,1].tick_params(axis='y', which='both', labelleft='off', labelright='off')
#~ ax[2,1].yaxis.set_label_position("right")
#~ ax[2,1].set_ylabel('WT depth (m)', fontsize=16, color = 'k')
#~ ax[2,1].text(datetime.datetime(2007,1,20),0.6,r'f)',FontSize=18)
ax[2,1].text(datetime.datetime(2007,1,20),0.6,r'f)',FontSize=9)

ax2 = ax[2,1].twinx()
ax2.plot(forc['P'].resample('D').sum()*30*60,'b')
ax2.set_ylim([0,190])
ax2.set_yticks([0,20,40,60])
ax2.yaxis.set_label_position("right")
#~ ax2.tick_params(axis='y',colors='b',labelsize=16)
#~ ax2.set_ylabel('Precip (mm/d)', fontsize=16, color = 'b')
ax2.tick_params(axis='y',colors='b',labelsize=9)
ax2.set_ylabel('Precip (mm/d)', fontsize=9, color = 'b')

for i in range(6):
    fig.axes[i].invert_yaxis()
#~ fig.subplots_adjust(bottom=0.03, top =0.95,wspace=0.04, hspace=0.001)
fig.subplots_adjust(bottom=0.05, top =0.98,left=0.07,right =0.93,wspace=0.04, hspace=0.001)

simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_n13_2018_03_28'
plt.savefig(os.sep.join([simdir,'Figure_compare_WT.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir,'Figure_compare_WT.pdf']),dpi=400,format='pdf')
