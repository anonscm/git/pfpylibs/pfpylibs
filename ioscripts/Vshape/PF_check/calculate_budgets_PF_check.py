#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Anaylse PF outputs for Vshape paper: Budget calculation
    This script is made to intercompare two similar simulations to check for any difference in PF code
    e.g. when checking out a new pf version of when testing out some new features
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import datetime
import pandas as pd
from netCDF4 import Dataset
import os
import time
import glob

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout

plt.close("all")

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
#~ simuname = 'ref'
#~ simuname = 'ref_n13'
simuname = 'ref_PFgit'
simuname = 'ref_slope_parshall'
#simuname = 'ref_n12'
#simuname = 'ref_restol7'
#~ simuname = 'ref_slope2x'
simuname = 'ref_slope05x'
simuname = 'sap'
#~ simuname = 'mix'
#~ simuname = 'noBF'
#~ simuname = 'herb'
#~ simuname = 'trees'
simuname = 'ref_2yrs'

#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_2018_03_13'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_n13_2018_03_28'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_slope_parshall_2018_08_22'

#simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_n12_2018_03_31'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_slope2x_2018_07_03'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_slope05x_2018_07_04'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/sap_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/mix_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/noBF_2018_03_29'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/herb_2018_03_13'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/trees_2018_03_29'
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/ref_2yrs_2020_01_21'
n_coupled_layers = 13
n_CLM_2Dlayers = 14

"""
CLM_var is a dictionnary where keys are local variable names as in PFCLM
values are lists of string[filename, PFCLM variable name, long name, units]
"""
CLM_var={'evap_tot':['','qflx_evap_tot','total evapotranspiration','mm/s'],\
        'evap_soil':['','qflx_evap_soi','soil evaporation','mm/s'],\
        'evap_veg':['','qflx_evap_veg','vegetation evaporation + transpiration','mm/s'],\
        'surf_press':['','h_top','Surface pressure','m'],\
        }

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def PFB_read(pfb_file_path, flag):
    import struct
    import numpy
    #print("**This code DOES handle subgrids**")
    
    hdr_dict = {}
    
    with open(pfb_file_path,'rb') as pfbf:
        hdr_dict['len_x'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['len_y'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['len_z'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['nx'] = struct.unpack('>i', pfbf.read(4))[0]
        hdr_dict['ny'] = struct.unpack('>i', pfbf.read(4))[0]
        hdr_dict['nz'] = struct.unpack('>i', pfbf.read(4))[0]
        hdr_dict['dx'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['dy'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['dz'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['ns'] = struct.unpack('>i', pfbf.read(4))[0]
        ns = hdr_dict['ns']
        nx = hdr_dict['nx']
        ny = hdr_dict['ny']
        nz = hdr_dict['nz']
        
        if nx == 0:
            nx = int(raw_input("What should the NX be?"))
            hdr_dict['nx'] = nx
        if ny == 0:
            ny = int(raw_input("What should the NY be?"))
            hdr_dict['ny'] = ny
        if nz ==0:
            nz =int(raw_input("What should the NZ be?"))
            hdr_dict['nz'] = nz
        
        len_x = hdr_dict['len_x']
        #print "len_x, len_y, len_z, nx ny nz --> %f %f %f %i %i %i" %(len_x, len_x, len_x, nx, ny, nz)
        
        pfb_array = numpy.zeros((nx, ny, nz))
        
        for subgrids in range(ns):
            hdr_dict['ix'] = struct.unpack('>i', pfbf.read(4))[0]
            ix = hdr_dict['ix']
            hdr_dict['iy'] = struct.unpack('>i', pfbf.read(4))[0]
            iy = hdr_dict['iy']
            #print(iy)
            hdr_dict['iz'] = struct.unpack('>i', pfbf.read(4))[0]
            iz = hdr_dict['iz']
            hdr_dict['nx2'] = struct.unpack('>i', pfbf.read(4))[0]
            nx2 = hdr_dict['nx2']
            hdr_dict['ny2'] = struct.unpack('>i', pfbf.read(4))[0]
            ny2 = hdr_dict['ny2']
            #print(ny2)
            hdr_dict['nz2'] = struct.unpack('>i', pfbf.read(4))[0]
            nz2 = hdr_dict['nz2']
            hdr_dict['rx'] = struct.unpack('>i', pfbf.read(4))[0]
            rx = hdr_dict['rx']
            hdr_dict['ry'] = struct.unpack('>i', pfbf.read(4))[0]
            ry = hdr_dict['ry']
            hdr_dict['rz'] = struct.unpack('>i', pfbf.read(4))[0]
            rz = hdr_dict['rz']
            #print("ix iy iz nx2 ny2 nz2 rx ry rz --> %i %i %i %i %i %i %i %i %i\n" %(ix,iy, iz, nx2, ny2, nz2, rx,ry, rz))
            for lays in range(nz2):
                for rows in range(ny2):
                    for cols in range(nx2):
                        val = struct.unpack('>d', pfbf.read(8))[0]
                        #q = raw_input(val)
                        pfb_array[(cols+ix), (rows + iy), (nz2-(lays + iz)-1)] = val
                        #pfb_array[(cols+ix-1), (nx2-(iy)-1), (nz2-(lays + iz)-1)] = val
                        #pfb_array[(rows + iy-1), (cols+ix-1), (nz2-(lays + iz)-1)] = val
                        #print val
                        
    if flag ==1:
        return(pfb_array, hdr_dict)
    else:
        return(pfb_array)

def plot_clustered_stacked(dfall, labels=None,  H="/", **kwargs):
    """Given a list of dataframes, with identical columns and index, create a clustered stacked bar plot. 
labels is a list of the names of the dataframe, used for the legend
title is a string for the title of the plot
H is the hatch used for identification of the different dataframe"""

    n_df = len(dfall)
    n_col = len(dfall[0].columns) 
    n_ind = len(dfall[0].index)
    axe = plt.subplot(111)

    for df in dfall : # for each data frame
        if isinstance(df, pd.DataFrame):
            axe = df.plot(kind="bar",
            linewidth=0,
            stacked=True,
            ax=axe,
            legend=False,
            grid=False,
            **kwargs)  # make bar plots
        else:
             axe = df.plot(kind="bar",
             linewidth=0,
             stacked=True,
             ax=axe,
             legend=False,
             grid=False,
	     color='b',
             **kwargs)  # make bar plots

    h,l = axe.get_legend_handles_labels() # get the handles we want to modify
    for i in range(0, n_df * n_col, n_col): # len(h) = n_col * n_df
        for j, pa in enumerate(h[i:i+n_col]):
            for rect in pa.patches: # for each index
                rect.set_x(rect.get_x() + 1 / float(n_df + 1) * i / float(n_col))
                rect.set_hatch(H * int(i / n_col)) #edited part     
                rect.set_width(1 / float(n_df + 1))

    axe.set_xticks((np.arange(0, 2 * n_ind, 2) + 1 / float(n_df + 1)) / 2.)
    axe.set_xticklabels(df.index, rotation = 0)

    # Add invisible data to add another legend
    n=[]        
    for i in range(n_df):
        n.append(axe.bar(0, 0, color="gray", hatch=H * i))

    l1 = axe.legend(h[:n_col], l[:n_col], loc=[1.01, 0.5])
    if labels is not None:
        l2 = plt.legend(n, labels, loc=[1.01, 0.1]) 
    axe.add_artist(l1)
    return axe

def calc_simulation_budget(simdir,simuname,simu_prefix = 'doro'):
    
    """ add the filename in the dic"""        
    for k,val in CLM_var.items():
        CLM_var[k][0]=os.sep.join([simdir,''.join([simuname,'_'+val[1]+'.nc'])])

    
    #### Read & process simulation outputs:
    sim = PFout.PFout(simdir,simu_prefix)

    """NetCDF"""
    sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
    sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_eflx_lh_tot.nc'])]),filetype='CLM')

    """Read PF outputs"""
    start_time = time.time()
    with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
        sim.satur_array = np.transpose(ds.variables['saturation'][:],[3,2,1,0])
        topmask=np.swapaxes(ds.variables['top_mask'][:],0,1)
    """MASK is a nx x ny boolean mask of the domain
    When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
    (as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
    """
    MASK=topmask>0 
    with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
        sim.press_array = np.transpose(ds.variables['succion'][:],[3,2,1,0])

    """Read CLM outputs"""
    for k,val in CLM_var.items():
        try:
            with Dataset(val[0], 'r') as ds:
                print('read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
                exec('sim.%s=np.swapaxes(ds.variables[\'%s\'][:],0,2)'%(k,val[1]))        
        except FileNotFoundError:
                print('CANNOT read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
    print("--- %d seconds --- read all PFCLM outputs from NCs" % (time.time() - start_time))

    """ Read constant files"""
    ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
    #watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
    sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,2)
    sim.mannings_arr = np.zeros((sim.nx,sim.ny))+5e-6
    sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,1)
    sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
    sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
    sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,2)
    sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,2)
    #~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))
    sim.dz = np.diff(ds.variables['depth'][:])[0]
    sim.z = np.cumsum(sim.dz_mult_arr[0,0,::-1]*sim.dz) - sim.dz_mult_arr[0,0,::-1]*sim.dz/2
    sim.z = np.round(sim.z*1000)/1000


    """ Read Forcings: """
    forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
    #~ forc = pd.read_csv(os.sep.join([simdir,'forcagePF.txt.0']),header=None,sep='\t')
    forc = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')

    forc.rename(columns=forcing_headers,inplace=True)
    forc.index=pd.date_range('1/1/2005 00:00:00', periods=len(forc), freq='30min')
    sim.P=sum(forc['P'])*30*60


    """Prepare Budg datadfram"""
    Budg = pd.DataFrame()

    """ Get Q"""
    sim.calculate_runoff_from_press(x_outlet = 7,y_outlet = 35)
    Budg['Q'] = sim.surfQ
    catchmentsurf=np.sum(MASK)*sim.dx*sim.dy

    """ Alternative, get overlandsum and evaptranssum from PFBs:
    That's to avoid calculating budgets on snapshots (say hourly instead of 30mn actual time step)
    but on internally calculated running average.
    Note that evaptranssum has to be mult by the vardz of each layer and then P has to be removed from it
    Note
    """
    overlandsum_array = np.zeros((sim.nx,sim.ny,sim.n_t-1))
    evaptranssum_array = np.zeros((sim.nx,sim.ny,sim.n_t-1))
    prop_base = os.sep.join([simdir,'overlandPFBs/',simu_prefix+'.out'])
    prop_baseEvap = os.sep.join([simdir,'evaptranssumPFBs/',simu_prefix+'.out'])
    for t in range(sim.n_t-1):
        # first 24hrs is empty for some reason
        if t==0:
            overlandsum_array[:,:,t]=0.
            evaptranssum_array[:,:,t]=0.
        else:
            # Get all time file directories
            overlandsum_fp = '.'.join([prop_base,"overlandsum",format((t+1)*24,"05.0F"),"pfb"])
            evaptranssum_fp = '.'.join([prop_baseEvap, "evaptranssum",format((t+1)*24,"05.0F"),"pfb"])
            #~ evaptrans_fp = '.'.join([prop_base, "evaptrans", format((t+1)*dump_int,"05.0F"),"pfb"])
            
            # Read pfb
            overlandsum_arr = PFB_read(overlandsum_fp,0)
            evaptranssum_arr = PFB_read(evaptranssum_fp,0)
            #~ evaptrans_arr = PFB_read(evaptrans_fp,0)
            
            #Fill arrays for each time output
            overlandsum_array[:,:,t]= overlandsum_arr[:,:,0]
            for i in range(n_coupled_layers):
                evaptranssum_array[:,:,t]+=evaptranssum_arr[:,:,i]*(sim.dz*sim.dz_mult_arr[0,0,-(i+1)])

    """ convert from m3/h to mm/h"""
    Budg['Q']=Budg['Q']*1000./catchmentsurf
    Budg.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t_clm, freq='H')
    Budg['P']=(forc['P']*30.*60.).resample('H').sum()

    Budg = Budg.resample('D').sum()
    """ NEW WAY ! Q as obtained from overlandsum doesn't lack data as when obtained from undersampled snapshots! """
    Budg['Q'] = overlandsum_array.sum(axis=0).sum(axis=0)*1000./catchmentsurf
    """ Get ET"""
    #~ Budg['ET']=np.reshape(sim.evap_tot*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[MASK].mean(axis=0)
    #Same:
    #~ Budg['ET']=np.reshape(sim.evap_veg*3600.,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[MASK].mean(axis=0)+\
        #~ np.reshape(sim.evap_soil*3600.,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[MASK].mean(axis=0)

    """ NEW WAY ! ET as obtained from overlandsum doesn't lack data as when obtained from undersampled snapshots! """
    #~ Budg['ET'] = evaptranssum_array.sum(axis=0).sum(axis=0)*1000./catchmentsurf
    #~ Budg['ET'] = -(evaptranssum_array.sum(axis=0).sum(axis=0)*sim.dz_mult_arr[0,0,-1]*sim.dz*1000./catchmentsurf-Budg['P'])
    #~ Budg['ET'] = -(evaptranssum_array.sum(axis=0).sum(axis=0)*sim.dz_mult_arr[0,0,-1]*1000./catchmentsurf-Budg['P'])
    #~ Budg['ET'] = -(evaptranssum_array.sum(axis=0).sum(axis=0)*sim.dz_mult_arr[0,0,-1]*1000./catchmentsurf-Budg['P'])
    #~ Budg['ET'] = -(evaptranssum_array.sum(axis=0).sum(axis=0)*1000./(catchmentsurf*3.6*sim.dz)-Budg['P'])
    #the closest: evaptranssum in pf is mult by dxdydzdt, so here we devide by dxdy (catchment surf), and divide by dz (0.5 only, assuming in PF they don't take vardz')
    # *1000 is to go mm. In pf_couple, it is divided by dz*vardz, so we mult by vardz only() -> then we remove the precip.
    # actually the flux is divided by each layer dz in pf_couple !!
    """
    From pf_couple.F90:
       if (k == 1) then
          clm(t)%pf_flux(k)=(-clm(t)%qflx_tran_veg*clm(t)%rootfr(k)) + clm(t)%qflx_infl + clm(t)%qflx_qirr_inst(k)
    !!print*, 'Beta:',(-clm(t)%qflx_tran_veg*clm(t)%rootfr(k)),clm(t)%qflx_infl,saturation(l),pressure(l)
       else  
          clm(t)%pf_flux(k)=(-clm(t)%qflx_tran_veg*clm(t)%rootfr(k)) + clm(t)%qflx_qirr_inst(k)
       endif
       ! copy back to pf, assumes timing for pf is hours and timing for clm is seconds
       ! IMF: replaced drv%dz with clm(t)%dz to allow variable DZ...
       evap_trans(l) = clm(t)%pf_flux(k) * 3.6d0 / clm(t)%dz(k)
    enddo

    =>> see also solver_richards and evaptranssum.c where evap_trans_sum_ptr[index_evap_trans_sum] += evap_trans_ptr[index_evap_trans] * vol_time;
    with vol_time = dx*dy*dz*dt;
    """
    Budg['ET'] = -(evaptranssum_array.sum(axis=0).sum(axis=0)*1000./catchmentsurf/sim.dz-Budg['P'])

    #~ fig,ax = plt.subplots(nrows=1,figsize=(20,7), squeeze=True)
    #~ ax.plot(Budg['P'],'b',linewidth=1.0)	
    #~ ax.plot(Budg['ET'],'r',linewidth=1.0)
    #~ ax.legend(['P','ET'],loc='upper left',ncol=2,fontsize=17)    

    """ Calculate storage """
    stock = np.zeros(sim.n_t)
    for t in range(sim.n_t):
        # underground storage
        stock[t] = (sim.dx*sim.dy*sim.dz*sim.dz_mult_arr*sim.porosity_arr*sim.satur_array[:,:,:,t]).sum(axis=0).sum(axis=0).sum(axis=0)
        # poroelastic storage
        tmp = (sim.dx*sim.dy*sim.dz*sim.dz_mult_arr*sim.specific_storage_arr*sim.satur_array[:,:,:,t]*sim.press_array[:,:,:,t])
        # Veery curious... so the specific storage is actually applied in PF even for < pressure: the following need be commented:
        #~ tmp[tmp<0]=0.
        stock[t]+=tmp.sum(axis=0).sum(axis=0).sum(axis=0)
        # surface storage
        tmp = sim.dx*sim.dy*sim.press_array[:,:,-1,t]
        tmp[tmp<0]=0
        stock[t]+=tmp.sum(axis=0).sum(axis=0)
        

    stock = stock*1000./catchmentsurf
    stock = stock-stock[0]
    Budg['WSC']  = np.delete(stock,0)
    Budg = Budg.sort_index()
    Budg.to_csv(os.sep.join([simdir,'Budg.csv']))


    """ Prepare yearly budget """
    BudgYR = Budg.resample('Y').sum()
    PYR=BudgYR['P']
    BudgYR2 = BudgYR.drop('P',axis = 1)
    for i, y in enumerate(Budg.index.year.unique()):
        if i ==0:
             BudgYR2['WSC'][BudgYR2.index.year==y] = Budg['WSC'][Budg.index.year==y][-1]
        else:
             BudgYR2['WSC'][BudgYR2.index.year==y] = Budg['WSC'][Budg.index.year==y][-1] - Budg['WSC'][Budg.index.year==y-1][-1] 
         

    BudgYR2.index=BudgYR2.index.year
    PYR.index = PYR.index.year
    BudgYR2=BudgYR2[['ET','Q','WSC']] #reorder
    #~ ax=plot_clustered_stacked([BudgYR2,PYR])
    #~ ax.set_ylabel('mm')
    #~ plt.savefig(os.sep.join([simdir,'Budg.png']),dpi=400,format='png')


    #~ for y in Budg.index.year.unique():
        #~ for col in ['Q','P','ET']:
            #~ Budg[col][Budg.index.year==y] = np.cumsum(Budg[col][Budg.index.year==y])
        #~ Budg['WSC'][Budg.index.year==y] = Budg['WSC'][Budg.index.year==y] - Budg['WSC'][Budg.index.year==y][0] 
        
    yr=[yr for yr in BudgYR2.index]

    budgerr = [PYR[PYR.index==yr] - (BudgYR2[BudgYR2.index==yr]).sum(axis=1) for yr in BudgYR2.index]
    B=pd.DataFrame()
    B['P']=PYR
    B['ET']=BudgYR2['ET']
    B['Q']=BudgYR2['Q']
    B['WSC']=BudgYR2['WSC']
    return B,BudgYR2,PYR,budgerr

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

simuname_ref = 'ref_n13'
#~ simuname_ref = 'ref'
simdir_ref = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_n13_2018_03_28'
#~ simdir_ref = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_2018_03_13'
B_ref,BudgYR2_ref,PYR_ref,budgerr_ref = calc_simulation_budget(simdir_ref,simuname_ref,simu_prefix = 'doro')


#~ simuname = 'sap'
#~ simuname_ref2 = 'ref'
simuname_ref2 = 'ref_2yrs'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/sap_2018_03_28'
#~ simdir_ref2 = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_2020_01_20'
simdir_ref2 = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_2yrs_2020_01_21'
B_ref2,BudgYR2_ref2,PYR_ref2,budgerr_ref2  = calc_simulation_budget(simdir_ref2,simuname_ref2,simu_prefix = 'ref')

fig,ax1 = plt.subplots(1,1,figsize=(15,10), squeeze=True)

ax1=plot_clustered_stacked([BudgYR2_ref,PYR_ref])
ax1.set_ylabel('mm')
plt.savefig(os.sep.join([simdir_ref2,'Budg_'+simuname_ref+'.png']),dpi=400,format='png')

fig,ax2 = plt.subplots(1,1,figsize=(15,10), squeeze=True)

ax2=plot_clustered_stacked([BudgYR2_ref2,PYR_ref2])
ax2.set_ylabel('mm')
plt.savefig(os.sep.join([simdir_ref2,'Budg_'+simuname_ref2+'.png']),dpi=400,format='png')



