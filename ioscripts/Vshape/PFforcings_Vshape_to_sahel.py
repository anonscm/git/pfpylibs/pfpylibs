#-*- coding: utf-8 -*-
"""
    PFlibs - Example script for CLM forcing fields (currently 1D)

    Handling forcing data and formatting for CLM
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime
from procsycz import readDataAMMA as rdA
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
""" CST for PF forcing files"""
outdir = '/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/'
filenameIN = outdir+'forcagePF.200706070809101112131415_forc_nal2.30mn.dat'
#~ filenameIN = outdir+'forcagePF.200706070809101112.30mn_updated.txt.0'

filenameOUT = outdir+'forcagePF.200706070809101112131415_forc_nal2_Sahelian.30mn.dat'
#~ filenameOUT = outdir+'forcagePF.200706070809101112_Sahelian.30mn.dat'

forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
Legtxt_Daily = {'SWin':'SWin (W/m2)', 'LWin':'LWin (W/m2)', 'P':'P (mm)','T':'T (°C)','WindX':'W-E Wind (m/s)','WindY':'S-N Wind (m/s)','Press':'Press (Pa)','Hs':'Hs (kg/kg)'}
rad_data = ['SWin','LWin']
T = ['T']
H = ['Hs']
W = ['WindX','WindY']



##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

""" Read A PF forcing file"""
#~ forc = pd.read_csv(filenameIN,header=None,sep=' ')
forc = pd.read_csv(filenameIN,header=None,sep='\t')
forc.rename(columns=forcing_headers,inplace=True)
forc.index=pd.date_range('1/1/2005 00:00:00', periods=len(forc), freq='30min')
forc['P']= forc['P']/3.0
forc['WindY']=0.0

#~ forc.to_csv(filenameOUT,float_format='%2.6f', sep=' ', header=False, index=False)
forc.to_csv(filenameOUT,float_format='%6f', sep='\t', header=False, index=False)
#~ forc.to_csv(filenameOUT,float_format='%f', sep=' ', header=False, index=False)
#~ forc.to_csv(filenameOUT,float_format='%2.4f', sep='\t', header=False, index=False)
#~ forc.to_csv(filenameOUT,float_format='%F', sep='\t', header=False, index=False)
#~ forc.to_csv(filenameOUT, sep=' ', header=False, index=False)

