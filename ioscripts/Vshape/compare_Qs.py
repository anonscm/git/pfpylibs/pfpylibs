#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    Anaylse PF outputs for Vshape paper: Streamflow
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os,glob
from netCDF4 import Dataset
import time
import datetime
from matplotlib.dates import MonthLocator, DateFormatter
import locale
locale.setlocale(locale.LC_ALL,'en_US.UTF-8')

"""local imports:"""
from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout

plt.close("all")


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/ref/'
#~ simuname = 'clay_normal_klesshigh2'
simuname = 'ref'
simuname = 'ref_n13'
#~ simuname = 'ref_n12'
#~ simuname = 'sap'
#~ simuname = 'mix'
#~ simuname = 'noBF'
#~ simuname = 'herb'
#~ simuname = 'trees'

#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/paper/outputs/clay_normal_klesshigh2_2016_06_07'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_2018_03_13'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_n13_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_n12_2018_03_31'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/sap_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/mix_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/noBF_2018_03_29'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/herb_2018_03_13'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/trees_2018_03_29'

n_coupled_layers = 13
n_CLM_2Dlayers = 14

statsDay =True
statsDay =False

"""
CLM_var is a dictionnary where keys are local variable names as in PFCLM
values are lists of string[filename, PFCLM variable name, long name, units]
"""
CLM_var={'surf_press':['','h_top','Surface pressure','m']}

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def minimalist_xldate_as_datetime(xldate, datemode):
    # datemode: 0 for 1900-based, 1 for 1904-based
    return (
        datetime.datetime(1899, 12, 30)
        + datetime.timedelta(days=xldate + 1462 * datemode)
        )
        
def roundDateTime(shakyDT,nearest_minutes=1):
	"""
	"""
	shakyDT += datetime.timedelta(minutes=nearest_minutes/2)
	shakyDT -= datetime.timedelta(minutes=shakyDT.minute % nearest_minutes,
                         seconds=shakyDT.second,
                         microseconds=shakyDT.microsecond)
	return shakyDT
	

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
def getSimQ(simdir,simuname='ref'):    
    """make a function of all the reading part"""
    
    """ add the filename in the dic"""        
    for k,val in CLM_var.items():
        CLM_var[k][0]=os.sep.join([simdir,''.join([simuname,'_'+val[1]+'.nc'])])    
        
    #### Read & process simulation outputs:
    sim = PFout.PFout(simdir,'doro')
    sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
    sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_h_top.nc'])]),filetype='CLM')

    """Read PF outputs"""
    start_time = time.time()
    with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
        sim.satur_array = ds.variables['saturation'][:]
    with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
        sim.press_array = ds.variables['succion'][:] 

    """Read CLM outputs"""
    for k,val in CLM_var.items():
        try:
            with Dataset(val[0], 'r') as ds:
                print('read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
                exec('sim.%s=np.swapaxes(ds.variables[\'%s\'][:],0,2)'%(k,val[1]))        
        except FileNotFoundError:
                print('CANNOT read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
    print("--- %d seconds --- read all PFCLM outputs from NCs" % (time.time() - start_time))

    """ Read constant files"""
    ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
    #watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
    sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,1)
    sim.mannings_arr = np.zeros((sim.nx,sim.ny))+5e-6
    sim.mask_arr = np.transpose(ds.variables['mask'][:])
    topmask = sim.mask_arr[:,:,0]
    MASK=topmask>0 
    """MASK is a nx x ny boolean mask of the domain
    When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
    (as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
    """    
    sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
    sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
    sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,1)
    sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,1)
    #~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))

    """ Read Forcings: """
    #~ forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
    #~ forc = pd.read_csv(os.sep.join([simdir,'forcagePF.txt.0']),header=None,sep='\t')
    #~ forc = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')
    #~ forc.rename(columns=forcing_headers,inplace=True)
    #~ forc.index=pd.date_range('1/1/2005 00:00:00', periods=len(forc), freq='30min')
    #~ sim.P=sum(forc['P'])*30*60


    """Create CLM output DataFrames: """
    dsclm=pd.DataFrame()
    #~ sim.calculate_runoff_from_press(x_outlet = 7,y_outlet = 35)
    # to compare obs with sim runoff:
    sim.calculate_runoff_from_press(x_outlet = 7,y_outlet = 35)
    dsclm['Q']=sim.surfQ
    catchmentsurf=np.sum(MASK)*sim.dx*sim.dy
    # to compare obs witj sim runoff
    catchmentsurf=np.sum(MASK[:,0:-3])*sim.dx*sim.dy

    dsclm.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t_clm, freq='H')
    #~ dsclm['P']=(forc['P']*3600).resample('H').sum()

    dsclmD_ref=pd.DataFrame()
    for val in dsclm.columns:
        if val=='Q': dsclmD_ref[val] = dsclm[val].resample('D').sum()
        #~ if val=='P': dsclmD_ref[val] = dsclm[val].resample('D').sum()
        else: dsclmD_ref[val] = dsclm[val].resample('D').mean()

    dsclmD=pd.DataFrame()

    #~ dsclmD['P'] = dsclm['P'].resample('D').sum()
    dsclmD['Q'] = dsclm['Q'].resample('D').sum()
    dsclmD.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
    return dsclmD

simuname = 'sap_herb_sou2'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/sap_herb_sou_2018_10_09'
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_herb_sou2_2018_10_12'
D1 = getSimQ(simdir,simuname)

simuname = 'sap_herb_sah'
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_herb_sah_2018_10_12'
D2 = getSimQ(simdir,simuname)

simuname = 'sap_trees_sou'
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_trees_sou_2018_10_09'
D3 = getSimQ(simdir,simuname)

simuname = 'sap_trees_sah'
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_trees_sah_2018_10_12'
D4 = getSimQ(simdir,simuname)

simuname = 'sap_trees_sah_lowWT'
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_trees_sah_lowWT_2018_10_12'
D5 = getSimQ(simdir,simuname)

"""PLOT"""
# change locale to english for plotting
# locale.locale_alias lists all the country (locale) codes
#~ locale.setlocale(locale.LC_ALL,'fr_FR.UTF-8')

#~ fig,ax = plt.subplots(nrows=len(np.arange(2008,2013,1)),figsize=(15,10),sharex=True, squeeze=True)
#~ fig,ax = plt.subplots(nrows=len(np.arange(2008,2013,1)),figsize=(20,10), squeeze=True)
#~ fig,ax = plt.subplots(nrows=3,figsize=(20,7), squeeze=True)
#A4 in inches is 8.3 * 11.7
fig,ax = plt.subplots(1,1,figsize=(8,2), squeeze=True)
#~ yr = 2011
yr = 2015
threshold=2.0 #"""m3/s"""
simu1 = D1['Q'][D1.index.year==yr]
simu1[simu1<=threshold]=np.nan
simu2 = D2['Q'][D2.index.year==yr]
simu2[simu2<=threshold]=np.nan
simu3 = D3['Q'][D3.index.year==yr]
simu3[simu3<=threshold]=np.nan
simu4 = D4['Q'][D4.index.year==yr]
simu4[simu4<=threshold]=np.nan
simu5 = D5['Q'][D5.index.year==yr]
simu5[simu5<=threshold]=np.nan

# every month
months = MonthLocator(bymonth=range(1, 13), bymonthday=1, interval=1)
monthsFmt = DateFormatter("%b")

ax.plot(simu1,'k',linewidth=0.5)
ax.plot(simu2,'r',linewidth=0.5)
ax.plot(simu3,'b',linewidth=0.5)
ax.plot(simu4,'g',linewidth=0.5)
ax.plot(simu5,'y',linewidth=0.5)
ax.set_ylim([10,1500])
ax.set_yscale('log')
#~ ax[0].set_xlim([datetime.datetime(yr,6,15), datetime.datetime(yr,11,15)])
#~ ax[0].set_ylabel('$m^3/day$',FontSize=17)
ax.set_ylabel('$m^3/day$',FontSize=9)
#~ ax[0].legend(['ref','no I. Valley'],loc='upper left',ncol=2,fontsize=17)    
ax.legend(['sap_herb_sou','sap_herb_sah','sap_trees_sou','sap_trees_sah','sap_trees_sah_lowWT'],loc='lower left',ncol=5,fontsize=9)    
ax.xaxis.set_major_locator(months)
ax.xaxis.set_major_formatter(monthsFmt)
#~ ax[0].tick_params(axis='both', which='major', labelsize=16)
ax.tick_params(axis='both', which='major', labelsize=9)


#~ simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_herb_sou_2018_10_09'
#~ forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
#~ forc = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')
#~ forc.rename(columns=forcing_headers,inplace=True)
#~ forc.index=pd.date_range('1/1/2005 00:00:00', periods=len(forc), freq='30min')
#~ P=(forc['P']*30*60).resample('D').sum()
#~ ax2 = ax.twinx()
#~ ax2.plot(forc['P'].resample('D').sum()*30*60,'b')
#~ ax2.set_ylim([0,190])
#~ ax2.set_yticks([0,20,40,60])
#~ ax2.yaxis.set_label_position("right")
#~ ax2.tick_params(axis='y',colors='b',labelsize=9)
#~ ax2.set_ylabel('Precip (mm/d)', fontsize=9, color = 'b')

#~ fig.subplots_adjust(bottom=0.05, top =0.95, hspace=0.001)
fig.subplots_adjust(bottom=0.1, top =0.99,left=0.07,right=0.99, hspace=0.001)

simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_herb_sou_2018_10_09'
plt.savefig(os.sep.join([simdir,'Q_comp_%d.png'%yr]),dpi=400,format='png')
plt.savefig(os.sep.join([simdir,'Q_comp_%d.pdf'%yr]),dpi=400,format='pdf')

