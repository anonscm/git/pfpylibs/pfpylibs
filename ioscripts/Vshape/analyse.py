#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Anaylse PF outputs for Vshape paper


    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import pandas as pd
import time
import os
import matplotlib.pyplot as plt
#Added for handling NC (to be moved to another library)
#~ import kolinkelly as kk
#~ import joestork as js
from netCDF4 import Dataset

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout


##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/ref/'
simuname = 'ref'

simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_2018_03_13'


n_coupled_layers = 13
n_CLM_2Dlayers = 14

"""
CLM_var is a dictionnary where keys are local variable names as in PFCLM
values are lists of string[filename, PFCLM variable name, long name, units]
"""
CLM_var={'lh_tot':['','eflx_lfh_tot','latent heat','W/m2'],\
        'lwrad_out':['','eflx_lwrad_out','outgoing lw radiation','W/m2'],\
        'sh_tot':['','eflx_sh_tot','sensible heat','W/m2'],\
        'soil_grnd':['','eflx_soil_grnd','ground heat flux','W/m2'],\
        'evap_tot':['','qflx_evap_tot','total evapotranspiration','mm/s'],\
        'evap_ground':['','qflx_evap_grnd','ground evaporation without condesation','mm/s'],\
        'evap_soil':['','qflx_evap_soi','soil evaporation','mm/s'],\
        'evap_veg':['','qflx_evap_veg','vegetation evaporation + transpiration','mm/s'],\
        'tran_veg':['','qflx_tran_veg','vegetation transpiration','mm/s'],\
        't_grnd':['','t_grnd','Ground temperature','K'],\
        'surf_press':['','h_top','Surface pressure','m'],\
        }
""" add the filename in the dic"""        
for k,val in CLM_var.items():
    CLM_var[k][0]=os.sep.join([simdir,''.join([simuname,'_'+val[1]+'.nc'])])
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

#### Read & process simulation outputs:
sim = PFout.PFout(simdir,'doro')

#sim.set_reading_parameters(n_t=2921,n_t_clm=70080,n_t_forc = 70080*2, dump_int=24,dump_int_clm=1,conv=1,conv_clm=1 * 60 * 60, conv_forc= 1 * 60 * 30)

"""
sim.read_domain_dimensions_from_PFBs()
sim.read_allPFoutput_from_PFBs()
sim.read_CLMoutput(nlayers = n_coupled_layers+n_CLM_2Dlayers)
#~ sim.extract_CLM_2Doutputs(nlayers = 16 +14 )
sim.read_allconstant_outputs()
sim.read_forcings()
sim.calculate_runoff(x_outlet = 7,y_outlet = -1)
#~ sim.calculate_WSC()
"""
"""NetCDF"""

sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_eflx_lh_tot.nc'])]),filetype='CLM')

"""Read PF outputs"""
start_time = time.time()
with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
    sim.satur_array = ds.variables['saturation'][:]
    topmask=np.swapaxes(ds.variables['top_mask'][:],0,1)
"""MASK is a nx x ny boolean mask of the domain
When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
(as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
"""
MASK=topmask>0 
with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
    sim.press_array = ds.variables['succion'][:] 

"""Read CLM outputs"""
for k,val in CLM_var.items():
    try:
        with Dataset(val[0], 'r') as ds:
            print('read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
            exec('sim.%s=np.swapaxes(ds.variables[\'%s\'][:],0,2)'%(k,val[1]))        
    except FileNotFoundError:
            print('CANNOT read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
print("--- %d seconds --- read all PFCLM outputs from NCs" % (time.time() - start_time))

""" Read constant files"""
ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
#watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,1)
sim.mannings_arr = np.zeros((sim.nx,sim.ny))+5.5e-6
sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,1)
sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,1)
sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,1)
#~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))

""" Read Forcings: """
forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
forc = pd.read_csv(os.sep.join([simdir,'forcagePF.txt.0']),header=None,sep='\t')
forc.rename(columns=forcing_headers,inplace=True)
forc.index=pd.date_range('1/1/2005 00:00:00', periods=len(forc), freq='30min')
sim.P=sum(forc['P'])*30*60


"""Create CLM output DataFrames: """
dsclm=pd.DataFrame()
CLM_varsubset=['evap_tot','evap_ground','evap_soil','evap_veg','tran_veg']
for k in CLM_varsubset:
    exec('dsclm[k]=sim.%s.mean(axis=0).mean(axis=0)*3600'%k)

sim.calculate_runoff_from_press(x_outlet = 7,y_outlet = 35)
dsclm['Q']=sim.surfQ
dsclm.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t_clm, freq='H')
dsclm['P']=(forc['P']*3600).resample('H').sum()

dsclmD_ref=pd.DataFrame()
for val in dsclm.columns:
    if val=='Q': dsclmD_ref[val] = dsclm[val].resample('D').sum()
    if val=='P': dsclmD_ref[val] = dsclm[val].resample('D').sum()
    else: dsclmD_ref[val] = dsclm[val].resample('D').mean()

dsclmD=pd.DataFrame()
dsclmD_min=pd.DataFrame()
dsclmD_max=pd.DataFrame()
for k in CLM_varsubset:
    exec('dsclmD[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[MASK].mean(axis=0)'%k)
    exec('dsclmD_min[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[MASK].min(axis=0)'%k)
    exec('dsclmD_max[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[MASK].max(axis=0)'%k)

dsclmD['P'] = dsclm['P'].resample('D').sum()
dsclmD['Q'] = dsclm['Q'].resample('D').sum()
dsclmD.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
dsclmD_min.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
dsclmD_max.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')


fig,ax = plt.subplots(nrows=len(dsclmD.columns),figsize=(15,10),sharex=True, squeeze=True)

for i in range(len(ax)):
    #~ if (dsclmD.columns[i] == 'P') or (dsclmD.columns[i] == 'Q'):
    if (dsclmD.columns[i] == 'P'):
        ax[i].plot(dsclm[dsclmD.columns[i]])
        ax[i].legend(['Precip'])
        ax[i].set_ylabel('mm/d')
    elif (dsclmD.columns[i] == 'Q'):
        ax[i].plot(dsclm[dsclmD.columns[i]])
        #~ ax[i].semilogy(dsclm[dsclmD.columns[i]])
        ax[i].legend(['Streamflow'])
    else:
        ax[i].plot(dsclmD[dsclmD.columns[i]])
        ax[i].plot(dsclmD_ref[dsclmD_ref.columns[i]])
        ax[i].fill_between(dsclmD[dsclmD.columns[i]].index,\
            np.reshape(np.array(dsclmD_min[dsclmD.columns[i]]),len(dsclmD_min),1),\
            np.reshape(np.array(dsclmD_max[dsclmD.columns[i]]),len(dsclmD_max),1),\
            alpha=0.5)
        #~ ax[i].legend([dsclmD.columns[i]])
        ax[i].legend([CLM_var[dsclmD.columns[i]][2]])
        #~ ax[i].set_ylabel(CLM_var[dsclmD.columns[i]][3])
        ax[i].set_ylabel('mm/d')

fig.subplots_adjust(bottom=0.03, top =0.95, hspace=0.001)

ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
ds2=Dataset(os.sep.join([simdir,''.join([simuname,'_qflx_evap_tot.nc'])]), 'r')

print(ds.variables.keys())
print(ds.variables['saturation'])

    
