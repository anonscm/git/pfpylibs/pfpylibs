#-*- coding: utf-8 -*-
"""
    PFlibs - Example script for CLM forcing fields (currently 1D)

    Handling forcing data and formatting for CLM
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import datetime
from procsycz import readDataAMMA as rdA
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
""" CST for PF forcing files"""
#~ srcdir = '/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/' # for the Vshape paper
srcdir ='/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/inputs/forcage/' # updated dataset in 2016 - 2017?
#~ filenameIN = 'forcagePF.200706070809101112.30mn_updated.txt.0'# for the Vshape paper
filenameIN = 'forcagePF.200706070809101112131415_forc_nal2.30mn_updated.txt.0'# updated dataset in 2016 - 2017?

outdir = '/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/'
filenameOUT = 'forcagePF.200706070809101112131415_forc_nal2.30mn.dat'

#~ save_figs=None
save_figs=True
#~ save_figs=False
show_figs=True
#~ show_figs=False

forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
Legtxt_Daily = {'SWin':'SWin (W/m2)', 'LWin':'LWin (W/m2)', 'P':'P (mm)','T':'T (°C)','WindX':'W-E Wind (m/s)','WindY':'S-N Wind (m/s)','Press':'Press (Pa)','Hs':'Hs (kg/kg)'}
rad_data = ['SWin','LWin']
T = ['T']
H = ['Hs']
W = ['WindX','WindY']

""" CST for AMMA-CATCH (AC) data"""
AC_rt_dir = r'/home/hectorb/DATA/METEO/Benin/AMMACATCH/gapfil_2006_2015_AD_032018'
#~ AC_station_list = {'Bira':['Bi1'] ,'Bellefoungou':['Be2'],'Djougou':['Djo'],'Nalohou':['Na2']}
AC_station_list = {'Nalohou':['Na2']}
AC_rad_data = ['SWin (W/m²)','LWin (W/m²)', 'Rnet (W/m²)']
AC_T = ['Tair (°C)']
AC_H = ['Hs']
AC_W = ['Wspeed (m/s)']
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def RelHum_to_SpecHum(HUR,TA,P):
    """
    Calculates Specific humidity from relative humidity
    wrt solid if T<=0;    wrt liquid if T>0
    
    TA is absolute T in K (not potential T)
    
    Goff and Gratch relationship:
    Psat0=6.1173*100; % Pression de vapeur saturante par rapport à la glace à To=273.15 K (en Pa)
    logPsat = -9.09718*(213.15/T-1) - 3.56654*log(273.15/T) + 0.876793(1-T/273.15) +log(PsatO);
    Psat=10^logPsat;
    
    Specific Humidity from Relative Humidity:
    Pvap = HUR./Psat;
    HUS=0.622*Pvap./(P-Pvap); 
    """
    Psat=GoffGratch(TA,'liq')
    #Pression partielle de l'eau :
    Pvap = HUR*Psat/100;
    HUS=0.622*Pvap/(P-Pvap);  
    return HUS

def GoffGratch(TA,Waterstatus):
    """
    Calcul de Pression de vapeur saturante par rapport au solide, 
    d'apres la relation de Goff and Gratch. 
    See RelHum_to_SpecHum() for more details
    
    TA temperature de l'air en K,
    type = 'ice' ou 'liq'
    """
    if Waterstatus == 'ice':
        PsatO=6.1173 #in hPa,  Pression de vapeur saturante par rapport à la glace à To=273.15 K
        logPsat= -9.09718*(273.15/TA-1) - 3.56654*np.log10(273.15/TA) + 0.876793*(1-TA/273.15) +np.log10(PsatO)
    elif Waterstatus == 'liq':
        Psat0=1013.25 #in hPa 'steam-point' pressure at 1atm, T=373.15K
        logPsat= -7.90298*(373.16/TA-1) + 5.02808*np.log10(373.16/TA) - 1.3816*10**(-7)*(10**(11.344*(1-TA/373.16))-1)\
        + 8.1328*10**(-3)*(10**(-3.49149*(373.16/TA-1))-1) + np.log10(Psat0)
        Psat=(10**logPsat)*100
    return Psat

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

""" Read A PF forcing file"""
forc = pd.read_csv(srcdir+filenameIN,header=None,sep='\t')
forc.rename(columns=forcing_headers,inplace=True)
forc.index=pd.date_range('1/1/2005 00:00:00', periods=len(forc), freq='30min')
forcD=pd.DataFrame()
for key, val in forcing_headers.items():
    if val=='P': forcD[val] = (forc[val]*30*60).resample('D').sum()
    elif val == 'T':forcD[val] = (forc[val]-273.15).resample('D').mean()
    #~ elif val == 'T':forcD[val] = (forc[val]-273.15).resample('D').max()
    else: forcD[val] = forc[val].resample('D').mean()
    #~ else: forcD[val] = forc[val].resample('D').max()

""" plot some fields"""
#~ ax=forcD[rad_data].plot()
#~ ax.legend([Legtxt_Daily[dataname] for dataname in ax.legendlabels])
#~ plt.show()


""" get some AMMA-CATCH forcings"""
Met=rdA.StaDic(name='Meteo')
Met.readMeteoFiles_from_pre_suf(AC_station_list,AC_rt_dir,pre_pattern='gap_',suf_pattern='.csv')
ACdata=Met['Nalohou'].met
ACdata['Hs']=RelHum_to_SpecHum(ACdata['HR (%)']/100,ACdata['Tair (°C)']+273.15,ACdata['Patm (hPa)'])
ACdata_D=ACdata.resample('D').mean()
#~ ACdata_D=ACdata.resample('D').max()

""" plot some fields"""
#~ ACdata_D[AC_rad_data].plot()
#~ plt.show()  

""" compare """
fig1 = plt.figure(figsize=(15,10))
ax1 = fig1.add_subplot(411)
ax1.plot(forcD[rad_data])
ax1.plot(ACdata_D[AC_rad_data])
ax1.legend(['former '+r for r in rad_data] + ['new '+r for r in AC_rad_data])

ax2 = fig1.add_subplot(412)
ax2.plot(forcD[T])
ax2.plot(ACdata_D[AC_T])
ax2.legend(['former '+r for r in T] + ['new '+r for r in AC_T])

ax3 = fig1.add_subplot(413)
ax3.plot(forcD[H])
ax3.plot(ACdata_D[AC_H])
ax3.legend(['former '+r for r in H] + ['new '+r for r in AC_H])

ax4 = fig1.add_subplot(414)
ax4.plot(forcD[W])
ax4.plot(ACdata_D[AC_W])
ax4.legend(['former '+r for r in W] + ['new '+r for r in AC_W])

if save_figs:
	plt.savefig(outdir+'old_vs_new_forcings_Nalohou2.png')



""" Write some PF forcing file """
Out = ACdata[['SWin (W/m²)','LWin (W/m²)','Tair (°C)','Wspeed (m/s)','Patm (hPa)','Hs']]
Out.insert(2,'P',forc['P'])
Out.insert(5,'WindY',0.0)

Out['Patm (hPa)']*=100.0
Out=Out.rename(columns={'Patm (hPa)':'Press'})

# Lack spinup year of 2005: we add it by adding year 2007 and modifying year number
# for P is ok because it was already the case

baseyr=2007
yrs_to_add =[2005]
for yr in yrs_to_add:
	tmp=Out[Out.index.year==baseyr]
	tmp.index=tmp.index.map(lambda t: t.replace(year = yr))
	Out=pd.concat([Out,tmp])
Out.sort_index(inplace=True)



Out['Tair (°C)']=Out['Tair (°C)']+273.15

OutD=pd.DataFrame()
OutDmin=pd.DataFrame()
OutDmax=pd.DataFrame()
for val in Out.columns:
    if val=='P': OutD[val] = (Out[val]*30*60).resample('D').sum()
    else: OutD[val] = Out[val].resample('D').mean()
    OutDmin[val] = Out[val].resample('D').min()
    OutDmax[val] = Out[val].resample('D').max()
    
OutD['Tair (°C)']-=273.15
OutDmin['Tair (°C)']-=273.15
OutDmax['Tair (°C)']-=273.15

OutD=OutD.drop(columns=['WindY'])
OutDmin=OutDmin.drop(columns=['WindY'])
OutDmax=OutDmax.drop(columns=['WindY'])
#to display in another order
#~ display_other_order=['SWin (W/m²)','LWin (W/m²)','Tair (°C)','Wspeed (m/s)','Patm (hPa)','Hs','P']
#~ display_other_order=['SWin (W/m²)','LWin (W/m²)','Tair (°C)','Wspeed (m/s)','Patm (hPa)','Hs','P']
display_other_order=['SWin (W/m²)','LWin (W/m²)','Tair (°C)','Wspeed (m/s)','Press','Hs','P']
OutD=OutD[display_other_order]
OutDmin=OutDmin[display_other_order]
OutDmax=OutDmax[display_other_order]


#~ fig2,ax = plt.subplots(nrows=len(OutD.columns)-1,figsize=(15,10),sharex=True, squeeze=True)
#A4 in inches is 8.3 * 11.7
fig2,ax = plt.subplots(nrows=len(OutD.columns)-1,figsize=(7.5,5),sharex=True, squeeze=True)
OutD.sort_index(inplace=True)
"""
for i in range(len(ax)):
    if OutD.columns[i] == 'P':
        ax[i].plot(OutD[OutD.columns[i]])
        #~ ax[i].bar(OutD[OutD.columns[i]])
        #~ ax[i]=OutD[OutD.columns[i]].plot(x=OutD.index,kind='bar',ax=ax[i])
        #seems not to work, issue related to https://github.com/matplotlib/matplotlib/issues/6023
        #~ ax[i]=OutD[OutD.columns[i]].plot(kind='bar')
        ax[i].legend([OutD.columns[i]])
    else:
        ax[i].plot(OutD[OutD.columns[i]])
        ax[i].fill_between(OutD[OutD.columns[i]].index,\
            np.reshape(np.array(OutDmin[OutD.columns[i]]),len(OutDmin),1),\
            np.reshape(np.array(OutDmax[OutD.columns[i]]),len(OutDmax),1),\
            alpha=0.5)
        ax[i].legend([OutD.columns[i]])

fig2.subplots_adjust(bottom=0.03, top =0.95, hspace=0.001)
#~ fig2.tight_layout()
"""
#~ ax[0].plot(OutD['P'])
# alternatively plot patches with -halfday +halfday: OKAY BUT VERY LONG
halfday = datetime.timedelta(0,3600*12,0)
for index, r in OutD['P'].iteritems():    
    x = (index-halfday, index+halfday)
    h = r
    ax[0].fill_between(x, 0, h,color = 'b')
ax[0].set_ylim([0,85])
ax[0].set_ylabel('Precip (mm)',FontSize=9)
ax[0].set_yticks([10,30,50,70])
#~ ax[0].set_yticklabels(ax[0].get_yticklabels(),FontSize=7)
#~ ax[0].set_yticklabels(['20','40','60','80'],FontSize=9)
#~ ax[0].legend('Precip')
ax[0].set_xlim([datetime.datetime(2005,1,1), datetime.datetime(2012,12,31)])

ax[1].plot(OutD['SWin (W/m²)'],linewidth = 0.5)
ax[1].fill_between(OutD['SWin (W/m²)'].index,\
    np.reshape(np.array(OutDmin['SWin (W/m²)']),len(OutDmin),1),\
    np.reshape(np.array(OutDmax['SWin (W/m²)']),len(OutDmax),1),\
    alpha=0.5)
ax[1].set_ylabel('SWi (W/m²)',FontSize=9)
ax[1].set_ylim([0.1,1200])
ax[1].set_yticks([0,400,800])

ax[1].set_xlim([datetime.datetime(2005,1,1), datetime.datetime(2012,12,31)])

    
ax[2].plot(OutD['LWin (W/m²)'],linewidth = 0.5)
ax[2].fill_between(OutD['LWin (W/m²)'].index,\
    np.reshape(np.array(OutDmin['LWin (W/m²)']),len(OutDmin),1),\
    np.reshape(np.array(OutDmax['LWin (W/m²)']),len(OutDmax),1),\
    alpha=0.5)
ax[2].set_ylabel('LWi (W/m²)',FontSize=9)    
ax[2].set_ylim([0.1,1200])
ax[2].set_yticks([0,400,800])

ax[2].set_xlim([datetime.datetime(2005,1,1), datetime.datetime(2012,12,31)])

ax[3].plot(OutD['Tair (°C)'],linewidth = 0.5)
ax[3].fill_between(OutD['Tair (°C)'].index,\
    np.reshape(np.array(OutDmin['Tair (°C)']),len(OutDmin),1),\
    np.reshape(np.array(OutDmax['Tair (°C)']),len(OutDmax),1),\
    alpha=0.5)
ax[3].set_ylabel('Tair (°C)',FontSize=9)        
ax[3].set_ylim([10,39])
ax[3].set_xlim([datetime.datetime(2005,1,1), datetime.datetime(2012,12,31)])

ax[4].plot(OutD['Hs'],linewidth = 0.5)
ax[4].fill_between(OutD['Hs'].index,\
    np.reshape(np.array(OutDmin['Hs']),len(OutDmin),1),\
    np.reshape(np.array(OutDmax['Hs']),len(OutDmax),1),\
    alpha=0.5)
ax[4].set_ylabel('SH (kg/kg)',FontSize=9)        
ax[4].set_ylim([0.001,0.019])
ax[4].set_yticks([0.005,0.01,0.015])

ax[4].set_xlim([datetime.datetime(2005,1,1), datetime.datetime(2012,12,31)])

ax[5].plot(OutD['Wspeed (m/s)'],linewidth = 0.5)
ax[5].fill_between(OutD['Wspeed (m/s)'].index,\
    np.reshape(np.array(OutDmin['Wspeed (m/s)']),len(OutDmin),1),\
    np.reshape(np.array(OutDmax['Wspeed (m/s)']),len(OutDmax),1),\
    alpha=0.5)
ax[5].set_ylabel('Wspeed (m/s)',FontSize=9)            
ax[5].set_ylim([0,8.9])
ax[5].set_yticks([0,2,4,6,8])

ax[5].set_xlim([datetime.datetime(2005,1,1), datetime.datetime(2012,12,31)])

fig2.subplots_adjust(bottom=0.05, top =0.95, hspace=0.001)


#~ Out.to_csv(outdir+filenameOUT,float_format='%2.4f', sep=' ', header=False, index=True) # check with index = true if index are well sorted
#~ Out.to_csv(outdir+filenameOUT,float_format='%2.4f', sep=' ', header=False, index=False)
Out.to_csv(outdir+filenameOUT,float_format='%6f', sep='\t', header=False, index=False)
if save_figs:
	#~ plt.savefig(outdir+'PF_Forcings_NalohouV2.png')
	plt.savefig(outdir+'PF_Forcings_Nalohou.pdf',format ='pdf')
if show_figs:
    plt.show()

