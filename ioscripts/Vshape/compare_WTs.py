#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    DESCRIPTION

    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime
import os,glob
import time
from netCDF4 import Dataset

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
plt.close('all')


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
simuname = 'ref'
simuname = 'ref_n13'
#~ simuname = 'sap'
#~ simuname = 'mix'
#~ simuname = 'noBF'
#~ simuname = 'herb'
#~ simuname = 'trees'

simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_2018_03_13'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_n13_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/sap_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/mix_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/noBF_2018_03_29'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/herb_2018_03_13'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/trees_2018_03_29'

n_coupled_layers = 13
n_CLM_2Dlayers = 14
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

def getSimWT(simdir,simuname='ref',xloc=0,yloc=0):
    """
    make a function of all the reading part
    """
    sim = PFout.PFout(simdir,'doro')

    sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
    sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
    #~ sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_h_top.nc'])]),filetype='CLM')

    """Read PF outputs"""
    start_time = time.time()
    with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
        sim.satur_array = np.transpose(ds.variables['saturation'][:],[3,2,1,0])
    with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
        sim.press_array = np.transpose(ds.variables['succion'][:],[3,2,1,0])

    """ Read constant files"""
    ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
    #watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
    sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,2)
    sim.mannings_arr = np.zeros((sim.nx,sim.ny))+5e-6
    sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,1)
    sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
    sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
    sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,2)
    sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,2)
    #~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))
    sim.dz = np.diff(ds.variables['depth'][:])[0]
    sim.z = np.cumsum(sim.dz_mult_arr[0,0,::-1]*sim.dz) - sim.dz_mult_arr[0,0,::-1]*sim.dz/2
    sim.z = np.round(sim.z*1000)/1000




    """ extract a T-D slice """
    # x= 140 y = 430 according to Matlab script:
    xloc=xloc
    yloc = yloc

    domain_thickness = np.round(np.cumsum(sim.dz_mult_arr[0,0,::-1]*sim.dz)[-1]*1000)/1000
    depth_last_cell = sim.z[-1]
    WT = domain_thickness - sim.press_array[xloc,yloc,0,:]
    WT = depth_last_cell - sim.press_array[xloc,yloc,0,:]


    """ Get All Water tables (perched)"""
    #first get a mask of the base layers of each saturated (p>0) contiguous (over Z) unit
    maskWT=np.zeros([sim.nz,sim.n_t])
    for t in range(sim.n_t):
        profile = np.zeros(sim.nz)
        #profile[0] = base of domain. if the criteria is p < 0 => 0, you realize there are some weird points at the bottom
        #of perched layers with p>0 but < sim.dz ... so as if the cell is not entirely saturated but p>0 ... 
        profile = np.array([0. if sim.press_array[xloc,yloc,i,t]<= sim.dz_mult_arr[xloc,yloc,i]*sim.dz else 1. for i in range(sim.nz)])
        for i in range(sim.nz-1):
            profile[i+1]=(profile[i]+profile[i+1])*profile[i+1]
        # a=[1,1,1,0,0,0,1,1,0,1,0,0,0,1] => [1, 2, 3, 0, 0, 0, 1, 2, 0, 1, 0, 0, 0, 1]
        profile[profile!=1.]=np.nan
        maskWT[:,t] = profile

    WTs = np.resize(sim.z[::-1],[sim.n_t,sim.nz]).T - (maskWT*sim.press_array[xloc,yloc,:,:])
    return WTs

# x= 140 y = 430 according to Matlab script:

xloc=8-1
xloc=7-1
yloc=23-1
yloc=36-1
simuname = 'sap_herb_sou'
simuname = 'sap_herb_sou2'
#~ simuname = 'sap_herb_sou3'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/sap_herb_sou_2018_10_09'
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_herb_sou_2018_10_09'
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_herb_sou2_2018_10_12'
#~ simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_herb_sou3_2018_10_11'
WTs1 = getSimWT(simdir,simuname,xloc,yloc)

simuname = 'sap_herb_sah'
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_herb_sah_2018_10_12'
WTs2 = getSimWT(simdir,simuname,xloc,yloc)

simuname = 'sap_trees_sou'
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_trees_sou_2018_10_09'
WTs3 = getSimWT(simdir,simuname,xloc,yloc)

#~ simuname = 'sap_trees_sah'
simuname = 'sap_trees_sah'
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_trees_sah_2018_10_12'
WTs4 = getSimWT(simdir,simuname,xloc,yloc)

simuname = 'sap_trees_sah_lowWT'
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_trees_sah_lowWT_2018_10_12'
WTs5 = getSimWT(simdir,simuname,xloc,yloc)

#~ simuname = 'sap_herb_sah_laismall'
#~ simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_herb_sah_laismall_2018_10_12'
#~ WTs6_side = getSimWT(simdir,simuname,xloc,yloc)
simuname = 'sap_trees_sah_deeproot'
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_trees_sah_deeproot_2018_10_12'
WTs6 = getSimWT(simdir,simuname,xloc,yloc)


# x= 30 y = 0 according to Matlab script:

xloc=15-1
yloc=4-1
simuname = 'sap_herb_sou2'
#~ simuname = 'sap_herb_sou3'
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_herb_sou2_2018_10_12'
#~ simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_herb_sou3_2018_10_11'
WTs1_side = getSimWT(simdir,simuname,xloc,yloc)

simuname = 'sap_herb_sah'
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_herb_sah_2018_10_12'
WTs2_side = getSimWT(simdir,simuname,xloc,yloc)

simuname = 'sap_trees_sou'
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_trees_sou_2018_10_09'
WTs3_side = getSimWT(simdir,simuname,xloc,yloc)

simuname = 'sap_trees_sah'
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_trees_sah_2018_10_12'
WTs4_side = getSimWT(simdir,simuname,xloc,yloc)

simuname = 'sap_trees_sah_lowWT'
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_trees_sah_lowWT_2018_10_12'
WTs5_side = getSimWT(simdir,simuname,xloc,yloc)

#~ simuname = 'sap_herb_sah_laismall'
#~ simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_herb_sah_laismall_2018_10_12'
#~ WTs6_side = getSimWT(simdir,simuname,xloc,yloc)
simuname = 'sap_trees_sah_deeproot'
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_trees_sah_deeproot_2018_10_12'
WTs6_side = getSimWT(simdir,simuname,xloc,yloc)


""" Read Forcings: """
forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_herb_sou2_2018_10_12'
forc = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')
forc.rename(columns=forcing_headers,inplace=True)
forc.index=pd.date_range('1/1/2005 00:00:00', periods=len(forc), freq='30min')


""" Read Forcings: """
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_trees_sah_2018_10_12'
forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
#### WATCH out, the separator is space in this file, no pb with CLM??
forc2 = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')
forc2.rename(columns=forcing_headers,inplace=True)
forc2.index=pd.date_range('1/1/2005 00:00:00', periods=len(forc2), freq='30min')


""" Plot """
tsim1 = pd.date_range('1/1/2005 00:00:00', periods=np.shape(WTs1)[1], freq='D')
tsim2 = pd.date_range('1/1/2005 00:00:00', periods=np.shape(WTs2)[1], freq='D')
tsim3 = pd.date_range('1/1/2005 00:00:00', periods=np.shape(WTs3)[1], freq='D')
tsim4 = pd.date_range('1/1/2005 00:00:00', periods=np.shape(WTs4)[1], freq='D')
tsim5 = pd.date_range('1/1/2005 00:00:00', periods=np.shape(WTs5)[1], freq='D')
tsim6 = pd.date_range('1/1/2005 00:00:00', periods=np.shape(WTs6)[1], freq='D')
#~ fig, ax = plt.subplots(2, 1,figsize=(20,10))
#A4 in inches is 8.3 * 11.7
fig, ax = plt.subplots(2,1,figsize=(8,4))
tsim = pd.date_range('1/1/2005 00:00:00', periods=np.shape(WTs1)[1], freq='D')

for i in range(np.shape(WTs1)[0]):
    lines1, = ax[0].plot(tsim1,WTs1[i,:],'k',linewidth=0.7)
    #~ lines1, = ax[0,0].plot(tsim,WTssap_herb_sou[i,:],'k',linewidth=0.7)
for i in range(np.shape(WTs2)[0]):
    lines2, = ax[0].plot(tsim2,WTs2[i,:],'r',linewidth=0.5)
for i in range(np.shape(WTs3)[0]):
    lines3, = ax[0].plot(tsim3,WTs3[i,:],'b',linewidth=0.5)    
for i in range(np.shape(WTs4)[0]):
    lines4, = ax[0].plot(tsim4,WTs4[i,:],'g',linewidth=0.5)    
for i in range(np.shape(WTs5)[0]):
    lines5, = ax[0].plot(tsim5,WTs5[i,:],'m',linewidth=0.5)       
for i in range(np.shape(WTs6)[0]):
    lines6, = ax[0].plot(tsim6,WTs6[i,:],'c',linewidth=0.5)       
ax[0].set_xlim([datetime.datetime(2005,1,1),max(tsim)])
#~ ax[0].set_ylim([0,7.9])
ax[0].set_ylim([0,10.9])
ax[0].set_ylim([0,13])
ax[0].set_ylim([0,10])
#~ ax[0].set_ylim([0,20.9])
#~ ax[0].legend((lines1,lines2,lines3,lines4,lines5),('sap_herb_sou','sap_herb_sah','sap_trees_sou','sap_trees_sah','sap_trees_sah_lowWT'),loc='lower right',fontsize=7,ncol=5)
#~ ax[0].legend((lines1,lines2,lines3,lines4,lines5,lines6),('sap_herb_sou','sap_herb_sah','sap_trees_sou','sap_trees_sah','sap_trees_sah_lowWT','sap_herb_sah_laismall'),loc='lower right',fontsize=7,ncol=6)
ax[0].legend((lines1,lines2,lines3,lines4,lines5,lines6),('herb_sou','herb_sah','trees_sou','trees_sah','trees_sah_lowWT','trees_sah_deeproot'),loc='lower right',fontsize=7,ncol=6)
ax[0].set_ylabel('WT depth (m)', fontsize=9, color = 'k')
ax[0].tick_params(axis='both', which='major', labelsize=9,bottom='on',top='on',right='on',left='on')
ax[0].tick_params(axis='x', which='both', labelbottom='off', labeltop='off')
ax[0].text(datetime.datetime(2005,1,20),0.6,r'a)',FontSize=9)

for i in range(np.shape(WTs1_side)[0]):
    lines1, = ax[1].plot(tsim1,WTs1_side[i,:],'k',linewidth=0.7)
    #~ lines1, = ax[0,0].plot(tsim,WTssap_herb_sou[i,:],'k',linewidth=0.7)
for i in range(np.shape(WTs2_side)[0]):
    lines2, = ax[1].plot(tsim2,WTs2_side[i,:],'r',linewidth=0.5)
for i in range(np.shape(WTs3_side)[0]):
    lines3, = ax[1].plot(tsim3,WTs3_side[i,:],'b',linewidth=0.5)    
for i in range(np.shape(WTs4_side)[0]):
    lines4, = ax[1].plot(tsim4,WTs4_side[i,:],'g',linewidth=0.5)    
for i in range(np.shape(WTs5_side)[0]):
    lines5, = ax[1].plot(tsim5,WTs5_side[i,:],'m',linewidth=0.5)       
for i in range(np.shape(WTs6_side)[0]):
    lines6, = ax[1].plot(tsim6,WTs6_side[i,:],'c',linewidth=0.5)       
    #~ lines2, = ax[0,0].plot(tsim,WTsnoBF[i,:],'r',linewidth=0.5)
ax[1].set_xlim([datetime.datetime(2005,1,1),max(tsim1)])
#~ ax[1].set_ylim([0,7.9])
ax[1].set_ylim([0,10.9])
ax[1].set_ylim([0,13])
ax[1].set_ylim([0,22])
#~ ax[1].legend((lines1,lines2,lines3,lines4,lines5),('sap_herb_sou','sap_herb_sah','sap_trees_sou','sap_trees_sah','sap_trees_sah_lowWT'),loc='upper right',fontsize=7,ncol=5)
#~ ax[1].legend((lines1,lines2,lines3,lines4,lines5,lines6),('sap_herb_sou','sap_herb_sah','sap_trees_sou','sap_trees_sah','sap_trees_sah_lowWT','sap_herb_sah_laismall'    ),loc='upper right',fontsize=7,ncol=6)
ax[1].legend((lines1,lines2,lines3,lines4,lines5,lines6),('herb_sou','herb_sah','trees_sou','trees_sah','trees_sah_lowWT','trees_sah_deeproot'),loc='upper right',fontsize=7,ncol=6)
ax[1].tick_params(axis='both', which='major', labelsize=9,bottom='on',top='on',right='on',left='on')
ax[1].set_ylabel('WT depth (m)', fontsize=9, color = 'k')
ax[1].tick_params(axis='y', which='both', labelleft='on', labelright='off')
ax[1].text(datetime.datetime(2005,1,20),0.6,r'b)',FontSize=9)

ax2 = ax[1].twinx()
ax2.plot(forc['P'].resample('D').sum()*30*60,'b')
ax2.plot(forc2['P'].resample('D').sum()*30*60,'r')
ax2.set_ylim([0,190])
ax2.set_yticks([0,20,40,60])
ax2.yaxis.set_label_position("right")
#~ ax2.tick_params(axis='y',colors='b',labelsize=16)
#~ ax2.set_ylabel('Precip (mm/d)', fontsize=16, color = 'b')
ax2.tick_params(axis='y',colors='b',labelsize=9)
ax2.set_ylabel('Precip (mm/d)', fontsize=9, color = 'b')

for i in range(2):
    fig.axes[i].invert_yaxis()
#~ fig.subplots_adjust(bottom=0.03, top =0.95,wspace=0.04, hspace=0.001)
fig.subplots_adjust(bottom=0.05, top =0.98,left=0.07,right =0.93,wspace=0.04, hspace=0.001)

simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/attractors/NCout/sap_herb_sou_2018_10_09'
plt.savefig(os.sep.join([simdir,'Figure_compare_WT_outlet.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir,'Figure_compare_WT_outlet.pdf']),dpi=400,format='pdf')
#~ plt.savefig(os.sep.join([simdir,'Figure_compare_WT.png']),dpi=400,format='png')
#~ plt.savefig(os.sep.join([simdir,'Figure_compare_WT.pdf']),dpi=400,format='pdf')
