#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Anaylse PF outputs for Vshape paper


    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import pandas as pd
import time
import os,glob
import matplotlib.pyplot as plt
import datetime
#Added for handling NC (to be moved to another library)
#~ import kolinkelly as kk
#~ import joestork as js
from netCDF4 import Dataset

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from procsycz import readDataAMMA as rdA

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def roundDateTime(shakyDT,nearest_minutes=1):
	"""
	"""
	shakyDT += datetime.timedelta(minutes=nearest_minutes/2)
	shakyDT -= datetime.timedelta(minutes=shakyDT.minute % nearest_minutes,
                         seconds=shakyDT.second,
                         microseconds=shakyDT.microsecond)
	return shakyDT
	
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
simuname = 'ref'
#~ simuname = 'ref_n13'
#~ simuname = 'sap'
#~ simuname = 'mix'
#~ simuname = 'noBF'
#~ simuname = 'herb'
#~ simuname = 'trees'

simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_2018_03_13'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_n13_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/sap_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/mix_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/noBF_2018_03_29'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/herb_2018_03_13'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/trees_2018_03_29'

# starting in July 2020:
simdir = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/herb_2018_03_13'
simuname = 'herb'
#~ simdir = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/trees_2018_03_29'
#~ simuname = 'trees'
#~ simdir = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_n13_2018_03_28'
#~ simuname = 'ref_n13'
n_coupled_layers = 13
n_CLM_2Dlayers = 14

"""
CLM_var is a dictionnary where keys are local variable names as in PFCLM
values are lists of string[filename, PFCLM variable name, long name, units]
"""
CLM_var={'lh_tot':['','eflx_lfh_tot','latent heat','W/m2'],\
        'lwrad_out':['','eflx_lwrad_out','outgoing lw radiation','W/m2'],\
        'sh_tot':['','eflx_sh_tot','sensible heat','W/m2'],\
        'soil_grnd':['','eflx_soil_grnd','ground heat flux','W/m2'],\
        'evap_tot':['','qflx_evap_tot','total evapotranspiration','mm/s'],\
        'evap_ground':['','qflx_evap_grnd','ground evaporation without condesation','mm/s'],\
        'evap_soil':['','qflx_evap_soi','soil evaporation','mm/s'],\
        'evap_veg':['','qflx_evap_veg','vegetation evaporation + transpiration','mm/s'],\
        'tran_veg':['','qflx_tran_veg','vegetation transpiration','mm/s'],\
        't_grnd':['','t_grnd','Ground temperature','K'],\
        'surf_press':['','h_top','Surface pressure','m'],\
        }
""" add the filename in the dic"""        
for k,val in CLM_var.items():
    CLM_var[k][0]=os.sep.join([simdir,''.join([simuname,'_'+val[1]+'.nc'])])
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

#### Read & process simulation outputs:
sim = PFout.PFout(simdir,'doro')

#sim.set_reading_parameters(n_t=2921,n_t_clm=70080,n_t_forc = 70080*2, dump_int=24,dump_int_clm=1,conv=1,conv_clm=1 * 60 * 60, conv_forc= 1 * 60 * 30)

"""
sim.read_domain_dimensions_from_PFBs()
sim.read_allPFoutput_from_PFBs()
sim.read_CLMoutput(nlayers = n_coupled_layers+n_CLM_2Dlayers)
#~ sim.extract_CLM_2Doutputs(nlayers = 16 +14 )
sim.read_allconstant_outputs()
sim.read_forcings()
sim.calculate_runoff(x_outlet = 7,y_outlet = -1)
#~ sim.calculate_WSC()
"""
"""NetCDF"""
sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_eflx_lh_tot.nc'])]),filetype='CLM')

"""Read PF outputs"""
start_time = time.time()
with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
    sim.satur_array = ds.variables['saturation'][:]
    topmask=np.swapaxes(ds.variables['top_mask'][:],0,1)
    vegmap=np.swapaxes(ds.variables['veg_map'][:],0,1)
    xslopes=np.swapaxes(ds.variables['xslope'][:],0,1)
    
"""MASK is a nx x ny boolean mask of the domain
When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
(as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
"""
MASK=topmask>0 
with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
    sim.press_array = ds.variables['succion'][:] 

"""Read CLM outputs"""
for k,val in CLM_var.items():
    try:
        with Dataset(val[0], 'r') as ds:
            print('read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
            exec('sim.%s=np.swapaxes(ds.variables[\'%s\'][:],0,2)'%(k,val[1]))        
    except FileNotFoundError:
            print('CANNOT read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
print("--- %d seconds --- read all PFCLM outputs from NCs" % (time.time() - start_time))

""" Read constant files"""
ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
#watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,1)
sim.mannings_arr = np.zeros((sim.nx,sim.ny))+5.5e-6
sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,1)
sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,1)
sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,1)
#~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))

""" Read Forcings: """
forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
#~ forc = pd.read_csv(os.sep.join([simdir,'forcagePF.txt.0']),header=None,sep='\t')
forc = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')
forc.rename(columns=forcing_headers,inplace=True)
forc.index=pd.date_range('1/1/2005 00:00:00', periods=len(forc), freq='30min')
sim.P=sum(forc['P'])*30*60


"""Create CLM output DataFrames: """
dsclm=pd.DataFrame()
CLM_varsubset=['evap_tot','evap_ground','evap_soil','evap_veg','tran_veg']
for k in CLM_varsubset:
    exec('dsclm[k]=sim.%s.mean(axis=0).mean(axis=0)*3600'%k)

sim.calculate_runoff_from_press(x_outlet = 7,y_outlet = 35)
dsclm['Q']=sim.surfQ
dsclm.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t_clm, freq='H')
dsclm['P']=(forc['P']*3600).resample('H').sum()

dsclmD_ref=pd.DataFrame()
for val in dsclm.columns:
    if val=='Q': dsclmD_ref[val] = dsclm[val].resample('D').sum()
    if val=='P': dsclmD_ref[val] = dsclm[val].resample('D').sum()
    else: dsclmD_ref[val] = dsclm[val].resample('D').mean()

dsclmD=pd.DataFrame()
dsclmD_min=pd.DataFrame()
dsclmD_max=pd.DataFrame()
dsclmDherb=pd.DataFrame()
dsclmDherb_min=pd.DataFrame()
dsclmDherb_max=pd.DataFrame()
dsclmDtrees=pd.DataFrame()
dsclmDtrees_min=pd.DataFrame()
dsclmDtrees_max=pd.DataFrame()
for k in CLM_varsubset:
    exec('dsclmD[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[MASK].mean(axis=0)'%k)
    exec('dsclmD_min[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[MASK].min(axis=0)'%k)
    exec('dsclmD_max[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[MASK].max(axis=0)'%k)
    
    exec('dsclmDherb[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[(vegmap==19.) & (MASK) & (xslopes!=0.)].mean(axis=0)'%k)
    #tmp as in july 2020:
    #~ exec('dsclmDherb_min[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[(vegmap==19.) & (MASK) & (xslopes!=0.)].min(axis=0)'%k)
    #~ exec('dsclmDherb_max[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[(vegmap==19.) & (MASK) & (xslopes!=0.)].max(axis=0)'%k)
    dsclmDherb_min[k]=dsclmDherb[k]
    dsclmDherb_max[k]=dsclmDherb[k]
    exec('dsclmDtrees[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[(vegmap==7.) & (MASK) & (xslopes!=0.)].mean(axis=0)'%k)
    #tmp as in july 2020:
    #~ exec('dsclmDtrees_min[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[(vegmap==7.) & (MASK) & (xslopes!=0.)].min(axis=0)'%k)
    #~ exec('dsclmDtrees_max[k]=np.reshape(sim.%s*3600,[sim.nx,sim.ny,sim.n_t_clm//24,24]).sum(axis=3)[(vegmap==7.) & (MASK) & (xslopes!=0.)].max(axis=0)'%k)
    dsclmDtrees_min[k]=dsclmDtrees[k]
    dsclmDtrees_max[k]=dsclmDtrees[k]
dsclmD['P'] = dsclm['P'].resample('D').sum()
dsclmD['Q'] = dsclm['Q'].resample('D').sum()
dsclmD.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
dsclmD_min.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
dsclmD_max.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')

dsclmDherb.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
dsclmDherb_min.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
dsclmDherb_max.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')

dsclmDtrees.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
dsclmDtrees_min.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')
dsclmDtrees_max.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')

""" Get ET data: see readDataAMMA for hardwired paths to change if the data is elsewhere """
ETdata = rdA.getLASET_and_oldECET_Nalohou()
naloD = rdA.getNaloET()
beleD = rdA.getBeleET()
ETdata.rename(columns={'LE_EC':'ETobs'},inplace=True)
naloD=pd.concat([naloD,ETdata])
beleD=pd.concat([beleD,ETdata['LE_LAS']])

etfig,etax=plt.subplots()
etax.plot(dsclmD['evap_tot'])
etax.plot(dsclmD_ref['evap_tot'])
etax.fill_between(dsclmD['evap_tot'].index,\
    np.reshape(np.array(dsclmD_min['evap_tot']),len(dsclmD_min),1),\
    np.reshape(np.array(dsclmD_max['evap_tot']),len(dsclmD_max),1),\
    alpha=0.5)
etax.plot(naloD)
etax.plot(beleD)
etax.legend([CLM_var['evap_tot'][2]])
etax.set_ylabel('mm/d')

fig,ax = plt.subplots(nrows=3,figsize=(15,10),sharex=True, squeeze=True)

ax[0].plot(dsclmD['evap_tot'])
ax[0].fill_between(dsclmD['evap_tot'].index,\
    np.reshape(np.array(dsclmD_min['evap_tot']),len(dsclmD_min),1),\
    np.reshape(np.array(dsclmD_max['evap_tot']),len(dsclmD_max),1),\
    alpha=0.5)
ax[0].plot(naloD,marker='+',markersize=4,linestyle='-',linewidth=0.5)
ax[0].legend(['sim','obs: EC','obs: LAS'])
ax[0].set_ylabel('mm/d')
ax[0].set_xlim([datetime.datetime(2006,1,1), datetime.datetime(2010,12,31)])

ax[1].plot(dsclmDherb['evap_tot'])
ax[1].fill_between(dsclmDherb['evap_tot'].index,\
    np.reshape(np.array(dsclmDherb_min['evap_tot']),len(dsclmD_min),1),\
    np.reshape(np.array(dsclmDherb_max['evap_tot']),len(dsclmD_max),1),\
    alpha=0.5)
ax[1].plot(naloD,marker='+',markersize=4,linestyle='-',linewidth=0.5)
ax[1].legend(['sim: herbaceous','obs: EC','obs: LAS'])
ax[1].set_ylabel('mm/d')
ax[1].set_xlim([datetime.datetime(2006,1,1), datetime.datetime(2010,12,31)])

ax[2].plot(dsclmDtrees['evap_tot'])
ax[2].fill_between(dsclmDtrees['evap_tot'].index,\
    np.reshape(np.array(dsclmDtrees_min['evap_tot']),len(dsclmD_min),1),\
    np.reshape(np.array(dsclmDtrees_max['evap_tot']),len(dsclmD_max),1),\
    alpha=0.5)
ax[2].plot(beleD,marker='+',markersize=4,linestyle='-',linewidth=0.5)
ax[2].legend(['sim: trees','obs: EC','obs: LAS'])
ax[2].set_ylabel('mm/d')
ax[2].set_xlim([datetime.datetime(2006,1,1), datetime.datetime(2010,12,31)])

#~ plt.savefig(os.sep.join([simdir,'ET.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir,'ETbis.png']),dpi=400,format='png')

# july 2020: get T/ET for Simon Carriere

T_ET = pd.concat([dsclmD['tran_veg'],dsclmD['evap_tot']],axis=1)
T_ET.rename({'tran_veg':'T (mm/d)','evap_tot':'ET (mm/d)'},axis=1,inplace=True)
T_ET.rename_axis('date',inplace=True)
T_ET.to_csv(os.sep.join([simdir,'T_ET_%s.csv'%simuname]))

T_ET.plot()
plt.savefig(os.sep.join([simdir,'T_ET_%s.png'%simuname]),dpi=400,format='png')

simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/herb_2018_03_13'
simuname1 = 'herb'
simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/trees_2018_03_29'
simuname2 = 'trees'
simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_n13_2018_03_28'
simuname3 = 'ref_n13'

T_ET_herb = pd.read_csv(os.sep.join([simdir1,'T_ET_%s.csv'%simuname1]))
T_ET_herb.set_index('date',inplace=True)
T_ET_herb['T/ET']=T_ET_herb.iloc[:,0]/T_ET_herb.iloc[:,1]

T_ET_trees = pd.read_csv(os.sep.join([simdir2,'T_ET_%s.csv'%simuname2]))
T_ET_trees.set_index('date',inplace=True)
T_ET_trees['T/ET']=T_ET_trees.iloc[:,0]/T_ET_trees.iloc[:,1]

T_ET_ref = pd.read_csv(os.sep.join([simdir3,'T_ET_%s.csv'%simuname3]))
T_ET_ref.set_index('date',inplace=True)
T_ET_ref['T/ET']=T_ET_ref.iloc[:,0]/T_ET_ref.iloc[:,1]

T_ET_sim=pd.concat([T_ET_ref['T/ET'],T_ET_herb['T/ET'],T_ET_trees['T/ET']],name=['ref','herb','trees'],axis=1)
T_ET_sim.index=pd.to_datetime(T_ET_sim.index)

T_ET_sim = T_ET_sim.loc[datetime.datetime(2006,1,1):,:]
T_ET_sim.to_csv(os.sep.join([simdir1,'T_ET_refn13_herb_trees.csv']))
T_ET_sim.to_csv(os.sep.join([simdir2,'T_ET_refn13_herb_trees.csv']))
T_ET_sim.to_csv(os.sep.join([simdir3,'T_ET_refn13_herb_trees.csv']))

T_ET_sim.resample('30D').mean().plot() 
plt.savefig(os.sep.join([simdir1,'T_ET_refn13_herb_trees.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir2,'T_ET_refn13_herb_trees.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir3,'T_ET_refn13_herb_trees.png']),dpi=400,format='png')
