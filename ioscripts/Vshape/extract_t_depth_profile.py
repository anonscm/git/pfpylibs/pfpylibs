#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    DESCRIPTION

    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import datetime
import pandas as pd
from netCDF4 import Dataset
import os,glob
import time
import matplotlib.cm as cm
"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from procsycz import readDataAMMA as rdA

plt.close("all")

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
simuname = 'ref'
simuname = 'ref_n13'
#~ simuname = 'sap'
#~ simuname = 'mix'
#~ simuname = 'noBF'
#~ simuname = 'herb'
#~ simuname = 'trees'

simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_2018_03_13'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_n13_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/sap_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/mix_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/noBF_2018_03_29'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/herb_2018_03_13'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/trees_2018_03_29'

n_coupled_layers = 13
n_CLM_2Dlayers = 14
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

### Read & process simulation outputs:
sim = PFout.PFout(simdir,'doro')

#sim.set_reading_parameters(n_t=2921,n_t_clm=70080,n_t_forc = 70080*2, dump_int=24,dump_int_clm=1,conv=1,conv_clm=1 * 60 * 60, conv_forc= 1 * 60 * 30)

"""
sim.read_domain_dimensions_from_PFBs()
sim.read_allPFoutput_from_PFBs()
sim.read_CLMoutput(nlayers = n_coupled_layers+n_CLM_2Dlayers)
#~ sim.extract_CLM_2Doutputs(nlayers = 16 +14 )
sim.read_allconstant_outputs()
sim.read_forcings()
sim.calculate_runoff(x_outlet = 7,y_outlet = -1)
#~ sim.calculate_WSC()
"""
"""NetCDF"""

sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_eflx_lh_tot.nc'])]),filetype='CLM')

"""Read PF outputs"""
start_time = time.time()
with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
    sim.satur_array = np.transpose(ds.variables['saturation'][:],[3,2,1,0])
    topmask=np.swapaxes(ds.variables['top_mask'][:],0,1)
"""MASK is a nx x ny boolean mask of the domain
When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
(as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
"""
MASK=topmask>0 
with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
    sim.press_array = np.transpose(ds.variables['succion'][:],[3,2,1,0])

""" Read constant files"""
ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
#watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,2)
sim.mannings_arr = np.zeros((sim.nx,sim.ny))+5e-6
sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,1)
sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,2)
sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,2)
#~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))
sim.dz = np.diff(ds.variables['depth'][:])[0]
sim.z = np.cumsum(sim.dz_mult_arr[0,0,::-1]*sim.dz) - sim.dz_mult_arr[0,0,::-1]*sim.dz/2
sim.z = np.round(sim.z*1000)/1000


""" Read Forcings: """
forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
#~ forc = pd.read_csv(os.sep.join([simdir,'forcagePF.txt.0']),header=None,sep='\t')
forc = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')

forc.rename(columns=forcing_headers,inplace=True)
forc.index=pd.date_range('1/1/2005 00:00:00', periods=len(forc), freq='30min')
sim.P=sum(forc['P'])*30*60

""" Get WT data"""
rt_dir = r'/home/hectorb/DATA/WT/Nalohou'
station_list = ['DC1','DC4','DB4']
P=rdA.StaDic()
P.read_WTD_from_Nalohou(filename = os.sep.join([rt_dir,'Piezo_Basile_95.xls']), station_list = station_list)

""" extract a T-D slice """
# x= 140 y = 430 according to Matlab script:
""" show how it matches well to BF when xloc = 7-1 instead of 8-1 !"""
xloc=7-1
yloc = 22-1
yloc = 23-1
TD = sim.satur_array[xloc,yloc,:,:]
newz = np.arange(0,8,0.02)
TD2=np.zeros([len(newz),sim.n_t])
for t in range(sim.n_t):
    TD2[:,t] = np.interp(newz,sim.z,TD[::-1,t])
    
domain_thickness = np.round(np.cumsum(sim.dz_mult_arr[0,0,::-1]*sim.dz)[-1]*1000)/1000
depth_last_cell = sim.z[-1]
WT = domain_thickness - sim.press_array[xloc,yloc,0,:]
WT = depth_last_cell - sim.press_array[xloc,yloc,0,:]


""" Get All Water tables (perched)"""
#first get a mask of the base layers of each saturated (p>0) contiguous (over Z) unit
maskWT=np.zeros([sim.nz,sim.n_t])
for t in range(sim.n_t):
    profile = np.zeros(sim.nz)
    #profile[0] = base of domain. if the criteria is p < 0 => 0, you realize there are some weird points at the bottom
    #of perched layers with p>0 but < sim.dz ... so as if the cell is not entirely saturated but p>0 ... 
    profile = np.array([0. if sim.press_array[xloc,yloc,i,t]<= sim.dz_mult_arr[xloc,yloc,i]*sim.dz else 1. for i in range(sim.nz)])
    for i in range(sim.nz-1):
        profile[i+1]=(profile[i]+profile[i+1])*profile[i+1]
    # a=[1,1,1,0,0,0,1,1,0,1,0,0,0,1] => [1, 2, 3, 0, 0, 0, 1, 2, 0, 1, 0, 0, 0, 1]
    profile[profile!=1.]=np.nan
    maskWT[:,t] = profile

WTs = np.resize(sim.z[::-1],[sim.n_t,sim.nz]).T - (maskWT*sim.press_array[xloc,yloc,:,:])


""" Plot """
tsim = pd.date_range('1/1/2005 00:00:00', periods=sim.n_t, freq='D')
fig, ax = plt.subplots(1, 1,figsize=(20,10))
ax.pcolormesh(tsim,newz,TD2,cmap=cm.jet_r, shading = 'gouraud')
#~ ax.plot(tsim,WT,'r')
for i in range(sim.nz):
    ax.plot(tsim,WTs[i,:],'r')
ax.plot(P['DB4'].wt,'g')
ax.plot(P['DC4'].wt,'g')

""" Trick to not plot lines during the dry season in the pearched WT"""
#make sure no duplicate:
P['DC1'].wt = P['DC1'].wt.groupby(P['DC1'].wt.index).mean()
P['DC1'].wt.sort_index
diffvec=np.diff(P['DC1'].wt.index).astype('timedelta64[D]')                                                       
loc_PointsHighOffset = [pos[0] for pos, val in np.ndenumerate(diffvec) if val.astype(int)>100]
#~ locDate = [P['DC1'].wt.index[i-1]+ (P['DC1'].wt.index[i]-P['DC1'].wt.index[i-1])/2 for i in loc_PointsHighOffset]
locDate = [P['DC1'].wt.index[i]+ (P['DC1'].wt.index[i+1]-P['DC1'].wt.index[i])/2 for i in loc_PointsHighOffset]
for loc in locDate:
    print(loc)
    P['DC1'].wt.at[loc]=None
P['DC1'].wt = P['DC1'].wt.sort_index()

ax.plot(P['DC1'].wt,'g')

ax.set_xlim([datetime.datetime(2006,1,1),max(tsim)])

fig.axes[0].invert_yaxis()
leg = plt.cm.ScalarMappable(cmap=cm.jet_r)
leg.set_array([0.,1.])
cb = fig.colorbar(leg, ax = ax)
cb.set_label('Saturation',fontsize=14)

ax.set_ylabel('Depth (m)', fontsize=16, color = 'k')
ax.tick_params(axis='both', which='major', labelsize=16)

#~ plt.savefig(os.sep.join([simdir,'WT.png']),dpi=400,format='png')
