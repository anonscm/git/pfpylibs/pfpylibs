#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Anaylse PF outputs for Vshape paper

    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import os

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
rootdir = '/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/'
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
filename=os.path.join(rootdir,'X_Vshape_slopes.pfb')
[x, hdr] =  PFlibs.io_pfb.PFB_read(filename,1)
filename=os.path.join(rootdir,'Y_Vshape_slopes.pfb')
[y, hdr] =  PFlibs.io_pfb.PFB_read(filename,1)

PFlibs.io_pfb.PFB_write(x*2, hdr, os.path.join(rootdir,'X_Vshape_slopes_2x.pfb'))
PFlibs.io_pfb.PFB_write(y*2, hdr, os.path.join(rootdir,'Y_Vshape_slopes_2x.pfb'))
PFlibs.io_pfb.PFB_write(x*0.5, hdr, os.path.join(rootdir,'X_Vshape_slopes_05x.pfb'))
PFlibs.io_pfb.PFB_write(y*0.5, hdr, os.path.join(rootdir,'Y_Vshape_slopes_05x.pfb'))

