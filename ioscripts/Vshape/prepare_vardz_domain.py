#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    SImple script to prepare var_dz and domains.pfb for Oueme

    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np


"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
inputdir = '/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/'


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

""" Prepare var dz :"""
v,h = PFlibs.io_pfb.PFB_read(inputdir+'var_dz.pfb',1)
#~ v[:,:,-1]=16.0
v[:,:,-1]=4.0
v[:,:,-2]=6.0
v[:,:,-3]=6.0
v[:,:,-4]=6.0

#~ PFlibs.io_pfb.PFB_write(v[:,:,::-1],h,inputdir+'var_dz_17_5m.pfb')


#~ v = np.array([0.09,0.39,0.75,0.75,1.2,1.8,12.9])
#~ vardz = v/3.0
#~ vardz3D = np.tile(vardz,(144,144,1))

#~ h ={'X0': 334500.0,
 #~ 'Y0': 987500.0,
 #~ 'Z0': 0.0,
 #~ 'dx': 1000.0,
 #~ 'dy': 1000.0,
 #~ 'dz': 3.0,
 #~ 'ix': 0,
 #~ 'iy': 0,
 #~ 'iz': 0,
 #~ 'ns': 1,
 #~ 'nx': 144,
 #~ 'nx2': 144,
 #~ 'ny': 144,
 #~ 'ny2': 144,
 #~ 'nz': 7,
 #~ 'nz2': 7,
 #~ 'rx': 1,
 #~ 'ry': 1,
 #~ 'rz': 1}




