"""
    PFPYLIBS - IOSCRIPTS

    Anaylse PF outputs for Vshape paper: Streamflow
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os,glob
from netCDF4 import Dataset
import time
import datetime
from matplotlib.dates import MonthLocator, DateFormatter
"""local imports:"""
from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout

plt.close("all")


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
simdir = r'/mnt/hectorb/srv12/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/ref/'
#~ simuname = 'clay_normal_klesshigh2'
simuname = 'ref'
simuname = 'ref_n13'
#~ simuname = 'ref_n12'
#~ simuname = 'sap'
#~ simuname = 'mix'
#~ simuname = 'noBF'
#~ simuname = 'herb'
#~ simuname = 'trees'

#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/paper/outputs/clay_normal_klesshigh2_2016_06_07'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_2018_03_13'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_n13_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_n12_2018_03_31'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/sap_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/mix_2018_03_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/noBF_2018_03_29'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/herb_2018_03_13'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/trees_2018_03_29'

n_coupled_layers = 13
n_CLM_2Dlayers = 14

statsDay =True
#~ statsDay =False

"""
CLM_var is a dictionnary where keys are local variable names as in PFCLM
values are lists of string[filename, PFCLM variable name, long name, units]
"""
CLM_var={'surf_press':['','h_top','Surface pressure','m']}
""" add the filename in the dic"""        
for k,val in CLM_var.items():
    CLM_var[k][0]=os.sep.join([simdir,''.join([simuname,'_'+val[1]+'.nc'])])

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def minimalist_xldate_as_datetime(xldate, datemode):
    # datemode: 0 for 1900-based, 1 for 1904-based
    return (
        datetime.datetime(1899, 12, 30)
        + datetime.timedelta(days=xldate + 1462 * datemode)
        )
        
def roundDateTime(shakyDT,nearest_minutes=1):
	"""
	"""
	shakyDT += datetime.timedelta(minutes=nearest_minutes/2)
	shakyDT -= datetime.timedelta(minutes=shakyDT.minute % nearest_minutes,
                         seconds=shakyDT.second,
                         microseconds=shakyDT.microsecond)
	return shakyDT
	

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

#### Read & process simulation outputs:
sim = PFout.PFout(simdir,'doro')
sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_eflx_lh_tot.nc'])]),filetype='CLM')

"""Read PF outputs"""
start_time = time.time()
with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
    sim.satur_array = ds.variables['saturation'][:]
    topmask=np.swapaxes(ds.variables['top_mask'][:],0,1)
"""MASK is a nx x ny boolean mask of the domain
When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
(as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
"""
MASK=topmask>0 
with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
    sim.press_array = ds.variables['succion'][:] 

"""Read CLM outputs"""
for k,val in CLM_var.items():
    try:
        with Dataset(val[0], 'r') as ds:
            print('read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
            exec('sim.%s=np.swapaxes(ds.variables[\'%s\'][:],0,2)'%(k,val[1]))        
    except FileNotFoundError:
            print('CANNOT read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
print("--- %d seconds --- read all PFCLM outputs from NCs" % (time.time() - start_time))

""" Read constant files"""
ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
#watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,1)
sim.mannings_arr = np.zeros((sim.nx,sim.ny))+5e-6
sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,1)
sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,1)
sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,1)
#~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))

""" Read Forcings: """
forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
#~ forc = pd.read_csv(os.sep.join([simdir,'forcagePF.txt.0']),header=None,sep='\t')
forc = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')
forc.rename(columns=forcing_headers,inplace=True)
forc.index=pd.date_range('1/1/2005 00:00:00', periods=len(forc), freq='30min')
sim.P=sum(forc['P'])*30*60


"""Create CLM output DataFrames: """
dsclm=pd.DataFrame()
#~ sim.calculate_runoff_from_press(x_outlet = 7,y_outlet = 35)
# to compare obs with sim runoff:
sim.calculate_runoff_from_press(x_outlet = 7,y_outlet = 32)
dsclm['Q']=sim.surfQ
catchmentsurf=np.sum(MASK)*sim.dx*sim.dy
# to compare obs witj sim runoff
catchmentsurf=np.sum(MASK[:,0:-3])*sim.dx*sim.dy

""" move from m3/s (hr?) to mm/h"""
dsclm['Q']=dsclm['Q']*1000/catchmentsurf
dsclm.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t_clm, freq='H')
dsclm['P']=(forc['P']*3600).resample('H').sum()

dsclmD_ref=pd.DataFrame()
for val in dsclm.columns:
    if val=='Q': dsclmD_ref[val] = dsclm[val].resample('D').sum()
    if val=='P': dsclmD_ref[val] = dsclm[val].resample('D').sum()
    else: dsclmD_ref[val] = dsclm[val].resample('D').mean()

dsclmD=pd.DataFrame()

dsclmD['P'] = dsclm['P'].resample('D').sum()
dsclmD['Q'] = dsclm['Q'].resample('D').sum()
dsclmD.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t-1, freq='D')

"""Get streamflow data"""
filename=r'/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/analyse/Data/flow_parshall_2006_2012.txt'
df = pd.read_csv(filename,header=None, sep='\t')
df.rename(columns={0:'date',1:'Qobs',2:'QBFobs'},inplace=True)
df['date']=df['date'].apply(lambda x: minimalist_xldate_as_datetime(x, 0))
df = df.set_index('date')
""" from m3/s to mm/hr"""
dfmms=df*1000./120000.
df=df*3600.*1000./120000.

#~ df.resample('5min').bfill()
""" from mm/s to mm/time step """
#~ df['Qobs']=df['Qobs']*df.index.to_series().diff().dt.total_seconds().fillna(0)

"""No idea why this data seems strange!!"""
rt_dir = r'/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/analyse/Data/streamflow/Nalohou/CE.Run_Odc_BD_AMMA-CATCH_2016_03_01'
suf_pattern = '.csv'
pre_pattern = 'CE.Run_Odc-'
df2 = pd.DataFrame()
sta = rdA.Station(name = 'PARSHALL')    
filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,'PARSHALL']),suf_pattern]))        
sta.read_Q(filepattern, data_col = 2)   
#~ sta.Q = sta.Q*sta.Q.index.to_series().diff().dt.total_seconds().fillna(0)
df2['Qobs2']=sta.Q*3600.*1000./120000.
df2['Qobs2'][df2['Qobs2']>=1000]=np.nan
#~ df2=df2.resample('H').sum()


def pdinterpolate(ts, datetime_index):
    """could decide to fill nans !
    but not adapted for calculating scores?
    To be tested"""
    x = pd.concat([ts, pd.Series(index=datetime_index)])
    #~ return x.groupby(x.index).first().sort_index().fillna(method="ffill")[datetime_index]
    return x.groupby(x.index).first().sort_index().interpolate('linear')[datetime_index]
    #~ return x.groupby(x.index).first().sort_index()[datetime_index]

d=df['Qobs'][df.index.year==2009]
m=dsclm['Q'][dsclm.index.year==2009]

m_interp=pdinterpolate(m, d.index).dropna()

"""
fig,ax = plt.subplots(nrows=2,figsize=(15,10), sharex=True, squeeze=True)
ax[0].plot(d,'k',linewidth=2.0)
ax[0].plot(m,'r',linewidth=0.4)	
ax[0].set_ylim([0,0.95])
ax[0].set_xlim([datetime.datetime(2009,6,15), datetime.datetime(2009,11,15)])
ax[0].set_ylabel('mm/hr')
ax[0].legend(['Qobs','Qsim'])   
 
ax[1].plot(d,'k',linewidth=2.0)
ax[1].plot(m_interp,'r',linewidth=0.4)	
ax[1].set_ylim([0,0.95])
ax[1].set_xlim([datetime.datetime(2009,6,15), datetime.datetime(2009,11,15)])
ax[1].set_ylabel('mm/hr')
ax[1].legend(['Qobs','Qsim_interp'])   
fig.subplots_adjust(bottom=0.03, top =0.95, hspace=0.001)
"""
""" Calculate objective functions """

"""PLOT"""
    
#~ fig,ax = plt.subplots(nrows=len(np.arange(2008,2013,1)),figsize=(15,10),sharex=True, squeeze=True)
fig,ax = plt.subplots(nrows=len(np.arange(2008,2013,1)),figsize=(20,10), squeeze=True)
for i,yr in enumerate(np.arange(2008,2013,1)):
        """
        in matlab:
        unique obs where retained , then interpolated on simulated timesteps
        nans set to 0; then stats where calculated on mean daily flow 
        
        other option:
        - Round the observations times at the hour time step
        - for each hour which has more than 1 obs: get the obs as the mean
        - simulation output is at a fixed time step: drop timesteps where obs is not available
        - calculate stats on remaining couples
        
        """
    
        print(yr)
        obs = df['Qobs'][df.index.year==yr]
        #~ obs = obs[(obs.index.month>=6) & (obs.index.month<=11) ]
        #~ obs = dfmms['Qobs'][df.index.year==yr]
        simu = dsclm['Q'][dsclm.index.year==yr]
        
        if statsDay==True:
            # interpolation on sim + daily approach:
            obs = obs.groupby(obs.index).mean()
            obs_int=pd.concat([obs,simu],axis=1).interpolate(method='linear')['Qobs']
            obs_intD = obs_int.resample('D').mean()
            simuD = simu.resample('D').mean()
            merge=pd.concat([obs_intD,simuD],axis=1)
            #
        else:
            # Interpolation on the closest available obs - sim couples:
            #simuD = simu.resample('D').asfreq()
            #simuD = dsclm['Q'][dsclm.index.hour==12]
            simu = simu[(simu.index.month>=6) & (simu.index.month<=11)]
            new_dates = obs.index.round('H')
            obs2=obs
            obs2.index = new_dates
            obs2 = obs2.groupby(obs2.index).mean()
            merge=pd.concat([obs2,simu],axis=1)
            #merge=pd.concat([obs2,simuD],axis=1)
        
        kept = merge.dropna()
        res = obj.kge(kept['Qobs'].values, kept['Q'].values, return_all=True)
        print(res)        
        
        
        ax[i].plot(obs,'k',linewidth=2.0)
        ax[i].plot(simu,'r',linewidth=0.4)	
        ax[i].set_ylim([0,0.99])
        ax[i].set_xlim([datetime.datetime(yr,6,15), datetime.datetime(yr,11,15)])
        # every month
        months = MonthLocator(bymonth=range(7, 12), bymonthday=1, interval=1)
        monthsFmt = DateFormatter("%b")
        ax[i].xaxis.set_major_locator(months)
        ax[i].xaxis.set_major_formatter(monthsFmt)
        ax[i].set_ylabel('mm/hr',FontSize=17)
        #~ ax[i].plot(kept['Qobs'],linestyle='', marker='o', color='g',markersize=2)
        #~ ax[i].plot(kept['Q'],linestyle='', marker='o', color='g',markersize=2)        
        ax[i].text(datetime.datetime(yr,10,25),0.65,r'$r^2 = %2.2f$'%(res[1]*res[1]),FontSize=17)
        ax[i].text(datetime.datetime(yr,10,25),0.5,r'KGE = %2.2f'%res[0],FontSize=17)
        ax[i].text(datetime.datetime(yr,10,25),0.8,r'%d'%yr,FontSize = 17, fontweight='bold')
        ax[i].tick_params(axis='both', which='major', labelsize=16)
ax[0].legend(['obs','sim'],loc='upper left',fontsize=17)    

fig.subplots_adjust(bottom=0.03, top =0.95, hspace=0.001)
if statsDay==True:
    plt.savefig(os.sep.join([simdir,'Q_statsDay.png']),dpi=400,format='png')
else:
    plt.savefig(os.sep.join([simdir,'Q_statsAllPts.png']),dpi=400,format='png')

"""
yr=2010

fig,ax = plt.subplots(nrows=len(dsclmD.columns),figsize=(15,10),sharex=True, squeeze=True)
for i in range(len(ax)):
    if (dsclmD.columns[i] == 'P'):
        #~ ax[i].plot(dsclm[dsclmD.columns[i]])
        ax[i].plot(dsclm[dsclmD.columns[i]][dsclm.index.year==yr])
        ax[i].legend(['Precip'])
        ax[i].set_ylabel('mm/d')
    elif (dsclmD.columns[i] == 'Q'):
        #~ ax[i].plot(df['Qobs'],'k',linewidth=2.0)
        ax[i].plot(df['Qobs'][df.index.year==yr],'k',linewidth=2.0)
        #~ ax[i].plot(df2['Qobs2'],'b')	
        #~ ax[i].plot(dsclm[dsclmD.columns[i]],'r',linewidth=0.2)	
        ax[i].plot(dsclm[dsclmD.columns[i]][dsclm.index.year==yr],'r',linewidth=0.2)	
        ax[i].set_ylim([0,0.95])
        ax[i].legend(['Qobs','Qsim'])    
        #~ ax[i].legend(['Qobs','Qobs2'])
	
    else:
        ax[i].plot(dsclmD[dsclmD.columns[i]])
        ax[i].plot(dsclmD_ref[dsclmD_ref.columns[i]])
        ax[i].legend([CLM_var[dsclmD.columns[i]][2]])
        ax[i].set_ylabel('mm/d')

fig.subplots_adjust(bottom=0.03, top =0.95, hspace=0.001)

plt.show()
"""
