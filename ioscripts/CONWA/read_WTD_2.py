#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    quick read & plot of WTD Parflow 
    
    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import xarray as xr

"""local imports:"""


plt.close('all')

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##


##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

"""reads in PF WTD"""
#~ WTD = xr.open_dataset('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_mean_y9.nc')
#~ WTD = xr.open_dataset('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_mean_y12.nc')
WTD = xr.open_dataset('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_mean_y14.nc')
#~ WTD_amp = xr.open_dataset('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_amplitude_y9.nc')
#~ WTD_amp = xr.open_dataset('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_amplitude_y12.nc')
WTD_amp = xr.open_dataset('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_amplitude_y14.nc')
#~ WTD_trend = xr.open_dataset('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_trend_y9.nc')
#~ WTD_trend = xr.open_dataset('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_trend_y12.nc')
WTD_trend = xr.open_dataset('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_trend_y14.nc')
#~ WTD.WTD.attrs['units']='m'
WTD_amp.WTD.attrs['units']='m'
WTD_trend.WTD.attrs['units']='m'
WTD_amp = WTD_amp.rename_vars({'WTD':'WTD_amp'})    
WTD_trend = WTD_trend.rename_vars({'WTD':'WTD_trend'})    


""" Plot amplitude & trend """

fig,ax = plt.subplots(1,2,figsize=(14,5),squeeze=True)
### WTD_amp:
WTD_amp.WTD_amp.plot(ax=ax[0],add_labels=False,vmin=0,vmax=5,cbar_kwargs={'label':'WTD amplitude (m)'})
#~ ax[0].set_title('WTD amplitude (yr 10)')
#~ ax[0].set_title('WTD amplitude (yr 13)')
ax[0].set_title('WTD amplitude (yr 15)')
### WTD_trend:
#~ WTD_trend.WTD_trend.plot(ax=ax[1],add_labels=False,vmin=-3,vmax=3,cbar_kwargs:{'label':'WTD trend (m/yr)'}}
WTD_trend.WTD_trend.plot(ax=ax[1],add_labels=False,vmin=-1.5,vmax=1.5,cbar_kwargs={'label':'WTD trend (m/yr)'})
#~ ax[1].set_title('WTD trend (yr 10)')
#~ ax[1].set_title('WTD trend (yr 13)')
ax[1].set_title('WTD trend (yr 15)')
for a in ax.flat: a.set_aspect(1); a.set_xticklabels(''); a.set_yticklabels('')
#~ plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_amplitude_trend_y9.png')
#~ plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_amplitude_trend_y12.png')
plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_amplitude_trend_y14.png')

""" Plot amp-trend """
detrend = WTD_amp.rename_vars({'WTD_amp':'WTD'}) - WTD_trend.rename_vars({'WTD_trend':'WTD'})  
fig,ax = plt.subplots(1,1,figsize=(7,5),squeeze=True)
### WTD_amp:
detrend.WTD.plot(ax=ax,add_labels=False,vmin=0,vmax=50,cmap='terrain',cbar_kwargs={'label':'WTD (m)'})
ax.set_aspect(1); ax.set_xticklabels(''); ax.set_yticklabels('')
#~ plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_y9.png')
#~ plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_y12.png')
plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_y14.png')
""" Plot mean """
fig,ax = plt.subplots(1,1,figsize=(7,5),squeeze=True)
### WTD_amp:
WTD.WTD.plot(ax=ax,add_labels=False,vmin=0,vmax=50,cmap='terrain',cbar_kwargs={'label':'WTD (m)'})
ax.set_aspect(1); ax.set_xticklabels(''); ax.set_yticklabels('')
#~ plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_y9.png')
#~ plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_y12.png')
plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_y14.png')
