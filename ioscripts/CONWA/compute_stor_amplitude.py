
#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    compute yearly storage amplitude

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os, glob, copy
import xarray as xr

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb

plt.close('all')

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
stor_file = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/post_spinup/d_storage_y14_vcorr.nc'
stor_file = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/nc_results/d_storage_y0_full.nc'
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

stor = xr.open_dataset(stor_file)

stor_amp = stor.max(dim='time') - stor.min(dim='time')

#~ stor_amp.to_netcdf('/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/post_spinup/storage_amplitude_y14.nc',format ="NETCDF4")
stor_amp.to_netcdf('/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/eval/WSC/domain_setups/CONWA_AD/simulations/fullyear1/storage_amplitude_y0_full.nc',format ="NETCDF4")
# should we use http://xarray.pydata.org/en/stable/generated/xarray.Dataset.to_netcdf.html unlimited dims?

stor_trend = stor.isel(time=-1) - stor.isel(time=0)
stor_trend.to_netcdf('/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/eval/WSC/domain_setups/CONWA_AD/simulations/fullyear1/storage_trend_y0_full.nc',format ="NETCDF4")

""" Compute monthly averages """
#uncomment if needed

#~ ET_corr.groupby("time.month").mean().to_netcdf('/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/post_spinup/evaptranssum_y9_vcorr2_monthly.nc',format ="NETCDF4")
