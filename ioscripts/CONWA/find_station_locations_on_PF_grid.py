#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    example script to find the location of stations on the PF grid
    ADHI DB as obtained from    
    
    Tramblay, Y., Rouché, N., Paturel, J.-E., Mahé, G., Boyer, J.-F., 
    Amoussou, E., Bodian, A., Dacosta, H., Dakhlaoui, H., Dezetter, A., 
    Hughes, D., Hanich, L., Peugeot, C., Tshimanga, R. and Lachassagne, P.: 
    The African Database of Hydrometric Indices (ADHI), 
    Earth System Science Data Discussions, 1–21, 
    https://doi.org/10.5194/essd-2020-281, 2020.
    
    currently steps:
    - step 1: load a PF grid : Either hard code the grid parameters, 
                use pfpylibs scripts to read a tiff or pfb or read netcdf
    - step 2: load some x,y data : Get streamflow data Oueme
    
    - step 3: convert data to PF projection & find station locations on
                PF coordinates
            Here we make use of the following "trick":
        in the example below all 3 stations should have the PF coordinate of the center 
        of cell so if a transformation of PF coordinate allow the llcorners 
        coordinates to increment to unity, the same transf can be applied to the 
        data points to get their cell values directly. 
        eg. data 1 = [18565,88552], data2 =[18120,89053],data4=[19100,89000]
        PF dx,dy = 1000
        center of cell is 18300,88700, so ll is [17800,88200]
        so we do x offset = +200 & y_offset = -200
        ll becomes [18000,88000]
        data1 becomes [18765,88352]
        data2 becomes [18320,88853]
        data4 becomes [19300,88800]
         ___________
        |         .3|
        |  .2       |  .4
        |    _|_    |
        |     |     |
        |         .1|
        |___________|
        ll corner

        cell coordinate (data1) = [floor(data[0]/1000)*1000-x_offset+dx/2, floor(data[1]/1000)*1000-y_offset+dy/2
        
    - step 4 (optional) load a stream network and find station location 
            on the streams. Flag stations that can be moved (stream cell
            distance < 1km) (flag = 2)

    - step 5 (optionnal) load a drainage area map and find catchment area
            in PF for each station. for remaining stations, find the 
            nearby cell within a 5km radius box where draining area 
            most closely match PF's (flag = 3)

    - step 6 (optionnal) flag stations with obvious mismatch of 
            catchment size : TODO (challenge: e.g; endoreism in Niger catchment)

    - step 7 (optionnal) inspect individual stations and correct location
            manually if needed: TODO

    - step 8 save outputs


    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import shapefile
import copy,os
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
import pyproj

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb
from procsycz import readDataAMMA as rdA
from procsycz import procGeodata_Gdal

plt.close('all')
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
projAEA = pyproj.Proj("+proj=aea +lat_1=16.12 +lat_2=6.58 +lat_0=11.35 +lon_0=-4.3 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs")
projUTM = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')

#data_dir is where the OUTPUT, formatted streamflow data will be stored:
# (if applicable: this preproc can be done elsewhere)
data_dir = '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/eval/streamflow/data/'

# domain_setup dir: directory where to save outputs relative to the specific grid:
domain_setup_dir = '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/eval/streamflow/domain_setups/CONWA_AD/'

#data_loc_dir is where the OUTPUT, formatted streamflow data LOCATION will be stored:
# (if applicable: this preproc can be done elsewhere)
data_loc_dir = os.sep.join([domain_setup_dir,'data_loc/'])

# aux_data_dir is where auxiliary data (eg draining area map, streamflow map)
#can be found: it is not specific to a single simulation but can be used for se
aux_data_dir = os.sep.join([domain_setup_dir,'aux_data/'])

#setup_figures_dir is directory where to save the figures relative to the specifc grid (stations locations, pf stream network, etc)
setup_figures_dir = os.sep.join([domain_setup_dir,'figures/'])

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


def read_plot_shapefile(filename,ax):
    #~ sf = shapefile.Reader(filename)
    #for some reason after fresh install of shapefile library (pip install pyshp), the following spec is needed
    sf = shapefile.Reader(filename,encoding = 'latin-1')
    recs    = sf.records()
    shapes  = sf.shapes()
    Nshp    = len(shapes)
    cns     = []
    for nshp in range(Nshp):
        cns.append(recs[nshp][1])
    cns = np.array(cns)
    cm    = plt.get_cmap('Dark2')
    cccol = cm(1.*np.arange(Nshp)/Nshp)
    #   -- plot --
    #~ fig     = plt.figure()
    #~ ax      = fig.add_subplot(111)
    for nshp in range(Nshp):
        ptchs   = []
        pts     = np.array(shapes[nshp].points)
        prt     = shapes[nshp].parts
        par     = list(prt) + [pts.shape[0]]
        for pij in range(len(prt)):
            ptchs.append(Polygon(pts[par[pij]:par[pij+1]]))
        #~ ax.add_collection(PatchCollection(ptchs,facecolor=cccol[nshp,:],edgecolor='k', linewidths=.1))
        ax.add_collection(PatchCollection(ptchs,facecolor=[0,0,1,0],edgecolor='k', linewidths=.1))
    return ax


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##



""" step 1 load a PF grid """
## option 1: hard code the grid parameters
x0 = -1425000.0
y0 = -779000.0
dx = 1000; dy = 1000;
nx = 2880
ny = 1600

## option 2: read a pfb: data,header = iopfb.PFB_read(filename,1)
#~ data,header = iopfb.PFB_read(filename,1)
#~ x0 = header['X0']
#~ y0 = header['Y0']
#~ dx = header['dx']
#~ dy = header['dy']
#~ nx = header['nx']
#~ ny = header['ny']

## option 3: read a tiff: header, vecx, vecy = PF.get_PF_grid_from_Gtiff(filename)

## option 4: read a netcdf...

x = np.arange(nx)*dx+x0
y = np.arange(ny)*dy+y0

""" step 2 load some x,y data """
ADHI = pd.read_csv('/home/hectorb/DATA/streamflow/ADHI/ADHI/ADHI_stations.tab',sep='\t')

""" step 3 convert data to PF projection & find PF coordinates"""

ADHI['xAEA'],ADHI['yAEA'] = pyproj.transform(geo_system,projAEA,np.array(ADHI.loc[:,'Longitude']), np.array(ADHI.loc[:,'Latitude']))

#option possible when steps increments unitarily in coordinates (or transformation is possible)
xlowerleft = x-dx/2 #that's made to go from say x0 = -1425000.0 to x0 ll = -1424500.0
ylowerleft = y-dy/2
# make a vector which increments unitarily (this will depend on resolution)
xoffset = - dx/2 #(could be + too: that's made to go from say x0ll = -1424500.0 to x0 ll = -1424000.0 to have it rounded)
yoffset = - dy/2
xlowerleftround = xlowerleft + xoffset 
ylowerleftround = ylowerleft + yoffset
"""
in the example below all 3 stations should have the PF coordinate of the center 
of cell so if a transformation of PF coordinate allow the llcorners 
coordinates to increment to unity, the same transf can be applied to the 
data points to get their cell values directly. 
eg. data 1 = [18565,88552], data2 =[18120,89053],data4=[19100,89000]
PF dx,dy = 1000
center of cell is 18300,88700, so ll is [17800,88200]
so we do x offset = +200 & y_offset = -200
ll becomes [18000,88000]
data1 becomes [18765,88352]
data2 becomes [18320,88853]
data4 becomes [19300,88800]
 ___________
|         .3|
|  .2       |  .4
|    _|_    |
|     |     |
|         .1|
|___________|
ll corner

cell coordinate (data1) = [floor(data[0]/1000)*1000-x_offset+dx/2, floor(data[1]/1000)*1000-y_offset+dy/2

"""
ADHI['xPF'] = np.floor((ADHI['xAEA'] + xoffset)/1000)*1000 - xoffset +dx/2
ADHI['yPF'] = np.floor((ADHI['yAEA'] + yoffset)/1000)*1000 - yoffset +dy/2
ADHI['iPF'] = ((ADHI['xPF'] - x0)/dx).apply(lambda x:int(x))
ADHI['jPF'] = ((ADHI['yPF'] - y0)/dy).apply(lambda x:int(x))
# drop stations outside the domain:
ADHI = ADHI.loc[(ADHI.iPF>0) & (ADHI.iPF<2880) & (ADHI.jPF>0) & (ADHI.jPF<1600)] 

""" step 4 (optional) load a stream network and find station location 
on the streams. Flag stations that needs be moved """

""" step 4.a : load stream network"""
#~ stream,xstream,ystream = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/CONWA/active/CONWA_AD/preproc/DEM/CONWA_MERIT.segments_1000_05_ep001_200504.out.tif",nodata_value = 255)
stream,xstream,ystream = procGeodata_Gdal.readRasterWithGdal(aux_data_dir+"/CONWA_MERIT.segments_1000_05_ep001_200504.out.tif",nodata_value = 255)
[xx,yy]=np.meshgrid(xstream-dx/2,ystream+dy/2) # no idea why xstream[0] is -1424500., and ystream[0] -779500.,while x0 is -1425000.0 and y0 is -779000.0
stream = stream.astype(float)
stream[stream==0] = np.nan
stream = stream[::-1,:]
#tmp save for thierry
#~ lonlon,latlat = pyproj.transform(projAEA,geo_system,xx,yy)
#~ pd.DataFrame({'x':xx.flatten(),'y':yy.flatten(),'lon':np.round(lonlon.flatten()*1000)/1000,'lat':np.round(latlat.flatten()*1000)/1000}).to_csv('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/CONWA_AD/PF_grid_albers_to_geo.csv')
#stream is meshgrid-like, that is [ny,nx], ny being correctly in increasing order. So pF-compatible version is:
streamPF = np.swapaxes(stream,0,1)


""" step 4.b :  Flag points already on major streams: FLAG = 1 """
ADHI = ADHI.assign(onStream= ADHI.apply(lambda x: 1.0 if not np.isnan(stream[x.jPF,x.iPF]) else np.nan,axis=1).values)
ADHI = ADHI.assign(xStream= ADHI.apply(lambda x: x.xPF if x.onStream == 1.  else np.nan,axis=1).values)
ADHI = ADHI.assign(yStream= ADHI.apply(lambda x: x.yPF if x.onStream == 1.  else np.nan,axis=1).values)
ADHI = ADHI.assign(iStream= ADHI.apply(lambda x: x.iPF if x.onStream == 1.  else np.nan,axis=1).values)
ADHI = ADHI.assign(jStream= ADHI.apply(lambda x: x.jPF if x.onStream == 1.  else np.nan,axis=1).values)

print('%d stations out of %d are already located on a stream'%(len(ADHI.onStream.dropna()),len(ADHI)))

""" step 4.c :  identify points with rivers nearby (<1km) FLAG = 2 """
## move points that are nearby major streams: FLAG = 2
for index, row in ADHI.iterrows():
    if np.isnan(row.onStream):
        # crop a window 25km radius
        stream_tmp = stream[ADHI.loc[index,'jPF'].item() - 25 : ADHI.loc[index,'jPF'].item() + 25 , ADHI.loc[index,'iPF'].item() - 25 : ADHI.loc[index,'iPF'].item() + 25]
        xx_tmp = xx[ADHI.loc[index,'jPF'].item() - 25 : ADHI.loc[index,'jPF'].item() + 25 , ADHI.loc[index,'iPF'].item() - 25 : ADHI.loc[index,'iPF'].item() + 25]
        yy_tmp = yy[ADHI.loc[index,'jPF'].item() - 25 : ADHI.loc[index,'jPF'].item() + 25 , ADHI.loc[index,'iPF'].item() - 25 : ADHI.loc[index,'iPF'].item() + 25]

        # find the matching stream cell: 
        xx_tmp2 = copy.deepcopy(xx_tmp)
        yy_tmp2 = copy.deepcopy(yy_tmp)
        xx_tmp2[np.isnan(stream_tmp)]=np.nan       
        yy_tmp2[np.isnan(stream_tmp)]=np.nan       
        
        dist_to_river = np.sqrt((xx_tmp2 - ADHI.loc[index,'xAEA'].item())**2 + (yy_tmp2 - ADHI.loc[index,'yAEA'].item())**2)
        
        if dist_to_river.size:
            if np.nanmin(dist_to_river) <= 2*np.sqrt(dx*dx + dy*dy):
                ind_min = np.where(dist_to_river == np.nanmin(dist_to_river))
                ADHI.loc[index,'onStream'] = 2 #flag for modifying cell according to PF river nearby
                ADHI.loc[index,'xStream'] = xx_tmp2[ind_min][0]
                ADHI.loc[index,'yStream'] = yy_tmp2[ind_min][0]
                ADHI.loc[index,'iStream'] = (xx_tmp2[ind_min][0]- x0)/dx
                ADHI.loc[index,'jStream'] = (yy_tmp2[ind_min][0]- y0)/dy

print('%d stations out of %d are less than 1 km of a stream cell'%(sum(ADHI.onStream==2),len(ADHI)))

"""  plot the whole map: """
fig = plt.figure(figsize=(10,8))
ax = fig.add_subplot(111)
ax.set_aspect(1)
p0 = ax.pcolormesh(xx-dx/2, yy-dy/2, stream)
l1, = ax.plot(ADHI.xPF.to_numpy(),ADHI.yPF.to_numpy(),'ro',markersize=1,markeredgewidth=1)
l2, = ax.plot(ADHI.xAEA.to_numpy(),ADHI.yAEA.to_numpy(),'b+',markersize=1,markeredgewidth=1)
l3, = ax.plot(ADHI.loc[ADHI.onStream==1,'xPF'].to_numpy(),ADHI.loc[ADHI.onStream==1,'yPF'].to_numpy(),'go',markersize=3,markeredgewidth=3)
l4, = ax.plot(ADHI.loc[ADHI.onStream==2,'xStream'].to_numpy(),ADHI.loc[ADHI.onStream==2,'yStream'].to_numpy(),'bo',markersize=3,markeredgewidth=3)

props = dict(boxstyle='round', facecolor='white', alpha=0.5)
for index, row in ADHI.iterrows():
    ax.text(row.xAEA,row.yAEA,row.Name.split(sep=' ')[0],fontsize = 6, bbox=props)
#~ ax.legend((l1,l2),('PF station','True station'),fontsize=6,loc='upper left',ncol=2)
ax.legend((l1,l2,l3,l4),('PF station','True station','station on stream','station moved to nearby stream'),fontsize=6,loc='upper left',ncol=2)
####
plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/fullsim/ADHI_station_maps.png')
plt.savefig(setup_figures_dir+'ADHI_station_maps.png')


""" step 5 (optionnal) load a drainage area map and find catchment area
in PF for each station """
#~ area,xarea,yarea = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/CONWA/active/CONWA_AD/preproc/DEM/CONWA_MERIT.area_05_ep001_n.out.tif",nodata_value = 0)
area,xarea,yarea = procGeodata_Gdal.readRasterWithGdal(aux_data_dir+ "CONWA_MERIT.area_05_ep001_n.out.tif",nodata_value = 0)
area = area.astype(float)
area = area[::-1,:]

ADHI = ADHI.assign(PFarea = ADHI.apply(lambda x: area[x.jPF,x.iPF] if x.onStream == 1.  else np.nan,axis=1).values)
ADHI = ADHI.assign(PFarea = ADHI.apply(lambda x: area[int(x.jStream),int(x.iStream)] if x.onStream==2  else x.PFarea,axis=1).values)

diff_rel_catch_area = abs(ADHI.PFarea - ADHI.loc[:,'Catchment area (km2)'] )/ADHI.loc[:,'Catchment area (km2)']    
# plot the relative difference in catchment area: the plot is bounded between 0 and 1 but there are some high values probably due to Niger & endoreism areas
fig,ax = plt.subplots(1,1)
diff_rel_catch_area.hist(bins = np.arange(0,1,0.02)) 
ax.set_xlabel('$|\dfrac{PFarea - Realarea}{Realarea}|$')

""" step 5.b : for remaining stations, find the nearby pixel (within say 10km where draining area closely match PF's)"""

box_size = 5 #radius to look around the station (actually rectangle) 
for index, row in ADHI.iterrows():
    if np.isnan(row.onStream):
        # crop a window 
        area_tmp = area[ADHI.loc[index,'jPF'].item() - box_size : ADHI.loc[index,'jPF'].item() + box_size , ADHI.loc[index,'iPF'].item() - box_size : ADHI.loc[index,'iPF'].item() + box_size]
        xx_tmp = xx[ADHI.loc[index,'jPF'].item() - box_size : ADHI.loc[index,'jPF'].item() + box_size , ADHI.loc[index,'iPF'].item() - box_size : ADHI.loc[index,'iPF'].item() + box_size]
        yy_tmp = yy[ADHI.loc[index,'jPF'].item() - box_size : ADHI.loc[index,'jPF'].item() + box_size , ADHI.loc[index,'iPF'].item() - box_size : ADHI.loc[index,'iPF'].item() + box_size]

        # find the matching draining area cell: 
        closest_area = abs(area_tmp-ADHI.loc[index,'Catchment area (km2)'])
        
        closest_area_location = np.where(closest_area == closest_area.min())
       
        ADHI.loc[index,'onStream'] = 3 #flag for modifying cell according to PF river nearby
        ADHI.loc[index,'xStream'] = xx_tmp[closest_area_location][0]
        ADHI.loc[index,'yStream'] = yy_tmp[closest_area_location][0]
        ADHI.loc[index,'iStream'] = (xx_tmp[closest_area_location][0]- x0)/dx
        ADHI.loc[index,'jStream'] = (yy_tmp[closest_area_location][0]- y0)/dy

print('%d stations out of %d have been moved to a nearby cell with similar draining area'%(sum(ADHI.onStream==3),len(ADHI)))


"""  plot again the whole map: """
fig = plt.figure(figsize=(15,12))
ax = fig.add_subplot(111)
ax.set_aspect(1)
#~ p1 = ax.pcolormesh(xx-dx/2, yy-dy/2, area,vmin=100,vmax=5000)
p0 = ax.pcolormesh(xx-dx/2, yy-dy/2, stream)
l1, = ax.plot(ADHI.xPF.to_numpy(),ADHI.yPF.to_numpy(),'ro',markersize=1,markeredgewidth=1)
l2, = ax.plot(ADHI.xAEA.to_numpy(),ADHI.yAEA.to_numpy(),'b+',markersize=1,markeredgewidth=1)
l3, = ax.plot(ADHI.loc[ADHI.onStream==1,'xPF'].to_numpy(),ADHI.loc[ADHI.onStream==1,'yPF'].to_numpy(),'go',markersize=3,markeredgewidth=3)
l4, = ax.plot(ADHI.loc[ADHI.onStream==2,'xStream'].to_numpy(),ADHI.loc[ADHI.onStream==2,'yStream'].to_numpy(),'bo',markersize=3,markeredgewidth=3)
l5, = ax.plot(ADHI.loc[ADHI.onStream==3,'xStream'].to_numpy(),ADHI.loc[ADHI.onStream==3,'yStream'].to_numpy(),'co',markersize=3,markeredgewidth=3)

props = dict(boxstyle='round', facecolor='white', alpha=0.5)
for index, row in ADHI.iterrows():
    ax.text(row.xAEA,row.yAEA,row.Name.split(sep=' ')[0],fontsize = 5, bbox=props)
#~ ax.legend((l1,l2),('PF station','True station'),fontsize=6,loc='upper left',ncol=2)
ax.legend((l1,l2,l3,l4,l5),('PF station','True station','station on stream','station moved to nearby stream','station moved to similar draining area'),fontsize=6,loc='upper left',ncol=2)
####

plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/fullsim/ADHI_station_maps2.png',dpi=300)
plt.savefig(setup_figures_dir+'ADHI_station_maps2.png')

""" step 6 (optionnal) flag stations with obvious mismatch of catchment size """

""" step 7 (optionnal) inspect individual stations and correct location
manually if needed"""

""" step 8 save outputs """

ADHI.to_csv(os.sep.join([data_loc_dir,'ADHI/','ADHI_PF.csv']),sep='\t')

""" Get stream network:"""


""" Find the location of data in the PF framework, and calculate simulated Q:"""
data = pd.read_csv('/home/hectorb/DATA/streamflow/ADHI/ADHI/ADHI_summary.tab',sep='\t')
