
#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    compute yearly WTD amplitude, trends, and trend corrected amplitude (amplitude -trend)

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os, glob, copy
import xarray as xr

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb

plt.close('all')

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
WTD_file = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/post_spinup/WTD_y14_vcorr.nc'
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

WTD = xr.open_dataset(WTD_file)

WTD_amp = WTD.max(dim='time') - WTD.min(dim='time')
WTD_trend = WTD.isel(time=-1) - WTD.isel(time=0)
WTD_amp_detrend = WTD_amp-WTD_trend
WTD_mean = WTD.mean(dim='time')

WTD_amp.to_netcdf('/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/post_spinup/WTD_amplitude_y14.nc',format ="NETCDF4")
WTD_trend.to_netcdf('/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/post_spinup/WTD_trend_y14.nc',format ="NETCDF4")
WTD_mean.to_netcdf('/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/post_spinup/WTD_mean_y14.nc',format ="NETCDF4")
# should we use http://xarray.pydata.org/en/stable/generated/xarray.Dataset.to_netcdf.html unlimited dims?


""" Compute monthly averages """
#uncomment if needed

#~ ET_corr.groupby("time.month").mean().to_netcdf('/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/post_spinup/evaptranssum_y9_vcorr2_monthly.nc',format ="NETCDF4")
