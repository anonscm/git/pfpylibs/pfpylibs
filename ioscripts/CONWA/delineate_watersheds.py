#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    Delineate watersheds
    make use of pysheds to try to delineate watersheds and make vectors 
    shp from stations outlet and drainage maps as derived from priority flow
    
    pysheds
    https://github.com/mdbartos/pysheds
    
    pip install pysheds
    
    @copyright: 2021 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2021"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import fiona
import os
from shapely.geometry import Point, mapping
from geojson import dump
"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb
from pysheds.grid import Grid


plt.close('all')
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

"""create the .asc flow direction map by replacing "NA" with another NaN value
then copy and adapt the header of another asc"""
#~ flowdir = pd.read_table('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/CONWA_AD/preproc/DEM/CONWA_MERIT.direction_05_ep001_n.out.txt',sep=' ',header =None)   
#~ flowdir.fillna(9999).astype(int).to_csv('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/CONWA_AD/preproc/DEM/CONWA_MERIT.direction_05_ep001_n.out.asc',sep=' ',header=None,index=None)


# domain_setup dir: directory where to save outputs relative to the specific grid:
domain_setup_dir = '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/eval/streamflow/domain_setups/CONWA_AD/'

#data_loc_dir is where the OUTPUT, formatted streamflow data LOCATION will be stored:
# (if applicable: this preproc can be done elsewhere)
data_loc_dir = os.sep.join([domain_setup_dir,'data_loc/'])

# aux_data_dir is where auxiliary data (eg draining area map, streamflow map)
#can be found: it is not specific to a single simulation but can be used for se
aux_data_dir = os.sep.join([domain_setup_dir,'aux_data/'])

#setup_figures_dir is directory where to save the figures relative to the specifc grid (stations locations, pf stream network, etc)
setup_figures_dir = os.sep.join([domain_setup_dir,'figures/'])

direction_file =aux_data_dir+"CONWA_MERIT.direction_05_ep001_n.out.tif"
#~ direction_file = "/home/hectorb/PARFLOW/PROJECTS/CONWA/active/CONWA_AD/preproc/DEM/CONWA_MERIT.direction_05_ep001_n.out.asc"
#~ accumulation_file = "/home/hectorb/PARFLOW/PROJECTS/CONWA/active/CONWA_AD/preproc/DEM/CONWA_MERIT.area_05_ep001_n.out.tif"
accumulation_file = aux_data_dir+"CONWA_MERIT.area_05_ep001_n.out.tif"

"""Specify flow direction values
d8: for d4 just use not specified values for NE, SE, SW, NW?
"""
#N    NE    E    SE    S    SW    W    NW
dirmap = (3,6,4,7,1,8,2,9)
#~ dirmap = (1,6,2,7,3,8,4,9)
#~ dirmap2 = (1,5,4,5,3,5,2,5)


"""Load and process DBs"""
#ADHI:
#~ ADHI = pd.read_csv('/home/hectorb/DATA/streamflow/ADHI/ADHI/ADHI_PF.csv',sep='\t')
ADHI = pd.read_csv(data_loc_dir+'ADHI/ADHI_PF.csv',sep='\t')
ADHI.set_index(ADHI.Id,inplace=True)
shortname={'Benin':'Be', 'Burkina Faso':'BF', "Cote d'Ivoire":'CI', 'Ghana':'Gh', 'Guinea':'Gu',
       'Guinea-Bissau':'GB', 'Liberia':'Li', 'Mali':'Ma', 'Niger':'Ni', 'Nigeria':'Nia', 'Senegal':'Se',
       'Togo':'To'}
ADHI['Country_short']=ADHI.Country.apply(lambda x: shortname[x]) 
ADHI['Name_'] = ADHI.Name.apply(lambda x : '_'.join(x.split())) 
ADHI['Name_'] = ADHI.Name_.apply(lambda x : x.replace('(','').replace(')','').replace('?','').replace('=','')) 

#SIEREM:
#~ SIEREM = pd.read_csv('/home/hectorb/DATA/streamflow/Afrique_Sierem/SIEREM_PF.csv',sep='\t')
SIEREM = pd.read_csv(data_loc_dir+'SIEREM/SIEREM_PF.csv',sep='\t')
SIEREM.set_index('Id',inplace=True)
shortname2={"Cote d'Ivoire":'CI', "Côte d'Ivoire":'CI', 'Guinée':'Gu', 'Mali':'Ma', 'Sénégal':'Se'}
SIEREM['Country_short']=SIEREM.Pays.apply(lambda x: shortname2[x]) 
SIEREM.drop_duplicates(subset=['Name'],inplace=True)
SIEREM['Name_'] = SIEREM.Name.apply(lambda x : '_'.join(x.split())) 
SIEREM['Name_'] = SIEREM.Name_.apply(lambda x : x.replace('(','').replace(')','').replace('?','').replace('=','')) 
SIEREM = SIEREM.rename(columns={'Rivière':'River', 'Pays':'Country'})
SIEREM['Country_'] = SIEREM.Country.apply(lambda x: '_'.join(x.split()))
#AC:
#~ AC_Benin = pd.read_csv('/home/hectorb/DATA/streamflow/AMMA_Benin/AC_Benin.csv',sep='\t')
AC_Benin = pd.read_csv(data_loc_dir+'AC_Benin/AC_Benin_PF.csv',sep='\t')
AC_Benin.set_index(AC_Benin.columns[0],inplace=True)

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##


grid = Grid()

#define the grid: beware: grid.affine or grid.bbox or grid.extent are
# pretty clear: the bounding box is given similar to values in the .asc header
# so for CONWA: its offsets of 1000, not 500 (so when saved then as raster, 
# and opened into qgis, center of cells will be in ...500.)
grid.from_raster(direction_file,data_name='dir')
#~ grid.from_ascii(direction_file,data_name='dir')

grid.read_raster(direction_file, data_name='dir')
#~ grid.read_ascii(direction_file, data_name='dir')
grid.dir[grid.dir ==9999] = np.nan
plt.imshow(grid.dir)  

#optionnal:
grid.read_raster(accumulation_file, data_name='acc')


""" Find an outlet: and compute catchment"""
#clip back again to the whole domain:
grid.clip_to('dir')  
# this seems to work together with xytype='index' in grid.catchment:
x = int(AC_Benin.loc['BETEROU','iStream'])
y = int(grid.dir.shape[0] - AC_Benin.loc['BETEROU','jStream'])-1

grid.catchment(data='dir', x=x, y=y, dirmap=dirmap, out_name='catch',
               recursionlimit=20000, xytype='index', nodata_out=0)
"""
The grid.clip_to method clips the grid’s current view to nonzero 
elements in a given dataset. This is especially useful for clipping 
the view to an irregular feature like a delineated watershed."""
grid.clip_to('catch')
# Get a view of the catchment
#~ grid.view('dir',nodata = np.nan)    
#~ grid.view('catch',nodata = np.nan)    

"""plot catchment"""
fig, ax = plt.subplots(figsize=(8,6))
fig.patch.set_alpha(0)

plt.grid('on', zorder=0)
im = ax.imshow(grid.catch, extent=grid.extent, zorder=1, cmap='viridis')
boundaries = ([0] + sorted(list(dirmap)))
plt.colorbar(im, ax=ax, boundaries=boundaries, values=sorted(dirmap), label='Flow Direction')
plt.xlabel('Longitude')
plt.ylabel('Latitude')
plt.title('Delineated Catchment')
ax.set_aspect(1)
#~ plt.savefig('img/catchment.png', bbox_inches='tight')

""" get river network """
grid.clip_to('catch')
#~ grid.accumulation(data='catch', out_name='acc', dirmap=dirmap)
#~ plt.imshow(grid.acc) 
grid.clip_to('dir')
grid.clip_to('catch')

# create a temporary grid with nan - 1 values to delineate the stream network:
grid.acc2 = grid.view('acc')
grid.acc2[grid.acc2<100] = np.nan
grid.acc2[grid.acc2>=100] = 1
#~ grid.to_raster('acc2', '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/CONWA_AD/preproc/DEM/catchments_05_ep001_n_AC/AC_rivers.tif', blockxsize=16, blockysize=16)
grid.to_raster('acc2', data_loc_dir+'AC_Benin/catchments_05_ep001_n/AC_rivers.tif', blockxsize=16, blockysize=16)

"""convert to shapefile or raster?"""
#~ shapes = grid.polygonize('acc2')


# to make shapefile out of river network, but there's an issue with both accumulatio computation and 
# also with extract river network when using a predefined accumulation map (from PF)
#~ branches = grid.extract_river_network('catch', 'acc2',dirmap=dirmap)
#~ for branch in branches['features']:
    #~ line = np.asarray(branch['geometry']['coordinates'])
    #~ plt.plot(line[:, 0], line[:, 1])
# to save as geosjon:
#~ with open('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/CONWA_AD/preproc/DEM/catchments_05_ep001_n/AC_rivers.geojson', 'w') as f:
   #~ dump(branches, f)


    
""" loop over AC DB"""
print('loop over AC data: ')

schema_catchment = {'geometry': 'Polygon',
        'properties': {'Name': 'str','Latitude':'float','Longitude':'float',
        'area':'float','xPF':'float','yPF':'float','iPF':'int','jPF':'int',
        'onStream':'int','xStream':'float','yStream':'float','iStream':'int',
        'jStream':'int','PFarea':'float'}}

schema_outlet = {'geometry': 'Point',
            'properties': {'Name': 'str','Latitude':'float','Longitude':'float',
            'area':'float','xPF':'float','yPF':'float','iPF':'int','jPF':'int',
            'onStream':'int','xStream':'float','yStream':'float','iStream':'int',
            'jStream':'int','PFarea':'float'}}        
i = 0
# Write shapefile catchment
with fiona.open(data_loc_dir+'AC_Benin/catchments_05_ep001_n/AC_catchments.shp', 'w',
                    driver='ESRI Shapefile',
                    crs=grid.crs.srs,
                    schema=schema_catchment) as c:        
    for index, row in AC_Benin.iterrows():
        #clip back again to the whole domain:
        grid.clip_to('dir')  
        x = int(row.iStream)
        y = int(grid.dir.shape[0] - row.jStream)-1

        grid.catchment(data='dir', x=x, y=y, dirmap=dirmap, out_name=index,
                       recursionlimit=15000, xytype='index', nodata_out=0)

        grid.clip_to(index)

        """convert to shapefile"""
        shapes = grid.polygonize()
        for shape, value in shapes:
            rec = {}
            rec['geometry'] = shape
            rec['properties'] = {'Name' : index,'Latitude':row.Latitude,
            'Longitude':row.Longitude,'area':row['Catchment area (km2)'],
            'xPF':row.xPF,'yPF':row.yPF,'iPF':float(row.iPF),'jPF':float(row.jPF),
            'onStream':row.onStream,'xStream':row.xStream,'yStream':row.yStream,
            'iStream':row.iStream,'jStream':row.jStream,'PFarea':row.PFarea}
            rec['id'] = str(i)
            c.write(rec)
            i += 1
                
i = 0
# Write shapefile outlets
with fiona.open(data_loc_dir+'AC_Benin/outlets_05_ep001_n/AC_outlets.shp', 'w',
                    driver='ESRI Shapefile',
                    crs=grid.crs.srs,
                    schema=schema_outlet) as c:        
    for index, row in AC_Benin.iterrows():
        #clip back again to the whole domain:
        grid.clip_to('dir')  
        x = int(row.iStream)
        y = int(grid.dir.shape[0] - row.jStream)-1

        rec = {}
        point = Point(float(row['xStream']), float(row['yStream']))            
        rec['geometry'] = mapping(point)            
        rec['properties'] = {'Name' : index,'Latitude':row.Latitude,
        'Longitude':row.Longitude,'area':row['Catchment area (km2)'],
        'xPF':row.xPF,'yPF':row.yPF,'iPF':float(row.iPF),'jPF':float(row.jPF),
        'onStream':row.onStream,'xStream':row.xStream,'yStream':row.yStream,
        'iStream':row.iStream,'jStream':row.jStream,'PFarea':row.PFarea}
        rec['id'] = str(i)
        c.write(rec)
        i += 1
        
# save rasters of river network
for index, row in AC_Benin.iterrows():
    grid.clip_to(index)
    grid.acc2 = grid.view('acc')
    grid.acc2[grid.acc2<100] = np.nan
    grid.acc2[grid.acc2>=100] = 1
    #~ grid.to_raster('acc2', '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/CONWA_AD/preproc/DEM/rivers_05_ep001_n_AC/AC_rivers_%s.tif'%index, blockxsize=16, blockysize=16)
    grid.to_raster('acc2', data_loc_dir+'AC_Benin/rivers_05_ep001_n/AC_rivers_%s.tif'%index, blockxsize=16, blockysize=16)


print('loop over AC data: done')



""" loop over ADHI DB"""
print('loop over ADHI data: ')


schema_catchment = {'geometry': 'Polygon',
        'properties': {'Name': 'str','index':'str','Latitude':'float','Longitude':'float',
        'area':'float','Country':'str','num_dams':'int','xPF':'float','yPF':'float','iPF':'int','jPF':'int',
        'onStream':'int','xStream':'float','yStream':'float','iStream':'int',
        'jStream':'int','PFarea':'float'}}
schema_outlet = {'geometry': 'Point',
        'properties': {'Name': 'str','index':'str','Latitude':'float','Longitude':'float',
        'area':'float','Country':'str','num_dams':'int','xPF':'float','yPF':'float','iPF':'int','jPF':'int',
        'onStream':'int','xStream':'float','yStream':'float','iStream':'int',
        'jStream':'int','PFarea':'float'}}       
i = 0
# Write shapefile catchment
with fiona.open(data_loc_dir+'ADHI/catchments_05_ep001_n/ADHI_catchments.shp', 'w',
                    driver='ESRI Shapefile',
                    crs=grid.crs.srs,
                    schema=schema_catchment) as c:        
    for index, row in ADHI.iterrows():
        #clip back again to the whole domain:
        grid.clip_to('dir')  
        x = int(row.iStream)
        y = int(grid.dir.shape[0] - row.jStream)-1

        grid.catchment(data='dir', x=x, y=y, dirmap=dirmap, out_name=index,
                       recursionlimit=15000, xytype='index', nodata_out=0)

        grid.clip_to(index)

        """convert to shapefile"""
        shapes = grid.polygonize()
        for shape, value in shapes:
            rec = {}
            rec['geometry'] = shape
            rec['properties'] = {'Name' : str(row.Name_),'index':str(index),'Latitude':float(row.Latitude),
            'Longitude':float(row.Longitude),'area':float(row['Catchment area (km2)']),
            'Country':str(row.Country),'num_dams':float(row['Number of dams']),
            'xPF':float(row.xPF),'yPF':float(row.yPF),'iPF':float(row.iPF),'jPF':float(row.jPF),
            'onStream':float(row.onStream),'xStream':float(row.xStream),'yStream':float(row.yStream),
            'iStream':float(row.iStream),'jStream':float(row.jStream),'PFarea':float(row.PFarea)}
            rec['id'] = str(i)
            c.write(rec)
            i += 1
                
i = 0
# Write shapefile outlets
with fiona.open(data_loc_dir+'ADHI/outlets_05_ep001_n/ADHI_outlets.shp', 'w',
                    driver='ESRI Shapefile',
                    crs=grid.crs.srs,
                    schema=schema_outlet) as c:        
    for index, row in ADHI.iterrows():
        #clip back again to the whole domain:
        grid.clip_to('dir')  
        x = int(row.iStream)
        y = int(grid.dir.shape[0] - row.jStream)-1

        rec = {}
        point = Point(float(row['xStream']), float(row['yStream']))            
        rec['geometry'] = mapping(point)            
        rec['properties'] = {'Name' : str(row.Name_),'index':str(index),'Latitude':float(row.Latitude),
        'Longitude':float(row.Longitude),'area':float(row['Catchment area (km2)']),
        'Country':str(row.Country),'num_dams':float(row['Number of dams']),
        'xPF':float(row.xPF),'yPF':float(row.yPF),'iPF':float(row.iPF),'jPF':float(row.jPF),
        'onStream':float(row.onStream),'xStream':float(row.xStream),'yStream':float(row.yStream),
        'iStream':float(row.iStream),'jStream':float(row.jStream),'PFarea':float(row.PFarea)}
        rec['id'] = str(i)
        c.write(rec)
        i += 1
        
# save rasters of river network
for index, row in ADHI.iterrows():
    grid.clip_to(index)
    grid.acc2 = grid.view('acc')
    grid.acc2[grid.acc2<100] = np.nan
    grid.acc2[grid.acc2>=100] = 1
    #~ grid.to_raster('acc2', '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/CONWA_AD/preproc/DEM/rivers_05_ep001_n_AC/AC_rivers_%s.tif'%index, blockxsize=16, blockysize=16)
    grid.to_raster('acc2', data_loc_dir+'ADHI/rivers_05_ep001_n/ADHI_rivers_%s.tif'%index, blockxsize=16, blockysize=16)



print('loop over ADHI data: done')




""" loop over SIEREM DB"""
print('loop over SIEREM data: ')


schema_catchment = {'geometry': 'Polygon',
        'properties': {'Name': 'str','index':'str','Latitude':'float',
        'Longitude':'float','area':'float','Country':'str',
        'Bassin':'str','River':'str','xPF':'float','yPF':'float',
        'iPF':'int','jPF':'int','onStream':'int','xStream':'float',
        'yStream':'float','iStream':'int','jStream':'int','PFarea':'float'}}
schema_outlet = {'geometry': 'Point',
        'properties': {'Name': 'str','index':'str','Latitude':'float',
        'Longitude':'float','area':'float','Country':'str',
        'Bassin':'str','River':'str','xPF':'float','yPF':'float',
        'iPF':'int','jPF':'int','onStream':'int','xStream':'float',
        'yStream':'float','iStream':'int','jStream':'int','PFarea':'float'}}    
i = 0
# Write shapefile catchment
with fiona.open(data_loc_dir+'SIEREM/catchments_05_ep001_n/SIEREM_catchments.shp', 'w',
                    driver='ESRI Shapefile',
                    crs=grid.crs.srs,
                    schema=schema_catchment) as c:        
    for index, row in SIEREM.iterrows():
        #clip back again to the whole domain:
        grid.clip_to('dir')  
        x = int(row.iStream)
        y = int(grid.dir.shape[0] - row.jStream)-1

        grid.catchment(data='dir', x=x, y=y, dirmap=dirmap, out_name=str(index),
                       recursionlimit=15000, xytype='index', nodata_out=0)

        grid.clip_to(str(index))

        """convert to shapefile"""
        shapes = grid.polygonize()
        for shape, value in shapes:
            rec = {}
            rec['geometry'] = shape
            rec['properties'] = {'Name' : str(row.Name_),'index':str(index),'Latitude':float(row.Latitude),
            'Longitude':float(row.Longitude),'area':float(row['Catchment area (km2)']),
            'Country':str(row.Country),'Bassin':str(row['Bassin']),'River':str(row['River']),
            'xPF':float(row.xPF),'yPF':float(row.yPF),'iPF':float(row.iPF),'jPF':float(row.jPF),
            'onStream':float(row.onStream),'xStream':float(row.xStream),'yStream':float(row.yStream),
            'iStream':float(row.iStream),'jStream':float(row.jStream),'PFarea':float(row.PFarea)}
            rec['id'] = str(i)
            c.write(rec)
            i += 1

i = 0
# Write shapefile outlets
with fiona.open(data_loc_dir+'SIEREM/outlets_05_ep001_n/SIEREM_outlets.shp', 'w',
                    driver='ESRI Shapefile',
                    crs=grid.crs.srs,
                    schema=schema_outlet) as c:        
    for index, row in SIEREM.iterrows():
        #clip back again to the whole domain:
        grid.clip_to('dir')  
        x = int(row.iStream)
        y = int(grid.dir.shape[0] - row.jStream)-1

        rec = {}
        point = Point(float(row['xStream']), float(row['yStream']))            
        rec['geometry'] = mapping(point)            
        rec['properties'] = {'Name' : str(row.Name_),'index':str(index),'Latitude':float(row.Latitude),
        'Longitude':float(row.Longitude),'area':float(row['Catchment area (km2)']),
        'Country':str(row.Country),'Bassin':str(row['Bassin']),'River':str(row['River']),
        'xPF':float(row.xPF),'yPF':float(row.yPF),'iPF':float(row.iPF),'jPF':float(row.jPF),
        'onStream':float(row.onStream),'xStream':float(row.xStream),'yStream':float(row.yStream),
        'iStream':float(row.iStream),'jStream':float(row.jStream),'PFarea':float(row.PFarea)}
        rec['id'] = str(i)
        c.write(rec)
        i += 1
        
# save rasters of river network
for index, row in SIEREM.iterrows():
    grid.clip_to(str(index))
    grid.acc2 = grid.view('acc')
    grid.acc2[grid.acc2<100] = np.nan
    grid.acc2[grid.acc2>=100] = 1
    grid.to_raster('acc2', data_loc_dir+'SIEREM/rivers_05_ep001_n/SIEREM_rivers_%s.tif'%index, blockxsize=16, blockysize=16)



print('loop over SIEREM data: done')




