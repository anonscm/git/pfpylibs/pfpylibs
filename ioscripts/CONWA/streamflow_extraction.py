#-*- coding: utf-8 -*-
"""
    
    PFPYLIBS - IOSCRIPTS

    STREAMFLOW EXTRACTION    
    
    Example script which does:
    - finding of PF coordinates of a gauging station, check it's on the 
        PF streamnetwork and similar draining area and flag if station 
        needs be moved
    - From these outlets coordinates, extract PF catchments, outlets as 
        geopandas which can be saved as ESRI shapefiles, and stream 
        network currently only saved as raster tifs
    - Extraction of auxiliary information for each catchment (slopes, 
        Ks, precip, simulation outputs as ET, WTD...)
    - Extraction of simulation outputs time series and early comparison
        with data 
    - multi-dimensional analysis using catchment-level infos gathered as
        pandas DataFrames (Q metrics, Q observed, auxiliary data...)
        
    Each step can be computed independently but require I/O from other steps
    either by keeping data in memory (DataFrame or GeoDataFrame), or by
    reading again the data: most of the time you can choose by commenting
    / uncommenting the proper lines. For instance, step 4: extracting 
    simulation outputs is really long and could be done only once.
        
    Examples are given for AMMA-CATCH databa, ADHI, and SIEREM. The last 2 are
    commented: just uncomment to use them. 
    
    Any other DataFrame can be used as input. It only requires x and y
    columns and optionally catch_area for step 1, or if you want to jump
    to step 2 by providing your own PF coordinates (eg. to extract streamflow)
    at some specific virtual location), only iStream, jStream, xStream, yStream
    columns are required, corresponding to index & coordinates values in the PF grid.
    
    Between Step 1 and Step 2, a quality check may be done on the relevance
    of input x, and y, for instance by opening tifs and .shp outputs from step 2
    into QGIS and overlying aerial images or other products. x and y locations
    can be adapted manually, or specific datapoints be flagged as holding
    doubtful locations
    
    @copyright: 2021 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2021"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import geopandas as gpd
import xarray as xr
import matplotlib.pyplot as plt
import os,datetime
from calendar import month_abbr
import pyproj
from rasterstats import zonal_stats
import rasterio as rio
from rasterio.mask import mask
from shapely.geometry import Point, mapping
import seaborn as sns
#on faro: if python modules are needed: 
#pip3 install rasterstats
#pip3 install pysheds
#~ """local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb
from PFlibs import PFpostproc


plt.close('all')

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
projAEA = pyproj.Proj("+proj=aea +lat_1=16.12 +lat_2=6.58 +lat_0=11.35 +lon_0=-4.3 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs")
projUTM = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')
crs = '+proj=aea +lat_1=16.12 +lat_2=6.58 +lat_0=11.35 +lon_0=-4.3 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs'

#data_dir is where the OUTPUT, formatted streamflow data will be stored:
# (if applicable: this preproc can be done elsewhere)
#~ data_dir = '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/eval/streamflow/data/'
#faro
data_dir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/eval/streamflow/data/'

# domain_setup dir: directory where to save outputs relative to the specific grid:
#~ domain_setup_dir = '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/eval/streamflow/domain_setups/CONWA_AD/'
#faro:
domain_setup_dir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/eval/streamflow/domain_setups/CONWA_AD/'

#data_loc_dir is where the OUTPUT, formatted streamflow data LOCATION will be stored:
# (if applicable: this preproc can be done elsewhere)
data_loc_dir = os.sep.join([domain_setup_dir,'data_loc/'])

# aux_data_dir is where auxiliary data (eg draining area map, streamflow map)
#can be found: it is not specific to a single simulation but can be used for se
aux_data_dir = os.sep.join([domain_setup_dir,'aux_data/'])

#setup_figures_dir is directory where to save the figures relative to the specifc grid (stations locations, pf stream network, etc)
setup_figures_dir = os.sep.join([domain_setup_dir,'figures/'])

# specific simulation dir for adding up simulation outputs:
simulation_name = 'fullyear1'
sim_dir = os.sep.join([domain_setup_dir,'simulations/%s/'%simulation_name])

#simulation_specific figures:
fig_dir = os.sep.join([sim_dir,'figures/'])

#simulation Q netcdfs dir:
nc_rootdir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/post_spinup/results_fullsim1/'

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def extract_discrete_classes_from_tif_using_shp(tif_filename,gds):
    """reads in a tif, crop it using a geopandas with polygons
    then compute histograms and get count values for each class
    returns a panda object with frequencies (%) per class """

    # extract the geometries in GeoJSON format
    geoms = gds.geometry.values # list of shapely geometries
    records = gds.values
    with rio.open(tif_filename) as src:
        classes,counts  = np.unique(src.read(1),return_counts=True)

    with rio.open(tif_filename) as src:
        print('nodata value:', src.nodata)
        idx_area = 0
        for index, row in gds.iterrows():
            upslope_area = row['geometry']
            name = row['Name']
            mapped_geom = [mapping(upslope_area)]
            out_image, out_transform = mask(src, mapped_geom, crop=True)# extract the raster values values within the polygon
            no_data=src.nodata #no data values of the original raster
            #~ data = out_image.data[0] #extract the values of the masked array
            data = out_image #extract the values of the masked array
            clas = np.extract(data != no_data, data) # extract the row, columns of the valid values
            #~ frequencies, class_limits = np.histogram(clas,bins=classes,range=[range_min, range_max])
            frequencies, class_limits = np.histogram(clas,bins=classes)
            if idx_area == 0:
                VEG = pd.DataFrame({str(name): frequencies})
                VEG.index = class_limits[:-1]
            else:
                VEG[str(name)] = frequencies
            idx_area += 1
    
    #classes from int to str:
    VEG = VEG.rename(index={t:str(int(t)) for t in VEG.index})
    # counts -> %
    VEG = 100*VEG/VEG.sum()
    #~ return VEG.set_index(VEG.index.astype(int)).T
    return VEG.T
    

def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    from https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console
    
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(round(length * iteration // total))
    #~ bar = fill * filledLength + '-' * (length - filledLength)
    bar = '=' * filledLength + '>' + '-' * (length - filledLength)
    #~ print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    #~ print('\r%s |%s| %s%% %s'.encode(sys.stdout.encoding, errors='replace')%(prefix,bar,percent,suffix), end = printEnd)
    #~ print('\r%s |%s| %s%% %s'.encode('utf-8')%(prefix,bar,percent,suffix), end = printEnd)
    #~ print('\r%s |%s| %s%% %s'%(prefix,bar,percent,suffix), end = printEnd)
    print('\r%s |%s| %s%% %s'%(prefix,bar,percent,suffix), end = printEnd)
    # Print New Line on Complete
    if iteration == total: 
        print()

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##


""" 
########################################################################
step 1: find the PF coordinates of station data
########################################################################
"""

""" step 1.a load a PF grid """
## option 1: hard code the grid parameters
PFgrid ={}
PFgrid['X0'] = -1425000.0; PFgrid['Y0'] = -779000.0
PFgrid['dx'] = 1000; PFgrid['dy'] = 1000
PFgrid['nx'] = 2880; PFgrid['ny'] = 1600
## option 2: read a pfb: data,PFgrid = iopfb.PFB_read(filename,1)
## option 3: read a tiff: PFgrid, vecx, vecy = PF.get_PF_grid_from_Gtiff(filename)
## option 4: read a netcdf...

""" step 1.b load some x,y data """
AC_Benin = pd.read_csv(os.sep.join([data_dir,'AC_Benin','Oueme_stations.csv']))
AC_Benin.set_index(AC_Benin.columns[0],inplace=True)

ADHI = pd.read_csv('/home/hectorb/DATA/streamflow/ADHI/ADHI/ADHI_stations.tab',sep='\t')
#~ SIEREM = pd.read_csv('/home/hectorb/DATA/streamflow/Afrique_Sierem/SIEREM_stations.csv')

""" step 1.c  find_station_location_on_PF """
# the function < find_station_location_on_PF > from the PFlibs requires the dataset to have x, y and catch_area fields
AC_Benin.loc[:,'x'],AC_Benin.loc[:,'y'] = pyproj.transform(geo_system,projAEA,np.array(AC_Benin.loc[:,'Longitude']), np.array(AC_Benin.loc[:,'Latitude']))
AC_Benin.rename(columns={'Catchment area (km2)':'catch_area'},inplace=True)

ADHI.loc[:,'x'],ADHI.loc[:,'y'] = pyproj.transform(geo_system,projAEA,np.array(ADHI.loc[:,'Longitude']), np.array(ADHI.loc[:,'Latitude']))
ADHI.rename(columns={'Catchment area (km2)':'catch_area'},inplace=True)

#~ SIEREM.loc[:,'x'],SIEREM.loc[:,'y'] = pyproj.transform(geo_system,projAEA,np.array(SIEREM.loc[:,'Longitude']), np.array(SIEREM.loc[:,'Latitude']))
#~ SIEREM.rename(columns={'Superficie':'catch_area'},inplace=True)


drain_file = aux_data_dir+ "CONWA_MERIT.area_05_ep001_n.out.tif"
stream_file = aux_data_dir+"/CONWA_MERIT.segments_1000_05_ep001_200504.out.tif"

AC_Benin = PFpostproc.find_station_location_on_PF(AC_Benin,PFgrid, streamfile=stream_file,plot_streams=False,drainage_file = drain_file, box_size=5,plot_drainage=False)
ADHI = PFpostproc.find_station_location_on_PF(ADHI,PFgrid, streamfile=stream_file,plot_streams=False,drainage_file = drain_file, box_size=5,plot_drainage=False)
#~ SIEREM = PFpostproc.find_station_location_on_PF(SIEREM,PFgrid, streamfile=stream_file,plot_streams=False,drainage_file = drain_file, box_size=5,plot_drainage=False)

AC_Benin.to_csv(os.sep.join([data_loc_dir,'AC_Benin/','AC_Benin_PF_v2.csv']),sep='\t')
ADHI.to_csv(os.sep.join([data_loc_dir,'ADHI/','ADHI_PF_v2.csv']),sep='\t')
#~ SIEREM.to_csv(os.sep.join([data_loc_dir,'SIEREM/','SIEREM_PF_v2.csv']),sep='\t')


""" 
########################################################################
step 2: extract PF catchments, outlet and streams as shp, raster
########################################################################
"""
direction_file =aux_data_dir+"CONWA_MERIT.direction_05_ep001_n.out.tif"

outdir = data_loc_dir+'AC_Benin/rivers_05_ep001_n/'

AC_Benin_catchment,AC_Benin_outlet = PFpostproc.compute_catchments_from_PF_stations(AC_Benin,
    direction_file = direction_file,drainage_file = drain_file,
    crs =crs,dirmap=(3,6,4,7,1,8,2,9),outdir=None, stream_acc_threshold = 100)

ADHI_catchment,ADHI_outlet = PFpostproc.compute_catchments_from_PF_stations(ADHI,
    direction_file = direction_file,drainage_file = drain_file,
    crs =crs,dirmap=(3,6,4,7,1,8,2,9),outdir=None, stream_acc_threshold = 100)

#~ SIEREM_catchment,SIEREM_outlet = PFpostproc.compute_catchments_from_PF_stations(SIEREM,
    #~ direction_file = direction_file,drainage_file = drain_file,
    #~ crs =crs,dirmap=(3,6,4,7,1,8,2,9),outdir=None, stream_acc_threshold = 100)


AC_Benin_catchment.to_file(data_loc_dir+'AC_Benin/catchments_05_ep001_n/AC_catchments_v2.shp')
AC_Benin_outlet.to_file(data_loc_dir+'AC_Benin/outlets_05_ep001_n/AC_outlets_v2.shp')

ADHI_catchment.to_file(data_loc_dir+'ADHI/catchments_05_ep001_n/ADHI_catchments_v2.shp')
ADHI_outlet.to_file(data_loc_dir+'ADHI/outlets_05_ep001_n/ADHI_outlets_v2.shp')

#~ SIEREM_catchment.to_file(data_loc_dir+'SIEREM/catchments_05_ep001_n/SIEREM_catchments_v2.shp')
#~ SIEREM_outlet.to_file(data_loc_dir+'SIEREM/outlets_05_ep001_n/SIEREM_outlets_v2.shp')


""" 
########################################################################
 step 3: extract auxiliary information for each catchment 
 
 this section can be extended at will
########################################################################
"""

""" Add yearly precipitation """
print('compute yearly P per catchment')
#AC_Benin
ACgpd = gpd.read_file(data_loc_dir+'AC_Benin/catchments_05_ep001_n/AC_catchments_v2.shp')
ACgpd = ACgpd.set_index(ACgpd.Name)
tmp=zonal_stats(ACgpd,aux_data_dir+ "ini_Pre_yearly_mm.tif")
ACgpd['P_year'] = np.array([t['mean'] for t in tmp]) 

#ADHI
ADHIgpd = gpd.read_file(data_loc_dir+'ADHI/catchments_05_ep001_n/ADHI_catchments_v2.shp')
ADHIgpd = ADHIgpd.set_index(ADHIgpd.Name)
tmp=zonal_stats(ADHIgpd,aux_data_dir+ "ini_Pre_yearly_mm.tif")
ADHIgpd['P_year'] = np.array([t['mean'] for t in tmp]) 

#SIEREM
#~ SIEREMgpd = gpd.read_file(data_loc_dir+'SIEREM/catchments_05_ep001_n/SIEREM_catchments_v2.shp')
#~ SIEREMgpd = SIEREMgpd.set_index(SIEREMgpd.Name)
#~ tmp=zonal_stats(SIEREMgpd,aux_data_dir+ "ini_Pre_yearly_mm.tif")
#~ SIEREMgpd['P_year'] = np.array([t['mean'] for t in tmp]) 


""" Add vegetation distribution """
print('compute vegetation distribution per catchment')
tif_filename = aux_data_dir+'LCC.tif'
# first create a tif, if needed : uncomment the following
#~ pfb_filename = '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/eval/streamflow/domain_setups/CONWA_AD/aux_data/LCC.pfb'
#~ crs = '+proj=aea +lat_1=16.12 +lat_2=6.58 +lat_0=11.35 +lon_0=-4.3 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs'
#~ iopfb.write_tif_from_pfb(pfb_filename,tif_filename,crs)

#AC_Benin
ACgpd = pd.concat([ACgpd,extract_discrete_classes_from_tif_using_shp(tif_filename,ACgpd)],axis=1)

#ADHI
ADHIgpd = pd.concat([ADHIgpd,extract_discrete_classes_from_tif_using_shp(tif_filename,ADHIgpd)],axis=1)

#SIEREM
#~ SIEREMgpd = pd.concat([SIEREMgpd,extract_discrete_classes_from_tif_using_shp(tif_filename,SIEREMgpd)],axis=1)

""" Add slopes """
print('compute mean slopes per catchment')

tif_filename = aux_data_dir+'CONWA_MERIT_slope_mag_stan_05_ep001_200504.tif'

# first create a tif with slope magnitudes, out of x y slopes: if needed : uncomment the following
#~ pfb_filename_slopex = '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/eval/streamflow/domain_setups/CONWA_AD/aux_data/CONWA_MERIT_slopex_stan_05_ep001_200504.pfb'
#~ pfb_filename_slopey = '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/eval/streamflow/domain_setups/CONWA_AD/aux_data/CONWA_MERIT_slopey_stan_05_ep001_200504.pfb'
#~ crs = '+proj=aea +lat_1=16.12 +lat_2=6.58 +lat_0=11.35 +lon_0=-4.3 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs'
#~ slopex,h = iopfb.PFB_read(pfb_filename_slopex,1) 
#~ slopey,h = iopfb.PFB_read(pfb_filename_slopey,1) 
#~ slopex = slopex[:,:,0]
#~ slopey = slopey[:,:,0]
#~ d = np.sqrt(slopex*slopex+slopey*slopey)   
#~ # TODO : !check this is ok:
#~ transform = Affine.translation(h['X0'],  h['Y0']+(h['ny']-1)*h['dy']) * Affine.scale(h['dx'], -h['dy'])
#~ d2 = d.swapaxes(0,1)
#~ d2 = d2[::-1,:]
#~ with rio.open(tif_filename,'w',driver='GTiff',height=d2.shape[0],width=d2.shape[1],count=1,dtype=d2.dtype,
    #~ crs=crs,transform=transform) as dst:
        #~ dst.write(d2,1)

# then extract slope magnitudes

#AC_Benin
tmp=zonal_stats(ACgpd,tif_filename)
ACgpd['slope'] = np.array([t['mean'] for t in tmp]) 

#ADHI
tmp=zonal_stats(ADHIgpd,tif_filename)
ADHIgpd['slope'] = np.array([t['mean'] for t in tmp]) 

#SIEREM
#~ tmp=zonal_stats(SIEREMgpd,tif_filename)
#~ SIEREMgpd['slope'] = np.array([t['mean'] for t in tmp]) 

""" Add Ksat of the aquifer (cell nz -1 because of hard rock masking) """
print('compute mean Ks per catchment')
tif_filename = aux_data_dir+'Ksat_mask_glhymps_v3_nonan_with_crust_MAD_no_river_crust_WA_dz2.tif'

# first create a tif with slope magnitudes, out of x y slopes: if needed : uncomment the following
#~ pfb_filename = '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/eval/streamflow/domain_setups/CONWA_AD/aux_data/Ksat_mask_glhymps_v3_nonan_with_crust_MAD_no_river_crust_WA_dz2.pfb'
#~ crs = '+proj=aea +lat_1=16.12 +lat_2=6.58 +lat_0=11.35 +lon_0=-4.3 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs'
#~ d,h = iopfb.PFB_read(pfb_filename,1) 
#~ d = d[:,:,-2]
#~ # TODO : check this is ok:
#~ transform = Affine.translation(h['X0'],  h['Y0']+(h['ny']-1)*h['dy']) * Affine.scale(h['dx'], -h['dy'])
#~ d2 = d.swapaxes(0,1)
#~ d2 = d2[::-1,:]
#~ with rio.open(tif_filename,'w',driver='GTiff',height=d2.shape[0],width=d2.shape[1],count=1,dtype=d2.dtype,
    #~ crs=crs,transform=transform) as dst:
        #~ dst.write(d2,1)

# then extract Ks (m/h)

#AC_Benin
tmp=zonal_stats(ACgpd,tif_filename)
ACgpd['Ks'] = np.array([t['mean'] for t in tmp]) 

#ADHI
tmp=zonal_stats(ADHIgpd,tif_filename)
ADHIgpd['Ks'] = np.array([t['mean'] for t in tmp]) 

#SIEREM
#~ tmp=zonal_stats(SIEREMgpd,tif_filename)
#~ SIEREMgpd['Ks'] = np.array([t['mean'] for t in tmp]) 

"""save"""

ACgpd.to_file(data_loc_dir+'AC_Benin/catchments_05_ep001_n/AC_catchments_v2_P_veg_slope_Ks.shp')
ADHIgpd.to_file(data_loc_dir+'ADHI/catchments_05_ep001_n/ADHI_catchments_v2_P_veg_slope_Ks.shp')
#~ SIEREMgpd.to_file(data_loc_dir+'SIEREM/catchments_05_ep001_n/SIEREM_catchments_v2_P_veg_slope_Ks.shp')

"""
Now add up some simulation specific outputs !!! save in a different dir (simulation outputs)
"""

""" Add mean WTD from simulation outputs"""
print('compute mean WTD per catchment')
#AC_Benin
tmp=zonal_stats(ACgpd,sim_dir+ "WTD_y0_full_Ymean.tif")
ACgpd['WTD'] = np.array([t['mean'] for t in tmp]) 

#ADHI
tmp=zonal_stats(ADHIgpd,sim_dir+ "WTD_y0_full_Ymean.tif")
ADHIgpd['WTD'] = np.array([t['mean'] for t in tmp]) 

#SIEREM
#~ tmp=zonal_stats(SIEREMgpd,sim_dir+ "WTD_y0_full_Ymean.tif")
#~ SIEREMgpd['WTD'] = np.array([t['mean'] for t in tmp]) 

""" Add yearly ET from simulation outputs"""
print('compute yearly ET per catchment')
#AC_Benin
tmp=zonal_stats(ACgpd,sim_dir+ "evaptranssum_y0_full_Ysum_corrP.tif")
ACgpd['ET'] = np.array([t['mean'] for t in tmp]) 

#ADHI
tmp=zonal_stats(ADHIgpd,sim_dir+ "evaptranssum_y0_full_Ysum_corrP.tif")
ADHIgpd['ET'] = np.array([t['mean'] for t in tmp]) 

#SIEREM
#~ tmp=zonal_stats(SIEREMgpd,sim_dir+ "evaptranssum_y0_full_Ysum_corrP.tif")
#~ SIEREMgpd['ET'] = np.array([t['mean'] for t in tmp]) 



"""save"""

ACgpd.to_file(sim_dir+'/catchments/AC_catchments_v2_P_veg_slope_Ks_%s_WTD_ET.shp'%simulation_name)
ADHIgpd.to_file(sim_dir+'/catchments/ADHI_catchments_v2_P_veg_slope_Ks_%s_WTD_ET.shp'%simulation_name)
#~ SIEREMgpd.to_file(sim_dir+'/catchments/SIEREM_catchments_v2_P_veg_slope_Ks_%s_WTD_ET.shp'%simulation_name)

""" 
########################################################################
step 4: extract simulation outputs and compare with data 
########################################################################
"""

#read in stations (or keep in memory from above)
#~ AC_Benin = pd.read_csv(data_loc_dir+'/AC_Benin/AC_Benin_PF_v2.csv',sep='\t')
#~ AC_Benin.set_index(AC_Benin.columns[0],inplace=True)

#~ ADHI = pd.read_csv(data_loc_dir+'/ADHI/ADHI_PF_v2.csv',sep='\t')
#~ ADHI.set_index(ADHI.Id,inplace=True)

#~ SIEREM = pd.read_csv(data_loc_dir+'/SIEREM/SIEREM_PF_v2.csv',sep='\t')
#~ SIEREM.set_index('Id',inplace=True)


"""Load Simulation output: """
#read in a netcdf
AC_PF=pd.DataFrame()
ADHI_PF=pd.DataFrame()
#~ SIEREM_PF=pd.DataFrame()

for i in np.arange(12)+1:
    nc_file = os.sep.join([nc_rootdir,'Q_%d_nc4.nc'%i])
    print('read file: %s'%nc_file)
    d = xr.open_dataset(nc_file)
    print('read file: ok')    

    print('extract Q for AC stations')        
    AC_PFtmp=pd.DataFrame()    
    for index, row in AC_Benin.iterrows():
        AC_PFtmp = pd.concat([AC_PFtmp,d.sel(x=row.xStream,y=row.yStream).streamflow.to_series().rename(index)],axis=1)
    AC_PF = pd.concat([AC_PF,AC_PFtmp],axis=0)        
    print('extract Q for AC stations: ok')  

    print('extract Q for ADHI stations')
    # Initial call to print 0% progress
    printProgressBar(0, len(ADHI), prefix = 'Progress:', suffix = 'Complete', length = 20)   
    progress=0
    ADHI_PFtmp=pd.DataFrame()
    for index, row in ADHI.iterrows():
        printProgressBar(progress + 1, len(ADHI), prefix = 'Progress:', suffix = 'Complete', length = 20)        
        progress+=1
        ADHI_PFtmp = pd.concat([ADHI_PFtmp,d.sel(x=row.xStream,y=row.yStream).streamflow.to_series().rename(index)],axis=1)
    ADHI_PF = pd.concat([ADHI_PF,ADHI_PFtmp],axis=0)
    print('extract Q for ADHI stations: ok')

    #~ print('extract Q for SIEREM stations')        
    #~ SIEREM_PFtmp=pd.DataFrame()    
    #~ for index, row in SIEREM.iterrows():
        #~ SIEREM_PFtmp = pd.concat([SIEREM_PFtmp,d.sel(x=row.xStream,y=row.yStream).streamflow.to_series().rename(index)],axis=1)
    #~ SIEREM_PF = pd.concat([SIEREM_PF,SIEREM_PFtmp],axis=0)        
    #~ print('extract Q for SIEREM stations: ok')  


    
AC_PF.to_csv(sim_dir+'/simQ/PF_Q_AC_v2.csv')
ADHI_PF.to_csv(sim_dir+'/simQ/PF_Q_ADHI_v2.csv')
#~ SIEREM_PF.to_csv(sim_dir+'/simQ/PF_Q_SIEREM_v2.csv')


""" 
########################################################################
step 5: do some multi-dimensional analysis 
########################################################################
"""

"""read in stations (or keep in memory from above)"""
# AMMA CATCH Benin
#~ ACgpd = gpd.read_file(sim_dir+'/catchments/AC_catchments_v2_P_veg_slope_Ks_%s_WTD_ET.shp'%simulation_name)
#~ ACgpd = ACgpd.set_index(ACgpd.Name)

# ADHI
#~ ADHIgpd = gpd.read_file(sim_dir+'/catchments/ADHI_catchments_v2_P_veg_slope_Ks_%s_WTD_ET.shp'%simulation_name)
#~ ADHIgpd = ADHIgpd.set_index(ADHIgpd['index'])
#~ shortname={'Benin':'Be', 'Burkina Faso':'BF', "Cote d'Ivoire":'CI', 'Ghana':'Gh', 'Guinea':'Gu',
       #~ 'Guinea-Bissau':'GB', 'Liberia':'Li', 'Mali':'Ma', 'Niger':'Ni', 'Nigeria':'Nia', 'Senegal':'Se',
       #~ 'Togo':'To'}
#~ ADHIgpd['Country_short']=ADHIgpd.Country.apply(lambda x: shortname[x]) 

# SIEREM
#~ SIEREMgpd = gpd.read_file(sim_dir+'/catchments/SIEREM_catchments_v2_P_veg_slope_Ks_%s_WTD_ET.shp'%simulation_name)
#~ SIEREMgpd = SIEREMgpd.set_index(SIEREMgpd.Name)

""" Load streamflow data (time series)""" 

# AMMA CATCH Benin
data_AC = pd.read_csv(data_dir+'/AC_Benin/Oueme_Q.csv')
data_AC.set_index(data_AC.columns[0],inplace=True)
data_AC = data_AC.set_index(pd.to_datetime(data_AC.index)).rename_axis('date')
data_AC.sort_index(inplace=True)

# ADHI
data_ADHI = pd.read_csv(data_dir+'/ADHI/ADHI_summary.tab',sep='\t')
data_ADHI=data_ADHI.loc[data_ADHI.Id.isin(ADHIgpd.index),:]
data_ADHI.reset_index(inplace=True)
data_ADHI = data_ADHI.set_index('Id')

data_ADHI2 = data_ADHI.iloc[:,5:17].T
data_ADHI2.index=[pd.datetime(2015,i,15) for i in np.arange(1,13,1)] 
data_ADHI_sort = data_ADHI.sort_values(by='Mean_daily_streamflow',ascending=False)

# SIEREM
#~ data_SIEREM = pd.read_csv(data_dir+'/SIEREM/SIEREM_data.csv')
#~ data_SIEREM.set_index(data_SIEREM.columns[0],inplace=True)
#~ data_SIEREM = data_SIEREM.set_index(pd.to_datetime(data_SIEREM.index)).rename_axis('date')
#~ data_SIEREM_month = data_SIEREM.groupby(data_SIEREM.index.month).mean()  
#~ data_SIEREM_month.index=[pd.datetime(2015,i,15) for i in np.arange(1,13,1)] 

""" Load simulation streamflow data""" 
# AMMA CATCH Benin
#~ AC_PF = pd.read_csv(sim_dir+'/simQ/PF_Q_AC.csv')
AC_PF = pd.read_csv(sim_dir+'/simQ/PF_Q_AC_v2.csv')
AC_PF.set_index(AC_PF.columns[0],inplace=True)
AC_PF = AC_PF.set_index(pd.to_datetime(AC_PF.index)).rename_axis('date')

# ADHI
ADHI_PF = pd.read_csv(sim_dir+'/simQ/PF_Q_ADHI_v2.csv')
ADHI_PF.set_index(ADHI_PF.columns[0],inplace=True)
ADHI_PF = ADHI_PF.set_index(pd.to_datetime(ADHI_PF.index)).rename_axis('date')

# SIEREM
#~ SIEREM_PF = pd.read_csv(sim_dir+'/simQ/PF_Q_SIEREM_v2.csv')
#~ SIEREM_PF.set_index(SIEREM_PF.columns[0],inplace=True)
#~ SIEREM_PF = SIEREM_PF.set_index(pd.to_datetime(SIEREM_PF.index)).rename_axis('date')


"""compute basic runoff stats from data"""
#mean Q (m3/s)
#over 2015:
ACgpd['Qmeanobs'] = data_AC.loc['2015-01-01':'2015-12-31 23:00',:].resample('Y').mean().iloc[0,:]
# climatology
ADHIgpd['Qmeanobs'] = data_ADHI.Mean_daily_streamflow

# yearly Q (m3)
#over 2015:
ACgpd['Q_Yobs'] = (data_AC*3600).loc['2015-01-01':'2015-12-31 23:00',:].resample('Y').sum().iloc[0,:] 
# climatology
ADHIgpd['Q_Yobs'] = ADHIgpd['Qmeanobs']*3600*24*365

# yearly Q (mm)
#over 2015:
ACgpd['Q_Ymmobs'] = ACgpd['Q_Yobs'] / (ACgpd['catch_area']*1000*1000) * 1000
# climatology
ADHIgpd['Q_Ymmobs'] = ADHIgpd['Q_Yobs'] / (ADHIgpd['catch_area']*1000*1000) * 1000


"""Quick QAQC"""
ADHIgpd = ADHIgpd.loc[ADHIgpd.Q_Ymmobs<3000,:]                                                                                                                                                                                     

""" remove the largest basins """
ADHIgpd = ADHIgpd.loc[ADHIgpd['area']<200000,:]      


"""compute basic runoff stats from obs"""
#mean Q (m3/s)
ACgpd['Qmeansim'] = AC_PF.loc['2015-01-01':'2015-12-31 23:00',:].resample('Y').mean().iloc[0,:]
ADHIgpd['Qmeansim'] = ADHI_PF.loc['2015-01-01':'2015-12-31 23:00',:].resample('Y').mean().iloc[0,:]
#~ SIEREMgpd['Qmeansim'] = SIEREM_PF.loc['2015-01-01':'2015-12-31 23:00',:].resample('Y').mean().iloc[0,:]

# yearly Q (m3)
ACgpd['Q_Ysim'] = (AC_PF*3600*3).loc['2015-01-01':'2015-12-31 23:00',:].resample('Y').sum().iloc[0,:] 
ADHIgpd['Q_Ysim'] = (ADHI_PF*3600*3).loc['2015-01-01':'2015-12-31 23:00',:].resample('Y').sum().iloc[0,:] 
#~ SIEREMgpd['Q_Ysim'] = (SIEREM_PF*3600*3).loc['2015-01-01':'2015-12-31 23:00',:].resample('Y').sum().iloc[0,:] 

# yearly Q (mm)
ACgpd['Q_Ymmsim'] = ACgpd['Q_Ysim'] / (ACgpd.PFarea*1000*1000) * 1000
ADHIgpd['Q_Ymmsim'] = ADHIgpd['Q_Ysim'] / (ADHIgpd.PFarea*1000*1000) * 1000
#~ SIEREMgpd['Q_Ymmsim'] = SIEREMgpd['Q_Ysim'] / (SIEREMgpd.PFarea*1000*1000) * 1000

#runoff coefficient
ACgpd['Krsim'] = ACgpd['Q_Ymmsim'] / ACgpd.P_year  
ADHIgpd['Krsim'] = ADHIgpd['Q_Ymmsim'] / ADHIgpd.P_year  
#~ SIEREMgpd['Krsim'] = SIEREMgpd['Q_Ymmsim'] / SIEREMgpd.P_year  

""" Plot some figures:"""

""" Figure: plot all climatologies and data: AMMA CATCH Beni"""
AC_Benin_sorted = AC_Benin.sort_values(by='catch_area')

fig, ax = plt.subplots(ncols = 3, nrows = 4,sharex = True,figsize=(14,9))
for i,a in enumerate(ax.flatten()[0:len(AC_Benin)]):
    #sort by catchment size:
    p1 = a.plot(data_AC.loc[:,AC_Benin_sorted.index[i]],'k')
    p2 = a.plot(AC_PF.loc[:,AC_Benin_sorted.index[i]],'r')    
    
    a.text(0.05,0.9,AC_Benin_sorted.loc[AC_Benin_sorted.index[i],'Name']+' %d $km^2$'%AC_Benin_sorted.loc[AC_Benin_sorted.index[i],'catch_area'],verticalalignment = 'top',transform = a.transAxes,FontSize=10)
    a.legend(handles=p1+p2,labels = ('obs','sim'),ncol=1,loc='upper right',framealpha=0.5,fontsize=12)
    if i!=0:
        a.get_legend().remove()
for i,a in enumerate(ax.flatten()):
    a.set_xticks([datetime.datetime(2015,7,1),datetime.datetime(2015,8,1),datetime.datetime(2015,9,1),datetime.datetime(2015,10,1)])
    a.set_xticklabels([month_abbr[datetime.datetime(2015,7,1).month],month_abbr[datetime.datetime(2015,8,1).month],month_abbr[datetime.datetime(2015,9,1).month],month_abbr[datetime.datetime(2015,10,1).month]],rotation = 45, ha="right",fontsize=12)
    a.set_xlim([datetime.datetime(2015,6,1),datetime.datetime(2015,11,1)])
    a.set_ylabel('Q ($m^3.s^{-1}$)')
#plt.subplots_adjust(hspace = 0,wspace = 0)
plt.savefig(fig_dir+'AC_Benin_streamflow.png')


""" Figure: plot all climatologies and data:  ADHI"""
fig, ax = plt.subplots(ncols = 21, nrows = 21,sharex = True,figsize=(16,10))
for i,a in enumerate(ax.flatten()[0:len(ADHIgpd)]):
    p1 = a.plot(data_ADHI2.iloc[:,i],'k')
    #p2 = a.plot(ADHI_PF.iloc[:,i],'r')
    p2 = a.plot(ADHI_PF.iloc[:,i].resample('M').mean(),'r')
    a.text(0.1,0.8,ADHIgpd.loc[ADHIgpd.index[i],'Name'][0:8]+'\n%s'%ADHIgpd.loc[ADHIgpd.index[i],'Country_short'],verticalalignment = 'top',transform = a.transAxes,FontSize=5)
    a.legend(handles=p1+p2,labels = ('obs','sim'),ncol=1,loc='upper right',framealpha=0.5,fontsize=5)
    if i!=0:
        a.get_legend().remove()
for i,a in enumerate(ax.flatten()):
    a.set_xticks([datetime.datetime(2015,1,1),datetime.datetime(2015,4,1),datetime.datetime(2015,7,1),datetime.datetime(2015,10,1)])
    a.set_xticklabels([month_abbr[datetime.datetime(2015,1,1).month],month_abbr[datetime.datetime(2015,4,1).month],month_abbr[datetime.datetime(2015,7,1).month],month_abbr[datetime.datetime(2015,10,1).month]],rotation = 45, ha="right",fontsize=7)
    a.set_yticks([])
    a.set_ylabel('rel. Q')
    a.label_outer()
plt.subplots_adjust(hspace = 0,wspace = 0)

#~ """ Figure: plot all climatologies and data: SIEREM"""
#~ fig, ax = plt.subplots(ncols = 10, nrows = 11,sharex = True,figsize=(16,10))
#~ for i,a in enumerate(ax.flatten()[0:len(SIEREM)]):
    #~ p1 = a.plot(data_SIEREM_id_month.iloc[:,i],'k')
    #~ p2 = a.plot(SIEREM_PF.iloc[:,i],'r')
    #~ a.text(0.1,0.8,SIEREM.loc[SIEREM.index[i],'Name'][0:8]+'\n%s'%SIEREM.loc[SIEREM.index[i],'Country_short'],verticalalignment = 'top',transform = a.transAxes,FontSize=5)
    #~ a.legend(handles=p1+p2,labels = ('obs','sim'),ncol=1,loc='upper right',framealpha=0.5,fontsize=5)
    #~ if i!=0:
        #~ a.get_legend().remove()
#~ for i,a in enumerate(ax.flatten()):
    #~ a.set_xticks([datetime.datetime(2015,1,1),datetime.datetime(2015,4,1),datetime.datetime(2015,7,1),datetime.datetime(2015,10,1)])
    #~ a.set_xticklabels([month_abbr[datetime.datetime(2015,1,1).month],month_abbr[datetime.datetime(2015,4,1).month],month_abbr[datetime.datetime(2015,7,1).month],month_abbr[datetime.datetime(2015,10,1).month]],rotation = 45, ha="right",fontsize=7)
    #~ a.set_yticks([])
    #~ a.set_ylabel('rel. Q')
    #~ a.label_outer()
#~ plt.subplots_adjust(hspace = 0,wspace = 0)
#~ plt.savefig(fig_dir+'SIEREM_streamflow.png')


#~ """ Figure: plot mean daily flow: ADHI """
#~ fig, ax = plt.subplots(1,1)
#~ data_ADHI_sort.loc[:,'Mean_daily_streamflow'].plot(kind='bar',log=True,ax=ax)
#~ ADHI_PF.mean().loc[data_ADHI_sort.index].plot(style ='r+',logy=True,ax=ax)
#~ ax.set_ylabel('log(Q) ($m^3.s^{-1}$)')
#~ plt.savefig(fig_dir+'ADHI_mean_daily_streamflow2.png')

#~ """ Figure: plot mean daily flow: SIEREM """
#~ data_sort.loc[:,'Mean_daily_streamflow'].plot(kind='bar',yerr=data_sort.Standard_deviation_daily_streamflow)
#~ fig, ax = plt.subplots(1,1)
#~ data_ADHI_sort.loc[:,'Mean_daily_streamflow'].plot(kind='bar',log=True,ax=ax)
#~ ADHI_PF.mean().loc[data_ADHI_sort.index].plot(style ='r',logy=True,ax=ax)
#~ ax.set_ylabel('log(Q) ($m^3.s^{-1}$)')

""" AMMA CATCH data check"""
AC = ACgpd.loc[:,['catch_area', 'PFarea','P_year', '7', '8',  '12', 'slope',
        'Ks','WTD', 'ET','Qmeanobs','Q_Ymmobs','Qmeansim', 'Q_Ysim', 'Q_Ymmsim', 'Krsim']]

g = sns.pairplot(data=AC)
g.fig.set_size_inches(30,30)
plt.savefig(fig_dir+'AC_Benin_pairplot.png')

ADHI = ADHIgpd.loc[:,['area', 'PFarea','P_year', '7', '8',  '12', 'slope',
        'Ks','WTD', 'ET','Qmeanobs','Q_Ymmobs','Qmeansim', 'Q_Ysim', 'Q_Ymmsim', 'Krsim']]

g2 = sns.pairplot(data=ADHI)
g2.fig.set_size_inches(30,30)
plt.savefig(fig_dir+'ADHI_Benin_pairplot.png')

ADHI2 = ADHIgpd.loc[:,['PFarea','P_year', '7', 'slope','Ks','WTD', 'ET','Qmeanobs','Qmeansim']]


g3 = sns.pairplot(data=ADHI2)
g3.fig.set_size_inches(30,30)
plt.savefig(fig_dir+'ADHI_Benin_pairplot2.png')
