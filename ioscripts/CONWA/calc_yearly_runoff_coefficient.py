#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    Take P and ET simulated by PF, and calculate P-ET which in this case should be ~Kr the runoff coeffient
    (assuming the model is spun up)

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import xarray as xr

"""local imports:"""


plt.close('all')
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
""" Read in PF forcing """
P = xr.open_dataset('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/ini_Pre_yearly_mm.nc')

""" Read in PF simulation output """
ET = xr.open_dataset('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/evaptranssum_y12_vcorr2_monthly.nc')
ET.evaptranssum.attrs['units'] = 'mm/d'
ET_yr = ET.mean(dim='month')*365.

Kr = xr.Dataset({"Kr": (("y", "x"), (P.Pre.values - ET_yr.evaptranssum.values)/P.Pre.values)},
    coords={"x":P.x.values,"y":P.y.values})

""" Plot Kr at different resolution"""

fig,ax = plt.subplots(2,2,figsize=(14,8.2),squeeze=True)
### Original PF:
Kr.Kr.plot(ax=ax[0,0],vmin=0,vmax=0.5)
ax[0,0].set_title('PF 1km')
### 10 km
Kr.coarsen(x=10).mean().coarsen(y=10).mean().Kr.plot(ax=ax[0,1],vmin=0,vmax=0.5)
ax[0,1].set_title('PF 10km')
### 25 km:
Kr.coarsen(x=20).mean().coarsen(y=20).mean().Kr.plot(ax=ax[1,0],vmin=0,vmax=0.5)
ax[1,0].set_title('PF 20km')
### 50 km:
Kr.coarsen(x=40).mean().coarsen(y=40).mean().Kr.plot(ax=ax[1,1],vmin=0,vmax=0.5)
ax[1,1].set_title('PF 40km')

for a in ax.flat: a.set_aspect(1)
plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/Kr_pf_y12.png')

