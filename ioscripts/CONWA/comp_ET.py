#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    exemple script to compare regional simulated ET & observed
    using gleam product v3.3b

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
import datetime,os
from calendar import monthrange
import copy
"""local imports:"""


plt.close('all')
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
#~ gleam_dir = '/home/hectorb/DATA/Flux/GLEAM/'
# domain_setup dir: directory where to save outputs relative to the specific grid:
# faro:
domain_setup_dir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/eval/ET/domain_setups/CONWA_AD/'

#data_dir : where the streamflow observation data can be found:
#faro
data_dir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/eval/ET/data/'

# specific simulation dir for adding up simulation outputs:
simulation_name = 'fullyear1'
sim_dir = os.sep.join([domain_setup_dir,'simulations/%s/'%simulation_name])

#simulation_specific figures:
fig_dir = os.sep.join([sim_dir,'figures/'])


resolution = 1
#~ resolution = 20
#~ resolution = 40
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

""" Read in ET data"""
#~ gleamdata = xr.open_dataset(gleam_dir+'E_2003_2018_GLEAM_v3.3b_MO.nc')
gleamdata = xr.open_dataset(data_dir+'E_2003_2018_GLEAM_v3.3b_MO.nc')

#~ Egleam = gleamdata.sel(lon=slice(-20,10), lat = slice(20,3)).transpose()
#~ Egleam = gleamdata.sel(lon=slice(-20,10), lat = slice(20,3),time = slice("2015-1-1","2015-12-31")).transpose()
Egleam = gleamdata.sel(lon=slice(-20,12), lat = slice(19,3),time = slice(datetime.datetime(2015,1,1),datetime.datetime(2015,12,31))).transpose()

# no idea why we need to create a new dataarray first
# http://xarray.pydata.org/en/stable/indexing.html#vectorized-indexing
Egleam['et_daily'] = xr.full_like(Egleam.E, fill_value=0)
#mm/month -> mm/day
for i in range(12):
    #isel fails silently: http://xarray.pydata.org/en/stable/indexing.html
    #~ Egleam.isel(time=i).E.values = Egleam.isel(time=i).E.values/monthrange(2015,i+1)[1]
    # so instead:
    Egleam.et_daily[dict(time=i)] = Egleam.E[dict(time=i)]/monthrange(2015,i+1)[1]

Egleam.et_daily.attrs['units'] = 'mm/d'


#~ Egleam.sel(lon=0,lat=10,method='nearest').E.groupby('time.year').sum().plot()
#~ Egleam.sel(lon=0,lat=10,method='nearest').E

""" Read in PF simulation output """
#~ ET = xr.open_dataset('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/evaptranssum_y14_vcorr2_monthly.nc')
ET = xr.open_dataset(sim_dir+'/evaptranssum_y0_full_vcorr2_monthly.nc')
ET.evaptranssum.attrs['units'] = 'mm/d'

#~ resolution = 40
if resolution == 20:
    ### PF 20km:
    ET = ET.coarsen(x=20).mean().coarsen(y=20).mean()
elif resolution == 40:
    ### PF 40km:
    ET = ET.coarsen(x=40).mean().coarsen(y=40).mean()

fig,ax = plt.subplots(4,2,figsize=(14,16),squeeze=True)
plot_args = {'add_labels':False,'vmin':0,'vmax':5,'cbar_kwargs':{'label':'ET (mm/d)'}}
######### PF:
## JFM
#~ ET.sel(month = slice(1,3)).mean(dim='month').evaptranssum.plot(ax=ax[0,0],add_labels=False,vmin=0,vmax=5)
ET.sel(month = slice(1,3)).mean(dim='month').evaptranssum.plot(ax=ax[0,0],**plot_args)
ax[0,0].set_title('PF: JFM')
## AMJ
ET.sel(month = slice(4,6)).mean(dim='month').evaptranssum.plot(ax=ax[1,0],**plot_args)
ax[1,0].set_title('PF: AMJ')
## JAS
ET.sel(month = slice(7,9)).mean(dim='month').evaptranssum.plot(ax=ax[2,0],**plot_args)
ax[2,0].set_title('PF: JAS')
## OND
ET.sel(month = slice(10,12)).mean(dim='month').evaptranssum.plot(ax=ax[3,0],**plot_args)
ax[3,0].set_title('PF: OND')

### GLEAM data
## JFM
Egleam.isel(time=slice(0,3)).mean(dim='time').et_daily.plot(ax=ax[0,1],**plot_args)
ax[0,1].set_title('GLEAM: JFM')
ax[0,1].set_xlim([-17.4,8.8])
ax[0,1].set_ylim([3.8,18.3])
## AMJ
Egleam.isel(time=slice(4,6)).mean(dim='time').et_daily.plot(ax=ax[1,1],**plot_args)
ax[1,1].set_title('GLEAM: AMJ')
ax[1,1].set_xlim([-17.4,8.8])
ax[1,1].set_ylim([3.8,18.3])
## JAS
Egleam.isel(time=slice(7,9)).mean(dim='time').et_daily.plot(ax=ax[2,1],**plot_args)
ax[2,1].set_title('GLEAM: JAS')
ax[2,1].set_xlim([-17.4,8.8])
ax[2,1].set_ylim([3.8,18.3])
## OND
Egleam.isel(time=slice(10,12)).mean(dim='time').et_daily.plot(ax=ax[3,1],**plot_args)
ax[3,1].set_title('GLEAM: OND')
ax[3,1].set_xlim([-17.4,8.8])
ax[3,1].set_ylim([3.8,18.3])
for a in ax.flat: a.set_aspect(1); a.set_xticklabels(''); a.set_yticklabels('')

if resolution == 1:
    ### PF 1km:
    #~ plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/ET_pf_y14_1km_vs_gleam.png')
    plt.savefig(fig_dir+'ET_pf_y0_full_1km_vs_gleam.png')
elif resolution == 20:
    ### PF 20km:
    #~ plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/ET_pf_y14_20km_vs_gleam.png')
    plt.savefig(fig_dir+'ET_pf_y0_full_20km_vs_gleam.png')
elif resolution == 40:
    ### PF 40km:
    #~ plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/ET_pf_y14_40km_vs_gleam.png')
    plt.savefig(fig_dir+'ET_pf_y0_full_40km_vs_gleam.png')


### PF 320km:
#~ S_amp_PF.coarsen(x=320).mean().coarsen(y=320).mean().storage_amplitude.plot(ax=ax[1,1],vmin=0,vmax=650)

""" Year sum """
fig,ax = plt.subplots(1,2,figsize=(14,5.2),squeeze=True)
#~ plot_args = {'add_labels':False,'vmin':0,'vmax':1400,'cbar_kwargs':{'label':'ET (mm)'}}
plot_args = {'add_labels':False,'add_colorbar':False,'vmin':0,'vmax':1400}
######### PF:
p1 = (ET.mean(dim='month').evaptranssum*365.).plot(ax=ax[0],**plot_args)
ax[0].set_title('PF')
### GLEAM data
p2 = (Egleam.mean(dim='time').et_daily*365.).plot(ax=ax[1],**plot_args)
ax[1].set_title('GLEAM')
ax[1].set_xlim([-17.4,8.8])
ax[1].set_ylim([3.8,18.3])
for a in ax.flat: a.set_aspect(1); a.set_xticklabels(''); a.set_yticklabels('')

cbar = fig.colorbar(p1, ax=ax.ravel().tolist(),orientation='horizontal')
cbar.set_label('ET (mm)')

if resolution == 1:
    ### PF 1km:
    #~ plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/ET_year_pf_y14_1km_vs_gleam.png')
    plt.savefig(fig_dir+'ET_year_pf_y0_full_1km_vs_gleam.png')    
elif resolution == 20:
    ### PF 20km:
    #~ plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/ET_year_pf_y14_20km_vs_gleam.png')
    plt.savefig(fig_dir+'ET_year_pf_y0_full_20km_vs_gleam.png')    
elif resolution == 40:
    ### PF 40km:
    #~ plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/ET_year_pf_y14_40km_vs_gleam.png')
    plt.savefig(fig_dir+'ET_year_pf_y0_full_40km_vs_gleam.png')
