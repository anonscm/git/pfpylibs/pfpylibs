#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    Exercice script to read in all pfbs of pressure, calculate WTD
    (from total domain thickness - last cell /2 and pressure value of last cell)
    and make a netcdf out of this for postprocessing in the Supercomputers

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy
import os,glob,struct
import xarray as xr
import pandas as pd
"""local imports:"""

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
datadir = '/home/hectorb/PARFLOW/PROJECTS/Ara/resolution2020/inputs/ara100/ara100'
#~ datadir = '/home/hectorb/PARFLOW/PROJECTS/Ara/resolution2020/inputs/ara250/ara250'
#~ datadir = '/home/hectorb/PARFLOW/PROJECTS/Ara/resolution2020/inputs/ara500/ara500'
n_coupled_layers = 13
v = numpy.array([0.08,0.32,0.08,0.08,0.32,0.64,0.88,0.32,0.32,0.64,1.2,1.8,3.6,5.4,8.32])
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def PFB_read(pfb_file_path, flag):

    #print("**This code DOES handle subgrids**")
    
    hdr_dict = {}
    
    with open(pfb_file_path,'rb') as pfbf:
        hdr_dict['X0'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['Y0'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['Z0'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['nx'] = struct.unpack('>i', pfbf.read(4))[0]
        hdr_dict['ny'] = struct.unpack('>i', pfbf.read(4))[0]
        hdr_dict['nz'] = struct.unpack('>i', pfbf.read(4))[0]
        hdr_dict['dx'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['dy'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['dz'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['ns'] = struct.unpack('>i', pfbf.read(4))[0]
        ns = hdr_dict['ns']
        #print(ns)
        nx = hdr_dict['nx']
        ny = hdr_dict['ny']
        nz = hdr_dict['nz']
        
        if nx == 0:
            nx = int(raw_input("What should the NX be?"))
            hdr_dict['nx'] = nx
        if ny == 0:
            ny = int(raw_input("What should the NY be?"))
            hdr_dict['ny'] = ny
        if nz ==0:
            nz =int(raw_input("What should the NZ be?"))
            hdr_dict['nz'] = nz
        
        X0 = hdr_dict['X0']
        #print "len_x, len_y, len_z, nx ny nz --> %f %f %f %i %i %i" %(len_x, len_x, len_x, nx, ny, nz)
        
        pfb_array = numpy.zeros((nx, ny, nz))
        
        for subgrids in range(ns):
            hdr_dict['ix'] = struct.unpack('>i', pfbf.read(4))[0]
            ix = hdr_dict['ix']
            hdr_dict['iy'] = struct.unpack('>i', pfbf.read(4))[0]
            iy = hdr_dict['iy']
            #print(iy)
            hdr_dict['iz'] = struct.unpack('>i', pfbf.read(4))[0]
            iz = hdr_dict['iz']
            hdr_dict['nx2'] = struct.unpack('>i', pfbf.read(4))[0]
            nx2 = hdr_dict['nx2']
            hdr_dict['ny2'] = struct.unpack('>i', pfbf.read(4))[0]
            ny2 = hdr_dict['ny2']
            #print(ny2)
            hdr_dict['nz2'] = struct.unpack('>i', pfbf.read(4))[0]
            nz2 = hdr_dict['nz2']
            hdr_dict['rx'] = struct.unpack('>i', pfbf.read(4))[0]
            rx = hdr_dict['rx']
            hdr_dict['ry'] = struct.unpack('>i', pfbf.read(4))[0]
            ry = hdr_dict['ry']
            hdr_dict['rz'] = struct.unpack('>i', pfbf.read(4))[0]
            rz = hdr_dict['rz']
            #print "ix iy iz nx2 ny2 nz2 rx ry rz --> %i %i %i %i %i %i %i %i %i\n" %(ix,iy, iz, nx2, ny2, nz2, rx,ry, rz)
            for lays in range(nz2):
                for rows in range(ny2):
                    for cols in range(nx2):
                        val = struct.unpack('>d', pfbf.read(8))[0]
                        #q = raw_input(val)
                        pfb_array[(cols+ix), (rows + iy), (nz2-(lays + iz)-1)] = val
                        #pfb_array[(cols+ix-1), (nx2-(iy)-1), (nz2-(lays + iz)-1)] = val
                        #pfb_array[(rows + iy-1), (cols+ix-1), (nz2-(lays + iz)-1)] = val
                        #print val
                        
    if flag ==1:
        return(pfb_array, hdr_dict)
    else:
        return(pfb_array)

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
"""identify evaptranssum files"""
files = glob.glob(os.path.join(datadir,'*press.*.pfb'))
files.sort()
#skip initial condition
files.pop(0)

"""reads in the first file to have the dimension & initialize"""
d,h=PFB_read(files[0],1)
dz = h['dz']
vardz = v/dz 
tot_thickness = numpy.sum(v)-v[-1]/2

final_array = numpy.zeros([h['nx'],h['ny'],len(files)])

"""reads in all files"""
for t,f in enumerate(files):
    print(t)
    d,h=PFB_read(f,1)
    d[d<0]=numpy.nan
    final_array[:,:,t] = tot_thickness - d[:,:,-1]


"""write netcdf"""
#~ ds = xr.Dataset({"evaptranssum": (("x", "y","time"), final_array)},
#~ ds = xr.Dataset({"evaptranssum": (("time","x", "y"), numpy.moveaxis(final_array,2,0))},
ds = xr.Dataset({"WTD": (("time","y", "x"), numpy.swapaxes(final_array,2,0))},
    coords={"x":numpy.arange(start = h['X0'],stop = h['X0']+h['nx']*h['dx'],step=h['dx']),
        "y":numpy.arange(start = h['Y0'],stop = h['Y0']+h['ny']*h['dy'],step=h['dy']), 
        "time": pd.date_range("2015-01-01", periods=len(files)),
        "reference_time": pd.Timestamp("2015-01-01")})

ds.WTD.attrs['units']='mm'
ds.x.attrs['units']='meters'
ds.y.attrs['units']='meters'

ds.to_netcdf(os.path.join(datadir,"WTD.nc"),format="NETCDF4")
