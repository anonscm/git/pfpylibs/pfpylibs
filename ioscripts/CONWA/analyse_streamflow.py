#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Scripts to start analysing streamflow outputs where data exists

    @copyright: 2021 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2021"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import xarray as xr
import os,datetime
from calendar import month_abbr
import geopandas as gpd
import seaborn as sns

"""local imports:"""

plt.close('all')
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
# domain_setup dir: directory where to save outputs relative to the specific grid:
domain_setup_dir = '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/eval/streamflow/domain_setups/CONWA_AD/'

#data_dir : where the streamflow observation data can be found:
#faro
#data_dir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/eval/streamflow/data/'
#Basile
data_dir = '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/eval/streamflow/data/'

#data_loc_dir is where the OUTPUT, formatted streamflow data LOCATION is:
# (if applicable: this preproc can be done elsewhere)
data_loc_dir = os.sep.join([domain_setup_dir,'data_loc/'])

# specific simulation dir for adding up simulation outputs:
simulation_name = 'fullyear2'
sim_dir = os.sep.join([domain_setup_dir,'simulations/%s/'%simulation_name])

#simulation_specific figures:
fig_dir = os.sep.join([sim_dir,'figures/'])
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

""" Load catchment data"""
# AMMA CATCH Benin
ACgpd = gpd.read_file(sim_dir+'/catchments/AC_catchments_v2_P_veg_slope_Ks_%s_WTD_ET.shp'%simulation_name)
ACgpd = ACgpd.set_index(ACgpd.Name)

# ADHI
ADHIgpd = gpd.read_file(sim_dir+'/catchments/ADHI_catchments_v2_P_veg_slope_Ks_%s_WTD_ET.shp'%simulation_name)
#~ ADHIgpd = ADHIgpd.set_index(ADHIgpd.Name)
ADHIgpd = ADHIgpd.set_index(ADHIgpd['index'])
shortname={'Benin':'Be', 'Burkina Faso':'BF', "Cote d'Ivoire":'CI', 'Ghana':'Gh', 'Guinea':'Gu',
       'Guinea-Bissau':'GB', 'Liberia':'Li', 'Mali':'Ma', 'Niger':'Ni', 'Nigeria':'Nia', 'Senegal':'Se',
       'Togo':'To'}
ADHIgpd['Country_short']=ADHIgpd.Country.apply(lambda x: shortname[x]) 

# SIEREM
SIEREMgpd = gpd.read_file(sim_dir+'/catchments/SIEREM_catchments_P_veg_slope_Ks_%s_WTD_ET.shp'%simulation_name)
SIEREMgpd = SIEREMgpd.set_index(SIEREMgpd.Name)

""" Load streamflow data""" 

# AMMA CATCH Benin
data_AC = pd.read_csv(data_dir+'/AC_Benin/Oueme_Q.csv')
data_AC.set_index(data_AC.columns[0],inplace=True)
data_AC = data_AC.set_index(pd.to_datetime(data_AC.index)).rename_axis('date')
data_AC.sort_index(inplace=True)

# ADHI
data_ADHI = pd.read_csv(data_dir+'/ADHI/ADHI_summary.tab',sep='\t')
data_ADHI=data_ADHI.loc[data_ADHI.Id.isin(ADHIgpd.index),:]
data_ADHI.reset_index(inplace=True)
data_ADHI = data_ADHI.set_index('Id')

data_ADHI2 = data_ADHI.iloc[:,5:17].T
data_ADHI2.index=[pd.datetime(2015,i,15) for i in np.arange(1,13,1)] 
data_ADHI_sort = data_ADHI.sort_values(by='Mean_daily_streamflow',ascending=False)

# SIEREM
data_SIEREM = pd.read_csv(data_dir+'/SIEREM/SIEREM_data.csv')
data_SIEREM_id = pd.read_csv(data_dir+'/SIEREM/SIEREM_data_id.csv')

data_SIEREM.set_index(data_SIEREM.columns[0],inplace=True)
data_SIEREM = data_SIEREM.set_index(pd.to_datetime(data_SIEREM.index)).rename_axis('date')

data_SIEREM_id.set_index(data_SIEREM_id.columns[0],inplace=True)    
data_SIEREM_id = data_SIEREM_id.set_index(pd.to_datetime(data_SIEREM_id.index)).rename_axis('date')


data_SIEREM_id_month = data_SIEREM_id.groupby(data_SIEREM_id.index.month).mean()  
data_SIEREM_id_month.index=[pd.datetime(2015,i,15) for i in np.arange(1,13,1)] 
data_SIEREM_month = data_SIEREM.groupby(data_SIEREM.index.month).mean()  
data_SIEREM_month.index=[pd.datetime(2015,i,15) for i in np.arange(1,13,1)] 

""" Load simulation streamflow data""" 
# AMMA CATCH Benin
AC_PF = pd.read_csv(sim_dir+'/simQ/PF_Q_AC.csv')
AC_PF.set_index(AC_PF.columns[0],inplace=True)
AC_PF = AC_PF.set_index(pd.to_datetime(AC_PF.index)).rename_axis('date')

# ADHI
ADHI_PF = pd.read_csv(sim_dir+'/simQ/PF_Q_ADHI.csv')
ADHI_PF.set_index(ADHI_PF.columns[0],inplace=True)
ADHI_PF = ADHI_PF.set_index(pd.to_datetime(ADHI_PF.index)).rename_axis('date')

# SIEREM
SIEREM_PF = pd.read_csv(sim_dir+'/simQ/PF_Q_SIEREM.csv')
SIEREM_PF.set_index(SIEREM_PF.columns[0],inplace=True)
SIEREM_PF = SIEREM_PF.set_index(pd.to_datetime(SIEREM_PF.index)).rename_axis('date')


"""compute basic runoff stats from data"""
#mean Q (m3/s)
#over 2015:
ACgpd['Qmeanobs'] = data_AC.loc['2015-01-01':'2015-12-31 23:00',:].resample('Y').mean().iloc[0,:]
# climatology
ADHIgpd['Qmeanobs'] = data_ADHI.Mean_daily_streamflow

# yearly Q (m3)
#over 2015:
ACgpd['Q_Yobs'] = (data_AC*3600).loc['2015-01-01':'2015-12-31 23:00',:].resample('Y').sum().iloc[0,:] 
# climatology
ADHIgpd['Q_Yobs'] = ADHIgpd['Qmeanobs']*3600*24*365

# yearly Q (mm)
#over 2015:
ACgpd['Q_Ymmobs'] = ACgpd['Q_Yobs'] / (ACgpd['area']*1000*1000) * 1000
# climatology
ADHIgpd['Q_Ymmobs'] = ADHIgpd['Q_Yobs'] / (ADHIgpd['area']*1000*1000) * 1000


"""Quick QAQC"""
ADHIgpd = ADHIgpd.loc[ADHIgpd.Q_Ymmobs<3000,:]                                                                                                                                                                                     

""" remove the largest basins """
ADHIgpd = ADHIgpd.loc[ADHIgpd['area']<200000,:]      


"""compute basic runoff stats from obs"""
#mean Q (m3/s)
ACgpd['Qmeansim'] = AC_PF.loc['2015-01-01':'2015-12-31 23:00',:].resample('Y').mean().iloc[0,:]
ADHIgpd['Qmeansim'] = ADHI_PF.loc['2015-01-01':'2015-12-31 23:00',:].resample('Y').mean().iloc[0,:]
SIEREMgpd['Qmeansim'] = SIEREM_PF.loc['2015-01-01':'2015-12-31 23:00',:].resample('Y').mean().iloc[0,:]

# yearly Q (m3)
ACgpd['Q_Ysim'] = (AC_PF*3600*3).loc['2015-01-01':'2015-12-31 23:00',:].resample('Y').sum().iloc[0,:] 
ADHIgpd['Q_Ysim'] = (ADHI_PF*3600*3).loc['2015-01-01':'2015-12-31 23:00',:].resample('Y').sum().iloc[0,:] 
SIEREMgpd['Q_Ysim'] = (SIEREM_PF*3600*3).loc['2015-01-01':'2015-12-31 23:00',:].resample('Y').sum().iloc[0,:] 

# yearly Q (mm)
ACgpd['Q_Ymmsim'] = ACgpd['Q_Ysim'] / (ACgpd.PFarea*1000*1000) * 1000
ADHIgpd['Q_Ymmsim'] = ADHIgpd['Q_Ysim'] / (ADHIgpd.PFarea*1000*1000) * 1000
SIEREMgpd['Q_Ymmsim'] = SIEREMgpd['Q_Ysim'] / (SIEREMgpd.PFarea*1000*1000) * 1000

#runoff coefficient
ACgpd['Krsim'] = ACgpd['Q_Ymmsim'] / ACgpd.P_year  
ADHIgpd['Krsim'] = ADHIgpd['Q_Ymmsim'] / ADHIgpd.P_year  
SIEREMgpd['Krsim'] = SIEREMgpd['Q_Ymmsim'] / SIEREMgpd.P_year  

""" Plot some figures:"""

#~ """ Figure: plot all climatologies and data"""
#~ fig, ax = plt.subplots(ncols = 21, nrows = 21,sharex = True,figsize=(16,10))
#~ for i,a in enumerate(ax.flatten()[0:len(ADHIgpd)]):
    #~ p1 = a.plot(data_ADHI2.iloc[:,i],'k')
    #~ #p2 = a.plot(ADHI_PF.iloc[:,i],'r')
    #~ p2 = a.plot(ADHI_PF.iloc[:,i].resample('M').mean(),'r')
    #~ a.text(0.1,0.8,ADHIgpd.loc[ADHIgpd.index[i],'Name'][0:8]+'\n%s'%ADHIgpd.loc[ADHIgpd.index[i],'Country_short'],verticalalignment = 'top',transform = a.transAxes,FontSize=5)
    #~ a.legend(handles=p1+p2,labels = ('obs','sim'),ncol=1,loc='upper right',framealpha=0.5,fontsize=5)
    #~ if i!=0:
        #~ a.get_legend().remove()
#~ for i,a in enumerate(ax.flatten()):
    #~ a.set_xticks([datetime.datetime(2015,1,1),datetime.datetime(2015,4,1),datetime.datetime(2015,7,1),datetime.datetime(2015,10,1)])
    #~ a.set_xticklabels([month_abbr[datetime.datetime(2015,1,1).month],month_abbr[datetime.datetime(2015,4,1).month],month_abbr[datetime.datetime(2015,7,1).month],month_abbr[datetime.datetime(2015,10,1).month]],rotation = 45, ha="right",fontsize=7)
    #~ a.set_yticks([])
    #~ a.set_ylabel('rel. Q')
    #~ a.label_outer()
#~ plt.subplots_adjust(hspace = 0,wspace = 0)

#~ """ Figure: plot mean daily flow """
#~ fig, ax = plt.subplots(1,1)
#~ data_ADHI_sort.loc[:,'Mean_daily_streamflow'].plot(kind='bar',log=True,ax=ax)
#~ ADHI_PF.mean().loc[data_ADHI_sort.index].plot(style ='r+',logy=True,ax=ax)
#~ ax.set_ylabel('log(Q) ($m^3.s^{-1}$)')
#~ plt.savefig(fig_dir+'ADHI_mean_daily_streamflow2.png')


""" AMMA CATCH data check"""


AC = ACgpd.loc[:,['area', 'PFarea','P_year', '7', '8',  '12', 'slope',
        'Ks','WTD', 'ET','Qmeanobs','Q_Ymmobs','Qmeansim', 'Q_Ysim', 'Q_Ymmsim', 'Krsim']]

g = sns.pairplot(data=AC)
g.fig.set_size_inches(30,30)
plt.savefig(fig_dir+'AC_Benin_pairplot.png')

ADHI = ADHIgpd.loc[:,['area', 'PFarea','P_year', '7', '8',  '12', 'slope',
        'Ks','WTD', 'ET','Qmeanobs','Q_Ymmobs','Qmeansim', 'Q_Ysim', 'Q_Ymmsim', 'Krsim']]

g2 = sns.pairplot(data=ADHI)
g2.fig.set_size_inches(30,30)
plt.savefig(fig_dir+'ADHI_Benin_pairplot.png')

ADHI2 = ADHIgpd.loc[:,['PFarea','P_year', '7', 'slope','Ks','WTD', 'ET','Qmeanobs','Qmeansim']]


g3 = sns.pairplot(data=ADHI2)
g3.fig.set_size_inches(30,30)
plt.savefig(fig_dir+'ADHI_Benin_pairplot2.png')
