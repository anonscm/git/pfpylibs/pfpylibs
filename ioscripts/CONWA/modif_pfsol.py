#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Modif Pfsol file
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import pandas as pd
import time
import os,glob
import matplotlib.pyplot as plt
import datetime
#Added for handling NC (to be moved to another library)
#~ import kolinkelly as kk
#~ import joestork as js
from netCDF4 import Dataset

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from procsycz import readDataAMMA as rdA

plt.close("all")

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

with open('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/preproc/CONWA_1km_AD.pfsol.txt', "r") as f:
    with open('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/preproc/CONWA_1km_AD.pfsol2.txt', "w") as fw:
        reads = f.readlines()
        for i,r in enumerate(reads):
            if (i>=2) & (i<=(19711)):
                curr_line=np.array([float(x) for x in r.strip().split()])
                fw.writelines('%7d %7d %7d\n'%(curr_line[0],curr_line[1],curr_line[2]))
            else:
                fw.writelines(r)

# replace tab to space
#~ :set et|retab
