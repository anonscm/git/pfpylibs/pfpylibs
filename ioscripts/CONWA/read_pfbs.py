#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Anaylse PFBs for CONWA : exemple script of different commands in io_pfb 
    

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
savedir = '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/pfb_files/'

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

"""CONWA"""
################################# MAPS
# Ks
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Ksat_mask_glhymps_v3_nonan_with_crust_MAD_no_river_crust.pfb' 
for layer in range(11): 
    print(layer) 
    #~ d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,logscale=True,colorlabel='LOG Ks (m/h)',title='Ks Z = %d'%layer,savefig=True,savedir=savedir,savename='Ksat_v3_nonan_with_crust_MAD_no_river_crust_lay%d.png'%layer,width=6.7,height=3.75) 
    d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,logscale=True,colorlabel='LOG Ks (m/h)',title='Ks Z = %d'%layer,savefig=True,savedir=savedir,savename='Ksat_mask_glhymps_v3_nonan_with_crust_MAD_no_river_crust_lay%d.png'%layer,width=6.7,height=3.75) 
    
#Wsat
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Wsat_masked_glhymps_v3_nonan_MAD.pfb' 
for layer in range(11): 
    print(layer) 
    d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,logscale=False,colorlabel='Poro',title='Poro Z = %d'%layer,savefig=True,savedir=savedir,savename='Wsat_masked_glhymps_v3_nonan_MAD_lay%d.png'%layer,width=6.7,height=3.75) 
    
# n
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/n_v3_nonan_min1.32_MAD.pfb'  
for layer in range(11): 
    print(layer) 
    d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,logscale=False,colorlabel='n',title='n Z = %d'%layer,savefig=True,savedir=savedir,savename='n_v3_nonan_MAD_lay%d.png'%layer,width=6.7,height=3.75) 
    
# Alpha
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/alpha_v3_nonan.pfb' 
for layer in range(11): 
    print(layer) 
    d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,logscale=False,colorlabel='alpha (m-1)',title='alpha Z = %d'%layer,savefig=True,savedir=savedir,savename='alpha_v3_nonan_lay%d.png'%layer,width=6.7,height=3.75) 
    
#Sr
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Sr_v3_nonan_MAD.pfb' 
for layer in range(11): 
    print(layer) 
    d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,logscale=False,colorlabel='Sr',title='Sr Z = %d'%layer,savefig=True,savedir=savedir,savename='Sr_v3_nonan_MAD_lay%d.png'%layer,width=6.7,height=3.75) 

#Ssat 
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Ssat_v3_nonan.pfb' 
for layer in range(11): 
    print(layer) 
    d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,logscale=False,colorlabel='Ssat',title='Ssat Z = %d'%layer,savefig=True,savedir=savedir,savename='Ssat_v3_nonan_lay%d.png'%layer,width=6.7,height=3.75) 

#slopes
layer =0
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/CONWA_MERIT_slopey_stan_05_ep001.pfb' 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,logscale=False,colorlabel='slope',title='slope Y',savefig=True,savedir=savedir,savename='CONWA_MERIT_slopey_stan_05_ep001.png',width=6.7,height=3.75) 
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/CONWA_MERIT_slopex_stan_05_ep001.pfb' 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,logscale=False,colorlabel='slope',title='slope X',savefig=True,savedir=savedir,savename='CONWA_MERIT_slopex_stan_05_ep001.png',width=6.7,height=3.75) 

#LCC
layer=0 
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/LCC.pfb' 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,logscale=False,colorlabel='LC classes',title='Land Cover',savefig=True,savedir=savedir,savename='LCC.png',width=6.7,height=3.75,cmap='YlGn_r') 

#PminusET
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/PminusET_FINAL_WA_div3_m_2h.pfb' 
layer=0 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,logscale=False,colorlabel='P - ET',title='P - ET',savefig=True,savedir=savedir,savename='PminusET_FINAL_WA_div3_m_2h.png',width=6.7,height=3.75,cmap='viridis_r')  
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/PminusET_FINAL_WA_div3_m_30min.pfb' 
layer=0 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,logscale=False,colorlabel='P - ET',title='P - ET',savefig=True,savedir=savedir,savename='PminusET_FINAL_WA_div3_m_30min.png',width=6.7,height=3.75,cmap='viridis_r') 
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/PminusET_FINAL_WA_div3_m_5j.pfb' 
layer=0 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,logscale=False,colorlabel='P - ET',title='P - ET',savefig=True,savedir=savedir,savename='PminusET_FINAL_WA_div3_m_5j.png',width=6.7,height=3.75,cmap='viridis_r')  
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/PminusET_FINAL_WA_div3_m_year.pfb' 
layer=0 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,logscale=False,colorlabel='P - ET',title='P - ET',savefig=True,savedir=savedir,savename='PminusET_FINAL_WA_div3_m_year.png',width=6.7,height=3.75,cmap='viridis_r') 

#GLDAS
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/GLDAS_PminusET_min0_m_2h.pfb' 
layer=0 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,logscale=False,colorlabel='P - ET',title='P - ET',savefig=True,savedir=savedir,savename='GLDAS_PminusET_min0_m_2h.png',width=6.7,height=3.75,cmap='viridis_r')  
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/GLDAS_PminusET_min0_m_5days.pfb' 
layer=0 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,logscale=False,colorlabel='P - ET',title='P - ET',savefig=True,savedir=savedir,savename='GLDAS_PminusET_min0_m_5days.png',width=6.7,height=3.75,cmap='viridis_r')  
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/GLDAS_PminusET_min0_m_1yr.pfb' 
layer=0 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,logscale=False,colorlabel='P - ET',title='P - ET',savefig=True,savedir=savedir,savename='GLDAS_PminusET_min0_m_1yr.png',width=6.7,height=3.75,cmap='viridis_r') 


#################################### Boxplots ####################### 
# Ks
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Ksat_mask_glhymps_v3_nonan_with_crust_MAD_no_river_crust.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,multiplier=1/3600,logscale=True,ylabel='Ks (m/s)',xlabel='layer',savefig=True,savedir=savedir,savename='Ksat_masked_glhymps_v3_nonan_with_crust_MAD_no_river_crust_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.05,linestyle=None)) 

#Wsat
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Wsat_masked_glhymps_v3_nonan_MAD.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,logscale=False,ylabel='Poro',xlabel='layer',savefig=True,savedir=savedir,savename='Wsat_masked_glhymps_v3_nonan_MAD_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.05,linestyle=None)) 

# n
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/n_v3_nonan_min1.32_MAD.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,logscale=False,ylabel='n',xlabel='layer',savefig=True,savedir=savedir,savename='n_v3_nonan_min1.32_MAD_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.5,linestyle=None)) 
 

# Alpha
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/alpha_v3_nonan.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,logscale=False,ylabel='Alpha',xlabel='layer',savefig=True,savedir=savedir,savename='Alpha_v3_nonan_MAD_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.05,linestyle=None)) 

#Sr
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Sr_v3_nonan_MAD.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,logscale=False,ylabel='Sr',xlabel='layer',savefig=True,savedir=savedir,savename='Sr_v3_nonan_MAD_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.05,linestyle=None)) 

#Ssat 
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Ssat_v3_nonan.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,logscale=False,ylabel='Ssat',xlabel='layer',savefig=True,savedir=savedir,savename='Ssat_v3_nonan_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.05,linestyle=None)) 

#################################### Histograms ####################### 
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/CONWA_MERIT_slopey_stan_05_ep001.pfb' 
layer=0
d,fig,ax=iopfb.plot_hist_PFB(filename,zlayer=layer,bins=100,logscale=False,title='slope Y',xlabel='slope',savefig=True,savedir=savedir,savename='dist_CONWA_MERIT_slopey_stan_05_ep001.png',width=4,height=4) 

"""DARGOL"""
#################################### MAPS ####################### 
# Ks
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Ksat_mask_glhymps_v3_nonan_with_crust_MAD_no_river_crust.pfb' 
for layer in range(11): 
    print(layer) 
    d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[1902,2048,1031,1116],logscale=True,colorlabel='LOG Ks (m/h)',title='Ks Z = %d'%layer,savefig=True,savedir=savedir,savename='Ksat_v3_nonan_with_crust_MAD_no_river_crust__Dargol_lay%d.png'%layer,width=6.7,height=3.75) 
    
#Wsat
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Wsat_masked_glhymps_v3_nonan_MAD.pfb' 
for layer in range(11): 
    print(layer) 
    d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[1902,2048,1031,1116],logscale=False,colorlabel='Poro',title='Poro Z = %d'%layer,savefig=True,savedir=savedir,savename='Wsat_masked_glhymps_v3_nonan_MAD__Dargol_lay%d.png'%layer,width=6.7,height=3.75) 
    
# n
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/n_v3_nonan_min1.32_MAD.pfb'  
for layer in range(11): 
    print(layer) 
    d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[1902,2048,1031,1116],logscale=False,colorlabel='n',title='n Z = %d'%layer,savefig=True,savedir=savedir,savename='n_v3_nonan_MAD__Dargol_lay%d.png'%layer,width=6.7,height=3.75) 
    
# Alpha
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/alpha_v3_nonan.pfb' 
for layer in range(11): 
    print(layer) 
    d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[1902,2048,1031,1116],logscale=False,colorlabel='alpha (m-1)',title='alpha Z = %d'%layer,savefig=True,savedir=savedir,savename='alpha_v3_nonan__Dargol_lay%d.png'%layer,width=6.7,height=3.75) 
    
#Sr
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Sr_v3_nonan_MAD.pfb' 
for layer in range(11): 
    print(layer) 
    d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[1902,2048,1031,1116],logscale=False,colorlabel='Sr',title='Sr Z = %d'%layer,savefig=True,savedir=savedir,savename='Sr_v3_nonan_MAD__Dargol_lay%d.png'%layer,width=6.7,height=3.75) 

#Ssat 
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Ssat_v3_nonan.pfb' 
for layer in range(11): 
    print(layer) 
    d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[1902,2048,1031,1116],logscale=False,colorlabel='Ssat',title='Ssat Z = %d'%layer,savefig=True,savedir=savedir,savename='Ssat_v3_nonan__Dargol_lay%d.png'%layer,width=6.7,height=3.75) 

#slopes
layer =0
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/CONWA_MERIT_slopey_stan_05_ep001.pfb' 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[1902,2048,1031,1116],logscale=False,colorlabel='slope',title='slope Y',savefig=True,savedir=savedir,savename='CONWA_MERIT_slopey_stan_05_ep001_Dargol.png',width=6.7,height=3.75) 
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/CONWA_MERIT_slopex_stan_05_ep001.pfb' 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[1902,2048,1031,1116],logscale=False,colorlabel='slope',title='slope X',savefig=True,savedir=savedir,savename='CONWA_MERIT_slopex_stan_05_ep001_Dargol.png',width=6.7,height=3.75) 

#LCC
layer=0 
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/LCC.pfb' 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[1902,2048,1031,1116],logscale=False,colorlabel='LC classes',title='Land Cover',savefig=True,savedir=savedir,savename='LCC_Dargol.png',width=6.7,height=3.75,cmap='YlGn_r') 

#PminusET
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/PminusET_FINAL_WA_div3_m_2h.pfb' 
layer=0 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[1902,2048,1031,1116],logscale=False,colorlabel='P - ET',title='P - ET',savefig=True,savedir=savedir,savename='PminusET_FINAL_WA_div3_m_2h_Dargol.png',width=6.7,height=3.75,cmap='viridis_r') 
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/PminusET_FINAL_WA_div3_m_30min.pfb' 
layer=0 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[1902,2048,1031,1116],logscale=False,colorlabel='P - ET',title='P - ET',savefig=True,savedir=savedir,savename='PminusET_FINAL_WA_div3_m_30min_Dargol.png',width=6.7,height=3.75,cmap='viridis_r') 
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/PminusET_FINAL_WA_div3_m_5j.pfb' 
layer=0 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[1902,2048,1031,1116],logscale=False,colorlabel='P - ET',title='P - ET',savefig=True,savedir=savedir,savename='PminusET_FINAL_WA_div3_m_5j_Dargol.png',width=6.7,height=3.75,cmap='viridis_r') 
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/PminusET_FINAL_WA_div3_m_year.pfb' 
layer=0 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[1902,2048,1031,1116],logscale=False,colorlabel='P - ET',title='P - ET',savefig=True,savedir=savedir,savename='PminusET_FINAL_WA_div3_m_year_Dargol.png',width=6.7,height=3.75,cmap='viridis_r') 

#################################### Boxplots ####################### 

# Ks
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Ksat_mask_glhymps_v3_nonan_with_crust_MAD_no_river_crust.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,multiplier=1/3600,logscale=True,crop2D=[1902,2048,1031,1116],ylabel='Ks (m/s)',xlabel='layer',savefig=True,savedir=savedir,savename='Ksat_masked_glhymps_v3_nonan_with_crust_MAD_no_river_crust_Dargol_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.05,linestyle=None)) 

#Wsat
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Wsat_masked_glhymps_v3_nonan_MAD.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,logscale=False,crop2D=[1902,2048,1031,1116],ylabel='Poro',xlabel='layer',savefig=True,savedir=savedir,savename='Wsat_masked_glhymps_v3_nonan_MAD_Dargol_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.05,linestyle=None)) 

# n
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/n_v3_nonan_min1.32_MAD.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,logscale=False,crop2D=[1902,2048,1031,1116],ylabel='n',xlabel='layer',savefig=True,savedir=savedir,savename='n_v3_nonan_min1.32_MAD_Dargol_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.5,linestyle=None)) 
 

# Alpha
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/alpha_v3_nonan.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,logscale=False,crop2D=[1902,2048,1031,1116],ylabel='Alpha',xlabel='layer',savefig=True,savedir=savedir,savename='Alpha_v3_nonan_MAD_Dargol_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.05,linestyle=None)) 

#Sr
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Sr_v3_nonan_MAD.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,logscale=False,crop2D=[1902,2048,1031,1116],ylabel='Sr',xlabel='layer',savefig=True,savedir=savedir,savename='Sr_v3_nonan_MAD_Dargol_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.05,linestyle=None)) 

#Ssat 
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Ssat_v3_nonan.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,logscale=False,crop2D=[1902,2048,1031,1116],ylabel='Ssat',xlabel='layer',savefig=True,savedir=savedir,savename='Ssat_v3_nonan_Dargol_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.05,linestyle=None)) 

#slopes
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/CONWA_MERIT_slopey_stan_05_ep001.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,logscale=False,crop2D=[1902,2048,1031,1116],ylabel='slope',xlabel='layer',savefig=True,savedir=savedir,savename='CONWA_MERIT_slopey_stan_05_ep001_Dargol_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.05,linestyle=None)) 

filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/CONWA_MERIT_slopex_stan_05_ep001.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,logscale=False,crop2D=[1902,2048,1031,1116],ylabel='slope',xlabel='layer',savefig=True,savedir=savedir,savename='CONWA_MERIT_slopex_stan_05_ep001_Dargol_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.05,linestyle=None)) 

#LCC
layer=0 
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/LCC.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,logscale=False,crop2D=[1902,2048,1031,1116],ylabel='LC classes',xlabel='layer',savefig=True,savedir=savedir,savename='LCC_Dargol_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.05,linestyle=None)) 


"""Oueme"""
#################################### MAPS ####################### 
# Ks
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Ksat_mask_glhymps_v3_nonan_with_crust_MAD_no_river_crust.pfb' 
for layer in range(11): 
    print(layer) 
    d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[2050,2200,514,673],logscale=True,colorlabel='LOG Ks (m/h)',title='Ks Z = %d'%layer,savefig=True,savedir=savedir,savename='Ksat_v3_nonan_with_crust_MAD_no_river_crust__Oueme_lay%d.png'%layer,width=6.7,height=3.75)  
    
#Wsat
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Wsat_masked_glhymps_v3_nonan_MAD.pfb' 
for layer in range(11): 
    print(layer) 
    d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[2050,2200,514,673],logscale=False,colorlabel='Poro',title='Poro Z = %d'%layer,savefig=True,savedir=savedir,savename='Wsat_masked_glhymps_v3_nonan_MAD__Oueme_lay%d.png'%layer,width=6.7,height=3.75) 
    
# n
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/n_v3_nonan_min1.32_MAD.pfb'  
for layer in range(11): 
    print(layer) 
    d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[2050,2200,514,673],logscale=False,colorlabel='n',title='n Z = %d'%layer,savefig=True,savedir=savedir,savename='n_v3_nonan_MAD__Oueme_lay%d.png'%layer,width=6.7,height=3.75) 
    
# Alpha
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/alpha_v3_nonan.pfb' 
for layer in range(11): 
    print(layer) 
    d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[2050,2200,514,673],logscale=False,colorlabel='alpha (m-1)',title='alpha Z = %d'%layer,savefig=True,savedir=savedir,savename='alpha_v3_nonan__Oueme_lay%d.png'%layer,width=6.7,height=3.75) 
    
#Sr
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Sr_v3_nonan_MAD.pfb' 
for layer in range(11): 
    print(layer) 
    d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[2050,2200,514,673],logscale=False,colorlabel='Sr',title='Sr Z = %d'%layer,savefig=True,savedir=savedir,savename='Sr_v3_nonan_MAD__Oueme_lay%d.png'%layer,width=6.7,height=3.75) 

#Ssat 
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Ssat_v3_nonan.pfb' 
for layer in range(11): 
    print(layer) 
    d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[2050,2200,514,673],logscale=False,colorlabel='Ssat',title='Ssat Z = %d'%layer,savefig=True,savedir=savedir,savename='Ssat_v3_nonan__Oueme_lay%d.png'%layer,width=6.7,height=3.75) 

#slopes
layer =0
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/CONWA_MERIT_slopey_stan_05_ep001.pfb' 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[2050,2200,514,673],logscale=False,colorlabel='slope',title='slope Y',savefig=True,savedir=savedir,savename='CONWA_MERIT_slopey_stan_05_ep001_Oueme.png',width=6.7,height=3.75) 
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/CONWA_MERIT_slopex_stan_05_ep001.pfb' 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[2050,2200,514,673],logscale=False,colorlabel='slope',title='slope X',savefig=True,savedir=savedir,savename='CONWA_MERIT_slopex_stan_05_ep001_Oueme.png',width=6.7,height=3.75) 

#LCC
layer=0 
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/LCC.pfb' 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[2050,2200,514,673],logscale=False,colorlabel='LC classes',title='Land Cover',savefig=True,savedir=savedir,savename='LCC_Oueme.png',width=6.7,height=3.75,cmap='YlGn_r') 

#LCC
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/LCC.pfb' 
layer=0 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[2050,2200,514,673],logscale=False,colorlabel='LC classes',title='Land Cover',savefig=True,savedir=savedir,savename='LCC_zoom_Oueme.png',width=6.7,height=3.75,cmap='YlGn_r') 

#PminusET
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/PminusET_WA_div3_2h.pfb' 
layer=0 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[2050,2200,514,673],logscale=False,colorlabel='P - ET',title='P - ET',savefig=True,savedir=savedir,savename='PminusET_WA_div3_2h_Oueme.png',width=6.7,height=3.75) 

#################################### Boxplots ####################### 

# Ks
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Ksat_mask_glhymps_v3_nonan_with_crust_MAD_no_river_crust.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,multiplier=1/3600,logscale=True,crop2D=[2050,2200,514,673],ylabel='Ks (m/s)',xlabel='layer',savefig=True,savedir=savedir,savename='Ksat_masked_glhymps_v3_nonan_with_crust_MAD_no_river_crust_Oueme_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.05,linestyle=None)) 

#Wsat
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Wsat_masked_glhymps_v3_nonan_MAD.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,logscale=False,crop2D=[2050,2200,514,673],ylabel='Poro',xlabel='layer',savefig=True,savedir=savedir,savename='Wsat_masked_glhymps_v3_nonan_MAD_Oueme_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.05,linestyle=None)) 

# n
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/n_v3_nonan_min1.32_MAD.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,logscale=False,crop2D=[2050,2200,514,673],ylabel='n',xlabel='layer',savefig=True,savedir=savedir,savename='n_v3_nonan_min1.32_MAD_Oueme_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.5,linestyle=None)) 
 

# Alpha
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/alpha_v3_nonan.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,logscale=False,crop2D=[2050,2200,514,673],ylabel='Alpha',xlabel='layer',savefig=True,savedir=savedir,savename='Alpha_v3_nonan_MAD_Oueme_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.05,linestyle=None)) 

#Sr
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Sr_v3_nonan_MAD.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,logscale=False,crop2D=[2050,2200,514,673],ylabel='Sr',xlabel='layer',savefig=True,savedir=savedir,savename='Sr_v3_nonan_MAD_Oueme_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.05,linestyle=None)) 

#Ssat 
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/Ssat_v3_nonan.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,logscale=False,crop2D=[2050,2200,514,673],ylabel='Ssat',xlabel='layer',savefig=True,savedir=savedir,savename='Ssat_v3_nonan_Oueme_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.05,linestyle=None)) 

#slopes
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/CONWA_MERIT_slopey_stan_05_ep001.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,logscale=False,crop2D=[2050,2200,514,673],ylabel='slope',xlabel='layer',savefig=True,savedir=savedir,savename='CONWA_MERIT_slopey_stan_05_ep001_Oueme_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.05,linestyle=None)) 

filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/CONWA_MERIT_slopex_stan_05_ep001.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,logscale=False,crop2D=[2050,2200,514,673],ylabel='slope',xlabel='layer',savefig=True,savedir=savedir,savename='CONWA_MERIT_slopex_stan_05_ep001_Oueme_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.05,linestyle=None)) 

#LCC
layer=0 
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/LCC.pfb' 
d,fig,ax=iopfb.plot_boxplots_PFB(filename,logscale=False,crop2D=[2050,2200,514,673],ylabel='LC classes',xlabel='layer',savefig=True,savedir=savedir,savename='LCC_Oueme_boxplot.png',width=6.7,height=3.75,whis=(1,99),showmeans=True,flierprops=dict(markersize=0.05,linestyle=None)) 

#PminusET
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/PminusET_FINAL_WA_div3_m_2h.pfb' 
layer=0 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[2050,2200,514,673],logscale=False,colorlabel='P - ET',title='P - ET',savefig=True,savedir=savedir,savename='PminusET_FINAL_WA_div3_m_2h_Oueme.png',width=6.7,height=3.75,cmap='viridis_r')  
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/PminusET_FINAL_WA_div3_m_30min.pfb' 
layer=0 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[2050,2200,514,673],logscale=False,colorlabel='P - ET',title='P - ET',savefig=True,savedir=savedir,savename='PminusET_FINAL_WA_div3_m_30min_Oueme.png',width=6.7,height=3.75,cmap='viridis_r')  
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/PminusET_FINAL_WA_div3_m_5j.pfb' 
layer=0 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[2050,2200,514,673],logscale=False,colorlabel='P - ET',title='P - ET',savefig=True,savedir=savedir,savename='PminusET_FINAL_WA_div3_m_5j_Oueme.png',width=6.7,height=3.75,cmap='viridis_r')  
filename='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/CONWA/pfb_files/PminusET_FINAL_WA_div3_m_year.pfb' 
layer=0 
d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,crop2D=[2050,2200,514,673],logscale=False,colorlabel='P - ET',title='P - ET',savefig=True,savedir=savedir,savename='PminusET_FINAL_WA_div3_m_year_Oueme.png',width=6.7,height=3.75,cmap='viridis_r')  
