#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Reads in catchments shapefiles matching the PF grid,
    as created with delineate_watersheds.py
    each catchment is related to an outlet (either real station or not)
    and in this script we add auxiliary info (vegetation distribution,
    yearly precip, Ks...) that we crop out a tiff using the shapes.

    @copyright: 2021 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2021"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import geopandas as gpd
import os
from rasterstats import zonal_stats
from rasterio.transform import Affine
import rasterio as rio
from rasterio.mask import mask
from shapely.geometry import mapping # transform to GeJSON format

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb


plt.close('all')
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
# domain_setup dir: directory where to save outputs relative to the specific grid:
domain_setup_dir = '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/eval/streamflow/domain_setups/CONWA_AD/'

#data_loc_dir is where the OUTPUT, formatted streamflow data LOCATION will be stored:
# (if applicable: this preproc can be done elsewhere)
data_loc_dir = os.sep.join([domain_setup_dir,'data_loc/'])

# aux_data_dir is where auxiliary data (eg draining area map, streamflow map)
#can be found: it is not specific to a single simulation but can be used for se
aux_data_dir = os.sep.join([domain_setup_dir,'aux_data/'])

#setup_figures_dir is directory where to save the figures relative to the specifc grid (stations locations, pf stream network, etc)
setup_figures_dir = os.sep.join([domain_setup_dir,'figures/'])

# specific simulation dir for adding up simulation outputs:
simulation_name = 'fullyear1'
sim_dir = os.sep.join([domain_setup_dir,'simulations/%s/'%simulation_name])
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def extract_discrete_classes_from_tif_using_shp(tif_filename,gds):
    """reads in a tif, crop it using a geopandas with polygons
    then compute histograms and get count values for each class
    returns a panda object with frequencies (%) per class """

    # extract the geometries in GeoJSON format
    geoms = gds.geometry.values # list of shapely geometries
    records = gds.values
    with rio.open(tif_filename) as src:
        classes,counts  = np.unique(src.read(1),return_counts=True)

    with rio.open(tif_filename) as src:
        print('nodata value:', src.nodata)
        idx_area = 0
        for index, row in gds.iterrows():
            upslope_area = row['geometry']
            name = row['Name']
            mapped_geom = [mapping(upslope_area)]
            out_image, out_transform = mask(src, mapped_geom, crop=True)# extract the raster values values within the polygon
            no_data=src.nodata #no data values of the original raster
            #~ data = out_image.data[0] #extract the values of the masked array
            data = out_image #extract the values of the masked array
            clas = np.extract(data != no_data, data) # extract the row, columns of the valid values
            #~ frequencies, class_limits = np.histogram(clas,bins=classes,range=[range_min, range_max])
            frequencies, class_limits = np.histogram(clas,bins=classes)
            if idx_area == 0:
                VEG = pd.DataFrame({str(name): frequencies})
                VEG.index = class_limits[:-1]
            else:
                VEG[str(name)] = frequencies
            idx_area += 1
    
    #classes from int to str:
    VEG = VEG.rename(index={t:str(int(t)) for t in VEG.index})
    # counts -> %
    VEG = 100*VEG/VEG.sum()
    #~ return VEG.set_index(VEG.index.astype(int)).T
    return VEG.T
    

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

""" Add yearly precipitation """
print('compute yearly P per catchment')
#AC_Benin
ACgpd = gpd.read_file(data_loc_dir+'AC_Benin/catchments_05_ep001_n/AC_catchments.shp')
ACgpd = ACgpd.set_index(ACgpd.Name)
tmp=zonal_stats(ACgpd,aux_data_dir+ "ini_Pre_yearly_mm.tif")
ACgpd['P_year'] = np.array([t['mean'] for t in tmp]) 

#ADHI
ADHIgpd = gpd.read_file(data_loc_dir+'ADHI/catchments_05_ep001_n/ADHI_catchments.shp')
ADHIgpd = ADHIgpd.set_index(ADHIgpd.Name)
tmp=zonal_stats(ADHIgpd,aux_data_dir+ "ini_Pre_yearly_mm.tif")
ADHIgpd['P_year'] = np.array([t['mean'] for t in tmp]) 

#SIEREM
SIEREMgpd = gpd.read_file(data_loc_dir+'SIEREM/catchments_05_ep001_n/SIEREM_catchments.shp')
SIEREMgpd = SIEREMgpd.set_index(SIEREMgpd.Name)
tmp=zonal_stats(SIEREMgpd,aux_data_dir+ "ini_Pre_yearly_mm.tif")
SIEREMgpd['P_year'] = np.array([t['mean'] for t in tmp]) 


""" Add vegetation distribution """
print('compute vegetation distribution per catchment')
tif_filename = '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/eval/streamflow/domain_setups/CONWA_AD/aux_data/LCC.tif'
# first create a tif, if needed : uncomment the following
#~ pfb_filename = '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/eval/streamflow/domain_setups/CONWA_AD/aux_data/LCC.pfb'
#~ crs = '+proj=aea +lat_1=16.12 +lat_2=6.58 +lat_0=11.35 +lon_0=-4.3 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs'
#~ iopfb.write_tif_from_pfb(pfb_filename,tif_filename,crs)

#AC_Benin
ACgpd = pd.concat([ACgpd,extract_discrete_classes_from_tif_using_shp(tif_filename,ACgpd)],axis=1)

#ADHI
ADHIgpd = pd.concat([ADHIgpd,extract_discrete_classes_from_tif_using_shp(tif_filename,ADHIgpd)],axis=1)

#SIEREM
SIEREMgpd = pd.concat([SIEREMgpd,extract_discrete_classes_from_tif_using_shp(tif_filename,SIEREMgpd)],axis=1)

""" Add slopes """
print('compute mean slopes per catchment')

tif_filename = '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/eval/streamflow/domain_setups/CONWA_AD/aux_data/CONWA_MERIT_slope_mag_stan_05_ep001_200504.tif'

# first create a tif with slope magnitudes, out of x y slopes: if needed : uncomment the following
#~ pfb_filename_slopex = '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/eval/streamflow/domain_setups/CONWA_AD/aux_data/CONWA_MERIT_slopex_stan_05_ep001_200504.pfb'
#~ pfb_filename_slopey = '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/eval/streamflow/domain_setups/CONWA_AD/aux_data/CONWA_MERIT_slopey_stan_05_ep001_200504.pfb'
#~ crs = '+proj=aea +lat_1=16.12 +lat_2=6.58 +lat_0=11.35 +lon_0=-4.3 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs'
#~ slopex,h = iopfb.PFB_read(pfb_filename_slopex,1) 
#~ slopey,h = iopfb.PFB_read(pfb_filename_slopey,1) 
#~ slopex = slopex[:,:,0]
#~ slopey = slopey[:,:,0]
#~ d = np.sqrt(slopex*slopex+slopey*slopey)   
#~ # TODO : !check this is ok:
#~ transform = Affine.translation(h['X0'],  h['Y0']+(h['ny']-1)*h['dy']) * Affine.scale(h['dx'], -h['dy'])
#~ d2 = d.swapaxes(0,1)
#~ d2 = d2[::-1,:]
#~ with rio.open(tif_filename,'w',driver='GTiff',height=d2.shape[0],width=d2.shape[1],count=1,dtype=d2.dtype,
    #~ crs=crs,transform=transform) as dst:
        #~ dst.write(d2,1)

# then extract slope magnitudes

#AC_Benin
tmp=zonal_stats(ACgpd,tif_filename)
ACgpd['slope'] = np.array([t['mean'] for t in tmp]) 

#ADHI
tmp=zonal_stats(ADHIgpd,tif_filename)
ADHIgpd['slope'] = np.array([t['mean'] for t in tmp]) 

#SIEREM
tmp=zonal_stats(SIEREMgpd,tif_filename)
SIEREMgpd['slope'] = np.array([t['mean'] for t in tmp]) 

""" Add Ksat of the aquifer (cell nz -1 because of hard rock masking) """
print('compute mean Ks per catchment')
tif_filename = '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/eval/streamflow/domain_setups/CONWA_AD/aux_data/Ksat_mask_glhymps_v3_nonan_with_crust_MAD_no_river_crust_WA_dz2.tif'

# first create a tif with slope magnitudes, out of x y slopes: if needed : uncomment the following
#~ pfb_filename = '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/eval/streamflow/domain_setups/CONWA_AD/aux_data/Ksat_mask_glhymps_v3_nonan_with_crust_MAD_no_river_crust_WA_dz2.pfb'
#~ crs = '+proj=aea +lat_1=16.12 +lat_2=6.58 +lat_0=11.35 +lon_0=-4.3 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs'
#~ d,h = iopfb.PFB_read(pfb_filename,1) 
#~ d = d[:,:,-2]
#~ # TODO : check this is ok:
#~ transform = Affine.translation(h['X0'],  h['Y0']+(h['ny']-1)*h['dy']) * Affine.scale(h['dx'], -h['dy'])
#~ d2 = d.swapaxes(0,1)
#~ d2 = d2[::-1,:]
#~ with rio.open(tif_filename,'w',driver='GTiff',height=d2.shape[0],width=d2.shape[1],count=1,dtype=d2.dtype,
    #~ crs=crs,transform=transform) as dst:
        #~ dst.write(d2,1)

# then extract Ks (m/h)

#AC_Benin
tmp=zonal_stats(ACgpd,tif_filename)
ACgpd['Ks'] = np.array([t['mean'] for t in tmp]) 

#ADHI
tmp=zonal_stats(ADHIgpd,tif_filename)
ADHIgpd['Ks'] = np.array([t['mean'] for t in tmp]) 

#SIEREM
tmp=zonal_stats(SIEREMgpd,tif_filename)
SIEREMgpd['Ks'] = np.array([t['mean'] for t in tmp]) 

"""save"""

ACgpd.to_file(data_loc_dir+'AC_Benin/catchments_05_ep001_n/AC_catchments_P_veg_slope_Ks.shp')
ADHIgpd.to_file(data_loc_dir+'ADHI/catchments_05_ep001_n/ADHI_catchments_P_veg_slope_Ks.shp')
SIEREMgpd.to_file(data_loc_dir+'SIEREM/catchments_05_ep001_n/SIEREM_catchments_P_veg_slope_Ks.shp')

"""
Now add up some simulation specific outputs !!! save in a different dir (simulation outputs)
"""

""" Add mean WTD from simulation outputs"""
print('compute mean WTD per catchment')
#AC_Benin
tmp=zonal_stats(ACgpd,sim_dir+ "WTD_y0_full_Ymean.tif")
ACgpd['WTD'] = np.array([t['mean'] for t in tmp]) 

#ADHI
tmp=zonal_stats(ADHIgpd,sim_dir+ "WTD_y0_full_Ymean.tif")
ADHIgpd['WTD'] = np.array([t['mean'] for t in tmp]) 

#SIEREM
tmp=zonal_stats(SIEREMgpd,sim_dir+ "WTD_y0_full_Ymean.tif")
SIEREMgpd['WTD'] = np.array([t['mean'] for t in tmp]) 

""" Add yearly ET from simulation outputs"""
print('compute yearly ET per catchment')
#AC_Benin
tmp=zonal_stats(ACgpd,sim_dir+ "evaptranssum_y0_full_Ysum_corrP.tif")
ACgpd['ET'] = np.array([t['mean'] for t in tmp]) 

#ADHI
tmp=zonal_stats(ADHIgpd,sim_dir+ "evaptranssum_y0_full_Ysum_corrP.tif")
ADHIgpd['ET'] = np.array([t['mean'] for t in tmp]) 

#SIEREM
tmp=zonal_stats(SIEREMgpd,sim_dir+ "evaptranssum_y0_full_Ysum_corrP.tif")
SIEREMgpd['ET'] = np.array([t['mean'] for t in tmp]) 



"""save"""

ACgpd.to_file(sim_dir+'/catchments/AC_catchments_P_veg_slope_Ks_%s_WTD_ET.shp'%simulation_name)
ADHIgpd.to_file(sim_dir+'/catchments/ADHI_catchments_P_veg_slope_Ks_%s_WTD_ET.shp'%simulation_name)
SIEREMgpd.to_file(sim_dir+'/catchments/SIEREM_catchments_P_veg_slope_Ks_%s_WTD_ET.shp'%simulation_name)
