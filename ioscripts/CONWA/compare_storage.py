#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Compare Parflow Storage outputs and GRACE data

    GRACE data
    Grace = xr.open_dataset('/home/hectorb/DATA/GRACE/GRACE_IGE/GRCTellus.JPL.200204_202009.GLO.RL06M.MSCNv02CRI.nc')
    from
    https://grace.jpl.nasa.gov/data/get-data/jpl_global_mascons/
    
    
    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import xarray as xr
import os,datetime

"""local imports:"""


plt.close('all')

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
# domain_setup dir: directory where to save outputs relative to the specific grid:
# faro:
domain_setup_dir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/eval/WSC/domain_setups/CONWA_AD/'

#data_dir : where the streamflow observation data can be found:
#faro
data_dir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/eval/WSC/data/'

# specific simulation dir for adding up simulation outputs:
simulation_name = 'fullyear1'
sim_dir = os.sep.join([domain_setup_dir,'simulations/%s/'%simulation_name])

#simulation_specific figures:
fig_dir = os.sep.join([sim_dir,'figures/'])

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

"""reads in PF storage"""
#~ S_amp_PF = xr.open_dataset('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/storage_amplitude_y9.nc')
#~ S_amp_PF = xr.open_dataset('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/storage_amplitude_y14.nc')
#faro:
S_amp_PF = xr.open_dataset(sim_dir+'/storage_amplitude_y0_full.nc')
S_amp_PF.d_storage.attrs['units']='mm'
S_amp_PF = S_amp_PF.rename_vars({'d_storage':'storage_amplitude'})    

""" Reads in GRACE data"""
#~ Grace = xr.open_dataset('/home/hectorb/DATA/GRACE/GRACE_IGE/GRCTellus.JPL.200204_202009.GLO.RL06M.MSCNv02CRI.nc')
#faro:
Grace = xr.open_dataset(data_dir+'GRCTellus.JPL.200204_202009.GLO.RL06M.MSCNv02CRI.nc')
Grace = Grace.sel(time=slice("2015-1-1","2015-12-31"))  
# change the longitude from 0 to 360 to -180 +180
#~ Grace = Grace.assign_coords(lon=(((Grace.lon + 180)%360)-180)).roll(lon=(Grace.dims['lon'] // 2),roll_coords = True)
Grace = Grace.assign_coords({'lon':(((Grace.lon + 180)%360)-180),'lat':(Grace.lat)}).roll(lon=(Grace.dims['lon'] // 2),roll_coords = True)

# cm -> mm
Grace.lwe_thickness.values = Grace.lwe_thickness.values*10.
#~ Grace = Grace.sel(lon=slice(-20,12),lat=slice(3,19),time=slice("2015-1-1","2015-12-31"))
#strangely 28/01/2021: the above doesn t work anymore, we need todo it earlier
Grace.lwe_thickness.attrs['units'] = 'mm'
Grace = Grace.rename_vars({'lwe_thickness':'storage_amplitude'})    
Gracedata = Grace.storage_amplitude.sel(lon=slice(-20,12),lat=slice(3,19))

""" Reads in scale factor data"""
#~ Grace_scale = xr.open_dataset('/home/hectorb/DATA/GRACE/GRACE_IGE/CLM4.SCALE_FACTOR.JPL.MSCNv02CRI.nc')
#faro:
Grace_scale = xr.open_dataset(data_dir+'CLM4.SCALE_FACTOR.JPL.MSCNv02CRI.nc')
# change the longitude from 0 to 360 to -180 +180
Grace_scale = Grace_scale.assign_coords(lon=(((Grace_scale.lon + 180)%360)-180)).roll(lon=(Grace_scale.dims['lon'] // 2),roll_coords = True)
Grace_scaledata = Grace_scale.scale_factor.sel(lon=slice(-20,12),lat=slice(3,19))

""" Correct GRACE"""
# comment if correction is not needed
#~ Grace.storage_amplitude.values = (Grace.storage_amplitude*Grace_scale.scale_factor).values
#~ Gracedata.values = (Gracedata*Grace_scaledata).values


""" Plot comparisons at different resolution"""

fig,ax = plt.subplots(2,2,figsize=(14,8.2),squeeze=True)
### Original PF:
S_amp_PF.storage_amplitude.plot(ax=ax[0,0])
ax[0,0].set_title('Original PF')
### GRACE data
#~ (Grace.storage_amplitude.max(dim="time") - Grace.storage_amplitude.min(dim='time')).plot(ax=ax[0,1],vmin=0,vmax=650) 
(Gracedata.max(dim="time") - Gracedata.min(dim='time')).plot(ax=ax[0,1],vmin=0,vmax=650) 
ax[0,1].set_title('GRACE data (JPL MASCONS)')
ax[0,1].set_xlim([-17.4,8.8])
ax[0,1].set_ylim([3.8,18.3])
### PF 160km:
S_amp_PF.coarsen(x=160).mean().coarsen(y=160).mean().storage_amplitude.plot(ax=ax[1,0])
ax[1,0].set_title('PF 160km')
### PF 320km:
S_amp_PF.coarsen(x=320).mean().coarsen(y=320).mean().storage_amplitude.plot(ax=ax[1,1],vmin=0,vmax=650)
ax[1,1].set_title('PF 320km')

for a in ax.flat: a.set_aspect(1)
# uncomment if correction is not needed
#~ plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/storage_amplitude_pf_vs_grace.png')
#~ plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/storage_amplitude_pf_y14_vs_grace.png')
#faro:
plt.savefig(fig_dir+'storage_amplitude_pf_y0_full_vs_grace.png')
# uncomment if correction is needed
#~ plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/storage_amplitude_pf_y14_vs_grace_w_scale_factor.png')
#faro:
#~ plt.savefig(fig_dir+'storage_amplitude_pf_y0_full_vs_grace_w_scale_factor.png')
