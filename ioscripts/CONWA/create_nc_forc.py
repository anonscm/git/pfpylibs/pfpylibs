
#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    convert 30mn precipitation file to daily precip
    
    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os, glob, copy
import xarray as xr
import dask

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb

plt.close('all')

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
precip_file = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/ini_files/ini_Pre.nc'
datadir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/post_spinup/'
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

P = xr.open_dataset(precip_file,chunks={'y':24})

times = pd.date_range("2015-01-01", periods=17520,freq='30min')

P = P.assign_coords(Time = times)

    #mm/s -> mm
    #~ d = d*60*30.

"""write netcdf"""
#~ ds = xr.Dataset({"evaptranssum": (("x", "y","time"), final_array)},
#~ ds = xr.Dataset({"evaptranssum": (("time","x", "y"), numpy.moveaxis(final_array,2,0))},
#~ ds = xr.Dataset({"precipitation": (("time","x", "y"), np.swapaxes(final_array,2,0))},
    #~ coords={"lon":np.arange(start = h['X0'],stop = h['X0']+h['nx']*h['dx'],step=h['dx']),
        #~ "lat":np.arange(start = h['Y0'],stop = h['Y0']+h['ny']*h['dy'],step=h['dy']), 
        #~ "time": pd.date_range("2015-01-01", periods=365),
        #~ "reference_time": pd.Timestamp("2015-01-01")})

#~ ds.precipitation.attrs['units']='mm'
#~ ds.lon.attrs['units']='meters'
#~ ds.lat.attrs['units']='meters'

#~ ds.to_netcdf(os.path.join(datadir,"precipitaion_IMERG_day.nc"))
