#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Anaylse PFBs for CONWA : exemple script of different commands in io_pfb 
    for plotting some outputs pfb
    

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb
plt.close('all')
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
#~ resdir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/post_spinup/'
#~ resdir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0_test_c4/'
#~ resdir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0_/'
#~ savedir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/post_spinup/fig/'
#~ savedir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0_test_c4/'
dz=0.01
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##



"""time step is 3h clm outputs is every 3 hours & pf every 24h: so file name increment of unit 1 every 3hrs and daily outputs of unit 8 every 24h. So end of month 1 is 30days x 8 = 240:"""
"""
for i in np.arange(1,13):
    print('read month: %d'%i)
    if i<=8:
        run=0
    else:
        run=1
    d,xx,yy,fig,ax,cb = iopfb.plot_2D_PFB(resdir+'results_WA_post_spinup_2560_y0_run%d/WA_vd_KWE_spinup.out.evaptranssum.%05d.pfb'%(run,i*30*8),savefig=True,savedir=savedir,\
    savename='evaptranssum_run%d_month%d_surface.png'%(run,i),vmin=-7,vmax=0,colorlabel='ET (mm/d)',zlayer=0,scale=1000*dz/(1000*1000),dpi=100) 

plt.close('all')
"""

"""sum up all ET from evaptranssum from top to surface"""
"""
d,h=iopfb.PFB_read(resdir+'results_WA_post_spinup_2560_y0_run0/WA_vd_KWE_spinup.out.evaptranssum.01944.pfb',1)
x=np.arange(h['X0'],h['X0']+h['nx']*h['dx'],h['dx'])   
y=np.arange(h['Y0'],h['Y0']+h['ny']*h['dy'],h['dy'])
[xx,yy] = np.meshgrid(x,y)
d[d>0]=np.nan
d=0.01*np.swapaxes(d[:,:,0],1,0)+0.01*np.swapaxes(d[:,:,1],1,0)+0.01*np.swapaxes(d[:,:,2],1,0)+0.01*np.swapaxes(d[:,:,3],1,0)+\
0.02*np.swapaxes(d[:,:,4],1,0)+0.04*np.swapaxes(d[:,:,5],1,0)+0.1*np.swapaxes(d[:,:,6],1,0)
d=d/1000

fig = plt.figure(figsize=(8,4))
ax = fig.add_subplot(111)
ax.set_aspect(1)

p = ax.pcolormesh(xx, yy, d,vmin=-7,vmax=0,zorder=-1,cmap='viridis')
#~ ax.set_title('WTD diff end-start',loc='left',fontdict={'fontsize':14, 'fontweight':'bold'})
ax.set_title('P-ET tot 1944',loc='left',fontdict={'fontsize':14, 'fontweight':'bold'})
ax.set_xticklabels('')
ax.set_yticklabels('')
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb = plt.colorbar(p,orientation='vertical',cax=cax)


cb.set_label('ET (mm/d)',fontsize=14)

plt.tight_layout()
#~ plt.savefig(os.sep.join([savedir,'WTD_diff_end_m_start.png']),dpi=100,format='png')
plt.savefig(os.sep.join([savedir,'evaptranssumtot_1944.png']),dpi=100,format='png')
"""


"""WTD"""
resdir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0_test_c4/'
savedir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0_test_c4/'
d,xx,yy,fig,ax,cb = iopfb.plot_2D_PFB(resdir+'WA_vd_KWE_spinup.out.press.00720.pfb',savefig=True,savedir=savedir,\
savename='WA_2560_y1510_newdz_run0_test_c4_press00720.png',vmin=0,vmax=65,colorlabel='WTD (m)',cmap='terrain',zlayer=10,scale=-1,offset=-65,nanvalue=-9999.,threshold_low=-10000000,dpi=100) 

resdir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0/'
savedir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0/'
d,xx,yy,fig,ax,cb = iopfb.plot_2D_PFB(resdir+'WA_vd_KWE_spinup.out.press.00720.pfb',savefig=True,savedir=savedir,\
savename='WA_2560_y1510_newdz_run0_press00720.png',vmin=0,vmax=65,colorlabel='WTD (m)',cmap='terrain',zlayer=10,scale=-1,offset=-65,nanvalue=-9999.,threshold_low=-10000000,dpi=100) 

resdir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run4_RST4/'
savedir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run4_RST4/'
d,xx,yy,fig,ax,cb = iopfb.plot_2D_PFB(resdir+'WA_vd_KWE_RST.out.press.03528.pfb',savefig=True,savedir=savedir,\
savename='WA_2560_y1510_newdz_run4_RST4_WTD_press03528.png',vmin=0,vmax=65,colorlabel='WTD (m)',cmap='terrain',zlayer=10,scale=-1,offset=-65,nanvalue=-9999.,threshold_low=-10000000,dpi=100) 


"""cell #5"""
resdir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0_test_c4/'
savedir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0_test_c4/'
d,xx,yy,fig,ax,cb = iopfb.plot_2D_PFB(resdir+'WA_vd_KWE_spinup.out.press.00720.pfb',savefig=True,savedir=savedir,\
savename='WA_2560_y1510_newdz_run0_cell5_test_c4_press00720.png',title='cell 5',vmin=-100,vmax=0,colorlabel='succion (m)',cmap='terrain_r',zlayer=4,scale=1,offset=0,nanvalue=-9999.,threshold_low=-10000000,dpi=100) 

resdir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0/'
savedir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0/'
d,xx,yy,fig,ax,cb = iopfb.plot_2D_PFB(resdir+'WA_vd_KWE_spinup.out.press.00720.pfb',savefig=True,savedir=savedir,\
savename='WA_2560_y1510_newdz_run0_cell5_press00720.png',title='cell 5',vmin=-100,vmax=0,colorlabel='succion (m)',cmap='terrain_r',zlayer=4,scale=1,offset=0,nanvalue=-9999.,threshold_low=-10000000,dpi=100) 


resdir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0_test_deep_root_upwind/'
savedir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0_test_deep_root_upwind/'
d,xx,yy,fig,ax,cb = iopfb.plot_2D_PFB(resdir+'WA_vd_KWE_spinup.out.press.00576.pfb',savefig=True,savedir=savedir,\
savename='WA_2560_y1510_newdz_run0_test_deep_root_upwind_cell4_press00576.png',title='cell 4',vmin=-2,vmax=0,colorlabel='succion (m)',cmap='terrain_r',zlayer=4,scale=1,offset=0,nanvalue=-9999.,threshold_low=-10000000,dpi=100) 

resdir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0_test_c4/'
savedir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0_test_c4/'
d,xx,yy,fig,ax,cb = iopfb.plot_2D_PFB(resdir+'WA_vd_KWE_spinup.out.press.00576.pfb',savefig=True,savedir=savedir,\
savename='WA_2560_y1510_newdz_run0_test_c4_cell4_press00576.png',title='cell 4',vmin=-2,vmax=0,colorlabel='succion (m)',cmap='terrain_r',zlayer=4,scale=1,offset=0,nanvalue=-9999.,threshold_low=-10000000,dpi=100) 

resdir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0/'
savedir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0/'
d,xx,yy,fig,ax,cb = iopfb.plot_2D_PFB(resdir+'WA_vd_KWE_spinup.out.press.00576.pfb',savefig=True,savedir=savedir,\
savename='WA_2560_y1510_newdz_run0_cell4_press00576.png',title='cell 4',vmin=-2,vmax=0,colorlabel='succion (m)',cmap='terrain_r',zlayer=4,scale=1,offset=0,nanvalue=-9999.,threshold_low=-10000000,dpi=100) 


"""cell #0"""
resdir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run4_RST4/'
savedir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run4_RST4/'
d,xx,yy,fig,ax,cb = iopfb.plot_2D_PFB(resdir+'WA_vd_KWE_RST.out.press.03528.pfb',savefig=True,savedir=savedir,\
savename='WA_2560_y1510_newdz_run4_RST4_cell0_press03528.png',title='cell 0',vmin=-2,vmax=0,colorlabel='succion (m)',cmap='terrain_r',zlayer=0,scale=1,offset=0,nanvalue=-9999.,threshold_low=-10000000,dpi=100) 

resdir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0_test_deep_root_upwind/'
savedir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0_test_deep_root_upwind/'
d,xx,yy,fig,ax,cb = iopfb.plot_2D_PFB(resdir+'WA_vd_KWE_spinup.out.press.00576.pfb',savefig=True,savedir=savedir,\
savename='WA_2560_y1510_newdz_run0_test_deep_root_upwind_cell0_press00576.png',title='cell 0',vmin=-2,vmax=0,colorlabel='succion (m)',cmap='terrain_r',zlayer=0,scale=1,offset=0,nanvalue=-9999.,threshold_low=-10000000,dpi=100) 

resdir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0_test_c4/'
savedir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0_test_c4/'
d,xx,yy,fig,ax,cb = iopfb.plot_2D_PFB(resdir+'WA_vd_KWE_spinup.out.press.00576.pfb',savefig=True,savedir=savedir,\
savename='WA_2560_y1510_newdz_run0_test_c4_cell0_press00576.png',title='cell 0',vmin=-2,vmax=0,colorlabel='succion (m)',cmap='terrain_r',zlayer=0,scale=1,offset=0,nanvalue=-9999.,threshold_low=-10000000,dpi=100) 

resdir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0/'
savedir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/results/results_WA_2560_y1510_newdz_run0/'
d,xx,yy,fig,ax,cb = iopfb.plot_2D_PFB(resdir+'WA_vd_KWE_spinup.out.press.00576.pfb',savefig=True,savedir=savedir,\
savename='WA_2560_y1510_newdz_run0_cell0_press00576.png',title='cell 0',vmin=-2,vmax=0,colorlabel='succion (m)',cmap='terrain_r',zlayer=0,scale=1,offset=0,nanvalue=-9999.,threshold_low=-10000000,dpi=100) 


"""WTD"""

"""
for i in np.arange(1,13):
    print('read month: %d'%i)
    if i<=8:
        run=0
    else:
        run=1
    d,xx,yy,fig,ax,cb = iopfb.plot_2D_PFB(resdir+'results_WA_post_spinup_2560_y0_run%d/WA_vd_KWE_spinup.out.press.%05d.pfb'%(run,i*30*8),savefig=True,savedir=savedir,\
    savename='WTD_run%d_month%d.png'%(run,i),vmin=0,vmax=65,colorlabel='WTD (m)',cmap='terrain',zlayer=10,scale=-1,offset=-65,nanvalue=-9999.,threshold_low=-10000000,dpi=100) 

plt.close('all')

d1,h=iopfb.PFB_read(resdir+'results_WA_post_spinup_2560_y0_run0/WA_vd_KWE_spinup.out.press.00000.pfb',1)
#~ d2,h=iopfb.PFB_read(resdir+'results_WA_post_spinup_2560_y0_run0/WA_vd_KWE_spinup.out.press.01440.pfb',1)
d2,h=iopfb.PFB_read(resdir+'results_WA_post_spinup_2560_y0_run1/WA_vd_KWE_spinup.out.press.02920.pfb',1)
x=np.arange(h['X0'],h['X0']+h['nx']*h['dx'],h['dx'])   
y=np.arange(h['Y0'],h['Y0']+h['ny']*h['dy'],h['dy'])
[xx,yy] = np.meshgrid(x,y)
nanvalue=-9999.
threshold_low=-10000000
scale=-1
offset=-65
zlayer=10
d1=scale*(np.swapaxes(d1[:,:,zlayer],1,0)+offset)
d2=scale*(np.swapaxes(d2[:,:,zlayer],1,0)+offset)
if nanvalue:
    d1[d1==nanvalue] = np.nan
    d2[d2==nanvalue] = np.nan
if threshold_low:
    d1[d1<threshold_low] = np.nan
    d2[d2<threshold_low] = np.nan

d=d2-d1

fig = plt.figure(figsize=(8,4))
ax = fig.add_subplot(111)
ax.set_aspect(1)

p = ax.pcolormesh(xx, yy,d,vmin=-30,vmax=5,zorder=-1,cmap='viridis')
ax.set_title('WTD diff end-start',loc='left',fontdict={'fontsize':14, 'fontweight':'bold'})
#~ ax.set_title('WTD diff 6months-start',loc='left',fontdict={'fontsize':14, 'fontweight':'bold'})
ax.set_xticklabels('')
ax.set_yticklabels('')
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb = plt.colorbar(p,orientation='vertical',cax=cax)


cb.set_label('WTD diff (m)',fontsize=14)

plt.tight_layout()
plt.savefig(os.sep.join([savedir,'WTD_diff_end_m_start.png']),dpi=100,format='png')
#~ plt.savefig(os.sep.join([savedir,'WTD_diff_6m_m_start.png']),dpi=100,format='png')
"""
