
#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    correct evaptranssum gathered from PFBs using concat_evaptranssum_to_nc.py
    => add up the precip (remove a negative percip)

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os, glob, copy
import xarray as xr

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb

plt.close('all')

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
precip_file = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/post_spinup/results_after_nc_concat/ini_Pre_daily.nc'
ET_file = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/post_spinup/evaptranssum_y14_vcorr.nc'
ET_file = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/nc_results/evaptranssum_y0_full.nc'
#~ ET_file = '/home/hectorb/PARFLOW/PROJECTS/Ara/resolution2020/inputs/ara100/ara100/evaptranssum.nc'
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
ET = xr.open_dataset(ET_file)
P = xr.open_dataset(precip_file)

#~ P.Time = 
#~ ET_corr = xr.Dataset({"evaptranssum": (("time","y", "x"), ET.evaptranssum.values[:,::-1,:] + (P.Pre.values*3600*24))},
    #~ coords={"x":P.x.values,"y":P.y.values,"time": ET.time.values,
        #~ "reference_time": pd.Timestamp("2015-01-01")})
    #version of Alban has wrong times: so setup manually
ET_corr = xr.Dataset({"evaptranssum": (("time","y", "x"), ET.evaptranssum.values[:,::-1,:] + (P.Pre.values*3600*24))},
    coords={"x":P.x.values,"y":P.y.values,"time": pd.date_range(start="2015-1-1",freq='D',periods=365),
        "reference_time": pd.Timestamp("2015-01-01")})

ET_corr.evaptranssum.attrs['units']='mm'
ET_corr.x.attrs['units']='meters'
ET_corr.y.attrs['units']='meters'

#~ ET_corr.to_netcdf('/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/post_spinup/evaptranssum_y14_vcorr2.nc',format ="NETCDF4")
#~ ET_corr.to_netcdf('/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/eval/ET/domain_setups/CONWA_AD/simulations/fullyear1/evaptranssum_y0_full_vcorr2.nc',format ="NETCDF4")
# should we use http://xarray.pydata.org/en/stable/generated/xarray.Dataset.to_netcdf.html unlimited dims?


""" Compute monthly averages """
#uncomment if needed

#~ ET_corr.groupby("time.month").mean().to_netcdf('/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/post_spinup/evaptranssum_y14_vcorr2_monthly.nc',format ="NETCDF4")
#~ ET_corr.groupby("time.month").mean().to_netcdf('/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/eval/ET/domain_setups/CONWA_AD/simulations/fullyear1/evaptranssum_y0_full_vcorr2_monthly.nc',format ="NETCDF4")
ET_corr.groupby("time.month").mean().to_netcdf('/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/eval/ET/domain_setups/CONWA_AD/simulations/fullyear1/evaptranssum_y0_full_vcorr2_monthly.nc',format ="NETCDF4")
