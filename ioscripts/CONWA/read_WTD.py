#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS
    
    Quick function to read WTD from CONWA spinup

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
import seaborn as sns
from decimal import Decimal
import scipy.spatial as ann
import pyproj
import matplotlib as mpl

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb
from procsycz import readDataAMMA as rdA
from procsycz import procGeodata_Gdal

plt.close('all')

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def ann_tree(x, y, X, Y, nb=1):
	"""
	takes 1D arrays, zip () makes a list of tuples ()
	"""
	tree = ann.KDTree(np.array([(i,j) for i,j in zip(x, y)]))
	res = tree.query(np.array([(i,j) for i,j in zip(X, Y)]), nb)[1]
	return res

def findNN(data,xdata,ydata,xtarget,ytarget):
	"""
	find the closest data points to xtarget and ytarget
	all x,y are 1D arrays (can use xx.flatten())
	but data is 2D (len(ydata),len(xdata))
	"""
	tree = ann_tree(xdata, ydata, xtarget,ytarget, nb=1)
	tmpvec=np.zeros(np.shape(data))
	k=0
	return [data[indexes // np.shape(data)[1],indexes % np.shape(data)[1]] for dist, indexes in enumerate(tree)]
	"""
	for dist, indexes in enumerate(tree):
		#if indices in the target coordinates are needed:
		#~ i = dist % nx
		#~ j = dist // nx            
		i0 = indexes % np.shape(data)[1]
		j0 = indexes // np.shape(data)[1]
		tmpvec[k]=data[j0,i0]
		k=k+1
	"""
def prepare_colormap(cmapname,bound_low,bound_high):
	"""
	prepare a colormap based on the data range
	also force first entry to be grey
	"""
	cmap = cmapname
	cmaplist = [cmap(i) for i in range(cmap.N)] 				# extract all colors from the .jet map
	#~ cmaplist[0] = (.5,.5,.5,1.0) 								# force the first color entry to be grey
	#~ cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N) 	# create the new map
	bounds = np.arange(bound_low,bound_high,5) 									# define the bins and normalize
	norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
	return cmap,norm,bounds


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
""" Read in PmET forcing field"""
#### shorten the datafile (do it only once)
#~ PmET,h = iopfb.PFB_read('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/pfb_files/GLDAS_PminusET_no0_m_1j_corrected_lw_newdz_div50.pfb',1)
#~ h['nz']=1;h['nz2']=1
#~ iopfb.PFB_write(PmET[:,:,0],h,'/home/hectorb/PARFLOW/PROJECTS/CONWA/active/pfb_files/GLDAS_PminusET_no0_m_1j_corrected_lw_newdz_div50_surface.pfb')
#~ iopfb.plot_2D_PFB('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/pfb_files/GLDAS_PminusET_no0_m_1j_corrected_lw_newdz_div50_surface.pfb',plot_fig=True)
PmET,xx,yy,fig,ax,cb = iopfb.plot_2D_PFB('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/pfb_files/GLDAS_PminusET_no0_m_1j_corrected_lw_newdz_div50_surface.pfb',plot_fig=True)
#PmET = [m3/m3]/j /dz/vardz
# so to have mm/yr x 365x1000 xdz xvardz
PmET = PmET*365*1000*0.1

""" Read in Ks """

#### shorten the datafile (do it only once)
#~ Ks,h = iopfb.PFB_read('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/pfb_files/Ksat_mask_glhymps_v3_nonan_with_crust_MAD_no_river_crust.pfb',1)
#~ h['nz']=1;h['nz2']=1
#~ iopfb.PFB_write(Ks[:,:,-2],h,'/home/hectorb/PARFLOW/PROJECTS/CONWA/active/pfb_files/Ksat_mask_glhymps_v3_nonan_with_crust_MAD_no_river_crust_aquifer.pfb')

Kslog,xx,yy,fig,ax,cb = iopfb.plot_2D_PFB('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/pfb_files/Ksat_mask_glhymps_v3_nonan_with_crust_MAD_no_river_crust_aquifer.pfb',plot_fig=True,logscale=True)
#~ Ks,h =iopfb.read_PFB('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/pfb_files/Ksat_mask_glhymps_v3_nonan_with_crust_MAD_no_river_crust_aquifer.pfb',1)
 
""" Read in spinup outputs"""
#~ WTD = xr.open_dataset('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/WTD_after_390y_of_spinup_riv_mask.nc',decode_times=False)
WTD_143yrs = xr.open_dataset('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup/WTD_after_143y_of_spinup_riv_mask.nc',decode_times=False)
WTD_230yrs = xr.open_dataset('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup/WTD_after_230y_of_spinup_riv_mask.nc',decode_times=False)
WTD_390yrs = xr.open_dataset('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup/WTD_after_390y_of_spinup_riv_mask.nc',decode_times=False)
WTD_750yrs = xr.open_dataset('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup/WTD_after_750y_of_spinup_riv_mask.nc',decode_times=False)
WTD_1110yrs = xr.open_dataset('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup/WTD_after_1110y_of_spinup_riv_mask.nc',decode_times=False)
WTD_1310yrs = xr.open_dataset('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup/WTD_after_1310y_of_spinup_riv_mask.nc',decode_times=False)
WTD_CLM_y12 = xr.open_dataset('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_mean_y12.nc')


#slice? comment/uncomment
#~ WTD_143yrs = WTD_143yrs.sel(longitude=slice(1.475,2.814),latitude=slice(10.2,8.96))
#~ WTD_230yrs = WTD_230yrs.sel(longitude=slice(1.475,2.814),latitude=slice(10.2,8.96))
#~ WTD_390yrs = WTD_390yrs.sel(longitude=slice(1.475,2.814),latitude=slice(10.2,8.96))
#~ WTD_750yrs = WTD_750yrs.sel(longitude=slice(1.475,2.814),latitude=slice(10.2,8.96))

WTD = WTD_1310yrs

"""Plots"""
fig,ax = plt.subplots(1,1,figsize = [14,7])
p = WTD.WTD.plot(ax=ax,vmin=0,vmax=60,cmap='terrain')
ax.set_aspect(1) 

fig,ax = plt.subplots(1,1,figsize = [14,7])
p = WTD_CLM_y12.WTD.plot(ax=ax,vmin=0,vmax=60,cmap='terrain')
ax.set_aspect(1) 

fig,ax = plt.subplots(1,1,figsize = [14,7])
WTD.WTD.plot.hist(ax=ax,bins=np.arange(0,60,1),density=True,histtype='step')   

fig,ax = plt.subplots(1,1,figsize = [14,7])
WTD_CLM_y12.WTD.plot.hist(ax=ax,bins=np.arange(0,60,1),density=True,histtype='step')   

""" Add Ks & PmET to the dataset """
WTD['Ks']=(('latitude','longitude'),Kslog[::-1,:])  
WTD['PmET']=(('latitude','longitude'),PmET[::-1,:])  

fig,ax = plt.subplots(1,1,figsize = [14,7])
p = WTD.PmET.plot(ax=ax,cmap='viridis_r')
p.colorbar.set_label('Yearly Recharge (mm)')  
ax.set_aspect(1) 
plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/Recharge_PmET_GLDAS_div50_CONWAspinup.png')

""" Calculate gradients"""
grdx,grdy = np.gradient(WTD.WTD.data)
WTD['grad'] = (('latitude','longitude'),np.sqrt(grdx*grdx+grdy*grdy)/1000)

grdx,grdy = np.gradient(WTD_143yrs.WTD.data)
WTD_143yrs['grad'] = (('latitude','longitude'),np.sqrt(grdx*grdx+grdy*grdy)/1000)
grdx,grdy = np.gradient(WTD_230yrs.WTD.data)
WTD_230yrs['grad'] = (('latitude','longitude'),np.sqrt(grdx*grdx+grdy*grdy)/1000)
grdx,grdy = np.gradient(WTD_390yrs.WTD.data)
WTD_390yrs['grad'] = (('latitude','longitude'),np.sqrt(grdx*grdx+grdy*grdy)/1000)
grdx,grdy = np.gradient(WTD_750yrs.WTD.data)
WTD_750yrs['grad'] = (('latitude','longitude'),np.sqrt(grdx*grdx+grdy*grdy)/1000)
grdx,grdy = np.gradient(WTD_1110yrs.WTD.data)
WTD_1110yrs['grad'] = (('latitude','longitude'),np.sqrt(grdx*grdx+grdy*grdy)/1000)
grdx,grdy = np.gradient(WTD_1310yrs.WTD.data)
WTD_1310yrs['grad'] = (('latitude','longitude'),np.sqrt(grdx*grdx+grdy*grdy)/1000)

fig,ax = plt.subplots(1,1,figsize = [14,7])
WTD.grad.plot.hist(ax=ax,bins=np.arange(0,0.04,0.0005),density=True,histtype='step')   

""" Include different time step of the spinup to plot the evolution"""
data = pd.DataFrame({'WTD':WTD.WTD.data.flatten(),'grad':WTD.grad.data.flatten(),'Ks':WTD.Ks.data.flatten(),'PmET':WTD.PmET.data.flatten()})
data['WTD_143yrs'] = WTD_143yrs.WTD.data.flatten()                                                                                         
data['WTD_230yrs'] = WTD_230yrs.WTD.data.flatten()                                                                                         
data['WTD_390yrs'] = WTD_390yrs.WTD.data.flatten()                                                                                         
data['WTD_750yrs'] = WTD_750yrs.WTD.data.flatten() 
data['WTD_1110yrs'] = WTD_1110yrs.WTD.data.flatten() 
data['WTD_1310yrs'] = WTD_1310yrs.WTD.data.flatten() 
#~ data['WTD_CLM'] = WTD_CLM_y12.WTD.data.flatten() 
data['WTD_CLM'] = WTD_CLM_y12.WTD.data[::-1,:].flatten() 
                                                                                       
data['grad_143yrs'] = WTD_143yrs.grad.data.flatten()                                                                                         
data['grad_230yrs'] = WTD_230yrs.grad.data.flatten()                                                                                         
data['grad_390yrs'] = WTD_390yrs.grad.data.flatten()                                                                                         
data['grad_750yrs'] = WTD_750yrs.grad.data.flatten() 
data['grad_1110yrs'] = WTD_1110yrs.grad.data.flatten() 
data['grad_1310yrs'] = WTD_1310yrs.grad.data.flatten() 
                                                                                       
#~ data['PmET_r'] = pd.cut(data['PmET'], [0, 5, 10, 15, 20, 25], labels=['0-5', '5-10', '10-15', '15-20','20-25']) # bin by WTHeight
#~ vec_sep = np.arange(0,18,2)
vec_sep = np.arange(0,1.8,0.2)
data['PmET_r'] = pd.cut(data['PmET'], vec_sep, labels=['%1.1f-%1.1f'%(x,vec_sep[i+1]) for i,x in enumerate(vec_sep) if i<len(vec_sep)-1] ) # bin by WTHeight
  
  
"""PLOT WTD violin /boxplot else WTD PF"""

fig,ax = plt.subplots(4,1,figsize = [10,14],sharex=True,sharey=True)

for i, ks in enumerate(np.delete(np.unique(Kslog),[1,2,3])):
    #~ ax[i].scatter(WTD.PmET.where(WTD.Ks==ks).data.flatten(),WTD.WTD.where(WTD.Ks==ks).data.flatten(),s=0.2,c='k')
    tmp = np.unique(data.loc[data['Ks']==ks,['PmET_r','WTD']].dropna(how = 'any').PmET_r, return_counts=True)
    #~ ax[i] = sns.boxplot(x="PmET_r", y="WTD", data=data.loc[data['Ks']==ks,:],showfliers=False, showmeans=True, palette="Set1",ax=ax[i])
    ax[i] = sns.violinplot(x="PmET_r", y="WTD", data=data.loc[data['Ks']==ks,:], palette="Set1",ax=ax[i])
    #~ ax[i] = sns.kdeplot(data.loc[data['Ks']==ks,'PmET'], data.loc[data['Ks']==ks,'WTD'], cmap="Blues", shade=True, shade_lowest=True, ax=ax[i])
    #~ ax[i].xlabel('latitude')
    #~ ax[i].ylabel('WTD (m)')
    #~ ax[i].set_xlim([0,27])
    ax[i].set_ylim([-10,75])
    ax[i].text(0,ax[i].get_ylim()[1]-0.2*ax[i].get_ylim()[1],'Ks = %.2E m/s'%(Decimal(ks/3600)),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.5})        
    fig.canvas.draw()
    xlabels = [t.get_text() for t in ax[i].get_xticklabels()]
    #~ for xindex in ax[i].get_xticks(): ax[i].text(xindex,ax[i].get_ylim()[1]-0.1*(ax[i].get_ylim()[1]-ax[i].get_ylim()[0]),str(tmp[1][list(tmp[0][:]).index(xlabels[xindex])]))    
    
    for xindex in ax[i].get_xticks(): ax[i].text(xindex,ax[i].get_ylim()[1]-0.1*(ax[i].get_ylim()[1]-ax[i].get_ylim()[0]),str(tmp[1][xindex]))    
    
    #~ ax[i].tick_params(axis='x', which='both', labelbottom=False, labeltop=False)
    #~ ax[0].tick_params(axis='both', which='both', bottom=False,top=True,right=False,left=True)
    ax[-1].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)

#~ plt.gcf().subplots_adjust(bottom=0.20, top =0.95, hspace=0.001,wspace=0.001)
plt.gcf().subplots_adjust(bottom=0.05, top =0.95,left=0.2, hspace=0.001,wspace=0.001)
plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup/WTD_1310yrs_vs_PmET_vs_Ks_CONWAspinup.png')

"""PLOT WTD violin /boxplot else WTD PF + CLM
distributions are different because river cells are not simulated in this PF version

"""

fig,ax = plt.subplots(4,1,figsize = [10,14],sharex=True,sharey=True)

for i, ks in enumerate(np.delete(np.unique(Kslog),[1,2,3])):
    tmp = np.unique(data.loc[data['Ks']==ks,['PmET_r','WTD_CLM']].dropna(how = 'any').PmET_r, return_counts=True)
    ax[i] = sns.violinplot(x="PmET_r", y="WTD_CLM", data=data.loc[data['Ks']==ks,:], palette="Set1",ax=ax[i])
    ax[i].set_ylim([-10,75])
    ax[i].text(0,ax[i].get_ylim()[1]-0.2*ax[i].get_ylim()[1],'Ks = %.2E m/s'%(Decimal(ks/3600)),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.5})        
    fig.canvas.draw()
    xlabels = [t.get_text() for t in ax[i].get_xticklabels()]    
    for xindex in ax[i].get_xticks(): ax[i].text(xindex,ax[i].get_ylim()[1]-0.1*(ax[i].get_ylim()[1]-ax[i].get_ylim()[0]),str(tmp[1][xindex]))    
    ax[-1].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)

plt.gcf().subplots_adjust(bottom=0.05, top =0.95,left=0.2, hspace=0.001,wspace=0.001)
plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_CLM_y12_vs_PmET_vs_Ks_CONWAspinup.png')

  
"""PLOT Gradient violin /boxplot else"""
fig,ax = plt.subplots(4,1,figsize = [10,14],sharex=True,sharey=True)

for i, ks in enumerate(np.delete(np.unique(Kslog),[1,2,3])):
    tmp = np.unique(data.loc[data['Ks']==ks,['PmET_r','grad']].dropna(how = 'any').PmET_r, return_counts=True)
    ax[i] = sns.violinplot(x="PmET_r", y="grad", data=data.loc[data['Ks']==ks,:], palette="Set1",ax=ax[i])
    ax[i].set_ylim([-0.001,0.019])
    ax[i].text(0,ax[i].get_ylim()[1]-0.2*ax[i].get_ylim()[1],'Ks = %.2E m/s'%(Decimal(ks/3600)),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.5})        
    fig.canvas.draw()
    xlabels = [t.get_text() for t in ax[i].get_xticklabels()]
    #~ for xindex in ax[i].get_xticks(): ax[i].text(xindex,ax[i].get_ylim()[1]-0.1*(ax[i].get_ylim()[1]-ax[i].get_ylim()[0]),str(tmp[1][list(tmp[0][:]).index(xlabels[xindex])]))    
    
    for xindex in ax[i].get_xticks(): ax[i].text(xindex,ax[i].get_ylim()[1]-0.1*(ax[i].get_ylim()[1]-ax[i].get_ylim()[0]),str(tmp[1][xindex]))    
    
    #~ ax[i].tick_params(axis='x', which='both', labelbottom=False, labeltop=False)
    #~ ax[0].tick_params(axis='both', which='both', bottom=False,top=True,right=False,left=True)
    ax[-1].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)

#~ plt.gcf().subplots_adjust(bottom=0.20, top =0.95, hspace=0.001,wspace=0.001)
plt.gcf().subplots_adjust(bottom=0.05, top =0.95,left=0.2, hspace=0.001,wspace=0.001)
plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup/grad_vs_PmET_vs_Ks_CONWAspinup.png')


"""PLOT WTD mean"""

fig,ax = plt.subplots(4,1,figsize = [10,14],sharex=True,sharey=True)

for i, ks in enumerate(np.delete(np.unique(Kslog),[1,2,3])):
    #~ ax[i].scatter(WTD.PmET.where(WTD.Ks==ks).data.flatten(),WTD.WTD.where(WTD.Ks==ks).data.flatten(),s=0.2,c='k')
    tmp = np.unique(data.loc[data['Ks']==ks,['PmET_r','WTD']].dropna(how = 'any').PmET_r, return_counts=True)
    ax[i] = data.loc[data['Ks']==ks,:].groupby('PmET_r').mean()['WTD_143yrs'].plot(ax=ax[i],marker='+',c='r',label='WTD_143yrs')
    ax[i] = data.loc[data['Ks']==ks,:].groupby('PmET_r').mean()['WTD_230yrs'].plot(ax=ax[i],marker='+',c='m',label='WTD_230yrs')
    ax[i] = data.loc[data['Ks']==ks,:].groupby('PmET_r').mean()['WTD_390yrs'].plot(ax=ax[i],marker='+',c='c',label='WTD_390yrs')
    ax[i] = data.loc[data['Ks']==ks,:].groupby('PmET_r').mean()['WTD_750yrs'].plot(ax=ax[i],marker='+',c='b',label='WTD_750yrs')
    ax[i] = data.loc[data['Ks']==ks,:].groupby('PmET_r').mean()['WTD_1110yrs'].plot(ax=ax[i],marker='+',c='k',label='WTD_1110yrs')

    ax[i].legend()
    #~ ax[i].set_ylim([-10,75])
    ax[i].text(0,ax[i].get_ylim()[1]-0.2*ax[i].get_ylim()[1],'Ks = %.2E m/s'%(Decimal(ks/3600)),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.5})        
    fig.canvas.draw()
    xlabels = [t.get_text() for t in ax[i].get_xticklabels()]
    #~ for xindex in ax[i].get_xticks(): ax[i].text(xindex,ax[i].get_ylim()[1]-0.1*(ax[i].get_ylim()[1]-ax[i].get_ylim()[0]),str(tmp[1][list(tmp[0][:]).index(xlabels[xindex])]))   
    ax[-1].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)

#~ plt.gcf().subplots_adjust(bottom=0.20, top =0.95, hspace=0.001,wspace=0.001)
plt.gcf().subplots_adjust(bottom=0.05, top =0.95,left=0.2, hspace=0.001,wspace=0.001)


plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/WTD_vs_PmET_vs_Ks_CONWAspinup_mean.png')
"""PLOT grad mean"""

fig,ax = plt.subplots(4,1,figsize = [10,14],sharex=True,sharey=True)

for i, ks in enumerate(np.delete(np.unique(Kslog),[1,2,3])):
    #~ ax[i].scatter(WTD.PmET.where(WTD.Ks==ks).data.flatten(),WTD.WTD.where(WTD.Ks==ks).data.flatten(),s=0.2,c='k')
    tmp = np.unique(data.loc[data['Ks']==ks,['PmET_r','WTD']].dropna(how = 'any').PmET_r, return_counts=True)
    ax[i] = data.loc[data['Ks']==ks,:].groupby('PmET_r').mean()['grad_143yrs'].plot(ax=ax[i],marker='+',c='r',label='grad_143yrs')
    ax[i] = data.loc[data['Ks']==ks,:].groupby('PmET_r').mean()['grad_230yrs'].plot(ax=ax[i],marker='+',c='m',label='grad_230yrs')
    ax[i] = data.loc[data['Ks']==ks,:].groupby('PmET_r').mean()['grad_390yrs'].plot(ax=ax[i],marker='+',c='c',label='grad_390yrs')
    ax[i] = data.loc[data['Ks']==ks,:].groupby('PmET_r').mean()['grad_750yrs'].plot(ax=ax[i],marker='+',c='b',label='grad_750yrs')
    ax[i] = data.loc[data['Ks']==ks,:].groupby('PmET_r').mean()['grad_1110yrs'].plot(ax=ax[i],marker='+',c='k',label='grad_1110yrs')

    ax[i].legend()
    #~ ax[i].set_ylim([-10,75])
    ax[i].text(0,ax[i].get_ylim()[1]-0.2*ax[i].get_ylim()[1],'Ks = %.2E m/s'%(Decimal(ks/3600)),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.5})        
    fig.canvas.draw()
    xlabels = [t.get_text() for t in ax[i].get_xticklabels()]
    #~ for xindex in ax[i].get_xticks(): ax[i].text(xindex,ax[i].get_ylim()[1]-0.1*(ax[i].get_ylim()[1]-ax[i].get_ylim()[0]),str(tmp[1][list(tmp[0][:]).index(xlabels[xindex])]))   
    ax[-1].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)

#~ plt.gcf().subplots_adjust(bottom=0.20, top =0.95, hspace=0.001,wspace=0.001)
plt.gcf().subplots_adjust(bottom=0.05, top =0.95,left=0.2, hspace=0.001,wspace=0.001)


plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/grad_vs_PmET_vs_Ks_CONWAspinup_mean.png')



"""Read Drill logs database"""
DB=pd.read_csv(r'/home/hectorb/DATA/Aquifers/HardRock/processed_DB/with_aux_variables/DB_tmp.csv')

"""clean up a little"""
DB.loc[DB.NS>=200,'NS'] = np.nan
DB.loc[DB.alt>=200,'alt'] = np.nan
DB.loc[DB.NS<=0,'NS'] = np.nan
DB.loc[DB.geology == 'others (sed/surf/nodata)',:] = np.nan
DB.loc[DB.country=='Niger','Q'] = DB.loc[DB.country=='Niger','Q'].apply(lambda x: x*3600) #m3/s -> m3/hr

DB['latlon'] = [t for t in zip(DB.lat,DB.lon)]
#many points are duplicated... see:
np.unique(DB.loc[DB.duplicated(subset=['latlon'],keep=False)==True,'country'],return_counts=True)
# if those points need be dropped:
#~ DB.drop_duplicates(subset=['latlon'],keep=False,inplace = True)   

#drop points with no location
DB.dropna(subset=['x','y'],inplace=True)
DB.drop(index=DB.loc[DB.country=="Zinder",:].index,inplace=True)  


DB.loc[:,'WTHeight'] = DB.loc[:,'Z'] - DB.loc[:,'NS']
DB['ZA_saturee'] = DB['alt'] - DB['NS']

"""Add CONWA IO """
projAEA = pyproj.Proj("+proj=aea +lat_1=16.12 +lat_2=6.58 +lat_0=11.35 +lon_0=-4.3 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs")
projUTM = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
xAEA,yAEA = pyproj.transform(projUTM,projAEA,np.array(DB.loc[:,'x']), np.array(DB.loc[:,'y']))
DB.loc[:,'PmET_CONWA'] = findNN(PmET,xx.flatten(), yy.flatten(),xAEA,yAEA)
DB.loc[:,'Ks_CONWA'] = findNN(Kslog,xx.flatten(), yy.flatten(),xAEA,yAEA)


"""MAP TO CHECK"""
plt.figure(num=None, figsize=(8,4), dpi=250, facecolor='w', edgecolor='k')
ax1 =plt.subplot(111)
cmap0,norm0,bounds0 = prepare_colormap(cmapname=plt.cm.viridis,bound_low=0,bound_high=50)
p2=ax1.scatter(DB['x'],DB['y'],c=DB['PmET_CONWA'],cmap=cmap0, norm=norm0,s=6.3,edgecolors='k',linewidths=0.4)
ax1.set_aspect(1)
cb = plt.colorbar(p2,orientation='horizontal')
cb.set_label('PmET (mm)',fontsize=7)


#~ vec_sep = np.arange(0,18,2)
vec_sep = np.arange(0,1.8,0.2)
DB['PmET_range'] = pd.cut(DB['PmET_CONWA'], vec_sep, labels=['%1.1f-%1.1f'%(x,vec_sep[i+1]) for i,x in enumerate(vec_sep) if i<len(vec_sep)-1] ) # bin by WTHeight
  


plt.figure(figsize=[10,8])
#For Benin bf
#~ ax = sns.boxplot(x="P_range2", y="NS", data=DB.loc[:,:],showfliers=False, showmeans=True, palette="Set1")
#including CI
ax = sns.boxplot(x="PmET_range", y="NS", data=DB.loc[:,:],showfliers=False, showmeans=True, palette="Set1")
# try to put easily the number of points
#~ tmp = np.unique(DB.loc[:,['P_range2','NS']].dropna(how = 'any').P_range2, return_counts=True)
tmp = np.unique(DB.loc[:,['PmET_range','NS']].dropna(how = 'any').PmET_range, return_counts=True)
xlabels = [t.get_text() for t in ax.get_xticklabels()]
for xindex in ax.get_xticks(): ax.text(xindex,ax.get_ylim()[1]-0.1*(ax.get_ylim()[1]-ax.get_ylim()[0]),str(tmp[1][list(tmp[0][:]).index(xlabels[xindex])]))    
plt.xticks(rotation=45)
plt.xlabel('PmET_range (mm)')
plt.ylabel('WTD (m)')
plt.gcf().subplots_adjust(bottom=0.20, top =0.95, hspace=0.001,wspace=0.001)
for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] + ax.get_xticklabels() + ax.get_yticklabels()):item.set_fontsize(14)
#~ plt.savefig(fig_outs+'NS_vs_P_range'+fig_suff+'.png')

""" COMPARE data vs ouputs PF """
fig,ax = plt.subplots(2,1,figsize = [10,7],sharex=True,sharey=True)

tmp0 = np.unique(data.loc[data['Ks']==np.unique(data['Ks'])[4],['PmET_r','WTD']].dropna(how = 'any').PmET_r, return_counts=True)
tmp1 = np.unique(DB.loc[DB['Ks_CONWA']==np.unique(data['Ks'])[4],['PmET_range','NS']].dropna(how = 'any').PmET_range, return_counts=True)
    #~ ax[i] = sns.boxplot(x="PmET_r", y="WTD", data=data.loc[data['Ks']==ks,:],showfliers=False, showmeans=True, palette="Set1",ax=ax[i])
ax[0] = sns.violinplot(x="PmET_r", y="WTD", data=data.loc[data['Ks']==np.unique(data['Ks'])[4],:], palette="Set1",ax=ax[0])
ax[1] = sns.violinplot(x="PmET_range", y="NS", data=DB.loc[DB['Ks_CONWA']==np.unique(data['Ks'])[4],:], palette="Set1",ax=ax[1])

ax[0] = data.loc[data['Ks']==np.unique(data['Ks'])[4],:].groupby('PmET_r').mean()['WTD_1310yrs'].plot(ax=ax[0],marker='+',c='k',label='WTD_1310yrs')
ax[1] = DB.loc[DB['Ks_CONWA']==np.unique(data['Ks'])[4],:].groupby('PmET_range').mean()['NS'].plot(ax=ax[1],marker='+',c='k',label='WTD')


ax[0].set_ylim([-5,55])
ax[1].set_ylim([-5,55])
ax[1].set_xlabel('Spinup forcing: scaled (P - ET) range (mm)')
ax[0].set_ylabel('simuletd WTD (m)')
ax[1].set_ylabel('observed WTD (m)')

fig.canvas.draw()
xlabels0 = [t.get_text() for t in ax[0].get_xticklabels()]
xlabels1 = [t.get_text() for t in ax[1].get_xticklabels()]
for xindex in ax[0].get_xticks(): ax[0].text(xindex,ax[0].get_ylim()[1]-0.1*(ax[0].get_ylim()[1]-ax[0].get_ylim()[0]),str(tmp0[1][xindex]))    
for xindex in ax[1].get_xticks(): ax[1].text(xindex,ax[1].get_ylim()[1]-0.1*(ax[1].get_ylim()[1]-ax[1].get_ylim()[0]),str(tmp1[1][xindex]))    
    
    #~ ax[i].tick_params(axis='x', which='both', labelbottom=False, labeltop=False)
    #~ ax[0].tick_params(axis='both', which='both', bottom=False,top=True,right=False,left=True)
ax[1].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)

plt.gcf().subplots_adjust(bottom=0.1, top =0.95,left=0.2, hspace=0.001,wspace=0.001)
plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup/WTD_vs_PmET_CONWAspinup1310yrs_hardrock sim_vs_obs.png')

""" COMPARE data vs ouputs PF+CLM """
fig,ax = plt.subplots(2,1,figsize = [10,7],sharex=True,sharey=True)

tmp0 = np.unique(data.loc[data['Ks']==np.unique(data['Ks'])[4],['PmET_r','WTD_CLM']].dropna(how = 'any').PmET_r, return_counts=True)
tmp1 = np.unique(DB.loc[DB['Ks_CONWA']==np.unique(data['Ks'])[4],['PmET_range','NS']].dropna(how = 'any').PmET_range, return_counts=True)
    #~ ax[i] = sns.boxplot(x="PmET_r", y="WTD", data=data.loc[data['Ks']==ks,:],showfliers=False, showmeans=True, palette="Set1",ax=ax[i])
ax[0] = sns.violinplot(x="PmET_r", y="WTD_CLM", data=data.loc[data['Ks']==np.unique(data['Ks'])[4],:], palette="Set1",ax=ax[0])
ax[1] = sns.violinplot(x="PmET_range", y="NS", data=DB.loc[DB['Ks_CONWA']==np.unique(data['Ks'])[4],:], palette="Set1",ax=ax[1])

ax[0] = data.loc[data['Ks']==np.unique(data['Ks'])[4],:].groupby('PmET_r').mean()['WTD_CLM'].plot(ax=ax[0],marker='+',c='k',label='WTD')
ax[1] = DB.loc[DB['Ks_CONWA']==np.unique(data['Ks'])[4],:].groupby('PmET_range').mean()['NS'].plot(ax=ax[1],marker='+',c='k',label='WTD')


ax[0].set_ylim([-5,55])
ax[1].set_ylim([-5,55])
ax[1].set_xlabel('Spinup forcing: scaled (P - ET) range (mm)')
ax[0].set_ylabel('simuletd WTD (m)')
ax[1].set_ylabel('observed WTD (m)')

fig.canvas.draw()
xlabels0 = [t.get_text() for t in ax[0].get_xticklabels()]
xlabels1 = [t.get_text() for t in ax[1].get_xticklabels()]
for xindex in ax[0].get_xticks(): ax[0].text(xindex,ax[0].get_ylim()[1]-0.1*(ax[0].get_ylim()[1]-ax[0].get_ylim()[0]),str(tmp0[1][xindex]))    
for xindex in ax[1].get_xticks(): ax[1].text(xindex,ax[1].get_ylim()[1]-0.1*(ax[1].get_ylim()[1]-ax[1].get_ylim()[0]),str(tmp1[1][xindex]))    
    
    #~ ax[i].tick_params(axis='x', which='both', labelbottom=False, labeltop=False)
    #~ ax[0].tick_params(axis='both', which='both', bottom=False,top=True,right=False,left=True)
ax[1].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)

plt.gcf().subplots_adjust(bottom=0.1, top =0.95,left=0.2, hspace=0.001,wspace=0.001)
plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_y12_vs_PmET_hardrock sim_vs_obs.png')


""" COMPARE data vs ouputs PF """
fig,ax = plt.subplots(1,1,figsize = [7,4],sharex=True,sharey=True)
ax = data.loc[data['Ks']==np.unique(data['Ks'])[4],:].groupby('PmET_r').mean()['WTD_1310yrs'].plot(ax=ax,marker='o',c='r',label='mean: sim')
ax = DB.loc[DB['Ks_CONWA']==np.unique(data['Ks'])[4],:].groupby('PmET_range').mean()['NS'].plot(ax=ax,marker='o',c='k',label='mean: obs')
#~ ax = data.loc[data['Ks']==np.unique(data['Ks'])[4],:].groupby('PmET_r').median()['WTD_1110yrs'].plot(ax=ax,style='--',marker='o',c='r',label='median :sim')
#~ ax = DB.loc[DB['Ks_CONWA']==np.unique(data['Ks'])[4],:].groupby('PmET_range').median()['NS'].plot(ax=ax,style='--',marker='o',c='k',label='median: obs')
ax.tick_params(axis='x', which='both', labelbottom=True, labeltop=False)
ax.legend()
plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup/WTD_vs_PmET_CONWAspinup1310yrs_hardrock sim_vs_obs_mean.png')

fig,ax = plt.subplots(1,1,figsize = [7,4],sharex=True,sharey=True)
#~ ax = data.loc[data['Ks']==np.unique(data['Ks'])[4],:].groupby('PmET_r').mean()['WTD_1110yrs'].plot(ax=ax,marker='+',c='r',label='mean: sim')
#~ ax = DB.loc[DB['Ks_CONWA']==np.unique(data['Ks'])[4],:].groupby('PmET_range').mean()['NS'].plot(ax=ax,marker='+',c='k',label='mean: obs')
ax = data.loc[data['Ks']==np.unique(data['Ks'])[4],:].groupby('PmET_r').median()['WTD_1310yrs'].plot(ax=ax,marker='o',c='r',label='median :sim')
ax = DB.loc[DB['Ks_CONWA']==np.unique(data['Ks'])[4],:].groupby('PmET_range').median()['NS'].plot(ax=ax,marker='o',c='k',label='median: obs')
ax.tick_params(axis='x', which='both', labelbottom=True, labeltop=False)
ax.legend()
plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup/WTD_vs_PmET_CONWAspinup1310yrs_hardrock sim_vs_obs_median.png')

""" COMPARE data vs ouputs PF +CLM """
fig,ax = plt.subplots(1,1,figsize = [7,4],sharex=True,sharey=True)
ax = data.loc[data['Ks']==np.unique(data['Ks'])[4],:].groupby('PmET_r').mean()['WTD_CLM'].plot(ax=ax,marker='o',c='r',label='mean: sim')
ax = DB.loc[DB['Ks_CONWA']==np.unique(data['Ks'])[4],:].groupby('PmET_range').mean()['NS'].plot(ax=ax,marker='o',c='k',label='mean: obs')
#~ ax = data.loc[data['Ks']==np.unique(data['Ks'])[4],:].groupby('PmET_r').median()['WTD_1110yrs'].plot(ax=ax,style='--',marker='o',c='r',label='median :sim')
#~ ax = DB.loc[DB['Ks_CONWA']==np.unique(data['Ks'])[4],:].groupby('PmET_range').median()['NS'].plot(ax=ax,style='--',marker='o',c='k',label='median: obs')
ax.tick_params(axis='x', which='both', labelbottom=True, labeltop=False)
ax.legend()
plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_y12_vs_PmET_hardrock sim_vs_obs_mean.png')

fig,ax = plt.subplots(1,1,figsize = [7,4],sharex=True,sharey=True)
#~ ax = data.loc[data['Ks']==np.unique(data['Ks'])[4],:].groupby('PmET_r').mean()['WTD_1110yrs'].plot(ax=ax,marker='+',c='r',label='mean: sim')
#~ ax = DB.loc[DB['Ks_CONWA']==np.unique(data['Ks'])[4],:].groupby('PmET_range').mean()['NS'].plot(ax=ax,marker='+',c='k',label='mean: obs')
ax = data.loc[data['Ks']==np.unique(data['Ks'])[4],:].groupby('PmET_r').median()['WTD_CLM'].plot(ax=ax,marker='o',c='r',label='median :sim')
ax = DB.loc[DB['Ks_CONWA']==np.unique(data['Ks'])[4],:].groupby('PmET_range').median()['NS'].plot(ax=ax,marker='o',c='k',label='median: obs')
ax.tick_params(axis='x', which='both', labelbottom=True, labeltop=False)
ax.legend()
plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/WTD_y12_vs_PmET_hardrock sim_vs_obs_median.png')

