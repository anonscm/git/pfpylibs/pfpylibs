#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    script to extract simulated streamflow for the CONWA domain 
    at location of observed streamflow database. 
    
    quick comparison 
    
    The matching between real gauges locations and PF domain must be done ahead,
    eg using find_station_location_on_PF_grid.py

    compare eg with ADHI:
    Tramblay, Y., Rouché, N., Paturel, J.-E., Mahé, G., Boyer, J.-F., 
    Amoussou, E., Bodian, A., Dacosta, H., Dakhlaoui, H., Dezetter, A., 
    Hughes, D., Hanich, L., Peugeot, C., Tshimanga, R. and Lachassagne, P.: 
    The African Database of Hydrometric Indices (ADHI), 
    Earth System Science Data Discussions, 1–21, 
    https://doi.org/10.5194/essd-2020-281, 2020.


    @copyright: 2021 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2021"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import xarray as xr
import datetime,os,time,sys
from calendar import month_abbr

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

# domain_setup dir: directory where to save outputs relative to the specific grid:
# faro:
domain_setup_dir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/eval/streamflow/domain_setups/CONWA_AD/'

#data_loc_dir is where the OUTPUT, formatted streamflow data LOCATION is stored:
# (if applicable: this preproc can be done elsewhere)
data_loc_dir = os.sep.join([domain_setup_dir,'data_loc/'])

#data_dir : where the streamflow observation data can be found:
#faro
data_dir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/eval/streamflow/data/'

# specific simulation dir for adding up simulation outputs:
simulation_name = 'fullyear1'
sim_dir = os.sep.join([domain_setup_dir,'simulations/%s/'%simulation_name])

#simulation_specific figures:
fig_dir = os.sep.join([sim_dir,'figures/'])
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    from https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console
    
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(round(length * iteration // total))
    #~ bar = fill * filledLength + '-' * (length - filledLength)
    bar = '=' * filledLength + '>' + '-' * (length - filledLength)
    #~ print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    #~ print('\r%s |%s| %s%% %s'.encode(sys.stdout.encoding, errors='replace')%(prefix,bar,percent,suffix), end = printEnd)
    #~ print('\r%s |%s| %s%% %s'.encode('utf-8')%(prefix,bar,percent,suffix), end = printEnd)
    #~ print('\r%s |%s| %s%% %s'%(prefix,bar,percent,suffix), end = printEnd)
    print('\r%s |%s| %s%% %s'%(prefix,bar,percent,suffix), end = printEnd)
    # Print New Line on Complete
    if iteration == total: 
        print()

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##


"""Load and process ADHI DB"""
#read in stations:
#~ ADHI = pd.read_csv('/home/hectorb/DATA/streamflow/ADHI/ADHI/ADHI_PF.csv',sep='\t')
ADHI = pd.read_csv(data_loc_dir+'/ADHI/ADHI_PF.csv',sep='\t')
ADHI.set_index(ADHI.Id,inplace=True)
shortname={'Benin':'Be', 'Burkina Faso':'BF', "Cote d'Ivoire":'CI', 'Ghana':'Gh', 'Guinea':'Gu',
       'Guinea-Bissau':'GB', 'Liberia':'Li', 'Mali':'Ma', 'Niger':'Ni', 'Nigeria':'Nia', 'Senegal':'Se',
       'Togo':'To'}
ADHI['Country_short']=ADHI.Country.apply(lambda x: shortname[x]) 

#read in data:
#~ data_ADHI = pd.read_csv('/home/hectorb/DATA/streamflow/ADHI/ADHI/ADHI_summary.tab',sep='\t')
data_ADHI = pd.read_csv(data_dir+'/ADHI/ADHI_summary.tab',sep='\t')
data_ADHI=data_ADHI.loc[data_ADHI.Id.isin(ADHI.index),:]
data_ADHI.reset_index(inplace=True)
data_ADHI = data_ADHI.set_index('Id')

data_ADHI2 = data_ADHI.iloc[:,5:17].T
data_ADHI2.index=[pd.datetime(2015,i,15) for i in np.arange(1,13,1)] 
data_ADHI_sort = data_ADHI.sort_values(by='Mean_daily_streamflow',ascending=False)


"""Load and process SIEREM DB"""
#read in stations:
#~ SIEREM = pd.read_csv('/home/hectorb/DATA/streamflow/Afrique_Sierem/SIEREM_PF.csv',sep='\t')
SIEREM = pd.read_csv(data_loc_dir+'/SIEREM/SIEREM_PF.csv',sep='\t')
SIEREM.set_index('Id',inplace=True)
shortname2={"Cote d'Ivoire":'CI', "Côte d'Ivoire":'CI', 'Guinée':'Gu', 'Mali':'Ma', 'Sénégal':'Se'}
SIEREM['Country_short']=SIEREM.Pays.apply(lambda x: shortname2[x]) 
SIEREM.drop_duplicates(subset=['Name'],inplace=True)
#read in data:
#~ data_SIEREM = pd.read_csv('/home/hectorb/DATA/streamflow/Afrique_Sierem/SIEREM_data.csv')
#~ data_SIEREM_id = pd.read_csv('/home/hectorb/DATA/streamflow/Afrique_Sierem/SIEREM_data_id.csv')
data_SIEREM = pd.read_csv(data_dir+'/SIEREM/SIEREM_data.csv')
data_SIEREM_id = pd.read_csv(data_dir+'/SIEREM/SIEREM_data_id.csv')

data_SIEREM.set_index(data_SIEREM.columns[0],inplace=True)
data_SIEREM = data_SIEREM.set_index(pd.to_datetime(data_SIEREM.index)).rename_axis('date')

data_SIEREM_id.set_index(data_SIEREM_id.columns[0],inplace=True)    
data_SIEREM_id = data_SIEREM_id.set_index(pd.to_datetime(data_SIEREM_id.index)).rename_axis('date')


data_SIEREM_id_month = data_SIEREM_id.groupby(data_SIEREM_id.index.month).mean()  
data_SIEREM_id_month.index=[pd.datetime(2015,i,15) for i in np.arange(1,13,1)] 
data_SIEREM_month = data_SIEREM.groupby(data_SIEREM.index.month).mean()  
data_SIEREM_month.index=[pd.datetime(2015,i,15) for i in np.arange(1,13,1)] 

""" Load and process AC Benin DB """
#read in stations:
#~ AC_Benin = pd.read_csv('/home/hectorb/DATA/streamflow/AMMA_Benin/AC_Benin.csv',sep='\t')
AC_Benin = pd.read_csv(data_loc_dir+'/AC_Benin/AC_Benin_PF.csv',sep='\t')
AC_Benin.set_index(AC_Benin.columns[0],inplace=True)

#read in data:
data_AC = pd.read_csv(data_dir+'/AC_Benin/Oueme_Q.csv')

data_AC.set_index(data_AC.columns[0],inplace=True)
data_AC = data_AC.set_index(pd.to_datetime(data_AC.index)).rename_axis('date')
data_AC.sort_index(inplace=True)

"""Load Simulation output: """
#read in a netcdf
#~ Q1 = '/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/evaptranssum_y14_vcorr2_monthly.nc'
nc_rootdir = '/homelocal5/phyrev/_PARFLOW/projects/CONWA/active/CONWA_AD/Joliot_curie/post_spinup/results_fullsim1/'

ADHI_PF=pd.DataFrame()
SIEREM_PF=pd.DataFrame()
AC_PF=pd.DataFrame()

for i in np.arange(12)+1:
    nc_file = os.sep.join([nc_rootdir,'Q_%d_nc4.nc'%i])
    print('read file: %s'%nc_file)
    d = xr.open_dataset(nc_file)
    print('read file: ok')    

    print('extract Q for ADHI stations')
    # Initial call to print 0% progress
    printProgressBar(0, len(ADHI), prefix = 'Progress:', suffix = 'Complete', length = 20)   
    progress=0
    ADHI_PFtmp=pd.DataFrame()
    for index, row in ADHI.iterrows():
        printProgressBar(progress + 1, len(ADHI), prefix = 'Progress:', suffix = 'Complete', length = 20)        
        progress+=1
        ADHI_PFtmp = pd.concat([ADHI_PFtmp,d.sel(x=row.xStream,y=row.yStream).streamflow.to_series().rename(index)],axis=1)
    ADHI_PF = pd.concat([ADHI_PF,ADHI_PFtmp],axis=0)
    print('extract Q for ADHI stations: ok')

    print('extract Q for SIEREM stations')        
    SIEREM_PFtmp=pd.DataFrame()    
    for index, row in SIEREM.iterrows():
        SIEREM_PFtmp = pd.concat([SIEREM_PFtmp,d.sel(x=row.xStream,y=row.yStream).streamflow.to_series().rename(index)],axis=1)
    SIEREM_PF = pd.concat([SIEREM_PF,SIEREM_PFtmp],axis=0)        
    print('extract Q for SIEREM stations: ok')  

    print('extract Q for AC stations')        
    AC_PFtmp=pd.DataFrame()    
    for index, row in AC_Benin.iterrows():
        AC_PFtmp = pd.concat([AC_PFtmp,d.sel(x=row.xStream,y=row.yStream).streamflow.to_series().rename(index)],axis=1)
    AC_PF = pd.concat([AC_PF,AC_PFtmp],axis=0)        
    print('extract Q for AC stations: ok')  
    
          

#~ ADHI_PF.to_csv(nc_rootdir+'PF_Q_ADHI.csv')
#~ SIEREM_PF.to_csv(nc_rootdir+'PF_Q_SIEREM.csv')
#~ AC_PF.to_csv(nc_rootdir+'PF_Q_AC.csv')

ADHI_PF.to_csv(sim_dir+'/simQ/PF_Q_ADHI.csv')
SIEREM_PF.to_csv(sim_dir+'/simQ/PF_Q_SIEREM.csv')
AC_PF.to_csv(sim_dir+'/simQ/PF_Q_AC.csv')


"""Compare Simulation and ADHI DB """

#~ ADHI_PF=pd.DataFrame()
#~ for index, row in ADHI.iterrows():
    #~ ADHI_PF = pd.concat([ADHI_PF,d.sel(x=row.xStream,y=row.yStream).evaptranssum.to_series().rename(index)],axis=1)

#tmp?
#~ ADHI_PF.index = ADHI_PF.index.to_series().apply(lambda x: pd.datetime(2015,x,15))  

#~ ax = data2.plot(subplots=True,sharex=True,layout=(20,22),legend = False,style='k')     
#~ ax2 = ADHI_PF.plot(ax=ax,subplots=True,sharex=True,layout=(20,22),legend = False,style='r')     
#~ plt.subplots_adjust(hspace = 0,wspace = 0)

""" Figure: plot all climatologies and data"""
fig, ax = plt.subplots(ncols = 21, nrows = 21,sharex = True,figsize=(16,10))
for i,a in enumerate(ax.flatten()[0:len(ADHI)]):
    p1 = a.plot(data_ADHI2.iloc[:,i],'k')
    #~ p2 = a.plot(ADHI_PF.iloc[:,i],'r')
    p2 = a.plot(ADHI_PF.iloc[:,i].resample('M').mean(),'r')
    a.text(0.1,0.8,ADHI.loc[ADHI.index[i],'Name'][0:8]+'\n%s'%ADHI.loc[ADHI.index[i],'Country_short'],verticalalignment = 'top',transform = a.transAxes,FontSize=5)
    a.legend(handles=p1+p2,labels = ('obs','sim'),ncol=1,loc='upper right',framealpha=0.5,fontsize=5)
    if i!=0:
        a.get_legend().remove()
for i,a in enumerate(ax.flatten()):
    a.set_xticks([datetime.datetime(2015,1,1),datetime.datetime(2015,4,1),datetime.datetime(2015,7,1),datetime.datetime(2015,10,1)])
    a.set_xticklabels([month_abbr[datetime.datetime(2015,1,1).month],month_abbr[datetime.datetime(2015,4,1).month],month_abbr[datetime.datetime(2015,7,1).month],month_abbr[datetime.datetime(2015,10,1).month]],rotation = 45, ha="right",fontsize=7)
    a.set_yticks([])
    a.set_ylabel('rel. Q')
    a.label_outer()
plt.subplots_adjust(hspace = 0,wspace = 0)

#~ plt.savefig('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/outputs/spinup_CLM/Joliot_curie/post_spinup/tmp_streamflow.png')
#~ plt.savefig(nc_rootdir+'ADHI_streamflow.png')
#~ plt.savefig(nc_rootdir+'ADHI_streamflow_month.png')
plt.savefig(fig_dir+'ADHI_streamflow_month.png')

""" Figure: plot mean daily flow """
#~ data_sort.loc[:,'Mean_daily_streamflow'].plot(kind='bar',yerr=data_sort.Standard_deviation_daily_streamflow)
fig, ax = plt.subplots(1,1)
data_ADHI_sort.loc[:,'Mean_daily_streamflow'].plot(kind='bar',log=True,ax=ax)
ADHI_PF.mean().loc[data_ADHI_sort.index].plot(style ='r+',logy=True,ax=ax)
ax.set_ylabel('log(Q) ($m^3.s^{-1}$)')
#~ plt.savefig(nc_rootdir+'ADHI_mean_daily_streamflow2.png')
plt.savefig(fig_dir+'ADHI_mean_daily_streamflow2.png')



#m3/s -> mm/yr
data_ADHI_sort['daily_mm'] = data_ADHI_sort['Mean_daily_streamflow']*3600*24*365*1000/(ADHI['Catchment area (km2)']*1000*1000)
data_ADHI_sort['daily_mm'].plot(kind='bar')


"""Compare Simulation and SIEREM DB """
#~ SIEREM_PF=pd.DataFrame()
#~ for index, row in SIEREM.iterrows():
    #~ SIEREM_PF = pd.concat([SIEREM_PF,d.sel(x=row.xStream,y=row.yStream).evaptranssum.to_series().rename(index)],axis=1)

#tmp?
#~ SIEREM_PF.index = SIEREM_PF.index.to_series().apply(lambda x: pd.datetime(2015,x,15))  


#~ ax = data2.plot(subplots=True,sharex=True,layout=(20,22),legend = False,style='k')     
#~ ax2 = ADHI_PF.plot(ax=ax,subplots=True,sharex=True,layout=(20,22),legend = False,style='r')     
#~ plt.subplots_adjust(hspace = 0,wspace = 0)

""" Figure: plot all climatologies and data"""
fig, ax = plt.subplots(ncols = 10, nrows = 11,sharex = True,figsize=(16,10))
for i,a in enumerate(ax.flatten()[0:len(SIEREM)]):
    p1 = a.plot(data_SIEREM_id_month.iloc[:,i],'k')
    p2 = a.plot(SIEREM_PF.iloc[:,i],'r')
    a.text(0.1,0.8,SIEREM.loc[SIEREM.index[i],'Name'][0:8]+'\n%s'%SIEREM.loc[SIEREM.index[i],'Country_short'],verticalalignment = 'top',transform = a.transAxes,FontSize=5)
    a.legend(handles=p1+p2,labels = ('obs','sim'),ncol=1,loc='upper right',framealpha=0.5,fontsize=5)
    if i!=0:
        a.get_legend().remove()
for i,a in enumerate(ax.flatten()):
    a.set_xticks([datetime.datetime(2015,1,1),datetime.datetime(2015,4,1),datetime.datetime(2015,7,1),datetime.datetime(2015,10,1)])
    a.set_xticklabels([month_abbr[datetime.datetime(2015,1,1).month],month_abbr[datetime.datetime(2015,4,1).month],month_abbr[datetime.datetime(2015,7,1).month],month_abbr[datetime.datetime(2015,10,1).month]],rotation = 45, ha="right",fontsize=7)
    a.set_yticks([])
    a.set_ylabel('rel. Q')
    a.label_outer()
plt.subplots_adjust(hspace = 0,wspace = 0)

#~ plt.savefig(nc_rootdir+'SIEREM_streamflow.png')
plt.savefig(fig_dir+'SIEREM_streamflow.png')

""" Figure: plot mean daily flow """
#~ data_sort.loc[:,'Mean_daily_streamflow'].plot(kind='bar',yerr=data_sort.Standard_deviation_daily_streamflow)
#~ fig, ax = plt.subplots(1,1)
#~ data_ADHI_sort.loc[:,'Mean_daily_streamflow'].plot(kind='bar',log=True,ax=ax)
#~ ADHI_PF.mean().loc[data_ADHI_sort.index].plot(style ='r',logy=True,ax=ax)
#~ ax.set_ylabel('log(Q) ($m^3.s^{-1}$)')


#m3/s -> mm/yr
#~ data_ADHI_sort['daily_mm'] = data_ADHI_sort['Mean_daily_streamflow']*3600*24*365*1000/(ADHI['Catchment area (km2)']*1000*1000)
#~ data_ADHI_sort['daily_mm'].plot(kind='bar')


"""Compare Simulation and AC Benin DB """

""" Figure: plot all climatologies and data"""
AC_Benin_sorted = AC_Benin.sort_values(by='Catchment area (km2)')

fig, ax = plt.subplots(ncols = 3, nrows = 4,sharex = True,figsize=(14,9))
for i,a in enumerate(ax.flatten()[0:len(AC_Benin)]):
    #~ p1 = a.plot(data_AC.iloc[:,i],'k')
    #~ p2 = a.plot(AC_PF.iloc[:,i],'r')
    #sort by catchment size:
    p1 = a.plot(data_AC.loc[:,AC_Benin_sorted.index[i]],'k')
    p2 = a.plot(AC_PF.loc[:,AC_Benin_sorted.index[i]],'r')    
    
    a.text(0.05,0.9,AC_Benin_sorted.loc[AC_Benin_sorted.index[i],'Name']+' %d $km^2$'%AC_Benin_sorted.loc[AC_Benin_sorted.index[i],'Catchment area (km2)'],verticalalignment = 'top',transform = a.transAxes,FontSize=10)
    a.legend(handles=p1+p2,labels = ('obs','sim'),ncol=1,loc='upper right',framealpha=0.5,fontsize=12)
    if i!=0:
        a.get_legend().remove()
for i,a in enumerate(ax.flatten()):
    a.set_xticks([datetime.datetime(2015,7,1),datetime.datetime(2015,8,1),datetime.datetime(2015,9,1),datetime.datetime(2015,10,1)])
    a.set_xticklabels([month_abbr[datetime.datetime(2015,7,1).month],month_abbr[datetime.datetime(2015,8,1).month],month_abbr[datetime.datetime(2015,9,1).month],month_abbr[datetime.datetime(2015,10,1).month]],rotation = 45, ha="right",fontsize=12)
    #~ a.set_yticks([])
    a.set_xlim([datetime.datetime(2015,6,1),datetime.datetime(2015,11,1)])
    a.set_ylabel('Q ($m^3.s^{-1}$)')
    #~ a.label_outer()
#~ plt.subplots_adjust(hspace = 0,wspace = 0)

#~ plt.savefig(nc_rootdir+'AC_Benin_streamflow.png')
plt.savefig(fig_dir+'AC_Benin_streamflow.png')

