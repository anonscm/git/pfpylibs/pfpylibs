#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Prepare forcings for 1D simulations in wankama. Based on 
    
    Leauthaud, C., Cappelaere, B., Demarty, J., Guichard, F., Velluet, C.,
    Kergoat, L., Vischel, T., Grippa, M., Mouhaimouni, M., Bouzou Moussa,I., 
    Mainassara, I., and Sultan, B.: 
    A 60-year reconstructed high-resolution local meteorological data 
    set in Central Sahel (1950–2009): evaluation, analysis and 
    application to land surface modelling, 37, 2699–2718, 
    https://doi.org/10.1002/joc.4874, 2017.


    @copyright: 2021 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2021"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime

"""local imports:"""
from PFlibs import io_pfb
from PFlibs import drv_vegm_io as vegio

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
datapath = "/home/hectorb/DATA/METEO/Niger/AMMA-CATCH/data/NAD_dataset/"
simpath = "/home/hectorb/PARFLOW/PROJECTS/plot_scale_1D/Niger/"
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

forc = pd.read_csv(datapath + "NAD-M_1.txt", delim_whitespace=True, header = None,\
names = ['year','DoY','hour','minute','R','T','q','P','LW','SW','U'], index_col = None)
forc.index = forc.apply(lambda x: datetime.datetime(int(x['year']),1,1)+datetime.timedelta(days=x['DoY'],seconds=x['hour']*3600+x['minute']*60) - datetime.timedelta(days=1),axis=1)  
forc['V']=0.0
F = forc.loc[forc.index>=datetime.datetime(2000,1,1),['SW','LW','R','T','U','V','P','q']]
F['q']=F['q']/1000.0
F['R']=F['R']/30/60
F['P']=F['P']*100

#F.to_csv(simpath+'forc_2000_2009.txt',float_format='%2.4f', sep=' ', header=False, index=False)


# compare with:
#~ forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
#~ forc = pd.read_csv('/home/hectorb/PARFLOW/PROJECTS/Hillslope/bele_nalo_tests/forcings_11yrs/forcagePF.200706070809101112131415_forc_nal2.30mn.dat',header=None,sep='\t')
#~ forc.rename(columns=forcing_headers,inplace=True)
#~ forc.index=pd.date_range('1/1/2000 00:00:00', periods=len(forc), freq='30min')


lai = pd.DataFrame({'lai':np.nan},index=F.index)
lai.loc[(lai.index.month==1) & (lai.index.day==1),'lai'] = 0
lai.loc[(lai.index.month==6) & (lai.index.day==1),'lai'] = 0
lai.loc[(lai.index.month==9) & (lai.index.day==15),'lai'] = 1.4
lai.loc[(lai.index.month==12) & (lai.index.day==15),'lai'] = 0
lai = lai.interpolate()
#~ lai.resample('D').mean().plot()  

lai = pd.concat([lai]*19,axis=1) 
#lai.to_csv(simpath+'lai.dat',float_format='%2.4f', sep=' ', header=False, index=False)


sai = lai.copy(deep=True)
sai.loc[:,:] = 0
#sai.to_csv(simpath+'sai.dat',float_format='%2.4f', sep=' ', header=False, index=False)

hveg = lai.copy(deep=True)
hveg = hveg * 2.
#hveg.to_csv(simpath+'hveg.dat',float_format='%2.4f', sep=' ', header=False, index=False)

z0m = lai.copy(deep=True)
z0m = z0m / 5.
#z0m.to_csv(simpath+'z0m.dat',float_format='%2.4f', sep=' ', header=False, index=False)


hdr ={'X0': 0.0,
 'Y0': 0.0,
 'Z0': 0.0,
 'dx': 1.0,
 'dy': 1.0,
 'dz': 1.0,
 'ix': 0,
 'iy': 0,
 'iz': 0,
 'ns': 1,
 'nx': 1,
 'nx2': 1,
 'ny': 1,
 'ny2': 1,
 'nz': 1,
 'nz2': 1,
 'rx': 1,
 'ry': 1,
 'rz': 1}

#io_pfb.PFB_write(np.zeros([1,1,1])+19, hdr, pfb_fp=simpath+'veg_map.pfb')

#vegio.write_vegm([0],[0],[19],simpath+'drv_vegm.dat')


# root density:
# density = m/ m3 ?
CVdensity = [0,22900,22900, 1603,0]
CVdepths = [0,0.03,0.1, 0.85,3.5]

vdz = np.array([2.5e-03,2.5e-03,2.5e-03,2.5e-03,5.0e-03,1.0e-02,2.5e-02,6.0e-02,1.0e-01,
 1.0e-01,1.0e-01,1.0e-01,1.0e-01,1.0e-01,1.0e-01,1.0e-01,1.0e-01,1.0e-01,
 1.0e-01,1.0e-01,7.0e-01,2.0e+00,6.0e+00,1.4e+01])
z = np.cumsum(vdz)-vdz/2.

PFdepths = np.cumsum(vdz)
a = 3
b =2
PFroot = 0.5*(a*np.exp(-a*PFdepths)+b*np.exp(-b*PFdepths))

area = np.trapz(CVdensity, x=CVdepths,dx=0.001)
plt.plot(CVdensity/area,CVdepths,'r')
plt.plot(PFroot,PFdepths,'k')



a = 3
b =2
PFroot1 = 0.5*(a*np.exp(-a*PFdepths)+b*np.exp(-b*PFdepths))
a =1
b =1
PFroot2 = 0.5*(a*np.exp(-a*PFdepths)+b*np.exp(-b*PFdepths))
a =2
b =2
PFroot3 = 0.5*(a*np.exp(-a*PFdepths)+b*np.exp(-b*PFdepths))
a = 6
b =2
PFroot4 = 0.5*(a*np.exp(-a*PFdepths)+b*np.exp(-b*PFdepths))
a =2
b =6
PFroot5 = 0.5*(a*np.exp(-a*PFdepths)+b*np.exp(-b*PFdepths))
a =6
b =6
PFroot6 = 0.5*(a*np.exp(-a*PFdepths)+b*np.exp(-b*PFdepths))

fig, ax = plt.subplots(1,1)
plt.plot(PFroot1,PFdepths,'k',label='a=3,b=2')
plt.plot(PFroot2,PFdepths,'r',label='a=1,b=1')
plt.plot(PFroot3,PFdepths,'m',label='a=2,b=2')
plt.plot(PFroot4,PFdepths,'g',label='a=6,b=2')
plt.plot(PFroot5,PFdepths,'b',label='a=2,b=6')
plt.plot(PFroot6,PFdepths,'c',label='a=6,b=6')
ax.set_xlim([0,6])
ax.set_ylim([0,2])
plt.legend()
plt.gca().invert_yaxis()


