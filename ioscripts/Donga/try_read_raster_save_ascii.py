
# coding: utf-8

import kolinkelly as kk
import joestork as js

import numpy as np
import pandas as pd
import rasterio
import pyproj
import matplotlib.pyplot as plt
import PFlibs

#kolinkelly is a module from waterbox that includes functionalities for NC manipulation


###################################
## STEP 1: 
###################################


#### Open DEM:
#~ with rasterio.open("/home/hectorb/PARFLOW/PROJECTS/Ara/Ara/input/data/DEM/ara_MNT_100m_extent_342000_348400_1076000_1082400_mean_agreg_D4_pitfilled.tif") as src:
with rasterio.open("avenelles_cultures2017.tiff") as src:
    print('number of bands: %s'%src.count)
    print('raster width: %s'%src.width)
    print('raster height: %s'%src.height)
    print('raster bounding box:')
    print(src.bounds)
    print('(row,col) coordinates to (x,y) spatial locations:')
    print('upper left corner:')
    print(src.transform * (0,0))
    print('lower right corner: ')
    print(src.transform * (src.width, src.height))
    print('coordinate reference system')
    print(src.profile)
    
    # read in the xx-like (meshgrid-like) coordinates:
    xx_dem = np.array([[src.xy(j,i)[0] for i in range(src.width)] for j in range(src.height)])
    yy_dem = np.flipud(np.array([[src.xy(j,i)[1] for i in range(src.width)] for j in range(src.height)]))
    dem = np.flipud(src.read(1).astype('float64'))    
    
    # create x-like coordinate vector:
    x_dem = np.array([src.xy(0,i)[0] for i in range(src.width)])
    y_dem = np.array([src.xy(j,0)[1] for j in range(src.height)])[::-1]
    
    # to get a specific pixel (here 200 E & 100 m S of upper left corner:
    x, y = (src.bounds.left + 200, src.bounds.top - 100)
    row,col = src.index(x, y)
    print([row,col])
    print(dem[row,col])
    print('center of the image:')
    print(src.xy(src.width // 2, src.height // 2))
    
