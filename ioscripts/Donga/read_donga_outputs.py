#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    Anaylse PF outputs for Donga: Streamflow
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import os, glob, shutil 
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pandas as pd
import rasterio
import pyproj
from copy import copy, deepcopy

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from procsycz import readDataAMMA as rdA


plt.close("all")

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

#### set root directories:
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Donga/Donga_1km/output/results2017/'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Donga/Donga_1km/output/results4'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Donga/Donga_1km/output/results3'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Donga/Donga_1km/output/results2'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Donga/Donga_1km/output/results_H2_clayey'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Donga/Donga_1km/output/results_H2_clayey_n_hg_H3_highK_lowWC'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Donga/Donga_1km/output/results_sap_clay3'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Donga/Donga_1km/output/results_clay_hardpan2'
#~ simuname = 'clay_hardpan2'
simuname = 'sap_clay3'

n_coupled_layers = 16
n_CLM_2Dlayers = 14

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

#### Read & process simulation outputs:
sim = PFout.PFout(simdir,'doro')
# warning, current simulations are in m/mn (not hardcoded in the main code...)
#~ sim.set_reading_parameters(n_t=11,n_t_clm=240,n_t_forc = 240*2, dump_int=24,dump_int_clm=1,conv=1,conv_clm=1 * 60 * 60, conv_forc= 1 * 60 * 30)
#~ sim.set_reading_parameters(n_t=233,n_t_clm=5592,n_t_forc = 5592*2, dump_int=24,dump_int_clm=1,conv=1,conv_clm=1 * 60 * 60, conv_forc= 1 * 60 * 30)
sim.set_reading_parameters(n_t=274,n_t_clm=6552,n_t_forc = 6552*2, dump_int=24,dump_int_clm=1,conv=1,conv_clm=1 * 60 * 60, conv_forc= 1 * 60 * 30)
#~ sim.set_reading_parameters(n_t=731,n_t_clm=17520,n_t_forc = 17520*2, dump_int=24,dump_int_clm=1,conv=1,conv_clm=1 * 60 * 60, conv_forc= 1 * 60 * 30)
#~ sim.set_reading_parameters(n_t=1096,n_t_clm=26280,n_t_forc = 26280*2, dump_int=24,dump_int_clm=1,conv=1,conv_clm=1 * 60 * 60, conv_forc= 1 * 60 * 30)
#~ sim.set_reading_parameters(n_t=366,n_t_clm=8760,n_t_forc = 8760*2, dump_int=24,dump_int_clm=1,conv=1,conv_clm=1 * 60 * 60, conv_forc= 1 * 60 * 30)
sim.read_domain_dimensions_from_PFBs()

sim.read_allPFoutput_from_PFBs()
sim.read_CLMoutput(nlayers = n_coupled_layers+n_CLM_2Dlayers)
#~ sim.extract_CLM_2Doutputs(nlayers = 16 +14 )
sim.read_allconstant_outputs()
sim.read_forcings()
#~ sim.calculate_runoff()
#~ sim.calculate_WSC()

#### Get Q data:
rt_dir = r'/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/analyse/Data/streamflow/Donga'
suf_pattern = '.csv'
pre_pattern = 'CL.Run_Od-'
station_list = {'ARA_PONT':2 ,'DONGA_PONT':3,'DONGA_ROUTE_DE_KOLOKONDE':3}
proj = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')

df = pd.DataFrame()
stadic ={}
for stationname,data_col in station_list.items():
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    sta.read_Q(filepattern, data_col = data_col)
    sta.Q = sta.Q*sta.Q.index.to_series().diff().dt.total_seconds().fillna(0)
    df[stationname]=sta.Q    
    print(sta.name)
    filenames = glob.glob(filepattern)    
    sta.read_latlon_from_Qfile(filenames[0])
    sta.x, sta.y = pyproj.transform(geo_system,proj,sta.lon, sta.lat)
    print(sta.lon)
    print(sta.lat)
    print(sta.x)
    print(sta.y)
    stadic[stationname] = sta
    
#### Get stream network:
#~ with rasterio.open("/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/inputs/geometries/Donga/data/Donga_streams_segments_1000m_masked_mean_agreg_D4_pitfilled.tif") as src:
#~ with rasterio.open("/homelocal5/phyrev/_PARFLOW/projects/Donga/active/Donga_1km/input/DEM/Rwatershed/streams.tif") as src:
with rasterio.open("/home/hectorb/PARFLOW/PROJECTS/Donga/Donga_1km/input/DEM/Rwatershed/streams.tif") as src:
    print('number of bands: %s'%src.count)
    print('raster width: %s'%src.width)
    print('raster height: %s'%src.height)
    print('raster bounding box:')
    print(src.bounds)
    print('(row,col) coordinates to (x,y) spatial locations:')
    print('upper left corner:')
    print(src.transform * (0,0))
    print('lower right corner: ')
    print(src.transform * (src.width, src.height))
    print('coordinate reference system')
    print(src.profile)    
    # read in the xx-like (meshgrid-like) coordinates:
    xx_dem = np.array([[src.xy(j,i)[0] for i in range(src.width)] for j in range(src.height)])
    yy_dem = np.flipud(np.array([[src.xy(j,i)[1] for i in range(src.width)] for j in range(src.height)]))
    dem = np.flipud(src.read(1).astype('uint16'))    
    # create x-like coordinate vector:
    x_dem = np.array([src.xy(0,i)[0] for i in range(src.width)])
    y_dem = np.array([src.xy(j,0)[1] for j in range(src.height)])[::-1]    
    # to get a specific pixel (here 200 E & 100 m S of upper left corner:
    x, y = (src.bounds.left + 200, src.bounds.top - 100)
    row,col = src.index(x, y)
    print([row,col])
    print(dem[row,col])
    print('center of the image:')
    print(src.xy(src.width // 2, src.height // 2))
    

#### Find the location of data in the PF framework, and calculate simulated Q:
xx_masked = np.round(xx_dem)
yy_masked = np.round(yy_dem)

#~ xx_masked[xx_dem == 0] = -9999999
#~ yy_masked[yy_dem == 0] = -9999999
#~ 
#~ xx_masked[xx_dem == 255] = -9999999
#~ yy_masked[yy_dem == 255] = -9999999

xx_masked[dem == 0] = -9999999
yy_masked[dem == 0] = -9999999

xx_masked[dem == 255] = -9999999
yy_masked[dem == 255] = -9999999

Qsim = pd.DataFrame()
#~ x = [sim.X0 + sim.dx*x+sim.dx/2 for x in range(sim.nx)]
#~ y = [sim.Y0 + sim.dy*y+sim.dy/2 for y in range(sim.ny)]
x = [sim.X0 + sim.dx*x for x in range(sim.nx)]
y = [sim.Y0 + sim.dy*y for y in range(sim.ny)]
for stationname,sta in stadic.items():
    print(sta.name)
    #find the closest point in the grid:  
    dist_to_river = np.sqrt((xx_masked - sta.x)**2 + (yy_masked - sta.y)**2)
    dist = np.sqrt((np.round(xx_dem)- sta.x)**2 + (np.round(yy_dem) - sta.y)**2)
    if (dist_to_river.min() <= 2*np.sqrt(sim.dx*sim.dx + sim.dy*sim.dy)):
        ind_min = np.where(dist_to_river == dist_to_river.min())
    else:
        ind_min = np.where(dist == dist.min())   
    sta.x_sim = np.round(xx_dem[ind_min])
    sta.y_sim = np.round(yy_dem[ind_min])
    print(sta.x_sim)
    print(sta.y_sim)    
    # now get the outlet location in the PF domain:
    sim.calculate_runoff_from_CLM(x_outlet = np.where(np.array(x) == sta.x_sim),y_outlet = np.where(np.array(y) == sta.y_sim),nlayers = n_coupled_layers+n_CLM_2Dlayers)    
    print(np.where(np.array(x) == sta.x_sim))
    print(np.where(np.array(y) == sta.y_sim))
    print(np.shape(sim.surfQ))
    # if m3 => m3/s:
    #~ sim.surfQ = [Q/(60*60) for Q in sim.surfQ ]      
    sta.Qsim  = sim.create_panda_series(np.reshape(sim.surfQ,sim.n_t_clm),start_date = "1/1/2007", start_hour = "00:00", freq = "1H",periods = sim.n_t_clm)        
    Qsim[stationname]=sta.Qsim        
    stadic[stationname] = sta
    
#~ dom = sim.read_constant_outputs(os.path.join(simdir,'donga_domains.pfb'),np.zeros((sim.nx,sim.ny,sim.nz)))
#~ dom = PFlibs.io_pfb.PFB_read(os.path.join(simdir,'donga_domains.pfb'),0)    
dom = sim.read_domain()
#~ print('\n'.join([''.join(['{:8}'.format(item) for item in row]) for row in np.reshape(dom[:,:,0],[64,32])]))

#### PLOT stream network map:
#~ fig = plt.figure(figsize=(16,8))
#~ ax = fig.add_subplot(111)
#~ ax.set_aspect(1)
#~ 
#~ p = ax.pcolormesh(xx_dem, yy_dem, dem, cmap="terrain", zorder=-1)
#~ ax.plot([xx_dem[0,0], xx_dem[-1,-1], xx_dem[-1,-1], xx_dem[0,0], xx_dem[0,0]],
        #~ [yy_dem[0,0], yy_dem[0,0], yy_dem[-1,-1], yy_dem[-1, -1], yy_dem[0,0]], 'r-' )
        #~ 
#~ for stationname,sta in stadic.items():
    #~ ax.plot(sta.x,sta.y,'r+', markeredgewidth = 2, markersize = 10) 
    #~ ax.plot(sta.x_sim,sta.y_sim,'k+', markeredgewidth = 2, markersize = 10)
#~ 
#~ cb = plt.colorbar(p)
#~ plt.show()

#### PLOT stream network map:

#~ df.plot()
#~ 

#~ ax = df.plot(subplots=True, figsize=(6, 6))
#~ Qsim.plot(ax = ax)
#~ plt.show()  


fig = plt.figure(figsize=(16,8))
ax = df['DONGA_PONT'].loc['2007-1-1':'2009-12-31'].dropna().plot(title=sta,label='Obs.')
Qsim['DONGA_PONT'].loc['2007-1-1':'2009-12-31'].dropna().plot(ax=ax, label = 'Sim.')  
ax.set_ylabel('m3 / s')
legend = ax.legend(loc = 'upper left')
#~ plt.show()
plt.savefig(''.join(['Donga_pont_Q_',simuname,'.png']),format='png', dpi = 500)

fig = plt.figure(figsize=(16,8))
ax = df['DONGA_PONT'].loc['2007-1-1':'2009-12-31'].dropna().resample('D').sum().plot(title=sta,label='Obs.')
Qsim['DONGA_PONT'].loc['2007-1-1':'2009-12-31'].dropna().resample('D').sum().plot(ax=ax, label = 'Sim.')  
ax.set_ylabel('m3 / d')
legend = ax.legend(loc = 'upper left')
#~ plt.show()
plt.savefig(''.join(['Donga_pont_Q_D_',simuname,'.png']),format='png', dpi = 500)

fig = plt.figure(figsize=(16,8))
ax = df['DONGA_PONT'].loc['2007-1-1':'2009-12-31'].dropna().resample('W').sum().plot(title=sta,label='Obs.')
Qsim['DONGA_PONT'].loc['2007-1-1':'2009-12-31'].dropna().resample('W').sum().plot(ax=ax, label = 'Sim.')  
ax.set_ylabel('m3 / w')
legend = ax.legend(loc = 'upper left')
#~ plt.show()
plt.savefig(''.join(['Donga_pont_Q_W_',simuname,'.png']),format='png', dpi = 500)

#~ fig = plt.figure(figsize=(16,8))
#~ ax = df['DONGA_PONT'].loc['2007-1-1':'2009-12-31'].dropna().resample('W').mean().plot(title=sta,label='Obs.')
#~ Qsim['DONGA_PONT'].loc['2007-1-1':'2009-12-31'].dropna().resample('W').mean().plot(ax=ax, label = 'Sim.')    
#~ ax.set_ylabel('m3 / s')
#~ legend = ax.legend(loc = 'upper left')
#~ plt.show()
#~ 
#~ fig, axes = plt.subplots(nrows=3, ncols=1, figsize=(16, 8),squeeze=False)
#~ for i,sta in enumerate(stadic.keys()):
    #~ df[sta].loc['2007-1-1':'2009-12-31'].dropna().plot(ax=axes[i,0], title=sta,label='Obs.')
    #~ Qsim[sta].loc['2007-1-1':'2009-12-31'].dropna().plot(ax=axes[i,0], label = 'Sim.')    
    #~ axes[i,0].set_ylabel('m3 / s')
    #~ legend = axes[i,0].legend(loc = 'upper left')
#~ plt.tight_layout()
#~ plt.show()
#~ plt.savefig('Donga_pont_Q_W.png',format='png', dpi = 500)

fig, axes = plt.subplots(nrows=3, ncols=1, figsize=(16, 8),squeeze=False)
for i,sta in enumerate(stadic.keys()):
    df[sta].loc['2007-1-1':'2009-12-31'].dropna().resample('D').sum().plot(ax=axes[i,0], title=sta,label='Obs.')
    Qsim[sta].loc['2007-1-1':'2009-12-31'].dropna().resample('D').sum().plot(ax=axes[i,0], label = 'Sim.')    
    axes[i,0].set_ylabel('m3 / d')
    legend = axes[i,0].legend(loc = 'upper left')
plt.tight_layout()
#~ plt.show()
plt.savefig(''.join(['Donga_Q_D_',simuname,'.png']),format='png', dpi = 500)


#~ fig, axes = plt.subplots(nrows=3, ncols=1, figsize=(8, 8))
#~ for i,sta in enumerate(stadic.keys()):
    #~ df[sta].loc['2007-1-1':'2008-12-31'].cumsum(0).plot(ax=axes[i], title=sta,label='Obs.')
    #~ Qsim[sta].loc['2007-1-1':'2008-12-31'].cumsum(0).plot(ax=axes[i], title=sta, label = 'Sim.')    
#~ axes[0].set_ylabel('cumulative sum')
#~ legend = axes[0].legend()
#~ plt.tight_layout()
#~ plt.show()


# to print a 2D array:
#~ print('\n'.join([''.join(['{:8}'.format(item) for item in row]) for row in np.reshape(sim.slope_x_arr,[64,32])]))
#~ print('\n'.join([''.join(['{:8}'.format(item) for item in row]) for row in np.reshape(sim.slope_x_arr,[64,32])]))



tmp2 = sim.clm_output_array
tmp1 = np.zeros(np.shape(sim.clm_output_array))+1
tmp1[:,:,n_coupled_layers+n_CLM_2Dlayers -13,:] = tmp2[:,:,n_coupled_layers+n_CLM_2Dlayers -13,:]
tmp2[tmp1<0] = 0.0
print('\n'.join([''.join(['{:8}'.format(int(10000*item)/10000) for item in row]) for row in np.reshape(tmp2[:,:,n_coupled_layers+n_CLM_2Dlayers-13,6002],[64,32])]))
xout = np.where(np.array(x) == stadic['DONGA_PONT'].x_sim)
yout = np.where(np.array(y) == stadic['DONGA_PONT'].y_sim)
#~ plt.plot(np.reshape(tmp2[xout,yout,17,:],sim.n_t_clm))
sim.calculate_runoff_from_CLM(x_outlet = xout,y_outlet = yout,nlayers = n_coupled_layers+n_CLM_2Dlayers)    

nlayers = n_coupled_layers+n_CLM_2Dlayers
Sy = sim.slope_y_arr[xout,yout]
Sx = sim.slope_x_arr[xout,yout]
print(Sx)
print(Sy)
surfQ = []
m = sim.mannings_arr[xout,yout]
print(m)
for i in range(sim.n_t_clm):
    if sim.clm_output_array[xout,yout,nlayers-13,i] >= 0 :
        Qx = PFout.calculate_OVFLOW(width = sim.dy, manning = m[0], slope = Sx[0],h = sim.clm_output_array[xout,yout,nlayers-13,i]) 
        #~ Qx = Qx[0] *(1000/self.surf)*self.conv_clm*self.dump_int_clm
        # m3    /hr => m3 
        Qx = Qx[0]
        Qy = PFout.calculate_OVFLOW(width = sim.dx, manning = m[0], slope = Sy[0],h = sim.clm_output_array[xout,yout,nlayers-13,i]) 
        #~ Qy = Qy[0] *(1000/self.surf)*self.conv_clm*self.dump_int_clm    
        Qy = Qy[0]             
        surfQ.append(np.sqrt((Qx * Qx) + (Qy * Qy)))
    else:
        surfQ.append(0.)
        
print(np.unique(surfQ))
#~ sim.surfQ = surfQ
