
# coding: utf-8

import kolinkelly as kk
import joestork as js

import numpy as np
import pandas as pd
import rasterio
import pyproj
import matplotlib.pyplot as plt
import PFlibs

#kolinkelly is a module from waterbox that includes functionalities for NC manipulation


###################################
## STEP 1: 
###################################


#### Open DEM:
with rasterio.open("/home/hectorb/PARFLOW/PROJECTS/Ara/Ara/input/data/DEM/ara_MNT_100m_extent_342000_348400_1076000_1082400_mean_agreg_D4_pitfilled.tif") as src:
    print('number of bands: %s'%src.count)
    print('raster width: %s'%src.width)
    print('raster height: %s'%src.height)
    print('raster bounding box:')
    print(src.bounds)
    print('(row,col) coordinates to (x,y) spatial locations:')
    print('upper left corner:')
    print(src.transform * (0,0))
    print('lower right corner: ')
    print(src.transform * (src.width, src.height))
    print('coordinate reference system')
    print(src.profile)
    
    # read in the xx-like (meshgrid-like) coordinates:
    xx_dem = np.array([[src.xy(j,i)[0] for i in range(src.width)] for j in range(src.height)])
    yy_dem = np.flipud(np.array([[src.xy(j,i)[1] for i in range(src.width)] for j in range(src.height)]))
    dem = np.flipud(src.read(1).astype('float64'))    
    
    # create x-like coordinate vector:
    x_dem = np.array([src.xy(0,i)[0] for i in range(src.width)])
    y_dem = np.array([src.xy(j,0)[1] for j in range(src.height)])[::-1]
    
    # to get a specific pixel (here 200 E & 100 m S of upper left corner:
    x, y = (src.bounds.left + 200, src.bounds.top - 100)
    row,col = src.index(x, y)
    print([row,col])
    print(dem[row,col])
    print('center of the image:')
    print(src.xy(src.width // 2, src.height // 2))
    

#### Open MASK:
with rasterio.open("/home/hectorb/PARFLOW/PROJECTS/Ara/Ara/input/data/DEM/mask_ara_MNT_100m_mean_agreg_D4pitfilled_outlet_347450_1078350.tif") as src:
    print('number of bands: %s'%src.count)
    print('raster width: %s'%src.width)
    print('raster height: %s'%src.height)
    print('raster bounding box:')
    print(src.bounds)
    print('(row,col) coordinates to (x,y) spatial locations:')
    print('upper left corner:')
    print(src.transform * (0,0))
    print('lower right corner: ')
    print(src.transform * (src.width, src.height))
    print('coordinate reference system')
    print(src.profile)
    
    # read in the xx-like (meshgrid-like) coordinates:
    xx_mask = np.array([[src.xy(j,i)[0] for i in range(src.width)] for j in range(src.height)])
    yy_mask = np.flipud(np.array([[src.xy(j,i)[1] for i in range(src.width)] for j in range(src.height)]))
    mask = np.flipud(src.read(1).astype('float64'))    
    mask[mask==255]=0

    # create x-like coordinate vector:
    x_mask = np.array([src.xy(0,i)[0] for i in range(src.width)])
    y_mask = np.array([src.xy(j,0)[1] for j in range(src.height)])[::-1]
    
    # to get a specific pixel (here 200 E & 100 m S of upper left corner:
    x, y = (src.bounds.left + 200, src.bounds.top - 100)
    row,col = src.index(x, y)
    print([row,col])
    print(mask[row,col])
    print('center of the image:')
    print(src.xy(src.width // 2, src.height // 2))
    
#### PLOT
fig = plt.figure(figsize=(16,8))
ax = fig.add_subplot(111)
ax.set_aspect(1)

p = ax.pcolormesh(xx_dem, yy_dem, dem, cmap="terrain", zorder=-1)
ax.plot([xx_dem[0,0], xx_dem[-1,-1], xx_dem[-1,-1], xx_dem[0,0], xx_dem[0,0]],
        [yy_dem[0,0], yy_dem[0,0], yy_dem[-1,-1], yy_dem[-1, -1], yy_dem[0,0]], 'r-' )
cb = plt.colorbar(p)
p2 = ax.pcolormesh(xx_mask, yy_mask, mask, cmap='Greys', zorder=-1, alpha=0.1)

plt.pcolor(x_mask, y_mask, np.ma.masked_equal(mask,0), hatch='/', alpha=0.)
#cb2 = plt.colorbar(p2)
#p3 = ax.pcolormesh(xx_d, yy_d, curr_var_bm.values.sum(axis=(0,1)), cmap='viridis_r', zorder=-2)
#~ ax.set_title('Delta_x = %.3f km, Delta_y = %.3f km'%(delta_x/1000, delta_y/1000))
plt.show()
