#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Reads in a distributed forcing file

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os,glob

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb

plt.close('all')

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

#~ Pdir='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/FORC/LAI'
#~ filepattern = glob.glob(os.path.join(Pdir,'Forc.LAI.*.pfb'))
#~ Pdir='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/FORC/SAI'
#~ filepattern = glob.glob(os.path.join(Pdir,'Forc.SAI.*.pfb'))
#~ Pdir='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/FORC/DISPLA'
#~ filepattern = glob.glob(os.path.join(Pdir,'Forc.DISPLA.*.pfb'))
#~ Pdir='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/FORC/Z0M'
#~ Pdir='/mnt/hectorb/faro/PARFLOW/PROJECTS/Oueme/forcings/FORC/LAI'
#~ Pdir='/mnt/hectorb/faro/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/forcings/FORC_veg/LAI'
#~ filepattern = glob.glob(os.path.join(Pdir,'Forc.LAI.*.pfb'))

#FARO
#~ Pdir = '/home/hectorb/PARFLOW/PROJECTS/Oueme/forcings/FORC/Temp'
#~ filepattern = glob.glob(os.path.join(Pdir,'Forc.Z0M.*.pfb'))
#~ filepattern = glob.glob(os.path.join(Pdir,'Forc.LAI.*.pfb'))
#~ filepattern = glob.glob(os.path.join(Pdir,'Forc.Temp.*.pfb'))
Pdir='/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/forcings/FORC_veg/LAI'
filepattern = glob.glob(os.path.join(Pdir,'Forc.LAI.*.pfb'))

filepattern.sort()
i = 0
for f in filepattern[0:20]:
    print('reading file number: %d'%i)
    d,h = PFlibs.io_pfb.PFB_read(f,1)
    if i == 0:
        P = np.zeros([h['nx'],h['ny']])
        P = np.dstack([P,d[:,:,::-1]])        
    else:
        P = np.dstack([P,d[:,:,::-1]])
    i+=1
d = pd.Series(P[50,50,:])
d.index = pd.date_range('1/1/2005 00:00:00', periods=len(d), freq='30MIN')

