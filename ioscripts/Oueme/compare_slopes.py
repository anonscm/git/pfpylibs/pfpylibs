#-*- coding: utf-8 -*-
#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    quick comparison of slopes for Ouémé

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl


"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as io

plt.close("all")

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

#~ sx_UP,xx,yy = io.plot_2D_PFB('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Xslopes_merit_f3_nosec_UP.pfb',plot_fig=False)
#~ sy_UP,xx,yy = io.plot_2D_PFB('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Yslopes_merit_f3_nosec_UP.pfb',plot_fig=False)
#~ sx,xx,yy = io.plot_2D_PFB('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Xslopes_merit_f3_nosec.pfb',plot_fig=False)
#~ sy,xx,yy = io.plot_2D_PFB('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Yslopes_merit_f3_nosec.pfb',plot_fig=False)

#~ sx_UP,xx,yy = io.plot_2D_PFB('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Xslopes_merit_f3_m1_UP.pfb',plot_fig=False)
#~ sy_UP,xx,yy = io.plot_2D_PFB('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Yslopes_merit_f3_m1_UP.pfb',plot_fig=False)
#~ sx,xx,yy = io.plot_2D_PFB('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Xslopes_merit_f3_m1.pfb',plot_fig=False)
#~ sy,xx,yy = io.plot_2D_PFB('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Yslopes_merit_f3_m1.pfb',plot_fig=False)

sx_UP,xx,yy = io.plot_2D_PFB('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Xslopes_merit_f3_m1_UP.pfb',plot_fig=False)
sy_UP,xx,yy = io.plot_2D_PFB('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Yslopes_merit_f3_m1_UP.pfb',plot_fig=False)
sx,xx,yy = io.plot_2D_PFB('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Xslopes_merit_f3_UP.pfb',plot_fig=False)
sy,xx,yy = io.plot_2D_PFB('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Yslopes_merit_f3_UP.pfb',plot_fig=False)

#~ sx_UP,xx,yy = io.plot_2D_PFB('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Xslopes_merit_f3_m1_border_UP.pfb',plot_fig=False)
#~ sy_UP,xx,yy = io.plot_2D_PFB('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Yslopes_merit_f3_m1_border_UP.pfb',plot_fig=False)
#~ sx,xx,yy = io.plot_2D_PFB('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Xslopes_merit_f3_m1_border.pfb',plot_fig=False)
#~ sy,xx,yy = io.plot_2D_PFB('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Yslopes_merit_f3_m1_border.pfb',plot_fig=False)
sx_UP[sx_UP==0]=np.nan
sy_UP[sy_UP==0]=np.nan
sx[sx==0]=np.nan
sy[sy==0]=np.nan

#colormap 
cmap = plt.cm.jet
cmaplist = [cmap(i) for i in range(cmap.N)] 				# extract all colors from the .jet map
#~ cmaplist[0] = (.5,.5,.5,1.0) 								# force the first color entry to be grey
cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N) 	# create the new map
bounds = np.arange(-0.02,0.02,0.0002) 									# define the bins and normalize
norm = mpl.colors.BoundaryNorm(bounds, cmap.N)


fig,ax = plt.subplots(2,2,figsize = [12,12],sharex=True,sharey=True)
for a in ax.flat: a.set_aspect(1)
ax1 = ax[0][0].pcolormesh(xx, yy, sx_UP, cmap=cmap, norm=norm, zorder=-1)
ax2 = ax[0][1].pcolormesh(xx, yy, sy_UP, cmap=cmap, norm=norm, zorder=-1)
ax3 = ax[1][0].pcolormesh(xx, yy, sx, cmap=cmap, norm=norm, zorder=-1)
ax4 = ax[1][1].pcolormesh(xx, yy, sy, cmap=cmap, norm=norm, zorder=-1)

#control colorbar
fig.colorbar(ax4, ax=ax.ravel().tolist(),label="WTD (m)")

#or:
#~ cbar_ax = fig.add_axes([0.15, 0.05, 0.7, 0.05])
#~ fig.colorbar(ax1, cax=cbar_ax,orientation='horizontal',label="WTD (m)")
#~ for i,a in enumerate(ax.flat): a.set_xticklabels('');a.set_yticklabels('');a.set_title('simulation %d'%i)

#~ ax[0][0].set_title('X_slopes_UP')
#~ ax[0][1].set_title('Y_slopes_UP')
#~ ax[1][0].set_title('X_slopes_Stan')
#~ ax[1][1].set_title('Y_slopes_Stan')

ax[0][0].set_title('X_slopes_f3_m1_UP')
ax[0][1].set_title('Y_slopes_f3_m1_UP')
ax[1][0].set_title('X_slopes_f3_UP')
ax[1][1].set_title('Y_slopes_f3_UP')


#~ plt.gcf().subplots_adjust(bottom=0.05, top =0.95,left=0.15, hspace=0.02,wspace=0.2)

#~ plt.savefig(os.sep.join([simdir1,'Figure_compare_WT_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')


