#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    Anaylse PF outputs for Oueme: Streamflow
    V2 = added Donga
    Amelie_runs = V2 + adapted for Amelie's outputs
    COmpare = the reading of the simulation part is put in a function
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
    script modified by Amelie Herzog, <amelie.herzog@hydrology.uni-freiburg.de>
"""

__author__     = "PHyREV Basile Hector, Amelie Herzog"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os,glob,copy
from netCDF4 import Dataset
import time
import datetime
import pyproj
from osgeo import gdal,ogr,osr
from osgeo.gdalconst import *
import rasterio
import seaborn as sns
import xarray as xr

from rasterio.mask import mask
from rasterio.plot import plotting_extent
import geopandas as gpd
from shapely.geometry import mapping
from matplotlib.dates import MonthLocator, DateFormatter
import statsmodels.api as sm
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
from matplotlib import gridspec

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
#from PFlibs import PFoutputs as PFout
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as io
# from procsycz import readDataAMMA as rdA
# from procsycz import objfunctions as obj
# from procsycz import procGeodata_Gdal

from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
from procsycz import procGeodata_Gdal
#~ from basicfct import crop_2Darray
plt.close("all")

""" import demand efficiency """
from de import de 
from de import util
from pathlib import Path
from de import kge
from de import nse

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
path=r"D:\Daten\Simus_Gre\simus_1309\Exp1_constmean"
#path=r"D:\Daten\Simus_Gre\simus_1309\Exp2_stochastic066"
#path=r"D:\Daten\Simus_Gre\simus_1309\Exp2_stochastic123"
#path=r"D:\Daten\Simus_Gre\simus_1309\Exp3_ZAZFA"
#path=r"D:\Daten\Simus_Gre\simus_1309\Exp4_H2constmean"
#path=r"D:\Daten\Simus_Gre\simus_1309\Exp5_clayprct"

### benchmark + const mean:
simdir1 = r'D:/Daten/Simus_Gre/simus_1309/final/01_H3015_H2clay24prct_2020_09_06'
simuname1 = '01_H3015_H2clay24prct'
short1='ZA_H3015_H2clay24'
#short1='1a) benchmark'
##
#simdir2 = r"D:\Daten\Simus_Gre\simus_1309\final\02_H306_H2clay24prct_2020_09_09"
#simuname2 = '02_H306_H2clay24prct'
##short2='H306_H2clay24'
#short2='1b)'
##
#simdir3 = r"D:\Daten\Simus_Gre\simus_1309\final\03_H36_H2clay24prct_2020_09_09"
#simuname3 = '03_H36_H2clay24prct'
##short3='ZA_H36_H2clay24'
#short3='1c)'
###
#simdir4 = r'D:/Daten/Simus_Gre/simus_1309/final/04_H30029_H2clay24prct_2020_09_09'
#simuname4 = '04_H30029_H2clay24prct'
##short4='H30029_H2clay24'
#short4='1d)'


### benchmark + random field 066:
##
#simdir1 = r"D:\Daten\Simus_Gre\simus_1309\final\05_H3015_H2clay24prct_2020_09_06"
#simuname1 = '05_H3015_H2clay24prct'
#short1='2a)'
#
#simdir2 = r'D:/Daten/Simus_Gre/simus_1309/final/06_H3015_H2clay24prct_l1000_std066_2020_09_14'
#simuname2 = '06_H3015_H2clay24prct_l1000_std066'
##short2='l1000_std066'
#short2='2b)'
##
#simdir3 = r"D:\Daten\Simus_Gre\simus_1309\final\07_H3015_H2clay24prct_l10000_std066_2020_09_14"
#simuname3 = '07_H3015_H2clay24prct_l10000_std066'
##short3='l10000_std066'
#short3='2c)'
###
#simdir4 = r"D:/Daten/Simus_Gre/simus_1309/final/08_H3015_H2clay24prct_l50000_std066_2020_09_14"
#simuname4 = '08_H3015_H2clay24prct_l50000_std066'
##short4='l50000_std066'
#short4='2d)'

### benchmark + random field 123:

#simdir1 = r"D:\Daten\Simus_Gre\simus_1309\final\01_H3015_H2clay24prct_2020_09_06"
#simuname1 = '01_H3015_H2clay24prct'
#short1='benchmark'
#
#simdir2 = r"D:\Daten\Simus_Gre\simus_1309\H3015_H2clay24prct_l1000_std132_2020_09_14"
#simuname2 = 'H3015_H2clay24prct_l1000_std132'
#short2='l1000_std132'
##
#simdir3 = r"D:\Daten\Simus_Gre\simus_1309\H3015_H2clay24prct_l10000_std132_2020_09_14"
#simuname3 = 'H3015_H2clay24prct_l10000_std132'
#short3='l10000_std132'
###
#simdir4 = r'D:/Daten/Simus_Gre/simus_1309/H3015_H2clay24prct_l50000_std132_2020_09_14'
#simuname4 = 'H3015_H2clay24prct_l50000_std132'
#short4='l50000_std132'

### vertical dist: 

#simdir1 = r"D:/Daten/Simus_Gre/simus_1309/final/09_H3015_H2clay24prct_2020_09_06"
#simuname1 = '09_H3015_H2clay24prct'
#short1='3a) benchmark'
#
#simdir2 = r"D:/Daten/Simus_Gre/simus_1309/final/10_H3015_H2clay24prct_ZFA48_H51_2020_09_17"
#simuname2 = '10_H3015_H2clay24prct_ZFA48_H51'
##short2='l1000_std123'
#short2='3b)'
##
#simdir3 = r"D:/Daten/Simus_Gre/simus_1309/final/11_H3015_H2clay24prct_ZFA48_H510_2020_09_18"
#simuname3 = '11_H3015_H2clay24prct_ZFA48_H510'
##short3='l10000_std123'
#short3='3c)'
###
#simdir4 = r'D:/Daten/Simus_Gre/simus_1309/final/12_H3015_H2clay24prct_ZFA48_H5100_2020_09_18'
#simuname4 = '12_H3015_H2clay24prct_ZFA48_H5100'
##short4='l50000_std123'
#short4='3d)'





# H2 sensitivity const:

#simdir1 = r"D:\Daten\Simus_Gre\simus_1309\final\13_H3015_H20001_2020_09_05"
#simuname1 = '13_H3015_H20001'
#short1='H20001'
#short1='4a)'
#
#simdir2 = r'D:/Daten/Simus_Gre/simus_1309/final/14_H3015_H20002_2020_09_05'
#simuname2 = '14_H3015_H20002'
#short2='H20002'
#short2='4b)'
##
#simdir3 = r"D:\Daten\Simus_Gre\simus_1309\final\15_H3015_H20003_2020_09_05"
#simuname3 = '15_H3015_H20003'
#short3='H20003'
#short3='4c)'
###
#simdir4 = r'D:/Daten/Simus_Gre/simus_1309/final/16_H3015_H20004_2020_09_05'
#simuname4 = '16_H3015_H20004'
#short4='H20004'
#short4='4d)'

# H2 sensitivity clay:
#
#simdir1 = r"D:\Daten\Simus_Gre\simus_1309\final\17_H3015_H2clay24prct_2020_09_06"
#simuname1 = '17_H3015_H2clay24prct'
#short1='5a) benchmark'
#
#simdir2 = r'D:/Daten/Simus_Gre/simus_1309/final/18_H3015_H2clay10prct_2020_09_06'
#simuname2 = '18_H3015_H2clay10prct'
##short2='clay10prct'
#short2='5b)'
##
#simdir3 = r"D:\Daten\Simus_Gre\simus_1309\final\19_H3015_H2clay48prct_2020_09_06"
#simuname3 = '19_H3015_H2clay48prct'
#short3='5c)'
###
#simdir4 = r"D:\Daten\Simus_Gre\simus_1309\final\01_H3015_H2clay24prct_2020_09_06"
#simuname4 = '01_H3015_H2clay24prct'
#short4='1a) benchmark'


experiments=[1,2,3,4,5]


# select which year:
#plot_yr = 2006
#~ plot_yr = 2005
#method = 'W'
method = 'D'
#~ method = 'H'
scale = True
#~ scale = False

statsDay =True
#~ statsDay =False

# to simplify the search for station locations
dx=1000
dy=1000


""" import precipitation data"""
dir_path=r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/inputs/precip/'
out_path=r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/inputs/precip/'
rain1='rainfall_2005_30mn_utm'
rain2='rainfall_2006_30mn_utm'

#~ rain_mask_bet, lonrain, latrain = procGeodata_Gdal.readRasterWithGdal(r"D:/Daten/Simus_Gre/data/rainfield_001_mask_BET_cropped.tif",nodata_value = 255)
rain_mask_bet, lonrain, latrain = procGeodata_Gdal.readRasterWithGdal(r"/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/inputs/precip/rainfield_001_mask_BET_cropped.tif",nodata_value = 255)
[rainlon,rainlat]=np.meshgrid(lonrain,latrain)
rain_mask_cot, loncot, latcot = procGeodata_Gdal.readRasterWithGdal(r"/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/inputs/precip/rainfield_001_mask_COT_cropped.tif",nodata_value = 255)
[cotlon,cotlat]=np.meshgrid(loncot,latcot)

#rain_mask_cot, meta=raster2array(r"D:/Daten/Simus_Gre/data/rainfield_001_mask_BET_cropped.tif")
#lonmax=479000
#lonmin=334000
#latmin=987000
#latmax=1132000
#lonmax=477500
#lonmin=334500
#latmin=987500
#latmax=1130500
#xcrop_min=lonmin
#xcrop_max=lonmax
#ycrop_min=latmin
#ycrop_max=latmax
#rain_mask_cot = rain_mask_cot[::-1,:]
#[rain_mask_cot,rainlon,rainlat,lonrain,latrain] = crop_2Darray(rain_mask_cot,rainlon,rainlat,lonrain,latrain,xcrop_min,xcrop_max,ycrop_min,ycrop_max)


P1 = xr.open_dataset(os.sep.join([dir_path,''.join([rain1,'.nc'])]),decode_times=False)
tsim = pd.date_range('1/1/2005 00:00:00', periods=len(P1.time), freq='30min')
P1 = P1.assign_coords(time = pd.to_datetime(tsim)) #for satur and press

P2 = xr.open_dataset(os.sep.join([dir_path,''.join([rain2,'.nc'])]),decode_times=False)
tsim2 = pd.date_range('1/1/2006 00:00:00', periods=len(P2.time), freq='30min')
P2 = P2.assign_coords(time = pd.to_datetime(tsim2)) #for satur and press

P1_sta_test=pd.DataFrame()
P2_sta_test=pd.DataFrame()

#P1_sta_test['beterou']=P1.rainfall[:,29,85]
#P2_sta_test['beterou']=P2.rainfall[:,29,85]
#P1_sta_test['beterou'].plot()
##P2_beterou.plot()
#
#P1_sta_test['cote']=P1.rainfall[:,17,65]
#P2_sta_test['cote']=P2.rainfall[:,17,65]
#P1_sta_test['time']=P1.time
#P2_sta_test['time']=P2.time
#P1_sta_test=P1_sta_test.set_index(['time'])
#P2_sta_test=P2_sta_test.set_index(['time'])
#P1_sta_test=P1_sta_test.resample('H').mean()
#P2_sta_test=P2_sta_test.resample('H').mean()


#Precip_all=P1_sta_test.append(P2_sta_test)

P1_stat=pd.DataFrame()
P2_stat=pd.DataFrame()

""" Beterou crop rain"""
P1.coords['mask_rain_bet'] = (('x', 'y'), rain_mask_bet)
P1_masked=P1.assign(rain_bet_masked=P1['rainfall'].where(P1.mask_rain_bet))
P1_masked=P1_masked.assign(bet_maskedmean=P1_masked['rain_bet_masked'].mean(["x", "y"], skipna=True))

P2.coords['mask_rain_bet'] = (('x', 'y'), rain_mask_bet)
P2_masked=P2.assign(rain_bet_masked=P2['rainfall'].where(P2.mask_rain_bet))
P2_masked=P2_masked.assign(bet_maskedmean=P2_masked['rain_bet_masked'].mean(["x", "y"], skipna=True))
#P1_sampled=P1_masked.resample(time='H').mean(skipna=True)
P1_stat['beterou']=P1_masked.bet_maskedmean
P1_stat['time']=P1_masked.time

#P1_stat.plot()

P2_stat['beterou']=P2_masked.bet_maskedmean
P2_stat['time']=P2_masked.time
#P2_stat.plot()

""" Cote crop rain """
P1.coords['mask_rain_cot'] = (('x', 'y'), rain_mask_cot)
P1_masked=P1.assign(rain_cot_masked=P1['rainfall'].where(P1.mask_rain_cot))
P1_masked=P1_masked.assign(cot_maskedmean=P1_masked['rain_cot_masked'].mean(["x", "y"], skipna=True))

P2.coords['mask_rain_cot'] = (('x', 'y'), rain_mask_cot)
P2_masked=P2.assign(rain_cot_masked=P2['rainfall'].where(P1.mask_rain_cot))
P2_masked=P2_masked.assign(cot_maskedmean=P2_masked['rain_cot_masked'].mean(["x", "y"], skipna=True))
#P1_sampled=P1_masked.resample(time='H').mean(skipna=True)
P1_stat['cote']=P1_masked.cot_maskedmean
P2_stat['cote']=P1_masked.cot_maskedmean

P1_stat['time']=P1_masked.time
P1_stat=P1_stat.set_index(['time'])
P1_stat=P1_stat.resample('H').mean()
#P1_sta['beterou'].plot()
#P1_sta['cote'].plot()

P2_stat['time']=P2_masked.time
P2_stat=P2_stat.set_index(['time'])
P2_stat=P2_stat.resample('H').mean()
#P2_sta['beterou'].plot()
#P2_sta['cote'].plot()

Precip_all=P1_stat.append(P2_stat)
Precip_all.to_csv(r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/inputs/precip/Precip_all.csv')
Precip_all=pd.read_csv(r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/inputs/precip/Precip_all.csv', index_col=0)
Precip_all.index=pd.to_datetime(Precip_all.index)

def rain_nonzero(Precip_year):
    rainmax=Precip_year[Precip_year==Precip_year.max()]
    nonzero=Precip_year.where(Precip_year>0)
    nonzero=nonzero.dropna()
    rainmin=nonzero[nonzero==nonzero.min()]
    return nonzero, rainmax, rainmin

Precip_2005=Precip_all[0:8760]
nonzero2005_bet, max05bet, min05bet=rain_nonzero(Precip_2005.beterou)
nonzero2005_cot, max05cot, min05cot=rain_nonzero(Precip_2005.cote)

Precip_2006=Precip_all[8760:]
nonzero2006_bet, max06bet, min06bet=rain_nonzero(Precip_2006.beterou)
nonzero2006_cot, max06cot, min06cot=rain_nonzero(Precip_2006.cote)

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def minimalist_xldate_as_datetime(xldate, datemode):
    # datemode: 0 for 1900-based, 1 for 1904-based
    return (
        datetime.datetime(1899, 12, 30)
        + datetime.timedelta(days=xldate + 1462 * datemode)
        )
        
def roundDateTime(shakyDT,nearest_minutes=1):
	"""
	"""
	shakyDT += datetime.timedelta(minutes=nearest_minutes/2)
	shakyDT -= datetime.timedelta(minutes=shakyDT.minute % nearest_minutes,
                         seconds=shakyDT.second,
                         microseconds=shakyDT.microsecond)
	return shakyDT
	

def pdinterpolate(ts, datetime_index):
    """could decide to fill nans !
    but not adapted for calculating scores?
    To be tested"""
    x = pd.concat([ts, pd.Series(index=datetime_index)])
    #~ return x.groupby(x.index).first().sort_index().fillna(method="ffill")[datetime_index]
    return x.groupby(x.index).first().sort_index().interpolate('linear')[datetime_index]
    #~ return x.groupby(x.index).first().sort_index()[datetime_index]

def get_streamflow_simulation_results(simdir,simuname,stadic,slope_mode = 'OF',manning=1e-6,manning_filename='whatever.pfb'):
    """
    get a pd DataFrame with Q for all stations (given in stadic)
    slope mode : 'OF' = 'Overland Flow' classical way of using the slopes straight to get Q
    'KWE' = 'Kinematic Wave Equation' using the flow direction to get the slopes to get Q
    """

    #### Read & process simulation outputs:
    sim = PFout.PFout(simdir,'oueme')
    sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
    sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_h_top.nc'])]),filetype='CLM')
#    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,''])]),filetype='CLM')
    sim.X0=np.min(sim.x)
    sim.Y0=np.min(sim.y)

    """Read PF outputs"""
    start_time = time.time()
    with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
        sim.satur_array = ds.variables['saturation'][:]
        #~ topmask=np.swapaxes(ds.variables['top_mask'][:],0,1)
    """MASK is a nx x ny boolean mask of the domain
    When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
    (as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
    """
    #~ MASK=topmask>0 
    with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
        sim.press_array = ds.variables['succion'][:] 

    """Read CLM outputs"""
    
    """
    CLM_var is a dictionnary where keys are local variable names as in PFCLM
    values are lists of string[filename, PFCLM variable name, long name, units]
    """
    CLM_var={'surf_press':['','h_top','Surface pressure','m']}
    """ add the filename in the dic"""        
    for k,val in CLM_var.items():
        CLM_var[k][0]=os.sep.join([simdir,''.join([simuname,'_'+val[1]+'.nc'])])
        
    for k,val in CLM_var.items():
        try:
            with Dataset(val[0], 'r') as ds:
                print('read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
                exec('sim.%s=np.swapaxes(ds.variables[\'%s\'][:],0,2)'%(k,val[1]))        
        except FileNotFoundError:
                print('CANNOT read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
    print("--- %d seconds --- read all PFCLM outputs from NCs" % (time.time() - start_time))

    """ Read constant files"""
    ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
    #watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
    sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,1)
    if manning==0:
        sim.mannings_arr = io.PFB_read(manning_filename,0)[:,:,0]
    else:
        sim.mannings_arr = np.zeros((sim.nx,sim.ny))+manning
    sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,2)
    sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
    sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
    sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,1)
    sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,1)
    #~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))
    ds.close()
    #xx_masked, yy_masked and data have shapes [ny,nx] and data[0,:] is the lowest latitude (bottom of map)
    Qsim = pd.DataFrame()
    #~ x = [sim.X0 + sim.dx*x+sim.dx/2 for x in range(sim.nx)]
    #~ y = [sim.Y0 + sim.dy*y+sim.dy/2 for y in range(sim.ny)]
    x = [sim.X0 + sim.dx*x for x in range(sim.nx)]
    y = [sim.Y0 + sim.dy*y for y in range(sim.ny)]
    
    if slope_mode == 'KWE':
        outflow = PFout.calculate_runoff_maps(htop = sim.surf_press,slopex=sim.slope_x_arr,slopey=sim.slope_y_arr,mannings=sim.mannings_arr,
        nx = sim.nx,ny = sim.ny,dx = sim.dx, dy = sim.dy, nt =sim.n_t_clm,mask=sim.mask_arr,KWE='Stan',large_array=True,epsilon = 1E-7)

    else:
       outflow = PFout.calculate_runoff_maps(htop = sim.surf_press,slopex=sim.slope_x_arr,slopey=sim.slope_y_arr,mannings=sim.mannings_arr,
        nx = sim.nx,ny = sim.ny,dx = sim.dx, dy = sim.dy, nt =sim.n_t_clm,mask=sim.mask_arr,KWE='UP',large_array=True,epsilon = 1E-7)
     
    
    for stationname,sta in stadic.items():
        print(sta.name)
        #~ print(sta.x)
        #~ print(sta.y)
        #~ print(sta.x_sim)
        #~ print(sta.y_sim)    
        # now get the outlet location in the PF domain:
        #~ sim.calculate_runoff_from_CLM(x_outlet = np.where(np.array(x) == sta.x_sim),y_outlet = np.where(np.array(y) == sta.y_sim),nlayers = n_coupled_layers+n_CLM_2Dlayers)
        #former
        #~ sim.calculate_runoff_from_press(x_outlet = np.where(np.array(x) == sta.x_sim)[0],y_outlet = np.where(np.array(y) == sta.y_sim)[0])    
        #new
        sim.surfQ = np.swapaxes(outflow,0,1)[sta.y_sim_ind,sta.x_sim_ind,:].flatten()
        #~ print(np.where(np.array(x) == sta.x_sim))
        #~ print(np.where(np.array(y) == sta.y_sim))
        #~ print(np.shape(sim.surfQ))
        # if m3 => m3/s:
        #~ sim.surfQ = [Q/(60*60) for Q in sim.surfQ ]
        """m3/hr (TO CHECK -> seems ok)"""
        #~ sta.Qsim  = sim.create_panda_series(np.reshape(sim.surfQ,sim.n_t_clm),start_date = "1/1/2007", start_hour = "00:00", freq = "1H",periods = sim.n_t_clm)        
        sta.Qsim  = sim.create_panda_series(np.reshape(sim.surfQ,sim.n_t_clm),start_date = "1/1/2005", start_hour = "00:00", freq = "1H",periods = sim.n_t_clm)        
        sta.Qsim[abs(sta.Qsim)>10e10]=np.nan
        """m3/hr -> m3/s"""
        sta.Qsim = sta.Qsim/3600.0
        Qsim[stationname]=sta.Qsim        
        stadic[stationname] = sta
    return sim,stadic,Qsim,x,y, outflow


def plot_host(stadic,P1_sta, P2_sta, plot_yr,method='D',scale=False,scale_free=True,legend=[''],stadic1=0, simuname1='sim1', ax=None):
    """ Function to plot precipitation and Qobs vs. Qsim for 2 stations (Cote_238 and Beterou) for the specific year or years (2005,2006) """
    fig=plt.figure(figsize=[20,5])
#    fig=plt.figure(figsize=[30,20])
#    gs=gridspec.GridSpec(2,1, height_ratios=[1,1])
#    gs=gridspec.GridSpec(1,2, height_ratios=[1,1])
#    gs=gridspec.GridSpec(1,2, height_ratios=[2,1])
#    gs=gridspec.GridSpec(2,2, height_ratios=[1,1])
#    gs=gridspec.GridSpec(2,1, height_ratios=[1,1])
#    gs=gridspec.GridSpec(1,2, height_ratios=[1,1])
    gs=gridspec.GridSpec(1,2, height_ratios=[1])
    host1 = host_subplot(gs[0])
#    plt.subplots_adjust(top=0.9)
    i=0
    j=0
    par1=host1.twinx() #declare second axis for precipitation
    par1.invert_yaxis() # invert precipitation axis
#    res2_vec={}
    res1_vec={}
#    res3_vec={}
#    res4_vec={}
    station={}
    obs_intD_vec={}
    # get Qobs and Qsim for each station
    for stationname,sta in stadic.items():
        # interpolation on sim + daily approach:
        obs = sta.Q.dropna().sort_index(axis=0)
        obs = obs.groupby(obs.index).mean()
        simu1 = stadic1[stationname].Qsim.dropna().sort_index(axis=0)

        #~ obs_int=pd.concat([obs,simu1],axis=1).interpolate(method='slinear',limit=50)[0]
        obs_int=pd.concat([obs,simu1],axis=1).interpolate(method='slinear',limit=50)[pd.concat([obs,simu1],axis=1).columns[0]]
        obs_intD = obs_int.resample('D').mean()
        
        """KGE scores for each simulation"""
        #simu 1: 
        simuD1 = simu1.resample('D').mean()
        merge1=pd.concat([obs_intD.rename('obs'),simuD1.rename('sim')],axis=1)
        kept1 = merge1.dropna()
        res1 = obj.kge(kept1['obs'].values, kept1['sim'].values, return_all=True)

        res1_vec[stationname]=res1
#        res2_vec[stationname]=res2
#        res3_vec[stationname]=res3
#        res4_vec[stationname]=res4
        obs_intD_vec[stationname]=obs_intD
    # print Stationname+associated KGE
#        print(stationname)
#    print(res1)  
#        print(res2)
#        print(res3)
#        print(res4)
    # to control obs 
#    print(obs_intD_vec)
    obs_intD_df=pd.DataFrame.from_dict(obs_intD_vec)
    res1_vec_df=pd.DataFrame.from_dict(res1_vec)
    print(res1_vec_df)
    
    sta1=stadic1['COTE_238']
    sta2=stadic1['BETEROU']

    
    
    """ final plot """
    host1.set_ylabel('$m^{3}/s$', fontsize=16)
    host1.set_xlabel('Time', fontsize=16)
    par1.set_ylabel('precipitation (mm)', fontsize=16)
#    host1.xaxis.set_major_formatter(mdates.DateFormatter("%Y-%m"))
#    par1.xaxis.set_major_formatter(mdates.DateFormatter("%Y-%m"))
#        ax[i].plot(obs_intD.resample('W').mean(),'k')
    p1=host1.plot(obs_intD_df['COTE_238'],'k', label='obs')
            # simulation data
    p2=host1.plot(sta1.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'r', label='%s'% short1)
#    p3=host1.plot(sta2.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'b', label='%s'% short2)
#    p10=host1.plot(sta5.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'g--', label='%s'% short3)
#    p11=host1.plot(sta6.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'y--', label='%s'% short4)
#    par1.set_xlim([pd.to_datetime(datetime.datetime(2005,1,1)),pd.to_datetime(datetime.datetime(2006,12,31))])
#    host1.set_xlim([pd.to_datetime(datetime.datetime(2005,3,1)),pd.to_datetime(datetime.datetime(2005,11,30))])
    host1.set_xlim([pd.to_datetime(datetime.datetime(2006,3,1)),pd.to_datetime(datetime.datetime(2006,12,1))])
    p4=par1.plot(P1_sta, label='Precipitation')
    host1.set_title('Cote', fontsize=20)
    host1.tick_params(axis='y', labelsize=16, which='both')
    host1.tick_params(axis='x', labelsize=14, which='both', bottom='off', top='off', labelbottom='off')
    par1.tick_params(axis='y', labelsize=16, which='both')
#    host1.text(datetime.datetime(2005,4,4),80,'COTE_238: %s: %s: $r^2 = %2.2f$ KGE = %2.2f'%(simuname1,res1_vec_df['COTE_238'][1]*res1_vec_df['COTE_238'][1],res1_vec_df['COTE_238'][0]), Fontsize=15, bbox = {'facecolor':'white', 'alpha':0.5})
#    host1.text(datetime.datetime(2005,3,1),320,'%s: %s: $r^2 = %2.2f$ KGE = %2.2f\n%s: %s: $r^2 = %2.2f$ KGE = %2.2f'%(plot_yr[0], simuname1,res1_vec_df['COTE_238'][1]*res1_vec_df['COTE_238'][1],res1_vec_df['COTE_238'][0],\
#               plot_yr[1],simuname1,res1_vec_df['COTE_238'][3]*res1_vec_df['COTE_238'][3],res1_vec_df['COTE_238'][2]),FontSize=15,bbox = {'facecolor':'white', 'alpha':0.5})
#    host1.text(datetime.datetime(2005,3,1),320,'%s: %s: $r^2 = %2.2f$ KGE = %2.2f'%(plot_yr[0], simuname1,res1_vec_df['COTE_238'][1]*res1_vec_df['COTE_238'][1],res1_vec_df['COTE_238'][0]),FontSize=15,bbox = {'facecolor':'white', 'alpha':0.5})
    host1.text(datetime.datetime(2006,3,1),320,'%s: %s: $r^2 = %2.2f$ KGE = %2.2f'%(plot_yr[1], simuname1,res1_vec_df['COTE_238'][3]*res1_vec_df['COTE_238'][3],res1_vec_df['COTE_238'][2]),FontSize=15,bbox = {'facecolor':'white', 'alpha':0.5})
#    par1.axis["right"].label.set_color(p4.get_color())
#    host2 = host_subplot(212, sharex=host1)    
    host2 = host_subplot(gs[1], sharey=host1)    
    par2=host2.twinx()
    par2.invert_yaxis()
    host2.set_title('Beterou', fontsize=20)
    host2.set_ylabel('$m^{3}/s$', fontsize=16)
    par2.set_ylabel('precipitation (mm)', fontsize=16)
    host2.set_xlabel('Time', fontsize=16)
#    host2.set_xlim([pd.to_datetime(datetime.datetime(2005,3,1)),pd.to_datetime(datetime.datetime(2005,11,30))])
    host2.set_xlim([pd.to_datetime(datetime.datetime(2006,3,1)),pd.to_datetime(datetime.datetime(2006,12,1))])
#    par2.set_xlim([pd.to_datetime(datetime.datetime(2005,1,1)),pd.to_datetime(datetime.datetime(2006,12,31))])
#    host2.plot(obs_intD.resample('D').mean(),'k', label='obs')
    host2.plot(obs_intD_df['BETEROU'],'k', label='obs')
    host2.plot(sta2.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'r', label='%s'% short1)
#    host2.plot(sta4.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'b', label='%s'% short2)
#    host2.plot(sta7.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'g--', label='%s'% short3)
#    host2.plot(sta8.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'y--', label='%s'% short4)
    par2.plot(P2_sta, label='Precipitation')
    host2.tick_params(axis='x', labelsize=14, which='both',top='off')
    host2.tick_params(axis='y', labelsize=16, which='both')
    par2.tick_params(axis='y',labelsize=16, which='both')
#    host2.text(datetime.datetime(2005,4,4),80,'BETEROU: %s: %s: $r^2 = %2.2f$ KGE = %2.2f'%(simuname1,res1_vec_df['BETEROU'][1]*res1_vec_df['BETEROU'][1],res1_vec_df['BETEROU'][0]),FontSize=15,bbox = {'facecolor':'white', 'alpha':0.5})
#    host2.text(datetime.datetime(2005,3,1),320,'%s: %s: $r^2 = %2.2f$ KGE = %2.2f\n%s: %s: $r^2 = %2.2f$ KGE = %2.2f'%(plot_yr[0], simuname1,res1_vec_df['BETEROU'][1]*res1_vec_df['BETEROU'][1],res1_vec_df['BETEROU'][0],\
#               plot_yr[1],simuname1,res1_vec_df['BETEROU'][3]*res1_vec_df['BETEROU'][3],res1_vec_df['BETEROU'][2]),FontSize=15,bbox = {'facecolor':'white', 'alpha':0.5})
#    host2.text(datetime.datetime(2005,3,1),320,'%s: %s: $r^2 = %2.2f$ KGE = %2.2f'%(plot_yr[0], simuname1,res1_vec_df['BETEROU'][1]*res1_vec_df['BETEROU'][1],res1_vec_df['BETEROU'][0]),FontSize=15,bbox = {'facecolor':'white', 'alpha':0.5})
    host2.text(datetime.datetime(2006,3,1),320,'%s: %s: $r^2 = %2.2f$ KGE = %2.2f'%(plot_yr[1], simuname1,res1_vec_df['BETEROU'][3]*res1_vec_df['BETEROU'][3],res1_vec_df['BETEROU'][2]),FontSize=15,bbox = {'facecolor':'white', 'alpha':0.5})
#    par1.axis["right"].label.set_color(p4.get_color())
#    host2.tick_params(axis='x', labelsize=16, which='both')
#    host2.set_xlim([datetime.datetime(plot_yr,4,1),datetime.datetime(plot_yr,11,1)])

    legend2=host2.legend()
    legend=host1.legend()
#    plt.gcf().subplots_adjust(bottom=0.15, top=0.95,left=0.2, hspace=0.001,wspace=0.001)
#    plt.gcf().subplots_adjust(bottom=0.15, top=0.95,right=0.75, left=0.2, hspace=1.0 ,wspace=0.5)
    plt.tight_layout()
#    plt.draw()
#    plt.show()
#    plt.savefig(os.sep.join([path,'Qprecip2005_bench'+'.pdf']),dpi=400,format='pdf')
    plt.savefig(os.sep.join([path,'Qprecip2006_bench'+'.pdf']),dpi=400,format='pdf')
#    obs_intD.to_csv(path + "/obs.csv",index=False)
#    return gs
    return obs_intD_vec

def diag_effiency_test(obs_sim):
#    """ DE score and polar plot """
    obs_arr=obs_sim["Qobs"].values
    sim_arr1=obs_sim["Qsim1"].values
#    sim_arr2=obs_sim["Qsim2"].values
#    sim_arr3=obs_sim["Qsim3"].values
#    sim_arr4=obs_sim["Qsim4"].values
#    idx = ["a)", "b)", "c)", "d)"]
    idx = ["a)"]
    cols = [
    "brel_mean",
    "b_area",
    "temp_cor",
    "de",
    "b_dir",
    "b_slope",
    "phi",
    "beta",
    "alpha",
    "kge",
    "nse",]
    df_eff_cam = pd.DataFrame(index=idx, columns=cols, dtype=np.float64)
    
# =============================================================================
#     Simu1
# =============================================================================
#         # mean relative bias
    brel_mean = de.calc_brel_mean(obs_arr, sim_arr1)
    df_eff_cam.iloc[0, 0] = brel_mean
    # residual relative bias
    brel_res = de.calc_brel_res(obs_arr, sim_arr1)
    # area of relative remaing bias
    b_area = de.calc_bias_area(brel_res)
    df_eff_cam.iloc[0, 1] = b_area
    # temporal correlation
    temp_cor = de.calc_temp_cor(obs_arr, sim_arr1)
    df_eff_cam.iloc[0, 2] = temp_cor
    # diagnostic efficiency
    df_eff_cam.iloc[0, 3] = de.calc_de(obs_arr, sim_arr1)
    # direction of bias
    b_dir = de.calc_bias_dir(brel_res)
    df_eff_cam.iloc[0, 4] = b_dir
    # slope of bias
    b_slope = de.calc_bias_slope(b_area, b_dir)
    df_eff_cam.iloc[0, 5] = b_slope
    # convert to radians
    # (y, x) Trigonometric inverse tangent
    df_eff_cam.iloc[0, 6] = np.arctan2(brel_mean, b_slope)

    # KGE beta
    df_eff_cam.iloc[0, 7] = kge.calc_kge_beta(obs_arr, sim_arr1)
    # KGE alpha
    df_eff_cam.iloc[0, 8] = kge.calc_kge_alpha(obs_arr, sim_arr1)
    # KGE
    df_eff_cam.iloc[0, 9] = kge.calc_kge(obs_arr, sim_arr1)

    # NSE
    df_eff_cam.iloc[0, 10] = nse.calc_nse(obs_arr, sim_arr1)
# =============================================================================
#     Simu2
# =============================================================================

#    brel_mean = de.calc_brel_mean(obs_arr, sim_arr2)
#    df_eff_cam.iloc[1, 0] = brel_mean
#    # residual relative bias
#    brel_res = de.calc_brel_res(obs_arr, sim_arr2)
#    # area of relative remaing bias
#    b_area = de.calc_bias_area(brel_res)
#    df_eff_cam.iloc[1, 1] = b_area
#    # temporal correlation
#    temp_cor = de.calc_temp_cor(obs_arr, sim_arr2)
#    df_eff_cam.iloc[1, 2] = temp_cor
#    # diagnostic efficiency
#    df_eff_cam.iloc[1, 3] = de.calc_de(obs_arr, sim_arr2)
#    # direction of bias
#    b_dir = de.calc_bias_dir(brel_res)
#    df_eff_cam.iloc[1, 4] = b_dir
#    # slope of bias
#    b_slope = de.calc_bias_slope(b_area, b_dir)
#    df_eff_cam.iloc[1, 5] = b_slope
#    # convert to radians
#    # (y, x) Trigonometric inverse tangent
#    df_eff_cam.iloc[1, 6] = np.arctan2(brel_mean, b_slope)
#
#    # KGE beta
#    df_eff_cam.iloc[1, 7] = kge.calc_kge_beta(obs_arr, sim_arr2)
#    # KGE alpha
#    df_eff_cam.iloc[1, 8] = kge.calc_kge_alpha(obs_arr, sim_arr2)
#    # KGE
#    df_eff_cam.iloc[1, 9] = kge.calc_kge(obs_arr, sim_arr2)
    
# =============================================================================
#     Simu 3
# =============================================================================
#    brel_mean = de.calc_brel_mean(obs_arr, sim_arr3)
#    df_eff_cam.iloc[2, 0] = brel_mean
#    # residual relative bias
#    brel_res = de.calc_brel_res(obs_arr, sim_arr3)
#    # area of relative remaing bias
#    b_area = de.calc_bias_area(brel_res)
#    df_eff_cam.iloc[2, 1] = b_area
#    # temporal correlation
#    temp_cor = de.calc_temp_cor(obs_arr, sim_arr3)
#    df_eff_cam.iloc[2, 2] = temp_cor
#    # diagnostic efficiency
#    df_eff_cam.iloc[2, 3] = de.calc_de(obs_arr, sim_arr3)
#    # direction of bias
#    b_dir = de.calc_bias_dir(brel_res)
#    df_eff_cam.iloc[2, 4] = b_dir
#    # slope of bias
#    b_slope = de.calc_bias_slope(b_area, b_dir)
#    df_eff_cam.iloc[2, 5] = b_slope
#    # convert to radians
#    # (y, x) Trigonometric inverse tangent
#    df_eff_cam.iloc[2, 6] = np.arctan2(brel_mean, b_slope)
#
#    # KGE beta
#    df_eff_cam.iloc[2, 7] = kge.calc_kge_beta(obs_arr, sim_arr3)
#    # KGE alpha
#    df_eff_cam.iloc[2, 8] = kge.calc_kge_alpha(obs_arr, sim_arr3)
#    # KGE
#    df_eff_cam.iloc[2, 9] = kge.calc_kge(obs_arr, sim_arr3)
#
#    # NSE
#    df_eff_cam.iloc[2, 10] = nse.calc_nse(obs_arr, sim_arr3)


# =============================================================================
#     Simu 4
# =============================================================================
#    brel_mean = de.calc_brel_mean(obs_arr, sim_arr4)
#    df_eff_cam.iloc[3, 0] = brel_mean
#    # residual relative bias
#    brel_res = de.calc_brel_res(obs_arr, sim_arr4)
#    # area of relative remaing bias
#    b_area = de.calc_bias_area(brel_res)
#    df_eff_cam.iloc[3, 1] = b_area
#    # temporal correlation
#    temp_cor = de.calc_temp_cor(obs_arr, sim_arr4)
#    df_eff_cam.iloc[3, 2] = temp_cor
#    # diagnostic efficiency
#    df_eff_cam.iloc[3, 3] = de.calc_de(obs_arr, sim_arr4)
#    # direction of bias
#    b_dir = de.calc_bias_dir(brel_res)
#    df_eff_cam.iloc[3, 4] = b_dir
#    # slope of bias
#    b_slope = de.calc_bias_slope(b_area, b_dir)
#    df_eff_cam.iloc[3, 5] = b_slope
#    # convert to radians
#    # (y, x) Trigonometric inverse tangent
#    df_eff_cam.iloc[3, 6] = np.arctan2(brel_mean, b_slope)
#
#    # KGE beta
#    df_eff_cam.iloc[3, 7] = kge.calc_kge_beta(obs_arr, sim_arr4)
#    # KGE alpha
#    df_eff_cam.iloc[3, 8] = kge.calc_kge_alpha(obs_arr, sim_arr4)
#    # KGE
#    df_eff_cam.iloc[3, 9] = kge.calc_kge(obs_arr, sim_arr4)
#
#    # NSE
#    df_eff_cam.iloc[3, 10] = nse.calc_nse(obs_arr, sim_arr4)
#         
#    return df_eff_cam, idx



   
#        
#        obs_test.to_csv(path + "/obs_Dmean.csv",index=False)
#        Qsim_test.to_csv(path+"/Qsim_test_Dmean.csv",index=False)
#        obs_arr = obs_test.values
#        sim_arr = Qsim_test.values
#        print(len(obs_arr))
#        print(len(sim_arr))
#        eff_de = de.calc_de(obs_arr, sim_arr)
#        de_polar=de.diag_polar_plot(obs_arr, sim_arr)
#        plt.savefig(os.sep.join([path,'test_polar'+'.pdf']),dpi=400,format='pdf')
#        return de_polar, eff_de, fig

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

"""Get streamflow data Oueme"""

rt_dir = r'D:/Daten/Simus_Gre/data/streamflow/Oueme/data0506'

suf_pattern = '.csv'
#~ pre_pattern = 'CL.Run_Od-'
pre_pattern = 'CL.Run_O-'
# station_list = {'ARA_PONT':2 ,'DONGA_PONT':3,'DONGA_ROUTE_DE_KOLOKONDE':3}
# station_list = {'BORI':1,'COTE_238':1,'IGBOMAKORO':1,'SANI_A_SANI':1,'SARMANGA':1,'TEBOU':1,'WEWE':1,'AFFON_PONT':1,'AGUIMO':1,'AVAL-SANI':1,'BAREROU':1,'BETEROU':1}
station_list = {'COTE_238':1,'BETEROU':1}
station_list= {k: 2 for k, v in station_list.items()}
#first col is time then daily average then hourly

proj = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')

df = pd.DataFrame()
stadic ={}
for stationname,data_column in station_list.items():
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    """m3/s"""
    sta.read_Q(filepattern, data_col = data_column)
    #~ sta.Q = sta.Q*sta.Q.index.to_series().diff().dt.total_seconds().fillna(0)
    """m3/s -> m3/hr"""
    #~ sta.Q = sta.Q*3600.
    sta.Q.dropna(inplace = True)
    #~ sta.Q[np.isnan(sta.Q)] = 500
    sta.Qamount = sta.Q*60*60
    df[stationname]=sta.Q
    print(sta.name)
    filenames = glob.glob(filepattern)    
    sta.read_latlon_from_Qfile(filenames[0])
    sta.x, sta.y = pyproj.transform(geo_system,proj,sta.lon, sta.lat)
    print(sta.lon)
    print(sta.lat)
    print(sta.x)
    print(sta.y)
    stadic[stationname] = sta


""" Get stream network:"""
#~ with rasterio.open("/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/inputs/geometries/Donga/data/Donga_streams_segments_1000m_masked_mean_agreg_D4_pitfilled.tif") as src:
#~ with rasterio.open("/homelocal5/phyrev/_PARFLOW/projects/Donga/active/Donga_1km/input/DEM/Rwatershed/streams.tif") as src:
#~ data,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/inputs/geometries/Oueme/data/Oueme_streams_segments_1000m_masked_mean_agreg_D4_pitfilled.tif",nodata_value = 65535)
# data,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/upg_merit_Benin_UTM_1km_masked_thinned_corr.tif",nodata_value = 255)
data,londem,latdem = procGeodata_Gdal.readRasterWithGdal("D:/Daten/Simus_Gre/data/upg_merit_Benin_UTM_1km_masked_thinned_corr.tif",nodata_value = 255)
[lonlon,latlat]=np.meshgrid(londem,latdem)

""" Find the location of data in the PF framework, and calculate simulated Q:"""
lonlon=lonlon-500
latlat=latlat-500

xx_masked = np.round(lonlon)
yy_masked = np.round(latlat)
#~ xx_masked[data == 0] = -99999999
#~ yy_masked[data == 0] = -99999999
xx_masked[data[::-1,:] == 0] = -99999999
yy_masked[data[::-1,:] == 0] = -99999999
#~ xx_masked[data == 255] = -99999999
#~ yy_masked[data == 255] = -99999999
xx_masked[data[::-1,:] == 255] = -99999999
yy_masked[data[::-1,:] == 255] = -99999999

# Get x_sim and y_sim by matching the station location and the simulation grid
# (here the simulation grid is the same as the one for the stream network, which
#further gives the stream cells that have been topographically processed as such")
for stationname,sta in stadic.items():
    print(sta.name)
    #find the closest point in the grid:  
    dist_to_river = np.sqrt((xx_masked - sta.x)**2 + (yy_masked - sta.y)**2)
    dist = np.sqrt((np.round(lonlon)- sta.x)**2 + (np.round(latlat) - sta.y)**2)
    # BH: mars 2020:
    if (dist_to_river.min() <= 2*np.sqrt(dx*dx + dy*dy)):
    #~ if (dist_to_river.min() < np.sqrt(sim.dx*sim.dx + sim.dy*sim.dy)):
        ind_min = np.where(dist_to_river == dist_to_river.min())
        print('river cell found')
    else:
        ind_min = np.where(dist == dist.min())   
        print('no river cell found')
    sta.y_sim_ind = ind_min[0]
    sta.x_sim_ind = ind_min[1]
    sta.x_sim = np.round(lonlon[ind_min])
    sta.y_sim = np.round(latlat[ind_min])
    stadic[stationname] = sta
    print(sta.y_sim_ind, sta.x_sim_ind)


#sim1,stadic1,Qsim1,x,y,outflow1 = get_streamflow_simulation_results(simdir1,simuname1,copy.deepcopy(stadic),slope_mode='OF',manning=1e-6)
sim1,stadic1,Qsim1,x,y,outflow1 = get_streamflow_simulation_results(simdir1,simuname1,copy.deepcopy(stadic),slope_mode='KWE',manning=3e-6)
#sim2,stadic2,Qsim2,x,y,outflow2 = get_streamflow_simulation_results(simdir2,simuname2,copy.deepcopy(stadic),slope_mode='KWE',manning=3e-6)
##sim2,stadic2,Qsim2,x,y,outflow2 = get_streamflow_simulation_results(simdir2,simuname2,copy.deepcopy(stadic),slope_mode='OF',manning=1e-6)
#sim3,stadic3,Qsim3,x,y,outflow3 = get_streamflow_simulation_results(simdir3,simuname3,copy.deepcopy(stadic),slope_mode='KWE',manning=3e-6)
##
### sim3,stadic3,Qsim3,x,y = sim1,stadic1,Qsim1,x,y
#sim4,stadic4,Qsim4,x,y,outflow4 = get_streamflow_simulation_results(simdir4,simuname4,copy.deepcopy(stadic),slope_mode='KWE',manning=3e-6)

#sim = sim1
#stadic = stadic1
#Qsim = Qsim1

# cumulative=pd.DataFrame()
# cumulative=Qsim.cumsum()
# sim = sim2
# stadic = stadic2
# Qsim = Qsim2

Qsim_05=Qsim1[0:8760]
Qsim_06=Qsim1[8760:]

Q2005_bet, maxQ05bet, minQ05bet=rain_nonzero(Qsim_05['BETEROU'])
Q2005_cot, maxQ05cot, minQ05cot=rain_nonzero(Qsim_05['COTE_238'])
obs2006_bet, maxQ06bet, minQ06bet=rain_nonzero(Qsim_06['BETEROU'])
obs2006_cot, maxQ06cot, minQ06cot=rain_nonzero(Qsim_06['COTE_238'])
#print('sta.y_sim_ind, sta.x_sim_ind)

""" Precip Test Plots"""

#P2_sta_test['beterou'].plot()
#P2_sta_test['cote'].plot()
#P1_sta_test['cote'].plot()
#P1_sta_test['beterou'].plot()
#P1_cote.plot()
#P2_cote.plot()

# =============================================================================
# Plot Streamflow Timeseries + Precip 
# =============================================================================
"""
plot time series
"""
#polar_test, de_test=plot_host(stadic,P1_sta=Precip_all['cote'], P2_sta=Precip_all['beterou'],plot_yr=[2005,2006],method='D',scale=False,scale_free=True,legend=[''],stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4, simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4)
#exp1=plot_host(stadic,P1_sta=Precip_all['cote'], P2_sta=Precip_all['beterou'],plot_yr=[2005,2006],method='D',scale=False,scale_free=True,legend=[''],stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4, simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4)
##plot_host(stadic,P1_sta=P1_sta_test, P2_sta=P2_sta_test,plot_yr=[2005,2006],method='D',scale=False,scale_free=True,legend=[''],stadic1=stadic1,stadic2=stadic2,simuname1=simuname1,simuname2=simuname2)

#plot_time_series(stadic,P1_sta=P1_sta_test, P2_sta=P2_sta_test,plot_yr=2005,method='D',scale=False,scale_free=True,legend=[''],stadic1=stadic1,stadic2=stadic2,simuname1=simuname1,simuname2=simuname2)


obs_vec1=plot_host(stadic,P1_sta=Precip_all['cote'], P2_sta=Precip_all['beterou'],plot_yr=[2005,2006],method='D',scale=False,scale_free=True,legend=[''],stadic1=stadic1, simuname1=simuname1)


obs_frame=pd.DataFrame(obs_vec1)

compmax05bet=obs_frame['BETEROU'][obs_frame.index=='2005-10-11']
compmax06bet=obs_frame['BETEROU'][obs_frame.index=='2006-09-05']
compmin05bet=obs_frame['BETEROU'][obs_frame.index=='2005-06-23']
compmin06bet=obs_frame['BETEROU'][obs_frame.index=='2006-11-15']
compmax05cot=obs_frame['COTE_238'][obs_frame.index=='2005-09-29']
compmax06cot=obs_frame['COTE_238'][obs_frame.index=='2006-09-17']
compmin05cot=obs_frame['COTE_238'][obs_frame.index=='2005-06-23']
compmin06cot=obs_frame['COTE_238'][obs_frame.index=='2006-07-27']


#obs_2005=obs_frame[0:365]
#obs2005_bet, maxobs05bet, minobs05bet=rain_nonzero(obs_2005['BETEROU'])
#obs2005_cot, maxobs05cot, minobs05cot=rain_nonzero(obs_2005['COTE_238'])
#obs_2006=obs_frame[365:]
#obs2006_bet, maxobs06bet, minobs06bet=rain_nonzero(obs_2006['BETEROU'])
#obs2006_cot, maxobs06cot, minobs06cot=rain_nonzero(obs_2006['COTE_238'])

minQsBet=minQ05bet.append(minQ06bet)
minQsBet=pd.DataFrame(minQsBet)
minQsBet=minQsBet.reset_index()
minQsBet['index']=pd.to_datetime(minQsBet['index']).dt.date
minQsBet= minQsBet.set_index('index')
minQsCot=minQ05cot.append(minQ06cot)
minQsCot=pd.DataFrame(minQsCot)
minQsCot=minQsCot.reset_index()
minQsCot['index']=pd.to_datetime(minQsCot['index']).dt.date
minQsCot= minQsCot.set_index('index')
maxQsBet=maxQ05bet.append(maxQ06bet)
maxQsBet=pd.DataFrame(maxQsBet)
maxQsBet=maxQsBet.reset_index()
maxQsBet['index']=pd.to_datetime(maxQsBet['index']).dt.date
maxQsBet= maxQsBet.set_index('index')
maxQsCot=maxQ05cot.append(maxQ06cot)
maxQsCot=pd.DataFrame(maxQsCot)
maxQsCot=maxQsCot.reset_index()
maxQsCot['index']=pd.to_datetime(maxQsCot['index']).dt.date
maxQsCot= maxQsCot.set_index('index')
#minobsBet=minobs05bet.append(minobs06bet)
#minobsCot=minobs05cot.append(minobs06cot)
#maxobsBet=maxobs05bet.append(maxobs06bet)
#maxobsCot=maxobs05cot.append(maxobs06cot)
minobsBet=compmin05bet.append(compmax06bet)
minobsBet=minobsBet.reset_index()
minobsBet['index']=pd.to_datetime(minobsBet['index']).dt.date
minobsBet= minobsBet.set_index('index')
minobsCot=compmin05cot.append(compmax06cot)
minobsCot=minobsCot.reset_index()
minobsCot['index']=pd.to_datetime(minobsCot['index']).dt.date
minobsCot= minobsCot.set_index('index')
maxobsBet=compmax05bet.append(compmax06bet)
maxobsBet=maxobsBet.reset_index()
maxobsBet['index']=pd.to_datetime(maxobsBet['index']).dt.date
maxobsBet= maxobsBet.set_index('index')
maxobsCot=compmax05cot.append(compmax06cot)
maxobsCot=maxobsCot.reset_index()
maxobsCot['index']=pd.to_datetime(maxobsCot['index']).dt.date
maxobsCot= maxobsCot.set_index('index')

dic_bet={'MaxQ':[maxQsBet], 'MinQ':[minQsBet], 'Maxobs':[maxobsBet], 'Minobs':[minobsBet]}
#dic_cot={'year': [2005,2006], 'MaxQ':[maxQ05cot,maxQ06cot], 'MinQ':[minQ05cot, minQ06cot], 'Maxobs':[maxobs05cot, minobs05cot], 'Minobs':[minobs05cot, minobs06cot]}
#bet_frame=pd.DataFrame(dic_bet)




#plt.savefig(os.sep.join([path,'test_Qprecip20052006'+'.pdf']),dpi=400,format='pdf')


""" Test with Plot subplots--> nor working """
#fig = plt.figure(figsize = (30, 30), facecolor = "white")
#fig=plt.figure(figsize=[30,40])
#ax1 = fig.add_subplot(2,1,1)
#ax2 = fig.add_subplot(2,1,2)
#    gs=gridspec.GridSpec(2,1, height_ratios=[1,1])
#    gs=gridspec.GridSpec(1,2, height_ratios=[1,1])
#    gs=gridspec.GridSpec(1,2, height_ratios=[2,1])
#gs=gridspec.GridSpec(2,1)
#plt.subplots_adjust(top=0.9)
#fax1=fig1.add_subplot(gs[0])
#fax2=fig1.add_subplot(gs[1])
#gs1=plot_host(stadic,P1_sta=Precip_all['cote'], P2_sta=Precip_all['beterou'],plot_yr=[2005,2006],method='D',scale=False,scale_free=True,legend=[''],stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4, simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4, ax=ax1)

#gs2=plot_host(stadic,P1_sta=Precip_all['cote'], P2_sta=Precip_all['beterou'],plot_yr=[2005,2006],method='D',scale=False,scale_free=True,legend=[''],stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4, simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4, ax=ax2)
#plt.show()
#fig.tight_layout()
#fig.savefig(os.sep.join([path,'test_Qprecip20052006_2x1'+'.pdf']),dpi=400,format='pdf')

# =============================================================================
# Calculate DE and plot polar plot
# =============================================================================
""" prepare DE:"""
Qsim1_D=Qsim1.sort_index(axis=0).resample('D').mean()
#Qsim2_D=Qsim2.sort_index(axis=0).resample('D').mean()
#Qsim3_D=Qsim3.sort_index(axis=0).resample('D').mean()
#Qsim4_D=Qsim4.sort_index(axis=0).resample('D').mean()

obs_sim_bet=pd.DataFrame(index=Qsim1_D.index, columns=["Qsim1", "Qobs"])
obs_sim_bet.loc[:,"Qsim1"]=Qsim1_D['BETEROU'].values
#obs_sim_bet.loc[:,"Qsim2"]=Qsim2_D['BETEROU'].values
#obs_sim_bet.loc[:,"Qsim3"]=Qsim3_D['BETEROU'].values
#obs_sim_bet.loc[:,"Qsim4"]=Qsim4_D['BETEROU'].values
obs_sim_bet.loc[:, "Qobs"] = obs_vec1['BETEROU'].values
obs_sim_bet=obs_sim_bet.dropna()
obs_sim_bet.to_csv(path+"/obs_sim_bet_bench.csv", header=True, index=True, sep=";")

obs_sim_cot=pd.DataFrame(index=Qsim1_D.index, columns=["Qsim1", "Qobs"])
obs_sim_cot.loc[:,"Qsim1"]=Qsim1_D['COTE_238'].values
#obs_sim_cot.loc[:,"Qsim2"]=Qsim2_D['COTE_238'].values
#obs_sim_cot.loc[:,"Qsim3"]=Qsim3_D['COTE_238'].values
#obs_sim_cot.loc[:,"Qsim4"]=Qsim4_D['COTE_238'].values
obs_sim_cot.loc[:, "Qobs"] = obs_vec1['COTE_238'].values
obs_sim_cot=obs_sim_cot.dropna()
obs_sim_cot.to_csv(path+"/obs_sim_cot_bench.csv", header=True, index=True, sep=";")

df_eff_bet, idx=diag_effiency_test(obs_sim_bet)
df_eff_bet.to_csv(path+"/df_eff_bet_bench.csv", header=True, index=True, sep=";")
df_eff_cot, idx2=diag_effiency_test(obs_sim_cot)
df_eff_cot.to_csv(path+"/df_eff_cot_bench.csv", header=True, index=True, sep=";")
### multi diagnostic plot ###
# make arrays
brel_mean_arr = df_eff_bet["brel_mean"].values
b_area_arr = df_eff_bet["b_area"].values
temp_cor_arr = df_eff_bet["temp_cor"].values
b_dir_arr = df_eff_bet["b_dir"].values
de_arr = df_eff_bet["de"].values
phi_arr = df_eff_bet["phi"].values
b_slope_arr = df_eff_bet["b_slope"].values

alpha_arr = df_eff_bet["alpha"].values
beta_arr = df_eff_bet["beta"].values
kge_arr = df_eff_bet["kge"].values

""" Polar Plots """

""" Polar Plots DE"""
fig_de_bet = util.diag_polar_plot_multi_fc(
    brel_mean_arr,
    b_area_arr,
    temp_cor_arr,
    de_arr,
    b_dir_arr,
    phi_arr,
    idx,
    ax_lim=0,
)

fig_de_bet.savefig(os.sep.join([path,'test_polar_bet_bench'+'.pdf']),dpi=400,format='pdf')

fig_kge = util.polar_plot_multi_fc(
        beta_arr, alpha_arr, temp_cor_arr, kge_arr, idx, ax_lim=0
    )

fig_kge.savefig(os.sep.join([path,'test_KGE_bet_bench'+'.pdf']),dpi=400,format='pdf')




brel_mean_arr = df_eff_cot["brel_mean"].values
b_area_arr = df_eff_cot["b_area"].values
temp_cor_arr = df_eff_cot["temp_cor"].values
b_dir_arr = df_eff_cot["b_dir"].values
de_arr = df_eff_cot["de"].values
phi_arr = df_eff_cot["phi"].values
b_slope_arr = df_eff_cot["b_slope"].values

alpha_arr = df_eff_cot["alpha"].values
beta_arr = df_eff_cot["beta"].values
kge_arr = df_eff_cot["kge"].values

fig_de_cot = util.diag_polar_plot_multi_fc(
    brel_mean_arr,
    b_area_arr,
    temp_cor_arr,
    de_arr,
    b_dir_arr,
    phi_arr,
    idx2,
    ax_lim=0,
)

fig_de_cot.savefig(os.sep.join([path,'test_polar_cot_bench'+'.pdf']),dpi=400,format='pdf')


fig_kge_cot = util.polar_plot_multi_fc(
        beta_arr, alpha_arr, temp_cor_arr, kge_arr, idx2, ax_lim=0
    )

fig_kge_cot.savefig(os.sep.join([path,'test_KGE_cot_bench'+'.pdf']),dpi=400,format='pdf')
