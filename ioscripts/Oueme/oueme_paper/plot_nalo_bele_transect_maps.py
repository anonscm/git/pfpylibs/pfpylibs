#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    Plotting script for Oueme sens. analysis paper:
    this will plot the WT transect locations in Nalohou & bellefoungou

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime, os, glob, copy
import pyproj
import matplotlib.patches as mpatches
import matplotlib as mpl
from matplotlib.colors import LightSource

import shapefile
import gdal, ogr
from osgeo.gdalconst import *
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
from shapely.geometry import Point
from spatialobj import Points

"""local imports:"""
from procsycz import readDataAMMA as rdA
from procsycz import procGeodata_Gdal

plt.close('all')
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
""" projection system """
proj = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def read_plot_shapefile(filename,ax):
    #~ sf = shapefile.Reader(filename)
    #for some reason after fresh install of shapefile library (pip install pyshp), the following spec is needed
    sf = shapefile.Reader(filename,encoding = 'latin-1')
    recs    = sf.records()
    shapes  = sf.shapes()
    Nshp    = len(shapes)
    cns     = []
    for nshp in range(Nshp):
        cns.append(recs[nshp][1])
    cns = np.array(cns)
    cm    = plt.get_cmap('Dark2')
    cccol = cm(1.*np.arange(Nshp)/Nshp)
    #   -- plot --
    #~ fig     = plt.figure()
    #~ ax      = fig.add_subplot(111)
    for nshp in range(Nshp):
        ptchs   = []
        pts     = np.array(shapes[nshp].points)
        prt     = shapes[nshp].parts
        par     = list(prt) + [pts.shape[0]]
        for pij in range(len(prt)):
            ptchs.append(Polygon(pts[par[pij]:par[pij+1]]))
        #~ ax.add_collection(PatchCollection(ptchs,facecolor=cccol[nshp,:],edgecolor='k', linewidths=.1))
        ax.add_collection(PatchCollection(ptchs,facecolor=[0,0,1,0],edgecolor='k', linewidths=.1))
    return ax
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

"""Read in Data"""
######### Oueme
#~ WTOu= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/WTOu.csv')
#~ WTOu= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/processed_data/WTOu_added_uncheckeddata_12h.csv')
WTOu= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/processed_data/WT_Oueme_smoothedDailyMin.csv')
WTOu = WTOu.set_index(WTOu.columns[0])
WTOu.index = pd.to_datetime(WTOu.index,format='%Y-%m-%d %H:%M:%S')
WTOu.index.rename('Date',inplace=True)
######### oueme transect (belle, naloh, etc)
WTOut= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/processed_data/WT_Oueme_transect_smoothedDailyMean.csv')
WTOut = WTOut.set_index(WTOut.columns[0])
WTOut.index = pd.to_datetime(WTOut.index,format='%Y-%m-%d %H:%M:%S')
WTOut.index.rename('Date',inplace=True)
WTOut.drop(['Nalo_P034_20_2013-2015','Nalo_P190_20_2013-2015'],axis=1,inplace=True)
WTOut = WTOut.where(WTOut<1000,np.nan)   
#~ stations_to_keep = ['NALO-P034-10','NALO-P190-11','NALO-P500-10','Bele_P0099_120','Bele_P0192_120','Bele_P0312_100','Bele_P0688_22','Bele_P0968_24','Bele_P1250_21']
#~ stations_to_keep = ['Bele_P0099_120','Bele_P0192_120','Bele_P0312_100','Bele_P0688_22','Bele_P0968_24','Bele_P1250_21']
stations_to_keep_bele = ['Bele_P0099_120','Bele_P0192_120','Bele_P0312_100','Bele_P0464_100','Bele_P0688_22','Bele_P0968_24','Bele_P1250_21']
#~ WTOut=WTOut[stations_to_keep]
stations_to_keep_nalo =['NALO-P005-12','NALO-P034-02','NALO-P034-10','NALO-P190-02','NALO-P190-11','NALO-P500-2','NALO-P500-10','NALO-P500-18']
WT=pd.concat([WTOu,WTOut],axis=1)

WT=-WT

######### METADATA
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/WTOuSta.csv')
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/WTOuSta_added_uncheckeddata_12h.csv')
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/WTOuSta_added_uncheckeddata_12h_added_precip_from_AC.csv')
WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/processed_data/WTOuSta_added_uncheckeddata_12h_added_precip_from_AC.csv')
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/WTOuSta_added_uncheckeddata_12h_15Dmedianmov_window_added_precip_from_AC.csv')
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/WTOuSta_smoothedDailyMin.csv')
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/WTOuSta_smoothedDailyMin_P001deg_1day.csv')
WTOuSta = WTOuSta.set_index(WTOuSta.columns[0])

WTOuSta.loc['NALO-P005-12',['lon','lat']]=[1.6048,9.7469]
WTOuSta.loc['NALO-P034-02',['lon','lat']]=[1.6049,9.7466]
WTOuSta.loc['NALO-P034-10',['lon','lat']]=[1.6049,9.7466]
WTOuSta.loc['NALO-P190-02',['lon','lat']]=[1.6053,9.7454]
WTOuSta.loc['NALO-P190-11',['lon','lat']]=[1.6053,9.7454]
WTOuSta.loc['NALO-P500-2',['lon','lat']]=[1.6064,9.743]
WTOuSta.loc['NALO-P500-10',['lon','lat']]=[1.6064,9.743]
WTOuSta.loc['NALO-P500-18',['lon','lat']]=[1.6064,9.743]
WTOuSta.loc[stations_to_keep_nalo,'x'], WTOuSta.loc[stations_to_keep_nalo,'y'] = pyproj.transform(geo_system,proj,np.array(WTOuSta.loc[stations_to_keep_nalo,'lon']) ,np.array(WTOuSta.loc[stations_to_keep_nalo,'lat']) )

stadic ={}
#create a dictionnary of station objects
for stationname in WT.columns:
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    sta.WT=WT[stationname]

    sta.x=WTOuSta.loc[stationname,'x'] 
    sta.y=WTOuSta.loc[stationname,'y'] 
    stadic[stationname]=sta
    
stadic['Bele_P0099_120'].z=403.35-1.35
stadic['Bele_P0192_120'].z=408.65-0.76
stadic['Bele_P0312_100'].z=412.56-0.8
stadic['Bele_P0464_100'].z=416.59-0.8
stadic['Bele_P0688_22'].z=423.4-0.8
stadic['Bele_P0968_24'].z=431.68-0.8
stadic['Bele_P1250_21'].z=428.9-0.93
stadic['NALO-P005-12'].z=427.41
stadic['NALO-P034-02'].z=430.17
stadic['NALO-P034-10'].z=430.13
stadic['NALO-P190-02'].z=439.52
stadic['NALO-P190-11'].z=439.64
stadic['NALO-P500-2'].z=446.3
stadic['NALO-P500-10'].z=446.3
stadic['NALO-P500-18'].z=446.32
WTOuSta.loc['Bele_P0099_120','z']=403.35-1.35
WTOuSta.loc['Bele_P0192_120','z']=408.65-0.76
WTOuSta.loc['Bele_P0312_100','z']=412.56-0.8
WTOuSta.loc['Bele_P0464_100','z']=416.59-0.8
WTOuSta.loc['Bele_P0688_22','z']=423.4-0.8
WTOuSta.loc['Bele_P0968_24','z']=431.68-0.8
WTOuSta.loc['Bele_P1250_21','z']=428.9-0.93
WTOuSta.loc['NALO-P005-12','z']=427.41
WTOuSta.loc['NALO-P034-02','z']=430.17
WTOuSta.loc['NALO-P034-10','z']=430.13
WTOuSta.loc['NALO-P190-02','z']=439.52
WTOuSta.loc['NALO-P190-11','z']=439.64
WTOuSta.loc['NALO-P500-2','z']=446.3
WTOuSta.loc['NALO-P500-10','z']=446.3
WTOuSta.loc['NALO-P500-18','z']=446.32

"""ET flux tower locations"""
x_ET_nalohou = 1.6
y_ET_nalohou = 9.74
x_ET_Bellefoungou = 1.718
y_ET_Bellefoungou = 9.791
x_ET_nalohou_UTM, y_ET_nalohou_UTM = pyproj.transform(geo_system,proj,x_ET_nalohou, y_ET_nalohou)
# actually

x_ET_nalohou_UTM =346922.549
y_ET_nalohou_UTM = 1077471.373

x_ET_Bellefoungou_UTM, y_ET_Bellefoungou_UTM = pyproj.transform(geo_system,proj,x_ET_Bellefoungou, y_ET_Bellefoungou)
x_ET_UTM=[x_ET_nalohou_UTM,x_ET_Bellefoungou_UTM]
y_ET_UTM=[y_ET_nalohou_UTM,y_ET_Bellefoungou_UTM]

""" Get stream network:"""
data,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/upg_merit_Benin_UTM_1km_masked_thinned_corr.tif",nodata_value = 255)
[lonlon,latlat]=np.meshgrid(londem,latdem)


#N-W
#~ ls = LightSource(azdeg=315, altdeg=45)  
#N-E good for Bele
ls = LightSource(azdeg=45, altdeg=45)  
""" Get HR DEM:"""
demHR,xdem,ydem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/DATA/GeoRefs/Oueme/Oueme/DEM/elv_merit_oueme_UTM.tif",nodata_value = 9999)
demHR=demHR[::-1,:]
[xxdem,yydem]=np.meshgrid(xdem,ydem)
demHRhill=ls.hillshade(demHR,vert_exag=100,dx=1000,dy=1000)


"""
plot station map: Bellefoungou
#~ https://stackoverflow.com/questions/40899350/python-quiver-and-pcolormesh-not-lining-up-exactly-right     
"""
fig = plt.figure(figsize=(10,8))
ax = fig.add_subplot(111)
ax.set_aspect(1)
# OK for bele for HR DEM:
#~ p0 = ax.pcolormesh(xxdem, yydem, demHR,vmin=380,vmax=440, cmap='gray')
p0 = ax.pcolormesh(xxdem, yydem, demHR,vmin=390,vmax=430, cmap='gray_r')
#
#~ p0 = ax.pcolormesh(xxdem, yydem, demHRhill, cmap='gray')
# OK for bele for simulation DEM:
#~ p0=P1.hillshade.plot(ax=ax,cmap='gray',add_colorbar=False)  
#~ p0=P1.DEM.plot(ax=ax,cmap='gray',vmin=380,vmax=410,add_colorbar=False)  
#alpha 0.3 to compare with higher resolution local DEM or 0.1 to compare with similar grid dem (should actually hatch it)
#~ p2=P1.veg_map.plot(ax=ax,cmap=plt.cm.get_cmap("YlGn_r",3), alpha=0.3,add_colorbar=False)  
#~ cb = plt.colorbar(p2,ticks=[1,2,3])
#~ cb.set_label('vegetation class',fontsize=20)
#~ cb.ax.set_yticklabels(['Trees', 'Open Savanna', 'Herbaceous'],fontsize = 14)
#~ cb.solids.set(alpha=1)
cb2 = plt.colorbar(p0)
cb2.set_label(label='relative elevation (m)',fontsize=20)

ax.set_xlabel('m',fontsize=20)
ax.set_ylabel('m',fontsize=20)


ax.set_ylim([y_ET_Bellefoungou_UTM-300,y_ET_Bellefoungou_UTM+1100])
ax.set_xlim([x_ET_Bellefoungou_UTM-1100,x_ET_Bellefoungou_UTM+200])
#~ ax.set_ylim([np.min(latlat)-10000,np.max(latlat)+5000])
#~ ax.plot([sta.x for stationname,sta in stadic.items()],[sta.y for stationname,sta in stadic.items()],'k+',markersize=18,markeredgewidth=4)

#~ l1, = ax.plot([sta.x_sim for stationname,sta in stadic.items()],[sta.y_sim for stationname,sta in stadic.items()],'r+',markersize=18,markeredgewidth=4)
#~ l2, = ax.plot([sta.x for stationname,sta in stadic_WT.items()],[sta.y for stationname,sta in stadic_WT.items()],'b+',markersize=18,markeredgewidth=4)
l1, = ax.plot(WTOuSta.loc[stations_to_keep_bele,'x'],WTOuSta.loc[stations_to_keep_bele,'y'],'bo',markersize=12,markeredgewidth=4)

"""station names for Q stations:"""
#~ props = dict(boxstyle='round', facecolor='white', alpha=0.5)
#~ for stationname,sta in stadic.items():
    #~ if stationname=='AFFON_PONT':
        #~ ax.text(sta.x_sim, sta.y_sim+3000,stationname,fontsize = 12, bbox=props)
    #~ else:
        #~ ax.text(sta.x_sim, sta.y_sim,stationname,fontsize = 12, bbox=props)
"""station names for WT stations:"""
props = dict(boxstyle='round', facecolor='white', alpha=0.5)
#~ for stationname,sta in stadic_WT.items():
#~ for stationname,sta in stadic.items():
    #~ ax.text(sta.x, sta.y,stationname,fontsize = 6, bbox=props)
	
l3, = ax.plot(x_ET_UTM,y_ET_UTM,'y2',markersize=24,markeredgewidth=4,zorder = 10000)

#~ ax.legend((l1,l2,l3),('Streamgauges','Wells','Flux towers'),fontsize=14,loc='lower left',ncol=3)
ax.legend((l1,l3),('Piezometers','Flux tower'),fontsize=20,loc='upper left',ncol=2)

""" change x y tick labels and colormap range
canvas should be drawn (or plt.show() to get non empty xtick labels)"""
fig.canvas.draw()
xlabels = [int(a.get_text()) for a in ax.get_xticklabels()]
ax.set_xticklabels([str(int(x)) for x in np.array(xlabels)-min(xlabels)],fontsize=20)

ylabels = [int(a.get_text()) for a in ax.get_yticklabels()]
ax.set_yticklabels([str(int(x)) for x in np.array(ylabels)-min(ylabels)],fontsize=20)

cb2.ax.set_yticklabels([int(a) for a in cb2.get_ticks() - min(cb2.get_ticks())],fontsize=20)

###### THIS IS THE slow PART of the code #######
sf = shapefile.Reader('/home/hectorb/DATA/GeoRefs/Oueme/Oueme/MESO_OUEME_HYDR_RIVIERE-5km2-SRTM_Vr_UTM31N.shp',encoding = 'latin-1')
for nshp in range(len(sf.records())):
    ax.plot([p[0] for p in sf.shape(nshp).points],[p[1] for p in sf.shape(nshp).points],'b',linewidth=2)

plt.savefig('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/outputs/Bele_transect.png',dpi=400,format='png')


"""
plot station map: Nalohou
#~ https://stackoverflow.com/questions/40899350/python-quiver-and-pcolormesh-not-lining-up-exactly-right     
"""
fig = plt.figure(figsize=(10,8))
ax = fig.add_subplot(111)
ax.set_aspect(1)
# OK for bele for HR DEM:
#~ p0 = ax.pcolormesh(xxdem, yydem, demHR,vmin=380,vmax=440, cmap='gray')
p0 = ax.pcolormesh(xxdem, yydem, demHR,vmin=425,vmax=465, cmap='gray_r')
#
#~ p0 = ax.pcolormesh(xxdem, yydem, demHRhill, cmap='gray')
# OK for bele for simulation DEM:
#~ p0=P1.hillshade.plot(ax=ax,cmap='gray',add_colorbar=False)  
#~ p0=P1.DEM.plot(ax=ax,cmap='gray',vmin=380,vmax=410,add_colorbar=False)  
#alpha 0.3 to compare with higher resolution local DEM or 0.1 to compare with similar grid dem (should actually hatch it)
#~ p2=P1.veg_map.plot(ax=ax,cmap=plt.cm.get_cmap("YlGn_r",3), alpha=0.3,add_colorbar=False)  
#~ cb = plt.colorbar(p2,ticks=[1,2,3])
#~ cb.set_label('vegetation class',fontsize=20)
#~ cb.ax.set_yticklabels(['Trees', 'Open Savanna', 'Herbaceous'],fontsize = 14)
#~ cb.solids.set(alpha=1)
cb2 = plt.colorbar(p0)
cb2.set_label(label='relative elevation (m)',fontsize=20)
ax.set_xlabel('m',fontsize=20)
ax.set_ylabel('m',fontsize=20)


ax.set_ylim([y_ET_nalohou_UTM-500,y_ET_nalohou_UTM+800])
ax.set_xlim([x_ET_nalohou_UTM-500,x_ET_nalohou_UTM+800])
#~ ax.set_ylim([np.min(latlat)-10000,np.max(latlat)+5000])
#~ ax.plot([sta.x for stationname,sta in stadic.items()],[sta.y for stationname,sta in stadic.items()],'k+',markersize=18,markeredgewidth=4)

#~ l1, = ax.plot([sta.x_sim for stationname,sta in stadic.items()],[sta.y_sim for stationname,sta in stadic.items()],'r+',markersize=18,markeredgewidth=4)
#~ l2, = ax.plot([sta.x for stationname,sta in stadic_WT.items()],[sta.y for stationname,sta in stadic_WT.items()],'b+',markersize=18,markeredgewidth=4)
l1, = ax.plot(WTOuSta.loc[stations_to_keep_nalo,'x'],WTOuSta.loc[stations_to_keep_nalo,'y'],'bo',markersize=12,markeredgewidth=4)

"""station names for Q stations:"""
#~ props = dict(boxstyle='round', facecolor='white', alpha=0.5)
#~ for stationname,sta in stadic.items():
    #~ if stationname=='AFFON_PONT':
        #~ ax.text(sta.x_sim, sta.y_sim+3000,stationname,fontsize = 12, bbox=props)
    #~ else:
        #~ ax.text(sta.x_sim, sta.y_sim,stationname,fontsize = 12, bbox=props)
"""station names for WT stations:"""
props = dict(boxstyle='round', facecolor='white', alpha=0.5)
#~ for stationname,sta in stadic_WT.items():
#~ for stationname,sta in stadic.items():
    #~ ax.text(sta.x, sta.y,stationname,fontsize = 6, bbox=props)
	
l3, = ax.plot(x_ET_UTM,y_ET_UTM,'y2',markersize=24,markeredgewidth=4,zorder = 10000)

#~ ax.legend((l1,l2,l3),('Streamgauges','Wells','Flux towers'),fontsize=14,loc='lower left',ncol=3)
ax.legend((l1,l3),('Piezometers','Flux tower'),fontsize=20,loc='upper left',ncol=2)

""" change x y tick labels and colormap range
canvas should be drawn (or plt.show() to get non empty xtick labels)"""
fig.canvas.draw()
xlabels = [int(a.get_text()) for a in ax.get_xticklabels()]
ax.set_xticklabels([str(int(x)) for x in np.array(xlabels)-min(xlabels)],fontsize=20)

ylabels = [int(a.get_text()) for a in ax.get_yticklabels()]
ax.set_yticklabels([str(int(x)) for x in np.array(ylabels)-min(ylabels)],fontsize=20)

cb2.ax.set_yticklabels([int(a) for a in cb2.get_ticks() - min(cb2.get_ticks())],fontsize=20)

sf = shapefile.Reader('/home/hectorb/DATA/GeoRefs/Oueme/Ara/LOCAL_ARA_HYDR_RAVINE-PRINCIPALE-GPS_V_UTM31N_LIGNE.shp',encoding = 'latin-1')
for nshp in range(len(sf.records())):
    ax.plot([p[0] for p in sf.shape(nshp).points],[p[1] for p in sf.shape(nshp).points],'b',linewidth=2)

plt.savefig('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/outputs/Nalohou_transect.png',dpi=400,format='png')
