#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Prepare vegetation map for Oueme
    mimic the former matlab version
    

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os,glob,copy
from netCDF4 import Dataset
import time
import datetime
import pyproj
import rasterio
import scipy.spatial as ann
from matplotlib.dates import MonthLocator, DateFormatter

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import drv_vegm_io as vegio
from PFlibs import io_pfb
from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
from procsycz import procGeodata_Gdal



##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##


##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def ann_tree(x, y, X, Y, nb=1):
    # generic tree function for a list of points (can originate from a structured grid or not)
    tree = ann.KDTree(np.array([(i,j) for i,j in zip(x, y)]))
    # res returns the (one, nb =1) closest (the cell) source point (x,y) from each destination point (X,Y)
    res = tree.query(np.array([(i,j) for i,j in zip(X, Y)]), nb)[1]
    return res


def progress(i, l):
    p = i/l*100
    if (p % 2) <= (100/l):
        print('...%s%%...'%int(p))
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

dem,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/elv_merit_Oueme_UTM_1km_min.tif",nodata_value = 65535)
veg,lonveg,latveg = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/DATA/VEG/LandCover/MESO_OUEME_LANDCOVER_20m_2007-10-07.tif",nodata_value = 65535)
vegtap,lonvegtap,latvegtap = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/DATA/VEG/LandCover/tappan/west_africa_land-use_land-cover_2000_2km/swa_2000lulc_2km_UTM31N_Oueme_sup.tif",nodata_value = 65535)
[lonlonveg,latlatveg]=np.meshgrid(lonveg,latveg)
[lonlonvegtap,latlatvegtap]=np.meshgrid(lonvegtap,latvegtap)
[lonlondem,latlatdem]=np.meshgrid(londem,latdem)

# get stats:
vegt = veg*1. 
vegt[vegt==1]=np.nan
vegt[vegt==0]=7 #woody savanna /savanne boisée
vegt[vegt==2]=8 #fallow /jachere
vegt[vegt==3]=19 #anthropised crops / cultures et zones anthropisées
vegt[vegt==4]=7 #woodlands / forets
vegt[vegt==5]=19 #urban / villes
vegt[vegt==6]=19 #water / eau

vzin,czin = np.unique(vegt,return_counts=True) 
totzin = np.sum(czin[0:3]) 
czin[0:3]/totzin *100 
# get stats:
vegtapt = vegtap*1. 
vegtapt[vegtapt==1] = 7 # Forest
vegtapt[vegtapt==2] = 7 # Savanna
vegtapt[vegtapt==15] = 7 #Gallery and riparian forest
vegtapt[vegtapt==25] = 7 #Woodland
vegtapt[vegtapt==6] = 7 #plantation
vegtapt[vegtapt==8] = 19 #agriculture
vegtapt[vegtapt==9] = 19 #water bodies
vegtapt[vegtapt==13] = 19 #settlements
vtap,ctap = np.unique(vegtapt,return_counts=True) 
tottap = np.sum(ctap[0:2]) 
ctap[0:2]/tottap *100 

"""Interpolate Zin's (to move as function)"""
# Interpôlate on thiessen (voronoi cells), requires first to connect each point to its 2 closest neighbours (tree);
# This is the triangulation. Then, cells can be viasualized by drawing the bissectrice of each segment.
# then the interpolation on the destination set of points (or grid) is made by looking onto which cell is
# each destination point
# the tree is made to know for each point selected, which are the two closest points, then the 2 closest of
# each of these two, etc... up to any chosen level.
   
#flatten makes vector from a matrix
tree = ann_tree(lonlonveg.flatten(), latlatveg.flatten(), lonlondem.flatten(), latlatdem.flatten(), nb=2500)

nx = len(lonveg)
l = len(tree)

veg2 = np.zeros((lonlondem.size, 1))

for s, p in enumerate(tree):
    #s is index of the destination grid
    #p is index of the source grid
    progress(s, l)       
    vegtmp = np.zeros((2500, 1))
    k=0
    for pp in p: 
        # this is short to go from vector back to matrix using modulo (reste) and integer division
        i0 = pp % nx
        j0 = pp // nx
        vegtmp[k] = veg[j0,i0]
        k+=1
        
    val,counts=np.unique(vegtmp,return_counts=True)
    veg2[s] = val[np.argmax(counts)]

nx=len(londem)
ny=len(latdem)
vegveg = np.zeros(np.shape(dem))
for i in range(ny):
    #~ vegveg[i,:] = np.reshape(veg2[i*nx:(i+1)*nx],(nx,1))
    vegveg[i,:] = veg2[i*nx:(i+1)*nx].T
    #~ vegveg[i,:] = veg2[i*nx:(i+1)*nx]
#~ vegveg []

vegveg=np.flipud(vegveg)


"""MESO_OUEME_LANDCOVER_20m_2007
0: woody savanna /savanne boisée
1: nodata
2: fallow /jachere
3: anthropised crops / cultures et zones anthropisées
4: woodlands / forets
5: urban / villes
6: water / eau
"""
vegvegzin=vegveg

# to keep
vegveg[vegveg==0]=7#woody savanna /savanne boisée
vegveg[vegveg==1]=7#nodata
vegveg[vegveg==2]=8#fallow /jachere
vegveg[vegveg==3]=19 #anthropised crops / cultures et zones anthropisées
vegveg[vegveg==4]=7#woodlands / forets
vegveg[vegveg==5]=19 #urban / villes
vegveg[vegveg==6]=19#water / eau

# get stats:
vegvegzint = vegvegzin*1.
vegvegzint[vegvegzint==1]=np.nan 
vegvegzint[vegvegzint==0]=7#woody savanna /savanne boisée
vegvegzint[vegvegzint==2]=8#fallow /jachere
vegvegzint[vegvegzint==3]=19 #anthropised crops / cultures et zones anthropisées
vegvegzint[vegvegzint==4]=7#woodlands / forets
vegvegzint[vegvegzint==5]=19 #urban / villes
vegvegzint[vegvegzint==6]=19#water / eau

vzin2,czin2 = np.unique(vegvegzint,return_counts=True)
czin2[0:3]/sum(czin2[0:3])*100 

#~ cnts,bins = np.histogram(vegveg,[0,1.2,2])
#~ print('n pixels trees: %i; n pixels herb:  %i'%(cnts[0],cnts[1]))
#~ print('ratio trees / n pixels vegetated : %0.2f'%(cnts[0]/(cnts[0]+cnts[1])))
vegveg_towrite = copy.copy(vegveg)
#~ vegveg_towrite[vegveg == 2] = 19
#~ vegveg_towrite[vegveg == 1] = 8	
#~ [x,y,veg]=read_vegm(inputdrvvegm,plot=False)
#~ [x,y,veg]=read_vegm(inputdrvvegm,plot=True)
#~ [XX,YY,vegveg]=veg_to_vegveg(x,y,veg)
[x2,y2,vegwrite]=vegio.vegveg_to_veg(lonlondem,latlatdem,vegveg_towrite)
#~ vegio.write_vegm(x2,y2,veg2,"/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/inputs/drv_vegm_Ara%d.dat"%resolution)
#~ vegio.write_vegm(x2,y2,vegwrite,"/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/drvvegm_zin_tmp_python.dat")
vegio.read_vegm("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/drvvegm_zin_tmp_python.dat",plot=True,cmap = plt.cm.RdYlGn_r)




"""Interpolate Tappan's (to move as function)"""
# Interpôlate on thiessen (voronoi cells), requires first to connect each point to its 2 closest neighbours (tree);
# This is the triangulation. Then, cells can be viasualized by drawing the bissectrice of each segment.
# then the interpolation on the destination set of points (or grid) is made by looking onto which cell is
# each destination point
# the tree is made to know for each point selected, which are the two closest points, then the 2 closest of
# each of these two, etc... up to any chosen level.
   
#flatten makes vector from a matrix
tree = ann_tree(lonlonvegtap.flatten(), latlatvegtap.flatten(), lonlondem.flatten(), latlatdem.flatten(), nb=1)

nx = len(lonvegtap)
l = len(tree)

veg2 = np.zeros((lonlondem.size, 1))

for s, p in enumerate(tree):
    #s is index of the destination grid
    #p is index of the source grid
    progress(s, l)       
    vegtmp = np.zeros((1, 1))
    # this is short to go from vector back to matrix using modulo (reste) and integer division
    i0 = p % nx
    j0 = p // nx

    veg2[s] = vegtap[j0,i0]

nx=len(londem)
ny=len(latdem)
vegveg = np.zeros(np.shape(dem))
for i in range(ny):
    #~ vegveg[i,:] = np.reshape(veg2[i*nx:(i+1)*nx],(nx,1))
    vegveg[i,:] = veg2[i*nx:(i+1)*nx].T
    #~ vegveg[i,:] = veg2[i*nx:(i+1)*nx]
#~ vegveg []

vegveg=np.flipud(vegveg)


"""Tappan found in Upper Ouémé 2000
1: Forest
2: Savanna
15: fGallery and riparian forest
25: Woodland
6: plantation
8: agriculture
9: water bodies
13: settlements
"""

vegveg[vegveg==1]=7 # Forest
vegveg[vegveg==2]=7 # Savanna
vegveg[vegveg==15]=7 #Gallery and riparian forest
vegveg[vegveg==25]=7 #Woodland
vegveg[vegveg==6]=7 #plantation
vegveg[vegveg==8]=19 #agriculture
vegveg[vegveg==9]=19 #water bodies
vegveg[vegveg==13]=19 #settlements
vegvegtap=vegveg

# get stats
vegvegtapt = vegvegtap*1.

vtap2,ctap2 = np.unique(vegvegtapt,return_counts=True)
ctap2[0:2]/sum(ctap2[0:2])*100 

#~ cnts,bins = np.histogram(vegveg,[0,1.2,2])
#~ print('n pixels trees: %i; n pixels herb:  %i'%(cnts[0],cnts[1]))
#~ print('ratio trees / n pixels vegetated : %0.2f'%(cnts[0]/(cnts[0]+cnts[1])))
vegveg_towrite = copy.copy(vegvegtap)
#~ vegveg_towrite[vegveg == 2] = 19
#~ vegveg_towrite[vegveg == 1] = 8	
#~ [x,y,veg]=read_vegm(inputdrvvegm,plot=False)
#~ [x,y,veg]=read_vegm(inputdrvvegm,plot=True)
#~ [XX,YY,vegveg]=veg_to_vegveg(x,y,veg)
[x2,y2,vegwrite]=vegio.vegveg_to_veg(lonlondem,latlatdem,vegveg_towrite)
#~ vegio.write_vegm(x2,y2,veg2,"/home/hectorb/PARFLOW/PROJECTS/Ara/resolution/inputs/drv_vegm_Ara%d.dat"%resolution)
#~ vegio.write_vegm(x2,y2,vegwrite,"/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/drvvegm_tap_tmp_python.dat")
vegio.read_vegm("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/drvvegm_tap_tmp_python.dat",plot=True,cmap = plt.cm.RdYlGn_r)

""" If pfb writing is needed:"""
h ={'X0': 334500.0,
 'Y0': 987500.0,
 'Z0': 0.0,
 'dx': 1000.0,
 'dy': 1000.0,
 'dz': 3.0,
 'ix': 0,
 'iy': 0,
 'iz': 0,
 'ns': 1,
 'nx': 144,
 'nx2': 144,
 'ny': 144,
 'ny2': 144,
 'nz': 1,
 'nz2': 1,
 'rx': 1,
 'ry': 1,
 'rz': 1}
filename="/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/vegmap_tap_tmp_python.pfb"
#~ io_pfb.PFB_write(np.swapaxes(vegveg_towrite,0,1), h, pfb_fp=filename)
d,xx,yy,fig,ax,cb=io_pfb.plot_2D_PFB(filename,zlayer=0,logscale=False,colorlabel='LC classes',title='Land Cover',savefig=True,savename='LCC_vegmap_tappan.png',savedir ='/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/',width=6.7,height=3.75,cmap='YlGn_r') 


filename="/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/veg_map_dahu.pfb"
d,xx,yy,fig,ax,cb=io_pfb.plot_2D_PFB(filename,zlayer=0,logscale=False,colorlabel='LC classes',title='Land Cover',savefig=True,savename='LCC_veg_map_Zin.png',savedir ='/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/',width=6.7,height=3.75,cmap='YlGn_r') 



