#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    Prepare manning file from a river network map
    
    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os,glob
from netCDF4 import Dataset
import time
import datetime
import pyproj
import rasterio
from matplotlib.dates import MonthLocator, DateFormatter
"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as io

from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
from procsycz import procGeodata_Gdal



##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
#~ mannings_river = 1E-6
#~ mannings_slopes = 1.6E-5
mannings_river = 1E-6
mannings_slopes = 5E-6
mannings_river = 3E-6
mannings_slopes = 8E-5
#~ mannings_river = 0.000001
#~ mannings_slopes = 0.000016
output_filename = '/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/mannings_riv1_slopes5.pfb'
output_filename = '/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/mannings_riv3_slopes8.pfb'
"""
Read in a tif file using gdal:
Data is ny x nx and y dimension is upside down (as a map view when you look at the matrix so data[0,0] is upper left (norh West))
"""
data,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/upg_merit_Benin_UTM_1km_masked_thinned_corr.tif",nodata_value = 255)

[lonlon,latlat]=np.meshgrid(londem,latdem)
lonlon=lonlon-500
latlat=latlat-500
data = data.astype(float)
data[data==255] = '{:f}'.format(mannings_slopes)
data[data==1] = '{:f}'.format(mannings_river)

data = data[::-1,:] # -> to make it data[0,0] to become lower left
data = np.swapaxes(data,0,1)# -> then its parflow compatible: nx x ny

"""
read in a 2D pfb file of the appropriate PF domain 
"""
d,h=io.PFB_read('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Yslopes_merit_f3_nosec2.pfb',1)                                                                                            

"""
Write the manning file
"""
io.PFB_write(data,h,output_filename)
#~ test,xx,yy = io.plot_2D_PFB(output_filename,plot_fig=True)
test,xx,yy,fig,ax,cb = io.plot_2D_PFB(output_filename,plot_fig=True)
