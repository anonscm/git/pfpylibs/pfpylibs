#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Example script to prepare distributed forcing fields for LAI, SAI, DISPLA, Z0M,
    based on a vegmap

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os, glob, copy

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb

plt.close('all')
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
# Local
vegmapfilename="/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/vegmap_tap_tmp_python.pfb"
source_dir = "/mnt/hectorb/faro/PARFLOW/PROJECTS/Oueme/Oueme2020/Amelie_runs/"
output_dir = "/mnt/hectorb/faro/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/forcings/FORC_veg"
# faro
vegmapfilename="/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/vegmap_tap_tmp_python.pfb"
source_dir = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/Amelie_runs/"
output_dir = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/forcings/FORC_veg"
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##



""" Read in a Vegmap"""
vegmap,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(vegmapfilename,zlayer=0,logscale=False,colorlabel='LC classes',title='Land Cover',savefig=False,width=6.7,height=3.75,cmap='YlGn_r') 
vegmap,h=iopfb.PFB_read(vegmapfilename,1)

""" Read in LAI, SAI, DISPLA, Z0M time series """
lai = pd.read_csv(source_dir+'lai.dat', delimiter=r"\s+", header =None)
sai = pd.read_csv(source_dir+'sai.dat', delimiter=r"\s+", header =None)
displa = pd.read_csv(source_dir+'displa.dat', delimiter=r"\s+", header =None)
z0m = pd.read_csv(source_dir+'z0m.dat', delimiter=r"\s+", header =None)

lai.index = pd.date_range('1/1/2005 00:00:00', periods=len(lai), freq='30MIN')
sai.index = pd.date_range('1/1/2005 00:00:00', periods=len(sai), freq='30MIN')
displa.index = pd.date_range('1/1/2005 00:00:00', periods=len(displa), freq='30MIN')
z0m.index = pd.date_range('1/1/2005 00:00:00', periods=len(z0m), freq='30MIN')


""" Set the file parsing system for PF-CLM setups """
#2005-2008 = 1461j
#so divide by 10 days periods
NT = 1461 # days
n_nt=np.floor(NT/10) # number of periods
nt=10*48 # number of timesteps per period
hout = h
hout['nz'] = nt
hout['nz2'] = nt
hout['dz'] = 1
veg3D = np.repeat(vegmap[:, :, np.newaxis], nt, axis=2)

for period in range(int(n_nt)):
    hout['Z0'] = period*nt+1
    pfbfilename = os.sep.join([output_dir,'LAI','Forc.LAI.%06d_to_%06d.pfb'%(period*nt+1,(period+1)*nt)])
    print(pfbfilename)
    
    Outdata = np.zeros(np.shape(veg3D))+ 0.09
    for vegclass in lai.columns: #0 to 18
        # the following is likely fast but quite risky. If there's a mismatch in vector length it may crash'
        Outdata[veg3D == vegclass+1 ] = np.tile(lai.iloc[period*nt:(period+1)*nt,vegclass] ,len(vegmap[vegmap==vegclass+1]))
    #~ PFlibs.io_pfb.PFB_write(Outdata[:,:,::-1],hout,pfbfilename)
    PFlibs.io_pfb.PFB_write(Outdata[:,:,:],hout,pfbfilename)

for period in range(int(n_nt)):
    hout['Z0'] = period*nt+1
    pfbfilename = os.sep.join([output_dir,'SAI','Forc.SAI.%06d_to_%06d.pfb'%(period*nt+1,(period+1)*nt)])
    print(pfbfilename)
    
    Outdata = np.zeros(np.shape(veg3D))+ 0.09
    for vegclass in lai.columns: #0 to 18
        # the following is likely fast but quite risky. If there's a mismatch in vector length it may crash'
        Outdata[veg3D == vegclass+1 ] = np.tile(sai.iloc[period*nt:(period+1)*nt,vegclass] ,len(vegmap[vegmap==vegclass+1]))
    #~ PFlibs.io_pfb.PFB_write(Outdata[:,:,::-1],hout,pfbfilename)
    PFlibs.io_pfb.PFB_write(Outdata[:,:,:],hout,pfbfilename)

for period in range(int(n_nt)):
    hout['Z0'] = period*nt+1
    pfbfilename = os.sep.join([output_dir,'DISPLA','Forc.DISPLA.%06d_to_%06d.pfb'%(period*nt+1,(period+1)*nt)])
    print(pfbfilename)
    
    Outdata = np.zeros(np.shape(veg3D))+ 0.09
    for vegclass in lai.columns: #0 to 18
        # the following is likely fast but quite risky. If there's a mismatch in vector length it may crash'
        Outdata[veg3D == vegclass+1 ] = np.tile(displa.iloc[period*nt:(period+1)*nt,vegclass] ,len(vegmap[vegmap==vegclass+1]))
    #~ PFlibs.io_pfb.PFB_write(Outdata[:,:,::-1],hout,pfbfilename)
    PFlibs.io_pfb.PFB_write(Outdata[:,:,:],hout,pfbfilename)

for period in range(int(n_nt)):
    hout['Z0'] = period*nt+1
    pfbfilename = os.sep.join([output_dir,'Z0M','Forc.Z0M.%06d_to_%06d.pfb'%(period*nt+1,(period+1)*nt)])
    print(pfbfilename)
    
    Outdata = np.zeros(np.shape(veg3D))+ 0.09
    for vegclass in lai.columns: #0 to 18
        # the following is likely fast but quite risky. If there's a mismatch in vector length it may crash'
        Outdata[veg3D == vegclass+1 ] = np.tile(z0m.iloc[period*nt:(period+1)*nt,vegclass] ,len(vegmap[vegmap==vegclass+1]))
    PFlibs.io_pfb.PFB_write(Outdata[:,:,:],hout,pfbfilename)
