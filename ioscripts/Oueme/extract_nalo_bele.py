#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    look at WT around station locations of Nalohou & Bellefoungou
    
    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import pandas as pd
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import datetime
import glob,os,copy
import matplotlib.patches as mpatches
import matplotlib as mpl
from matplotlib.colors import LightSource
import pyproj

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb
from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
from procsycz import procGeodata_Gdal


plt.close('all')
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/plot_scale_1D/NCout/ref_2020_01_22'
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_2020_02_18'
simuname1 = 'oueme_KWE_AD'
#~ simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_AD_2020_03_05'
#~ simuname1 = 'oueme_KWE_f3_AD'
#~ simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_2020_03_03'
#~ simuname1 = 'oueme_ZA24_H3015'
#~ simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_H3015_2020_03_06'
#~ simuname1 = 'oueme_ZA24_f3_H3015'
#~ simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H30029_4yrs_2020_03_18'
#~ simuname1 = 'oueme_ZA24_f3_OF_H30029_4yrs'
#~ simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_m1_nomask_AD_4yrs_2020_04_10'
#~ simuname1 = 'oueme_KWE_f3_m1_nomask_AD_4yrs'
#~ simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_AD_2020_03_05'
#~ simuname1 = 'oueme_KWE_f3_AD'
simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_10yrs_2020_06_02'
simuname1 = 'H2clay_10yrs'
simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015new_4yrs_2020_04_28'
simuname1 = r'oueme_ZA24_H3015new_4yrs'
#~ simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_ZAZFA_100_9yrs_2020_06_03'
#~ simuname1 = 'H2clay_ZAZFA_100_9yrs'
#FARO
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_m1_nomask_AD_4yrs_2020_04_10'
#~ simuname1 = 'oueme_KWE_f3_m1_nomask_AD_4yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_H1lowK_rt85_4yrs_2020_04_20'
#~ simuname1 = r'oueme_KWE_AD_H1lowK_rt85_4yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_H1lowK_rt10_4yrs_2020_04_25'
#~ simuname1 = r'oueme_KWE_AD_H1lowK_rt10_4yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_2020_04_25'
#~ simuname1 = r'oueme_AD_rt10_nvdz_nclm_rtaherb'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H1lowK_rt10_nvdz_nclm_rtaherb_man3_2020_04_26'
#~ simuname1 = r'oueme_AD_H1lowK_rt10_nvdz_nclm_rtaherb_man3'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_man3_2020_04_27'
#~ simuname1 = r'oueme_AD_rt10_nvdz_nclm_rtaherb_man3'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3_2020_04_29'
#~ simuname1 = r'oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3'

#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015new_4yrs_2020_04_28'
#~ simuname1 = r'oueme_ZA24_H3015new_4yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015new_4yrs_2020_04_28'
#~ simuname1 = r'oueme_ZA24_H3015new_4yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowKlown_4yrs_2020_04_30'
#~ simuname1 = r'oueme_ZA24_H3015_H2lowKlown_4yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_H3015_H2thicklowKlownhighP_2020_05_05'
#~ simuname1 = r'oueme_H3015_H2thicklowKlownhighP'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_4yrs_2020_05_02'
#~ simuname1 = r'oueme_ZA24_H3015_rivmaskarea72_4yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_2020_05_17'
#~ simuname1 = r'H2clay'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/rt6_H1thin2_H2claylowK_H3split_6yrs_2020_05_13'
#~ simuname1 = r'rt6_H1thin2_H2claylowK_H3split_6yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/rt6_H1thin2_H2claylowK_H3split_6yrs_2020_05_14'
#~ simuname1 = r'rt6_H1thin2_H2claylowK_H3split_6yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay2_mask2_2020_05_16'
#~ simuname1 = r'H2clay2_mask2'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_HPC_rivmaskarea8_6yrs_2020_05_03'
#~ simuname1 = r'oueme_ZA24_H3015_HPC_rivmaskarea8_6yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_HPC_rivmaskarea8_4yrs_2020_05_05'
#~ simuname1 = r'oueme_ZA24_H3015_HPC_rivmaskarea8_4yrs'


#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_2020_04_23'
#~ simuname1 = r'oueme_AD'
short1 = 'KWE_AD'
short1 = 'AD_KWE'
short1 = 'AD_KWE'
#~ short1 = 'KWE_H1lowK_rt85'
#~ short1 = 'AD_rt10_nvdz_nclm_rtaherb'
short1 = 'AD_H1lowK_man3'
short1 = 'AD_man3'
short1 = 'AH_H3015'
#~ short1 = 'AD_rt85'
#~ short1 = 'AD rt10'
#~ short1 = 'KWE'
# make a nice one: ref with homogeneous KH2
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_KH3/H2clay_10yrs_2020_05_29'
#~ simuname1 = r'H2clay_10yrs'
#~ short1 = 'nomask'
# make a nice one: ZIN vegmap
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay2_mask8_8yrs_2020_08_09'
simuname1 = 'H2clay2_mask8_8yrs'
short1= 'mask8_ZIN'
# make a nice one: tappan vegmap
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay2_mask8_tappan_8yrs_2020_08_07'
simuname1 = 'H2clay2_mask8_tappan_8yrs'
short1= 'mask8_TAP'
# make a paper one: benchmark one
simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H3015_H2clay24prct_2020_09_06'
simuname1 = 'H3015_H2clay24prct'
short1= 'H3015_H2clay24prct'


#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/plot_scale_1D/NCout/ref_2020_01_22'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_AD_2020_02_19'
simuname2 = 'oueme_OF_AD'
simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_AD_2020_03_05'
simuname2 = 'oueme_OF_f3_AD'
#~ simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H3015_2020_03_10'
#~ simuname2 = 'oueme_ZA24_f3_OF_H3015'
simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H306_4yrs_2020_03_18'
simuname2 = 'oueme_ZA24_f3_OF_H306_4yrs'
simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_m1_nomask_AD_4yrs_2020_04_11'
simuname2 = 'oueme_OF_f3_m1_nomask_AD_4yrs'
simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_H1lowK_rt85_4yrs_2020_04_17'
simuname2 = 'oueme_KWE_AD_H1lowK_rt85_4yrs'
simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_AD_2020_03_05'
simuname2 = 'oueme_OF_f3_AD'



#FARO
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_m1_nomask_highman_AD_4yrs_2020_04_13'
simuname2 = 'oueme_KWE_f3_m1_nomask_highman_AD_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_H1lowK_rt10_4yrs_2020_04_25'
#~ simuname2 = r'oueme_KWE_AD_H1lowK_rt10_4yrs'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H1lowK_rt10_nvdz_nclm_rtaherb_man3_2020_04_26'
simuname2 = r'oueme_AD_H1lowK_rt10_nvdz_nclm_rtaherb_man3'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H1lowK_H2lowP_rt10_nvdz_nclm_rtaherb_man3_2020_04_27'
simuname2 = r'oueme_AD_H1lowK_H2lowP_rt10_nvdz_nclm_rtaherb_man3'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_man3_2020_04_27'
simuname2 = r'oueme_AD_rt10_nvdz_nclm_rtaherb_man3'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3_2020_04_29'
simuname2 = r'oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_man5_2020_04_27'
#~ simuname2 = r'oueme_AD_rt10_nvdz_nclm_rtaherb_man5'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_varman_2020_04_27'
#~ simuname2 = r'oueme_AD_rt10_nvdz_nclm_rtaherb_varman'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_varman2_2020_04_28'
#~ simuname2 = r'oueme_AD_rt10_nvdz_nclm_rtaherb_varman2'

#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H306new_4yrs_2020_04_28'
#~ simuname2 = r'oueme_ZA24_H306new_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowKlown_4yrs_vdz_2020_05_01'
#~ simuname2 = r'oueme_ZA24_H3015_H2lowKlown_4yrs_vdz'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowKlown_4yrs_vdzAD_2020_05_01'
#~ simuname2 = r'oueme_ZA24_H3015_H2lowKlown_4yrs_vdzAD'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowlowKlown_4yrs_2020_05_01'
#~ simuname2 = r'oueme_ZA24_H3015_H2lowlowKlown_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H322_H2lowKlown_4yrs_2020_05_02'
#~ simuname2 = r'oueme_ZA24_H322_H2lowKlown_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_riv_4yrs_2020_05_02'
#~ simuname2 = r'oueme_ZA24_H3015_riv_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_H3015_H2thicklowKlownhighP_2020_05_05'
#~ simuname2 = r'oueme_H3015_H2thicklowKlownhighP'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_H3015_H2thicklowlowKlownhighP_2020_05_05'
#~ simuname2 = r'oueme_H3015_H2thicklowlowKlownhighP'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_4yrs_2020_05_02'
#~ simuname2 = r'oueme_ZA24_H3015_rivmaskarea72_4yrs'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_6yrs_2020_05_03'
simuname2 = r'oueme_ZA24_H3015_rivmaskarea72_6yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea8_6yrs_2020_05_03'
#~ simuname2 = r'oueme_ZA24_H3015_rivmaskarea8_6yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea2_6yrs_2020_05_03'
#~ simuname2 = r'oueme_ZA24_H3015_rivmaskarea2_6yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_HPC_rivmaskarea8_4yrs_2020_05_05'
#~ simuname2 = r'oueme_ZA24_H3015_HPC_rivmaskarea8_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2highret_HPC_rivmaskarea8_4yrs_2020_05_05'
#~ simuname2 = r'oueme_ZA24_H3015_H2highret_HPC_rivmaskarea8_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_HPC_rivmaskarea2_4varyrs_2020_05_05'
#~ simuname2 = r'oueme_ZA24_H3015_HPC_rivmaskarea2_4varyrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowK_4yrs_2020_04_29'
#~ simuname2 = r'oueme_ZA24_H3015_H2lowK_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_6yrs_2020_05_03'
#~ simuname2 = r'oueme_ZA24_H3015_rivmaskarea72_6yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_2020_04_25'
#~ simuname2 = r'oueme_AD_rt10_nvdz_nclm_rtaherb'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H3lowP_nort_nvdz_nclm_rtaherb_man3_2020_05_05'
#~ simuname2 = r'oueme_AD_H3lowP_nort_nvdz_nclm_rtaherb_man3'
short2 = 'OF_AD'
short2 = 'KWE_AD_deeproots'
short2 = 'AD_OF'
short2 = 'AD_KWE_highman'
#~ short2 = 'OF_KWE'
#~ short2 = 'KWE_f3'
#~ short2 = 'AD_KWE_H1lowK'
#~ short2 = 'AD_OF'
#~ short1 = 'KWE_H1lowK_rt85_nof3'
#~ short2 = 'KWE_H1lowK_rt85_newriv'
#~ short2 = 'KWE_H1lowK_rt10'
#~ short2 = 'AD_rt10_nvdz_nclm_rtaherb'
#~ short2 = 'AD_H1lowK_rt10_nvdz_nclm_rtaherb_man3'
short2 = 'AD_H1lowK_H2lowP_rt10_nvdz_nclm_rtaherb_man3'
short2 = 'AD_man3'
short2 = 'AD_man3 H3lowP'
#~ short2 = 'AD_man5'
#~ short2 = 'AD_varman'
#~ short2 = 'AD_man_riv3_slopes8'
#~ short2 = 'H306'
#~ short2 = 'AH_H3015 H2lowK'
#~ short2 = 'H3015 rivmaskarea72'
#~ short2 = 'H3015 rivmaskarea72'
short2 = 'H3015 rivmaskarea72_6yrs'
#~ short2 = 'H3015 rivmaskarea8_6yrs'
#~ short2 = 'H3015 rivmaskarea2_6yrs'
#~ short2 = 'H3015 HPC rivmaskarea8_6yrs'
#~ short2 = 'H3015 H2highret HPC rivmaskarea8_4yrs'
#~ short2 = 'HPC rivmaskarea8_4yrs'
#~ short2 = 'HPC rivmaskarea2_4yrs'
#~ short2='AD no rt'

simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_AD_2020_03_05'
simuname3 = 'oueme_KWE_f3_AD'
simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_nosec_AD_2020_03_15'
simuname3 = 'oueme_KWE_f3_nosec_AD'
#~ simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_H3015_2020_03_06'
#~ simuname3 = 'oueme_ZA24_f3_H3015'
#not f3:
#~ simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZAZFA48_H300023_H4023_2020_03_03'
#~ simuname3 = 'oueme_ZAZFA48_H300023_H4023'
simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H3015_4yrs_2020_03_14'
simuname3 = 'oueme_ZA24_f3_OF_H3015_4yrs'
simdir3= r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_KWE_H3015_4yrs_2020_04_09'
simuname3 = 'oueme_ZA24_f3_KWE_H3015_4yrs'
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_nosec_AD_2020_03_15'
simuname3 = 'oueme_KWE_f3_nosec_AD'

#FARO
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_man3_2020_04_27'
simuname3 = r'oueme_AD_rt10_nvdz_nclm_rtaherb_man3'
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea8_6yrs_2020_05_03'
simuname3 = r'oueme_ZA24_H3015_rivmaskarea8_6yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowlowKlown_4yrs_2020_05_01'
#~ simuname3 = r'oueme_ZA24_H3015_H2lowlowKlown_4yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H322_H2lowKlown_4yrs_2020_05_02'
#~ simuname3 = r'oueme_ZA24_H322_H2lowKlown_4yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_riv_4yrs_2020_05_02'
#~ simuname3 = r'oueme_ZA24_H3015_riv_4yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_H3015_H2thicklowKlownhighP_2020_05_05'
#~ simuname3 = r'oueme_H3015_H2thicklowKlownhighP'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_H3015_H2thicklowlowKlownhighP_2020_05_05'
#~ simuname3 = r'oueme_H3015_H2thicklowlowKlownhighP'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_4yrs_2020_05_02'
#~ simuname3 = r'oueme_ZA24_H3015_rivmaskarea72_4yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_6yrs_2020_05_03'
#~ simuname3 = r'oueme_ZA24_H3015_rivmaskarea72_6yrs'
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea8_6yrs_2020_05_03'
simuname3 = r'oueme_ZA24_H3015_rivmaskarea8_6yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea2_6yrs_2020_05_03'
#~ simuname3 = r'oueme_ZA24_H3015_rivmaskarea2_6yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_HPC_rivmaskarea8_4yrs_2020_05_05'
#~ simuname3 = r'oueme_ZA24_H3015_HPC_rivmaskarea8_4yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2highret_HPC_rivmaskarea8_4yrs_2020_05_05'
#~ simuname3 = r'oueme_ZA24_H3015_H2highret_HPC_rivmaskarea8_4yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_HPC_rivmaskarea2_4varyrs_2020_05_05'
#~ simuname3 = r'oueme_ZA24_H3015_HPC_rivmaskarea2_4varyrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowK_4yrs_2020_04_29'
#~ simuname3 = r'oueme_ZA24_H3015_H2lowK_4yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_6yrs_2020_05_03'
#~ simuname3 = r'oueme_ZA24_H3015_rivmaskarea72_6yrs'


short3 = 'KWE_AH'
short3 = 'AD_KWE_nosec'
short3 = 'AD'
short3 = 'H3015 rivmaskarea8'
short3 = 'H3015 rivmaskarea72_6yrs'
short3 = 'H3015 rivmaskarea8_6yrs'
#~ short3 = 'H3015 rivmaskarea2_6yrs'
#~ short3 = 'H3015 HPC rivmaskarea8_6yrs'
#~ short3 = 'H3015 H2highret HPC rivmaskarea8_4yrs'
#~ short3 = 'HPC rivmaskarea8_4yrs'
#~ short3 = 'HPC rivmaskarea2_4yrs'

#~ simdir4 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_AD_2020_03_05'
#~ simuname4 = 'oueme_OF_f3_AD'
simdir4 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_nosec_AD_2020_03_16'
simuname4 = 'oueme_OF_f3_nosec_AD'
#~ simdir4= r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZAZFA48_f3_OF_H300023_H4023_2020_03_10'
#~ simuname4 = 'oueme_ZAZFA48_f3_OF_H300023_H4023'
simdir4= r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZAZFA48_f3_OF_H300023_H4023_4yrs_2020_03_16'
simuname4 = 'oueme_ZAZFA48_f3_OF_H300023_H4023_4yrs'
simdir4= r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H3015_4yrs_2020_03_14'
simuname4 = 'oueme_ZA24_f3_OF_H3015_4yrs'
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_nosec_AD_2020_03_16'
simuname4 = 'oueme_OF_f3_nosec_AD'

#FARO
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowlowKlown_4yrs_2020_05_01'
#~ simuname4 = r'oueme_ZA24_H3015_H2lowlowKlown_4yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H322_H2lowKlown_4yrs_2020_05_02'
#~ simuname4 = r'oueme_ZA24_H322_H2lowKlown_4yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_riv_4yrs_2020_05_02'
#~ simuname4 = r'oueme_ZA24_H3015_riv_4yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_H3015_H2thicklowKlownhighP_2020_05_05'
#~ simuname4 = r'oueme_H3015_H2thicklowKlownhighP'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_H3015_H2thicklowlowKlownhighP_2020_05_05'
#~ simuname4 = r'oueme_H3015_H2thicklowlowKlownhighP'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_4yrs_2020_05_02'
#~ simuname4 = r'oueme_ZA24_H3015_rivmaskarea72_4yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_6yrs_2020_05_03'
#~ simuname4 = r'oueme_ZA24_H3015_rivmaskarea72_6yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea8_6yrs_2020_05_03'
#~ simuname4 = r'oueme_ZA24_H3015_rivmaskarea8_6yrs'
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea2_6yrs_2020_05_03'
simuname4 = r'oueme_ZA24_H3015_rivmaskarea2_6yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_HPC_rivmaskarea8_4yrs_2020_05_05'
#~ simuname4 = r'oueme_ZA24_H3015_HPC_rivmaskarea8_4yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2highret_HPC_rivmaskarea8_4yrs_2020_05_05'
#~ simuname4 = r'oueme_ZA24_H3015_H2highret_HPC_rivmaskarea8_4yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_HPC_rivmaskarea2_4varyrs_2020_05_05'
#~ simuname4 = r'oueme_ZA24_H3015_HPC_rivmaskarea2_4varyrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowK_4yrs_2020_04_29'
#~ simuname4 = r'oueme_ZA24_H3015_H2lowK_4yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_6yrs_2020_05_03'
#~ simuname4 = r'oueme_ZA24_H3015_rivmaskarea72_6yrs'

#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3_2020_04_29'
#~ simuname4 = r'oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea2_6yrs_2020_05_03'
#~ simuname4 = r'oueme_ZA24_H3015_rivmaskarea2_6yrs'
short4 = 'OF_AH'
short4 = 'AD_OF_nosec'
short4 = 'AD_H3lowP'
short4 = 'H3015 rivmaskarea2'
short4 = 'H3015 rivmaskarea72_6yrs'
#~ short4 = 'H3015 rivmaskarea8_6yrs'
short4 = 'H3015 rivmaskarea2_6yrs'
#~ short4 = 'H3015 HPC rivmaskarea8_6yrs'
#~ short4 = 'H3015 H2highret HPC rivmaskarea8_4yrs'
#~ short4 = 'HPC rivmaskarea8_4yrs'
#~ short4 = 'HPC rivmaskarea2_4yrs'

""" projection system """
proj = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def prepare_colormap(cmapname,bound_low,bound_high,bin_size = 1):
	"""
	prepare a colormap based on the data range
	also force first entry to be grey
	"""
	cmap = cmapname
	cmaplist = [cmap(i) for i in range(cmap.N)] 				# extract all colors from the .jet map
	#~ cmaplist[0] = (.5,.5,.5,1.0) 								# force the first color entry to be grey
	#~ cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N) 	# create the new map
	bounds = np.arange(bound_low,bound_high,bin_size) 									# define the bins and normalize
	norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
	return cmap,norm,bounds
    
def find_grid_location(xx,yy,x,y):
    """matches the simulation grid given by xx and yy created by meshgrid and a given location (x,y):
    finds the closest match
    """
    dist = np.sqrt((np.round(xx)- x)**2 + (np.round(yy) - y)**2)
    ind_min = np.where(dist == dist.min())   
    return ind_min[1][0],ind_min[0][0],np.round(xx[ind_min])[0],np.round(yy[ind_min])[0]
    
def xtract_WTs(P,x,y,z):
    """Extract WT time series at each depth from an xarray P at locations x and y and given the depth profile z"""
    Ptmp=P.succion.sel(longitude=x,latitude=y)    
    #note that in pfoutputs.py:
    #profile[0] = base of domain. if the criteria is p < 0 => 0, you realize there are some weird points at the bottom
    #of perched layers with p>0 but < self.dz ... so as if the cell is not entirely saturated but p>0 ... 
    #profile = np.array([0. if self.press_array[xloc,yloc,i,t]<= self.dz_mult_arr[xloc,yloc,i]*self.dz/2 else 1. for i in range(self.nz)])
    #so?:
    #~ tmp2=(tmp-np.tile((P1.var_dz[:,50,50]*3).data,(354,1))/2>=0)
    #~ tmp2=(tmp>=0)
    tmp=np.zeros(np.shape(Ptmp.data))
    tmp[Ptmp.data>=0]=1
    for i in range(np.shape(tmp)[1]-1):
        tmp[:,i+1]=(tmp[:,i]+tmp[:,i+1])*tmp[:,i+1]
        # a=[1,1,1,0,0,0,1,1,0,1,0,0,0,1] => [1, 2, 3, 0, 0, 0, 1, 2, 0, 1, 0, 0, 0, 1]
        tmp[tmp!=1.]=np.nan
    #~ z=np.cumsum(P.var_dz[::-1,ysim_ind,xsim_ind]*3) -  P1.var_dz[::-1,ysim_ind,xsim_ind]*3/2
    WTs =np.tile(z[::-1],(np.shape(Ptmp.data)[0],1)) -(tmp*Ptmp.data) 
    return pd.DataFrame({z.data[::-1][depth]:pd.Series(WTs[:,depth],index=P.time.data) for depth in range(len(z))})    

def get_WT_simulation_results(P,stadic):
    """fill in the station dictionnary with WTs at stations locations calculated from """
    
    for stationname,sta in stadic.items():
        print(sta.name)
        z=np.cumsum(P.var_dz[::-1,sta.ysim_ind,sta.xsim_ind]*3) -  P.var_dz[::-1,sta.ysim_ind,sta.xsim_ind]*3/2
        sta.wts=xtract_WTs(P,sta.xsim,sta.ysim,z)
        stadic[stationname]=sta
    return stadic
    
def plot_WT_time_series(stadic,simdir='',savefig=True):
    """"""
    fig,ax =plt.subplots(nrows=int(np.ceil(len(stadic)/3)),ncols = 3,figsize=(15,20), squeeze=True,sharex=True,sharey=True)

    i=0
    j=0
    k=0

    for stationname,sta in stadic.items():
        # interpolation on sim + daily approach:
        obs = sta.WT.dropna().sort_index(axis=0)
        #~ simu = sta.WTsim.dropna().sort_index(axis=0)
        #~ simu = sta.wts[0].dropna().sort_index(axis=0)
        simu = sta.wts.iloc[:,0].dropna().sort_index(axis=0)
        #~ obs = obs.groupby(obs.index).mean()
        obs = obs.groupby(obs.index).mean().rename('obs')
        
        #~ obs_int=pd.concat([obs,simu],axis=1).interpolate(method='slinear',limit=50)[0]
        obs_int=pd.concat([obs,simu],axis=1).interpolate(method='slinear',limit=50)['obs']
        obs_intD = obs_int.resample('D').mean()
        simuD = simu.resample('D').mean()
        merge=pd.concat([obs_intD.rename('obs'),simuD.rename('sim')],axis=1)
        #~ obs_intD.name = 'obs'
        #~ simuD.name = 'sim'
        #~ merge=pd.concat([obs_intD,simuD],axis=1)
        kept = merge.dropna()
        res = obj.kge(kept['obs'].values, kept['sim'].values, return_all=True)
        print(res)  
      

        #2 columns version:
        #~ if i>=int(np.ceil(len(stadic)/2)):
            #~ j=1
            #~ i=0
        if (k>=int(np.ceil(len(stadic)/3))) & (k<2*int(np.ceil(len(stadic)/3))):
            j=1
            i=k-int(np.ceil(len(stadic)/3))
        elif k>=2*int(np.ceil(len(stadic)/3)):
            j=2
            i=k-2*int(np.ceil(len(stadic)/3))        
            
            
        #~ ax[i][j].plot(sta.WT.loc['2007-1-1':'2007-12-31'].dropna(),'k')
        ax[i][j].plot(sta.WT.dropna().sort_index(axis=0),'k')
        
        #~ ax[i][j].plot(sta.WTsim.loc['2007-1-1':'2007-12-31'].dropna(),'r')
        #~ ax[i][j].plot(sta.WTsim.dropna().sort_index(axis=0),'r')
        #~ for k in range(sim.nz):
        for k2 in range(len(sta.wts.columns)):
            #~ ax[i][j].plot(sta.wts[k].dropna().sort_index(axis=0),'r')
            ax[i][j].plot(sta.wts.iloc[:,k2].dropna().sort_index(axis=0),'r')
        ax[i][j].plot(obs_intD,'b')
                
        
        #~ ax[i].plot(sta.WTsim.loc['2007-1-1':'2007-12-31'],'r')
        if j==0: ax[i][j].set_ylabel('WTD(m)')

        if i==0: ax[i][j].legend(['obs','sim'],fontsize=8,loc='upper left',ncol=2)
        #~ ax[i,j].set_xlim([datetime.datetime(2002,1,1),datetime.datetime(2011,12,31)])
        ax[i,j].set_xlim([datetime.datetime(2004,1,1),datetime.datetime(2007,12,31)])
        ax[i,j].set_ylim([-0.5,25])
        #~ ax[i,j].text(datetime.datetime(2004,3,25),18,r'%s: $r^2 = %2.2f$ KGE = %2.2f'%(stationname,res[1]*res[1],res[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})
        ax[i,j].text(datetime.datetime(2004,3,25),18,r'%s'%stationname,FontSize=10,bbox = {'facecolor':'white', 'alpha':0.7})
           
        ax[i,j].tick_params(axis='x', which='both', labelbottom=False, labeltop=False)
        if j ==0: ax[i,j].tick_params(axis='y', which='both', labelright=False, labelleft=True)
        if j ==1: ax[i,j].tick_params(axis='y', which='both', labelright=False, labelleft=False)
        if j ==2: ax[i,j].tick_params(axis='y', which='both', labelright=True, labelleft=False)
        if j ==0: ax[i,j].tick_params(axis='both', which='major', bottom=False,top=False,right=False,left=True)
        if j ==1: ax[i,j].tick_params(axis='both', which='major', bottom=False,top=False,right=False,left=False)
        if j ==2: ax[i,j].tick_params(axis='both', which='major', bottom=False,top=False,right=True,left=False)
        i+=1
        k+=1
        
    #~ for i in range(len(stadic)):
    fig.subplots_adjust(bottom=0.06, top =0.98,left=0.05,right =0.96,wspace=0.0, hspace=0.000)
        #~ fig.axes[i].invert_yaxis() # if share_y = True this may not work : all axes now behave as if their were one. For instance, when you invert one of them, you affect all 
    fig.axes[0].invert_yaxis() #if share_y=True            
    ax[0,0].tick_params(axis='both', which='major', bottom=False,top=True,right=False,left=True)
    ax[0,1].tick_params(axis='both', which='major', bottom=False,top=True,right=False,left=False)
    ax[0,2].tick_params(axis='both', which='major', bottom=False,top=True,right=True,left=False)
    ax[int(np.ceil(len(stadic)/3))-1,0].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)
    ax[int(np.ceil(len(stadic)/3))-1,1].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)
    ax[int(np.ceil(len(stadic)/3))-2,2].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)
    ax[int(np.ceil(len(stadic)/3))-2,2].tick_params(axis='x', which='major', bottom=True, labeltop=False)
    fig.autofmt_xdate()
    #~ ax[int(np.ceil(len(stadic)/3))-1,2].set_axis_off()

    if savefig:
        plt.savefig(os.sep.join([simdir,'WTD.png']),dpi=400,format='png')
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
"""Read in Data"""
######### Oueme
#~ WTOu= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/WTOu.csv')
#~ WTOu= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/processed_data/WTOu_added_uncheckeddata_12h.csv')
WTOu= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/processed_data/WT_Oueme_smoothedDailyMin.csv')
WTOu = WTOu.set_index(WTOu.columns[0])
WTOu.index = pd.to_datetime(WTOu.index,format='%Y-%m-%d %H:%M:%S')
WTOu.index.rename('Date',inplace=True)
######### oueme transect (belle, naloh, etc)
WTOut= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/processed_data/WT_Oueme_transect_smoothedDailyMean.csv')
WTOut = WTOut.set_index(WTOut.columns[0])
WTOut.index = pd.to_datetime(WTOut.index,format='%Y-%m-%d %H:%M:%S')
WTOut.index.rename('Date',inplace=True)
WTOut.drop(['Nalo_P034_20_2013-2015','Nalo_P190_20_2013-2015'],axis=1,inplace=True)
WTOut = WTOut.where(WTOut<1000,np.nan)   
#~ stations_to_keep = ['NALO-P034-10','NALO-P190-11','NALO-P500-10','Bele_P0099_120','Bele_P0192_120','Bele_P0312_100','Bele_P0688_22','Bele_P0968_24','Bele_P1250_21']
#~ stations_to_keep = ['Bele_P0099_120','Bele_P0192_120','Bele_P0312_100','Bele_P0688_22','Bele_P0968_24','Bele_P1250_21']
stations_to_keep = ['Bele_P0099_120','Bele_P0192_120','Bele_P0312_100','Bele_P0464_100','Bele_P0688_22','Bele_P0968_24','Bele_P1250_21']
#~ WTOut=WTOut[stations_to_keep]

WT=pd.concat([WTOu,WTOut],axis=1)

WT=-WT

######### METADATA
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/WTOuSta.csv')
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/WTOuSta_added_uncheckeddata_12h.csv')
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/WTOuSta_added_uncheckeddata_12h_added_precip_from_AC.csv')
WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/processed_data/WTOuSta_added_uncheckeddata_12h_added_precip_from_AC.csv')
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/WTOuSta_added_uncheckeddata_12h_15Dmedianmov_window_added_precip_from_AC.csv')
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/WTOuSta_smoothedDailyMin.csv')
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/WTOuSta_smoothedDailyMin_P001deg_1day.csv')
WTOuSta = WTOuSta.set_index(WTOuSta.columns[0])

WTOuSta.loc['NALO-P005-12',['x','y']]=[1.6048,9.7469]
WTOuSta.loc['NALO-P034-02',['x','y']]=[1.6049,9.7466]
WTOuSta.loc['NALO-P034-10',['x','y']]=[1.6049,9.7466]
WTOuSta.loc['NALO-P190-02',['x','y']]=[1.6053,9.7454]
WTOuSta.loc['NALO-P190-11',['x','y']]=[1.6053,9.7454]
WTOuSta.loc['NALO-P500-2',['x','y']]=[1.6064,9.743]
WTOuSta.loc['NALO-P500-10',['x','y']]=[1.6064,9.743]
WTOuSta.loc['NALO-P500-18',['x','y']]=[1.6064,9.743]


""" Find the location of data in the PF framework"""
P = xr.open_dataset(os.sep.join([simdir1,''.join([simuname1,'_press.nc'])]),decode_times=False)
[xx,yy]=np.meshgrid(P.longitude,P.latitude)
xx = np.round(xx)
yy = np.round(yy)

stadic ={}
#create a dictionnary of station objects
for stationname in WT.columns:
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    sta.WT=WT[stationname]

    WTOuSta['xsim_ind'],WTOuSta['ysim_ind'],WTOuSta['xsim'],WTOuSta['ysim'] = zip(*WTOuSta.apply(lambda col: find_grid_location(xx,yy,col.x,col.y),axis=1))
    sta.x=WTOuSta.loc[stationname,'x'] 
    sta.y=WTOuSta.loc[stationname,'y'] 
    sta.xsim=WTOuSta.loc[stationname,'xsim']  
    sta.ysim=WTOuSta.loc[stationname,'ysim']
    sta.xsim_ind=WTOuSta.loc[stationname,'xsim_ind']  
    sta.ysim_ind=WTOuSta.loc[stationname,'ysim_ind']
    stadic[stationname]=sta
    
stadic['Bele_P0099_120'].z=403.35-1.35
stadic['Bele_P0192_120'].z=408.65-0.76
stadic['Bele_P0312_100'].z=412.56-0.8
stadic['Bele_P0464_100'].z=416.59-0.8
stadic['Bele_P0688_22'].z=423.4-0.8
stadic['Bele_P0968_24'].z=431.68-0.8
stadic['Bele_P1250_21'].z=428.9-0.93
stadic['NALO-P005-12'].z=427.41
stadic['NALO-P034-02'].z=430.17
stadic['NALO-P034-10'].z=430.13
stadic['NALO-P190-02'].z=439.52
stadic['NALO-P190-11'].z=439.64
stadic['NALO-P500-2'].z=446.3
stadic['NALO-P500-10'].z=446.3
stadic['NALO-P500-18'].z=446.32
WTOuSta.loc['Bele_P0099_120','z']=403.35-1.35
WTOuSta.loc['Bele_P0192_120','z']=408.65-0.76
WTOuSta.loc['Bele_P0312_100','z']=412.56-0.8
WTOuSta.loc['Bele_P0464_100','z']=416.59-0.8
WTOuSta.loc['Bele_P0688_22','z']=423.4-0.8
WTOuSta.loc['Bele_P0968_24','z']=431.68-0.8
WTOuSta.loc['Bele_P1250_21','z']=428.9-0.93
WTOuSta.loc['NALO-P005-12','z']=427.41
WTOuSta.loc['NALO-P034-02','z']=430.17
WTOuSta.loc['NALO-P034-10','z']=430.13
WTOuSta.loc['NALO-P190-02','z']=439.52
WTOuSta.loc['NALO-P190-11','z']=439.64
WTOuSta.loc['NALO-P500-2','z']=446.3
WTOuSta.loc['NALO-P500-10','z']=446.3
WTOuSta.loc['NALO-P500-18','z']=446.32

"""ET flux tower locations"""
x_ET_nalohou = 1.6
y_ET_nalohou = 9.74
x_ET_Bellefoungou = 1.718
y_ET_Bellefoungou = 9.791
x_ET_nalohou_UTM, y_ET_nalohou_UTM = pyproj.transform(geo_system,proj,x_ET_nalohou, y_ET_nalohou)
x_ET_Bellefoungou_UTM, y_ET_Bellefoungou_UTM = pyproj.transform(geo_system,proj,x_ET_Bellefoungou, y_ET_Bellefoungou)
x_ET_UTM=[x_ET_nalohou_UTM,x_ET_Bellefoungou_UTM]
y_ET_UTM=[y_ET_nalohou_UTM,y_ET_Bellefoungou_UTM]

""" Get stream network:"""
data,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/upg_merit_Benin_UTM_1km_masked_thinned_corr.tif",nodata_value = 255)
[lonlon,latlat]=np.meshgrid(londem,latdem)
""" Get DEM:"""
DEM_filename='/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/Oueme_merit_f3_m1_newriv_DEM.pfb'
DEM = np.swapaxes(iopfb.PFB_read(DEM_filename,0)[:,:,0],0,1)

""" Read in sim outputs using xarray"""
P1 = xr.open_dataset(os.sep.join([simdir1,''.join([simuname1,'_press.nc'])]),decode_times=False)
tsim1 = pd.date_range('1/1/2005 00:00:00', periods=len(P1.time), freq='D')
# for saturation & pression
#~ tsim_full = np.insert(tsim,0,tsim[0]- (tsim[1]-tsim[0])) #for satur and press
P1 = P1.assign_coords(time = pd.to_datetime(tsim1)) #for satur and press



# not sure this is very python friendly:
P1['DEM']=P1['veg_map']  
P1['DEM'].data=DEM
P1['hillshade']=P1['DEM']
#N-W
#~ ls = LightSource(azdeg=315, altdeg=45)  
#N-E good for Bele
ls = LightSource(azdeg=45, altdeg=45)  
P1['hillshade'].data=ls.hillshade(DEM,vert_exag=100,dx=1000,dy=1000)
""" Get HR DEM:"""
demHR,xdem,ydem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/DATA/GeoRefs/Oueme/Oueme/DEM/elv_merit_oueme_UTM.tif",nodata_value = 9999)
demHR=demHR[::-1,:]
[xxdem,yydem]=np.meshgrid(xdem,ydem)
demHRhill=ls.hillshade(demHR,vert_exag=100,dx=1000,dy=1000)

#~ [xx,yy]=np.meshgrid(x,y)

#~ data[data!=0] =1
#~ vegmap[vegmap==7]=1
#~ vegmap[vegmap==8]=2
#~ vegmap[vegmap==19]=3
P1['veg_map']=P1.veg_map.where(P1.veg_map!=7,1) 
P1['veg_map']=P1.veg_map.where(P1.veg_map!=8,2) 
P1['veg_map']=P1.veg_map.where(P1.veg_map!=19,3) 
#~ P1['veg_map'] = P1.veg_map.where(P1.mask.sel(depth=0)==1,np.nan)          
#~ topmask[topmask<=0]=np.nan
#~ topmask[topmask>0]=1

#~ vegmasked = np.transpose(vegmap*topmask,[1,0])
#~ p2 = ax.pcolormesh(xx, yy, vegmap[::-1,::-1], cmap=plt.cm.get_cmap("YlGn_r",3), alpha=0.7)
#~ p2 = ax.pcolormesh(xx-sim.dx/2, yy-sim.dy/2, vegmasked, cmap=plt.cm.get_cmap("YlGn_r",3), alpha=0.7)
""" to check the location, use the stream mask:"""
#~ p = ax.pcolormesh(lonlon-sim.dx/2, latlat-sim.dy/2, np.ma.masked_equal(data[::-1,:],0), cmap="binary_r",alpha=0.5)

"""
plot station map: Bellefoungou
#~ https://stackoverflow.com/questions/40899350/python-quiver-and-pcolormesh-not-lining-up-exactly-right     
"""
fig = plt.figure(figsize=(16,8))
ax = fig.add_subplot(111)
ax.set_aspect(1)
# OK for bele for HR DEM:
p0 = ax.pcolormesh(xxdem, yydem, demHR,vmin=380,vmax=440, cmap='gray')
#
#~ p0 = ax.pcolormesh(xxdem, yydem, demHRhill, cmap='gray')
# OK for bele for simulation DEM:
#~ p0=P1.hillshade.plot(ax=ax,cmap='gray',add_colorbar=False)  
#~ p0=P1.DEM.plot(ax=ax,cmap='gray',vmin=380,vmax=410,add_colorbar=False)  
#alpha 0.3 to compare with higher resolution local DEM or 0.1 to compare with similar grid dem (should actually hatch it)
p2=P1.veg_map.plot(ax=ax,cmap=plt.cm.get_cmap("YlGn_r",3), alpha=0.3,add_colorbar=False)  
cb = plt.colorbar(p2,ticks=[1,2,3])
#~ cb.set_label('vegetation class',fontsize=20)
cb.ax.set_yticklabels(['Trees', 'Open Savanna', 'Herbaceous'],fontsize = 14)
cb.solids.set(alpha=1)
cb2 = plt.colorbar(p0,label='Z (m)')

ax.set_xlabel('X utm (m)',fontsize=20)
ax.set_ylabel('Y utm (m)',fontsize=20)

ax.set_ylim([y_ET_Bellefoungou_UTM-3500,y_ET_Bellefoungou_UTM+3500])
ax.set_xlim([x_ET_Bellefoungou_UTM-3500,x_ET_Bellefoungou_UTM+3500])
#~ ax.set_ylim([np.min(latlat)-10000,np.max(latlat)+5000])
#~ ax.plot([sta.x for stationname,sta in stadic.items()],[sta.y for stationname,sta in stadic.items()],'k+',markersize=18,markeredgewidth=4)

#~ l1, = ax.plot([sta.x_sim for stationname,sta in stadic.items()],[sta.y_sim for stationname,sta in stadic.items()],'r+',markersize=18,markeredgewidth=4)
#~ l2, = ax.plot([sta.x for stationname,sta in stadic_WT.items()],[sta.y for stationname,sta in stadic_WT.items()],'b+',markersize=18,markeredgewidth=4)
l1, = ax.plot(WTOuSta.loc[stations_to_keep,'x'],WTOuSta.loc[stations_to_keep,'y'],'b+',markersize=18,markeredgewidth=4)
l2, = ax.plot(WTOuSta.loc[stations_to_keep,'xsim'],WTOuSta.loc[stations_to_keep,'ysim'],'bo',markersize=6,markeredgewidth=4)

"""station names for Q stations:"""
#~ props = dict(boxstyle='round', facecolor='white', alpha=0.5)
#~ for stationname,sta in stadic.items():
    #~ if stationname=='AFFON_PONT':
        #~ ax.text(sta.x_sim, sta.y_sim+3000,stationname,fontsize = 12, bbox=props)
    #~ else:
        #~ ax.text(sta.x_sim, sta.y_sim,stationname,fontsize = 12, bbox=props)
"""station names for WT stations:"""
props = dict(boxstyle='round', facecolor='white', alpha=0.5)
#~ for stationname,sta in stadic_WT.items():
for stationname,sta in stadic.items():
    ax.text(sta.x, sta.y,stationname,fontsize = 6, bbox=props)
	
l3, = ax.plot(x_ET_UTM,y_ET_UTM,'y+',markersize=18,markeredgewidth=4,zorder = 10000)

#~ ax.legend((l1,l2,l3),('Streamgauges','Wells','Flux towers'),fontsize=14,loc='lower left',ncol=3)
ax.legend((l1,l3),('Piezometers','Flux tower'),fontsize=14,loc='lower left',ncol=3)

""""""""""""""""""""""""
""""""""""""""""""""""""

"""
plot time series
"""
z=np.cumsum(P1.var_dz[::-1,0,0]*3) -  P1.var_dz[::-1,0,0]*3/2

stadic['Bele_P1250_21'].wts=xtract_WTs(P1,stadic['Bele_P1250_21'].xsim,stadic['Bele_P1250_21'].ysim,z)

# 2D altidude plot
#~ ax1=WT[stations_to_keep].apply(lambda x: x-WTOuSta.loc[x.name,'Z']).plot()   
ax1=WT[stations_to_keep].apply(lambda x: WTOuSta.loc[x.name,'z']-x).plot()   
#~ ax1.invert_yaxis()
# 3D altitude x time plot:
# https://stackoverflow.com/questions/42677160/matplotlib-3d-scatter-plot-date
from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure(figsize=plt.figaspect(0.5))

"""Bele"""
t=[ (d-WT.index[0]).days for d in WT.index]
#~ fig = plt.figure()
#~ ax = fig.add_subplot(111, projection='3d')
ax = fig.add_subplot(1, 2, 1, projection='3d')
#~ ax.plot(np.zeros(len(t))+99,t,WTOuSta.loc['Bele_P0099_120','z']-WT['Bele_P0099_120'].data,'r')
#~ ax.plot(np.zeros(len(t))+192,t,WTOuSta.loc['Bele_P0192_120','z']-WT['Bele_P0192_120'].data,'m')
#~ ax.plot(np.zeros(len(t))+312,t,WTOuSta.loc['Bele_P0312_100','z']-WT['Bele_P0312_100'].data,'y')
#~ ax.plot(np.zeros(len(t))+464,t,WTOuSta.loc['Bele_P0464_100','z']-WT['Bele_P0464_100'].data,'y')
#~ ax.plot(np.zeros(len(t))+688,t,WTOuSta.loc['Bele_P0688_22','z']-WT['Bele_P0688_22'].data,'c')
#~ ax.plot(np.zeros(len(t))+968,t,WTOuSta.loc['Bele_P0968_24','z']-WT['Bele_P0968_24'].data,'b')
#~ ax.plot(np.zeros(len(t))+1250,t,WTOuSta.loc['Bele_P1250_21','z']-WT['Bele_P1250_21'].data,'g')
ax.plot(np.zeros(len(t))+99,t,WTOuSta.loc['Bele_P0099_120','z']-WT['Bele_P0099_120'].values,'r')
ax.plot(np.zeros(len(t))+192,t,WTOuSta.loc['Bele_P0192_120','z']-WT['Bele_P0192_120'].values,'m')
ax.plot(np.zeros(len(t))+312,t,WTOuSta.loc['Bele_P0312_100','z']-WT['Bele_P0312_100'].values,'y')
ax.plot(np.zeros(len(t))+464,t,WTOuSta.loc['Bele_P0464_100','z']-WT['Bele_P0464_100'].values,'y')
ax.plot(np.zeros(len(t))+688,t,WTOuSta.loc['Bele_P0688_22','z']-WT['Bele_P0688_22'].values,'c')
ax.plot(np.zeros(len(t))+968,t,WTOuSta.loc['Bele_P0968_24','z']-WT['Bele_P0968_24'].values,'b')
ax.plot(np.zeros(len(t))+1250,t,WTOuSta.loc['Bele_P1250_21','z']-WT['Bele_P1250_21'].values,'g')
dist_to_riv=[99,192,312,464,688,968,1250]
depth=np.array([12.51,12.3,10.04,10,22.7,24.43,20.76])
crepine=np.array([2,2,2,2,5.5,7.73,3.83])
surf_elev=np.array([WTOuSta.loc['Bele_P0099_120','z'],WTOuSta.loc['Bele_P0192_120','z'],
WTOuSta.loc['Bele_P0312_100','z'],WTOuSta.loc['Bele_P0464_100','z'],WTOuSta.loc['Bele_P0688_22','z'],
WTOuSta.loc['Bele_P0968_24','z'],WTOuSta.loc['Bele_P1250_21','z']])

ax.plot(dist_to_riv,np.zeros(len(dist_to_riv))+np.mean(t),surf_elev,'k')
ax.plot(dist_to_riv,np.zeros(len(dist_to_riv))+np.mean(t),surf_elev-depth,'k+')
ax.plot(dist_to_riv,np.zeros(len(dist_to_riv))+np.mean(t),surf_elev-(depth-crepine),'k+')
dt = [ pd.Timedelta(d,unit='D') + WT.index[0]  for d in  ax.get_yticks()]
#~ dtStr = [d.isoformat() for d in dt]
dtStr = [d.strftime('%Y-%m-%d') for d in dt]
ax.set_yticklabels(dtStr)
ax.set_xlabel('length to river (m)')
ax.set_zlabel('elevation')
ax.set_xlim([0,1300])
zlim=ax.get_zlim()

"""Nalo"""
t=[ (d-WT.index[0]).days for d in WT.index]
#~ fig = plt.figure()
#~ ax = fig.add_subplot(111, projection='3d')
ax = fig.add_subplot(1, 2, 2, projection='3d')
WTOuSta.loc['NALO-P005-12','z']=427.41
WTOuSta.loc['NALO-P034-02','z']=430.17
WTOuSta.loc['NALO-P034-10','z']=430.13
WTOuSta.loc['NALO-P190-02','z']=439.52
WTOuSta.loc['NALO-P190-11','z']=439.64
WTOuSta.loc['NALO-P500-2','z']=446.3
WTOuSta.loc['NALO-P500-10','z']=446.3
WTOuSta.loc['NALO-P500-18','z']=446.32

#~ plt.plot(np.zeros(len(t))+5,t,WTOuSta.loc['NALO-P005-12','z']-WT['NALO-P005-12'].data,'r')
#~ plt.plot(np.zeros(len(t))+34,t,WTOuSta.loc['NALO-P034-02','z']-WT['NALO-P034-02'].data,'m')
#~ plt.plot(np.zeros(len(t))+34,t,WTOuSta.loc['NALO-P034-10','z']-WT['NALO-P034-10'].data,'y')
#~ plt.plot(np.zeros(len(t))+190,t,WTOuSta.loc['NALO-P190-02','z']-WT['NALO-P190-02'].data,'y')
#~ plt.plot(np.zeros(len(t))+190,t,WTOuSta.loc['NALO-P190-11','z']-WT['NALO-P190-11'].data,'c')
#~ plt.plot(np.zeros(len(t))+500,t,WTOuSta.loc['NALO-P500-2','z']-WT['NALO-P500-2'].data,'b')
#~ plt.plot(np.zeros(len(t))+500,t,WTOuSta.loc['NALO-P500-10','z']-WT['NALO-P500-10'].data,'g')
#~ plt.plot(np.zeros(len(t))+500,t,WTOuSta.loc['NALO-P500-18','z']-WT['NALO-P500-18'].data,'g')

plt.plot(np.zeros(len(t))+5,t,WTOuSta.loc['NALO-P005-12','z']-WT['NALO-P005-12'].values,'r')
plt.plot(np.zeros(len(t))+34,t,WTOuSta.loc['NALO-P034-02','z']-WT['NALO-P034-02'].values,'m')
plt.plot(np.zeros(len(t))+34,t,WTOuSta.loc['NALO-P034-10','z']-WT['NALO-P034-10'].values,'y')
plt.plot(np.zeros(len(t))+190,t,WTOuSta.loc['NALO-P190-02','z']-WT['NALO-P190-02'].values,'y')
plt.plot(np.zeros(len(t))+190,t,WTOuSta.loc['NALO-P190-11','z']-WT['NALO-P190-11'].values,'c')
plt.plot(np.zeros(len(t))+500,t,WTOuSta.loc['NALO-P500-2','z']-WT['NALO-P500-2'].values,'b')
plt.plot(np.zeros(len(t))+500,t,WTOuSta.loc['NALO-P500-10','z']-WT['NALO-P500-10'].values,'g')
plt.plot(np.zeros(len(t))+500,t,WTOuSta.loc['NALO-P500-18','z']-WT['NALO-P500-18'].values,'g')
dist_to_riv=[5,34,34,190,190,500,500,500]
depth=np.array([12.0,2.13,10.27,1.98,9.98,1.93,9.28,16.72])
surf_elev=np.array([WTOuSta.loc['NALO-P005-12','z'],WTOuSta.loc['NALO-P034-02','z'],
WTOuSta.loc['NALO-P034-10','z'],WTOuSta.loc['NALO-P190-02','z'],WTOuSta.loc['NALO-P190-11','z'],
WTOuSta.loc['NALO-P500-2','z'],WTOuSta.loc['NALO-P500-10','z'],WTOuSta.loc['NALO-P500-18','z']])

plt.plot(dist_to_riv,np.zeros(len(dist_to_riv))+np.mean(t),surf_elev,'k')
plt.plot(dist_to_riv,np.zeros(len(dist_to_riv))+np.mean(t),surf_elev-depth,'k+')
dt = [ pd.Timedelta(d,unit='D') + WT.index[0]  for d in  ax.get_yticks()]
#~ dtStr = [d.isoformat() for d in dt]
dtStr = [d.strftime('%Y-%m-%d') for d in dt]
ax.set_yticklabels(dtStr)
ax.set_xlabel('length to river (m)')
ax.set_zlabel('elevation')
ax.set_xlim([0,1300])
ax.set_zlim(ax.get_zlim()[0],ax.get_zlim()[0]+zlim[1]-zlim[0])



"""
plot WTD - amp relationships
"""
fig,ax = plt.subplots(1,1,figsize = [7,7])
bele_stations=[station for station in WT.mean().index if 'Bele' in station]
bele_stations.pop(np.where(np.array(bele_stations)=='Bele_P0464_100')[0][0]) 
tmp=(WT.resample('Y').max()- WT.resample('Y').min()).mean()
plt.scatter(WT.mean().loc[bele_stations],tmp.loc[bele_stations],c='g')

nalohou_stations=[station for station in WT.mean().index if 'NALO' in station]
nalohou_stations=['NALO-P005-12','NALO-P034-10','NALO-P190-11','NALO-P500-18']
plt.scatter(WT.mean().loc[nalohou_stations],tmp.loc[nalohou_stations],c='r')

plt.legend(['Trees (belefoungou)','Herbaceous (Nalohou)'])
ax.set_xlabel('WTD (m)',fontsize=14)
ax.set_ylabel('amplitude (m)',fontsize=14)

""" Read Nalohou data"""
WTnal = pd.read_csv('/home/hectorb/DATA/WT/Nalohou/WTNal.csv')   
WTnal = WTnal.set_index(WTnal.columns[0])
WTnal.index = pd.to_datetime(WTnal.index,format='%Y-%m-%d %H:%M:%S')
WTnal.index.rename('Date',inplace=True)
WTnal = - WTnal
#~ WTOut.drop(['Nalo_P034_20_2013-2015','Nalo_P190_20_2013-2015'],axis=1,inplace=True)
#~ WTOut = WTOut.where(WTOut<1000,np.nan)   
tmpnal =(WTnal.resample('Y').max()- WTnal.resample('Y').min()).mean()

""" Read DG Eau data"""
WTDG = pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/DGEau/processed_data/WT_DGEau_smoothedDailyMean.csv')
WTDG = WTDG.set_index(WTDG.columns[0])
WTDG.index = pd.to_datetime(WTDG.index,format='%Y-%m-%d %H:%M:%S')
WTDG.index.rename('Date',inplace=True)
#~ WT.plot()
WTDGSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/DGEau/processed_data/WTsta_smoothedDailyMean.csv')
WTDGSta = WTDGSta.set_index(WTDGSta.columns[0])

WTDG.drop(labels='Nikki_Ouénou',axis=1,inplace=True) 
WTDG=-WTDG
fig,ax = plt.subplots(1,1,sharex=True)
WTDG.plot(ax=ax)
ax.invert_yaxis()
ax.set_ylabel('WTD (m)')  
plt.tight_layout()

WTDG_village=WTDG.loc[:,['Bassila_Kikélé']]
WTDG_herb=WTDG.loc[:,['Bétérou_centre','Bembèrèkè_CEG_Ina','Tchitchakou_Ouaké']]
WTDG_trees=WTDG.loc[:,['NDali_Komiguéa','Bari_kpédoré']]

WTDGSta_village=WTDGSta.loc[['Bassila_Kikélé'],:]
WTDGSta_herb=WTDGSta.loc[['Bétérou_centre','Bembèrèkè_CEG_Ina','Tchitchakou_Ouaké'],:]
WTDGSta_trees=WTDGSta.loc[['NDali_Komiguéa','Bari_kpédoré'],:]

""" With simulation"""
fig,ax = plt.subplots(1,1,figsize = [7,7])
WT2006 = WT.loc[(WT.index>=pd.datetime(2006,1,1)) & (WT.index<=pd.datetime(2006,12,31)),:]

P1m=P1.where(P1.mask>0).sel(time=slice('2006-01-01', '2006-12-20'))
tot_thickness1= np.array(P1.var_dz[::-1,50,50]*3).cumsum()[-1] - P1.var_dz[0,50,50]*3/2
amp = (P1m.succion[:,0,:,:].max(dim='time') - P1m.succion[:,0,:,:].min(dim='time'))
depth = (tot_thickness1 - P1m.succion[:,0,:,:].mean(dim='time'))

ax.scatter(depth.where(P1.veg_map==1,np.nan),amp.where(P1.veg_map==1,np.nan),c='g',s=0.2)#'Trees'
ax.scatter(depth.where(P1.veg_map==2,np.nan),amp.where(P1.veg_map==2,np.nan),c='y',s=0.2)#'Open Savanna'
ax.scatter(depth.where(P1.veg_map==3,np.nan),amp.where(P1.veg_map==3,np.nan),c='r',s=0.2)#'Herbaceous'

#~ l1=ax.scatter(depth.where(P1.veg_map==7,np.nan),amp.where(P1.veg_map==7,np.nan),c='g',s=0.1)#'Trees'
#~ l2=ax.scatter(depth.where(P1.veg_map==8,np.nan),amp.where(P1.veg_map==8,np.nan),c='y',s=0.1)#'Open Savanna'
#~ l3=ax.scatter(depth.where(P1.veg_map==19,np.nan),amp.where(P1.veg_map==19,np.nan),c='r',s=0.1)#'Herbaceous'
#~ plt.legend(['Trees','Open Savannah','Herbaceous'])
#~ ax.set_xlabel('WTD (m)',fontsize=14)
#~ ax.set_ylabel('amplitude (m)',fontsize=14)

#~ ax.scatter(depth,amp,c='g',s=0.15)
#~ plt.legend(['obs (belefoungou)','Trees','Open Savannah','Herbaceous'])
bele_stations=[station for station in WT.mean().index if 'Bele' in station]
bele_stations.pop(np.where(np.array(bele_stations)=='Bele_P0464_100')[0][0]) 
#~ tmp=(WT.resample('Y').max()- WT.resample('Y').min()).mean()
tmp=(WT2006.resample('Y').max()- WT2006.resample('Y').min()).mean()
#~ obsbele=plt.scatter(WT.mean().loc[bele_stations],tmp.loc[bele_stations],c='g',edgecolors='k')
obsbele=plt.scatter(WT2006.mean().loc[bele_stations],tmp.loc[bele_stations],c='g',edgecolors='k')
obsdgeau=plt.scatter(-WTDGSta_trees['mean'],WTDGSta_trees['meanAmp'],c='g',marker='P',edgecolors='k')

nalohou_stations=[station for station in WT.mean().index if 'NALO' in station]
nalohou_stations=['NALO-P005-12','NALO-P034-10','NALO-P190-11','NALO-P500-18']
#~ obsnal=plt.scatter(WT.mean().loc[nalohou_stations],tmp.loc[nalohou_stations],c='k',marker='+')
obsnal=plt.scatter(WT2006.mean().loc[nalohou_stations],tmp.loc[nalohou_stations],c='k',marker='+')
#~ obsnal=plt.scatter(WTnal.mean(),tmpnal,c='r',edgecolors='k')
obsdgeau=plt.scatter(-WTDGSta_herb['mean'],WTDGSta_herb['meanAmp'],c='r',marker='P',edgecolors='k')


#~ plt.legend(['Trees (sim)','Open Savanna (sim)','Herbaceous (sim)','Trees (obs)','Herbaceous (obs)'])
plt.legend(['Trees (sim)','Open Savanna (sim)','Herbaceous (sim)','Trees (obs)','Trees (obs DGEau)','Herbaceous (obs)','Herbaceous (obs DGEau)'])
ax.set_xlabel('WTD (m)',fontsize=14)
ax.set_ylabel('amplitude (m)',fontsize=14)

""" save for paper"""

Nalo_profile = pd.DataFrame({'WTD':WT.mean().loc[nalohou_stations],'WTA':tmp.loc[nalohou_stations] })
Nalo_all = pd.DataFrame({'WTD':WTnal.mean(),'WTA':tmpnal})
obsdgeau_herb = pd.DataFrame({'WTD':-WTDGSta_herb['mean'],'WTA':WTDGSta_herb['meanAmp']})
obsdgeau_tree = pd.DataFrame({'WTD':-WTDGSta_trees['mean'],'WTA':WTDGSta_trees['meanAmp']})
Bele_profile = pd.DataFrame({'WTD':WT.mean().loc[bele_stations],'WTA':tmp.loc[bele_stations] })

Nalo_profile.to_csv('/home/hectorb/PUBLIS/InProgress/AmeliePF/data/Nalo_profile.csv')
Nalo_all.to_csv('/home/hectorb/PUBLIS/InProgress/AmeliePF/data/Nalo_all.csv')
obsdgeau_herb.to_csv('/home/hectorb/PUBLIS/InProgress/AmeliePF/data/obsdgeau_herb.csv')
obsdgeau_tree.to_csv('/home/hectorb/PUBLIS/InProgress/AmeliePF/data/obsdgeau_tree.csv')
Bele_profile.to_csv('/home/hectorb/PUBLIS/InProgress/AmeliePF/data/Bele_profile.csv')

plt.savefig('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/outputs/WTD_vs_amp_%s.png'%simuname1,dpi=500,format='png')


""""""

"""compare histogram with wells data"""

fig,ax = plt.subplots(1,1,figsize = [7,7])
(-WTOuSta.loc[:,'mean']).plot.hist(ax=ax,bins=np.arange(0,25,0.5),density=True,histtype='step',color='k',label='obs')
(tot_thickness1 - P1m.succion[:,0,:,:].mean(dim='time')).plot.hist(ax=ax,bins=np.arange(0,25,0.5),density=True,histtype='step',color='r',label=short1)   
plt.legend()
ax.set_xlabel('WTD (m)',fontsize=14)
ax.set_ylabel('frequency',fontsize=14)
plt.savefig('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/outputs/WTD_dist_%s.png'%simuname1,dpi=500,format='png')


""""""
ax=WT[stations_to_keep].plot()        
ax.invert_yaxis()
tmp=stadic['Bele_P1250_21'].wts
for k2 in range(len(stadic['Bele_P1250_21'].wts.columns)):
    ax.plot(stadic['Bele_P1250_21'].wts.iloc[:,k2].dropna().sort_index(axis=0).loc[datetime.datetime(2005,1,1):datetime.datetime(2006,12,22)],'r')
            
tmp=stadic['Bele_P0099_120'].wts
for k2 in range(len(tmp.columns)):
    ax.plot(tmp.iloc[:,k2].dropna().sort_index(axis=0).loc[datetime.datetime(2005,1,1):datetime.datetime(2006,12,22)],'k')

tmp=wts        
for k2 in range(len(tmp.columns)):
    ax.plot(tmp.iloc[:,k2].dropna().sort_index(axis=0).loc[datetime.datetime(2005,1,1):datetime.datetime(2006,12,22)],'g')
               



P2 = xr.open_dataset(os.sep.join([simdir2,''.join([simuname2,'_press.nc'])]),decode_times=False)
tsim2 = pd.date_range('1/1/2005 00:00:00', periods=len(P2.time), freq='D')
# for saturation & pression
#~ tsim_full = np.insert(tsim,0,tsim[0]- (tsim[1]-tsim[0])) #for satur and press
P2 = P2.assign_coords(time = pd.to_datetime(tsim2)) #for satur and press
stadic2=get_WT_simulation_results(P2,copy.deepcopy(stadic))
plot_WT_time_series(stadic=stadic2,simdir=simdir2,savefig=True)

P3 = xr.open_dataset(os.sep.join([simdir3,''.join([simuname3,'_press.nc'])]),decode_times=False)
tsim3 = pd.date_range('1/1/2005 00:00:00', periods=len(P3.time), freq='D')
P3 = P3.assign_coords(time = pd.to_datetime(tsim3)) #for satur and press

stadic3=get_WT_simulation_results(P3,copy.deepcopy(stadic))
plot_WT_time_series(stadic=stadic3,simdir=simdir3,savefig=True)

P4 = xr.open_dataset(os.sep.join([simdir4,''.join([simuname4,'_press.nc'])]),decode_times=False)
tsim4 = pd.date_range('1/1/2005 00:00:00', periods=len(P4.time), freq='D')
P4 = P4.assign_coords(time = pd.to_datetime(tsim4)) #for satur and press
stadic4=get_WT_simulation_results(P4,copy.deepcopy(stadic))
plot_WT_time_series(stadic=stadic4,simdir=simdir4,savefig=True)


P1m=P1.where(P1.mask>0).sel(time=slice('2006-01-01', '2006-12-20'))
P2m=P2.where(P2.mask>0).sel(time=slice('2006-01-01', '2006-12-20'))
P3m=P3.where(P3.mask>0).sel(time=slice('2006-01-01', '2006-12-20'))
P4m=P4.where(P4.mask>0).sel(time=slice('2006-01-01', '2006-12-20'))
tot_thickness1= np.array(P1.var_dz[::-1,50,50]*3).cumsum()[-1] - P1.var_dz[0,50,50]*3/2
tot_thickness2= np.array(P2.var_dz[::-1,50,50]*3).cumsum()[-1] - P2.var_dz[0,50,50]*3/2
tot_thickness3= np.array(P3.var_dz[::-1,50,50]*3).cumsum()[-1] - P3.var_dz[0,50,50]*3/2
tot_thickness4= np.array(P4.var_dz[::-1,50,50]*3).cumsum()[-1] - P4.var_dz[0,50,50]*3/2


"""Plot maps: mean WTD"""
#~ fig,ax = plt.subplots(1,2,figsize = [7,7])
fig,ax = plt.subplots(2,2,figsize = [7,7])
for a in ax.flat: a.set_aspect(1)
#~ (tot_thickness - P1m.succion[:,0,:,:].mean(dim='time')).plot(ax=ax[0],robust=True,vmin=0,vmax=10,cbar_kwargs={'label':"WTD (m)"},add_labels=False)
#~ tmp = (tot_thickness - P2m.succion[:,0,:,:].mean(dim='time')).plot(ax=ax[1],robust=True,vmin=0,vmax=10,cbar_kwargs={'label':"WTD (m)"},add_labels=False)
ax1 = (tot_thickness1 - P1m.succion[:,0,:,:].mean(dim='time')).plot(ax=ax[0][0],robust=True,vmin=0,vmax=5,add_labels=False,add_colorbar=False)
ax2 = (tot_thickness2 - P2m.succion[:,0,:,:].mean(dim='time')).plot(ax=ax[0][1],robust=True,vmin=0,vmax=5,add_labels=False,add_colorbar=False)
ax3 = (tot_thickness3 - P3m.succion[:,0,:,:].mean(dim='time')).plot(ax=ax[1][0],robust=True,vmin=0,vmax=5,add_labels=False,add_colorbar=False)
ax4 = (tot_thickness4 - P4m.succion[:,0,:,:].mean(dim='time')).plot(ax=ax[1][1],robust=True,vmin=0,vmax=5,add_labels=False,add_colorbar=False)


#control colorbar
fig.colorbar(ax4, ax=ax.ravel().tolist(),label="WTD (m)")
#or:
#~ cbar_ax = fig.add_axes([0.15, 0.05, 0.7, 0.05])
#~ fig.colorbar(ax1, cax=cbar_ax,orientation='horizontal',label="WTD (m)")
for i,a in enumerate(ax.flat): a.set_xticklabels('');a.set_yticklabels('');a.set_title('simulation %d'%i)

ax[0][0].set_title(short1)
ax[0][1].set_title(short2)
ax[1][0].set_title(short3)
ax[1][1].set_title(short4)

#~ plt.gcf().subplots_adjust(bottom=0.05, top =0.95,left=0.15, hspace=0.02,wspace=0.2)

plt.savefig(os.sep.join([simdir1,'Figure_compare_WT_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir2,'Figure_compare_WT_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir3,'Figure_compare_WT_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir4,'Figure_compare_WT_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')


"""Plot maps: amplitude WTD"""
#~ fig,ax = plt.subplots(1,2,figsize = [7,7])
fig,ax = plt.subplots(2,2,figsize = [7,7])
for a in ax.flat: a.set_aspect(1)
#~ (tot_thickness - P1m.succion[:,0,:,:].mean(dim='time')).plot(ax=ax[0],robust=True,vmin=0,vmax=10,cbar_kwargs={'label':"WTD (m)"},add_labels=False)
#~ tmp = (tot_thickness - P2m.succion[:,0,:,:].mean(dim='time')).plot(ax=ax[1],robust=True,vmin=0,vmax=10,cbar_kwargs={'label':"WTD (m)"},add_labels=False)
ax1 = (P1m.succion[:,0,:,:].max(dim='time') - P1m.succion[:,0,:,:].min(dim='time')).plot(ax=ax[0][0],robust=True,vmin=0,vmax=5,add_labels=False,add_colorbar=False)
ax2 = (P2m.succion[:,0,:,:].max(dim='time') - P2m.succion[:,0,:,:].min(dim='time')).plot(ax=ax[0][1],robust=True,vmin=0,vmax=5,add_labels=False,add_colorbar=False)
ax3 = (P3m.succion[:,0,:,:].max(dim='time') - P3m.succion[:,0,:,:].min(dim='time')).plot(ax=ax[1][0],robust=True,vmin=0,vmax=5,add_labels=False,add_colorbar=False)
ax4 = (P4m.succion[:,0,:,:].max(dim='time') - P4m.succion[:,0,:,:].min(dim='time')).plot(ax=ax[1][1],robust=True,vmin=0,vmax=5,add_labels=False,add_colorbar=False)


#control colorbar
fig.colorbar(ax4, ax=ax.ravel().tolist(),label="WTD amplitude (m)")
#or:
#~ cbar_ax = fig.add_axes([0.15, 0.05, 0.7, 0.05])
#~ fig.colorbar(ax1, cax=cbar_ax,orientation='horizontal',label="WTD (m)")
for i,a in enumerate(ax.flat): a.set_xticklabels('');a.set_yticklabels('');a.set_title('simulation %d'%i)

ax[0][0].set_title(short1)
ax[0][1].set_title(short2)
ax[1][0].set_title(short3)
ax[1][1].set_title(short4)

#~ plt.gcf().subplots_adjust(bottom=0.05, top =0.95,left=0.15, hspace=0.02,wspace=0.2)

plt.savefig(os.sep.join([simdir1,'Figure_compare_WTamp_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir2,'Figure_compare_WTamp_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir3,'Figure_compare_WTamp_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir4,'Figure_compare_WTamp_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')



"""tmp"""
amp = (P1m.succion[:,0,:,:].max(dim='time') - P1m.succion[:,0,:,:].min(dim='time'))
depth = (tot_thickness1 - P1m.succion[:,0,:,:].mean(dim='time'))

amp2 = (P2m.succion[:,0,:,:].max(dim='time') - P2m.succion[:,0,:,:].min(dim='time')) 
depth2 = (tot_thickness2 - P2m.succion[:,0,:,:].mean(dim='time'))  

amp3 = (P3m.succion[:,0,:,:].max(dim='time') - P3m.succion[:,0,:,:].min(dim='time')) 
depth3 = (tot_thickness3 - P3m.succion[:,0,:,:].mean(dim='time'))   

amp4 = (P4m.succion[:,0,:,:].max(dim='time') - P4m.succion[:,0,:,:].min(dim='time')) 
depth4 = (tot_thickness4 - P4m.succion[:,0,:,:].mean(dim='time'))   

fig,ax = plt.subplots(1,1,figsize = [7,7])
#~ ax.scatter(amp,depth,c='k')
ax.scatter(amp.where(P1.veg_map==1,np.nan),depth.where(P1.veg_map==1,np.nan),c='g')#'Trees'
ax.scatter(amp.where(P1.veg_map==2,np.nan),depth.where(P1.veg_map==2,np.nan),c='y')#'Open Savanna'
ax.scatter(amp.where(P1.veg_map==3,np.nan),depth.where(P1.veg_map==3,np.nan),c='r')#'Herbaceous'
ax.scatter(amp2,depth2,c='b')
ax.scatter(amp3,depth3,c='r')
ax.scatter(amp4,depth4,c='g')

"""Plot hists"""
fig,ax = plt.subplots(1,1,figsize = [7,7])
(tot_thickness1 - P1m.succion[:,0,:,:].mean(dim='time')).plot.hist(ax=ax,bins=np.arange(0,5,0.2),density=True,histtype='step',color='r',label=short1)   
(tot_thickness2 - P2m.succion[:,0,:,:].mean(dim='time')).plot.hist(ax=ax,bins=np.arange(0,5,0.2),density=True,histtype='step',color='b',label=short2)   
(tot_thickness3 - P3m.succion[:,0,:,:].mean(dim='time')).plot.hist(ax=ax,bins=np.arange(0,5,0.2),density=True,histtype='step',color='m',label=short3)   
(tot_thickness4 - P4m.succion[:,0,:,:].mean(dim='time')).plot.hist(ax=ax,bins=np.arange(0,5,0.2),density=True,histtype='step',color='c',label=short4)   
plt.legend()
ax.set_xlabel('WTD (m)',fontsize=14)
ax.set_ylabel('frequency',fontsize=14)

plt.savefig(os.sep.join([simdir1,'Figure_compare_WT_hist_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir2,'Figure_compare_WT_hist_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir3,'Figure_compare_WT_hist_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir4,'Figure_compare_WT_hist_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')

"""Plot hists amplitude"""
fig,ax = plt.subplots(1,1,figsize = [7,7])
(P1m.succion[:,0,:,:].max(dim='time') - P1m.succion[:,0,:,:].min(dim='time')).plot.hist(ax=ax,bins=np.arange(0,5,0.2),density=True,histtype='step',color='r',label=short1)   
(P2m.succion[:,0,:,:].max(dim='time') - P2m.succion[:,0,:,:].min(dim='time')).plot.hist(ax=ax,bins=np.arange(0,5,0.2),density=True,histtype='step',color='b',label=short2)   
(P3m.succion[:,0,:,:].max(dim='time') - P3m.succion[:,0,:,:].min(dim='time')).plot.hist(ax=ax,bins=np.arange(0,5,0.2),density=True,histtype='step',color='m',label=short3)   
(P4m.succion[:,0,:,:].max(dim='time') - P4m.succion[:,0,:,:].min(dim='time')).plot.hist(ax=ax,bins=np.arange(0,5,0.2),density=True,histtype='step',color='c',label=short4)   
plt.legend()
ax.set_xlabel('WTD amplitude (m)',fontsize=14)
ax.set_ylabel('frequency',fontsize=14)

plt.savefig(os.sep.join([simdir1,'Figure_compare_WTamp_hist_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir2,'Figure_compare_WTamp_hist_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir3,'Figure_compare_WTamp_hist_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir4,'Figure_compare_WTamp_hist_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')

""" Read in sim outputs using xarray"""
#~ ET1 = xr.open_dataset(os.sep.join([simdir1,''.join([simuname1,'_qflx_evap_tot.nc'])]),decode_times=False)
#~ tsim = pd.date_range('1/1/2005 00:00:00', periods=len(ET1.time), freq='H')
#~ # for CLM outputs:
#~ ET1 = ET1.assign_coords(time = pd.to_datetime(tsim))

#~ ET2 = xr.open_dataset(os.sep.join([simdir2,''.join([simuname2,'_qflx_evap_tot.nc'])]),decode_times=False)
#~ tsim2 = pd.date_range('1/1/2005 00:00:00', periods=len(ET2.time), freq='H')
#~ ET2 = ET2.assign_coords(time = pd.to_datetime(tsim2))

#~ ET1=ET1.where(P1.mask==1).sel(time=slice('2005-01-01', '2006-12-20'))
#~ ET2=ET2.where(P2.mask==1).sel(time=slice('2005-01-01', '2006-12-20'))

#~ fig,ax = plt.subplots(1,1,figsize = [12,7])
#~ longitude = 0
#~ latitude = 0
#~ (ET1.resample(time='1D').sum().sel(longitude=longitude,latitude=latitude).qflx_evap_tot*3600).plot.line('b',ax=ax)
#~ (ET2.resample(time='1D').sum().sel(longitude=longitude,latitude=latitude).qflx_evap_tot*3600).plot.line('b--',ax=ax)
#~ (ET1.resample(time='1D').sum().mean(dim='longitude').mean(dim='latitude').qflx_evap_tot*3600).plot.line('b',ax=ax)
#~ (ET2.resample(time='1D').sum().mean(dim='longitude').mean(dim='latitude').qflx_evap_tot*3600).plot.line('r',ax=ax)

#~ plt.legend(['reference','new_sim'])
#~ ax.set_ylabel('mm/d')
#~ ax.set_ylim([-2,10])
#~ ax.set_ylim([0,12])







#example:
#~ fig,ax = plt.subplots(1,1,figsize = [7,7])
#~ ds.succion.mean(dim='time').mean(dim='depth').plot(ax=ax)
#~ (ds.succion[:,0,:,:].mean(dim='time')).plot(ax=ax,robust=True)
#~ (offset_for_WTD-ds.succion[:,0,:,:].mean(dim='time')).plot(ax=ax,cmap = 'viridis_r',robust=True, cbar_kwargs={'label':'WTD (m)'})
#~ for station in WTloc.Y.index:
    #~ ax.plot(WTloc.loc[station,'X'],WTloc.loc[station,'Y'],'+','r')
    #~ ax.text(WTloc.loc[station,'X'],WTloc.loc[station,'Y'],station,color ='r')
#~ ax.set_title("")
#~ ax.set_aspect('equal')


#~ filename = r'/home/hectorb/DATA/METEO/AO/ERA5/ERA5_AO.nc'
#~ xds = xarray.open_dataset(filename)
#~ xds_rolled = xds.assign_coords(longitude=(((xds.longitude + 180)%360)-180)).roll(longitude=(xds.dims['longitude'] // 2))
#~ tmp=xds_rolled.groupby('time.year').sum().mean(dim='year')                                                                                                      
#~ tmp.e_0001.sel(longitude = slice(-20,12),latitude = slice(20,4)).plot()           
#~ tmp.tp_0001.sel(longitude = slice(-20,12),latitude = slice(20,4)).plot()           

#~ (tmp.tp_0001.sel(longitude = slice(-20,12),latitude = slice(20,4))*30).plot()           
#~ ((tmp.tp_0001.sel(longitude = slice(-20,12),latitude = slice(20,4))*30) + (tmp.e_0001.sel(longitude = slice(-20,12),latitude = slice(20,4))*30))           

#~ (xds_rolled.tp_0001.sel(longitude = 3, latitude = 10).groupby('time.year').sum()*30).plot() 

#~ (tmp.tp_0001.sel(longitude = slice(-20,12),latitude = slice(20,4))*30).plot()           

#~ DB['P_ERA5'] = DB.apply(lambda x: float(tmp.tp_0001.interp(longitude = x.lon, latitude = x.lat).data*30) if (~np.isnan(x.lon)) &  (~np.isnan(x.lat)) else np.nan ,axis = 1)
#~ DB['PmET_ERA5'] = DB.apply(lambda x: float(tmp.tp_0001.interp(longitude = x.lon, latitude = x.lat).data*30 + tmp.e_0001.interp(longitude = x.lon, latitude = x.lat).data*30) if (~np.isnan(x.lon)) &  (~np.isnan(x.lat)) else np.nan ,axis = 1)



