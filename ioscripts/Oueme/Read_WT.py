#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    Anaylse PF outputs for Oueme: Water Table
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os,glob
from netCDF4 import Dataset
import time
import datetime
import pyproj
import rasterio
from matplotlib.dates import MonthLocator, DateFormatter

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
from procsycz import procGeodata_Gdal


plt.close("all")

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/test_2018_04_24'
simuname = 'test'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2017/oueme2D_KH3high4_2016_12_22'
simuname = 'oueme2D_KH3high4'


#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup_AH/oueme_2018_07_23'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_2018_08_29'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_Kapptot_exp30_CIconst_2018_09_04'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_Kapptot_exp30_Dz6_H20036_2018_09_04'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_Kapptot_exp30_Dz6_H1H2036_2018_09_06'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_Kapptot_gauss30_Dz6_H20036_2018_09_04'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_Kapptot_exp30_Dz6_H1H2036_6yrs_2018_09_07'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_2018_08_30'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_1layer_CIvar_2018_09_05'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_1layer_2018_08_30'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_domain18_2018_08_30'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H1H2036_6yrs_sandH2_2018_09_08'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H20036_CI6_2018_09_09'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H1H2036_CI6_6yrs_2018_09_10'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results__H1H2036_CI6_2018_09_08'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H20036_CI6_2018_09_11'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI6_2018_09_11'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H20036_CI6_6yrs_2018_09_12'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H20036_CI6_H3ret_2018_09_12'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H20036_CI6_H3ret_4yrs_2018_09_13'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H20036_CI6_6yrs_2018_09_13'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI6_5yrs_2018_09_13'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI4_P3_005_2018_09_15'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI4_P3_01_H4clay_tmp_2018_09_17'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI4_P3_01_2018_09_17'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_spinup_ZA_H236_CI10_P3_01_2018_09_18'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_spinup_ZA_H20036_CI10_P3_01_2018_09_18'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_spinup_40_ZA_H20036_CI10_P3_01_2018_09_18'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_spinup_10_ZA_CI10_P3_01_K1_2018_09_19'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H20036_CI4_P3_01_2018_09_18'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_H336_CI4_P3_01_2018_09_20'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI4_P3_01_H4clay_tmp_2018_09_20'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_H336_CI8_P3_01_2018_09_21'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI4_P3_01_anis_2018_09_21'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_H336_CI4_P3_01_H4clay_2018_09_21'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZAZFAlow_2018_09_26'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZA_stat_2018_09_27'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZAZFA_4yrs_2018_09_28'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZA_stat2_2018_09_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZA_2018_09_26'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZA_H30052_2018_09_26'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_H336_CI8_P3_01_anis_2018_09_26'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H2clay_2018_10_30'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H2clayV2_2018_10_31'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H2clayV3_2018_11_21'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H2imp_2018_11_22'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H2imp_deeprt_2018_11_26'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H2imp_deeprt2_2018_12_21'
simdir = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/A_Herzog_Ksat_regolith/outputs/oueme_Amelie_results_ZA_4yrs_2018_09_27'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/NCout/oueme_ADlike_2019_10_14'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/NCout/oueme_ADlike_2019_10_22'


simuname = 'oueme'
#~ simuname = 'oueme_results'
#~ simuname = 'oueme_results_1layer_CIvar'
simuname = 'oueme_results_Kapptot_exp30_CIconst'
#~ simuname = 'oueme_results_Kapptot_exp30_Dz6_H20036'
#~ simuname = 'oueme_results_Kapptot_exp30_Dz6_H1H2036'
#~ simuname = 'oueme_results_Kapptot_gauss30_Dz6_H20036'
#~ simuname = 'oueme_results_Kapptot_exp30_Dz6_H1H2036_6yrs'
#~ simuname = 'oueme_results_H1H2036_6yrs_sandH2'
#~ simuname = 'oueme_results_H20036_CI6'
#~ simuname = 'oueme_results_H1H2036_CI6_6yrs'
simuname = 'oueme_results__H1H2036_CI6'
simuname = 'oueme_results_ZA_H20036_CI6'
simuname = 'oueme_results_ZA_H236_CI6'
#~ simuname = 'oueme_results_domain18'
simuname = 'oueme_results_H20036_CI6_6yrs'
simuname = 'oueme_results_ZA_H20036_CI6_H3ret'
simuname = 'oueme_results_ZA_H20036_CI6_H3ret_4yrs'
simuname = 'oueme_results_ZA_H20036_CI6_6yrs'
simuname = 'oueme_results_ZA_H236_CI6_5yrs'
simuname = 'oueme_results_ZA_H236_CI4_P3_005'
simuname = 'oueme_results_ZA_H236_CI4_P3_01_H4clay_tmp'
#~ simuname = 'oueme_results_ZA_H236_CI4_P3_01'
simuname = 'oueme_spinup_ZA_H236_CI10_P3_01'
simuname = 'oueme_spinup_40_ZA_H20036_CI10_P3_01'
simuname = 'oueme_spinup_10_ZA_CI10_P3_01_K1'
#~ simuname = 'oueme_results_ZA_H20036_CI4_P3_01'
simuname = 'oueme_results_ZA_H236_H336_CI4_P3_01'
simuname = 'oueme_results_ZA_H236_CI4_P3_01_H4clay_tmp'
simuname = 'oueme_results_ZA_H236_H336_CI8_P3_01'
simuname = 'oueme_results_ZA_H236_CI4_P3_01_anis'
simuname = 'oueme_results_ZA_H236_H336_CI4_P3_01_H4clay'
simuname = 'oueme_Amelie_results_ZAZFAlow'
simuname = 'oueme_Amelie_results_ZA_stat'
simuname = 'oueme_Amelie_results_ZAZFA_4yrs'
simuname = 'oueme_Amelie_results_ZA_stat2'
#~ simuname = 'oueme_Amelie_results_ZA'
#~ simuname = 'oueme_Amelie_results_ZA_H30052'
#~ simuname = 'oueme_results_ZA_H236_H336_CI8_P3_01_anis'
simuname = 'oueme_results_H2clay'
simuname = 'oueme_results_H2clayV2'
simuname = 'oueme_results_H2clayV3'
simuname = 'oueme_results_H2imp'
simuname = 'oueme_results_H2imp_deeprt'
simuname = 'oueme_results_H2imp_deeprt2'
simuname = 'oueme_Amelie_results_ZA_4yrs'
simuname = 'oueme_ADlike'

n_coupled_layers = 5
n_CLM_2Dlayers = 14

statsDay =True
#~ statsDay =False

"""WT data"""
root_dir = r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/slight_modif_typo_badnumbers_2019_03/GWat_O_downloaded2019'
suf_pattern = '.csv'
pre_pattern = 'CL.GwatWell_O-'
read_spec_stations=False
if read_spec_stations:
    station_list = {'KONE_KAINA_NORD':1 ,'KONE_KAINA_PLATEAU':1}
    stationnames = station_list.keys()
else:
    filepattern = os.path.join(root_dir,'*'.join([pre_pattern,suf_pattern]))
    stationnames = np.unique([f.split('-')[1] for f in glob.glob(filepattern)])



##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def minimalist_xldate_as_datetime(xldate, datemode):
    # datemode: 0 for 1900-based, 1 for 1904-based
    return (
        datetime.datetime(1899, 12, 30)
        + datetime.timedelta(days=xldate + 1462 * datemode)
        )
        
def roundDateTime(shakyDT,nearest_minutes=1):
	"""
	"""
	shakyDT += datetime.timedelta(minutes=nearest_minutes/2)
	shakyDT -= datetime.timedelta(minutes=shakyDT.minute % nearest_minutes,
                         seconds=shakyDT.second,
                         microseconds=shakyDT.microsecond)
	return shakyDT
	

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

#### Read & process simulation outputs:
sim = PFout.PFout(simdir,'oueme')
sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
sim.X0=np.min(sim.x)
sim.Y0=np.min(sim.y)


"""Read PF outputs"""
start_time = time.time()
with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
    sim.satur_array = np.transpose(ds.variables['saturation'][:],[3,2,1,0])
    #~ topmask=np.swapaxes(ds.variables['top_mask'][:],0,1)
"""MASK is a nx x ny boolean mask of the domain
When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
(as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
"""
#~ MASK=topmask>0 
with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
    sim.press_array = np.transpose(ds.variables['succion'][:],[3,2,1,0])

""" Read constant files"""
ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
#watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,1)
sim.mannings_arr = np.zeros((sim.nx,sim.ny))+5e-6
sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,1)
sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,1)
sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,1)
#~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))
sim.read_z_from_NC(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))


"""option to get vardz from hardcoded tcl"""
"""
tcllines = io_tcl.TclLines(filename = r'/home/herzogam/parflow/outputs_final/oueme_2018_08_30/simu.tcl')
set_lines = tcllines.is_setline()
set_lines = tcllines.get_setlines()
split_lines = tcllines.to_tclsets()
for i in range(15):
    sim.dz_mult_arr[:,:,i] = (split_lines[split_lines.get_idx('Cell.%i.dzScale.Value'%i)].value)
"""



""" Read Forcings: """
#~ forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
#~ forc = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')
#~ forc.rename(columns=forcing_headers,inplace=True)
#~ forc.index=pd.date_range('1/1/2005 00:00:00', periods=len(forc), freq='30min')
#~ sim.P=sum(forc['P'])*30*60

"""Get WT data"""
proj = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')
df = pd.DataFrame()
stadic ={}
#~ for stationname,data_col in station_list.items():
for stationname in stationnames:
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(root_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    #~ sta.readWT(filepattern, data_col = data_col)
    sta.read_WT(filepattern, data_col = 1) 
    #~ df[stationname]=sta.WT
    df[stationname]=sta.WT.groupby(sta.WT.index).first()
    filenames = glob.glob(filepattern)    
    sta.read_latlonalt_from_WTfile(filenames[0],lon_pattern ='longitude', lat_pattern = 'latitude',alt_pattern ='altitude')
    sta.x, sta.y = pyproj.transform(geo_system,proj,sta.lon, sta.lat)
    print(sta)
    #~ print(sta.x)
    #~ print(sta.y)
    stadic[stationname] = sta


[xx,yy]=np.meshgrid(sim.x,sim.y)

""" Find the location of data in the PF framework, and calculate simulated Q:"""
#~ xx_masked = np.round(xx)
#~ yy_masked = np.round(yy)
#~ xx_masked[data == 0] = -9999999
#~ yy_masked[data == 0] = -9999999
#~ xx_masked[data == 255] = -9999999
#~ yy_masked[data == 255] = -9999999

WTsim = pd.DataFrame()
#~ x = [sim.X0 + sim.dx*x+sim.dx/2 for x in range(sim.nx)]
#~ y = [sim.Y0 + sim.dy*y+sim.dy/2 for y in range(sim.ny)]
#~ x = [sim.X0 + sim.dx*x for x in range(sim.nx)]
#~ y = [sim.Y0 + sim.dy*y for y in range(sim.ny)]
for stationname,sta in stadic.items():
    print(sta.name)
    #find the closest point in the grid:  
    dist = np.sqrt((np.round(xx)- sta.x)**2 + (np.round(yy) - sta.y)**2)
    ind_min = np.where(dist == dist.min())   
    sta.x_sim = np.round(xx[ind_min])
    sta.y_sim = np.round(yy[ind_min])
    xloc = np.where(np.array(sim.x) == sta.x_sim)[0][0]
    yloc = np.where(np.array(sim.y) == sta.y_sim)[0][0]
    # now get the outlet location in the PF domain:
    #~ sim.calculate_runoff_from_CLM(x_outlet = np.where(np.array(x) == sta.x_sim),y_outlet = np.where(np.array(y) == sta.y_sim),nlayers = n_coupled_layers+n_CLM_2Dlayers)
    simWTs = sim.calculate_WT(xloc = xloc, yloc = yloc)
    #~ sta.simWT = simWTs[0,:]

    #~ sta.WTsim  = sim.create_panda_series(np.reshape(simWTs[0,:],sim.n_t),start_date = "1/1/2005", start_hour = "00:00", freq = "1D",periods = sim.n_t)        
    #~ sta.WTsim[abs(sta.WTsim)>10000]=np.nan
    wts=pd.DataFrame()
    for i in range(simWTs.shape[0]):
        tmp  = sim.create_panda_series(np.reshape(simWTs[i,:],sim.n_t),start_date = "1/1/2005", start_hour = "00:00", freq = "1D",periods = sim.n_t)        
        tmp[abs(tmp)>10000]=np.nan
        wts[i] = tmp
    sta.wts=wts
    #~ sta.WTsim[abs(sta.WTsim)>10000]=np.nan
    #~ Qsim[stationname]=sta.Qsim        
    stadic[stationname] = sta


def pdinterpolate(ts, datetime_index):
    """could decide to fill nans !
    but not adapted for calculating scores?
    To be tested"""
    x = pd.concat([ts, pd.Series(index=datetime_index)])
    #~ return x.groupby(x.index).first().sort_index().fillna(method="ffill")[datetime_index]
    return x.groupby(x.index).first().sort_index().interpolate('linear')[datetime_index]
    #~ return x.groupby(x.index).first().sort_index()[datetime_index]



fig,ax =plt.subplots(nrows=int(np.ceil(len(stadic)/2)),ncols = 2,figsize=(10,20), squeeze=True)

i=0
j=0
for stationname,sta in stadic.items():

    # interpolation on sim + daily approach:
    obs = sta.WT.dropna().sort_index(axis=0)
    #~ simu = sta.WTsim.dropna().sort_index(axis=0)
    simu = sta.wts[0].dropna().sort_index(axis=0)
    #~ obs = obs.groupby(obs.index).mean()
    obs = obs.groupby(obs.index).mean().rename('obs')
    
    #~ obs_int=pd.concat([obs,simu],axis=1).interpolate(method='slinear',limit=50)[0]
    obs_int=pd.concat([obs,simu],axis=1).interpolate(method='slinear',limit=50)['obs']
    obs_intD = obs_int.resample('D').mean()
    simuD = simu.resample('D').mean()
    merge=pd.concat([obs_intD.rename('obs'),simuD.rename('sim')],axis=1)
    #~ obs_intD.name = 'obs'
    #~ simuD.name = 'sim'
    #~ merge=pd.concat([obs_intD,simuD],axis=1)
    kept = merge.dropna()
    res = obj.kge(kept['obs'].values, kept['sim'].values, return_all=True)
    print(res)  
        #~ else:
            #~ # Interpolation on the closest available obs - sim couples:
            #~ #simuD = simu.resample('D').asfreq()
            #~ #simuD = dsclm['Q'][dsclm.index.hour==12]
            #~ simu = simu[(simu.index.month>=6) & (simu.index.month<=11)]
            #~ new_dates = obs.index.round('H')
            #~ obs2=obs
            #~ obs2.index = new_dates
            #~ obs2 = obs2.groupby(obs2.index).mean()
            #~ merge=pd.concat([obs2,simu],axis=1)
            #~ #merge=pd.concat([obs2,simuD],axis=1)  
  
    
    
    if i>=int(np.ceil(len(stadic)/2)):
        j=1
        i=0
    #~ ax[i][j].plot(sta.WT.loc['2007-1-1':'2007-12-31'].dropna(),'k')
    ax[i][j].plot(sta.WT.dropna().sort_index(axis=0),'k')
    
    #~ ax[i][j].plot(sta.WTsim.loc['2007-1-1':'2007-12-31'].dropna(),'r')
    #~ ax[i][j].plot(sta.WTsim.dropna().sort_index(axis=0),'r')
    for k in range(sim.nz):
        ax[i][j].plot(sta.wts[k].dropna().sort_index(axis=0),'r')
    ax[i][j].plot(obs_intD,'b')
            
    
    #~ ax[i].plot(sta.WTsim.loc['2007-1-1':'2007-12-31'],'r')
    if j==0: ax[i][j].set_ylabel('WTD(m)')
    if i==0: ax[i][j].legend(['obs','sim'],fontsize=8,loc='upper left',ncol=2)
    ax[i,j].set_xlim([datetime.datetime(2002,1,1),datetime.datetime(2011,12,31)])
    #~ ax[i,j].set_ylim([0.2,19])
    ax[i,j].set_ylim([-0.5,9])
    ax[i,j].text(datetime.datetime(2002,3,25),18,r'%s: $r^2 = %2.2f$ KGE = %2.2f'%(stationname,res[1]*res[1],res[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})
    
    ax[i,j].tick_params(axis='x', which='both', labelbottom='off', labeltop='off')
    if j ==0: ax[i,j].tick_params(axis='y', which='both', labelright='off', labelleft='on')
    if j ==1: ax[i,j].tick_params(axis='y', which='both', labelright='on', labelleft='off')
    if j ==0: ax[i,j].tick_params(axis='both', which='major', bottom='off',top='off',right='off',left='on')
    if j ==1: ax[i,j].tick_params(axis='both', which='major', bottom='off',top='off',right='on',left='off')
    i+=1
    
for i in range(2*int(np.ceil(len(stadic)/2))):
    fig.axes[i].invert_yaxis()
    fig.subplots_adjust(bottom=0.03, top =0.98,left=0.05,right =0.96,wspace=0.04, hspace=0.001)
    
ax[0,0].tick_params(axis='both', which='major', bottom='off',top='on',right='off',left='on')
ax[0,1].tick_params(axis='both', which='major', bottom='off',top='on',right='on',left='off')
ax[int(np.ceil(len(stadic)/2))-1,0].tick_params(axis='x', which='both', labelbottom='on', labeltop='off')
ax[int(np.ceil(len(stadic)/2))-1,1].tick_params(axis='x', which='both', labelbottom='on', labeltop='off')
plt.savefig(os.sep.join([simdir,'Figure_compare_WT.png']),dpi=400,format='png')


"""so sorted by descending order! => DEMfile.read(1)[0,0] is the upper left corner!"""
with rasterio.open('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/preproc/DEM/dem_mean.tif') as DEMfile:
    dem = DEMfile.read(1)
    dem = dem[::-1,:]


icol, irow = np.meshgrid(range(sim.nx),range(sim.ny))
#~ [sim.calculate_WT(xloc = tup[0],yloc=tup[0]) for tup in zip(np.array(icol).flatten(),np.array(irow).flatten())]
amp = np.array([])
for tup in zip(np.array(icol).flatten(),np.array(irow).flatten(),np.array(dem).flatten()):
    simWTs =sim.calculate_WT(xloc = tup[0],yloc=tup[0]) 
    wts=pd.DataFrame()
    #~ print(tup)
    tmp  = sim.create_panda_series(np.reshape(simWTs[0,:],sim.n_t),start_date = "1/1/2005", start_hour = "00:00", freq = "1D",periods = sim.n_t)        
    tmp[abs(tmp)>10000]=np.nan
    amp = np.append(amp,(tmp.resample('Y').max() - tmp.resample('Y').min())[1])

import seaborn as sns
test = pd.DataFrame({'amplitude':amp,'elevation':np.array(dem).flatten()})
test['elevation range'] = pd.cut(test['elevation'],[0,250,300,350,400],labels = ['0-250','250-300','300-350','350-400'])
sns.boxplot(x="elevation range", y="amplitude",data=test,showfliers=False, showmeans=True, palette="Set1")
