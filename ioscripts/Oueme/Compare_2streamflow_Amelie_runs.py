#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    Anaylse PF outputs for Oueme: Streamflow
    V2 = added Donga
    Amelie_runs = V2 + adapted for Amelie's outputs
    COmpare = the reading of the simulation part is put in a function
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os,glob,copy
from netCDF4 import Dataset
import time
import datetime
import pyproj
from osgeo import gdal,ogr,osr
from osgeo.gdalconst import *
import rasterio
import seaborn as sns

from rasterio.mask import mask
from rasterio.plot import plotting_extent
import geopandas as gpd
from shapely.geometry import mapping
from matplotlib.dates import MonthLocator, DateFormatter
import statsmodels.api as sm

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as io
from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
from procsycz import procGeodata_Gdal


plt.close("all")

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_2020_02_18'
simuname1 = 'oueme_KWE_AD'
simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_AD_2020_03_05'
simuname1 = 'oueme_KWE_f3_AD'
#~ simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_H3015_2020_03_06'
#~ simuname1 = 'oueme_ZA24_f3_H3015'
#~ simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H30029_4yrs_2020_03_18'
#~ simuname1 = 'oueme_ZA24_f3_OF_H30029_4yrs'


simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_AD_2020_02_19'
simuname2 = 'oueme_OF_AD'
simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_AD_2020_03_05'
simuname2 = 'oueme_OF_f3_AD'
#~ simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H3015_2020_03_10'
#~ simuname2 = 'oueme_ZA24_f3_OF_H3015'
#~ simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H306_4yrs_2020_03_18'
#~ simuname2 = 'oueme_ZA24_f3_OF_H306_4yrs'


simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_AD_2020_03_05'
simuname3 = 'oueme_KWE_f3_AD'
simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_nosec_AD_2020_03_15'
simuname3 = 'oueme_KWE_f3_nosec_AD'
simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_m1_nomask_AD_2020_04_09'
simuname3 = 'oueme_KWE_f3_m1_nomask_AD'
#~ simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_H3015_2020_03_06'
#~ simuname3 = 'oueme_ZA24_f3_H3015'
#~ simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZAZFA48_f3_OF_H300023_H4023_2020_03_10'
#~ simuname3 = 'oueme_ZAZFA48_f3_OF_H300023_H4023'
#not f3:
#~ simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZAZFA48_H300023_H4023_2020_03_03'
#~ simuname3 = 'oueme_ZAZFA48_H300023_H4023'
#~ simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H3015_4yrs_2020_03_14'
#~ simuname3 = 'oueme_ZA24_f3_OF_H3015_4yrs'


#~ simdir4 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_AD_2020_03_05'
#~ simuname4 = 'oueme_OF_f3_AD'
simdir4 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_nosec_AD_2020_03_16'
simuname4 = 'oueme_OF_f3_nosec_AD'
simdir4 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_m1_nomask_AD_2020_04_08'
simuname4 = 'oueme_OF_f3_m1_nomask_AD'
#~ simdir4= r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZAZFA48_f3_OF_H300023_H4023_2020_03_10'
#~ simuname4 = 'oueme_ZAZFA48_f3_OF_H300023_H4023'
#~ simdir4= r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZAZFA48_f3_OF_H300023_H4023_4yrs_2020_03_16'
#~ simuname4 = 'oueme_ZAZFA48_f3_OF_H300023_H4023_4yrs'
#~ simdir4= r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H315_4yrs_2020_04_07'
#~ simuname4 = 'oueme_ZA24_f3_OF_H315_4yrs'
#~ simdir4= r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H3015_H2low_4yrs_2020_04_09'
#~ simuname4 = 'oueme_ZA24_f3_OF_H3015_H2low_4yrs'
#~ simdir4= r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_KWE_H3015_4yrs_2020_04_09'
#~ simuname4 = 'oueme_ZA24_f3_KWE_H3015_4yrs'


# select which year:
plot_yr = 2006
#~ plot_yr = 2005
method = 'W'
method = '5D'
#~ method = 'H'
scale = True
scale = False

statsDay =True
#~ statsDay =False

# to simplify the search for station locations
dx=1000
dy=1000


##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def minimalist_xldate_as_datetime(xldate, datemode):
    # datemode: 0 for 1900-based, 1 for 1904-based
    return (
        datetime.datetime(1899, 12, 30)
        + datetime.timedelta(days=xldate + 1462 * datemode)
        )
        
def roundDateTime(shakyDT,nearest_minutes=1):
	"""
	"""
	shakyDT += datetime.timedelta(minutes=nearest_minutes/2)
	shakyDT -= datetime.timedelta(minutes=shakyDT.minute % nearest_minutes,
                         seconds=shakyDT.second,
                         microseconds=shakyDT.microsecond)
	return shakyDT
	

def pdinterpolate(ts, datetime_index):
    """could decide to fill nans !
    but not adapted for calculating scores?
    To be tested"""
    x = pd.concat([ts, pd.Series(index=datetime_index)])
    #~ return x.groupby(x.index).first().sort_index().fillna(method="ffill")[datetime_index]
    return x.groupby(x.index).first().sort_index().interpolate('linear')[datetime_index]
    #~ return x.groupby(x.index).first().sort_index()[datetime_index]

def get_streamflow_simulation_results(simdir,simuname,stadic,slope_mode = 'OF'):
    """
    get a pd DataFrame with Q for all stations (given in stadic)
    slope mode : 'OF' = 'Overland Flow' classical way of using the slopes straight to get Q
    'KWE' = 'Kinematic Wave Equation' using the flow direction to get the slopes to get Q
    """

    #### Read & process simulation outputs:
    sim = PFout.PFout(simdir,'oueme')
    sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
    sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_h_top.nc'])]),filetype='CLM')
    sim.X0=np.min(sim.x)
    sim.Y0=np.min(sim.y)

    """Read PF outputs"""
    start_time = time.time()
    with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
        sim.satur_array = ds.variables['saturation'][:]
        #~ topmask=np.swapaxes(ds.variables['top_mask'][:],0,1)
    """MASK is a nx x ny boolean mask of the domain
    When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
    (as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
    """
    #~ MASK=topmask>0 
    with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
        sim.press_array = ds.variables['succion'][:] 

    """Read CLM outputs"""
    
    """
    CLM_var is a dictionnary where keys are local variable names as in PFCLM
    values are lists of string[filename, PFCLM variable name, long name, units]
    """
    CLM_var={'surf_press':['','h_top','Surface pressure','m']}
    """ add the filename in the dic"""        
    for k,val in CLM_var.items():
        CLM_var[k][0]=os.sep.join([simdir,''.join([simuname,'_'+val[1]+'.nc'])])
        
    for k,val in CLM_var.items():
        try:
            with Dataset(val[0], 'r') as ds:
                print('read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
                exec('sim.%s=np.swapaxes(ds.variables[\'%s\'][:],0,2)'%(k,val[1]))        
        except FileNotFoundError:
                print('CANNOT read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
    print("--- %d seconds --- read all PFCLM outputs from NCs" % (time.time() - start_time))

    """ Read constant files"""
    ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
    #watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
    sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,1)
    sim.mannings_arr = np.zeros((sim.nx,sim.ny))+1e-6
    sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,2)
    sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
    sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
    sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,1)
    sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,1)
    #~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))
    ds.close()
    #xx_masked, yy_masked and data have shapes [ny,nx] and data[0,:] is the lowest latitude (bottom of map)
    Qsim = pd.DataFrame()
    #~ x = [sim.X0 + sim.dx*x+sim.dx/2 for x in range(sim.nx)]
    #~ y = [sim.Y0 + sim.dy*y+sim.dy/2 for y in range(sim.ny)]
    x = [sim.X0 + sim.dx*x for x in range(sim.nx)]
    y = [sim.Y0 + sim.dy*y for y in range(sim.ny)]
    
    if slope_mode == 'KWE':
        outflow = PFout.calculate_runoff_maps(htop = sim.surf_press,slopex=sim.slope_x_arr,slopey=sim.slope_y_arr,mannings=sim.mannings_arr,
        nx = sim.nx,ny = sim.ny,dx = sim.dx, dy = sim.dy, nt =sim.n_t_clm,mask=sim.mask_arr,KWE='Stan',large_array=True,epsilon = 1E-7)

        #~ # calculate slopes in KWE formulation
        #~ sim.slope_x_arr_bckp = sim.slope_x_arr
        #~ sim.slope_y_arr_bckp = sim.slope_y_arr
        #~ d,h=io.PFB_read('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Oueme_merit_f3_Flow.Direction.pfb',1)
        #~ direction_arr = np.swapaxes(d,2,0) # nx,ny,nz -> nz, ny, nx as required by the following piece of code from Hoang Tran (JMC mail to gmail adress 16/03/2020)
        #~ sx = np.zeros((sim.ny,sim.nx))
        #~ sy = np.zeros((sim.ny,sim.nx))
        #~ slopex_arr = np.swapaxes(sim.slope_x_arr,1,0)
        #~ slopey_arr = np.swapaxes(sim.slope_y_arr,1,0)
        #~ for jj in range(sim.ny):
            #~ for ii in range(sim.nx):
                #~ tmp_dir = direction_arr[0,jj,ii]
                #~ if tmp_dir == 1:
                    #~ sx[jj,ii] = slopex_arr[jj,ii]
                    #~ sy[jj,ii] = slopey_arr[jj+1,ii]
                #~ elif tmp_dir == 3:
                    #~ sx[jj,ii] = slopex_arr[jj,ii]
                    #~ sy[jj,ii] = slopey_arr[jj,ii]
                #~ elif tmp_dir == 2:
                    #~ sx[jj,ii] = slopex_arr[jj,ii-1]
                    #~ sy[jj,ii] = slopey_arr[jj,ii]
                #~ elif tmp_dir == 4:
                    #~ sx[jj,ii] = slopex_arr[jj,ii]
                    #~ sy[jj,ii] = slopey_arr[jj,ii]
        #~ sim.slope_x_arr = np.swapaxes(sx,1,0)
        #~ sim.slope_y_arr = np.swapaxes(sy,1,0)
    else:
       outflow = PFout.calculate_runoff_maps(htop = sim.surf_press,slopex=sim.slope_x_arr,slopey=sim.slope_y_arr,mannings=sim.mannings_arr,
        nx = sim.nx,ny = sim.ny,dx = sim.dx, dy = sim.dy, nt =sim.n_t_clm,mask=sim.mask_arr,KWE='UP',large_array=True,epsilon = 1E-7)
     
    
    for stationname,sta in stadic.items():
        print(sta.name)
        #~ print(sta.x)
        #~ print(sta.y)
        #~ print(sta.x_sim)
        #~ print(sta.y_sim)    
        # now get the outlet location in the PF domain:
        #~ sim.calculate_runoff_from_CLM(x_outlet = np.where(np.array(x) == sta.x_sim),y_outlet = np.where(np.array(y) == sta.y_sim),nlayers = n_coupled_layers+n_CLM_2Dlayers)
        #former
        #~ sim.calculate_runoff_from_press(x_outlet = np.where(np.array(x) == sta.x_sim)[0],y_outlet = np.where(np.array(y) == sta.y_sim)[0])    
        #new
        sim.surfQ = np.swapaxes(outflow,0,1)[sta.y_sim_ind,sta.x_sim_ind,:].flatten()
        #~ print(np.where(np.array(x) == sta.x_sim))
        #~ print(np.where(np.array(y) == sta.y_sim))
        #~ print(np.shape(sim.surfQ))
        # if m3 => m3/s:
        #~ sim.surfQ = [Q/(60*60) for Q in sim.surfQ ]
        """m3/hr (TO CHECK -> seems ok)"""
        #~ sta.Qsim  = sim.create_panda_series(np.reshape(sim.surfQ,sim.n_t_clm),start_date = "1/1/2007", start_hour = "00:00", freq = "1H",periods = sim.n_t_clm)        
        sta.Qsim  = sim.create_panda_series(np.reshape(sim.surfQ,sim.n_t_clm),start_date = "1/1/2005", start_hour = "00:00", freq = "1H",periods = sim.n_t_clm)        
        sta.Qsim[abs(sta.Qsim)>10e10]=np.nan
        """m3/hr -> m3/s"""
        sta.Qsim = sta.Qsim/3600.0
        Qsim[stationname]=sta.Qsim        
        stadic[stationname] = sta
    return sim,stadic,Qsim,x,y, outflow
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

"""Get streamflow data Oueme"""
rt_dir = r'/home/hectorb/DATA/streamflow/AMMA_Benin/Donga'
rt_dir = r'/home/hectorb/DATA/streamflow/AMMA_Benin/Oueme'
#~ rt_dir = r'/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/analyse/Data/streamflow/Oueme'
suf_pattern = '.csv'
#~ pre_pattern = 'CL.Run_Od-'
pre_pattern = 'CL.Run_O-'
station_list = {'ARA_PONT':2 ,'DONGA_PONT':3,'DONGA_ROUTE_DE_KOLOKONDE':3}
station_list = {'BORI':1,'COTE_238':1,'IGBOMAKORO':1,'SANI_A_SANI':1,'SARMANGA':1,'TEBOU':1,'WEWE':1,'AFFON_PONT':1,'AGUIMO':1,'AVAL-SANI':1,'BAREROU':1,'BETEROU':1}
station_list= {k: 2 for k, v in station_list.items()}
#first col is time then daily average then hourly

proj = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')

df = pd.DataFrame()
stadic ={}
for stationname,data_column in station_list.items():
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    """m3/s"""
    sta.read_Q(filepattern, data_col = data_column)
    #~ sta.Q = sta.Q*sta.Q.index.to_series().diff().dt.total_seconds().fillna(0)
    """m3/s -> m3/hr"""
    #~ sta.Q = sta.Q*3600.
    sta.Q.dropna(inplace = True)
    #~ sta.Q[np.isnan(sta.Q)] = 500
    sta.Qamount = sta.Q*60*60
    df[stationname]=sta.Q
    print(sta.name)
    filenames = glob.glob(filepattern)    
    sta.read_latlon_from_Qfile(filenames[0])
    sta.x, sta.y = pyproj.transform(geo_system,proj,sta.lon, sta.lat)
    print(sta.lon)
    print(sta.lat)
    print(sta.x)
    print(sta.y)
    stadic[stationname] = sta

"""Get streamflow data: Donga
data is in m3/s for periods of 10mn or 15Mn depending on stations
"""
rt_dir = r'/home/hectorb/DATA/streamflow/AMMA_Benin/Donga'
suf_pattern = '.csv'
pre_pattern = 'CL.Run_Od-'
station_list = {'ARA_PONT':2 ,'DONGA_PONT':3,'DONGA_ROUTE_DE_KOLOKONDE':3}
#first col is time then instantaneous then 10mn average then 15mn average

for stationname,data_column in station_list.items():
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    """m3/s"""
    sta.read_Q(filepattern, data_col = data_column)
    sta.Q.sort_index(inplace = True)
    sta.Q.rename(stationname,inplace = True)
    #~ df3 = pd.concat([df3,sta.Q.dropna()],axis = 1)       
    #~ for yr in np.unique(sta.Q.index.year): #that's to remove years with too many gaps
        #~ if np.isnan(sta.Q.loc[sta.Q.index.year==yr]).sum()>(6*48):
        #~ if np.isnan(sta.Q.loc[(sta.Q.index.year==yr) & (sta.Q.index.month>4) & (sta.Q.index.month<=10)]).sum()>(6*48):
            #~ sta.Q.loc[sta.Q.index.year==yr] = np.nan
    sta.Q.dropna(inplace = True)
    #~ sta.Q[np.isnan(sta.Q)] = 500
    if data_column == 2: sta.Qamount = sta.Q*60*10
    else: sta.Qamount = sta.Q*60*15
    #~ dfamount_yr = pd.concat([dfamount_yr,sta.Qamount.resample('Y').sum()],axis = 1)
    #~ df = pd.concat([df,sta.Q.dropna()],axis = 1)
    #~ df = df.join(sta.Q,how='outer')
    print(sta.name)
    filenames = glob.glob(filepattern)    
    sta.read_latlon_from_Qfile(filenames[0])
    sta.x, sta.y = pyproj.transform(geo_system,proj,sta.lon, sta.lat)    
    stadic[stationname] = sta

""" Get stream network:"""
#~ with rasterio.open("/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/inputs/geometries/Donga/data/Donga_streams_segments_1000m_masked_mean_agreg_D4_pitfilled.tif") as src:
#~ with rasterio.open("/homelocal5/phyrev/_PARFLOW/projects/Donga/active/Donga_1km/input/DEM/Rwatershed/streams.tif") as src:
#~ data,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/inputs/geometries/Oueme/data/Oueme_streams_segments_1000m_masked_mean_agreg_D4_pitfilled.tif",nodata_value = 65535)
data,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/upg_merit_Benin_UTM_1km_masked_thinned_corr.tif",nodata_value = 255)
[lonlon,latlat]=np.meshgrid(londem,latdem)

""" Find the location of data in the PF framework, and calculate simulated Q:"""
#test Basile 03/2020:
lonlon=lonlon-500
latlat=latlat-500

xx_masked = np.round(lonlon)
yy_masked = np.round(latlat)
#~ xx_masked[data == 0] = -99999999
#~ yy_masked[data == 0] = -99999999
xx_masked[data[::-1,:] == 0] = -99999999
yy_masked[data[::-1,:] == 0] = -99999999
#~ xx_masked[data == 255] = -99999999
#~ yy_masked[data == 255] = -99999999
#BH mars 2020:
xx_masked[data[::-1,:] == 255] = -99999999
yy_masked[data[::-1,:] == 255] = -99999999

# Get x_sim and y_sim by matching the station location and the simulation grid
# (here the simulation grid is the same as the one for the stream network, which
#further gives the stream cells that have been topographically processed as such")
for stationname,sta in stadic.items():
    print(sta.name)
    #find the closest point in the grid:  
    dist_to_river = np.sqrt((xx_masked - sta.x)**2 + (yy_masked - sta.y)**2)
    dist = np.sqrt((np.round(lonlon)- sta.x)**2 + (np.round(latlat) - sta.y)**2)
    # BH: mars 2020:
    if (dist_to_river.min() <= 2*np.sqrt(dx*dx + dy*dy)):
    #~ if (dist_to_river.min() < np.sqrt(sim.dx*sim.dx + sim.dy*sim.dy)):
        ind_min = np.where(dist_to_river == dist_to_river.min())
        print('river cell found')
    else:
        ind_min = np.where(dist == dist.min())   
        print('no river cell found')
    # yes, here ind_min[0] gives the rows which are y dimensions, which is different to the PF or PFout world. 
    # that's why the outflow array which is nx ny nt will be axe-swapped to retrieve the correct station pixel later using y_sim_ind
    sta.y_sim_ind = ind_min[0]
    sta.x_sim_ind = ind_min[1]
    sta.x_sim = np.round(lonlon[ind_min])
    sta.y_sim = np.round(latlat[ind_min])
    stadic[stationname] = sta


#~ sim1,stadic1,Qsim1,x,y = get_streamflow_simulation_results(simdir1,simuname1,copy.deepcopy(stadic),slope_mode='KWE')
sim1,stadic1,Qsim1,x,y,outflow1 = get_streamflow_simulation_results(simdir1,simuname1,copy.deepcopy(stadic),slope_mode='KWE')
#~ sim1,stadic1,Qsim1,x,y,outflow1 = get_streamflow_simulation_results(simdir1,simuname1,copy.deepcopy(stadic),slope_mode='OF')

sim2,stadic2,Qsim2,x,y,outflow2 = get_streamflow_simulation_results(simdir2,simuname2,copy.deepcopy(stadic),slope_mode='OF')

#~ sim3,stadic3,Qsim3,x,y = get_streamflow_simulation_results(simdir3,simuname3,copy.deepcopy(stadic),slope_mode='KWE')
sim3,stadic3,Qsim3,x,y,outflow3 = get_streamflow_simulation_results(simdir3,simuname3,copy.deepcopy(stadic),slope_mode='KWE')
#~ sim3,stadic3,Qsim3,x,y,outflow3 = get_streamflow_simulation_results(simdir3,simuname3,copy.deepcopy(stadic),slope_mode='OF')
#~ sim3,stadic3,Qsim3,x,y = sim1,stadic1,Qsim1,x,y
sim4,stadic4,Qsim4,x,y,outflow4 = get_streamflow_simulation_results(simdir4,simuname4,copy.deepcopy(stadic),slope_mode='OF')
#~ sim4,stadic4,Qsim4,x,y,outflow4 = get_streamflow_simulation_results(simdir4,simuname4,copy.deepcopy(stadic),slope_mode='KWE')
#~ sim = sim1
#~ stadic = stadic1
#~ Qsim = Qsim1

"""
plot station map
#~ https://stackoverflow.com/questions/40899350/python-quiver-and-pcolormesh-not-lining-up-exactly-right     

"""
#~ fig = plt.figure(figsize=(16,8))
#~ ax = fig.add_subplot(111)
#~ ax.set_aspect(1)
#~ [xx,yy]=np.meshgrid(x,y)

""" to check the location with respect to pressure field:"""
#p = ax.pcolormesh(xx-sim.dx/2, yy-sim.dy/2, sim.press_array[240,-1,:,:], cmap="viridis", zorder=-1)
""" to check the location, use the stream mask:"""
#~ p = ax.pcolormesh(lonlon-sim.dx/2, latlat-sim.dy/2, data[::-1,:], cmap="viridis", zorder=-1)
#~ cb = plt.colorbar(p)
#~ cb.set_label('P(m)',fontsize=20)
#~ ax.set_xlabel('X utm (m)',fontsize=20)
#~ ax.set_ylabel('Y utm (m)',fontsize=20)

#~ ax.plot([sta.x for stationname,sta in stadic.items()],[sta.y for stationname,sta in stadic.items()],'k+',markersize=12)
#~ ax.plot([sta.x_sim for stationname,sta in stadic.items()],[sta.y_sim for stationname,sta in stadic.items()],'r+',markersize=12)



"""Plot maps"""
#~ fig,ax = plt.subplots(1,2,figsize = [7,7])
fig,ax = plt.subplots(2,2,figsize = [7,7])
for a in ax.flat: a.set_aspect(1)
[xx,yy]=np.meshgrid(x,y)
flow1 = np.mean(outflow1[:,:,8640::],axis=2)
flow2 = np.mean(outflow2[:,:,8640::],axis=2)
flow3 = np.mean(outflow3[:,:,8640::],axis=2)
flow4 = np.mean(outflow4[:,:,8640::],axis=2)
p1 = ax[0][0].pcolormesh(xx-dx/2, yy-dy/2, np.swapaxes(flow1,0,1), cmap="viridis",vmin=0,vmax=300000, zorder=-1)
p2 = ax[0][1].pcolormesh(xx-dx/2, yy-dy/2, np.swapaxes(flow2,0,1), cmap="viridis",vmin=0,vmax=300000, zorder=-1)
p3 = ax[1][0].pcolormesh(xx-dx/2, yy-dy/2, np.swapaxes(flow3,0,1), cmap="viridis",vmin=0,vmax=300000, zorder=-1)
p4 = ax[1][1].pcolormesh(xx-dx/2, yy-dy/2, np.swapaxes(flow4,0,1), cmap="viridis",vmin=0,vmax=300000, zorder=-1)
#~ cb = plt.colorbar(p)
#~ cb.set_label('Q(m3/h)',fontsize=20)

#control colorbar
fig.colorbar(p4, ax=ax.ravel().tolist(),label="Q (m3/h)")
#or:
#~ cbar_ax = fig.add_axes([0.15, 0.05, 0.7, 0.05])
#~ fig.colorbar(ax1, cax=cbar_ax,orientation='horizontal',label="WTD (m)")
#~ for i,a in enumerate(ax.flat): a.set_xticklabels('');a.set_yticklabels('');a.set_title('simulation %d'%i)
for i,a in enumerate(ax.flat): a.set_xticklabels('');a.set_yticklabels('')
#~ ax[0][0].set_title('KWE')
#~ ax[0][1].set_title('OF')
#~ ax[1][0].set_title('KWE_f3')
#~ ax[1][1].set_title('OF_f3')

#~ ax[0][0].set_title('KWE_f3')
#~ ax[0][1].set_title('OF_f3')
#~ ax[1][0].set_title('KWE_f3_nosec')
#~ ax[1][1].set_title('OF_f3_nosec')

ax[0][0].set_title('KWE_f3')
ax[0][1].set_title('OF_f3')
ax[1][0].set_title('KWE_f3_nomask')
ax[1][1].set_title('OF_f3_nomask')

#~ ax[0][0].set_title('Ks_0029m_h')
#~ ax[0][1].set_title('Ks_06m_h')
#~ ax[1][0].set_title('Ks_015m_h')
#~ ax[1][1].set_title('Ks_2lyr_00023_023')

#~ ax[0][0].set_title('Ks_0029m_h')
#~ ax[0][1].set_title('Ks_06m_h')
#~ ax[1][0].set_title('Ks_015m_h')
#~ ax[1][1].set_title('Ks_015m_h_KsH2_low')

#~ ax[0][0].set_title('Ks_0029m_h')
#~ ax[0][1].set_title('Ks_06m_h')
#~ ax[1][0].set_title('Ks_015m_h')
#~ ax[1][1].set_title('Ks_015m_h_KWE')


#~ ax[0][0].set_title('KWE_f3_ZA')
#~ ax[0][1].set_title('OF_f3_ZA')
#~ ax[1][0].set_title('KWE_ZAZFA')
#~ ax[1][1].set_title('OF_f3_ZAZFA')
#~ plt.gcf().subplots_adjust(bottom=0.05, top =0.95,left=0.15, hspace=0.02,wspace=0.2)

plt.savefig(os.sep.join([simdir1,'Figure_compare_Qmap_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir2,'Figure_compare_Qmap_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir3,'Figure_compare_Qmap_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
plt.savefig(os.sep.join([simdir4,'Figure_compare_Qmap_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')



"""
plot time series
"""

fig,ax =plt.subplots(nrows=int(np.ceil(len(stadic)/2)),ncols = 2,figsize=(20,10), squeeze=True)
i=0
j=0

for stationname,sta in stadic.items():
    # interpolation on sim + daily approach:
    obs = sta.Q.dropna().sort_index(axis=0)
    obs = obs.groupby(obs.index).mean()
    simu1 = stadic1[stationname].Qsim.dropna().sort_index(axis=0)
    simu2 = stadic2[stationname].Qsim.dropna().sort_index(axis=0)
    obs_int=pd.concat([obs,simu1],axis=1).interpolate(method='slinear',limit=50)[0]
    obs_intD = obs_int.resample('D').mean()
    #simu 1: 
    simuD1 = simu1.resample('D').mean()
    merge1=pd.concat([obs_intD.rename('obs'),simuD1.rename('sim')],axis=1)
    kept1 = merge1.dropna()
    res1 = obj.kge(kept1['obs'].values, kept1['sim'].values, return_all=True)
    #simu 2:
    simuD2 = simu2.resample('D').mean()
    merge2=pd.concat([obs_intD.rename('obs'),simuD2.rename('sim')],axis=1)
    kept2 = merge2.dropna()
    res2 = obj.kge(kept2['obs'].values, kept2['sim'].values, return_all=True)
    print(stationname)
    print(res1)  
    print(res2)  
        #~ else:
            #~ # Interpolation on the closest available obs - sim couples:
            #~ #simuD = simu.resample('D').asfreq()
            #~ #simuD = dsclm['Q'][dsclm.index.hour==12]
            #~ simu = simu[(simu.index.month>=6) & (simu.index.month<=11)]
            #~ new_dates = obs.index.round('H')
            #~ obs2=obs
            #~ obs2.index = new_dates
            #~ obs2 = obs2.groupby(obs2.index).mean()
            #~ merge=pd.concat([obs2,simu],axis=1)
            #~ #merge=pd.concat([obs2,simuD],axis=1)  
  
    
    
    if i>=int(np.ceil(len(stadic)/2)):
        j=1
        i=0
    sta1=stadic1[stationname]
    sta2=stadic2[stationname]
    sta3=stadic3[stationname]
    sta4=stadic4[stationname]
    # raw data:
    #~ ax[i][j].plot(sta.Q.dropna().sort_index(axis=0),'k')    
    # data interpolated daily
    #~ ax[i][j].plot(obs_intD,'k')
    # data interpolated weekly
    ax[i][j].plot(obs_intD.resample(method).mean(),'k')
    # simulation data
    #~ ax[i][j].plot(sta.Qsim.dropna().sort_index(axis=0),'r')
    ax[i][j].plot(sta1.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'r')
    ax[i][j].plot(sta2.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'b')
    ax[i][j].plot(sta3.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'r--')
    ax[i][j].plot(sta4.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'b--')

    
    #~ ax[i].plot(sta.WTsim.loc['2007-1-1':'2007-12-31'],'r')
    if j==0: ax[i][j].set_ylabel('$m^{3}/s$')
    #~ if i==0: ax[i][j].legend(['obs','sim1','sim2'],fontsize=8,loc='upper right',ncol=3)
    #~ if i==0: ax[i][j].legend(['obs','KWE','OF','KWE_f3','OF_f3'],fontsize=10,loc='upper right',ncol=5)
    #~ if i==0: ax[i][j].legend(['obs','KWE_f3','OF_f3','KWE_f3_nosec','OF_f3_nosec'],fontsize=10,loc='upper right',ncol=5)
    #~ if i==0: ax[i][j].legend(['obs','KWE_f3','OF_f3_new','KWE_f3_nosec','OF_f3_nosec_new'],fontsize=10,loc='upper right',ncol=5)
    #~ if i==0: ax[i][j].legend(['obs','KWE_f3','OF_f3','KWE_f3_nomask','OF_f3_nomask'],fontsize=10,loc='upper right',ncol=5)
    #~ if i==0: ax[i][j].legend(['obs','Ks_0029m_h','Ks_06m_h','Ks_015m_h','Ks_2lyr_00023_023'],fontsize=10,loc='upper right',ncol=5)
    #~ if i==0: ax[i][j].legend(['obs','Ks_0029m_h','Ks_06m_h','Ks_015m_h','Ks_15m_h'],fontsize=10,loc='upper right',ncol=5)
    #~ if i==0: ax[i][j].legend(['obs','Ks_0029m_h','Ks_06m_h','Ks_015m_h','Ks_015m_h_H2low'],fontsize=10,loc='upper right',ncol=5)
    if i==0: ax[i][j].legend(['obs','Ks_0029m_h','Ks_06m_h','Ks_015m_h','Ks_015m_h_KWE'],fontsize=10,loc='upper right',ncol=5)
    ax[i,j].set_xlim([datetime.datetime(plot_yr,4,1),datetime.datetime(plot_yr,11,1)])
    if scale:
        ax[i,j].set_ylim([0,520])
        #~ ax[i,j].set_ylim([0,300])
        #~ ax[i,j].set_ylim([0,220])
        ax[i,j].text(datetime.datetime(plot_yr,4,4),350,'%s: %s: $r^2 = %2.2f$ KGE = %2.2f\n%s: %s: $r^2 = %2.2f$ KGE = %2.2f'%(stationname,simuname1,res1[1]*res1[1],res1[0],stationname,simuname2,res2[1]*res2[1],res2[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})
        #~ ax[i,j].text(datetime.datetime(plot_yr,4,4),150,'%s: %s: $r^2 = %2.2f$ KGE = %2.2f\n%s: %s: $r^2 = %2.2f$ KGE = %2.2f'%(stationname,simuname1,res1[1]*res1[1],res1[0],stationname,simuname2,res2[1]*res2[1],res2[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})
    else:
        #~ mask = (obs_intD.resample(method).mean().index >=datetime.datetime(plot_yr,4,1)) & (obs_intD.resample(method).mean().index<datetime.datetime(plot_yr,11,1))
        #~ ax[i,j].set_ylim([0,obs_intD.resample(method).mean().loc[mask].max()+0.24*obs_intD.resample(method).mean().loc[mask].max()])
        
        ax[i,j].text(datetime.datetime(plot_yr,4,4),ax[i,j].get_ylim()[1]-0.24*ax[i,j].get_ylim()[1],'%s: %s: $r^2 = %2.2f$ KGE = %2.2f\n%s: %s: $r^2 = %2.2f$ KGE = %2.2f'%(stationname,simuname1,res1[1]*res1[1],res1[0],stationname,simuname2,res2[1]*res2[1],res2[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})
        
    
    ax[i,j].tick_params(axis='x', which='both', labelbottom=False, labeltop=False)
    if j ==0: ax[i,j].tick_params(axis='y', which='both', labelright=False, labelleft=True)
    if j ==1: ax[i,j].tick_params(axis='y', which='both', labelright=True, labelleft=False)
    if j ==0: ax[i,j].tick_params(axis='both', which='major', bottom=False,top=False,right=False,left=True)
    if j ==1: ax[i,j].tick_params(axis='both', which='major', bottom=False,top=False,right=True,left=False)
    i+=1
    
#~ for i in range(2*int(np.ceil(len(stadic)/2))):
    #~ fig.axes[i].invert_yaxis()
    #~ fig.subplots_adjust(bottom=0.03, top =0.98,left=0.1,right =0.9,wspace=0.04, hspace=0.001)
    
ax[0,0].tick_params(axis='both', which='both', bottom=False,top=True,right=False,left=True)
ax[0,1].tick_params(axis='both', which='both', bottom=False,top=True,right=True,left=False)
ax[int(np.ceil(len(stadic)/2))-1,0].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)
ax[int(np.ceil(len(stadic)/2))-1,1].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)
plt.gcf().subplots_adjust(bottom=0.05, top =0.95,left=0.2, hspace=0.001,wspace=0.001)

if scale:
    #~ plt.savefig(os.sep.join([simdir,'Figure_compare_Q_%s_'%method+np.str(int(plot_yr))+'.png']),dpi=400,format='png')
    
    #~ plt.savefig(os.sep.join([simdir1,'Figure_compare_Q_2sims_%s_%s_vs_%s_'%(method,simuname1,simuname2)+np.str(int(plot_yr))+'.png']),dpi=400,format='png')
    #~ plt.savefig(os.sep.join([simdir2,'Figure_compare_Q_2sims_%s_%s_vs_%s_'%(method,simuname1,simuname2)+np.str(int(plot_yr))+'.png']),dpi=400,format='png')
    
    plt.savefig(os.sep.join([simdir1,'Figure_compare_Q_4sims_tmp2_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'.png']),dpi=400,format='png')
    plt.savefig(os.sep.join([simdir2,'Figure_compare_Q_4sims_tmp2_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'.png']),dpi=400,format='png')
    plt.savefig(os.sep.join([simdir3,'Figure_compare_Q_4sims_tmp2_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'.png']),dpi=400,format='png')
    plt.savefig(os.sep.join([simdir4,'Figure_compare_Q_4sims_tmp2_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'.png']),dpi=400,format='png')
    
    
else:
    #~ plt.savefig(os.sep.join([simdir,'Figure_compare_Q_%s_'%method+np.str(int(plot_yr))+'_unscaled.png']),dpi=400,format='png')
    
    #~ plt.savefig(os.sep.join([simdir1,'Figure_compare_Q_2sims_%s_%s_vs_%s_'%(method,simuname1,simuname2)+np.str(int(plot_yr))+'_unscaled.png']),dpi=400,format='png')
    #~ plt.savefig(os.sep.join([simdir2,'Figure_compare_Q_2sims_%s_%s_vs_%s_'%(method,simuname1,simuname2)+np.str(int(plot_yr))+'_unscaled.png']),dpi=400,format='png')
    
    #~ plt.savefig(os.sep.join([simdir1,'Figure_compare_Q_4sims_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'_unscaled.png']),dpi=400,format='png')
    #~ plt.savefig(os.sep.join([simdir2,'Figure_compare_Q_4sims_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'_unscaled.png']),dpi=400,format='png')    
    #~ plt.savefig(os.sep.join([simdir3,'Figure_compare_Q_4sims_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'_unscaled.png']),dpi=400,format='png')
    #~ plt.savefig(os.sep.join([simdir4,'Figure_compare_Q_4sims_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'_unscaled.png']),dpi=400,format='png')
    
    plt.savefig(os.sep.join([simdir1,'Figure_compare_Q_4sims_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'_unscaled_free.png']),dpi=400,format='png')
    plt.savefig(os.sep.join([simdir2,'Figure_compare_Q_4sims_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'_unscaled_free.png']),dpi=400,format='png')    
    plt.savefig(os.sep.join([simdir3,'Figure_compare_Q_4sims_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'_unscaled_free.png']),dpi=400,format='png')
    plt.savefig(os.sep.join([simdir4,'Figure_compare_Q_4sims_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'_unscaled_free.png']),dpi=400,format='png')
    
    

Qdf = pd.DataFrame()
for stationname,sta in stadic.items():
    Qdf = pd.concat([Qdf,sta.Qsim.rename(stationname,inplace = True)],axis = 1)

#~ Qdf.to_csv(os.sep.join([simdir,'simulated_Q.csv']))


"""
Calculate runoff coefficients
"""

#~ corres_station = {'BORI':'Bori','COTE_238':'Cote_238','IGBOMAKORO':'Igbomakoro','SANI_A_SANI':'Sani','SARMANGA':'Sarmanga','TEBOU':'Tebou','WEWE':'Wewe','AFFON_PONT':'Affon_Pont','AGUIMO':'Aguimo','AVAL-SANI':'Aval_Sani','BAREROU':'Barerou','BETEROU':'Beterou','DONGA_PONT':'Donga_Pont','DONGA_ROUTE_DE_KOLOKONDE':'Donga_route_Kolokonde','ARA_PONT':'Ara'}
corres_station = {'BORI':'Bori','COTE_238':'Cote_238','IGBOMAKORO':'Igbomakoro','SANI_A_SANI':'Sani','SARMANGA':'Sarmanga','TEBOU':'Tebou','WEWE':'Wewe','AFFON_PONT':'Affon_Pont','AGUIMO':'Aguimo','AVAL_SANI':'Aval_Sani','BAREROU':'Barerou','BETEROU':'Beterou','DONGA_PONT':'Donga_Pont','DONGA_ROUTE_DE_KOLOKONDE':'Donga_route_Kolokonde','ARA_PONT':'Ara','BOKPEROU':'Bokperou','DONGA_AFFON':'Donga Affon','KOUA':'Koua','NEKETE':'Nekete','OUEME_SUPERIEUR':'Oueme_Superieur'}
rev_corres_station = {v:k for k,v in corres_station.items()}



Kr = pd.read_csv('/home/hectorb/DATA/streamflow/AMMA_Benin/Analysis/Kr_analysis.csv')
Kr.loc[:,Kr.columns[0]].replace(to_replace = 'Donga route Kolokonde', value = 'Donga_route_Kolokonde',inplace = True)
Kr.loc[:,Kr.columns[0]] = Kr.loc[:,Kr.columns[0]].apply(lambda x: rev_corres_station[x])                                                       
Kr = Kr.set_index(Kr.columns[0])
Kr = pd.concat([Kr,(1000*(Qdf.resample('Y').sum().loc[Qdf.resample('Y').sum().index.year==2006,:]*3600/(Kr.area*1000*1000)).T)],axis = 1)
Kr = Kr.rename({Kr.columns[-1]: 'Qsim'},axis = 'columns')


PrecipOu =  pd.read_csv(r'/home/hectorb/DATA/Precipitation/Benin/Oueme/PrecipOu_streamgauges.csv')
PrecipOu = PrecipOu.set_index(PrecipOu.columns[0])
PrecipOu.index = pd.to_datetime(PrecipOu.index,format='%Y-%m-%d %H:%M:%S')
PrecipOu.index.rename('Date',inplace=True)
p_yr = PrecipOu.resample('Y').sum()
p_yr = p_yr.rename(rev_corres_station,axis ='columns')
Kr = pd.concat([Kr,p_yr.loc[p_yr.index.year==2006,:].T],axis = 1)
Kr = Kr.rename({Kr.columns[-1]: 'P2006'},axis = 'columns')
Kr['Krsim'] = Kr.Qsim/Kr.P2006


tmp=Kr.loc[:,['Mean_offset','std_offset','P_mean','Trees_proportionZin20m','Trees_proportion20m','Trees_proportion','Tree_Density','BasFondsSurface','Qsim','Krsim']].dropna(how='any')
tmp=Kr.dropna(how='any')
#~ model = sm.OLS(tmp.Mean_offset,sm.add_constant(tmp.loc[:,'Trees_proportion20m'])).fit()
#~ model = sm.OLS(tmp.Mean_offset,sm.add_constant(tmp.loc[:,'Trees_proportionZin20m'])).fit()
#~ model = sm.OLS(tmp.Mean_offset,sm.add_constant(tmp.loc[:,'BasFondsSurface'])).fit()
#~ model = sm.OLS(tmp.Mean_offset,sm.add_constant(tmp.loc[:,'Trees_proportion20m'])).fit()
model = sm.OLS(tmp.Krsim,sm.add_constant(tmp.loc[:,'Kr'])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
ax.legend(['data: r2 = %2.2f pval = %f'%(model.rsquared,model.pvalues[1]),'fit']) 
#~ plt.savefig('/home/hectorb/ADMIN/PROJETS/WorkInProgress/Q_Oueme/figures/Offset_btw_PmET0onset_Qonset_vs_vegetation20m.png')
#~ plt.savefig('/home/hectorb/ADMIN/PROJETS/WorkInProgress/Q_Oueme/figures/Offset_btw_PmET0onset_Qonset_vs_bas_fonds_surf.png')
plt.savefig(os.sep.join([simdir,'Figure_compare_Krsim_obs_%s_'%np.str(int(plot_yr))+'.png']),dpi=400,format='png')





""" This part is nastier: try to compare Kr & veg:

Try read the vegetation distribution: use a random tif matching the grid to get the metadata 

"""
#~ https://stackoverflow.com/questions/48063038/calculate-raster-landscape-proportion-percentage-within-multiple-overlaping-po

shape_fn = '/home/hectorb/DATA/streamflow/AMMA_Benin/QGIS/sous_bassins_polygonized_UTM_with_stats_slope_statsveg.shp'
#~ raster_fn  = '/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/preproc/DEM/DEM_hydrosheds_3s/dem_hdshds_UTM_334500_478500_987500_1131500_min_agreg.tif'
raster_fn  = '/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/preproc/DEM/DEM_hydrosheds_3s/dem_hdshds_UTM_334500_478500_987500_1131500_min_agreg.tif'
with rasterio.open(raster_fn) as src:
    transf = src.get_transform()
    
""" Save vegetation as tif """    
vmap,hdr = io.PFB_read('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2019/veg_map.pfb',1)
drv = gdal.GetDriverByName("GTiff")
ds = drv.Create("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2019/veg_map.tif", 144,144, 1, gdal.GDT_Float32)
#~ #ds.SetGeoTransform(upper_left_x, x_resolution, x_skew, upper_left_y, y_resolution, y_skew)
ds.SetGeoTransform(transf)
srs = osr.SpatialReference()                 # Establish its coordinate encoding
srs.ImportFromEPSG(32631)                     # This one specifies WGS84 lat long UTM
ds.SetProjection(srs.ExportToWkt() )   # Exports the coordinate system
#ds.SetProjection(ds_tmp.GetProjection())
outBand=ds.GetRasterBand(1)
vmap[np.isnan(vmap)]=-9999.0
outBand.WriteArray(np.swapaxes(vmap[:,::-1,0],1,0)) #checked w Qgis
outBand.SetNoDataValue(-9999.0)
outBand.FlushCache()
ds=None

raster_fn = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2019/veg_map.tif"

write_zero_frequencies = True
show_plot = False

shapefile = gpd.read_file(shape_fn)

# extract the geometries in GeoJSON format
geoms = shapefile.geometry.values # list of shapely geometries
records = shapefile.values
with rasterio.open(raster_fn) as src:
    classes = np.unique(src.read(1))

with rasterio.open(raster_fn) as src:
    print('nodata value:', src.nodata)
    idx_area = 0
    for index, row in shapefile.iterrows():
        upslope_area = row['geometry']
        name = row['nom']
        from shapely.geometry import mapping # transform to GeJSON format
        mapped_geom = [mapping(upslope_area)]
        out_image, out_transform = mask(src, mapped_geom, crop=True)# extract the raster values values within the polygon
        no_data=src.nodata #no data values of the original raster
        data = out_image.data[0] #extract the values of the masked array
        clas = np.extract(data != no_data, data) # extract the row, columns of the valid values
        #~ frequencies, class_limits = np.histogram(clas,bins=classes,range=[range_min, range_max])
        frequencies, class_limits = np.histogram(clas,bins=classes)
        if idx_area == 0:
            veg = pd.DataFrame({str(name): frequencies})
            veg.index = class_limits[:-1]
        else:
            veg[str(name)] = frequencies
        idx_area += 1

veg_sta = {'Affon Pont':'AFFON_PONT', 'Aguimo':"AGUIMO", 'Ara':'ARA', 'Aval Sani':"AVAL_SANI", 'Barerou':"BAREROU", 'Beterou':"BETEROU",
       'Bokperou':"BOKPEROU", 'Bori':'BORI', 'Cote 238':"COTE_238", 'Donga Pont':"DONGA_PONT", 'Donga Affon':'DONGA_AFFON',
       'Donga route Kolokonde':"DONGA_ROUTE_DE_KOLOKONDE", 'Igbomakoro':'IGBOMAKORO', 'Koua':"KOUA", 'Nekete':"NEKETE", 'Sani':"SANI",
       'Sarmanga':"SARMANGA", 'Tebou':'TEBOU', 'Wewe':"WEWE", 'Oueme Superieur':'OUEME_SUPERIEUR'}
veg = veg.rename(columns = veg_sta)

veg_prop = veg.apply(lambda x: x[7.0] / x.sum()).rename('Trees_proportion_sim')
Kr = pd.concat([Kr,veg_prop],axis = 1)
tmp1=Kr.loc[:,['Krsim','Trees_proportion_sim']].dropna(how='any')
tmp2=Kr.loc[:,['Kr','Trees_proportion']].dropna(how='any')
model1 = sm.OLS(tmp1.Krsim,sm.add_constant(tmp1.loc[:,'Trees_proportion_sim'])).fit()
model2 = sm.OLS(tmp2.Kr,sm.add_constant(tmp2.loc[:,'Trees_proportion'])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model1, 1, ax=ax)
sm.graphics.plot_fit(model2, 1, ax=ax)
ax.legend(['sim: r2 = %2.2f pval = %f'%(model1.rsquared,model1.pvalues[1]),'fit']) 
ax.legend(['data: r2 = %2.2f pval = %f'%(model2.rsquared,model2.pvalues[1]),'fit']) 
#~ plt.savefig(os.sep.join([simdir,'Figure_compare_Krsim_obs_%s_'%np.str(int(plot_yr))+'.png']),dpi=400,format='png')

fig, ax = plt.subplots()
ax.scatter(tmp1.Trees_proportion_sim,tmp1.Krsim,c='r')
#~ ax.plot(tmp2.Trees_proportion,tmp2.Kr,'k')



tmp=Kr.loc[:,['Kr','area','Mean_slopes','P_mean','Trees_proportionZin20m','Trees_proportion20m','Trees_proportion','Tree_Density']].dropna(how='any')
model = sm.OLS(tmp.Kr,sm.add_constant(tmp.loc[:,['Trees_proportion20m','area','Mean_slopes']])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
ax.legend(['data: r2 = %2.2f pval = %f'%(model.rsquared,model.pvalues[1]),'fit']) 
#~ plt.savefig('/home/hectorb/ADMIN/PROJETS/WorkInProgress/Q_Oueme/figures/Offset_btw_PmET0onset_Qonset_vs_vegetation20m.png')
#~ plt.sav

