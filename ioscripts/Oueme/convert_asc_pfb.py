#-*- coding: utf-8 -*-

"""
    PFPYLIBS - MODULE

    convert .asc to pfb
    .asc should be axe swapped and upside down

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd


"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
pfb_ex = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2019/Yslopes_river_dug_acc_forced_slope.pfb"

#~ inputasc = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/crop_CONWA_DEM/Oueme_MERIT_slopex_nodat9999_UTM.asc"
#~ inputasc = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/crop_CONWA_DEM/Oueme_MERIT_slopey_nodat9999_UTM.asc"
#~ inputasc = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/crop_CONWA_DEM/Oueme_MERIT_slopey_up_nodat9999_UTM.asc"
inputasc = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/crop_CONWA_DEM/Oueme_MERIT_slopex_up_nodat9999_UTM.asc"
#~ output_pfb = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/crop_CONWA_DEM/Oueme_MERIT_slopex_nodat9999_UTM.pfb"
#~ output_pfb = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/crop_CONWA_DEM/Oueme_MERIT_slopey_nodat9999_UTM.pfb"
#~ output_pfb = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/crop_CONWA_DEM/Oueme_MERIT_slopey_up_nodat9999_UTM.pfb"
output_pfb = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/crop_CONWA_DEM/Oueme_MERIT_slopex_up_nodat9999_UTM.pfb"
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

DEM = np.loadtxt(inputasc, skiprows=6)
d,h = iopfb.PFB_read(pfb_ex,1)

DEM = np.swapaxes(DEM,0,1)
iopfb.PFB_write(np.reshape(DEM[:,::-1],[DEM.shape[0],DEM.shape[1],1]),h,output_pfb)
