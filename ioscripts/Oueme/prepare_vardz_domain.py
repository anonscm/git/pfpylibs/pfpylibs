#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    SImple script to prepare var_dz and domains.pfb for Oueme

    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import matplotlib.pyplot as plt


"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
#~ inputdir = '/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/input/'
#~ inputdir = '/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/inputs/'
inputdir = '/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/Amelie_runs/'


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

""" Prepare var dz :"""
#~ v = np.array([0.09,0.39,0.75,0.75,1.2,1.8,12.9])
#~ v = np.array([0.09,0.39,0.05,0.5,0.95,3,12.9])
#~ v = np.array([0.09,0.39,0.05,0.10,0.20,0.20,16.85])
#9l:
#~ v = np.array([0.09,0.39,0.09,0.09,0.99,0.42,5.28,5.28,5.28])
#12l:
#~ v = np.array([0.09,0.39,0.09,0.09,0.99,0.42,2.64,2.64,2.64,2.64,2.64,2.64])
#9l H2imp:
v = np.array([0.09,0.39,0.09,0.09,0.39,0.09,6.01,5.28,5.28])
# AH
v = np.array([0.09,0.39,0.09,0.09,0.39,0.09,6.01,8.425,8.425])
# AH H2 highret and thicker
v = np.array([0.09,0.39,0.09,0.09,2.39,0.09,4.01,8.425,8.425])
# AH H2 highret and thicker and split H3: means we split the upper cell of H3 in 2 to have only the upper one subjected to ET uptake
v = np.array([0.09,0.39,0.09,0.09,2.39,2.1,2.01,8.425,8.425])
# AH H1thin_H2 highret and thicker and split H3: means we split the upper cell of H3 in 2 to have only the upper one subjected to ET uptake
#~ v = np.array([0.09,0.19,0.09,0.09,2.58,2.1,2.01,8.425,8.425])     
# This is the paper vardz:
v = np.array([0.09,0.12,0.09,0.09,2.58,2.1,2.01,8.425,8.425])     
# This is the paper vardz with ZAZFA:
# ZAZFA last layer = 24m=ZFA
#~ v = np.array([0.09,0.12,0.09,0.09,2.58,2.1,2.01,16.850,24.])     
#AD-like
#~ v = np.array([0.04,0.15,0.30,0.50,1,2,6,10,10])
#AD-like with crust
#~ v = np.array([0.005,0.005,0.04,0.15,0.30,0.50,3,6,20])
#AD-like sum to 27 (nz x dz = 9 x 3)
#~ v = np.array([0.04,0.15,0.30,0.50,1,2,6,10,7])
# AH  sum to 27 (nz x dz = 9 x 3):
#~ v = np.array([0.09,0.39,0.09,0.09,0.39,0.09,6.01,8.425,11.425])
# AH v parshall compatible for H1-H2 quite close in KS and H4 (clay), H3 (hardpan) very different
#~ v = np.array([0.09,0.39,1.39,0.09,0.09,2.1,3.0,5.425,11.425])                                                                                                                                                               

vardz = v/3.0
vardz3D = np.tile(vardz,(144,144,1))

h ={'X0': 334500.0,
 'Y0': 987500.0,
 'Z0': 0.0,
 'dx': 1000.0,
 'dy': 1000.0,
 'dz': 3.0,
 'ix': 0,
 'iy': 0,
 'iz': 0,
 'ns': 1,
 'nx': 144,
 'nx2': 144,
 'ny': 144,
 'ny2': 144,
 'nz': 9,
 'nz2': 9,
 'rx': 1,
 'ry': 1,
 'rz': 1}

#~ PFlibs.io_pfb.PFB_write(vardz3D[:,:,::-1],h,inputdir+'vardz_17_88m_7l.pfb')
#~ PFlibs.io_pfb.PFB_write(vardz3D[:,:,::-1],h,inputdir+'vardz_17_88m_7l_H2clay.pfb')
#~ PFlibs.io_pfb.PFB_write(vardz3D[:,:,::-1],h,inputdir+'vardz_17_88m_7l_H2clayV2.pfb')
#~ #9l:
#~ PFlibs.io_pfb.PFB_write(vardz3D[:,:,::-1],h,inputdir+'vardz_17_88m_9l.pfb')
#12l:
#~ PFlibs.io_pfb.PFB_write(vardz3D[:,:,::-1],h,inputdir+'vardz_17_88m_12l.pfb')
#9l H2imp:
#~ PFlibs.io_pfb.PFB_write(vardz3D[:,:,::-1],h,inputdir+'vardz_17_88m_9l_H2imp.pfb')
#AD-like
#~ PFlibs.io_pfb.PFB_write(vardz3D[:,:,::-1],h,inputdir+'vardz_ADlike.pfb')
#~ PFlibs.io_pfb.PFB_write(vardz3D[:,:,::-1],h,inputdir+'vardz_ADlike_crust.pfb')
#~ PFlibs.io_pfb.PFB_write(vardz3D[:,:,::-1],h,inputdir+'vardz_ADlike27.pfb')
#~ PFlibs.io_pfb.PFB_write(vardz3D[:,:,::-1],h,inputdir+'vardz_27_9l_H3_dzcorr.pfb')
#~ PFlibs.io_pfb.PFB_write(vardz3D[:,:,::-1],h,inputdir+'vardz_24_9l_H2thick.pfb')
#~ PFlibs.io_pfb.PFB_write(vardz3D[:,:,::-1],h,inputdir+'vardz_24_9l_H2thickH3split.pfb')
#~ PFlibs.io_pfb.PFB_write(vardz3D[:,:,::-1],h,inputdir+'vardz_24_9l_H1thinH2thickH3split.pfb')
# this is the paper vardz:
#~ PFlibs.io_pfb.PFB_write(vardz3D[:,:,::-1],h,inputdir+'vardz_24_9l_H1thin2H2thickH3split.pfb')
# This is the paper vardz with ZAZFA:
#~ PFlibs.io_pfb.PFB_write(vardz3D[:,:,::-1],h,inputdir+'vardz_ZAZFA48_9l_H1thin2H2thickH3split.pfb')

#~ PFlibs.io_pfb.PFB_write(vardz3D[:,:,::-1],h,inputdir+'vardz_24_9l_H1H2H3H4_clay_H5_HP.pfb')



""" Prepare Oueme domains: """
riv,head=iopfb.PFB_read('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Oueme_merit_f3_m1_newriv.rivers.out.pfb',1)
area,head=iopfb.PFB_read('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Oueme_merit_f3_m1_newriv.area.out.pfb',1)
#to check:
#~ layer=0  
#~ filename='/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Oueme_merit_f3_m1_newriv.rivers.out.pfb'  
#~ d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,logscale=False,colorlabel='rivers',title='',savefig=False,savedir='',savename='LCC.png',width=6.7,height=3.75) 

ind1,ind2,ind3=np.asarray(riv==1).nonzero()
# for Oueme_merit_f3_m1_newriv.area.out.pfb : np.sum(area>72) = 1445 et np.sum(area>0)=14414 => 72=threshold for 10% rivercells
area_threshold = 72
area_threshold = 8
#~ area_threshold = 2
ind1,ind2,ind3=np.asarray(area>area_threshold).nonzero()

#~ d = np.array([1,1,2,2,3,3,3])
#~ d = np.array([1,1,2,4,3,3,3])
#~ d = np.array([1,1,1,2,2,2,3])
#9l:
#~ d = np.array([1,1,1,2,2,2,3,3,3])
#12l:
#~ d = np.array([1,1,1,2,2,2,3,3,3,3,3,3])
#AD-like with crust
#~ d = np.array([1,1,1,1,1,2,2,3,3])
#~ d3D = np.tile(d,(144,144,1))


#9l H2imp:
#~ d = np.array([1,1,1,2,2,2,3,3,3])
#~ d3D = np.tile(d,(144,144,1))
#~ d3D[ind1,ind2,3:6]=4

#9l H2imp H3 split:
#~ d = np.array([1,1,1,2,2,3,3,3,3])
#~ d3D = np.tile(d,(144,144,1))
#~ d3D[ind1,ind2,3:5]=4

# paper domains:
# matching: 'vardz_24_9l_H1thin2H2thickH3split.pfb'
#~ d = np.array([1,1,1,2,2,3,3,3,3])
#~ d3D = np.tile(d,(144,144,1))
# matching: 'vardz_ZAZFA48_9l_H1thin2H2thickH3split.pfb'
d = np.array([1,1,1,2,2,3,3,3,5])
d3D = np.tile(d,(144,144,1))
d3D[ind1,ind2,3:5]=4

#vardz_24_9l_H1H2H3H4_clay_H5_HP
#~ d = np.array([1,1,2,2,3,3,3,3,3])
#~ d3D = np.tile(d,(144,144,1))
#~ d3D[ind1,ind2,2:4]=5 #hardpan
#~ d3D[ind1,ind2,4:6]=4 #clay

#### if mask out some cells using a river mask:

#~ PFlibs.io_pfb.PFB_write(d3D[:,:,::-1],h,inputdir+'oueme_domains_7l.pfb')
#~ PFlibs.io_pfb.PFB_write(d3D[:,:,::-1],h,inputdir+'oueme_domains_7l_H4.pfb')
#~ PFlibs.io_pfb.PFB_write(d3D[:,:,::-1],h,inputdir+'oueme_domains_7l_H2clay.pfb')
#~ PFlibs.io_pfb.PFB_write(d3D[:,:,::-1],h,inputdir+'oueme_domains_7l_H2clayV2.pfb')
#9l :
#~ PFlibs.io_pfb.PFB_write(d3D[:,:,::-1],h,inputdir+'oueme_domains_9l.pfb')
#12l :
#~ PFlibs.io_pfb.PFB_write(d3D[:,:,::-1],h,inputdir+'oueme_domains_12l.pfb')
#AD-like with crust
#~ PFlibs.io_pfb.PFB_write(d3D[:,:,::-1],h,inputdir+'oueme_domains_9l_crust.pfb')
#9l H2imp:
#~ PFlibs.io_pfb.PFB_write(d3D[:,:,::-1],h,inputdir+'oueme_domains_9l_H2imp.pfb')
# matching: 'vardz_24_9l_H1thin2H2thickH3split.pfb'
#~ PFlibs.io_pfb.PFB_write(d3D[:,:,::-1],h,inputdir+'oueme_domains_9l_H3split.pfb')
# matching: 'vardz_ZAZFA48_9l_H1thin2H2thickH3split.pfb'
#~ PFlibs.io_pfb.PFB_write(d3D[:,:,::-1],h,inputdir+'oueme_domains_9l_H3split_ZAZFA48.pfb')
# matching: 'vardz_ZAZFA48_9l_H1thin2H2thickH3split.pfb' with rivmask
PFlibs.io_pfb.PFB_write(d3D[:,:,::-1],h,inputdir+'oueme_domains_9l_H3split_ZAZFA48_rivmaskarea%d.pfb'%area_threshold)

#rivmask:
#~ PFlibs.io_pfb.PFB_write(d3D[:,:,::-1],h,inputdir+'oueme_domains_9l_rivmask.pfb')
#rivmaskarea:
#~ PFlibs.io_pfb.PFB_write(d3D[:,:,::-1],h,inputdir+'oueme_domains_9l_rivmaskarea%d.pfb'%area_threshold)
#H3 split:
#~ PFlibs.io_pfb.PFB_write(d3D[:,:,::-1],h,inputdir+'oueme_domains_9l_H3split_rivmaskarea%d.pfb'%area_threshold)
#~ PFlibs.io_pfb.PFB_write(d3D[:,:,::-1],h,inputdir+'oueme_domains_9l_H1H2H3H4_clay_H5_HP_rivmaskarea%d.pfb'%area_threshold)


#check:
filename=inputdir+'oueme_domains_9l_H3split.pfb'
#~ filename=inputdir+'oueme_domains_9l_rivmask.pfb'
#~ filename=inputdir+'oueme_domains_9l_rivmaskarea%d.pfb'%area_threshold
#~ filename=inputdir+'oueme_domains_9l_H1H2H3H4_clay_H5_HP_rivmaskarea%d.pfb'%area_threshold
for layer in range(9): 
    print(layer) 
    #~ d,xx,yy,fig,ax,cb=iopfb.plot_2D_PFB(filename,zlayer=layer,colorlabel='class',title='Z = %d'%layer) 
   
""" Prepare dgridz: """
dem,head=iopfb.PFB_read('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Oueme_merit_f3_m1_newriv_DEM.pfb',1)
v3D = -np.tile(np.cumsum(v)-v/2,(144,144,1))
# target is DEM - vardz x dz: dgridZ should have the same rationale as other 3Dpfb (as, domains, porosity, etc)
# so first index [0] is the surface.
# because when read in PFBtoNC it will follow the same transformation and first index will become the bottom of domain
final_v = v3D +dem
#initial v: 9 layers 3m (in the same rationale as other 3D PFB): top = first index = 8, bottom = 0
initial_V = np.tile(np.arange(8,-1,-1),(144,144,1))
dgridZ = final_v - initial_V
#~ PFlibs.io_pfb.PFB_write(dgridZ[:,:,::-1],h,inputdir+'dgridZ_9l_H3split_ZAZFA48.pfb')
#~ PFlibs.io_pfb.PFB_write(dgridZ[:,:,::-1],h,inputdir+'dgridZ_9l_H3split_vtmp.pfb')


""" Prepare Ks: """
#~ K,h2 = PFlibs.io_pfb.PFB_read(inputdir+'KappZA_gauss30_Dz6_178_H236.pfb',1)

K = np.dstack([K,K[:,:,-1]])
K[:,:,2:4] = 0.36
#~ K[:,:,2:4] = 0.0036
K[:,:,3]=0.00036
#~ PFlibs.io_pfb.PFB_write(K[:,:,::-1],h,inputdir+'KappZA_gauss30_Dz7_1788_H236.pfb')
#~ PFlibs.io_pfb.PFB_write(K[:,:,::-1],h,inputdir+'KappZA_gauss30_Dz7_1788_H236_H400036.pfb')
#~ PFlibs.io_pfb.PFB_write(K[:,:,::-1],h,inputdir+'KappZA_gauss30_Dz7_1788_H20036.pfb')


