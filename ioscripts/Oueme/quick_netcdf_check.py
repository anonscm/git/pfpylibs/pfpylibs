#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Quick analysis of a simulation outputs

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import pandas as pd
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import datetime
import glob,os

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/plot_scale_1D/NCout/ref_2020_01_22'
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_2020_02_18'

#~ simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_2018_03_13'
simuname1 = 'ref'
simuname1 = 'oueme_KWE_AD'

#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/plot_scale_1D/NCout/ref_2020_01_22'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/plot_scale_1D/NCout/sak_2020_01_22'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_AD_2020_02_19'

#~ simuname2 = 'ref'
simuname2 = 'sak'
simuname2 = 'oueme_OF_AD'

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##


""" Read Forcings: """
forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
forc = pd.read_csv(glob.glob(os.sep.join([simdir1,'*forc*txt*']))[0],header=None,sep='\t')
forc.rename(columns=forcing_headers,inplace=True)
forc.index=pd.date_range('1/1/2005 00:00:00', periods=len(forc), freq='30min')
#~ sim.P=sum(forc['P'])*30*60

""" Read in sim outputs using xarray"""
ds1 = xr.open_dataset(os.sep.join([simdir1,''.join([simuname1,'_qflx_evap_tot.nc'])]),decode_times=False)
tsim = pd.date_range('1/1/2005 00:00:00', periods=len(ds1.time), freq='H')
# for saturation & pression
#~ tsim_full = np.insert(tsim,0,tsim[0]- (tsim[1]-tsim[0])) #for satur and press
#~ ds = ds.assign_coords(time = pd.to_datetime(tsim_full)) #for satur and press
# for CLM outputs:
ds1 = ds1.assign_coords(time = pd.to_datetime(tsim))

ds2 = xr.open_dataset(os.sep.join([simdir2,''.join([simuname2,'_qflx_evap_tot.nc'])]),decode_times=False)
tsim2 = pd.date_range('1/1/2005 00:00:00', periods=len(ds2.time), freq='H')
ds2 = ds2.assign_coords(time = pd.to_datetime(tsim2))


#~ ds3 = xr.open_dataset(os.sep.join([simdir1,''.join([simuname1,'_qflx_evap_soi.nc'])]),decode_times=False)
#~ ds3 = ds3.assign_coords(time = pd.to_datetime(tsim))

#~ ds4 = xr.open_dataset(os.sep.join([simdir1,''.join([simuname1,'_qflx_evap_veg.nc'])]),decode_times=False)
#~ ds4 = ds4.assign_coords(time = pd.to_datetime(tsim))

#~ ds5 = xr.open_dataset(os.sep.join([simdir1,''.join([simuname1,'_qflx_tran_veg.nc'])]),decode_times=False)
#~ ds5 = ds5.assign_coords(time = pd.to_datetime(tsim))

#~ ds6 = xr.open_dataset(os.sep.join([simdir2,''.join([simuname2,'_qflx_evap_soi.nc'])]),decode_times=False)
#~ ds6 = ds6.assign_coords(time = pd.to_datetime(tsim2))

#~ ds7 = xr.open_dataset(os.sep.join([simdir2,''.join([simuname2,'_qflx_evap_veg.nc'])]),decode_times=False)
#~ ds7 = ds7.assign_coords(time = pd.to_datetime(tsim2))

#~ ds8 = xr.open_dataset(os.sep.join([simdir2,''.join([simuname2,'_qflx_tran_veg.nc'])]),decode_times=False)
#~ ds8 = ds8.assign_coords(time = pd.to_datetime(tsim2))


#~ ds.groupby('time.month').sum().sel(longitude=20,latitude=20)
fig,ax = plt.subplots(1,1,figsize = [12,7])
longitude = 0
latitude = 0
#~ (ds1.resample(time='1D').sum().sel(longitude=longitude,latitude=latitude).qflx_evap_tot*3600).plot.line('b',ax=ax)
#~ (ds2.resample(time='1D').sum().sel(longitude=longitude,latitude=latitude).qflx_evap_tot*3600).plot.line('b--',ax=ax)
(ds1.resample(time='1D').sum().mean(dim='longitude').mean(dim='latitude').qflx_evap_tot*3600).plot.line('b',ax=ax)
(ds2.resample(time='1D').sum().mean(dim='longitude').mean(dim='latitude').qflx_evap_tot*3600).plot.line('r',ax=ax)
#~ (ds3.resample(time='1D').sum().sel(longitude=longitude,latitude=latitude).qflx_evap_soi*3600).plot.line('r',ax=ax)
#~ (ds4.resample(time='1D').sum().sel(longitude=longitude,latitude=latitude).qflx_evap_veg*3600).plot.line('g',ax=ax)
#~ (ds5.resample(time='1D').sum().sel(longitude=longitude,latitude=latitude).qflx_tran_veg*3600).plot.line('c',ax=ax)
#~ (ds6.resample(time='1D').sum().sel(longitude=longitude,latitude=latitude).qflx_evap_soi*3600).plot.line('r--',ax=ax)
#~ (ds7.resample(time='1D').sum().sel(longitude=longitude,latitude=latitude).qflx_evap_veg*3600).plot.line('g--',ax=ax)
#~ (ds8.resample(time='1D').sum().sel(longitude=longitude,latitude=latitude).qflx_tran_veg*3600).plot.line('c--',ax=ax)
plt.legend(['reference','new_sim'])
#~ plt.legend(['reference','new_sim','ref_evap_soi','ref_evap_veg','ref_tran_veg','new_evap_soi','new_evap_veg','new_tran_veg'])
#~ plt.legend(['reference','new_sim','ref_evap_soi','ref_evap_veg','new_evap_soi','new_evap_veg'])
ax.set_ylabel('mm/d')
ax.set_ylim([-2,10])
#~ ax.set_ylim([0,12])







#example:
#~ fig,ax = plt.subplots(1,1,figsize = [7,7])
#~ ds.succion.mean(dim='time').mean(dim='depth').plot(ax=ax)
#~ (ds.succion[:,0,:,:].mean(dim='time')).plot(ax=ax,robust=True)
#~ (offset_for_WTD-ds.succion[:,0,:,:].mean(dim='time')).plot(ax=ax,cmap = 'viridis_r',robust=True, cbar_kwargs={'label':'WTD (m)'})
#~ for station in WTloc.Y.index:
    #~ ax.plot(WTloc.loc[station,'X'],WTloc.loc[station,'Y'],'+','r')
    #~ ax.text(WTloc.loc[station,'X'],WTloc.loc[station,'Y'],station,color ='r')
#~ ax.set_title("")
#~ ax.set_aspect('equal')


#~ filename = r'/home/hectorb/DATA/METEO/AO/ERA5/ERA5_AO.nc'
#~ xds = xarray.open_dataset(filename)
#~ xds_rolled = xds.assign_coords(longitude=(((xds.longitude + 180)%360)-180)).roll(longitude=(xds.dims['longitude'] // 2))
#~ tmp=xds_rolled.groupby('time.year').sum().mean(dim='year')                                                                                                      
#~ tmp.e_0001.sel(longitude = slice(-20,12),latitude = slice(20,4)).plot()           
#~ tmp.tp_0001.sel(longitude = slice(-20,12),latitude = slice(20,4)).plot()           

#~ (tmp.tp_0001.sel(longitude = slice(-20,12),latitude = slice(20,4))*30).plot()           
#~ ((tmp.tp_0001.sel(longitude = slice(-20,12),latitude = slice(20,4))*30) + (tmp.e_0001.sel(longitude = slice(-20,12),latitude = slice(20,4))*30))           

#~ (xds_rolled.tp_0001.sel(longitude = 3, latitude = 10).groupby('time.year').sum()*30).plot() 

#~ (tmp.tp_0001.sel(longitude = slice(-20,12),latitude = slice(20,4))*30).plot()           

#~ DB['P_ERA5'] = DB.apply(lambda x: float(tmp.tp_0001.interp(longitude = x.lon, latitude = x.lat).data*30) if (~np.isnan(x.lon)) &  (~np.isnan(x.lat)) else np.nan ,axis = 1)
#~ DB['PmET_ERA5'] = DB.apply(lambda x: float(tmp.tp_0001.interp(longitude = x.lon, latitude = x.lat).data*30 + tmp.e_0001.interp(longitude = x.lon, latitude = x.lat).data*30) if (~np.isnan(x.lon)) &  (~np.isnan(x.lat)) else np.nan ,axis = 1)


