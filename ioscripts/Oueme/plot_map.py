#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    plot station maps

    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os,glob
from netCDF4 import Dataset
import time
import datetime
import pyproj
import rasterio
from matplotlib.dates import MonthLocator, DateFormatter
"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
from procsycz import procGeodata_Gdal

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI6_5yrs_2018_09_13'
simuname = 'oueme_results_ZA_H236_CI6_5yrs'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H2imp_2018_11_22'
simuname = 'oueme_results_H2imp'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZA_2018_09_26'
simuname = 'oueme_Amelie_results_ZA'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_2020_02_18'
simuname = r'oueme_KWE_AD'
""" projection system """
proj = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')

"""Get streamflow data"""
rt_dir = r'/home/hectorb/DATA/streamflow/AMMA_Benin/Donga'
rt_dir = r'/home/hectorb/DATA/streamflow/AMMA_Benin/Oueme'
#~ rt_dir = r'/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/analyse/Data/streamflow/Oueme'
suf_pattern = '.csv'
#~ pre_pattern = 'CL.Run_Od-'
pre_pattern = 'CL.Run_O-'
station_list = {'ARA_PONT':2 ,'DONGA_PONT':3,'DONGA_ROUTE_DE_KOLOKONDE':3}
station_list = {'BORI':1,'COTE_238':1,'IGBOMAKORO':1,'SANI_A_SANI':1,'SARMANGA':1,'TEBOU':1,'WEWE':1,'AFFON_PONT':1,'AGUIMO':1,'AVAL-SANI':1,'BAREROU':1,'BETEROU':1}
station_list= {k: 2 for k, v in station_list.items()}
#first col is time then daily average then hourly

"""WT data"""
#~ root_dir_WT = r'/home/hectorb/DATA/WT/Oueme/'
root_dir_WT = r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/slight_modif_typo_badnumbers_2019_03/GWat_O_downloaded2019/'
suf_pattern_WT = '.csv'
pre_pattern_WT = 'CL.GwatWell_O-'
read_spec_stations=False
if read_spec_stations:
    station_list = {'KONE_KAINA_NORD':1 ,'KONE_KAINA_PLATEAU':1}
    stationnames = station_list.keys()
else:
    filepattern_WT = os.path.join(root_dir_WT,'*'.join([pre_pattern_WT,suf_pattern_WT]))
    stationnames_WT = np.unique([f.split('-')[1] for f in glob.glob(filepattern_WT)])


"""ET flux tower locations"""
x_ET_nalohou = 1.6
y_ET_nalohou = 9.74
x_ET_Bellefoungou = 1.718
y_ET_Bellefoungou = 9.791
x_ET_nalohou_UTM, y_ET_nalohou_UTM = pyproj.transform(geo_system,proj,x_ET_nalohou, y_ET_nalohou)
x_ET_Bellefoungou_UTM, y_ET_Bellefoungou_UTM = pyproj.transform(geo_system,proj,x_ET_Bellefoungou, y_ET_Bellefoungou)
x_ET_UTM=[x_ET_nalohou_UTM,x_ET_Bellefoungou_UTM]
y_ET_UTM=[y_ET_nalohou_UTM,y_ET_Bellefoungou_UTM]


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##


#### Read & process simulation outputs:
sim = PFout.PFout(simdir,'oueme')
sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_h_top.nc'])]),filetype='CLM')
sim.X0=np.min(sim.x)
sim.Y0=np.min(sim.y)


"""Read PF outputs"""
start_time = time.time()
with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
    sim.satur_array = ds.variables['saturation'][:]
    vegmap=np.swapaxes(ds.variables['veg_map'][:],0,1)
    #~ topmask=np.swapaxes(ds.variables['top_mask'][:],0,1)
    topmask = np.swapaxes(ds.variables['mask'][0,:,:],0,1) 
"""MASK is a nx x ny boolean mask of the domain
When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
(as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
"""

""" Read constant files"""
ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
#watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
#~ sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,1)
#~ sim.mask_arr = ds.variables['mask'][:]
sim.mask_array = np.transpose(ds.variables['mask'][:],[2,1,0])

"""
In the following, get location of observed Q, and find the matching domain cell
"""

stadic ={}
for stationname,data_column in station_list.items():
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    filenames = glob.glob(filepattern)    
    sta.read_latlon_from_Qfile(filenames[0])
    sta.x, sta.y = pyproj.transform(geo_system,proj,sta.lon, sta.lat)
    stadic[stationname] = sta
""" Get stream network:"""
#~ with rasterio.open("/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/inputs/geometries/Donga/data/Donga_streams_segments_1000m_masked_mean_agreg_D4_pitfilled.tif") as src:
#~ with rasterio.open("/homelocal5/phyrev/_PARFLOW/projects/Donga/active/Donga_1km/input/DEM/Rwatershed/streams.tif") as src:
#~ data,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/inputs/geometries/Oueme/data/Oueme_streams_segments_1000m_masked_mean_agreg_D4_pitfilled.tif",nodata_value = 65535)
data,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/upg_merit_Benin_UTM_1km_masked_thinned_corr.tif",nodata_value = 255)
data[data==255]=0
[lonlon,latlat]=np.meshgrid(londem,latdem)



""" Find the location of data in the PF framework, and calculate simulated Q:"""
xx_masked = np.round(lonlon)
yy_masked = np.round(latlat)
xx_masked[data[::-1,:] == 0] = -99999999
yy_masked[data[::-1,:] == 0] = -99999999
xx_masked[data == 255] = -99999999
yy_masked[data == 255] = -99999999
#xx_masked, yy_masked and data have shapes [ny,nx] and data[0,:] is the lowest latitude (bottom of map)
x = [sim.X0 + sim.dx*x for x in range(sim.nx)]
y = [sim.Y0 + sim.dy*y for y in range(sim.ny)]
for stationname,sta in stadic.items():
    print(sta.name)
    #find the closest point in the grid:  
    dist_to_river = np.sqrt((xx_masked - sta.x)**2 + (yy_masked - sta.y)**2)
    dist = np.sqrt((np.round(lonlon)- sta.x)**2 + (np.round(latlat) - sta.y)**2)
    #~ if (dist_to_river.min() <= 2*np.sqrt(sim.dx*sim.dx + sim.dy*sim.dy)):
    if (dist_to_river.min() < np.sqrt(sim.dx*sim.dx + sim.dy*sim.dy)):
        ind_min = np.where(dist_to_river == dist_to_river.min())
        print('river cell found')
    else:
        ind_min = np.where(dist == dist.min())   
        print('no river cell found')
    sta.x_sim = np.round(lonlon[ind_min])
    sta.y_sim = np.round(latlat[ind_min])
    stadic[stationname] = sta


"""
In the following, get location of observed WT
"""

stadic_WT ={}
#~ for stationname,data_col in station_list.items():
for stationname in stationnames_WT:
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(root_dir_WT,'*'.join([''.join([pre_pattern_WT,stationname]),suf_pattern_WT]))
    filenames = glob.glob(filepattern)    
    sta.read_latlonalt_from_WTfile(filenames[0],lon_pattern ='longitude', lat_pattern = 'latitude',alt_pattern ='altitude')
    sta.x, sta.y = pyproj.transform(geo_system,proj,sta.lon, sta.lat)
    print(sta)
    stadic_WT[stationname] = sta

"""
plot station map
#~ https://stackoverflow.com/questions/40899350/python-quiver-and-pcolormesh-not-lining-up-exactly-right     
"""
fig = plt.figure(figsize=(16,8))
ax = fig.add_subplot(111)
ax.set_aspect(1)
[xx,yy]=np.meshgrid(x,y)
data[data!=0] =1
vegmap[vegmap==7]=1
vegmap[vegmap==8]=2
vegmap[vegmap==19]=3
topmask[topmask<=0]=np.nan
topmask[topmask>0]=1

vegmasked = np.transpose(vegmap*topmask,[1,0])
#~ p2 = ax.pcolormesh(xx, yy, vegmap[::-1,::-1], cmap=plt.cm.get_cmap("YlGn_r",3), alpha=0.7)
p2 = ax.pcolormesh(xx-sim.dx/2, yy-sim.dy/2, vegmasked, cmap=plt.cm.get_cmap("YlGn_r",3), alpha=0.7)
""" to check the location, use the stream mask:"""
#uncomment to plot streams location
p = ax.pcolormesh(lonlon-sim.dx/2, latlat-sim.dy/2, np.ma.masked_equal(data[::-1,:],0), cmap="binary_r",alpha=0.5)

cb = plt.colorbar(p2,ticks=[1,2,3])
#~ cb.set_label('vegetation class',fontsize=20)
cb.ax.set_yticklabels(['Trees', 'Open Savanna', 'Herbaceous'],fontsize = 14)
ax.set_xlabel('X utm (m)',fontsize=20)
ax.set_ylabel('Y utm (m)',fontsize=20)
ax.set_ylim([np.min(latlat)-10000,np.max(latlat)+5000])
#~ ax.plot([sta.x for stationname,sta in stadic.items()],[sta.y for stationname,sta in stadic.items()],'k+',markersize=18,markeredgewidth=4)
l1, = ax.plot([sta.x_sim for stationname,sta in stadic.items()],[sta.y_sim for stationname,sta in stadic.items()],'r+',markersize=18,markeredgewidth=4)
#uncomment to plot wells location
#~ l2, = ax.plot([sta.x for stationname,sta in stadic_WT.items()],[sta.y for stationname,sta in stadic_WT.items()],'b+',markersize=18,markeredgewidth=4)

"""station names for Q stations:"""
props = dict(boxstyle='round', facecolor='white', alpha=0.5)
for stationname,sta in stadic.items():
    if stationname=='AFFON_PONT':
        #uncomment for full plot
        #~ ax.text(sta.x_sim, sta.y_sim+3000,stationname,fontsize = 12, bbox=props)
        #~ ax.text(sta.x_sim, sta.y_sim+3000,stationname,fontsize = 16, bbox=props)
        #uncomment for plot with Q only:
        ax.text(sta.x_sim-2000, sta.y_sim+2000,stationname,fontsize = 16, bbox=props,horizontalalignment='right')
    #comment for full plot:
    elif stationname=='AVAL-SANI':
        ax.text(sta.x_sim-2000, sta.y_sim-2000,stationname,fontsize = 16, bbox=props,horizontalalignment='right',verticalalignment='top')
    #comment for full plot:
    elif stationname=='BORI':
        ax.text(sta.x_sim+2000, sta.y_sim-2000,stationname,fontsize = 16, bbox=props,verticalalignment='top')
    #comment for full plot:
    elif stationname=='IGBOMAKORO':
        ax.text(sta.x_sim-2000, sta.y_sim-2000,stationname,fontsize = 16, bbox=props,horizontalalignment='right',verticalalignment='top')
    #comment for full plot:
    elif stationname=='COTE_238':
        ax.text(sta.x_sim+2000, sta.y_sim-2000,stationname,fontsize = 16, bbox=props,verticalalignment='top')
    else:
        #~ ax.text(sta.x_sim, sta.y_sim,stationname,fontsize = 12, bbox=props)
        ax.text(sta.x_sim+2000, sta.y_sim+2000,stationname,fontsize = 16, bbox=props)
"""station names for WT stations:"""
#~ props = dict(boxstyle='round', facecolor='white', alpha=0.5)
#~ for stationname,sta in stadic_WT.items():
    #~ ax.text(sta.x, sta.y,stationname,fontsize = 6, bbox=props)

#uncomment for full plot
#~ l3, = ax.plot(x_ET_UTM,y_ET_UTM,'y+',markersize=18,markeredgewidth=4,zorder = 10000)

#uncomment to add wells legend
#~ ax.legend((l1,l2,l3),('Streamgauges','Wells','Flux towers'),fontsize=14,loc='lower left',ncol=3)
#~ ax.legend((l1,l3),('Streamgauges','Flux towers'),fontsize=14,loc='lower left',ncol=3)
ax.legend(['Streamgauges'],fontsize=14,loc='lower left')


#~ plt.savefig('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/Oueme_simulation_map.png',dpi=500,format='png')
#~ plt.savefig('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/Oueme_simulation_map_wells.png',dpi=500,format='png')
#~ plt.savefig('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/outputs/Oueme_simulation_map_wells.png',dpi=500,format='png')
plt.savefig('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/outputs/Oueme_simulation_map_no_wells.png',dpi=500,format='png')

