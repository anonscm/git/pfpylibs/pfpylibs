#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Quick analysis of WT outputs
    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import pandas as pd
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import datetime
import warnings
import glob,os,copy
import matplotlib.patches as mpatches
import matplotlib as mpl
from collections import OrderedDict

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb
from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
from procsycz import procGeodata_Gdal


plt.close('all')
warnings.filterwarnings(action='once')
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

rt_dir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/'
plot_yr = 2006
dz = 3

""" SIMU 1"""
props1 = {}; props1['rt_dir'] = rt_dir

simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/plot_scale_1D/NCout/ref_2020_01_22'
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_2020_02_18'
simuname1 = 'oueme_KWE_AD'
#~ simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_AD_2020_03_05'
#~ simuname1 = 'oueme_KWE_f3_AD'
#~ simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_2020_03_03'
#~ simuname1 = 'oueme_ZA24_H3015'
#~ simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_H3015_2020_03_06'
#~ simuname1 = 'oueme_ZA24_f3_H3015'
#~ simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H30029_4yrs_2020_03_18'
#~ simuname1 = 'oueme_ZA24_f3_OF_H30029_4yrs'
#~ simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_m1_nomask_AD_4yrs_2020_04_10'
#~ simuname1 = 'oueme_KWE_f3_m1_nomask_AD_4yrs'
#~ simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_AD_2020_03_05'
#~ simuname1 = 'oueme_KWE_f3_AD'

#FARO
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_m1_nomask_AD_4yrs_2020_04_10'
#~ simuname1 = 'oueme_KWE_f3_m1_nomask_AD_4yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_H1lowK_rt85_4yrs_2020_04_20'
#~ simuname1 = r'oueme_KWE_AD_H1lowK_rt85_4yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_H1lowK_rt10_4yrs_2020_04_25'
#~ simuname1 = r'oueme_KWE_AD_H1lowK_rt10_4yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_2020_04_25'
#~ simuname1 = r'oueme_AD_rt10_nvdz_nclm_rtaherb'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H1lowK_rt10_nvdz_nclm_rtaherb_man3_2020_04_26'
#~ simuname1 = r'oueme_AD_H1lowK_rt10_nvdz_nclm_rtaherb_man3'
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_man3_2020_04_27'
simuname1 = r'oueme_AD_rt10_nvdz_nclm_rtaherb_man3'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3_2020_04_29'
#~ simuname1 = r'oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3'

#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015new_4yrs_2020_04_28'
#~ simuname1 = r'oueme_ZA24_H3015new_4yrs'
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015new_4yrs_2020_04_28'
simuname1 = r'oueme_ZA24_H3015new_4yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowKlown_4yrs_2020_04_30'
#~ simuname1 = r'oueme_ZA24_H3015_H2lowKlown_4yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_H3015_H2thicklowKlownhighP_2020_05_05'
#~ simuname1 = r'oueme_H3015_H2thicklowKlownhighP'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_4yrs_2020_05_02'
#~ simuname1 = r'oueme_ZA24_H3015_rivmaskarea72_4yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rt6_H1thin2_H3split_rivmaskarea72_4varyrs_2020_05_07'
#~ simuname1 = r'oueme_ZA24_H3015_rt6_H1thin2_H3split_rivmaskarea72_4varyrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_2020_05_17'
#~ simuname1 = r'H2clay'
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_10yrs_2020_05_29'
simuname1 = r'H2clay_10yrs'
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015new_4yrs_2020_04_28'
simuname1 = r'oueme_ZA24_H3015new_4yrs'
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_rand_l1000_std066_10yrs_2020_06_02'
simuname1 = r'H2clay_rand_l1000_std066_10yrs'

#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_HPC_rivmaskarea8_6yrs_2020_05_03'
#~ simuname1 = r'oueme_ZA24_H3015_HPC_rivmaskarea8_6yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_HPC_rivmaskarea8_4yrs_2020_05_05'
#~ simuname1 = r'oueme_ZA24_H3015_HPC_rivmaskarea8_4yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/rt6_H1thin2_H2clay_H3split_6yrs_2020_05_12'
#~ simuname1 = r'rt6_H1thin2_H2clay_H3split_6yrs'

#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_2020_04_23'
#~ simuname1 = r'oueme_AD'
short1 = 'KWE_AD'
short1 = 'AD_KWE'
short1 = 'AD_KWE'
#~ short1 = 'KWE_H1lowK_rt85'
#~ short1 = 'AD_rt10_nvdz_nclm_rtaherb'
short1 = 'AD_H1lowK_man3'
short1 = 'AD_man3'
short1 = 'AH_H3015'
short1 = 'H2 K00036'
#~ short1 = 'AD_rt85'
#~ short1 = 'AD rt10'
#~ short1 = 'KWE'
#~ short1 = 'H1thin2 mask72'
#~ short1 = 'H2clay'
#~ short1 = 'H3015'
#~ short1 = 'H3015_H200036'
short1 = 'H3015_H236'
short1 = 'H3015_1km'
# compare OF vs KWE:
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_OFvsKWE/oueme_OF_f3_m1_nomask_AD_4yrs_2020_04_11'
simuname1 = 'oueme_OF_f3_m1_nomask_AD_4yrs'
short1 = 'OF_F3_m1_nomask_4yrs'
# compare mannings2:
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_manning/oueme_AD_rt10_nvdz_nclm_rtaherb_2020_04_25'
simuname1 = 'oueme_AD_rt10_nvdz_nclm_rtaherb'
short1 = 'man1e-6'
# compare H1H2H3:
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_H2vegroot/oueme_AD_rt10_nvdz_nclm_rtaherb_man3_2020_04_27'
simuname1 = 'oueme_AD_rt10_nvdz_nclm_rtaherb_man3'
short1 = 'ref'
# compare rivmaskarea:
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/oueme_ZA24_H3015new_4yrs_2020_04_28'
simuname1 = 'oueme_ZA24_H3015new_4yrs'
short1 = 'ref'
# compare rivmaskarea2:
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/rt6_H1thin2_H2clay_H3split_6yrs_2020_05_12'
simuname1 = 'rt6_H1thin2_H2clay_H3split_6yrs'
short1 = 'nomask'
# make a nice one:
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay2_mask8_8yrs_2020_08_09'
simuname1 = 'H2clay2_mask8_8yrs'
#~ short1= 'mask8'
short1= 'mask8_ZIN'
# compare KH2:
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_KH2/rt6_H1thin2_H2clay_H3split_6yrs_2020_05_12'
#~ simuname1 = 'rt6_H1thin2_H2clay_H3split_6yrs'
#~ short1 = '0.00036 (ref)'
# compare C3 C4 with tappan:
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay2_mask8_tappan_8yrs_2020_08_07'
simuname1 = 'H2clay2_mask8_tappan_8yrs'
short1= 'mask8_tapC4'

props1['NCdate'] = '2020_08_24'
props1['simuname'] = 'H3015_H20001'
props1['short'] = 'Ks(H2) = 1.10-4 m/h'

props1['NCdate'] = '2020_08_27'
props1['simuname'] = 'H3015_H20002_long'
props1['short'] = 'Ks(H2) = 2.10-4 m/h'

""" SIMU 2"""
props2 = {}; props2['rt_dir'] = rt_dir

#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/plot_scale_1D/NCout/ref_2020_01_22'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_AD_2020_02_19'
simuname2 = 'oueme_OF_AD'
simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_AD_2020_03_05'
simuname2 = 'oueme_OF_f3_AD'
#~ simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H3015_2020_03_10'
#~ simuname2 = 'oueme_ZA24_f3_OF_H3015'
simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H306_4yrs_2020_03_18'
simuname2 = 'oueme_ZA24_f3_OF_H306_4yrs'
simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_m1_nomask_AD_4yrs_2020_04_11'
simuname2 = 'oueme_OF_f3_m1_nomask_AD_4yrs'
simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_H1lowK_rt85_4yrs_2020_04_17'
simuname2 = 'oueme_KWE_AD_H1lowK_rt85_4yrs'
simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_AD_2020_03_05'
simuname2 = 'oueme_OF_f3_AD'



#FARO
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_m1_nomask_highman_AD_4yrs_2020_04_13'
simuname2 = 'oueme_KWE_f3_m1_nomask_highman_AD_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_H1lowK_rt10_4yrs_2020_04_25'
#~ simuname2 = r'oueme_KWE_AD_H1lowK_rt10_4yrs'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H1lowK_rt10_nvdz_nclm_rtaherb_man3_2020_04_26'
simuname2 = r'oueme_AD_H1lowK_rt10_nvdz_nclm_rtaherb_man3'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H1lowK_H2lowP_rt10_nvdz_nclm_rtaherb_man3_2020_04_27'
simuname2 = r'oueme_AD_H1lowK_H2lowP_rt10_nvdz_nclm_rtaherb_man3'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_man3_2020_04_27'
simuname2 = r'oueme_AD_rt10_nvdz_nclm_rtaherb_man3'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3_2020_04_29'
simuname2 = r'oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_man5_2020_04_27'
#~ simuname2 = r'oueme_AD_rt10_nvdz_nclm_rtaherb_man5'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_varman_2020_04_27'
#~ simuname2 = r'oueme_AD_rt10_nvdz_nclm_rtaherb_varman'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_varman2_2020_04_28'
#~ simuname2 = r'oueme_AD_rt10_nvdz_nclm_rtaherb_varman2'

#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H306new_4yrs_2020_04_28'
#~ simuname2 = r'oueme_ZA24_H306new_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowKlown_4yrs_vdz_2020_05_01'
#~ simuname2 = r'oueme_ZA24_H3015_H2lowKlown_4yrs_vdz'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowKlown_4yrs_vdzAD_2020_05_01'
#~ simuname2 = r'oueme_ZA24_H3015_H2lowKlown_4yrs_vdzAD'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowlowKlown_4yrs_2020_05_01'
#~ simuname2 = r'oueme_ZA24_H3015_H2lowlowKlown_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H322_H2lowKlown_4yrs_2020_05_02'
#~ simuname2 = r'oueme_ZA24_H322_H2lowKlown_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_riv_4yrs_2020_05_02'
#~ simuname2 = r'oueme_ZA24_H3015_riv_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_H3015_H2thicklowKlownhighP_2020_05_05'
#~ simuname2 = r'oueme_H3015_H2thicklowKlownhighP'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_H3015_H2thicklowlowKlownhighP_2020_05_05'
#~ simuname2 = r'oueme_H3015_H2thicklowlowKlownhighP'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_4yrs_2020_05_02'
#~ simuname2 = r'oueme_ZA24_H3015_rivmaskarea72_4yrs'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_6yrs_2020_05_03'
simuname2 = r'oueme_ZA24_H3015_rivmaskarea72_6yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea8_6yrs_2020_05_03'
#~ simuname2 = r'oueme_ZA24_H3015_rivmaskarea8_6yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea2_6yrs_2020_05_03'
#~ simuname2 = r'oueme_ZA24_H3015_rivmaskarea2_6yrs'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rt6_H1thin2_H3split_rivmaskarea8_4varyrs_2020_05_07'
simuname2 = r'oueme_ZA24_H3015_rt6_H1thin2_H3split_rivmaskarea8_4varyrs'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay2_mask72_2020_05_16'
simuname2 = r'H2clay2_mask72'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H306_H2clay_10yrs_2020_05_29'
simuname2 = r'H306_H2clay_10yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_ZAZFA_1_8yrs_2020_05_28'
#~ simuname2 = r'H2clay_ZAZFA_1_8yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_ZAZFA_1_10yrs_2020_06_03'
#~ simuname2 = r'H2clay_ZAZFA_1_10yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/rt6_H1thin2_H2claylowK_H3split_6yrs_2020_05_13'
#~ simuname2 = r'rt6_H1thin2_H2claylowK_H3split_6yrs'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/rt6_H1thin2_H2claylowK2_H3split_6yrs_2020_05_14'
simuname2 = r'rt6_H1thin2_H2claylowK2_H3split_6yrs'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H306new_4yrs_2020_04_28'
simuname2 = r'oueme_ZA24_H306new_4yrs'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_H3015_rand_l10000_std066_8yrs_2020_06_08'
simuname2 = r'H2clay_H3015_rand_l10000_std066_8yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_HPC_rivmaskarea8_4yrs_2020_05_05'
#~ simuname2 = r'oueme_ZA24_H3015_HPC_rivmaskarea8_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2highret_HPC_rivmaskarea8_4yrs_2020_05_05'
#~ simuname2 = r'oueme_ZA24_H3015_H2highret_HPC_rivmaskarea8_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_HPC_rivmaskarea2_4varyrs_2020_05_05'
#~ simuname2 = r'oueme_ZA24_H3015_HPC_rivmaskarea2_4varyrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowK_4yrs_2020_04_29'
#~ simuname2 = r'oueme_ZA24_H3015_H2lowK_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_6yrs_2020_05_03'
#~ simuname2 = r'oueme_ZA24_H3015_rivmaskarea72_6yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_2020_04_25'
#~ simuname2 = r'oueme_AD_rt10_nvdz_nclm_rtaherb'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H3lowP_nort_nvdz_nclm_rtaherb_man3_2020_05_05'
#~ simuname2 = r'oueme_AD_H3lowP_nort_nvdz_nclm_rtaherb_man3'
short2 = 'OF_AD'
short2 = 'KWE_AD_deeproots'
short2 = 'AD_OF'
short2 = 'AD_KWE_highman'
#~ short2 = 'OF_KWE'
#~ short2 = 'KWE_f3'
#~ short2 = 'AD_KWE_H1lowK'
#~ short2 = 'AD_OF'
#~ short1 = 'KWE_H1lowK_rt85_nof3'
#~ short2 = 'KWE_H1lowK_rt85_newriv'
#~ short2 = 'KWE_H1lowK_rt10'
#~ short2 = 'AD_rt10_nvdz_nclm_rtaherb'
#~ short2 = 'AD_H1lowK_rt10_nvdz_nclm_rtaherb_man3'
short2 = 'AD_H1lowK_H2lowP_rt10_nvdz_nclm_rtaherb_man3'
short2 = 'AD_man3'
short2 = 'AD_man3 H3lowP'
#~ short2 = 'AD_man5'
#~ short2 = 'AD_varman'
#~ short2 = 'AD_man_riv3_slopes8'
#~ short2 = 'H306'
#~ short2 = 'AH_H3015 H2lowK'
#~ short2 = 'H3015 rivmaskarea72'
#~ short2 = 'H3015 rivmaskarea72'
short2 = 'H3015 rivmaskarea72_6yrs'
short2 = 'H1thin2 mask8'
short2 = 'H2clay_mask72'
#~ short2 = 'H3015 rivmaskarea8_6yrs'
#~ short2 = 'H3015 rivmaskarea2_6yrs'
#~ short2 = 'H3015 HPC rivmaskarea8_6yrs'
#~ short2 = 'H3015 H2highret HPC rivmaskarea8_4yrs'
#~ short2 = 'HPC rivmaskarea8_4yrs'
#~ short2 = 'HPC rivmaskarea2_4yrs'
#~ short2='AD no rt'
#~ short2 = 'H2 K0001'
#~ short2 = 'H2 K0003'
#~ short2 = 'H306'
short2 = 'H3015_H20003'
short2 = 'H306_H236'
short2 = 'H3015_10km'
#~ short2 = 'ZAZFA_1'
# compare OF vs KWE:
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_OFvsKWE/oueme_KWE_f3_m1_nomask_AD_4yrs_2020_04_10'
simuname2 = 'oueme_KWE_f3_m1_nomask_AD_4yrs'
short2 = 'KWE_f3_m1_nomask_4yrs'
# compare mannings2:
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_manning/oueme_AD_rt10_nvdz_nclm_rtaherb_man3_2020_04_27'
simuname2 = 'oueme_AD_rt10_nvdz_nclm_rtaherb_man3'
short2 = 'man3e-6'
# compare H1H2H3:
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_H2vegroot/oueme_AD_H1lowK_rt10_nvdz_nclm_rtaherb_man3_2020_04_26'
simuname2 = 'oueme_AD_H1lowK_rt10_nvdz_nclm_rtaherb_man3'
short2 = 'H1lowK'
# compare rivmaskarea:
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/oueme_ZA24_H3015_rivmaskarea72_6yrs_2020_05_03'
simuname2 = 'oueme_ZA24_H3015_rivmaskarea72_6yrs'
short2 = 'mask72'
# compare rivmaskarea:
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/oueme_ZA24_H3015_H2highret_HPC_rivmaskarea8_4yrs_2020_05_05'
simuname2 = 'oueme_ZA24_H3015_H2highret_HPC_rivmaskarea8_4yrs'
short2 = 'HPC_mask8_H2highret'
# compare rivmaskarea2:
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/H2clay2_mask72_2020_05_16'
simuname2 = 'H2clay2_mask72'
short2 = 'mask72'
#make a nice one
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay2_mask8_tappan_8yrs_2020_08_07'
simuname2 = 'H2clay2_mask8_tappan_8yrs'
#~ short2= 'mask8'
short2= 'mask8_TAP'
# compare KH2:
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_KH2/rt6_H1thin2_H2claylowK2_H3split_6yrs_2020_05_14'
#~ simuname2 = 'rt6_H1thin2_H2claylowK2_H3split_6yrs'
#~ short2 = '0.0003'
# compare C3 C4 with tappan:
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/mask8_tappan_C3tree_2020_08_13'
simuname2 = 'mask8_tappan_C3tree'
short2= 'mask8_tapC3'

props2['NCdate'] = '2020_08_24'
props2['simuname'] = 'H3015_H20002'
props2['short'] = 'Ks(H2) = 2.10-4 m/h'

props2['NCdate'] = '2020_08_27'
props2['simuname'] = 'H3015_H2clay24prct_long'
props2['short'] = 'Ks(H2) clay (24%)'


""" SIMU 3"""
props3 = {}; props3['rt_dir'] = rt_dir

simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_AD_2020_03_05'
simuname3 = 'oueme_KWE_f3_AD'
simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_nosec_AD_2020_03_15'
simuname3 = 'oueme_KWE_f3_nosec_AD'
#~ simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_H3015_2020_03_06'
#~ simuname3 = 'oueme_ZA24_f3_H3015'
#not f3:
#~ simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZAZFA48_H300023_H4023_2020_03_03'
#~ simuname3 = 'oueme_ZAZFA48_H300023_H4023'
simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H3015_4yrs_2020_03_14'
simuname3 = 'oueme_ZA24_f3_OF_H3015_4yrs'
simdir3= r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_KWE_H3015_4yrs_2020_04_09'
simuname3 = 'oueme_ZA24_f3_KWE_H3015_4yrs'
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_nosec_AD_2020_03_15'
simuname3 = 'oueme_KWE_f3_nosec_AD'

#FARO
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_man3_2020_04_27'
simuname3 = r'oueme_AD_rt10_nvdz_nclm_rtaherb_man3'
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea8_6yrs_2020_05_03'
simuname3 = r'oueme_ZA24_H3015_rivmaskarea8_6yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowlowKlown_4yrs_2020_05_01'
#~ simuname3 = r'oueme_ZA24_H3015_H2lowlowKlown_4yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H322_H2lowKlown_4yrs_2020_05_02'
#~ simuname3 = r'oueme_ZA24_H322_H2lowKlown_4yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_riv_4yrs_2020_05_02'
#~ simuname3 = r'oueme_ZA24_H3015_riv_4yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_H3015_H2thicklowKlownhighP_2020_05_05'
#~ simuname3 = r'oueme_H3015_H2thicklowKlownhighP'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_H3015_H2thicklowlowKlownhighP_2020_05_05'
#~ simuname3 = r'oueme_H3015_H2thicklowlowKlownhighP'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_4yrs_2020_05_02'
#~ simuname3 = r'oueme_ZA24_H3015_rivmaskarea72_4yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_6yrs_2020_05_03'
#~ simuname3 = r'oueme_ZA24_H3015_rivmaskarea72_6yrs'
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea8_6yrs_2020_05_03'
simuname3 = r'oueme_ZA24_H3015_rivmaskarea8_6yrs'
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H30029_H2clay_10yrs_2020_05_29'
simuname3 = r'H30029_H2clay_10yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_ZAZFA_10_8yrs_2020_05_29'
#~ simuname3 = r'H2clay_ZAZFA_10_8yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_ZAZFA_10_10yrs_2020_06_03'
#~ simuname3 = r'H2clay_ZAZFA_10_10yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/rt6_H1thin2_H2claylowK_H3split_6yrs_2020_05_14'
#~ simuname3 = r'rt6_H1thin2_H2claylowK_H3split_6yrs'
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_10yrs_2020_05_29'
simuname3 = r'H2clay_10yrs'
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H30029new_4yrs_2020_04_28'
simuname3 = r'oueme_ZA24_H30029new_4yrs'
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_H306_rand_l1000_std066_8yrs_2020_06_08'
simuname3 = r'H2clay_H306_rand_l1000_std066_8yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay2_mask8_2020_05_16'
#~ simuname3 = r'H2clay2_mask8'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea2_6yrs_2020_05_03'
#~ simuname3 = r'oueme_ZA24_H3015_rivmaskarea2_6yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_HPC_rivmaskarea8_4yrs_2020_05_05'
#~ simuname3 = r'oueme_ZA24_H3015_HPC_rivmaskarea8_4yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2highret_HPC_rivmaskarea8_4yrs_2020_05_05'
#~ simuname3 = r'oueme_ZA24_H3015_H2highret_HPC_rivmaskarea8_4yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_HPC_rivmaskarea2_4varyrs_2020_05_05'
#~ simuname3 = r'oueme_ZA24_H3015_HPC_rivmaskarea2_4varyrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowK_4yrs_2020_04_29'
#~ simuname3 = r'oueme_ZA24_H3015_H2lowK_4yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_6yrs_2020_05_03'
#~ simuname3 = r'oueme_ZA24_H3015_rivmaskarea72_6yrs'


short3 = 'KWE_AH'
short3 = 'AD_KWE_nosec'
short3 = 'AD'
short3 = 'H3015 rivmaskarea8'
short3 = 'H3015 rivmaskarea72_6yrs'
short3 = 'H3015 rivmaskarea8_6yrs'
#~ short3 = 'H3015 rivmaskarea2_6yrs'
#~ short3 = 'H3015 HPC rivmaskarea8_6yrs'
#~ short3 = 'H3015 H2highret HPC rivmaskarea8_4yrs'
#~ short3 = 'HPC rivmaskarea8_4yrs'
#~ short3 = 'HPC rivmaskarea2_4yrs'
short3 = 'H2 K0002'
short3 = 'H3015'
#~ short3 = 'H2clay_mask8'
#~ short3 = 'H30029'
#~ short3 = 'ZAZFA_10'
short3 = 'H30029_H236'
short3 = 'H306_1km'
# compare OF vs KWE:
simdir3= r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_OFvsKWE/oueme_OF_f3_AD_2020_03_05'
simuname3 = 'oueme_OF_f3_AD'
short3 = 'OF_f3_AD'
# compare mannings2:
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_manning/oueme_AD_rt10_nvdz_nclm_rtaherb_man5_2020_04_27'
simuname3 = 'oueme_AD_rt10_nvdz_nclm_rtaherb_man5'
short3 = 'man5e-6'
# compare H1H2H3:
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_H2vegroot/oueme_AD_H1lowK_H2lowP_rt10_nvdz_nclm_rtaherb_man3_2020_04_27'
simuname3 = 'oueme_AD_H1lowK_H2lowP_rt10_nvdz_nclm_rtaherb_man3'
short3 = 'H1lowK H2lowP'
# compare rivmaskarea:
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/oueme_ZA24_H3015_rivmaskarea8_6yrs_2020_05_03'
simuname3 = 'oueme_ZA24_H3015_rivmaskarea8_6yrs'
short3 = 'mask8'
# compare rivmaskarea:
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/oueme_ZA24_H3015_HPC_rivmaskarea8_6yrs_2020_05_03'
simuname3 = 'oueme_ZA24_H3015_HPC_rivmaskarea8_6yrs'
short3 = 'HPC_mask8'
# compare rivmaskarea2:
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/H2clay2_mask8_2020_05_16'
simuname3 = 'H2clay2_mask8'
short3= 'mask8'
# make a nice one:
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay2_mask8_8yrs_2020_08_09'
simuname3 = 'H2clay2_mask8_8yrs'
#~ short1= 'mask8'
short3= 'mask8_ZIN'
# compare KH2:
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_KH2/rt6_H1thin2_H2claylowK_H3split_6yrs_2020_05_14'
#~ simuname3 = 'rt6_H1thin2_H2claylowK_H3split_6yrs'
#~ short3 = '0.0002'
# compare C3 C4 with tappan:
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay2_mask8_tappan_8yrs_2020_08_07'
simuname3 = 'H2clay2_mask8_tappan_8yrs'
short3= 'mask8_tapC4'

props3['NCdate'] = '2020_08_24'
props3['simuname'] = 'H3015_H20003'
props3['short'] = 'Ks(H2) = 3.10-4 m/h'

props3['NCdate'] = '2020_08_24'
props3['simuname'] = 'H3015_H20002'
props3['short'] = 'Ks(H2) = 2.10-4 m/h'

props3['NCdate'] = '2020_08_29'
props3['simuname'] = 'H3015_H20002_long_4yrs'
props3['short'] = 'Ks(H2) = 2.10-4 m/h + 4yrs'

""" SIMU 4"""
props4 = {}; props4['rt_dir'] = rt_dir

#~ simdir4 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_AD_2020_03_05'
#~ simuname4 = 'oueme_OF_f3_AD'
simdir4 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_nosec_AD_2020_03_16'
simuname4 = 'oueme_OF_f3_nosec_AD'
#~ simdir4= r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZAZFA48_f3_OF_H300023_H4023_2020_03_10'
#~ simuname4 = 'oueme_ZAZFA48_f3_OF_H300023_H4023'
simdir4= r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZAZFA48_f3_OF_H300023_H4023_4yrs_2020_03_16'
simuname4 = 'oueme_ZAZFA48_f3_OF_H300023_H4023_4yrs'
simdir4= r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H3015_4yrs_2020_03_14'
simuname4 = 'oueme_ZA24_f3_OF_H3015_4yrs'
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_nosec_AD_2020_03_16'
simuname4 = 'oueme_OF_f3_nosec_AD'

#FARO
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowlowKlown_4yrs_2020_05_01'
#~ simuname4 = r'oueme_ZA24_H3015_H2lowlowKlown_4yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H322_H2lowKlown_4yrs_2020_05_02'
#~ simuname4 = r'oueme_ZA24_H322_H2lowKlown_4yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_riv_4yrs_2020_05_02'
#~ simuname4 = r'oueme_ZA24_H3015_riv_4yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_H3015_H2thicklowKlownhighP_2020_05_05'
#~ simuname4 = r'oueme_H3015_H2thicklowKlownhighP'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_H3015_H2thicklowlowKlownhighP_2020_05_05'
#~ simuname4 = r'oueme_H3015_H2thicklowlowKlownhighP'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_4yrs_2020_05_02'
#~ simuname4 = r'oueme_ZA24_H3015_rivmaskarea72_4yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_6yrs_2020_05_03'
#~ simuname4 = r'oueme_ZA24_H3015_rivmaskarea72_6yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea8_6yrs_2020_05_03'
#~ simuname4 = r'oueme_ZA24_H3015_rivmaskarea8_6yrs'
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea2_6yrs_2020_05_03'
simuname4 = r'oueme_ZA24_H3015_rivmaskarea2_6yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_HPC_rivmaskarea8_4yrs_2020_05_05'
#~ simuname4 = r'oueme_ZA24_H3015_HPC_rivmaskarea8_4yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2highret_HPC_rivmaskarea8_4yrs_2020_05_05'
#~ simuname4 = r'oueme_ZA24_H3015_H2highret_HPC_rivmaskarea8_4yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_HPC_rivmaskarea2_4varyrs_2020_05_05'
#~ simuname4 = r'oueme_ZA24_H3015_HPC_rivmaskarea2_4varyrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowK_4yrs_2020_04_29'
#~ simuname4 = r'oueme_ZA24_H3015_H2lowK_4yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_6yrs_2020_05_03'
#~ simuname4 = r'oueme_ZA24_H3015_rivmaskarea72_6yrs'
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/rt6_H1thin2_H2claylowK2_H3split_6yrs_2020_05_14'
simuname4 = r'rt6_H1thin2_H2claylowK2_H3split_6yrs'
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/rt6_H1thin2_H2claylowK_H3split_6yrs_2020_05_13'
simuname4 = r'rt6_H1thin2_H2claylowK_H3split_6yrs'
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2claylowK_8yrs_2020_05_20'
simuname4 = r'H2claylowK_8yrs'
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_10yrs_2020_05_29'
simuname4 = r'H2clay_10yrs'
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_H306_rand_l10000_std066_8yrs_2020_06_08'
simuname4 = r'H2clay_H306_rand_l10000_std066_8yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_ZAZFA_1_10yrs_2020_06_03'
#~ simuname4 = r'H2clay_ZAZFA_1_10yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_ZAZFA_100_7yrs_2020_05_29'
#~ simuname4 = r'H2clay_ZAZFA_100_7yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_ZAZFA_100_9yrs_2020_06_03'
#~ simuname4 = r'H2clay_ZAZFA_100_9yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay2_mask2_2020_05_16'
#~ simuname4 = r'H2clay2_mask2'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay2_mask2_6yrs_2020_05_19'
#~ simuname4 = r'H2clay2_mask2_6yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3_2020_04_29'
#~ simuname4 = r'oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea2_6yrs_2020_05_03'
#~ simuname4 = r'oueme_ZA24_H3015_rivmaskarea2_6yrs'
short4 = 'OF_AH'
short4 = 'AD_OF_nosec'
short4 = 'AD_H3lowP'
short4 = 'H3015 rivmaskarea2'
short4 = 'H3015 rivmaskarea72_6yrs'
#~ short4 = 'H3015 rivmaskarea8_6yrs'
short4 = 'H3015 rivmaskarea2_6yrs'
#~ short4 = 'H3015 HPC rivmaskarea8_6yrs'
#~ short4 = 'H3015 H2highret HPC rivmaskarea8_4yrs'
#~ short4 = 'HPC rivmaskarea8_4yrs'
#~ short4 = 'HPC rivmaskarea2_4yrs'
#~ short4 = 'H2 K0003'
short4 = 'H2 K0001'
short4 = 'H3015_H20001'
#~ short4 = 'H2clay_mask2'
#~ short4 = 'ZAZFA_1'
#~ short4 = 'ZAZFA_100'
short4 = 'H3015'
short4 = 'H306_10km'
# compare OF vs KWE:
simdir4= r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_OFvsKWE/oueme_KWE_f3_AD_2020_03_05'
simuname4 = 'oueme_KWE_f3_AD'
short4 = 'KWE_f3_AD'
# compare mannings2:
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_manning/oueme_AD_rt10_nvdz_nclm_rtaherb_varman_2020_04_27'
simuname4 = 'oueme_AD_rt10_nvdz_nclm_rtaherb_varman'
short4 = 'varman_riv1E-6_slop5E-6'
# compare H1H2H3:
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_H2vegroot/oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3_2020_04_29'
simuname4 = 'oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3'
short4 = 'H3lowP'
# compare rivmaskarea:
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/oueme_ZA24_H3015_rivmaskarea2_6yrs_2020_05_03'
simuname4 = 'oueme_ZA24_H3015_rivmaskarea2_6yrs'
short4 = 'mask2'
# compare rivmaskarea:
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/oueme_ZA24_H3015_HPC_rivmaskarea2_4varyrs_2020_05_05'
simuname4 = 'oueme_ZA24_H3015_HPC_rivmaskarea2_4varyrs'
short4 = 'HPC_mask2'
# compare rivmaskarea2:
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/H2clay2_mask2_6yrs_2020_05_19'
simuname4 = 'H2clay2_mask2_6yrs'
short4= 'mask2'
#make a nice one
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay2_mask8_tappan_8yrs_2020_08_07'
simuname4 = 'H2clay2_mask8_tappan_8yrs'
#~ short2= 'mask8'
short4= 'mask8_TAP'
# compare KH2:
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_KH2/rt6_H1thin2_H2claylowK_H3split_6yrs_2020_05_13'
#~ simuname4 = 'rt6_H1thin2_H2claylowK_H3split_6yrs'
#~ short4 = '0.0001'
# compare C3 C4 with tappan:
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/mask8_tappan_C3tree_2020_08_13'
simuname4 = 'mask8_tappan_C3tree'
short4 = 'mask8_tapC3'

props4['NCdate'] = '2020_08_24'
props4['simuname'] = 'H3015_H20004'
props4['short'] = 'Ks(H2) = 4.10-4 m/h'

props4['NCdate'] = '2020_08_24'
props4['simuname'] = 'H3015_H20002'
props4['short'] = 'Ks(H2) = 2.10-4 m/h'

props4['NCdate'] = '2020_08_29'
props4['simuname'] = 'H3015_H2clay24prct_long_4yrs'
props4['short'] = 'Ks(H2) clay (24%) + 4yrs'


sims=OrderedDict() #ordered dict classes is like a dictionnary (a={}) but preserved the order in which items are assigned
sims['sim1'] = {'props':props1}
sims['sim2'] = {'props':props2}
sims['sim3'] = {'props':props3}
sims['sim4'] = {'props':props4}

for sim, simdata in sims.items(): 
    simdata['props']['simdir'] = rt_dir + copy.copy(simdata['props']['simuname']) + '_' + simdata['props']['NCdate']
    sims[sim] = simdata
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def prepare_colormap(cmapname,bound_low,bound_high,bin_size = 1):
	"""
	prepare a colormap based on the data range
	also force first entry to be grey
	"""
	cmap = cmapname
	cmaplist = [cmap(i) for i in range(cmap.N)] 				# extract all colors from the .jet map
	#~ cmaplist[0] = (.5,.5,.5,1.0) 								# force the first color entry to be grey
	#~ cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N) 	# create the new map
	bounds = np.arange(bound_low,bound_high,bin_size) 									# define the bins and normalize
	norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
	return cmap,norm,bounds
    
def find_grid_location(xx,yy,x,y):
    """matches the simulation grid given by xx and yy created by meshgrid and a given location (x,y):
    finds the closest match
    """
    dist = np.sqrt((np.round(xx)- x)**2 + (np.round(yy) - y)**2)
    ind_min = np.where(dist == dist.min())   
    return ind_min[1][0],ind_min[0][0],np.round(xx[ind_min])[0],np.round(yy[ind_min])[0]
    
def xtract_WTs(P,x,y,z):
    """Extract WT time series at each depth from an xarray P at locations x and y and given the depth profile z"""
    Ptmp=P.succion.sel(longitude=x,latitude=y)    
    #note that in pfoutputs.py:
    #profile[0] = base of domain. if the criteria is p < 0 => 0, you realize there are some weird points at the bottom
    #of perched layers with p>0 but < self.dz ... so as if the cell is not entirely saturated but p>0 ... 
    #profile = np.array([0. if self.press_array[xloc,yloc,i,t]<= self.dz_mult_arr[xloc,yloc,i]*self.dz/2 else 1. for i in range(self.nz)])
    #so?:
    #~ tmp2=(tmp-np.tile((P1.var_dz[:,50,50]*3).data,(354,1))/2>=0)
    #~ tmp2=(tmp>=0)
    tmp=np.zeros(np.shape(Ptmp.data))
    tmp[Ptmp.data>=0]=1
    for i in range(np.shape(tmp)[1]-1):
        tmp[:,i+1]=(tmp[:,i]+tmp[:,i+1])*tmp[:,i+1]
        # a=[1,1,1,0,0,0,1,1,0,1,0,0,0,1] => [1, 2, 3, 0, 0, 0, 1, 2, 0, 1, 0, 0, 0, 1]
        tmp[tmp!=1.]=np.nan
    #~ z=np.cumsum(P.var_dz[::-1,ysim_ind,xsim_ind]*3) -  P1.var_dz[::-1,ysim_ind,xsim_ind]*3/2
    WTs =np.tile(z[::-1],(np.shape(Ptmp.data)[0],1)) -(tmp*Ptmp.data) 
    return pd.DataFrame({z.data[::-1][depth]:pd.Series(WTs[:,depth],index=P.time.data) for depth in range(len(z))})    

def get_WT_simulation_results(P,stadic):
    """fill in the station dictionnary with WTs at stations locations calculated from """
    
    for stationname,sta in stadic.items():
        print(sta.name)
        z=np.cumsum(P.var_dz[::-1,sta.ysim_ind,sta.xsim_ind]*3) -  P.var_dz[::-1,sta.ysim_ind,sta.xsim_ind]*3/2
        sta.wts=xtract_WTs(P,sta.xsim,sta.ysim,z)
        stadic[stationname]=sta
    return stadic
    
def plot_WT_time_series(stadic,simdir='',savefig=True):
    """"""
    fig,ax =plt.subplots(nrows=int(np.ceil(len(stadic)/3)),ncols = 3,figsize=(15,20), squeeze=True,sharex=True,sharey=True)

    i=0
    j=0
    k=0

    for stationname,sta in stadic.items():
        # interpolation on sim + daily approach:
        obs = sta.WT.dropna().sort_index(axis=0)
        #~ simu = sta.WTsim.dropna().sort_index(axis=0)
        #~ simu = sta.wts[0].dropna().sort_index(axis=0)
        simu = sta.wts.iloc[:,0].dropna().sort_index(axis=0)
        #~ obs = obs.groupby(obs.index).mean()
        obs = obs.groupby(obs.index).mean().rename('obs')
        
        #~ obs_int=pd.concat([obs,simu],axis=1).interpolate(method='slinear',limit=50)[0]
        obs_int=pd.concat([obs,simu],axis=1).interpolate(method='slinear',limit=50)['obs']
        obs_intD = obs_int.resample('D').mean()
        simuD = simu.resample('D').mean()
        merge=pd.concat([obs_intD.rename('obs'),simuD.rename('sim')],axis=1)
        #~ obs_intD.name = 'obs'
        #~ simuD.name = 'sim'
        #~ merge=pd.concat([obs_intD,simuD],axis=1)
        kept = merge.dropna()
        res = obj.kge(kept['obs'].values, kept['sim'].values, return_all=True)
        print(res)  
      

        #2 columns version:
        #~ if i>=int(np.ceil(len(stadic)/2)):
            #~ j=1
            #~ i=0
        if (k>=int(np.ceil(len(stadic)/3))) & (k<2*int(np.ceil(len(stadic)/3))):
            j=1
            i=k-int(np.ceil(len(stadic)/3))
        elif k>=2*int(np.ceil(len(stadic)/3)):
            j=2
            i=k-2*int(np.ceil(len(stadic)/3))        
            
            
        #~ ax[i][j].plot(sta.WT.loc['2007-1-1':'2007-12-31'].dropna(),'k')
        ax[i][j].plot(sta.WT.dropna().sort_index(axis=0),'k')
        
        #~ ax[i][j].plot(sta.WTsim.loc['2007-1-1':'2007-12-31'].dropna(),'r')
        #~ ax[i][j].plot(sta.WTsim.dropna().sort_index(axis=0),'r')
        #~ for k in range(sim.nz):
        for k2 in range(len(sta.wts.columns)):
            #~ ax[i][j].plot(sta.wts[k].dropna().sort_index(axis=0),'r')
            ax[i][j].plot(sta.wts.iloc[:,k2].dropna().sort_index(axis=0),'r')
        ax[i][j].plot(obs_intD,'b')
                
        
        #~ ax[i].plot(sta.WTsim.loc['2007-1-1':'2007-12-31'],'r')
        if j==0: ax[i][j].set_ylabel('WTD(m)')

        if i==0: ax[i][j].legend(['obs','sim'],fontsize=8,loc='upper left',ncol=2)
        #~ ax[i,j].set_xlim([datetime.datetime(2002,1,1),datetime.datetime(2011,12,31)])
        ax[i,j].set_xlim([datetime.datetime(2004,1,1),datetime.datetime(2007,12,31)])
        ax[i,j].set_ylim([-0.5,25])
        #~ ax[i,j].text(datetime.datetime(2004,3,25),18,r'%s: $r^2 = %2.2f$ KGE = %2.2f'%(stationname,res[1]*res[1],res[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})
        ax[i,j].text(datetime.datetime(2004,3,25),18,r'%s'%stationname,FontSize=10,bbox = {'facecolor':'white', 'alpha':0.7})
           
        ax[i,j].tick_params(axis='x', which='both', labelbottom=False, labeltop=False)
        if j ==0: ax[i,j].tick_params(axis='y', which='both', labelright=False, labelleft=True)
        if j ==1: ax[i,j].tick_params(axis='y', which='both', labelright=False, labelleft=False)
        if j ==2: ax[i,j].tick_params(axis='y', which='both', labelright=True, labelleft=False)
        if j ==0: ax[i,j].tick_params(axis='both', which='major', bottom=False,top=False,right=False,left=True)
        if j ==1: ax[i,j].tick_params(axis='both', which='major', bottom=False,top=False,right=False,left=False)
        if j ==2: ax[i,j].tick_params(axis='both', which='major', bottom=False,top=False,right=True,left=False)
        i+=1
        k+=1
        
    #~ for i in range(len(stadic)):
    fig.subplots_adjust(bottom=0.06, top =0.98,left=0.05,right =0.96,wspace=0.0, hspace=0.000)
        #~ fig.axes[i].invert_yaxis() # if share_y = True this may not work : all axes now behave as if their were one. For instance, when you invert one of them, you affect all 
    fig.axes[0].invert_yaxis() #if share_y=True            
    ax[0,0].tick_params(axis='both', which='major', bottom=False,top=True,right=False,left=True)
    ax[0,1].tick_params(axis='both', which='major', bottom=False,top=True,right=False,left=False)
    ax[0,2].tick_params(axis='both', which='major', bottom=False,top=True,right=True,left=False)
    ax[int(np.ceil(len(stadic)/3))-1,0].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)
    ax[int(np.ceil(len(stadic)/3))-1,1].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)
    ax[int(np.ceil(len(stadic)/3))-2,2].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)
    ax[int(np.ceil(len(stadic)/3))-2,2].tick_params(axis='x', which='major', bottom=True, labeltop=False)
    fig.autofmt_xdate()
    #~ ax[int(np.ceil(len(stadic)/3))-1,2].set_axis_off()

    if savefig:
        plt.savefig(os.sep.join([simdir,'WTD.png']),dpi=400,format='png')
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
"""Read in Data"""
######### Oueme
#~ WTOu= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/WTOu.csv')
#~ WTOu= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/processed_data/WTOu_added_uncheckeddata_12h.csv')
WTOu= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/processed_data/WT_Oueme_smoothedDailyMin.csv')
WTOu = WTOu.set_index(WTOu.columns[0])
WTOu.index = pd.to_datetime(WTOu.index,format='%Y-%m-%d %H:%M:%S')
WTOu.index.rename('Date',inplace=True)
######### oueme transect (belle, naloh, etc)
WTOut= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/processed_data/WT_Oueme_transect_smoothedDailyMean.csv')
WTOut = WTOut.set_index(WTOut.columns[0])
WTOut.index = pd.to_datetime(WTOut.index,format='%Y-%m-%d %H:%M:%S')
WTOut.index.rename('Date',inplace=True)
#~ stations_to_keep = ['NALO-P034-10','NALO-P190-11','NALO-P500-10','Bele_P0099_120','Bele_P0192_120','Bele_P0312_100','Bele_P0688_22','Bele_P0968_24','Bele_P1250_21']
stations_to_keep = ['Bele_P0099_120','Bele_P0192_120','Bele_P0312_100','Bele_P0688_22','Bele_P0968_24','Bele_P1250_21']
WTOut=WTOut[stations_to_keep]

WT=pd.concat([WTOu,WTOut],axis=1)
#example plot:
#~ ax = WTOu.plot(subplots=True,layout=[15,3],sharey=True,ylim=[-25,0],figsize=[20,10])
ax = WT.plot(subplots=True,layout=[18,3],sharey=True,ylim=[-25,0],figsize=[20,10])
plt.gcf().subplots_adjust(bottom=0.05, top =0.95, hspace=0.001,wspace=0.001)
for a in ax:
    for b in a:
        b.legend(fontsize=6)
        #~ try: b.get_lines()[0].set_color(vegcolor300m[WTOuSta.loc[b.get_legend_handles_labels()[1][0],'ESA300_2015t']])
        try: b.get_lines()[0].set_color(handcolor[WTOuSta.loc[b.get_legend_handles_labels()[1][0],'hand_range']])
        except: print('no ax in here')

WT=-WT

######### METADATA
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/WTOuSta.csv')
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/WTOuSta_added_uncheckeddata_12h.csv')
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/WTOuSta_added_uncheckeddata_12h_added_precip_from_AC.csv')
WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/processed_data/WTOuSta_added_uncheckeddata_12h_added_precip_from_AC.csv')
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/WTOuSta_added_uncheckeddata_12h_15Dmedianmov_window_added_precip_from_AC.csv')
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/WTOuSta_smoothedDailyMin.csv')
#~ WTOuSta= pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/WTOuSta_smoothedDailyMin_P001deg_1day.csv')
WTOuSta = WTOuSta.set_index(WTOuSta.columns[0])

""" Find the location of data in the PF framework"""
#~ P = xr.open_dataset(os.sep.join([simdir1,''.join([simuname1,'_press.nc'])]),decode_times=False)
P = xr.open_dataset(os.sep.join([sims['sim1']['props']['simdir'],''.join([sims['sim1']['props']['simuname'],'_press.nc'])]),decode_times=False)
[xx,yy]=np.meshgrid(P.longitude,P.latitude)
xx = np.round(xx)
yy = np.round(yy)

stadic ={}
#create a dictionnary of station objects
for stationname in WT.columns:
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    sta.WT=WT[stationname]
    WTOuSta['xsim_ind'],WTOuSta['ysim_ind'],WTOuSta['xsim'],WTOuSta['ysim'] = zip(*WTOuSta.apply(lambda col: find_grid_location(xx,yy,col.x,col.y),axis=1))

    sta.xsim=WTOuSta.loc[stationname,'xsim']  
    sta.ysim=WTOuSta.loc[stationname,'ysim']
    sta.xsim_ind=WTOuSta.loc[stationname,'xsim_ind']  
    sta.ysim_ind=WTOuSta.loc[stationname,'ysim_ind']
    
    stadic[stationname]=sta

 



""" Read in sim outputs using xarray"""

for sim, simdata in sims.items(): 
    simdata['D'] = xr.open_dataset(os.sep.join([simdata['props']['simdir'],''.join([simdata['props']['simuname'],'_press.nc'])]),decode_times=False)
    tsim = pd.date_range('1/1/2005 00:00:00', periods=len(simdata['D'].time), freq='D')
    simdata['D'] = simdata['D'].assign_coords(time = pd.to_datetime(tsim)) #for satur and press
    simdata['Dm'] = simdata['D'].where(simdata['D'].mask>0).sel(time=slice('%d-01-01'%plot_yr, '%d-12-20'%plot_yr))
    simdata['tot_thickness']= np.array(simdata['D'].var_dz[::-1,0,0]*dz).cumsum()[-1] - simdata['D'].var_dz[0,0,0]*dz/2
    simdata['stadic']=get_WT_simulation_results(simdata['D'],copy.deepcopy(stadic))
    plot_WT_time_series(stadic=simdata['stadic'],simdir=simdata['props']['simdir'],savefig=True)
    sims[sim] = simdata

""" Prepare plot outputs """
figname_template='%dsims'%len(sims)
for sim, simdata in sims.items(): 
    figname_template=figname_template+'_%s'%simdata['props']['simuname']
outdirs = []
for sim, simdata in sims.items(): 
    outdir = os.sep.join([simdata['props']['simdir'],'compare_'+figname_template])
    simdata['props']['outdir'] = outdir
    outdirs.append(outdir)
    try: os.mkdir(outdir)
    except: print('directory already exist: %s'%outdir)

"""Plot maps: mean WTD"""
fig,ax = plt.subplots(int(np.ceil(len(sims)/2)),2,figsize = [7,7],sharex=True,sharey=True)
i=0;j=0
for sim, simdata in sims.items(): 
    p=(simdata['tot_thickness'] - simdata['Dm'].succion[:,0,:,:].mean(dim='time')).plot(ax=ax[j][i],robust=True,vmin=0,vmax=15,add_labels=False,add_colorbar=False)
    ax[j][i].set_title(simdata['props']['short'],loc='left',fontdict={'fontsize':12, 'fontweight':'bold'})
    ax[j][i].set_xticklabels('');ax[j][i].set_yticklabels('');ax[j][i].set_aspect(1)    
    if i>=1: j+=1; i=-1;        
    i+=1

#control colorbar
cb = fig.colorbar(p, ax=ax.ravel().tolist())
cb.set_label(label="WTD (m)",size=18)
fig.canvas.draw(); cb.ax.set_yticklabels([a.get_text() for a in cb.ax.get_yticklabels()],fontsize = 18)
#or:
#~ cbar_ax = fig.add_axes([0.15, 0.05, 0.7, 0.05])
#~ fig.colorbar(ax1, cax=cbar_ax,orientation='horizontal',label="WTD (m)")
for outdir in outdirs:
    plt.savefig(os.sep.join([outdir,'WTD_%s'%(figname_template)+'.png']),dpi=400,format='png')


"""Plot maps: amplitude WTD"""
fig,ax = plt.subplots(int(np.ceil(len(sims)/2)),2,figsize = [7,7],sharex=True,sharey=True)
i=0;j=0
for sim, simdata in sims.items(): 
    p = (simdata['Dm'].succion[:,0,:,:].max(dim='time') - simdata['Dm'].succion[:,0,:,:].min(dim='time')).plot(ax=ax[j][i],robust=True,vmin=0,vmax=8,add_labels=False,add_colorbar=False)
    ax[j][i].set_title(simdata['props']['short'],loc='left',fontdict={'fontsize':14, 'fontweight':'bold'})
    ax[j][i].set_xticklabels('');ax[j][i].set_yticklabels('');ax[j][i].set_aspect(1)    
    if i>=1: j+=1; i=-1;        
    i+=1

#control colorbar
cb = fig.colorbar(p, ax=ax.ravel().tolist())
cb.set_label(label="WT amplitude (m)",size=18)
fig.canvas.draw(); cb.ax.set_yticklabels([a.get_text() for a in cb.ax.get_yticklabels()],fontsize = 18)
#or:
#~ cbar_ax = fig.add_axes([0.15, 0.05, 0.7, 0.05])
#~ fig.colorbar(ax1, cax=cbar_ax,orientation='horizontal',label="WTD (m)")
for outdir in outdirs:
    plt.savefig(os.sep.join([outdir,'WT_amp_%s'%(figname_template)+'.png']),dpi=400,format='png')


"""Plot scatters: WT amplitude vs WTD"""
# find axes ranges
xmax = np.max([simdata['tot_thickness'] for sim, simdata in sims.items()])
ymax = xmax/2
fig,ax = plt.subplots(int(np.ceil(len(sims)/2)),2,figsize = [7,7])
i=0
j=0
for sim, simdata in sims.items(): 
    amp = (simdata['Dm'].succion[:,0,:,:].max(dim='time') - simdata['Dm'].succion[:,0,:,:].min(dim='time'))
    depth = (simdata['tot_thickness'] - simdata['Dm'].succion[:,0,:,:].mean(dim='time'))
    l1=ax[j][i].scatter(depth.where(simdata['Dm'].veg_map==7,np.nan),amp.where(simdata['Dm'].veg_map==7,np.nan),c='g',s=0.1)#'Trees'
    #~ l2=ax[j][i].scatter(depth.where(simdata['Dm'].veg_map==8,np.nan),amp.where(simdata['Dm'].veg_map==8,np.nan),c='y',s=0.1)#'Open Savanna'
    l2=ax[j][i].scatter(depth.where(simdata['Dm'].veg_map==19,np.nan),amp.where(simdata['Dm'].veg_map==19,np.nan),c='r',s=0.1)#'Herbaceous'
    ax[j][i].set_xlim([0,xmax])
    ax[j][i].set_ylim([0,ymax])
    #~ ax[j][i].legend(['Trees','Open Savannah','Herbaceous'])
    ax[j][i].legend(['Trees','Herbaceous'])
    ax[j][i].set_xlabel('WTD (m)',fontsize=14)
    ax[j][i].set_ylabel('amplitude (m)',fontsize=14)
    ax[j][i].set_title(simdata['props']['short'],loc='left',fontdict={'fontsize':14, 'fontweight':'bold'})
    
    if i>=1: j+=1; i=-1;        
    i+=1

plt.gcf().subplots_adjust(bottom=0.07, top =0.95,left=0.07,right=0.98,hspace=0.3,wspace=0.2)
for outdir in outdirs:
    plt.savefig(os.sep.join([outdir,'scat WT_amp_vs_WTD_%s'%(figname_template)+'.png']),dpi=400,format='png')


fig,ax = plt.subplots(1,1,figsize = [7,7])
for sim, simdata in sims.items(): 
    (simdata['tot_thickness'] - simdata['Dm'].succion[:,0,:,:].mean(dim='time')).plot.hist(ax=ax,bins=np.arange(0,5,0.2),density=True,histtype='step',label=simdata['props']['simuname'])   

    plt.legend()
    ax.set_xlabel('WTD (m)',fontsize=14)
    ax.set_ylabel('frequency',fontsize=14)

for outdir in outdirs:
    plt.savefig(os.sep.join([outdir,'hist_WTD_%s'%(figname_template)+'.png']),dpi=400,format='png')

"""Plot hists amplitude"""

fig,ax = plt.subplots(1,1,figsize = [7,7])

for sim, simdata in sims.items(): 
    (simdata['Dm'].succion[:,0,:,:].max(dim='time') - simdata['Dm'].succion[:,0,:,:].min(dim='time')).plot.hist(ax=ax,bins=np.arange(0,5,0.2),density=True,histtype='step',label=simdata['props']['simuname'])   

    plt.legend()
    ax.set_xlabel('WT amplitude (m)',fontsize=14)
    ax.set_ylabel('frequency',fontsize=14)

for outdir in outdirs:
    plt.savefig(os.sep.join([outdir,'hist_WTD_%s'%(figname_template)+'.png']),dpi=400,format='png')

""" Read in sim outputs using xarray"""
#~ ET1 = xr.open_dataset(os.sep.join([simdir1,''.join([simuname1,'_qflx_evap_tot.nc'])]),decode_times=False)
#~ tsim = pd.date_range('1/1/2005 00:00:00', periods=len(ET1.time), freq='H')
#~ # for CLM outputs:
#~ ET1 = ET1.assign_coords(time = pd.to_datetime(tsim))

#~ ET2 = xr.open_dataset(os.sep.join([simdir2,''.join([simuname2,'_qflx_evap_tot.nc'])]),decode_times=False)
#~ tsim2 = pd.date_range('1/1/2005 00:00:00', periods=len(ET2.time), freq='H')
#~ ET2 = ET2.assign_coords(time = pd.to_datetime(tsim2))

#~ ET1=ET1.where(P1.mask==1).sel(time=slice('2005-01-01', '2006-12-20'))
#~ ET2=ET2.where(P2.mask==1).sel(time=slice('2005-01-01', '2006-12-20'))

#~ fig,ax = plt.subplots(1,1,figsize = [12,7])
#~ longitude = 0
#~ latitude = 0
#~ (ET1.resample(time='1D').sum().sel(longitude=longitude,latitude=latitude).qflx_evap_tot*3600).plot.line('b',ax=ax)
#~ (ET2.resample(time='1D').sum().sel(longitude=longitude,latitude=latitude).qflx_evap_tot*3600).plot.line('b--',ax=ax)
#~ (ET1.resample(time='1D').sum().mean(dim='longitude').mean(dim='latitude').qflx_evap_tot*3600).plot.line('b',ax=ax)
#~ (ET2.resample(time='1D').sum().mean(dim='longitude').mean(dim='latitude').qflx_evap_tot*3600).plot.line('r',ax=ax)

#~ plt.legend(['reference','new_sim'])
#~ ax.set_ylabel('mm/d')
#~ ax.set_ylim([-2,10])
#~ ax.set_ylim([0,12])







#example:
#~ fig,ax = plt.subplots(1,1,figsize = [7,7])
#~ ds.succion.mean(dim='time').mean(dim='depth').plot(ax=ax)
#~ (ds.succion[:,0,:,:].mean(dim='time')).plot(ax=ax,robust=True)
#~ (offset_for_WTD-ds.succion[:,0,:,:].mean(dim='time')).plot(ax=ax,cmap = 'viridis_r',robust=True, cbar_kwargs={'label':'WTD (m)'})
#~ for station in WTloc.Y.index:
    #~ ax.plot(WTloc.loc[station,'X'],WTloc.loc[station,'Y'],'+','r')
    #~ ax.text(WTloc.loc[station,'X'],WTloc.loc[station,'Y'],station,color ='r')
#~ ax.set_title("")
#~ ax.set_aspect('equal')


#~ filename = r'/home/hectorb/DATA/METEO/AO/ERA5/ERA5_AO.nc'
#~ xds = xarray.open_dataset(filename)
#~ xds_rolled = xds.assign_coords(longitude=(((xds.longitude + 180)%360)-180)).roll(longitude=(xds.dims['longitude'] // 2))
#~ tmp=xds_rolled.groupby('time.year').sum().mean(dim='year')                                                                                                      
#~ tmp.e_0001.sel(longitude = slice(-20,12),latitude = slice(20,4)).plot()           
#~ tmp.tp_0001.sel(longitude = slice(-20,12),latitude = slice(20,4)).plot()           

#~ (tmp.tp_0001.sel(longitude = slice(-20,12),latitude = slice(20,4))*30).plot()           
#~ ((tmp.tp_0001.sel(longitude = slice(-20,12),latitude = slice(20,4))*30) + (tmp.e_0001.sel(longitude = slice(-20,12),latitude = slice(20,4))*30))           

#~ (xds_rolled.tp_0001.sel(longitude = 3, latitude = 10).groupby('time.year').sum()*30).plot() 

#~ (tmp.tp_0001.sel(longitude = slice(-20,12),latitude = slice(20,4))*30).plot()           

#~ DB['P_ERA5'] = DB.apply(lambda x: float(tmp.tp_0001.interp(longitude = x.lon, latitude = x.lat).data*30) if (~np.isnan(x.lon)) &  (~np.isnan(x.lat)) else np.nan ,axis = 1)
#~ DB['PmET_ERA5'] = DB.apply(lambda x: float(tmp.tp_0001.interp(longitude = x.lon, latitude = x.lat).data*30 + tmp.e_0001.interp(longitude = x.lon, latitude = x.lat).data*30) if (~np.isnan(x.lon)) &  (~np.isnan(x.lat)) else np.nan ,axis = 1)



