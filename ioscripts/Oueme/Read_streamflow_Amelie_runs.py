#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    Anaylse PF outputs for Oueme: Streamflow
    V2 = added Donga
    Amelie_runs = V2 + adapted for Amelie's outputs
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os,glob
from netCDF4 import Dataset
import time
import datetime
import pyproj
from osgeo import gdal,ogr,osr
from osgeo.gdalconst import *
import rasterio
import seaborn as sns

from rasterio.mask import mask
from rasterio.plot import plotting_extent
import geopandas as gpd
from shapely.geometry import mapping
from matplotlib.dates import MonthLocator, DateFormatter
import statsmodels.api as sm

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as io
from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
from procsycz import procGeodata_Gdal


plt.close("all")

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/test_2018_04_24'
simuname = 'test'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2017/oueme2D_KH3high4_2016_12_22'
simuname = 'oueme2D_KH3high4'

#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup_AH/oueme_2018_07_23'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_2018_08_30'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_2018_08_29'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_Kapptot_exp30_CIconst_2018_09_04'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_Kapptot_exp30_Dz6_H20036_2018_09_04'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_Kapptot_exp30_Dz6_H1H2036_6yrs_2018_09_07'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_Kapptot_gauss30_Dz6_H20036_2018_09_04'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_2018_08_30'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_1layer_CIvar_2018_09_05'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_1layer_2018_08_30'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_domain18_2018_08_30'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H1H2036_6yrs_sandH2_2018_09_08'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H20036_CI6_2018_09_09'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H1H2036_CI6_6yrs_2018_09_10'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H20036_CI6_2018_09_11'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI6_2018_09_11'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H20036_CI6_6yrs_2018_09_12'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H20036_CI6_H3ret_2018_09_12'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H20036_CI6_H3ret_4yrs_2018_09_13'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H20036_CI6_6yrs_2018_09_13'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI6_5yrs_2018_09_13'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI4_P3_005_2018_09_15'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI4_P3_005_2018_09_15'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI4_P3_01_2018_09_17'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H20036_CI4_P3_01_2018_09_18'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_H336_CI4_P3_01_2018_09_20'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI4_P3_01_H4clay_tmp_2018_09_20'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_H336_CI8_P3_01_2018_09_21'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZA_2018_09_25'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI4_P3_01_anis_2018_09_21'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_H336_CI4_P3_01_H4clay_2018_09_21'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZAZFA_4yrs_2018_09_28'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZA_stat2_2018_09_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZA_2018_09_26'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZA_H30052_2018_09_26'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_H336_CI8_P3_01_anis_2018_09_26'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H2clay_2018_10_30'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H2clayV2_2018_10_31'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H2clayV3_2018_11_20'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H2imp_2018_11_22'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H2imp_deeprt_2018_11_26'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H2imp_deeprt2_2018_12_21'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H2imp_deeprt2_2018_12_21'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/NCout/oueme_ADlike_2019_10_14'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/NCout/oueme_ADlike_2019_10_22'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_2020_02_18'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_AD_2020_02_19'
#~ simdir = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_AD_2020_03_05'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/NCout/ref_2yrs_2020_01_21'

simuname = 'oueme'
#~ simuname = 'oueme_results'
#~ simuname = 'oueme_results_1layer_CIvar'
simuname = 'oueme_results_Kapptot_exp30_CIconst'
#~ simuname = 'oueme_results_Kapptot_exp30_Dz6_H20036'
#~ simuname = 'oueme_results_Kapptot_exp30_Dz6_H1H2036_6yrs'
#~ simuname = 'oueme_results_Kapptot_gauss30_Dz6_H20036'
#~ simuname = 'oueme_results_domain18'
simuname = 'oueme_results_eH1H2036_6yrs_sandH2'
simuname = 'oueme_results_H20036_CI6'
simuname = 'oueme_results_H1H2036_CI6_6yrs'
simuname = 'oueme_results_ZA_H20036_CI6'
simuname = 'oueme_results_ZA_H236_CI6'
simuname = 'oueme_results_H20036_CI6_6yrs'
simuname = 'oueme_results_ZA_H20036_CI6_H3ret'
simuname = 'oueme_results_ZA_H20036_CI6_H3ret_4yrs'
simuname = 'oueme_results_ZA_H20036_CI6_6yrs'
simuname = 'oueme_results_ZA_H236_CI6_5yrs'
simuname = 'oueme_results_ZA_H236_CI4_P3_005'
simuname = 'oueme_results_ZA_H236_CI4_P3_01_H4clay_tmp'
simuname = 'oueme_results_ZA_H236_CI4_P3_01'
simuname = 'oueme_results_ZA_H20036_CI4_P3_01'
simuname = 'oueme_results_ZA_H236_H336_CI4_P3_01'
simuname = 'oueme_results_ZA_H236_CI4_P3_01_H4clay_tmp'
simuname = 'oueme_results_ZA_H236_H336_CI8_P3_01'
#~ simuname = 'oueme_Amelie_results_ZA'
#~ simuname = 'oueme_results_ZA_H236_CI4_P3_01_anis'
#~ simuname = 'oueme_results_ZA_H236_H336_CI4_P3_01_H4clay'
#~ simuname = 'oueme_Amelie_results_ZAZFA_4yrs'
simuname = 'oueme_Amelie_results_ZA_stat2'
#~ simuname = 'oueme_Amelie_results_ZA'
#~ simuname = 'oueme_Amelie_results_ZA_H30052'
#~ simuname = 'oueme_results_ZA_H236_H336_CI8_P3_01_anis'
#~ simuname = 'oueme_results_H2clay'
#~ simuname = 'oueme_results_H2clayV2'
#~ simuname = 'oueme_results_H2clayV3'
#~ simuname = 'oueme_results_H2imp'
#~ simuname = 'oueme_results_H2imp_deeprt2'
#~ simuname = 'oueme_ADlike'
#~ simuname = 'oueme_KWE_AD'
simuname = 'oueme_OF_AD'
#~ simuname = 'oueme_KWE_f3_AD'
#~ simuname = 'ref_2yrs'
plot_yr = 2006
#~ plot_yr = 2005
method = 'W'
#~ method = 'D'
#~ method = 'H'
#~ n_coupled_layers = 5
#~ n_CLM_2Dlayers = 14
scale = True
#~ scale = False

statsDay =True
#~ statsDay =False

"""
CLM_var is a dictionnary where keys are local variable names as in PFCLM
values are lists of string[filename, PFCLM variable name, long name, units]
"""
CLM_var={'surf_press':['','h_top','Surface pressure','m']}
""" add the filename in the dic"""        
for k,val in CLM_var.items():
    CLM_var[k][0]=os.sep.join([simdir,''.join([simuname,'_'+val[1]+'.nc'])])

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def minimalist_xldate_as_datetime(xldate, datemode):
    # datemode: 0 for 1900-based, 1 for 1904-based
    return (
        datetime.datetime(1899, 12, 30)
        + datetime.timedelta(days=xldate + 1462 * datemode)
        )
        
def roundDateTime(shakyDT,nearest_minutes=1):
	"""
	"""
	shakyDT += datetime.timedelta(minutes=nearest_minutes/2)
	shakyDT -= datetime.timedelta(minutes=shakyDT.minute % nearest_minutes,
                         seconds=shakyDT.second,
                         microseconds=shakyDT.microsecond)
	return shakyDT
	

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

#### Read & process simulation outputs:
sim = PFout.PFout(simdir,'oueme')
sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_h_top.nc'])]),filetype='CLM')
sim.X0=np.min(sim.x)
sim.Y0=np.min(sim.y)


"""Read PF outputs"""
start_time = time.time()
with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
    sim.satur_array = ds.variables['saturation'][:]
    #~ topmask=np.swapaxes(ds.variables['top_mask'][:],0,1)
"""MASK is a nx x ny boolean mask of the domain
When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
(as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
"""
#~ MASK=topmask>0 
with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
    sim.press_array = ds.variables['succion'][:] 

"""Read CLM outputs"""
for k,val in CLM_var.items():
    try:
        with Dataset(val[0], 'r') as ds:
            print('read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
            exec('sim.%s=np.swapaxes(ds.variables[\'%s\'][:],0,2)'%(k,val[1]))        
            #~ exec('sim.%s=np.transpose(ds.variables[\'%s\'][:],[2,1,0])'%(k,val[1]))        
    except FileNotFoundError:
            print('CANNOT read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
print("--- %d seconds --- read all PFCLM outputs from NCs" % (time.time() - start_time))

""" Read constant files"""
ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
#watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,1)
sim.mannings_arr = np.zeros((sim.nx,sim.ny))+1e-6
sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,2)
#~ sim.mask_arr = np.transpose(ds.variables['mask'][:],[2,1,0])
sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,1)
sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,1)
#~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))


# TRY !!
#~ sim.slope_x_arr = sim.slope_x_arr[:,::-1]
#~ sim.slope_y_arr = sim.slope_y_arr[:,::-1]
"""Calculate streamflow for each cell"""
start_time = time.time()
print("Calculate streamflow....")
outflow = np.zeros((sim.nx,sim.ny,sim.n_t_clm))


outflow = PFout.calculate_runoff_maps(htop = sim.surf_press,slopex=sim.slope_x_arr,slopey=sim.slope_y_arr,mannings=sim.mannings_arr,
nx = sim.nx,ny = sim.ny,dx = sim.dx, dy = sim.dy, nt =sim.n_t_clm,mask=sim.mask_arr,KWE='UP',large_array=True,epsilon = 1E-7)

#~ outflow = PFout.calculate_runoff_maps(htop = sim.surf_press,slopex=sim.slope_x_arr,slopey=sim.slope_y_arr,mannings=sim.mannings_arr,
#~ nx = sim.nx,ny = sim.ny,dx = sim.dx, dy = sim.dy, nt =sim.n_t_clm,mask=sim.mask_arr,KWE='UP',large_array=False,epsilon = 1E-7)

#~ outflow = PFout.calculate_runoff_maps(htop = sim.surf_press,slopex=sim.slope_x_arr,slopey=sim.slope_y_arr,mannings=sim.mannings_arr,
#~ nx = sim.nx,ny = sim.ny,dx = sim.dx, dy = sim.dy, nt =sim.n_t_clm,mask=sim.mask_arr,KWE='Stan',large_array=True,epsilon = 1E-7)

#if working with large NX NY NT array is doable: 
#~ qx= -np.sign(np.repeat(sim.slope_x_arr[:,:,np.newaxis],sim.n_t_clm,axis=2))*np.abs(np.repeat(sim.slope_x_arr[:,:,np.newaxis],sim.n_t_clm,axis=2))**0.5/np.repeat(sim.mannings_arr[:,:,np.newaxis],sim.n_t_clm,axis=2) * np.maximum(sim.surf_press,np.zeros((sim.nx,sim.ny,sim.n_t_clm)))**(5/3) * sim.dy #Units should be l3/t
#~ qeast= np.maximum(qx[0:(sim.nx-1),:,:],np.zeros((sim.nx-1,sim.ny,sim.n_t_clm))) - np.maximum(-qx[1:sim.nx,:,:],np.zeros((sim.nx-1,sim.ny,sim.n_t_clm))) # 100s for 2 years oueme domain (144x144)
#~ qeast=np.concatenate((-np.maximum(-np.reshape(qx[0,:,:],(1,sim.ny,sim.n_t_clm)),np.zeros((1,sim.ny,sim.n_t_clm))),qeast),axis=0) 
#~ qeast=np.concatenate((qeast,np.maximum(np.reshape(qx[sim.nx-1,:,:],(1,sim.ny,sim.n_t_clm)),np.zeros((1,sim.ny,sim.n_t_clm)))),axis=0) 
#~ qy= -np.sign(np.repeat(sim.slope_y_arr[:,:,np.newaxis],sim.n_t_clm,axis=2))*np.abs(np.repeat(sim.slope_y_arr[:,:,np.newaxis],sim.n_t_clm,axis=2))**0.5/np.repeat(sim.mannings_arr[:,:,np.newaxis],sim.n_t_clm,axis=2) * np.maximum(sim.surf_press,np.zeros((sim.nx,sim.ny,sim.n_t_clm)))**(5/3) * sim.dx #Units should be l3/t
#~ qnorth= np.maximum(qy[:,0:(sim.ny-1),:],np.zeros((sim.nx,sim.ny-1,sim.n_t_clm))) - np.maximum(-qy[:,1:sim.ny,:],np.zeros((sim.nx,sim.ny-1,sim.n_t_clm)))
#~ qnorth=np.concatenate((-np.maximum(-np.reshape(qy[:,0,:],(sim.nx,1,sim.n_t_clm)),np.zeros((sim.nx,1,sim.n_t_clm))),qnorth),axis=1) 
#~ qnorth=np.concatenate((qnorth,np.maximum(np.reshape(qy[:,sim.ny-1,:],(sim.nx,1,sim.n_t_clm)),np.zeros((sim.nx,1,sim.n_t_clm)))),axis=1) 
#~ outflow=np.maximum(qeast[1:sim.nx+1,:,:],np.zeros((sim.nx,sim.ny,sim.n_t_clm))) + np.maximum(-qeast[0:sim.nx,:,:], np.zeros((sim.nx,sim.ny,sim.n_t_clm))) + np.maximum(qnorth[:,1:sim.ny+1,:],np.zeros((sim.nx,sim.ny,sim.n_t_clm))) + np.maximum(-qnorth[:,0:sim.ny,:], np.zeros((sim.nx,sim.ny,sim.n_t_clm)))


#otherwise:
"""

for i in range(sim.n_t_clm):
    ptop = sim.surf_press[:,:,i]
    ptop[ptop<0]=0
    #####
    #Calculate fluxes across east and north faces
    #First the x direction
    #~ qx= -np.sign(sim.slope_x_arr)*np.abs(sim.slope_x_arr)**0.5/sim.mannings_arr * ptop**(5/3) * sim.dy #Units should be l3/t

    #Upwinding to get flux across the east face of cells - based in qx[i] if its positive and qx[i+1] if its negative
    #qeast= pmax(qx[1:(nx-1),],zeros[1:(nx-1),]) - pmax(-qx[2:nx,],zeros[2:nx,])
    #TO CHECK max formulaton (axis=0 ?)
    #~ qeast= np.maximum(qx[0:(sim.nx-1),:],np.zeros((sim.nx-1,sim.ny))) - np.maximum(-qx[1:sim.nx,:],np.zeros((sim.nx-1,sim.ny))) # 100s for 2 years oueme domain (144x144)
    #tmp1 =  qx[0:sim.nx-1,:]
    #tmp2 = -qx[1:sim.nx,:]
    #qeast = tmp1.clip(0) - tmp2.clip(0) # 102s for 2 years oueme domain (144x144)
    #qeast = np.where(tmp1>0,tmp1,0) -np.where(tmp2>0,tmp2,0) #  106s for 2 years oueme domain (144x144)
    
    #adding the left boundary - pressures outside domain are 0 so flux across this boundary only occurs when qx[1] is negative
    #qeast= rbind(-pmax(-qx[1,],0), qeast)
    # TO CHECK: 
    #~ qeast=np.concatenate((-np.maximum(-np.reshape(qx[0,:],(1,sim.ny)),np.zeros((1,sim.ny))),qeast),axis=0) 

    #adding the right boundary - pressures outside domain are 0 so flux across this boundary only occurs when qx[nx] is positive
    #qeast= rbind(qeast, pmax(qx[nx,],0))
    #~ qeast=np.concatenate((qeast,np.maximum(np.reshape(qx[sim.nx-1,:],(1,sim.ny)),np.zeros((1,sim.ny)))),axis=0) 

    #####
    #Next the y direction
    #qy= -sign(slopey)*abs(slopey)^0.5/mannings * ptop^(5/3) * dx #Units should be l3/t
    #~ qy= -np.sign(sim.slope_y_arr)*np.abs(sim.slope_y_arr)**0.5/sim.mannings_arr * ptop**(5/3) * sim.dx #Units should be l3/t

    #Upwinding to get flux across the north face of cells - based in qy[j] if its positive and qy[j+1] if its negative
    #qnorth= pmax(qy[,1:(ny-1)],zeros[,1:(ny-1)]) - pmax(-qy[, 2:ny],zeros[, 2:ny])
    #~ qnorth= np.maximum(qy[:,0:(sim.ny-1)],np.zeros((sim.nx,sim.ny-1))) - np.maximum(-qy[:,1:sim.ny],np.zeros((sim.nx,sim.ny-1)))
    #tmp1 =  qy[:,0:(sim.ny-1)]
    #tmp2 = -qx[:,1:sim.ny]
    #qnorth = tmp1.clip(0) - tmp2.clip(0)
    #qnorth = np.where(tmp1>0,tmp1,0) -np.where(tmp2>0,tmp2,0)
    
    #adding the bottom - pressures outside domain are 0 so flux across this boundary only occurs when qy[1] is negative
    #qnorth= cbind(-pmax(-qy[,1],0), qnorth)
    # TO CHECK: 
    #~ qnorth=np.concatenate((-np.maximum(-np.reshape(qy[:,0],(sim.nx,1)),np.zeros((sim.nx,1))),qnorth),axis=1) 

    #adding the right boundary - pressures outside domain are 0 so flux across this boundary only occurs when qx[nx] is positive
    #qnorth= cbind(qnorth, pmax(qy[,ny],0))
    #~ qnorth=np.concatenate((qnorth,np.maximum(np.reshape(qy[:,sim.ny-1],(sim.nx,1)),np.zeros((sim.nx,1)))),axis=1) 

    #Calculate total outflow
    #Outflow is a postitive qeast[i,j] or qnorth[i,j] or a negative qeast[i-1,j], qnorth[i,j-1]
    #outflow=pmax(qeast[2:(nx+1),],zeros) + pmax(-qeast[1:nx,], zeros) + 
     #     pmax(qnorth[,2:(ny+1)],zeros) + pmax(-qnorth[, 1:ny], zeros)
    #~ outflow[:,:,i]=np.maximum(qeast[1:sim.nx+1,:],np.zeros((sim.nx,sim.ny))) + np.maximum(-qeast[0:sim.nx,:], np.zeros((sim.nx,sim.ny))) + np.maximum(qnorth[:,1:sim.ny+1],np.zeros((sim.nx,sim.ny))) + np.maximum(-qnorth[:,0:sim.ny], np.zeros((sim.nx,sim.ny)))
    #tmp1 = qeast[1:sim.nx+1,:]
    #tmp2 = -qeast[0:sim.nx,:]
    #tmp3 = qnorth[:,1:sim.ny+1]
    #tmp4 = -qnorth[:,0:sim.ny]
    #outflow[:,:,i] =tmp1.clip(0) + tmp2.clip(0) + tmp3.clip(0) + tmp4.clip(0)
    #outflow[:,:,i]  = np.where(tmp1>0,tmp1,0) + np.where(tmp2>0,tmp2,0) + np.where(tmp3>0,tmp3,0) + np.where(tmp4>0,tmp4,0)


    # KWE way
    #ptop = sim.surf_press[:,:,6300]
    #ptop[ptop<0]=0
    #Repeat the slopes on the lower and left boundaries that are inside the domain but outside the mask
    #find indices of all cells that are off the mask but have a neigbor to their right that is on the mask
    #fill.left=which((rbind(mask[2:nx,],rep(0,ny)) - mask[1:nx,]) ==1, arr.ind=T)
    fill_left = np.where((np.concatenate((sim.mask_arr.data[1:sim.nx,:,0],np.zeros((1,sim.ny))),axis=0) - sim.mask_arr.data[:,:,0])==1)                                          
    #get the indices of their neigbors to the right 
    #fill.left2=fill.left
    #fill.left2[,1]=fill.left[,1]+1
    fill_left2 = fill_left
    fill_left2[0][:] = fill_left[0][:]+1
    #pad the slopes to the left with their neigboring cells in the mask 
    #slopex[fill.left]=slopex[fill.left2]
    sim.slope_x_arr[fill_left] = sim.slope_x_arr[fill_left2]
    #find indices of all cells that are off the mask but have a neigbor above them that is on the mask
    #fill.down=which((cbind(mask[,2:ny],rep(0,nx)) - mask[,1:ny]) ==1, arr.ind=T)
    fill_down = np.where((np.concatenate((sim.mask_arr.data[:,1:sim.ny,0],np.zeros((sim.nx,1))),axis=1) - sim.mask_arr.data[:,:,0])==1)                                          
    #get the indices of their neigbors above
    #fill.down2=fill.down
    #fill.down2[,2]=fill.down[,2]+1
    fill_down2 = fill_down
    fill_down2[1][:]= fill_down[1][:]+1
    #pad the slopes to below  with their neigboring cells in the mask 
    sim.slope_y_arr[fill_down]=sim.slope_y_arr[fill_down2]

    ####
    #calculate the slope magnitude
    epsilon = 1E-7
    sfmag=np.where((sim.slope_x_arr**2+sim.slope_y_arr**2)**0.5>epsilon,(sim.slope_x_arr**2+sim.slope_y_arr**2)**0.5,epsilon)
    #~ sfmag=np.where((sim.slope_x_arr*sim.slope_x_arr+sim.slope_y_arr*sim.slope_y_arr)**0.5>epsilon,(sim.slope_x_arr*sim.slope_x_arr+sim.slope_y_arr*sim.slope_y_arr)**0.5,epsilon)


    ###
    # IS THIS NEEDED ? (Basile)
    #For OverlandKinematic slopes are face centered and calculated across the upper and right boundaries
    # (i.e. Z[i+1]-Z[i])
    # For cells on the lower and left boundaries its assumed that the slopes repeat 
    # (i.e. repeating the upper and right face boundary for the lower and left for these border cells)
    #slopex.pad=rbind(slopex[1,], slopex)
    #slopey.pad=cbind(slopey[,1], slopey)

    ####
    # upwind the pressure - Note this is for the north and east face of all cells
    # The slopes are calculated across these boundaries so the upper boundary is included in these 
    # calculations and the lower and righ boundary of the domain will be added later
    #pupwindx=pmax(sign(slopex)*rbind(ptop[2:(nx),], rep(0,ny)),0) +
    #       pmax(-sign(slopex)*ptop[1:nx,], 0 )
    #pupwindy=pmax(sign(slopey) * cbind(ptop[,2:ny], rep(0,nx)),0) +
    #       pmax(-sign(slopey) * ptop[, 1:ny],0)
    pupwindx = np.maximum(np.sign(sim.slope_x_arr)*np.concatenate((ptop[1:sim.nx,:],np.zeros((1,sim.ny))),axis=0),np.zeros((sim.nx,sim.ny))) + np.maximum(-np.sign(sim.slope_x_arr)*ptop[0:sim.nx,:],np.zeros((sim.nx,sim.ny)))
    pupwindy = np.maximum(np.sign(sim.slope_y_arr)*np.concatenate((ptop[:,1:sim.ny],np.zeros((sim.nx,1))),axis=1),np.zeros((sim.nx,sim.ny))) + np.maximum(-np.sign(sim.slope_y_arr)*ptop[:,0:sim.ny],np.zeros((sim.nx,sim.ny)))
    ###
    # Calculate fluxes across east and north faces
    # First the x direction
    #qeast = -slopex/(sfmag^0.5*mannings) * pupwindx^(5/3) *dy #Units should be l3/t
    #qnorth = -slopey/(sfmag^0.5*mannings) * pupwindy^(5/3) *dx #Units should be l3/t
    qeast = -sim.slope_x_arr / ((sfmag**0.5)*sim.mannings_arr) * pupwindx**(5/3) * sim.dy
    qnorth = -sim.slope_y_arr / ((sfmag**0.5)*sim.mannings_arr) * pupwindy**(5/3) * sim.dx
    ###
    #Fix the lower x boundary 
    # Use the slopes of the first column with the pressures for cell i 
    #qleft=-slopex[1,]/(sfmag[1,]^0.5*mannings)* (pmax(sign(slopex[1,])*ptop[1,],0))^(5/3) * dy
    #qeast=rbind(qleft,qeast)
    qleft = -sim.slope_x_arr[0,:]/((sfmag[0,:]**0.5)*sim.mannings_arr[0,:]) * (np.maximum(np.sign(sim.slope_x_arr[0,:])*ptop[0,:],np.zeros((1,sim.ny))))**(5/3) * sim.dy
    qeast = np.concatenate((qleft,qeast),axis=0)
    ###
    # Fix the lower y boundary 
    # Use the slopes of the bottom row with the pressures for cell j
    #qbottom=-slopey[,1]/(sfmag[,1]^0.5*mannings)* (pmax(sign(slopey[,1])*ptop[,1],0))^(5/3) * dx
    #qnorth=cbind(qbottom,qnorth)
    # beware here sfmag[:,0] for instance produce a row vector ! of dimension (144,) and  np.zeros((sim.nx,1)).shape = (144, 1) while np.zeros((sim.nx)).shape = (144,)

    qbottom = -sim.slope_y_arr[:,0]/((sfmag[:,0]**0.5)*sim.mannings_arr[:,0]) * (np.maximum(np.sign(sim.slope_y_arr[:,0])*ptop[:,0],np.zeros((sim.nx))))**(5/3) * sim.dx
    qnorth = np.concatenate((qbottom[:,np.newaxis],qnorth),axis=1)
    
    outflow[:,:,i]=np.maximum(qeast[1:sim.nx+1,:],np.zeros((sim.nx,sim.ny))) + np.maximum(-qeast[0:sim.nx,:], np.zeros((sim.nx,sim.ny))) + np.maximum(qnorth[:,1:sim.ny+1],np.zeros((sim.nx,sim.ny))) + np.maximum(-qnorth[:,0:sim.ny], np.zeros((sim.nx,sim.ny)))
"""

print("--- %d seconds --- Calculate streamflow" % (time.time() - start_time))

"""Get streamflow data Oueme"""
rt_dir = r'/home/hectorb/DATA/streamflow/AMMA_Benin/Donga'
rt_dir = r'/home/hectorb/DATA/streamflow/AMMA_Benin/Oueme'
#~ rt_dir = r'/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/analyse/Data/streamflow/Oueme'
suf_pattern = '.csv'
#~ pre_pattern = 'CL.Run_Od-'
pre_pattern = 'CL.Run_O-'
station_list = {'ARA_PONT':2 ,'DONGA_PONT':3,'DONGA_ROUTE_DE_KOLOKONDE':3}
station_list = {'BORI':1,'COTE_238':1,'IGBOMAKORO':1,'SANI_A_SANI':1,'SARMANGA':1,'TEBOU':1,'WEWE':1,'AFFON_PONT':1,'AGUIMO':1,'AVAL-SANI':1,'BAREROU':1,'BETEROU':1}
station_list= {k: 2 for k, v in station_list.items()}
#first col is time then daily average then hourly

proj = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')

df = pd.DataFrame()
stadic ={}
for stationname,data_column in station_list.items():
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    """m3/s"""
    sta.read_Q(filepattern, data_col = data_column)
    #~ sta.Q = sta.Q*sta.Q.index.to_series().diff().dt.total_seconds().fillna(0)
    """m3/s -> m3/hr"""
    #~ sta.Q = sta.Q*3600.
    sta.Q.dropna(inplace = True)
    #~ sta.Q[np.isnan(sta.Q)] = 500
    sta.Qamount = sta.Q*60*60
    df[stationname]=sta.Q
    print(sta.name)
    filenames = glob.glob(filepattern)    
    sta.read_latlon_from_Qfile(filenames[0])
    sta.x, sta.y = pyproj.transform(geo_system,proj,sta.lon, sta.lat)
    print(sta.lon)
    print(sta.lat)
    print(sta.x)
    print(sta.y)
    stadic[stationname] = sta

"""Get streamflow data: Donga
data is in m3/s for periods of 10mn or 15Mn depending on stations
"""
rt_dir = r'/home/hectorb/DATA/streamflow/AMMA_Benin/Donga'
suf_pattern = '.csv'
pre_pattern = 'CL.Run_Od-'
station_list = {'ARA_PONT':2 ,'DONGA_PONT':3,'DONGA_ROUTE_DE_KOLOKONDE':3}
#first col is time then instantaneous then 10mn average then 15mn average

for stationname,data_column in station_list.items():
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    """m3/s"""
    sta.read_Q(filepattern, data_col = data_column)
    sta.Q.sort_index(inplace = True)
    sta.Q.rename(stationname,inplace = True)
    #~ df3 = pd.concat([df3,sta.Q.dropna()],axis = 1)       
    #~ for yr in np.unique(sta.Q.index.year): #that's to remove years with too many gaps
        #~ if np.isnan(sta.Q.loc[sta.Q.index.year==yr]).sum()>(6*48):
        #~ if np.isnan(sta.Q.loc[(sta.Q.index.year==yr) & (sta.Q.index.month>4) & (sta.Q.index.month<=10)]).sum()>(6*48):
            #~ sta.Q.loc[sta.Q.index.year==yr] = np.nan
    sta.Q.dropna(inplace = True)
    #~ sta.Q[np.isnan(sta.Q)] = 500
    if data_column == 2: sta.Qamount = sta.Q*60*10
    else: sta.Qamount = sta.Q*60*15
    #~ dfamount_yr = pd.concat([dfamount_yr,sta.Qamount.resample('Y').sum()],axis = 1)
    #~ df = pd.concat([df,sta.Q.dropna()],axis = 1)
    #~ df = df.join(sta.Q,how='outer')
    print(sta.name)
    filenames = glob.glob(filepattern)    
    sta.read_latlon_from_Qfile(filenames[0])
    sta.x, sta.y = pyproj.transform(geo_system,proj,sta.lon, sta.lat)    
    stadic[stationname] = sta

""" Get stream network:"""
#~ with rasterio.open("/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/inputs/geometries/Donga/data/Donga_streams_segments_1000m_masked_mean_agreg_D4_pitfilled.tif") as src:
#~ with rasterio.open("/homelocal5/phyrev/_PARFLOW/projects/Donga/active/Donga_1km/input/DEM/Rwatershed/streams.tif") as src:
#~ data,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/inputs/geometries/Oueme/data/Oueme_streams_segments_1000m_masked_mean_agreg_D4_pitfilled.tif",nodata_value = 65535)
data,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/upg_merit_Benin_UTM_1km_masked_thinned_corr.tif",nodata_value = 255)
[lonlon,latlat]=np.meshgrid(londem,latdem)

""" Find the location of data in the PF framework, and calculate simulated Q:"""
#test Basile 03/2020:
lonlon=lonlon-500
latlat=latlat-500


xx_masked = np.round(lonlon)
yy_masked = np.round(latlat)
#~ xx_masked[data == 0] = -99999999
#~ yy_masked[data == 0] = -99999999
xx_masked[data[::-1,:] == 0] = -99999999
yy_masked[data[::-1,:] == 0] = -99999999
#~ xx_masked[data == 255] = -99999999
#~ yy_masked[data == 255] = -99999999
#BH mars 2020:
xx_masked[data[::-1,:] == 255] = -99999999
yy_masked[data[::-1,:] == 255] = -99999999


#xx_masked, yy_masked and data have shapes [ny,nx] and data[0,:] is the lowest latitude (bottom of map)

Qsim = pd.DataFrame()
#~ x = [sim.X0 + sim.dx*x+sim.dx/2 for x in range(sim.nx)]
#~ y = [sim.Y0 + sim.dy*y+sim.dy/2 for y in range(sim.ny)]
x = [sim.X0 + sim.dx*x for x in range(sim.nx)]
y = [sim.Y0 + sim.dy*y for y in range(sim.ny)]


# calculate slopes in KWE formulation
#~ sim.slope_x_arr_bckp = sim.slope_x_arr
#~ sim.slope_y_arr_bckp = sim.slope_y_arr
#~ d,h=io.PFB_read('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Oueme_merit_f3_Flow.Direction.pfb',1)
#~ direction_arr = np.swapaxes(d,2,0) # nx,ny,nz -> nz, ny, nx as required by the following piece of code from Hoang Tran (JMC mail to gmail adress 16/03/2020)
#~ sx = np.zeros((sim.ny,sim.nx))
#~ sy = np.zeros((sim.ny,sim.nx))
#~ slopex_arr = np.swapaxes(sim.slope_x_arr,1,0)
#~ slopey_arr = np.swapaxes(sim.slope_y_arr,1,0)
#~ for jj in range(sim.ny):
	#~ for ii in range(sim.nx):
		#~ tmp_dir = direction_arr[0,jj,ii]
		#~ if tmp_dir == 1:
			#~ sx[jj,ii] = slopex_arr[jj,ii]
			#~ sy[jj,ii] = slopey_arr[jj+1,ii]
		#~ elif tmp_dir == 3:
			#~ sx[jj,ii] = slopex_arr[jj,ii]
			#~ sy[jj,ii] = slopey_arr[jj,ii]
		#~ elif tmp_dir == 2:
			#~ sx[jj,ii] = slopex_arr[jj,ii-1]
			#~ sy[jj,ii] = slopey_arr[jj,ii]
		#~ elif tmp_dir == 4:
			#~ sx[jj,ii] = slopex_arr[jj,ii]
			#~ sy[jj,ii] = slopey_arr[jj,ii]
#~ sim.slope_x_arr = np.swapaxes(sx,1,0)
#~ sim.slope_y_arr = np.swapaxes(sy,1,0)


# also find the draining area for each outlet in the PF framework:
area,head=io.PFB_read('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Oueme_merit_f3_m1_newriv.area.out.pfb',1)

for stationname,sta in stadic.items():
    print(sta.name)
    #find the closest point in the grid:  
    dist_to_river = np.sqrt((xx_masked - sta.x)**2 + (yy_masked - sta.y)**2)
    dist = np.sqrt((np.round(lonlon)- sta.x)**2 + (np.round(latlat) - sta.y)**2)
    # BH: mars 2020:
    if (dist_to_river.min() <= 4*np.sqrt(sim.dx*sim.dx + sim.dy*sim.dy)):
    #~ if (dist_to_river.min() < np.sqrt(sim.dx*sim.dx + sim.dy*sim.dy)):
        ind_min = np.where(dist_to_river == dist_to_river.min())
        print('river cell found')
    else:
        ind_min = np.where(dist == dist.min())   
        print('no river cell found')
    sta.y_sim_ind = ind_min[0]
    sta.x_sim_ind = ind_min[1]    
    sta.x_sim = np.round(lonlon[ind_min])
    sta.y_sim = np.round(latlat[ind_min])
    print(sta.x)
    print(sta.y)
    print(sta.x_sim)
    print(sta.y_sim)    
    # now get the outlet location in the PF domain:
    #~ sim.calculate_runoff_from_CLM(x_outlet = np.where(np.array(x) == sta.x_sim),y_outlet = np.where(np.array(y) == sta.y_sim),nlayers = n_coupled_layers+n_CLM_2Dlayers)
    
    #former
    #~ sim.calculate_runoff_from_press(x_outlet = np.where(np.array(x) == sta.x_sim)[0],y_outlet = np.where(np.array(y) == sta.y_sim)[0])    
    #new: 
    #~ sim.surfQ = outflow[ind_min[0],ind_min[1],:].flatten()
    sim.surfQ = np.swapaxes(outflow,0,1)[ind_min[0],ind_min[1],:].flatten()
    print(np.where(np.array(x) == sta.x_sim))
    print(np.where(np.array(y) == sta.y_sim))
    print(np.shape(sim.surfQ))
    # if m3 => m3/s:
    #~ sim.surfQ = [Q/(60*60) for Q in sim.surfQ ]
    """m3/hr (TO CHECK -> seems ok)"""
    #~ sta.Qsim  = sim.create_panda_series(np.reshape(sim.surfQ,sim.n_t_clm),start_date = "1/1/2007", start_hour = "00:00", freq = "1H",periods = sim.n_t_clm)        
    sta.Qsim  = sim.create_panda_series(np.reshape(sim.surfQ,sim.n_t_clm),start_date = "1/1/2005", start_hour = "00:00", freq = "1H",periods = sim.n_t_clm)        
    sta.Qsim[abs(sta.Qsim)>10e10]=np.nan
    """m3/hr -> m3/s"""
    sta.Qsim = sta.Qsim/3600.0
    Qsim[stationname]=sta.Qsim      
    sta.area = area[sta.x_sim_ind,sta.y_sim_ind]  
    stadic[stationname] = sta


def pdinterpolate(ts, datetime_index):
    """could decide to fill nans !
    but not adapted for calculating scores?
    To be tested"""
    x = pd.concat([ts, pd.Series(index=datetime_index)])
    #~ return x.groupby(x.index).first().sort_index().fillna(method="ffill")[datetime_index]
    return x.groupby(x.index).first().sort_index().interpolate('linear')[datetime_index]
    #~ return x.groupby(x.index).first().sort_index()[datetime_index]


"""
plot station map
#~ https://stackoverflow.com/questions/40899350/python-quiver-and-pcolormesh-not-lining-up-exactly-right     

"""

fig = plt.figure(figsize=(16,8))
ax = fig.add_subplot(111)
ax.set_aspect(1)
[xx,yy]=np.meshgrid(x,y)

""" to check the location with respect to pressure field:"""
#~ p = ax.pcolormesh(xx-sim.dx/2, yy-sim.dy/2, sim.press_array[240,-1,:,:], cmap="viridis", zorder=-1)
""" to check the location, use the stream mask:"""
#~ p = ax.pcolormesh(lonlon-sim.dx/2, latlat-sim.dy/2, data[::-1,:], cmap="viridis", zorder=-1)
#~ cb = plt.colorbar(p)
#~ cb.set_label('P(m)',fontsize=20)
#or if one want to plot a mean floxw map
flow = np.mean(outflow[:,:,8640::],axis=2)
#~ tmp= sim.surf_press
#~ tmp[tmp<0]=0
#~ flow = np.mean(tmp[:,:,8640::],axis=2)
p = ax.pcolormesh(xx-sim.dx/2, yy-sim.dy/2, np.swapaxes(flow,0,1), cmap="viridis", zorder=-1)
#~ p = ax.pcolormesh(xx-sim.dx/2, yy-sim.dy/2, np.swapaxes(flow,0,1), cmap="viridis", zorder=-1)
cb = plt.colorbar(p)
cb.set_label('Q(m3/h)',fontsize=20)
ax.set_xlabel('X utm (m)',fontsize=20)
ax.set_ylabel('Y utm (m)',fontsize=20)

#~ ax.plot([sta.x for stationname,sta in stadic.items()],[sta.y for stationname,sta in stadic.items()],'k+',markersize=12)
#~ ax.plot([sta.x_sim for stationname,sta in stadic.items()],[sta.y_sim for stationname,sta in stadic.items()],'r+',markersize=12)




"""
plot time series
"""
#~ fig,ax =plt.subplots(nrows=len(stadic),figsize=(20,7), squeeze=True)
#~ i=0
#~ for stationname,sta in stadic.items():
    #~ ax[i].plot(sta.Q.loc['2007-1-1':'2007-12-31'].dropna(),'k')
    #~ ax[i].plot(sta.Qsim.loc['2007-1-1':'2007-12-31'],'r')
    #~ ax[i].set_ylabel('$m3/hr')
    #~ ax[i].legend(['%s: obs'%stationname,'%s: sim'%stationname])
    #~ i+=1

fig,ax =plt.subplots(nrows=int(np.ceil(len(stadic)/2)),ncols = 2,figsize=(20,10), squeeze=True)
i=0
j=0


#~ for stationname,sta in stadic.items():
for stationname,sta in sorted(stadic.items(), key=lambda x: x[1].area):
    # interpolation on sim + daily approach:
    obs = sta.Q.dropna().sort_index(axis=0)
    simu = sta.Qsim.dropna().sort_index(axis=0)
    obs = obs.groupby(obs.index).mean()[datetime.datetime(plot_yr,1,1):datetime.datetime(plot_yr,12,31)]
    obs_int=pd.concat([obs,simu],axis=1).interpolate(method='slinear',limit=50)[0]
    obs_intD = obs_int.resample('D').mean()
    simuD = simu.resample('D').mean()[datetime.datetime(plot_yr,1,1):datetime.datetime(plot_yr,12,31)]
    merge=pd.concat([obs_intD.rename('obs'),simuD.rename('sim')],axis=1)
    kept = merge.dropna()
    res = obj.kge(kept['obs'].values, kept['sim'].values, return_all=True)
    print(res)  
        #~ else:
            #~ # Interpolation on the closest available obs - sim couples:
            #~ #simuD = simu.resample('D').asfreq()
            #~ #simuD = dsclm['Q'][dsclm.index.hour==12]
            #~ simu = simu[(simu.index.month>=6) & (simu.index.month<=11)]
            #~ new_dates = obs.index.round('H')
            #~ obs2=obs
            #~ obs2.index = new_dates
            #~ obs2 = obs2.groupby(obs2.index).mean()
            #~ merge=pd.concat([obs2,simu],axis=1)
            #~ #merge=pd.concat([obs2,simuD],axis=1)  
  
    
    
    if i>=int(np.ceil(len(stadic)/2)):
        j=1
        i=0
    ax[i][j].plot(sta.Q.dropna().sort_index(axis=0),'k')    
    #~ ax[i][j].plot(sta.Qsim.dropna().sort_index(axis=0),'r')
    ax[i][j].plot(sta.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'r')
    ax[i][j].plot(obs_intD,'b')
    ax[i][j].plot(obs_intD.resample('W').mean(),'b--')
    
    #~ ax[i].plot(sta.WTsim.loc['2007-1-1':'2007-12-31'],'r')
    if j==0: ax[i][j].set_ylabel('$m^{3}/s$')
    if i==0: ax[i][j].legend(['obs','sim'],fontsize=8,loc='upper right',ncol=2)
    ax[i,j].set_xlim([datetime.datetime(plot_yr,4,1),datetime.datetime(plot_yr,11,1)])
    if scale:
        ax[i,j].set_ylim([0,880])
        #~ ax[i,j].text(datetime.datetime(plot_yr,4,4),750,r'%s: $r^2 = %2.2f$ KGE = %2.2f'%(stationname,res[1]*res[1],res[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})
        ax[i,j].text(datetime.datetime(plot_yr,4,4),620,'%s: $%dkm^2$\n $r^2 = %2.2f$ KGE = %2.2f'%(stationname,sta.area,res[1]*res[1],res[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})
    else:
        #~ ax[i,j].text(datetime.datetime(plot_yr,4,4),ax[i,j].get_ylim()[1]-0.08*ax[i,j].get_ylim()[1],r'%s: $r^2 = %2.2f$ KGE = %2.2f'%(stationname,res[1]*res[1],res[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})
        ax[i,j].text(datetime.datetime(plot_yr,4,4),ax[i,j].get_ylim()[1]-0.16*ax[i,j].get_ylim()[1],'%s: $%dkm^2$\n $r^2 = %2.2f$ KGE = %2.2f'%(stationname,sta.area,res[1]*res[1],res[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})
        
    
    ax[i,j].tick_params(axis='x', which='both', labelbottom=False, labeltop=False)
    if j ==0: ax[i,j].tick_params(axis='y', which='both', labelright=False, labelleft=True)
    if j ==1: ax[i,j].tick_params(axis='y', which='both', labelright=True, labelleft=False)
    if j ==0: ax[i,j].tick_params(axis='both', which='major', bottom=False,top=False,right=False,left=True)
    if j ==1: ax[i,j].tick_params(axis='both', which='major', bottom=False,top=False,right=True,left=False)
    i+=1
    
ax[0,0].tick_params(axis='both', which='both', bottom=False,top=True,right=False,left=True)
ax[0,1].tick_params(axis='both', which='both', bottom=False,top=True,right=True,left=False)
ax[int(np.ceil(len(stadic)/2))-1,0].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)
ax[int(np.ceil(len(stadic)/2))-1,1].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)
plt.gcf().subplots_adjust(bottom=0.05, top =0.95,left=0.2, hspace=0.001,wspace=0.001)

if scale:
    plt.savefig(os.sep.join([simdir,'Figure_compare_Q_tmp_new_calc_revslopestmp2_%s_'%method+np.str(int(plot_yr))+'.png']),dpi=400,format='png')
    #~ plt.savefig(os.sep.join([simdir,'Figure_compare_Q_%s_'%method+np.str(int(plot_yr))+'_new_runoffcalc.png']),dpi=400,format='png')
else:
    plt.savefig(os.sep.join([simdir,'Figure_compare_Q_%s_'%method+np.str(int(plot_yr))+'_unscaled.png']),dpi=400,format='png')




Qdf = pd.DataFrame()
for stationname,sta in stadic.items():
    Qdf = pd.concat([Qdf,sta.Qsim.rename(stationname,inplace = True)],axis = 1)

Qdf.index=pd.to_datetime(Qdf.index)
Qdf.to_csv(os.sep.join([simdir,'simulated_Q.csv']))


"""
Calculate runoff coefficients
"""

#~ corres_station = {'BORI':'Bori','COTE_238':'Cote_238','IGBOMAKORO':'Igbomakoro','SANI_A_SANI':'Sani','SARMANGA':'Sarmanga','TEBOU':'Tebou','WEWE':'Wewe','AFFON_PONT':'Affon_Pont','AGUIMO':'Aguimo','AVAL-SANI':'Aval_Sani','BAREROU':'Barerou','BETEROU':'Beterou','DONGA_PONT':'Donga_Pont','DONGA_ROUTE_DE_KOLOKONDE':'Donga_route_Kolokonde','ARA_PONT':'Ara'}
corres_station = {'BORI':'Bori','COTE_238':'Cote_238','IGBOMAKORO':'Igbomakoro','SANI_A_SANI':'Sani','SARMANGA':'Sarmanga','TEBOU':'Tebou','WEWE':'Wewe','AFFON_PONT':'Affon_Pont','AGUIMO':'Aguimo','AVAL-SANI':'Aval_Sani','BAREROU':'Barerou','BETEROU':'Beterou','DONGA_PONT':'Donga_Pont','DONGA_ROUTE_DE_KOLOKONDE':'Donga_route_Kolokonde','ARA_PONT':'Ara','BOKPEROU':'Bokperou','DONGA_AFFON':'Donga Affon','KOUA':'Koua','NEKETE':'Nekete','OUEME_SUPERIEUR':'Oueme_Superieur'}
rev_corres_station = {v:k for k,v in corres_station.items()}




Kr = pd.read_csv('/home/hectorb/DATA/streamflow/AMMA_Benin/Analysis/Kr_analysis.csv')
Kr.loc[:,Kr.columns[0]].replace(to_replace = 'Donga route Kolokonde', value = 'Donga_route_Kolokonde',inplace = True)
Kr.loc[:,Kr.columns[0]] = Kr.loc[:,Kr.columns[0]].apply(lambda x: rev_corres_station[x])                                                       
Kr = Kr.set_index(Kr.columns[0])
for stationname,sta in stadic.items(): Kr.loc[stationname,'areasim']=sta.area

#~ Kr = pd.concat([Kr,(1000*(Qdf.resample('Y').sum().loc[Qdf.resample('Y').sum().index.year==2006,:]*3600/(Kr.area*1000*1000)).T)],axis = 1)
Kr = pd.concat([Kr,(1000*(Qdf.resample('Y').sum().loc[Qdf.resample('Y').sum().index.year==2006,:]*3600/(Kr.areasim*1000*1000)).T)],axis = 1)
Kr = Kr.rename({Kr.columns[-1]: 'Qsim'},axis = 'columns')


PrecipOu =  pd.read_csv(r'/home/hectorb/DATA/Precipitation/Benin/Oueme/PrecipOu_streamgauges.csv')
PrecipOu = PrecipOu.set_index(PrecipOu.columns[0])
PrecipOu.index = pd.to_datetime(PrecipOu.index,format='%Y-%m-%d %H:%M:%S')
PrecipOu.index.rename('Date',inplace=True)
p_yr = PrecipOu.resample('Y').sum()
p_yr = p_yr.rename(rev_corres_station,axis ='columns')
Kr = pd.concat([Kr,p_yr.loc[p_yr.index.year==2006,:].T],axis = 1)
Kr = Kr.rename({Kr.columns[-1]: 'P2006'},axis = 'columns')
Kr['Krsim'] = Kr.Qsim/Kr.P2006


tmp=Kr.loc[:,['Mean_offset','std_offset','P_mean','Trees_proportionZin20m','Trees_proportion20m','Trees_proportion','Tree_Density','BasFondsSurface','Qsim','Krsim']].dropna(how='any')
tmp=Kr.dropna(how='any')
#~ model = sm.OLS(tmp.Mean_offset,sm.add_constant(tmp.loc[:,'Trees_proportion20m'])).fit()
#~ model = sm.OLS(tmp.Mean_offset,sm.add_constant(tmp.loc[:,'Trees_proportionZin20m'])).fit()
#~ model = sm.OLS(tmp.Mean_offset,sm.add_constant(tmp.loc[:,'BasFondsSurface'])).fit()
#~ model = sm.OLS(tmp.Mean_offset,sm.add_constant(tmp.loc[:,'Trees_proportion20m'])).fit()
model = sm.OLS(tmp.Krsim,sm.add_constant(tmp.loc[:,'Kr'])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
ax.legend(['data: r2 = %2.2f pval = %f'%(model.rsquared,model.pvalues[1]),'fit']) 
#~ plt.savefig('/home/hectorb/ADMIN/PROJETS/WorkInProgress/Q_Oueme/figures/Offset_btw_PmET0onset_Qonset_vs_vegetation20m.png')
#~ plt.savefig('/home/hectorb/ADMIN/PROJETS/WorkInProgress/Q_Oueme/figures/Offset_btw_PmET0onset_Qonset_vs_bas_fonds_surf.png')
plt.savefig(os.sep.join([simdir,'Figure_compare_Krsim_obs_%s_'%np.str(int(plot_yr))+'.png']),dpi=400,format='png')





""" This part is nastier: try to compare Kr & veg:

Try read the vegetation distribution: use a random tif matching the grid to get the metadata 

"""
#~ https://stackoverflow.com/questions/48063038/calculate-raster-landscape-proportion-percentage-within-multiple-overlaping-po

shape_fn = '/home/hectorb/DATA/streamflow/AMMA_Benin/QGIS/sous_bassins_polygonized_UTM_with_stats_slope_statsveg.shp'
#~ raster_fn  = '/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/preproc/DEM/DEM_hydrosheds_3s/dem_hdshds_UTM_334500_478500_987500_1131500_min_agreg.tif'
raster_fn  = '/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/preproc/DEM/DEM_hydrosheds_3s/dem_hdshds_UTM_334500_478500_987500_1131500_min_agreg.tif'
with rasterio.open(raster_fn) as src:
    transf = src.get_transform()
    
""" Save vegetation as tif """    
vmap,hdr = io.PFB_read('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2019/veg_map.pfb',1)
drv = gdal.GetDriverByName("GTiff")
ds = drv.Create("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2019/veg_map.tif", 144,144, 1, gdal.GDT_Float32)
#~ #ds.SetGeoTransform(upper_left_x, x_resolution, x_skew, upper_left_y, y_resolution, y_skew)
ds.SetGeoTransform(transf)
srs = osr.SpatialReference()                 # Establish its coordinate encoding
srs.ImportFromEPSG(32631)                     # This one specifies WGS84 lat long UTM
ds.SetProjection(srs.ExportToWkt() )   # Exports the coordinate system
#ds.SetProjection(ds_tmp.GetProjection())
outBand=ds.GetRasterBand(1)
vmap[np.isnan(vmap)]=-9999.0
outBand.WriteArray(np.swapaxes(vmap[:,::-1,0],1,0)) #checked w Qgis
outBand.SetNoDataValue(-9999.0)
outBand.FlushCache()
ds=None

raster_fn = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2019/veg_map.tif"

write_zero_frequencies = True
show_plot = False

shapefile = gpd.read_file(shape_fn)

# extract the geometries in GeoJSON format
geoms = shapefile.geometry.values # list of shapely geometries
records = shapefile.values
with rasterio.open(raster_fn) as src:
    classes = np.unique(src.read(1))

with rasterio.open(raster_fn) as src:
    print('nodata value:', src.nodata)
    idx_area = 0
    for index, row in shapefile.iterrows():
        upslope_area = row['geometry']
        name = row['nom']
        from shapely.geometry import mapping # transform to GeJSON format
        mapped_geom = [mapping(upslope_area)]
        out_image, out_transform = mask(src, mapped_geom, crop=True)# extract the raster values values within the polygon
        no_data=src.nodata #no data values of the original raster
        #~ data = out_image.data[0] #extract the values of the masked array
        data = np.reshape(out_image.data,[out_image.data.shape[1],out_image.data.shape[2]]) #extract the values of the masked array
        clas = np.extract(data != no_data, data) # extract the row, columns of the valid values
        #~ frequencies, class_limits = np.histogram(clas,bins=classes,range=[range_min, range_max])
        frequencies, class_limits = np.histogram(clas,bins=classes)
        if idx_area == 0:
            veg = pd.DataFrame({str(name): frequencies})
            veg.index = class_limits[:-1]
        else:
            veg[str(name)] = frequencies
        idx_area += 1

veg_sta = {'Affon Pont':'AFFON_PONT', 'Aguimo':"AGUIMO", 'Ara':'ARA', 'Aval Sani':"AVAL_SANI", 'Barerou':"BAREROU", 'Beterou':"BETEROU",
       'Bokperou':"BOKPEROU", 'Bori':'BORI', 'Cote 238':"COTE_238", 'Donga Pont':"DONGA_PONT", 'Donga Affon':'DONGA_AFFON',
       'Donga route Kolokonde':"DONGA_ROUTE_DE_KOLOKONDE", 'Igbomakoro':'IGBOMAKORO', 'Koua':"KOUA", 'Nekete':"NEKETE", 'Sani':"SANI",
       'Sarmanga':"SARMANGA", 'Tebou':'TEBOU', 'Wewe':"WEWE", 'Oueme Superieur':'OUEME_SUPERIEUR'}
veg = veg.rename(columns = veg_sta)

veg_prop = veg.apply(lambda x: x[7.0] / x.sum()).rename('Trees_proportion_sim')
Kr = pd.concat([Kr,veg_prop],axis = 1)
tmp1=Kr.loc[:,['Krsim','Trees_proportion_sim']].dropna(how='any')
tmp2=Kr.loc[:,['Kr','Trees_proportion']].dropna(how='any')
model1 = sm.OLS(tmp1.Krsim,sm.add_constant(tmp1.loc[:,'Trees_proportion_sim'])).fit()
model2 = sm.OLS(tmp2.Kr,sm.add_constant(tmp2.loc[:,'Trees_proportion'])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model1, 1, ax=ax)
sm.graphics.plot_fit(model2, 1, ax=ax)
ax.legend(['sim: r2 = %2.2f pval = %f'%(model1.rsquared,model1.pvalues[1]),'fit']) 
ax.legend(['data: r2 = %2.2f pval = %f'%(model2.rsquared,model2.pvalues[1]),'fit']) 
#~ plt.savefig(os.sep.join([simdir,'Figure_compare_Krsim_obs_%s_'%np.str(int(plot_yr))+'.png']),dpi=400,format='png')

fig, ax = plt.subplots()
ax.scatter(tmp1.Trees_proportion_sim,tmp1.Krsim,c='r')
#~ ax.plot(tmp2.Trees_proportion,tmp2.Kr,'k')



tmp=Kr.loc[:,['Kr','area','Mean_slopes','P_mean','Trees_proportionZin20m','Trees_proportion20m','Trees_proportion','Tree_Density']].dropna(how='any')
model = sm.OLS(tmp.Kr,sm.add_constant(tmp.loc[:,['Trees_proportion20m','area','Mean_slopes']])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
ax.legend(['data: r2 = %2.2f pval = %f'%(model.rsquared,model.pvalues[1]),'fit']) 
#~ plt.savefig('/home/hectorb/ADMIN/PROJETS/WorkInProgress/Q_Oueme/figures/Offset_btw_PmET0onset_Qonset_vs_vegetation20m.png')
#~ plt.sav

