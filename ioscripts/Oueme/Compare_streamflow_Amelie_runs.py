#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    Anaylse PF outputs for Oueme: Streamflow
    V2 = added Donga
    Amelie_runs = V2 + adapted for Amelie's outputs
    COmpare = the reading of the simulation part is put in a function
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os,glob,copy
from netCDF4 import Dataset
import time
import datetime
import warnings
import pyproj
from osgeo import gdal,ogr,osr
from osgeo.gdalconst import *
import rasterio
import seaborn as sns

from rasterio.mask import mask
from rasterio.plot import plotting_extent
import geopandas as gpd
from shapely.geometry import mapping
from matplotlib.dates import MonthLocator, DateFormatter
import statsmodels.api as sm

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as io
from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
from procsycz import procGeodata_Gdal


plt.close("all")
warnings.filterwarnings(action='once')
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

#~ simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_AD_2020_03_05'
#~ simuname1 = 'oueme_KWE_f3_AD'
#~ simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_2020_02_18'
#~ simuname1 = 'oueme_KWE_AD'
#~ simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_m1_nomask_AD_4yrs_2020_04_10'
#~ simuname1 = 'oueme_KWE_f3_m1_nomask_AD_4yrs'
#~ simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_H1lowK_rt85_4yrs_2020_04_20'
#~ simuname1 = 'oueme_KWE_AD_H1lowK_rt85_4yrs'
#~ simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_H3015_2020_03_06'
#~ simuname1 = 'oueme_ZA24_f3_H3015'
#~ simdir1= r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H3015_4yrs_2020_03_14'
#~ simuname1 = 'oueme_ZA24_f3_OF_H3015_4yrs'
#~ simdir1 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H30029_4yrs_2020_03_18'
#~ simuname1 = 'oueme_ZA24_f3_OF_H30029_4yrs'

#FARO
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_2020_03_04'
simuname1 = 'oueme_OF'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_m1_nomask_AD_4yrs_2020_04_11'
#~ simuname1 = 'oueme_OF_f3_m1_nomask_AD_4yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_H1lowK_rt85_4yrs_2020_04_20'
#~ simuname1 = r'oueme_KWE_AD_H1lowK_rt85_4yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_H1lowK_rt10_4yrs_2020_04_25'
#~ simuname1 = r'oueme_KWE_AD_H1lowK_rt10_4yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_2020_04_25'
#~ simuname1 = r'oueme_AD_rt10_nvdz_nclm_rtaherb'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H1lowK_rt10_nvdz_nclm_rtaherb_man3_2020_04_26'
#~ simuname1 = r'oueme_AD_H1lowK_rt10_nvdz_nclm_rtaherb_man3'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_man3_2020_04_27'
#~ simuname1 = r'oueme_AD_rt10_nvdz_nclm_rtaherb_man3'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3_2020_04_29'
#~ simuname1 = r'oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015new_4yrs_2020_04_28'
#~ simuname1 = r'oueme_ZA24_H3015new_4yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowKlown_4yrs_2020_04_30'
#~ simuname1 = r'oueme_ZA24_H3015_H2lowKlown_4yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_H3015_H2thicklowKlownhighP_2020_05_05'
#~ simuname1 = r'oueme_H3015_H2thicklowKlownhighP'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_4yrs_2020_05_02'
#~ simuname1 = r'oueme_ZA24_H3015_rivmaskarea72_4yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea8_6yrs_2020_05_03'
#~ simuname1 = r'oueme_ZA24_H3015_rivmaskarea8_6yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rt6_H3split_rivmaskarea8_4yrs_2020_05_06'
#~ simuname1 = r'oueme_ZA24_H3015_rt6_H3split_rivmaskarea8_4yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea2_6yrs_2020_05_03'
#~ simuname1 = r'oueme_ZA24_H3015_rivmaskarea2_6yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rt6_H1thin_H3split_rivmaskarea8_4varyrs_2020_05_07'
#~ simuname1 = r'oueme_ZA24_H3015_rt6_H1thin_H3split_rivmaskarea8_4varyrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rt6_H1thin2_H3split_rivmaskarea8_4varyrs_2020_05_07'
#~ simuname1 = r'oueme_ZA24_H3015_rt6_H1thin2_H3split_rivmaskarea8_4varyrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rt6_H1thin2_H3split_rivmaskarea72_4varyrs_2020_05_07'
#~ simuname1 = r'oueme_ZA24_H3015_rt6_H1thin2_H3split_rivmaskarea72_4varyrs'
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_10yrs_2020_05_29'
simuname1 = r'H2clay_10yrs'
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015new_4yrs_2020_04_28'
simuname1 = r'oueme_ZA24_H3015new_4yrs'
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_rand_l1000_std066_10yrs_2020_06_02'
simuname1 = r'H2clay_rand_l1000_std066_10yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/rt6_H1thin2_H2clay_H3split_6yrs_2020_05_12'
#~ simuname1 = r'rt6_H1thin2_H2clay_H3split_6yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_HPC_rivmaskarea8_6yrs_2020_05_03'
#~ simuname1 = r'oueme_ZA24_H3015_HPC_rivmaskarea8_6yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_HPC_rivmaskarea8_4yrs_2020_05_05'
#~ simuname1 = r'oueme_ZA24_H3015_HPC_rivmaskarea8_4yrs'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_2020_04_23'
#~ simuname1 = r'oueme_AD'
short1 = 'test2'
#~ short1 = 'OF'
#~ short1 = 'OF_F3_m1_nomask_4yrs'
#~ short1 = 'AD_KWE'
#~ short1 = 'KWE_H1lowK_rt85'
#~ short1 = 'AD_rt10_nvdz_nclm_rtaherb'
#~ short1 = 'AD_H1lowK_man3'
#~ short1 = 'AD_man3'
#~ short1 = 'H3015'
#~ short1 = 'H3015_lowKlown'
#~ short1 = 'H2thicklowKlownhighP'
#~ short1 = 'AD_rt85'
#~ short1 = 'AD_rt10'
#~ short1 = 'KWE'
#~ short1 = 'AD'
#~ short1 = 'H3015 rivmaskarea72_4yrs'
#~ short1 = 'H3015 rivmaskarea8_6yrs'
#~ short1 = 'H3015 rt 6 H3split rivmaskarea8'
#~ short1 = 'H3015 rivmaskarea2_6yrs'
#~ short1 = 'HPC rivmaskarea8_2yrs'
#~ short1 = 'HPC rivmaskarea8_4yrs'
#~ short1 = 'H3015 rt 6 H3split rivmaskarea8'
#~ short1 = 'H3015 rt 6 H1thin H3split rivmaskarea8'
#~ short1 = 'H3015 rt 6 H1thin2 H3split rivmaskarea8'
#~ short1 = 'H3015 rt 6 H1thin2 H3split rivmaskarea72'
#~ short1 = 'H1thin2 mask72'
#~ short1 = 'H2clay'
#~ short1 = 'H3015'
short1 = 'H3015_H236'
#~ short1 = 'H3015 rt6 H1thin2 H2clay H3split'
#~ short1 = 'H2 K00036'
short1 = 'H3015_1km'

# compare OF vs KWE:
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_OFvsKWE/oueme_OF_f3_m1_nomask_AD_4yrs_2020_04_11'
simuname1 = 'oueme_OF_f3_m1_nomask_AD_4yrs'
short1 = 'OF_F3_m1_nomask_4yrs'
# # compare OF vs KWE w secondary slopes
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_OFvsKWE/oueme_OF_f3_AD_2020_03_05'
simuname1 = 'oueme_OF_f3_AD'
short1 = 'OF_f3_AD'
# compare OF vs KWE w more realistic sim:
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_OFvsKWE/oueme_AD_OF_H3lowP_rt10_nvdz_nclm_rtaherb_man3_2020_04_30'
simuname1 = r'oueme_AD_OF_H3lowP_rt10_nvdz_nclm_rtaherb_man3'
short1 = 'OF_AD_best'
# compare mannings:
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_manning/oueme_KWE_f3_m1_nomask_AD_4yrs_2020_04_10'
#~ simuname1 = 'oueme_KWE_f3_m1_nomask_AD_4yrs'
#~ short1 = 'man1e-6'
# compare mannings2:
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_manning/oueme_AD_rt10_nvdz_nclm_rtaherb_2020_04_25'
#~ simuname1 = 'oueme_AD_rt10_nvdz_nclm_rtaherb'
#~ short1 = 'man1e-6'
# compare roots:
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_H2vegroot/oueme_AD_H3lowP_nort_nvdz_nclm_rtaherb_man3_2020_05_05'
simuname1 = 'oueme_AD_H3lowP_nort_nvdz_nclm_rtaherb_man3'
short1 = 'LSM-like exp root'
# compare H1H2H3:
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_H2vegroot/oueme_AD_rt10_nvdz_nclm_rtaherb_man3_2020_04_27'
simuname1 = 'oueme_AD_rt10_nvdz_nclm_rtaherb_man3'
short1 = 'ref'
# compare rivmaskarea:
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/oueme_ZA24_H3015new_4yrs_2020_04_28'
simuname1 = 'oueme_ZA24_H3015new_4yrs'
short1 = 'ref'
# compare rivmaskarea2:
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/rt6_H1thin2_H2clay_H3split_6yrs_2020_05_12'
simuname1 = 'rt6_H1thin2_H2clay_H3split_6yrs'
short1 = 'nomask'
# make a nice one:
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/H2clay2_mask8_2020_05_16'
#~ simuname1 = 'H2clay2_mask8'
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay2_mask8_8yrs_2020_08_09'
simuname1 = 'H2clay2_mask8_8yrs'
#~ short1= 'mask8'
short1= 'mask8_ZIN'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay2_mask8_tappan_8yrs_2020_08_07'
#~ simuname1 = 'H2clay2_mask8_tappan_8yrs'
#~ short1= 'mask8'
# compare C3 C4 with tappan:
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay2_mask8_tappan_8yrs_2020_08_07'
simuname1 = 'H2clay2_mask8_tappan_8yrs'
short1= 'mask8_tapC4'
# compare with tappan LAI 's map:
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H3015_H20002_long_2020_08_27'
simuname1 = 'H3015_H20002_long'
short1= 'H20002_long'

# long simulations:
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H3015_H2clay24prct_long_4yrs_2020_08_29'
simuname1 = 'H3015_H2clay24prct_long_4yrs'
short1= 'H2clay24prct_long_4yrs'

# compare KH2:
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_KH2/rt6_H1thin2_H2clay_H3split_6yrs_2020_05_12'
#~ simuname1 = 'rt6_H1thin2_H2clay_H3split_6yrs'
#~ short1 = '0.00036 (ref)'

#~ simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_AD_2020_03_05'
#~ simuname2 = 'oueme_KWE_f3_AD'
#~ simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_H1lowK_4yrs_2020_04_15'
#~ simuname2 = 'oueme_KWE_AD_H1lowK_4yrs'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_AD_2020_02_19'
simuname2 = 'oueme_OF_AD'
#~ simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_AD_2020_03_05'
#~ simuname2 = 'oueme_OF_f3_AD'
#~ simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_H1lowK_rt85_nof3_4yrs_2020_04_20'
#~ simuname2 = 'oueme_KWE_AD_H1lowK_rt85_nof3_4yrs'
#~ simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_H1lowK_rt85_newriv_4yrs_2020_04_22'
#~ simuname2 = 'oueme_KWE_AD_H1lowK_rt85_newriv_4yrs'
#~ simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_m1_nomask_varman_AD_4yrs_2020_04_13'
#~ simuname2 = 'oueme_KWE_f3_m1_nomask_varman_AD_4yrs'
#~ simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_m1_nomask_highman_AD_4yrs_2020_04_13'
#~ simuname2 = 'oueme_KWE_f3_m1_nomask_highman_AD_4yrs'
#~ simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_H1lowK_4yrs_2020_04_15'
#~ simuname2 = 'oueme_KWE_AD_H1lowK_4yrs'
#~ simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_m1_nomask_AD_4yrs_2020_04_11'
#~ simuname2 = 'oueme_OF_f3_m1_nomask_AD_4yrs'
#~ simdir2= r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_KWE_H3015_4yrs_2020_04_09'
#~ simuname2 = 'oueme_ZA24_f3_KWE_H3015_4yrs'
#~ simdir2= r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H3015_H2lowlow_4yrs_2020_04_15'
#~ simuname2 = 'oueme_ZA24_f3_OF_H3015_H2lowlow_4yrs'
#~ simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H3015_2020_03_10'
#~ simuname2 = 'oueme_ZA24_f3_OF_H3015'
#~ simdir2 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H306_4yrs_2020_03_18'
#~ simuname2 = 'oueme_ZA24_f3_OF_H306_4yrs'

#FARO
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_2020_02_17'
#~ simuname2 = 'oueme_KWE'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_m1_nomask_AD_4yrs_2020_04_10'
#~ simuname2 = 'oueme_KWE_f3_m1_nomask_AD_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_m1_nomask_highman_AD_4yrs_2020_04_13'
#~ simuname2 = 'oueme_KWE_f3_m1_nomask_highman_AD_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_H1lowK_rt10_4yrs_2020_04_25'
#~ simuname2 = r'oueme_KWE_AD_H1lowK_rt10_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H1lowK_rt10_nvdz_nclm_rtaherb_man3_2020_04_26'
#~ simuname2 = r'oueme_AD_H1lowK_rt10_nvdz_nclm_rtaherb_man3'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H1lowK_H2lowP_rt10_nvdz_nclm_rtaherb_man3_2020_04_27'
#~ simuname2 = r'oueme_AD_H1lowK_H2lowP_rt10_nvdz_nclm_rtaherb_man3'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_man3_2020_04_27'
#~ simuname2 = r'oueme_AD_rt10_nvdz_nclm_rtaherb_man3'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3_2020_04_29'
#~ simuname2 = r'oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H3lowP_nort_nvdz_nclm_rtaherb_man3_2020_05_05'
#~ simuname2 = r'oueme_AD_H3lowP_nort_nvdz_nclm_rtaherb_man3'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_OF_H3lowP_rt10_nvdz_nclm_rtaherb_man3_2020_04_30'
#~ simuname2 = r'oueme_AD_OF_H3lowP_rt10_nvdz_nclm_rtaherb_man3'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3_vdz_2020_05_01'
#~ simuname2 = r'oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3_vdz'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_man5_2020_04_27'
#~ simuname2 = r'oueme_AD_rt10_nvdz_nclm_rtaherb_man5'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_varman_2020_04_27'
#~ simuname2 = r'oueme_AD_rt10_nvdz_nclm_rtaherb_varman'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_varman2_2020_04_28'
#~ simuname2 = r'oueme_AD_rt10_nvdz_nclm_rtaherb_varman2'

#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H306new_4yrs_2020_04_28'
#~ simuname2 = r'oueme_ZA24_H306new_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowK_4yrs_2020_04_29'
#~ simuname2 = r'oueme_ZA24_H3015_H2lowK_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowKlown_4yrs_vdz_2020_05_01'
#~ simuname2 = r'oueme_ZA24_H3015_H2lowKlown_4yrs_vdz'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowKlown_4yrs_vdzAD_2020_05_01'
#~ simuname2 = r'oueme_ZA24_H3015_H2lowKlown_4yrs_vdzAD'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowlowKlown_4yrs_2020_05_01'
#~ simuname2 = r'oueme_ZA24_H3015_H2lowlowKlown_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H322_H2lowKlown_4yrs_2020_05_02'
#~ simuname2 = r'oueme_ZA24_H322_H2lowKlown_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_riv_4yrs_2020_05_02'
#~ simuname2 = r'oueme_ZA24_H3015_riv_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_H3015_H2thicklowKlownhighP_2020_05_05'
#~ simuname2 = r'oueme_H3015_H2thicklowKlownhighP'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_H3015_H2thicklowlowKlownhighP_2020_05_05'
#~ simuname2 = r'oueme_H3015_H2thicklowlowKlownhighP'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_4yrs_2020_05_02'
#~ simuname2 = r'oueme_ZA24_H3015_rivmaskarea72_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea72_6yrs_2020_05_03'
#~ simuname2 = r'oueme_ZA24_H3015_rivmaskarea72_6yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea8_6yrs_2020_05_03'
#~ simuname2 = r'oueme_ZA24_H3015_rivmaskarea8_6yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea2_6yrs_2020_05_03'
#~ simuname2 = r'oueme_ZA24_H3015_rivmaskarea2_6yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_HPC_rivmaskarea8_4yrs_2020_05_05'
#~ simuname2 = r'oueme_ZA24_H3015_HPC_rivmaskarea8_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2highret_HPC_rivmaskarea8_4yrs_2020_05_05'
#~ simuname2 = r'oueme_ZA24_H3015_H2highret_HPC_rivmaskarea8_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_HPC_rivmaskarea2_4varyrs_2020_05_05'
#~ simuname2 = r'oueme_ZA24_H3015_HPC_rivmaskarea2_4varyrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rt6_H3split_rivmaskarea8_4yrs_2020_05_06'
#~ simuname2 = r'oueme_ZA24_H3015_rt6_H3split_rivmaskarea8_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rt6_H1thin_H3split_rivmaskarea8_4varyrs_2020_05_07'
#~ simuname2 = r'oueme_ZA24_H3015_rt6_H1thin_H3split_rivmaskarea8_4varyrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rt6_H1thin2_H3split_rivmaskarea8_4varyrs_2020_05_07'
#~ simuname2 = r'oueme_ZA24_H3015_rt6_H1thin2_H3split_rivmaskarea8_4varyrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_mask72_2020_05_15'
#~ simuname2 = r'H2clay_mask72'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay2_mask72_2020_05_16'
#~ simuname2 = r'H2clay2_mask72'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H306_H2clay_10yrs_2020_05_29'
simuname2 = r'H306_H2clay_10yrs'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_ZAZFA_1_10yrs_2020_06_03'
simuname2 = r'H2clay_ZAZFA_1_10yrs'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/rt6_H1thin2_H2clay_H3split_6yrs_2020_05_12'
simuname2 = r'rt6_H1thin2_H2clay_H3split_6yrs'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H306new_4yrs_2020_04_28'
simuname2 = r'oueme_ZA24_H306new_4yrs'
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_H3015_rand_l10000_std066_8yrs_2020_06_08'
simuname2 = r'H2clay_H3015_rand_l10000_std066_8yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rt6_H1thin2_H3split_rivmaskarea72_4varyrs_2020_05_07'
#~ simuname2 = r'oueme_ZA24_H3015_rt6_H1thin2_H3split_rivmaskarea72_4varyrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/rt10_H1thin2_H2thick_H3split_4yrs_2020_05_12'
#~ simuname2 = r'rt10_H1thin2_H2thick_H3split_4yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/rt6_H1thin2_H2clay_H3split_6yrs_2020_05_12'
#~ simuname2 = r'rt6_H1thin2_H2clay_H3split_6yrs'
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/rt6_H1thin2_H2claylowK_H3split_6yrs_2020_05_13'
#~ simuname2 = r'rt6_H1thin2_H2claylowK_H3split_6yrs'

#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_2020_04_25'
#~ simuname2 = r'oueme_AD_rt10_nvdz_nclm_rtaherb'
short2 = 'test'
#~ short2 = 'KWE'
#~ short2 = 'KWE_f3_m1_nomask_4yrs'
#~ short2 = 'AD_KWE_highman'
#~ short2 = 'OF_KWE'
#~ short2 = 'KWE_f3'
#~ short2 = 'AD_KWE_H1lowK'
#~ short2 = 'AD_OF'
#~ short2 = 'AD_vdz'
#~ short2 = 'AD_vdzAD'
#~ short2 = 'AD_no_rt'
#~ short1 = 'KWE_H1lowK_rt85_nof3'
#~ short2 = 'KWE_H1lowK_rt85_newriv'
#~ short2 = 'KWE_H1lowK_rt10'
#~ short2 = 'AD_rt10_nvdz_nclm_rtaherb'
#~ short2 = 'AD_H1lowK_rt10_nvdz_nclm_rtaherb_man3'
#~ short2 = 'AD_H1lowK_H2lowP_rt10_nvdz_nclm_rtaherb_man3'
#~ short2 = 'AD_man3'
#~ short2 = 'AD_man3 H3lowP'
#~ short2 = 'AD_man5'
#~ short2 = 'AD_varman'
#~ short2 = 'AD_man_riv3_slopes8'
#~ short2 = 'H306'
#~ short2 = 'H3015 H2lowK'
#~ short2 = 'H3015 H2lowKlown'
#~ short2 = 'H3015 H2lowKlown_vdz'
#~ short2 = 'H3015 H2lowlowKlown'
#~ short2 = 'H322 H2lowKlown'
#~ short2 = 'H3015 H2thicklowKlownhighP'
#~ short2 = 'H3015 H2thicklowlowKlownhighP'
#~ short2 = 'H3015 riv'
#~ short2 = 'H3015 rivmaskarea72'
#~ short2 = 'H3015 rivmaskarea72_6yrs'
#~ short2 = 'H3015 rivmaskarea8_6yrs'
#~ short2 = 'H3015 rivmaskarea2_6yrs'
#~ short2 = 'H3015 HPC rivmaskarea8_6yrs'
#~ short2 = 'H3015 H2highret HPC rivmaskarea8_4yrs'
#~ short2 = 'HPC rivmaskarea8_4yrs'
#~ short2 = 'HPC rivmaskarea2_4yrs'
#~ short2 = 'H3015 rt 6 H3split rivmaskarea8'
#~ short2 = 'H3015 rt 6 H1thin H3split rivmaskarea8'
#~ short2 = 'H3015 rt 6 H1thin2 H3split rivmaskarea8'
#~ short2 = 'H1thin2 mask8'
#~ short2 = 'H2clay_mask72'
#~ short2 = 'H306'
#~ short2 = 'ZAZFA_1'
#~ short2 = 'H3015_H200036'
short2 = 'H306_H236'
short2 = 'H3015_10km'
#~ short2 = 'H3015 rt 6 H1thin2 H3split rivmaskarea72'
#~ short2 = 'H3015 rt10 H1thin2 H2thick H3split'
#~ short2 = 'H3015 rt6 H1thin2 H2clay H3split'
#~ short2 = 'H2 K0001'

# compare OF vs KWE:
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_OFvsKWE/oueme_KWE_f3_m1_nomask_AD_4yrs_2020_04_10'
simuname2 = 'oueme_KWE_f3_m1_nomask_AD_4yrs'
short2 = 'KWE_f3_m1_nomask_4yrs'
# # compare OF vs KWE w secondary slopes
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_OFvsKWE/oueme_KWE_f3_AD_2020_03_05'
simuname2 = 'oueme_KWE_f3_AD'
short2 = 'KWE_f3_AD'
# compare OF vs KWE w more realistic sim:
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_OFvsKWE/oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3_2020_04_29'
simuname2 = r'oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3'
short2 = 'KWE_AD_best'
# compare mannings:
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_manning/oueme_KWE_f3_m1_nomask_highman_AD_4yrs_2020_04_13'
#~ simuname2 = 'oueme_KWE_f3_m1_nomask_highman_AD_4yrs'
#~ short2 = 'man5e-6'
# compare mannings2:
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_manning/oueme_AD_rt10_nvdz_nclm_rtaherb_man3_2020_04_27'
#~ simuname2 = 'oueme_AD_rt10_nvdz_nclm_rtaherb_man3'
#~ short2 = 'man3e-6'
# compare roots:
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_H2vegroot/oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3_2020_04_29'
simuname2 = 'oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3'
short2 = '10m unif. root'
# compare H1H2H3:
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_H2vegroot/oueme_AD_H1lowK_rt10_nvdz_nclm_rtaherb_man3_2020_04_26'
simuname2 = 'oueme_AD_H1lowK_rt10_nvdz_nclm_rtaherb_man3'
short2 = 'H1lowK'
# compare rivmaskarea:
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/oueme_ZA24_H3015_rivmaskarea72_6yrs_2020_05_03'
simuname2 = 'oueme_ZA24_H3015_rivmaskarea72_6yrs'
short2 = 'mask72'
# compare rivmaskarea:
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/oueme_ZA24_H3015_H2highret_HPC_rivmaskarea8_4yrs_2020_05_05'
simuname2 = 'oueme_ZA24_H3015_H2highret_HPC_rivmaskarea8_4yrs'
short2 = 'HPC_mask8_H2highret'
# compare rivmaskarea2:
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/H2clay2_mask72_2020_05_16'
simuname2 = 'H2clay2_mask72'
short2 = 'mask72'
# make a nice one:
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/H2clay2_mask8_2020_05_16'
simuname2 = 'H2clay2_mask8'
short2= 'mask8'

simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay2_mask8_tappan_8yrs_2020_08_07'
simuname2 = 'H2clay2_mask8_tappan_8yrs'
#~ short2= 'mask8'
short2= 'mask8_TAP'

# compare C3 C4 with tappan:
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/mask8_tappan_C3tree_2020_08_13'
simuname2 = 'mask8_tappan_C3tree'
short2= 'mask8_tapC3'

# compare with tappan LAI 's map:
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H3015_H2clay24prct_long_2020_08_27'
simuname2 = 'H3015_H2clay24prct_long'
short2= 'H2clay24prct_long'

# long simulations:
simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H3015_H2clay10prct_long_2020_09_14'
simuname2 = 'H3015_H2clay10prct_long'
short2= 'H2clay10prct'

# compare KH2:
#~ simdir2 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_KH2/rt6_H1thin2_H2claylowK2_H3split_6yrs_2020_05_14'
#~ simuname2 = 'rt6_H1thin2_H2claylowK2_H3split_6yrs'
#~ short2 = '0.0003'
#~ simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_m1_nomask_AD_2020_04_09'
#~ simuname3 = 'oueme_KWE_f3_m1_nomask_AD'
#~ simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H1lowK_rt5_4yrs_2020_04_16'
#~ simuname3 = 'oueme_AD_H1lowK_rt5_4yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_nosec_AD_2020_03_16'
#~ simuname3 = 'oueme_OF_f3_nosec_AD'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_nosec_AD_2020_03_15'
#~ simuname3 = 'oueme_KWE_f3_nosec_AD'
#~ simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_nosec_AD_2020_03_15'
#~ simuname3 = 'oueme_KWE_f3_nosec_AD'
#~ simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_m1_nomask_AD_2020_04_09'
#~ simuname3 = 'oueme_KWE_f3_m1_nomask_AD'
#~ simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_H3015_2020_03_06'
#~ simuname3 = 'oueme_ZA24_f3_H3015'
#~ simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZAZFA48_f3_OF_H300023_H4023_2020_03_10'
#~ simuname3 = 'oueme_ZAZFA48_f3_OF_H300023_H4023'
#not f3:
#~ simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZAZFA48_H300023_H4023_2020_03_03'
#~ simuname3 = 'oueme_ZAZFA48_H300023_H4023'
#~ simdir3 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H3015_4yrs_2020_03_14'
#~ simuname3 = 'oueme_ZA24_f3_OF_H3015_4yrs'

#FARO
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_nosec_AD_2020_03_16'
simuname3 = 'oueme_OF_f3_nosec_AD'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_m1_nomask_veryhighman_AD_4yrs_2020_04_17'
#~ simuname3 = r'oueme_KWE_f3_m1_nomask_veryhighman_AD_4yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_man5_2020_04_27'
#~ simuname3 = r'oueme_AD_rt10_nvdz_nclm_rtaherb_man5'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea8_6yrs_2020_05_03'
#~ simuname3 = r'oueme_ZA24_H3015_rivmaskarea8_6yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_HPC_rivmaskarea8_4yrs_2020_05_05'
#~ simuname3 = r'oueme_ZA24_H3015_HPC_rivmaskarea8_4yrs'
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/rt6_H1thin2_H2claylowK_H3split_6yrs_2020_05_14'
#~ simuname3 = r'rt6_H1thin2_H2claylowK_H3split_6yrs'
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_mask8_2020_05_15'
simuname3 = r'H2clay_mask8'
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay2_mask8_2020_05_16'
simuname3 = r'H2clay2_mask8'
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H30029_H2clay_10yrs_2020_05_29'
simuname3 = r'H30029_H2clay_10yrs'
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_ZAZFA_10_10yrs_2020_06_03'
simuname3 = r'H2clay_ZAZFA_10_10yrs'
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H30029new_4yrs_2020_04_28'
simuname3 = r'oueme_ZA24_H30029new_4yrs'
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_H306_rand_l1000_std066_8yrs_2020_06_08'
simuname3 = r'H2clay_H306_rand_l1000_std066_8yrs'
#~ short3 = 'AD_KWE_veryhighman'
#~ short3 = 'AD_KWE'
#~ short3 = 'KWE_f3_m1_nomask'
#~ short3 = 'AD_KWE_H1lowK_rt5'
#~ short3 = 'AD_KWE_nosec'
short3 = 'AD_OF_nosec'
#~ short3 = 'AD_man5'
#~ short3 = 'H3015 rivmaskarea8'
#~ short3 = 'HPC rivmaskarea8_4yrs'
short3 = 'H2 K0002'
short3 = 'H2clay_mask8'
short3 = 'H30029'
short3 = 'H30029_H236'
#~ short3 = 'ZAZFA_10'
short3 = 'H306_1km'
# # compare OF vs KWE w secondary slopes
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_OFvsKWE/oueme_OF_f3_nosec_AD_2020_03_16'
simuname3 = 'oueme_OF_f3_nosec_AD'
short3 = 'OF_f3_nosec_AD'
# compare mannings:
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_manning/oueme_KWE_f3_m1_nomask_veryhighman_AD_4yrs_2020_04_17'
simuname3 = 'oueme_KWE_f3_m1_nomask_veryhighman_AD_4yrs'
short3 = 'man1.6e-5'
# compare mannings2:
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_manning/oueme_AD_rt10_nvdz_nclm_rtaherb_man5_2020_04_27'
simuname3 = 'oueme_AD_rt10_nvdz_nclm_rtaherb_man5'
short3 = 'man5e-6'
# compare H1H2H3:
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_H2vegroot/oueme_AD_H1lowK_H2lowP_rt10_nvdz_nclm_rtaherb_man3_2020_04_27'
simuname3 = 'oueme_AD_H1lowK_H2lowP_rt10_nvdz_nclm_rtaherb_man3'
short3 = 'H1lowK H2lowP'
# compare rivmaskarea:
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/oueme_ZA24_H3015_rivmaskarea8_6yrs_2020_05_03'
simuname3 = 'oueme_ZA24_H3015_rivmaskarea8_6yrs'
short3 = 'mask8'
# compare rivmaskarea:
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/oueme_ZA24_H3015_HPC_rivmaskarea8_6yrs_2020_05_03'
simuname3 = 'oueme_ZA24_H3015_HPC_rivmaskarea8_6yrs'
short3 = 'HPC_mask8'
# compare rivmaskarea2:
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/H2clay2_mask8_2020_05_16'
simuname3 = 'H2clay2_mask8'
short3= 'mask8'
# compare with tappan LAI 's map:
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H3015_H20002_2020_08_24'
simuname3 = 'H3015_H20002'
short3= 'H20002'
#long simulations
simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H3015_H20002_long_4yrs_2020_08_29'
simuname3 = 'H3015_H20002_long_4yrs'
short3= 'H20002_long_4yrs'

# compare KH2:
#~ simdir3 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_KH2/rt6_H1thin2_H2claylowK_H3split_6yrs_2020_05_14'
#~ simuname3 = 'rt6_H1thin2_H2claylowK_H3split_6yrs'
#~ short3 = '0.0002'
#~ simdir4 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_AD_2020_03_05'
#~ simuname4 = 'oueme_OF_f3_AD'
#~ simdir4 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H1lowK_rt5_H2low_H3high_4yrs_2020_04_16'
#~ simuname4 = 'oueme_AD_H1lowK_rt5_H2low_H3high_4yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_nosec_AD_2020_03_16'
#~ simuname4 = 'oueme_OF_f3_nosec_AD'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_nosec_AD_2020_03_15'
#~ simuname4 = 'oueme_KWE_f3_nosec_AD'
#~ simdir4 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_H1lowK_rt85_4yrs_2020_04_17'
#~ simuname4 = 'oueme_KWE_AD_H1lowK_rt85_4yrs'
#~ simdir4 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_nosec_AD_2020_03_16'
#~ simuname4 = 'oueme_OF_f3_nosec_AD'
#~ simdir4 = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_OF_f3_m1_nomask_AD_2020_04_08'
#~ simuname4 = 'oueme_OF_f3_m1_nomask_AD'
#~ simdir4= r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZAZFA48_f3_OF_H300023_H4023_2020_03_10'
#~ simuname4 = 'oueme_ZAZFA48_f3_OF_H300023_H4023'
#~ simdir4= r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZAZFA48_f3_OF_H300023_H4023_4yrs_2020_03_16'
#~ simuname4 = 'oueme_ZAZFA48_f3_OF_H300023_H4023_4yrs'
#~ simdir4= r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H315_4yrs_2020_04_07'
#~ simuname4 = 'oueme_ZA24_f3_OF_H315_4yrs'
#~ simdir4= r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_OF_H3015_H2low_4yrs_2020_04_09'
#~ simuname4 = 'oueme_ZA24_f3_OF_H3015_H2low_4yrs'
#~ simdir4= r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_f3_KWE_H3015_4yrs_2020_04_09'
#~ simuname4 = 'oueme_ZA24_f3_KWE_H3015_4yrs'

#FARO
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_nosec_AD_2020_03_15'
simuname4 = 'oueme_KWE_f3_nosec_AD'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_f3_m1_nomask_varman_AD_4yrs_2020_04_13'
#~ simuname4 = 'oueme_KWE_f3_m1_nomask_varman_AD_4yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_rt10_nvdz_nclm_rtaherb_varman_2020_04_27'
#~ simuname4 = r'oueme_AD_rt10_nvdz_nclm_rtaherb_varman'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_rivmaskarea2_6yrs_2020_05_03'
#~ simuname4 = r'oueme_ZA24_H3015_rivmaskarea2_6yrs'
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_HPC_rivmaskarea2_4varyrs_2020_05_05'
#~ simuname4 = r'oueme_ZA24_H3015_HPC_rivmaskarea2_4varyrs'
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/rt6_H1thin2_H2claylowK2_H3split_6yrs_2020_05_14'
simuname4 = r'rt6_H1thin2_H2claylowK2_H3split_6yrs'
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_mask2_2020_05_15'
simuname4 = r'H2clay_mask2'
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay2_mask2_2020_05_16'
simuname4 = r'H2clay2_mask2'
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay2_mask2_6yrs_2020_05_19'
simuname4 = r'H2clay2_mask2_6yrs'
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_ZAZFA_1_8yrs_2020_05_28'
simuname4 = r'H2clay_ZAZFA_1_8yrs'
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_ZAZFA_100_9yrs_2020_06_03'
simuname4 = r'H2clay_ZAZFA_100_9yrs'
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_10yrs_2020_05_29'
simuname4 = r'H2clay_10yrs'
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_H306_rand_l10000_std066_8yrs_2020_06_08'
simuname4 = r'H2clay_H306_rand_l10000_std066_8yrs'
#~ short4 = 'AD_KWE_varman'
#~ short4 = 'AD_KWE'
#~ short4 = 'OF_f3'
#~ short4 = 'AD_KWE_H1lowK_rt5_H2low_H3high'
#~ short4 = 'AD_OF_nosec'
short4 = 'AD_KWE_nosec'
#~ short4 = 'AD_KWE_H1lowK_rt85'
#~ short4 = 'AD_varman'
#~ short4 = 'H3015 rivmaskarea2'
#~ short4 = 'HPC rivmaskarea2_4yrs'
short4 = 'H2 K0003'
short4 = 'H2clay_mask2'
short4 = 'ZAZFA_1'
short4 = 'ZAZFA_100'
short4 = 'H3015'
short4 = 'H306_10km'
# # compare OF vs KWE w secondary slopes
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_OFvsKWE/oueme_KWE_f3_nosec_AD_2020_03_15'
simuname4 = 'oueme_KWE_f3_nosec_AD'
short4 = 'KWE_f3_nosec_AD'
# compare mannings:
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_manning/oueme_KWE_f3_m1_nomask_varman_AD_4yrs_2020_04_13'
simuname4 = 'oueme_KWE_f3_m1_nomask_varman_AD_4yrs'
short4 = 'varman_riv1e-6_slopes1.6e-5'
# compare mannings2:
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_manning/oueme_AD_rt10_nvdz_nclm_rtaherb_varman_2020_04_27'
simuname4 = 'oueme_AD_rt10_nvdz_nclm_rtaherb_varman'
short4 = 'varman_riv1E-6_slop5E-6'
# compare H1H2H3:
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_H2vegroot/oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3_2020_04_29'
simuname4 = 'oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3'
short4 = 'H3lowP'
# compare rivmaskarea:
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/oueme_ZA24_H3015_rivmaskarea2_6yrs_2020_05_03'
simuname4 = 'oueme_ZA24_H3015_rivmaskarea2_6yrs'
short4 = 'mask2'
# compare rivmaskarea:
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/oueme_ZA24_H3015_HPC_rivmaskarea2_4varyrs_2020_05_05'
simuname4 = 'oueme_ZA24_H3015_HPC_rivmaskarea2_4varyrs'
short4 = 'HPC_mask2'
# compare rivmaskarea2:
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_rivmask/H2clay2_mask2_6yrs_2020_05_19'
simuname4 = 'H2clay2_mask2_6yrs'
short4= 'mask2'
# compare with tappan LAI 's map:
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H3015_H20002_2020_08_24'
simuname4 = 'H3015_H20002'
short4= 'H20002'
# long simulations:
simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H3015_H2clay24prct_long_4yrs_2020_08_29'
simuname4 = 'H3015_H2clay24prct_long_4yrs'
short4= 'H2clay24prct_long_4yrs'
# compare KH2:
#~ simdir4 = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/sensexp_KH2/rt6_H1thin2_H2claylowK_H3split_6yrs_2020_05_13'
#~ simuname4 = 'rt6_H1thin2_H2claylowK_H3split_6yrs'
#~ short4 = '0.0001'

nsim=4
nsim=2
if nsim==2:
    legend_ts = ['obs',short1,short2]
    legend_map = [short1,short2,short1,short2]
elif nsim==4:
    legend_ts = ['obs',short1,short2,short3,short4]
    legend_map = [short1,short2,short3,short4]

# select which year:
plot_yr = 2006
#~ plot_yr = 2005
method = 'W'
method = '5D'
#~ method = 'H'
scale = True
scale = False

statsDay =True
#~ statsDay =False

# to simplify the search for station locations
dx=1000
dy=1000


##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def minimalist_xldate_as_datetime(xldate, datemode):
    # datemode: 0 for 1900-based, 1 for 1904-based
    return (
        datetime.datetime(1899, 12, 30)
        + datetime.timedelta(days=xldate + 1462 * datemode)
        )
        
def roundDateTime(shakyDT,nearest_minutes=1):
	"""
	"""
	shakyDT += datetime.timedelta(minutes=nearest_minutes/2)
	shakyDT -= datetime.timedelta(minutes=shakyDT.minute % nearest_minutes,
                         seconds=shakyDT.second,
                         microseconds=shakyDT.microsecond)
	return shakyDT
	

def pdinterpolate(ts, datetime_index):
    """could decide to fill nans !
    but not adapted for calculating scores?
    To be tested"""
    x = pd.concat([ts, pd.Series(index=datetime_index)])
    #~ return x.groupby(x.index).first().sort_index().fillna(method="ffill")[datetime_index]
    return x.groupby(x.index).first().sort_index().interpolate('linear')[datetime_index]
    #~ return x.groupby(x.index).first().sort_index()[datetime_index]

def get_streamflow_simulation_results(simdir,simuname,stadic,slope_mode = 'OF',manning=1e-6,manning_filename='whatever.pfb'):
    """
    get a pd DataFrame with Q for all stations (given in stadic)
    slope mode : 'OF' = 'Overland Flow' classical way of using the slopes straight to get Q
    'KWE' = 'Kinematic Wave Equation' using the flow direction to get the slopes to get Q
    """

    #### Read & process simulation outputs:
    sim = PFout.PFout(simdir,'oueme')
    sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
    sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_h_top.nc'])]),filetype='CLM')
    sim.X0=np.min(sim.x)
    sim.Y0=np.min(sim.y)

    """Read PF outputs"""
    start_time = time.time()
    with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
        sim.satur_array = ds.variables['saturation'][:]
        #~ topmask=np.swapaxes(ds.variables['top_mask'][:],0,1)
    """MASK is a nx x ny boolean mask of the domain
    When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
    (as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
    """
    #~ MASK=topmask>0 
    with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
        sim.press_array = ds.variables['succion'][:] 

    """Read CLM outputs"""
    
    """
    CLM_var is a dictionnary where keys are local variable names as in PFCLM
    values are lists of string[filename, PFCLM variable name, long name, units]
    """
    CLM_var={'surf_press':['','h_top','Surface pressure','m']}
    """ add the filename in the dic"""        
    for k,val in CLM_var.items():
        CLM_var[k][0]=os.sep.join([simdir,''.join([simuname,'_'+val[1]+'.nc'])])
        
    for k,val in CLM_var.items():
        try:
            with Dataset(val[0], 'r') as ds:
                print('read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
                exec('sim.%s=np.swapaxes(ds.variables[\'%s\'][:],0,2)'%(k,val[1]))        
        except FileNotFoundError:
                print('CANNOT read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
    print("--- %d seconds --- read all PFCLM outputs from NCs" % (time.time() - start_time))

    """ Read constant files"""
    ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
    #~ ds=Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r')
    #watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
    sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,1)
    if manning==0:
        sim.mannings_arr = io.PFB_read(manning_filename,0)[:,:,0]
    else:
        sim.mannings_arr = np.zeros((sim.nx,sim.ny))+manning
    sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,2)
    sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
    sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
    sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,1)
    sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,1)
    #~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))
    ds.close()
    #xx_masked, yy_masked and data have shapes [ny,nx] and data[0,:] is the lowest latitude (bottom of map)
    Qsim = pd.DataFrame()
    #~ x = [sim.X0 + sim.dx*x+sim.dx/2 for x in range(sim.nx)]
    #~ y = [sim.Y0 + sim.dy*y+sim.dy/2 for y in range(sim.ny)]
    x = [sim.X0 + sim.dx*x for x in range(sim.nx)]
    y = [sim.Y0 + sim.dy*y for y in range(sim.ny)]
    
    if slope_mode == 'KWE':
        outflow = PFout.calculate_runoff_maps(htop = sim.surf_press,slopex=sim.slope_x_arr,slopey=sim.slope_y_arr,mannings=sim.mannings_arr,
        nx = sim.nx,ny = sim.ny,dx = sim.dx, dy = sim.dy, nt =sim.n_t_clm,mask=sim.mask_arr,KWE='Stan',large_array=True,epsilon = 1E-7)

        #~ # calculate slopes in KWE formulation
        #~ sim.slope_x_arr_bckp = sim.slope_x_arr
        #~ sim.slope_y_arr_bckp = sim.slope_y_arr
        #~ d,h=io.PFB_read('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Oueme_merit_f3_Flow.Direction.pfb',1)
        #~ direction_arr = np.swapaxes(d,2,0) # nx,ny,nz -> nz, ny, nx as required by the following piece of code from Hoang Tran (JMC mail to gmail adress 16/03/2020)
        #~ sx = np.zeros((sim.ny,sim.nx))
        #~ sy = np.zeros((sim.ny,sim.nx))
        #~ slopex_arr = np.swapaxes(sim.slope_x_arr,1,0)
        #~ slopey_arr = np.swapaxes(sim.slope_y_arr,1,0)
        #~ for jj in range(sim.ny):
            #~ for ii in range(sim.nx):
                #~ tmp_dir = direction_arr[0,jj,ii]
                #~ if tmp_dir == 1:
                    #~ sx[jj,ii] = slopex_arr[jj,ii]
                    #~ sy[jj,ii] = slopey_arr[jj+1,ii]
                #~ elif tmp_dir == 3:
                    #~ sx[jj,ii] = slopex_arr[jj,ii]
                    #~ sy[jj,ii] = slopey_arr[jj,ii]
                #~ elif tmp_dir == 2:
                    #~ sx[jj,ii] = slopex_arr[jj,ii-1]
                    #~ sy[jj,ii] = slopey_arr[jj,ii]
                #~ elif tmp_dir == 4:
                    #~ sx[jj,ii] = slopex_arr[jj,ii]
                    #~ sy[jj,ii] = slopey_arr[jj,ii]
        #~ sim.slope_x_arr = np.swapaxes(sx,1,0)
        #~ sim.slope_y_arr = np.swapaxes(sy,1,0)
    else:
       outflow = PFout.calculate_runoff_maps(htop = sim.surf_press,slopex=sim.slope_x_arr,slopey=sim.slope_y_arr,mannings=sim.mannings_arr,
        nx = sim.nx,ny = sim.ny,dx = sim.dx, dy = sim.dy, nt =sim.n_t_clm,mask=sim.mask_arr,KWE='UP',large_array=True,epsilon = 1E-7)
     
    
    for stationname,sta in stadic.items():
        print(sta.name)
        #~ print(sta.x)
        #~ print(sta.y)
        #~ print(sta.x_sim)
        #~ print(sta.y_sim)    
        # now get the outlet location in the PF domain:
        #~ sim.calculate_runoff_from_CLM(x_outlet = np.where(np.array(x) == sta.x_sim),y_outlet = np.where(np.array(y) == sta.y_sim),nlayers = n_coupled_layers+n_CLM_2Dlayers)
        #former
        #~ sim.calculate_runoff_from_press(x_outlet = np.where(np.array(x) == sta.x_sim)[0],y_outlet = np.where(np.array(y) == sta.y_sim)[0])    
        #new
        sim.surfQ = np.swapaxes(outflow,0,1)[sta.y_sim_ind,sta.x_sim_ind,:].flatten()
        #~ print(np.where(np.array(x) == sta.x_sim))
        #~ print(np.where(np.array(y) == sta.y_sim))
        #~ print(np.shape(sim.surfQ))
        # if m3 => m3/s:
        #~ sim.surfQ = [Q/(60*60) for Q in sim.surfQ ]
        """m3/hr (TO CHECK -> seems ok)"""
        #~ sta.Qsim  = sim.create_panda_series(np.reshape(sim.surfQ,sim.n_t_clm),start_date = "1/1/2007", start_hour = "00:00", freq = "1H",periods = sim.n_t_clm)        
        sta.Qsim  = sim.create_panda_series(np.reshape(sim.surfQ,sim.n_t_clm),start_date = "1/1/2005", start_hour = "00:00", freq = "1H",periods = sim.n_t_clm)        
        sta.Qsim[abs(sta.Qsim)>10e10]=np.nan
        """m3/hr -> m3/s"""
        sta.Qsim = sta.Qsim/3600.0
        Qsim[stationname]=sta.Qsim        
        stadic[stationname] = sta
    return sim,stadic,Qsim,x,y, outflow
    
    
def plot_time_series(stadic,plot_yr,method='D',scale=False,scale_free=True,legend=[''],stadic1=0,stadic2=0,stadic3=0,stadic4=0,simuname1='sim1',simuname2='sim2',simuname3='sim3',simuname4='sim4',simdir1='',simdir2='',simdir3='',simdir4='',nsim=2):

    fig,ax =plt.subplots(nrows=int(np.ceil(len(stadic)/2)),ncols = 2,figsize=(20,10), squeeze=True)
    i=0
    j=0

    #~ for stationname,sta in stadic.items():
    for stationname,sta in sorted(stadic.items(), key=lambda x: x[1].area):
        # interpolation on sim + daily approach:
        obs = sta.Q.dropna().sort_index(axis=0)
        obs = obs.groupby(obs.index).mean()[datetime.datetime(plot_yr,1,1):datetime.datetime(plot_yr,12,31)]
        simu1 = stadic1[stationname].Qsim.dropna().sort_index(axis=0)[datetime.datetime(plot_yr,1,1):datetime.datetime(plot_yr,12,31)]
        simu2 = stadic2[stationname].Qsim.dropna().sort_index(axis=0)[datetime.datetime(plot_yr,1,1):datetime.datetime(plot_yr,12,31)]
        if nsim==4:
            simu3 = stadic3[stationname].Qsim.dropna().sort_index(axis=0)[datetime.datetime(plot_yr,1,1):datetime.datetime(plot_yr,12,31)]
            simu4 = stadic4[stationname].Qsim.dropna().sort_index(axis=0)[datetime.datetime(plot_yr,1,1):datetime.datetime(plot_yr,12,31)]
        #~ obs_int=pd.concat([obs,simu1],axis=1).interpolate(method='slinear',limit=50)[0]
        obs_int=pd.concat([obs,simu1],axis=1).interpolate(method='slinear',limit=50)[pd.concat([obs,simu1],axis=1).columns[0]]
        obs_intD = obs_int.resample('D').mean()
        #simu 1: 
        simuD1 = simu1.resample('D').mean()
        merge1=pd.concat([obs_intD.rename('obs'),simuD1.rename('sim')],axis=1)
        kept1 = merge1.dropna()
        res1 = obj.kge(kept1['obs'].values, kept1['sim'].values, return_all=True)
        #simu 2:
        simuD2 = simu2.resample('D').mean()
        merge2=pd.concat([obs_intD.rename('obs'),simuD2.rename('sim')],axis=1)
        kept2 = merge2.dropna()
        res2 = obj.kge(kept2['obs'].values, kept2['sim'].values, return_all=True)
        if nsim==4:
            #simu 3: 
            simuD3 = simu3.resample('D').mean()
            merge3=pd.concat([obs_intD.rename('obs'),simuD3.rename('sim')],axis=1)
            kept3 = merge3.dropna()
            res3 = obj.kge(kept3['obs'].values, kept3['sim'].values, return_all=True)
            #simu 4:
            simuD4 = simu4.resample('D').mean()
            merge4=pd.concat([obs_intD.rename('obs'),simuD4.rename('sim')],axis=1)
            kept4 = merge4.dropna()
            res4 = obj.kge(kept4['obs'].values, kept4['sim'].values, return_all=True)        
        print(stationname)
        print(res1)  
        print(res2)  
            #~ else:
                #~ # Interpolation on the closest available obs - sim couples:
                #~ #simuD = simu.resample('D').asfreq()
                #~ #simuD = dsclm['Q'][dsclm.index.hour==12]
                #~ simu = simu[(simu.index.month>=6) & (simu.index.month<=11)]
                #~ new_dates = obs.index.round('H')
                #~ obs2=obs
                #~ obs2.index = new_dates
                #~ obs2 = obs2.groupby(obs2.index).mean()
                #~ merge=pd.concat([obs2,simu],axis=1)
                #~ #merge=pd.concat([obs2,simuD],axis=1)  
      
        
        
        if i>=int(np.ceil(len(stadic)/2)):
            j=1
            i=0
        
        #4 simulations:
        if nsim==4:
            sta1=stadic1[stationname]
            sta2=stadic2[stationname]
            sta3=stadic3[stationname]
            sta4=stadic4[stationname]
        
        #2 simulations:
        elif nsim==2:
            sta1=stadic1[stationname]
            sta2=stadic2[stationname]
        
        # raw data:
        #~ ax[i][j].plot(sta.Q.dropna().sort_index(axis=0),'k')    
        # data interpolated daily
        #~ ax[i][j].plot(obs_intD,'k')
        # data interpolated weekly
        if method=='H':
            ax[i][j].plot(obs_int.resample(method).mean(),'k')
        else:
            ax[i][j].plot(obs_intD.resample(method).mean(),'k')
            
        # simulation data
        #~ ax[i][j].plot(sta.Qsim.dropna().sort_index(axis=0),'r')
        
        #Add an extra empty inseplot so that the legend item #2 is empty and 'obs' label stays alone (in both nsim=2 and 4)
        ax[i][j].plot([],[],color=(0,0,0,0))
        legend_tmp=copy.copy(legend)
        legend_tmp.insert(1,'   ') 
        
        if nsim==4:
        #4 simulations:
            ax[i][j].plot(sta1.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'r')
            ax[i][j].plot(sta2.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'b')
            ax[i][j].plot(sta3.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'g')
            ax[i][j].plot(sta4.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'c')
        #2 simulations:
        elif nsim==2:
            ax[i][j].plot(sta1.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'r')
            ax[i][j].plot(sta2.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'b')
        
        #~ ax[i].plot(sta.WTsim.loc['2007-1-1':'2007-12-31'],'r')
        if j==0: ax[i][j].set_ylabel('$m^{3}/s$')
        #4 simulations:
        #~ if i==0: ax[i][j].legend(['obs','sim1','sim2'],fontsize=8,loc='upper right',ncol=3)
        #~ if i==0: ax[i][j].legend(['obs','KWE','OF','KWE_f3','OF_f3'],fontsize=10,loc='upper right',ncol=5)
        #~ if i==0: ax[i][j].legend(['obs','KWE_f3','OF_f3','KWE_f3_nosec','OF_f3_nosec'],fontsize=10,loc='upper right',ncol=5)
        #~ if i==0: ax[i][j].legend(['obs','KWE_f3','OF_f3_new','KWE_f3_nosec','OF_f3_nosec_new'],fontsize=10,loc='upper right',ncol=5)
        #~ if i==0: ax[i][j].legend(['obs','KWE_f3','OF_f3','KWE_f3_nomask','OF_f3_nomask'],fontsize=10,loc='upper right',ncol=5)
        #~ if i==0: ax[i][j].legend(['obs','Ks_0029m_h','Ks_06m_h','Ks_015m_h','Ks_2lyr_00023_023'],fontsize=10,loc='upper right',ncol=5)
        #~ if i==0: ax[i][j].legend(['obs','Ks_0029m_h','Ks_06m_h','Ks_015m_h','Ks_15m_h'],fontsize=10,loc='upper right',ncol=5)
        #~ if i==0: ax[i][j].legend(['obs','Ks_0029m_h','Ks_06m_h','Ks_015m_h','Ks_015m_h_H2low'],fontsize=10,loc='upper right',ncol=5)
        #~ if i==0: ax[i][j].legend(['obs','Ks_0029m_h','Ks_06m_h','Ks_015m_h','Ks_015m_h_KWE'],fontsize=10,loc='upper right',ncol=5)
        
        #2 simulations:
        #~ if i==0: ax[i][j].legend(['obs','AD','AH'],fontsize=10,loc='upper right',ncol=5)
        #~ if i==0: ax[i][j].legend(['obs','KWE','KWE_varman'],fontsize=10,loc='upper right',ncol=5)
        #~ if i==0: ax[i][j].legend(['obs','KWE','KWE_highman'],fontsize=10,loc='upper right',ncol=5)
        #~ if i==0: ax[i][j].legend(['obs','AD_4yrs','AD_2yrs'],fontsize=10,loc='upper right',ncol=5)
        #~ if i==0: ax[i][j].legend(['obs','KWE','OF'],fontsize=10,loc='upper right',ncol=5)

        if (i==0) and (j==0): ax[i][j].legend(legend_tmp,fontsize=10,loc='upper right',ncol=3)
        
        ax[i,j].set_xlim([datetime.datetime(plot_yr,4,1),datetime.datetime(plot_yr,11,1)])
        if scale:
            ax[i,j].set_ylim([0,520])
            #~ ax[i,j].set_ylim([0,300])
            #~ ax[i,j].set_ylim([0,220])
            #~ ax[i,j].text(datetime.datetime(plot_yr,4,4),350,'%s: %s: $r^2 = %2.2f$ KGE = %2.2f\n%s: %s: $r^2 = %2.2f$ KGE = %2.2f'%(stationname,simuname1,res1[1]*res1[1],res1[0],stationname,simuname2,res2[1]*res2[1],res2[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})
            ax[i,j].text(datetime.datetime(plot_yr,4,4),270,'%s: $%dkm^2$\n%s: $r^2 = %2.2f$ KGE = %2.2f\n%s: $r^2 = %2.2f$ KGE = %2.2f'%(stationname,sta.area,simuname1,res1[1]*res1[1],res1[0],simuname2,res2[1]*res2[1],res2[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})
            #~ ax[i,j].text(datetime.datetime(plot_yr,4,4),150,'%s: %s: $r^2 = %2.2f$ KGE = %2.2f\n%s: %s: $r^2 = %2.2f$ KGE = %2.2f'%(stationname,simuname1,res1[1]*res1[1],res1[0],stationname,simuname2,res2[1]*res2[1],res2[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})
        elif scale_free:    
			# comment for unscaling "free":
            #~ ax[i,j].text(datetime.datetime(plot_yr,4,4),ax[i,j].get_ylim()[1]-0.24*ax[i,j].get_ylim()[1],'%s: %s: $r^2 = %2.2f$ KGE = %2.2f\n%s: %s: $r^2 = %2.2f$ KGE = %2.2f'%(stationname,simuname1,res1[1]*res1[1],res1[0],stationname,simuname2,res2[1]*res2[1],res2[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})
            if nsim==4:
                ax[i,j].text(datetime.datetime(plot_yr,4,4),ax[i,j].get_ylim()[1]-0.72*ax[i,j].get_ylim()[1],'%s: $%dkm^2$\n%s: $r^2 = %2.2f$ KGE = %2.2f\n%s: $r^2 = %2.2f$ KGE = %2.2f\n%s: $r^2 = %2.2f$ KGE = %2.2f\n%s: $r^2 = %2.2f$ KGE = %2.2f'%(stationname,sta.area,simuname1,res1[1]*res1[1],res1[0],simuname2,res2[1]*res2[1],res2[0],simuname3,res3[1]*res3[1],res3[0],simuname4,res4[1]*res4[1],res4[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})				
            elif nsim==2:
                ax[i,j].text(datetime.datetime(plot_yr,4,4),ax[i,j].get_ylim()[1]-0.41*ax[i,j].get_ylim()[1],'%s: $%dkm^2$\n%s: $r^2 = %2.2f$ KGE = %2.2f\n%s: $r^2 = %2.2f$ KGE = %2.2f'%(stationname,sta.area,simuname1,res1[1]*res1[1],res1[0],simuname2,res2[1]*res2[1],res2[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})
        else:            
            mask = (obs_intD.resample(method).mean().index >=datetime.datetime(plot_yr,4,1)) & (obs_intD.resample(method).mean().index<datetime.datetime(plot_yr,11,1))
            ax[i,j].set_ylim([0,obs_intD.resample(method).mean().loc[mask].max()+0.24*obs_intD.resample(method).mean().loc[mask].max()])
                   
            if nsim==4:
                ax[i,j].text(datetime.datetime(plot_yr,4,4),ax[i,j].get_ylim()[1]-0.72*ax[i,j].get_ylim()[1],'%s: $%dkm^2$\n%s: $r^2 = %2.2f$ KGE = %2.2f\n%s: $r^2 = %2.2f$ KGE = %2.2f\n%s: $r^2 = %2.2f$ KGE = %2.2f\n%s: $r^2 = %2.2f$ KGE = %2.2f'%(stationname,sta.area,simuname1,res1[1]*res1[1],res1[0],simuname2,res2[1]*res2[1],res2[0],simuname3,res3[1]*res3[1],res3[0],simuname4,res4[1]*res4[1],res4[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})
            elif nsim==2:
                #~ ax[i,j].text(datetime.datetime(plot_yr,4,4),ax[i,j].get_ylim()[1]-0.24*ax[i,j].get_ylim()[1],'%s: %s: $r^2 = %2.2f$ KGE = %2.2f\n%s: %s: $r^2 = %2.2f$ KGE = %2.2f'%(stationname,simuname1,res1[1]*res1[1],res1[0],stationname,simuname2,res2[1]*res2[1],res2[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})
                ax[i,j].text(datetime.datetime(plot_yr,4,4),ax[i,j].get_ylim()[1]-0.41*ax[i,j].get_ylim()[1],'%s: $%dkm^2$\n%s: $r^2 = %2.2f$ KGE = %2.2f\n%s: $r^2 = %2.2f$ KGE = %2.2f'%(stationname,sta.area,simuname1,res1[1]*res1[1],res1[0],simuname2,res2[1]*res2[1],res2[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})        
        ax[i,j].tick_params(axis='x', which='both', labelbottom=False, labeltop=False)
        if j ==0: ax[i,j].tick_params(axis='y', which='both', labelright=False, labelleft=True)
        if j ==1: ax[i,j].tick_params(axis='y', which='both', labelright=True, labelleft=False)
        if j ==0: ax[i,j].tick_params(axis='both', which='major', bottom=False,top=False,right=False,left=True)
        if j ==1: ax[i,j].tick_params(axis='both', which='major', bottom=False,top=False,right=True,left=False)
        i+=1
        
    #~ for i in range(2*int(np.ceil(len(stadic)/2))):
        #~ fig.axes[i].invert_yaxis()
        #~ fig.subplots_adjust(bottom=0.03, top =0.98,left=0.1,right =0.9,wspace=0.04, hspace=0.001)
        
    ax[0,0].tick_params(axis='both', which='both', bottom=False,top=True,right=False,left=True)
    ax[0,1].tick_params(axis='both', which='both', bottom=False,top=True,right=True,left=False)
    ax[int(np.ceil(len(stadic)/2))-1,0].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)
    ax[int(np.ceil(len(stadic)/2))-1,1].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)
    plt.gcf().subplots_adjust(bottom=0.05, top =0.95,left=0.2, hspace=0.001,wspace=0.001)

    if nsim==4:
        simuname_template = '4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)
        outdir1 = os.sep.join([simdir1,'compare_'+simuname_template])
        outdir2 = os.sep.join([simdir2,'compare_'+simuname_template])
        outdir3 = os.sep.join([simdir3,'compare_'+simuname_template])
        outdir4 = os.sep.join([simdir4,'compare_'+simuname_template])
        outdirs = [outdir1,outdir2,outdir3,outdir4]
    elif nsim==2:
        simuname_template = '2sims_%s_vs_%s'%(simuname1,simuname2)
        outdir1 = os.sep.join([simdir1,'compare_'+simuname_template])
        outdir2 = os.sep.join([simdir2,'compare_'+simuname_template])
        outdirs = [outdir1,outdir2]
        
    for outdir in outdirs:
        try: os.mkdir(outdir)
        except: print('directory already exist: %s'%outdir)

    for outdir in outdirs:
        if scale:
            plt.savefig(os.sep.join([outdir,'Q_%s_%s_'%(simuname_template,method)+np.str(int(plot_yr))+'.png']),dpi=400,format='png')
        elif scale_free:   
            plt.savefig(os.sep.join([outdir,'Q_%s_%s_'%(simuname_template,method)+np.str(int(plot_yr))+'_unscaled_free.png']),dpi=400,format='png')
        else:
            plt.savefig(os.sep.join([outdir,'Q_%s_%s_'%(simuname_template,method)+np.str(int(plot_yr))+'_unscaled.png']),dpi=400,format='png')
            


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

"""Get streamflow data Oueme"""
rt_dir = r'/home/hectorb/DATA/streamflow/AMMA_Benin/Donga'
rt_dir = r'/home/hectorb/DATA/streamflow/AMMA_Benin/Oueme'
#~ rt_dir = r'/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/analyse/Data/streamflow/Oueme'
suf_pattern = '.csv'
#~ pre_pattern = 'CL.Run_Od-'
pre_pattern = 'CL.Run_O-'
station_list = {'ARA_PONT':2 ,'DONGA_PONT':3,'DONGA_ROUTE_DE_KOLOKONDE':3}
station_list = {'BORI':1,'COTE_238':1,'IGBOMAKORO':1,'SANI_A_SANI':1,'SARMANGA':1,'TEBOU':1,'WEWE':1,'AFFON_PONT':1,'AGUIMO':1,'AVAL-SANI':1,'BAREROU':1,'BETEROU':1}
station_list= {k: 2 for k, v in station_list.items()}
#first col is time then daily average then hourly

proj = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')

df = pd.DataFrame()
stadic ={}
for stationname,data_column in station_list.items():
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    """m3/s"""
    sta.read_Q(filepattern, data_col = data_column)
    #~ sta.Q = sta.Q*sta.Q.index.to_series().diff().dt.total_seconds().fillna(0)
    """m3/s -> m3/hr"""
    #~ sta.Q = sta.Q*3600.
    sta.Q.dropna(inplace = True)
    #~ sta.Q[np.isnan(sta.Q)] = 500
    sta.Qamount = sta.Q*60*60
    df[stationname]=sta.Q
    print(sta.name)
    filenames = glob.glob(filepattern)    
    sta.read_latlon_from_Qfile(filenames[0])
    sta.x, sta.y = pyproj.transform(geo_system,proj,sta.lon, sta.lat)
    print(sta.lon)
    print(sta.lat)
    print(sta.x)
    print(sta.y)
    stadic[stationname] = sta

"""Get streamflow data: Donga
data is in m3/s for periods of 10mn or 15Mn depending on stations
"""
rt_dir = r'/home/hectorb/DATA/streamflow/AMMA_Benin/Donga'
suf_pattern = '.csv'
pre_pattern = 'CL.Run_Od-'
station_list = {'ARA_PONT':2 ,'DONGA_PONT':3,'DONGA_ROUTE_DE_KOLOKONDE':3}
#first col is time then instantaneous then 10mn average then 15mn average

for stationname,data_column in station_list.items():
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    """m3/s"""
    sta.read_Q(filepattern, data_col = data_column)
    sta.Q.sort_index(inplace = True)
    #BAsile: 04/2010: comment the following
    sta.Q.rename(stationname,inplace = True)
    #~ df3 = pd.concat([df3,sta.Q.dropna()],axis = 1)       
    #~ for yr in np.unique(sta.Q.index.year): #that's to remove years with too many gaps
        #~ if np.isnan(sta.Q.loc[sta.Q.index.year==yr]).sum()>(6*48):
        #~ if np.isnan(sta.Q.loc[(sta.Q.index.year==yr) & (sta.Q.index.month>4) & (sta.Q.index.month<=10)]).sum()>(6*48):
            #~ sta.Q.loc[sta.Q.index.year==yr] = np.nan
    sta.Q.dropna(inplace = True)
    #~ sta.Q[np.isnan(sta.Q)] = 500
    if data_column == 2: sta.Qamount = sta.Q*60*10
    else: sta.Qamount = sta.Q*60*15
    #~ dfamount_yr = pd.concat([dfamount_yr,sta.Qamount.resample('Y').sum()],axis = 1)
    #~ df = pd.concat([df,sta.Q.dropna()],axis = 1)
    #~ df = df.join(sta.Q,how='outer')
    print(sta.name)
    filenames = glob.glob(filepattern)    
    sta.read_latlon_from_Qfile(filenames[0])
    sta.x, sta.y = pyproj.transform(geo_system,proj,sta.lon, sta.lat)    
    stadic[stationname] = sta

""" Get stream network:"""
#~ with rasterio.open("/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/inputs/geometries/Donga/data/Donga_streams_segments_1000m_masked_mean_agreg_D4_pitfilled.tif") as src:
#~ with rasterio.open("/homelocal5/phyrev/_PARFLOW/projects/Donga/active/Donga_1km/input/DEM/Rwatershed/streams.tif") as src:
#~ data,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/inputs/geometries/Oueme/data/Oueme_streams_segments_1000m_masked_mean_agreg_D4_pitfilled.tif",nodata_value = 65535)
data,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/upg_merit_Benin_UTM_1km_masked_thinned_corr.tif",nodata_value = 255)
[lonlon,latlat]=np.meshgrid(londem,latdem)

""" Find the location of data in the PF framework, and calculate simulated Q:"""
#test Basile 03/2020:
lonlon=lonlon-500
latlat=latlat-500

xx_masked = np.round(lonlon)
yy_masked = np.round(latlat)
#~ xx_masked[data == 0] = -99999999
#~ yy_masked[data == 0] = -99999999
xx_masked[data[::-1,:] == 0] = -99999999
yy_masked[data[::-1,:] == 0] = -99999999
#~ xx_masked[data == 255] = -99999999
#~ yy_masked[data == 255] = -99999999
#BH mars 2020:
xx_masked[data[::-1,:] == 255] = -99999999
yy_masked[data[::-1,:] == 255] = -99999999

# Get x_sim and y_sim by matching the station location and the simulation grid
# (here the simulation grid is the same as the one for the stream network, which
#further gives the stream cells that have been topographically processed as such")

# also find the draining area for each outlet in the PF framework:
area,head=io.PFB_read('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/DEM/priorityflow_outputs/Oueme_merit_f3_m1_newriv.area.out.pfb',1)

for stationname,sta in stadic.items():
#~ for stationname,sta in sorted(stadic.items(), key=lambda x: x[1].area):
    print(sta.name)
    #find the closest point in the grid:  
    dist_to_river = np.sqrt((xx_masked - sta.x)**2 + (yy_masked - sta.y)**2)
    dist = np.sqrt((np.round(lonlon)- sta.x)**2 + (np.round(latlat) - sta.y)**2)
    # BH: mars 2020:
    if (dist_to_river.min() <= 2*np.sqrt(dx*dx + dy*dy)):
    #~ if (dist_to_river.min() < np.sqrt(sim.dx*sim.dx + sim.dy*sim.dy)):
        ind_min = np.where(dist_to_river == dist_to_river.min())
        print('river cell found')
    else:
        ind_min = np.where(dist == dist.min()) 
        print('no river cell found')
    # yes, here ind_min[0] gives the rows which are y dimensions, which is different to the PF or PFout world. 
    # that's why the outflow array which is nx ny nt will be axe-swapped to retrieve the correct station pixel later using y_sim_ind
    sta.y_sim_ind = ind_min[0]
    sta.x_sim_ind = ind_min[1]
    sta.x_sim = np.round(lonlon[ind_min])
    sta.y_sim = np.round(latlat[ind_min])
    sta.area = area[sta.x_sim_ind,sta.y_sim_ind]
    stadic[stationname] = sta



#~ manning_filename='/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/mannings.pfb'
manning_filename='/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/mannings_riv1_slopes5.pfb'
#~ manning_filename='/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/preproc/mannings_riv3_slopes8.pfb'
                                                                                       


#~ sim1,stadic1,Qsim1,x,y = get_streamflow_simulation_results(simdir1,simuname1,copy.deepcopy(stadic),slope_mode='KWE')
#~ sim1,stadic1,Qsim1,x,y,outflow1 = get_streamflow_simulation_results(simdir1,simuname1,copy.deepcopy(stadic),slope_mode='KWE',manning=1e-6)
sim1,stadic1,Qsim1,x,y,outflow1 = get_streamflow_simulation_results(simdir1,simuname1,copy.deepcopy(stadic),slope_mode='KWE',manning=3e-6)
#~ sim1,stadic1,Qsim1,x,y,outflow1 = get_streamflow_simulation_results(simdir1,simuname1,copy.deepcopy(stadic),slope_mode='OF',manning=1e-6)

#~ sim2,stadic2,Qsim2,x,y,outflow2 = get_streamflow_simulation_results(simdir2,simuname2,copy.deepcopy(stadic),slope_mode='OF')
#~ sim2,stadic2,Qsim2,x,y,outflow2 = get_streamflow_simulation_results(simdir2,simuname2,copy.deepcopy(stadic),slope_mode='OF',manning=1e-6)
#~ sim2,stadic2,Qsim2,x,y,outflow2 = get_streamflow_simulation_results(simdir2,simuname2,copy.deepcopy(stadic),slope_mode='KWE',manning=1e-6)
#~ sim2,stadic2,Qsim2,x,y,outflow2 = get_streamflow_simulation_results(simdir2,simuname2,copy.deepcopy(stadic),slope_mode='KWE',manning=5e-6)
sim2,stadic2,Qsim2,x,y,outflow2 = get_streamflow_simulation_results(simdir2,simuname2,copy.deepcopy(stadic),slope_mode='KWE',manning=3e-6)
#~ sim2,stadic2,Qsim2,x,y,outflow2 = get_streamflow_simulation_results(simdir2,simuname2,copy.deepcopy(stadic),slope_mode='OF',manning=3e-6)
#~ sim2,stadic2,Qsim2,x,y,outflow2 = get_streamflow_simulation_results(simdir2,simuname2,copy.deepcopy(stadic),slope_mode='KWE',manning=5e-6)
#~ sim2,stadic2,Qsim2,x,y,outflow2 = get_streamflow_simulation_results(simdir2,simuname2,copy.deepcopy(stadic),slope_mode='KWE',manning=0,manning_filename= manning_filename)
#~ sim2,stadic2,Qsim2,x,y,outflow2 =sim1,stadic1,Qsim1,x,y,outflow1

if nsim==4:
    #~ sim3,stadic3,Qsim3,x,y,outflow3 = get_streamflow_simulation_results(simdir3,simuname3,copy.deepcopy(stadic),slope_mode='KWE',manning=5e-6)
    sim3,stadic3,Qsim3,x,y,outflow3 = get_streamflow_simulation_results(simdir3,simuname3,copy.deepcopy(stadic),slope_mode='KWE',manning=3e-6)
    #~ sim3,stadic3,Qsim3,x,y,outflow3 = get_streamflow_simulation_results(simdir3,simuname3,copy.deepcopy(stadic),slope_mode='KWE',manning=1.6e-5)
    #~ sim3,stadic3,Qsim3,x,y,outflow3 = get_streamflow_simulation_results(simdir3,simuname3,copy.deepcopy(stadic),slope_mode='KWE',manning=1e-6)
    #~ sim3,stadic3,Qsim3,x,y,outflow3 = get_streamflow_simulation_results(simdir3,simuname3,copy.deepcopy(stadic),slope_mode='KWE')
    #~ sim3,stadic3,Qsim3,x,y,outflow3 = get_streamflow_simulation_results(simdir3,simuname3,copy.deepcopy(stadic),slope_mode='OF')
    #~ sim3,stadic3,Qsim3,x,y,outflow3 = get_streamflow_simulation_results(simdir3,simuname3,copy.deepcopy(stadic),slope_mode='OF',manning=1e-6)
    #~ sim3,stadic3,Qsim3,x,y = sim1,stadic1,Qsim1,x,y


    #~ sim4,stadic4,Qsim4,x,y,outflow4 = get_streamflow_simulation_results(simdir4,simuname4,copy.deepcopy(stadic),slope_mode='KWE',manning=0,manning_filename= manning_filename)
    #~ sim4,stadic4,Qsim4,x,y,outflow4 = get_streamflow_simulation_results(simdir4,simuname4,copy.deepcopy(stadic),slope_mode='OF',manning=1e-6)
    #~ sim4,stadic4,Qsim4,x,y,outflow4 = get_streamflow_simulation_results(simdir4,simuname4,copy.deepcopy(stadic),slope_mode='KWE',manning=1e-6)
    sim4,stadic4,Qsim4,x,y,outflow4 = get_streamflow_simulation_results(simdir4,simuname4,copy.deepcopy(stadic),slope_mode='KWE',manning=3e-6)
    #~ sim4,stadic4,Qsim4,x,y,outflow4 = get_streamflow_simulation_results(simdir4,simuname4,copy.deepcopy(stadic),slope_mode='KWE')
    #~ sim = sim1
    #~ stadic = stadic1
    #~ Qsim = Qsim1
    #~ sim4,stadic4,Qsim4,x,y,outflow4 =sim3,stadic3,Qsim3,x,y,outflow3


"""
plot time series
"""
#~ legend = ['obs','AD_KWE','AD_OF']
#~ legend = ['obs','AD','AD_H1lowK']
#~ legend = ['obs','OF_AH','OF_AH_H2lowK']
#~ legend = ['obs','KWE','KWE_varman']
#~ legend = ['obs','KWE','KWE_highman']

if nsim==2:
    simuname3=simuname1
    simuname4=simuname2
    stadic3=stadic1
    stadic4=stadic2
    simdir3=simdir1
    simdir4=simdir2
    
plot_time_series(stadic,plot_yr=2005,method='H',scale=False,scale_free=True,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2005,method='D',scale=False,scale_free=True,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2005,method='5D',scale=False,scale_free=True,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2005,method='D',scale=False,scale_free=False,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2005,method='5D',scale=False,scale_free=False,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2005,method='D',scale=True,scale_free=False,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2005,method='5D',scale=True,scale_free=False,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)

plot_time_series(stadic,plot_yr=2006,method='H',scale=False,scale_free=True,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2006,method='D',scale=False,scale_free=True,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2006,method='5D',scale=False,scale_free=True,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2006,method='D',scale=False,scale_free=False,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2006,method='5D',scale=False,scale_free=False,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2006,method='D',scale=True,scale_free=False,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2006,method='5D',scale=True,scale_free=False,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)

plot_time_series(stadic,plot_yr=2007,method='H',scale=False,scale_free=True,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2007,method='D',scale=False,scale_free=True,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2007,method='5D',scale=False,scale_free=True,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2007,method='D',scale=False,scale_free=False,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2007,method='5D',scale=False,scale_free=False,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2007,method='D',scale=True,scale_free=False,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2007,method='5D',scale=True,scale_free=False,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)


plot_time_series(stadic,plot_yr=2008,method='H',scale=False,scale_free=True,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2008,method='D',scale=False,scale_free=True,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2008,method='5D',scale=False,scale_free=True,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2008,method='D',scale=False,scale_free=False,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2008,method='5D',scale=False,scale_free=False,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2008,method='D',scale=True,scale_free=False,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)
plot_time_series(stadic,plot_yr=2008,method='5D',scale=True,scale_free=False,legend=legend_ts,stadic1=stadic1,stadic2=stadic2,stadic3=stadic3,stadic4=stadic4,simuname1=simuname1,simuname2=simuname2,simuname3=simuname3,simuname4=simuname4,simdir1=simdir1,simdir2=simdir2,simdir3=simdir3,simdir4=simdir4,nsim=nsim)


"""
plot station map
#~ https://stackoverflow.com/questions/40899350/python-quiver-and-pcolormesh-not-lining-up-exactly-right     

"""
#~ fig = plt.figure(figsize=(16,8))
#~ ax = fig.add_subplot(111)
#~ ax.set_aspect(1)
#~ [xx,yy]=np.meshgrid(x,y)

""" to check the location with respect to pressure field:"""
#p = ax.pcolormesh(xx-sim.dx/2, yy-sim.dy/2, sim.press_array[240,-1,:,:], cmap="viridis", zorder=-1)
""" to check the location, use the stream mask:"""
#~ p = ax.pcolormesh(lonlon-sim.dx/2, latlat-sim.dy/2, data[::-1,:], cmap="viridis", zorder=-1)
#~ cb = plt.colorbar(p)
#~ cb.set_label('P(m)',fontsize=20)
#~ ax.set_xlabel('X utm (m)',fontsize=20)
#~ ax.set_ylabel('Y utm (m)',fontsize=20)

#~ ax.plot([sta.x for stationname,sta in stadic.items()],[sta.y for stationname,sta in stadic.items()],'k+',markersize=12)
#~ ax.plot([sta.x_sim for stationname,sta in stadic.items()],[sta.y_sim for stationname,sta in stadic.items()],'r+',markersize=12)


"""Plot maps"""
#~ fig,ax = plt.subplots(1,2,figsize = [7,7])
fig,ax = plt.subplots(2,2,figsize = [7,7])
for a in ax.flat: a.set_aspect(1)
[xx,yy]=np.meshgrid(x,y)
#4 simulations:
if nsim==4:
    flow1 = np.mean(outflow1[:,:,8640::],axis=2)
    flow2 = np.mean(outflow2[:,:,8640::],axis=2)
    flow3 = np.mean(outflow3[:,:,8640::],axis=2)
    flow4 = np.mean(outflow4[:,:,8640::],axis=2)

# when ontly 2 simulations (1 & 2):
elif nsim==2:
    flow1 = np.mean(outflow1[:,:,8640::],axis=2)
    flow2 = np.mean(outflow2[:,:,8640::],axis=2)
    flow3 = np.mean(outflow1[:,:,8640::],axis=2)
    flow4 = np.mean(outflow2[:,:,8640::],axis=2)


p1 = ax[0][0].pcolormesh(xx-dx/2, yy-dy/2, np.swapaxes(flow1,0,1), cmap="viridis",vmin=0,vmax=300000, zorder=-1)
p2 = ax[0][1].pcolormesh(xx-dx/2, yy-dy/2, np.swapaxes(flow2,0,1), cmap="viridis",vmin=0,vmax=300000, zorder=-1)
p3 = ax[1][0].pcolormesh(xx-dx/2, yy-dy/2, np.swapaxes(flow3,0,1), cmap="viridis",vmin=0,vmax=300000, zorder=-1)
p4 = ax[1][1].pcolormesh(xx-dx/2, yy-dy/2, np.swapaxes(flow4,0,1), cmap="viridis",vmin=0,vmax=300000, zorder=-1)
#~ cb = plt.colorbar(p)
#~ cb.set_label('Q(m3/h)',fontsize=20)

#control colorbar
fig.colorbar(p4, ax=ax.ravel().tolist(),label="Q (m3/h)")
#or:
#~ cbar_ax = fig.add_axes([0.15, 0.05, 0.7, 0.05])
#~ fig.colorbar(ax1, cax=cbar_ax,orientation='horizontal',label="WTD (m)")
#~ for i,a in enumerate(ax.flat): a.set_xticklabels('');a.set_yticklabels('');a.set_title('simulation %d'%i)
for i,a in enumerate(ax.flat): a.set_xticklabels('');a.set_yticklabels('')
#~ ax[0][0].set_title('KWE')
#~ ax[0][1].set_title('OF')
#~ ax[1][0].set_title('KWE_f3')
#~ ax[1][1].set_title('OF_f3')

#~ ax[0][0].set_title('KWE_f3')
#~ ax[0][1].set_title('OF_f3')
#~ ax[1][0].set_title('KWE_f3_nosec')
#~ ax[1][1].set_title('OF_f3_nosec')

#~ ax[0][0].set_title('KWE_f3')
#~ ax[0][1].set_title('OF_f3')
#~ ax[1][0].set_title('KWE_f3_nomask')
#~ ax[1][1].set_title('OF_f3_nomask')

#~ ax[0][0].set_title('KWE_f3_AD')
#~ ax[0][1].set_title('KWE_f3_AH')
#~ ax[1][0].set_title('KWE_f3_AD')
#~ ax[1][1].set_title('KWE_f3_AH')

#~ ax[0][0].set_title('KWE')
#~ ax[0][1].set_title('KWE_varman')
#~ ax[1][0].set_title('KWE')
#~ ax[1][1].set_title('KWE_varman')

#~ ax[0][0].set_title('old_OF')
#~ ax[0][1].set_title('new_KWE')
#~ ax[1][0].set_title('old_OF')
#~ ax[1][1].set_title('new_KWE')

#~ ax[0][0].set_title('OF_AH')
#~ ax[0][1].set_title('OF_AH_H2lowK')
#~ ax[1][0].set_title('OF_AH')
#~ ax[1][1].set_title('OF_AH_H2lowK')

ax[0][0].set_title(legend_map[0])
ax[0][1].set_title(legend_map[1])
ax[1][0].set_title(legend_map[2])
ax[1][1].set_title(legend_map[3])

#~ ax[0][0].set_title('KWE')
#~ ax[0][1].set_title('KWE_highman')
#~ ax[1][0].set_title('KWE')
#~ ax[1][1].set_title('KWE_highman')

#~ ax[0][0].set_title('KWE_f3_AD')
#~ ax[0][1].set_title('OF_f3_AD')
#~ ax[1][0].set_title('KWE_f3_AD')
#~ ax[1][1].set_title('OF_f3_AD')

#~ ax[0][0].set_title('Ks_0029m_h')
#~ ax[0][1].set_title('Ks_06m_h')
#~ ax[1][0].set_title('Ks_015m_h')
#~ ax[1][1].set_title('Ks_2lyr_00023_023')

#~ ax[0][0].set_title('Ks_0029m_h')
#~ ax[0][1].set_title('Ks_06m_h')
#~ ax[1][0].set_title('Ks_015m_h')
#~ ax[1][1].set_title('Ks_015m_h_KsH2_low')

#~ ax[0][0].set_title('Ks_0029m_h')
#~ ax[0][1].set_title('Ks_06m_h')
#~ ax[1][0].set_title('Ks_015m_h')
#~ ax[1][1].set_title('Ks_015m_h_KWE')


#~ ax[0][0].set_title('KWE_f3_ZA')
#~ ax[0][1].set_title('OF_f3_ZA')
#~ ax[1][0].set_title('KWE_ZAZFA')
#~ ax[1][1].set_title('OF_f3_ZAZFA')
#~ plt.gcf().subplots_adjust(bottom=0.05, top =0.95,left=0.15, hspace=0.02,wspace=0.2)


# 4 simulations:
if nsim==4:
    plt.savefig(os.sep.join([simdir1,'Figure_compare_Qmap_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
    plt.savefig(os.sep.join([simdir2,'Figure_compare_Qmap_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
    plt.savefig(os.sep.join([simdir3,'Figure_compare_Qmap_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')
    plt.savefig(os.sep.join([simdir4,'Figure_compare_Qmap_4sims_%s_vs_%s_vs_%s_vs_%s'%(simuname1,simuname2,simuname3,simuname4)+'.png']),dpi=400,format='png')

elif nsim==2:
    # 2 simulations:
    plt.savefig(os.sep.join([simdir1,'Figure_compare_Qmap_2sims_%s_vs_%s'%(simuname1,simuname2)+'.png']),dpi=400,format='png')
    plt.savefig(os.sep.join([simdir2,'Figure_compare_Qmap_2sims_%s_vs_%s'%(simuname1,simuname2)+'.png']),dpi=400,format='png')




"""
fig,ax =plt.subplots(nrows=int(np.ceil(len(stadic)/2)),ncols = 2,figsize=(20,10), squeeze=True)
i=0
j=0

for stationname,sta in stadic.items():
    # interpolation on sim + daily approach:
    obs = sta.Q.dropna().sort_index(axis=0)
    obs = obs.groupby(obs.index).mean()
    simu1 = stadic1[stationname].Qsim.dropna().sort_index(axis=0)
    simu2 = stadic2[stationname].Qsim.dropna().sort_index(axis=0)
    #~ obs_int=pd.concat([obs,simu1],axis=1).interpolate(method='slinear',limit=50)[0]
    obs_int=pd.concat([obs,simu1],axis=1).interpolate(method='slinear',limit=50)[pd.concat([obs,simu1],axis=1).columns[0]]
    obs_intD = obs_int.resample('D').mean()
    #simu 1: 
    simuD1 = simu1.resample('D').mean()
    merge1=pd.concat([obs_intD.rename('obs'),simuD1.rename('sim')],axis=1)
    kept1 = merge1.dropna()
    res1 = obj.kge(kept1['obs'].values, kept1['sim'].values, return_all=True)
    #simu 2:
    simuD2 = simu2.resample('D').mean()
    merge2=pd.concat([obs_intD.rename('obs'),simuD2.rename('sim')],axis=1)
    kept2 = merge2.dropna()
    res2 = obj.kge(kept2['obs'].values, kept2['sim'].values, return_all=True)
    print(stationname)
    print(res1)  
    print(res2)  
        #~ else:
            #~ # Interpolation on the closest available obs - sim couples:
            #~ #simuD = simu.resample('D').asfreq()
            #~ #simuD = dsclm['Q'][dsclm.index.hour==12]
            #~ simu = simu[(simu.index.month>=6) & (simu.index.month<=11)]
            #~ new_dates = obs.index.round('H')
            #~ obs2=obs
            #~ obs2.index = new_dates
            #~ obs2 = obs2.groupby(obs2.index).mean()
            #~ merge=pd.concat([obs2,simu],axis=1)
            #~ #merge=pd.concat([obs2,simuD],axis=1)  
  
    
    
    if i>=int(np.ceil(len(stadic)/2)):
        j=1
        i=0
    
    #4 simulations:
    #~ sta1=stadic1[stationname]
    #~ sta2=stadic2[stationname]
    #~ sta3=stadic3[stationname]
    #~ sta4=stadic4[stationname]
    
    #2 simulations:
    sta1=stadic1[stationname]
    sta2=stadic2[stationname]
    
    # raw data:
    #~ ax[i][j].plot(sta.Q.dropna().sort_index(axis=0),'k')    
    # data interpolated daily
    #~ ax[i][j].plot(obs_intD,'k')
    # data interpolated weekly
    ax[i][j].plot(obs_intD.resample(method).mean(),'k')
    # simulation data
    #~ ax[i][j].plot(sta.Qsim.dropna().sort_index(axis=0),'r')
    
    #4 simulations:
    #~ ax[i][j].plot(sta1.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'r')
    #~ ax[i][j].plot(sta2.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'b')
    #~ ax[i][j].plot(sta3.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'r--')
    #~ ax[i][j].plot(sta4.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'b--')
    #2 simulations:
    ax[i][j].plot(sta1.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'r')
    ax[i][j].plot(sta2.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'b')
    
    #~ ax[i].plot(sta.WTsim.loc['2007-1-1':'2007-12-31'],'r')
    if j==0: ax[i][j].set_ylabel('$m^{3}/s$')
    #4 simulations:
    #~ if i==0: ax[i][j].legend(['obs','sim1','sim2'],fontsize=8,loc='upper right',ncol=3)
    #~ if i==0: ax[i][j].legend(['obs','KWE','OF','KWE_f3','OF_f3'],fontsize=10,loc='upper right',ncol=5)
    #~ if i==0: ax[i][j].legend(['obs','KWE_f3','OF_f3','KWE_f3_nosec','OF_f3_nosec'],fontsize=10,loc='upper right',ncol=5)
    #~ if i==0: ax[i][j].legend(['obs','KWE_f3','OF_f3_new','KWE_f3_nosec','OF_f3_nosec_new'],fontsize=10,loc='upper right',ncol=5)
    #~ if i==0: ax[i][j].legend(['obs','KWE_f3','OF_f3','KWE_f3_nomask','OF_f3_nomask'],fontsize=10,loc='upper right',ncol=5)
    #~ if i==0: ax[i][j].legend(['obs','Ks_0029m_h','Ks_06m_h','Ks_015m_h','Ks_2lyr_00023_023'],fontsize=10,loc='upper right',ncol=5)
    #~ if i==0: ax[i][j].legend(['obs','Ks_0029m_h','Ks_06m_h','Ks_015m_h','Ks_15m_h'],fontsize=10,loc='upper right',ncol=5)
    #~ if i==0: ax[i][j].legend(['obs','Ks_0029m_h','Ks_06m_h','Ks_015m_h','Ks_015m_h_H2low'],fontsize=10,loc='upper right',ncol=5)
    #~ if i==0: ax[i][j].legend(['obs','Ks_0029m_h','Ks_06m_h','Ks_015m_h','Ks_015m_h_KWE'],fontsize=10,loc='upper right',ncol=5)
    
    #2 simulations:
    #~ if i==0: ax[i][j].legend(['obs','AD','AH'],fontsize=10,loc='upper right',ncol=5)
    #~ if i==0: ax[i][j].legend(['obs','KWE','KWE_varman'],fontsize=10,loc='upper right',ncol=5)
    if i==0: ax[i][j].legend(['obs','KWE','KWE_highman'],fontsize=10,loc='upper right',ncol=5)
    #~ if i==0: ax[i][j].legend(['obs','AD_4yrs','AD_2yrs'],fontsize=10,loc='upper right',ncol=5)
    #~ if i==0: ax[i][j].legend(['obs','KWE','OF'],fontsize=10,loc='upper right',ncol=5)

    
    ax[i,j].set_xlim([datetime.datetime(plot_yr,4,1),datetime.datetime(plot_yr,11,1)])
    if scale:
        ax[i,j].set_ylim([0,520])
        #~ ax[i,j].set_ylim([0,300])
        #~ ax[i,j].set_ylim([0,220])
        ax[i,j].text(datetime.datetime(plot_yr,4,4),350,'%s: %s: $r^2 = %2.2f$ KGE = %2.2f\n%s: %s: $r^2 = %2.2f$ KGE = %2.2f'%(stationname,simuname1,res1[1]*res1[1],res1[0],stationname,simuname2,res2[1]*res2[1],res2[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})
        #~ ax[i,j].text(datetime.datetime(plot_yr,4,4),150,'%s: %s: $r^2 = %2.2f$ KGE = %2.2f\n%s: %s: $r^2 = %2.2f$ KGE = %2.2f'%(stationname,simuname1,res1[1]*res1[1],res1[0],stationname,simuname2,res2[1]*res2[1],res2[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})
    else:
        # comment for unscaling "free"
        #~ mask = (obs_intD.resample(method).mean().index >=datetime.datetime(plot_yr,4,1)) & (obs_intD.resample(method).mean().index<datetime.datetime(plot_yr,11,1))
        #~ ax[i,j].set_ylim([0,obs_intD.resample(method).mean().loc[mask].max()+0.24*obs_intD.resample(method).mean().loc[mask].max()])
        
        ax[i,j].text(datetime.datetime(plot_yr,4,4),ax[i,j].get_ylim()[1]-0.24*ax[i,j].get_ylim()[1],'%s: %s: $r^2 = %2.2f$ KGE = %2.2f\n%s: %s: $r^2 = %2.2f$ KGE = %2.2f'%(stationname,simuname1,res1[1]*res1[1],res1[0],stationname,simuname2,res2[1]*res2[1],res2[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})
        
    
    ax[i,j].tick_params(axis='x', which='both', labelbottom=False, labeltop=False)
    if j ==0: ax[i,j].tick_params(axis='y', which='both', labelright=False, labelleft=True)
    if j ==1: ax[i,j].tick_params(axis='y', which='both', labelright=True, labelleft=False)
    if j ==0: ax[i,j].tick_params(axis='both', which='major', bottom=False,top=False,right=False,left=True)
    if j ==1: ax[i,j].tick_params(axis='both', which='major', bottom=False,top=False,right=True,left=False)
    i+=1
    
#~ for i in range(2*int(np.ceil(len(stadic)/2))):
    #~ fig.axes[i].invert_yaxis()
    #~ fig.subplots_adjust(bottom=0.03, top =0.98,left=0.1,right =0.9,wspace=0.04, hspace=0.001)
    
ax[0,0].tick_params(axis='both', which='both', bottom=False,top=True,right=False,left=True)
ax[0,1].tick_params(axis='both', which='both', bottom=False,top=True,right=True,left=False)
ax[int(np.ceil(len(stadic)/2))-1,0].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)
ax[int(np.ceil(len(stadic)/2))-1,1].tick_params(axis='x', which='both', labelbottom=True, labeltop=False)
plt.gcf().subplots_adjust(bottom=0.05, top =0.95,left=0.2, hspace=0.001,wspace=0.001)

if scale:
    #~ plt.savefig(os.sep.join([simdir,'Figure_compare_Q_%s_'%method+np.str(int(plot_yr))+'.png']),dpi=400,format='png')
    
    #~ plt.savefig(os.sep.join([simdir1,'Figure_compare_Q_2sims_%s_%s_vs_%s_'%(method,simuname1,simuname2)+np.str(int(plot_yr))+'.png']),dpi=400,format='png')
    #~ plt.savefig(os.sep.join([simdir2,'Figure_compare_Q_2sims_%s_%s_vs_%s_'%(method,simuname1,simuname2)+np.str(int(plot_yr))+'.png']),dpi=400,format='png')
    
    #4 simulations:
    #~ plt.savefig(os.sep.join([simdir1,'Figure_compare_Q_4sims_tmp2_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'.png']),dpi=400,format='png')
    #~ plt.savefig(os.sep.join([simdir2,'Figure_compare_Q_4sims_tmp2_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'.png']),dpi=400,format='png')
    #~ plt.savefig(os.sep.join([simdir3,'Figure_compare_Q_4sims_tmp2_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'.png']),dpi=400,format='png')
    #~ plt.savefig(os.sep.join([simdir4,'Figure_compare_Q_4sims_tmp2_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'.png']),dpi=400,format='png')

    #2 simulations:
    plt.savefig(os.sep.join([simdir1,'Figure_compare_Q_2sims_tmp2_%s_%s_vs_%s_'%(method,simuname1,simuname2)+np.str(int(plot_yr))+'.png']),dpi=400,format='png')
    plt.savefig(os.sep.join([simdir2,'Figure_compare_Q_2sims_tmp2_%s_%s_vs_%s_'%(method,simuname1,simuname2)+np.str(int(plot_yr))+'.png']),dpi=400,format='png')
    
else:
    #~ plt.savefig(os.sep.join([simdir,'Figure_compare_Q_%s_'%method+np.str(int(plot_yr))+'_unscaled.png']),dpi=400,format='png')
    
    #~ plt.savefig(os.sep.join([simdir1,'Figure_compare_Q_2sims_%s_%s_vs_%s_'%(method,simuname1,simuname2)+np.str(int(plot_yr))+'_unscaled.png']),dpi=400,format='png')
    #~ plt.savefig(os.sep.join([simdir2,'Figure_compare_Q_2sims_%s_%s_vs_%s_'%(method,simuname1,simuname2)+np.str(int(plot_yr))+'_unscaled.png']),dpi=400,format='png')
    
    #4 simulations unscaled (but somewhat limited to the observation range not the highest range):
    #~ plt.savefig(os.sep.join([simdir1,'Figure_compare_Q_4sims_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'_unscaled.png']),dpi=400,format='png')
    #~ plt.savefig(os.sep.join([simdir2,'Figure_compare_Q_4sims_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'_unscaled.png']),dpi=400,format='png')    
    #~ plt.savefig(os.sep.join([simdir3,'Figure_compare_Q_4sims_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'_unscaled.png']),dpi=400,format='png')
    #~ plt.savefig(os.sep.join([simdir4,'Figure_compare_Q_4sims_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'_unscaled.png']),dpi=400,format='png')
    
    #4 simulations unscaled free:    
    #~ plt.savefig(os.sep.join([simdir1,'Figure_compare_Q_4sims_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'_unscaled_free.png']),dpi=400,format='png')
    #~ plt.savefig(os.sep.join([simdir2,'Figure_compare_Q_4sims_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'_unscaled_free.png']),dpi=400,format='png')    
    #~ plt.savefig(os.sep.join([simdir3,'Figure_compare_Q_4sims_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'_unscaled_free.png']),dpi=400,format='png')
    #~ plt.savefig(os.sep.join([simdir4,'Figure_compare_Q_4sims_%s_%s_vs_%s_vs_%s_vs_%s_'%(method,simuname1,simuname2,simuname3,simuname4)+np.str(int(plot_yr))+'_unscaled_free.png']),dpi=400,format='png')
    
    #2 simulations unscaled (but somewhat limited to the observation range not the highest range):
    #~ plt.savefig(os.sep.join([simdir1,'Figure_compare_Q_4sims_%s_%s_vs_%s_'%(method,simuname1,simuname2)+np.str(int(plot_yr))+'_unscaled.png']),dpi=400,format='png')
    #~ plt.savefig(os.sep.join([simdir2,'Figure_compare_Q_4sims_%s_%s_vs_%s_'%(method,simuname1,simuname2)+np.str(int(plot_yr))+'_unscaled.png']),dpi=400,format='png')    

    #2 simulations unscaled free:    
    plt.savefig(os.sep.join([simdir1,'Figure_compare_Q_2sims_%s_%s_vs_%s_'%(method,simuname1,simuname2)+np.str(int(plot_yr))+'_unscaled_free.png']),dpi=400,format='png')
    plt.savefig(os.sep.join([simdir2,'Figure_compare_Q_2sims_%s_%s_vs_%s_'%(method,simuname1,simuname2)+np.str(int(plot_yr))+'_unscaled_free.png']),dpi=400,format='png')    


"""

Qdf = pd.DataFrame()
for stationname,sta in stadic.items():
    Qdf = pd.concat([Qdf,sta.Qsim.rename(stationname,inplace = True)],axis = 1)

#~ Qdf.to_csv(os.sep.join([simdir,'simulated_Q.csv']))


"""
Calculate runoff coefficients
"""

#~ corres_station = {'BORI':'Bori','COTE_238':'Cote_238','IGBOMAKORO':'Igbomakoro','SANI_A_SANI':'Sani','SARMANGA':'Sarmanga','TEBOU':'Tebou','WEWE':'Wewe','AFFON_PONT':'Affon_Pont','AGUIMO':'Aguimo','AVAL-SANI':'Aval_Sani','BAREROU':'Barerou','BETEROU':'Beterou','DONGA_PONT':'Donga_Pont','DONGA_ROUTE_DE_KOLOKONDE':'Donga_route_Kolokonde','ARA_PONT':'Ara'}
corres_station = {'BORI':'Bori','COTE_238':'Cote_238','IGBOMAKORO':'Igbomakoro','SANI_A_SANI':'Sani','SARMANGA':'Sarmanga','TEBOU':'Tebou','WEWE':'Wewe','AFFON_PONT':'Affon_Pont','AGUIMO':'Aguimo','AVAL_SANI':'Aval_Sani','BAREROU':'Barerou','BETEROU':'Beterou','DONGA_PONT':'Donga_Pont','DONGA_ROUTE_DE_KOLOKONDE':'Donga_route_Kolokonde','ARA_PONT':'Ara','BOKPEROU':'Bokperou','DONGA_AFFON':'Donga Affon','KOUA':'Koua','NEKETE':'Nekete','OUEME_SUPERIEUR':'Oueme_Superieur'}
rev_corres_station = {v:k for k,v in corres_station.items()}



Kr = pd.read_csv('/home/hectorb/DATA/streamflow/AMMA_Benin/Analysis/Kr_analysis.csv')
Kr.loc[:,Kr.columns[0]].replace(to_replace = 'Donga route Kolokonde', value = 'Donga_route_Kolokonde',inplace = True)
Kr.loc[:,Kr.columns[0]] = Kr.loc[:,Kr.columns[0]].apply(lambda x: rev_corres_station[x])                                                       
Kr = Kr.set_index(Kr.columns[0])
Kr = pd.concat([Kr,(1000*(Qdf.resample('Y').sum().loc[Qdf.resample('Y').sum().index.year==2006,:]*3600/(Kr.area*1000*1000)).T)],axis = 1)
Kr = Kr.rename({Kr.columns[-1]: 'Qsim'},axis = 'columns')


PrecipOu =  pd.read_csv(r'/home/hectorb/DATA/Precipitation/Benin/Oueme/PrecipOu_streamgauges.csv')
PrecipOu = PrecipOu.set_index(PrecipOu.columns[0])
PrecipOu.index = pd.to_datetime(PrecipOu.index,format='%Y-%m-%d %H:%M:%S')
PrecipOu.index.rename('Date',inplace=True)
p_yr = PrecipOu.resample('Y').sum()
p_yr = p_yr.rename(rev_corres_station,axis ='columns')
Kr = pd.concat([Kr,p_yr.loc[p_yr.index.year==2006,:].T],axis = 1)
Kr = Kr.rename({Kr.columns[-1]: 'P2006'},axis = 'columns')
Kr['Krsim'] = Kr.Qsim/Kr.P2006


tmp=Kr.loc[:,['Mean_offset','std_offset','P_mean','Trees_proportionZin20m','Trees_proportion20m','Trees_proportion','Tree_Density','BasFondsSurface','Qsim','Krsim']].dropna(how='any')
tmp=Kr.dropna(how='any')
#~ model = sm.OLS(tmp.Mean_offset,sm.add_constant(tmp.loc[:,'Trees_proportion20m'])).fit()
#~ model = sm.OLS(tmp.Mean_offset,sm.add_constant(tmp.loc[:,'Trees_proportionZin20m'])).fit()
#~ model = sm.OLS(tmp.Mean_offset,sm.add_constant(tmp.loc[:,'BasFondsSurface'])).fit()
#~ model = sm.OLS(tmp.Mean_offset,sm.add_constant(tmp.loc[:,'Trees_proportion20m'])).fit()
model = sm.OLS(tmp.Krsim,sm.add_constant(tmp.loc[:,'Kr'])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
ax.legend(['data: r2 = %2.2f pval = %f'%(model.rsquared,model.pvalues[1]),'fit']) 
#~ plt.savefig('/home/hectorb/ADMIN/PROJETS/WorkInProgress/Q_Oueme/figures/Offset_btw_PmET0onset_Qonset_vs_vegetation20m.png')
#~ plt.savefig('/home/hectorb/ADMIN/PROJETS/WorkInProgress/Q_Oueme/figures/Offset_btw_PmET0onset_Qonset_vs_bas_fonds_surf.png')
plt.savefig(os.sep.join([simdir,'Figure_compare_Krsim_obs_%s_'%np.str(int(plot_yr))+'.png']),dpi=400,format='png')





""" This part is nastier: try to compare Kr & veg:

Try read the vegetation distribution: use a random tif matching the grid to get the metadata 

"""
#~ https://stackoverflow.com/questions/48063038/calculate-raster-landscape-proportion-percentage-within-multiple-overlaping-po

shape_fn = '/home/hectorb/DATA/streamflow/AMMA_Benin/QGIS/sous_bassins_polygonized_UTM_with_stats_slope_statsveg.shp'
#~ raster_fn  = '/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/preproc/DEM/DEM_hydrosheds_3s/dem_hdshds_UTM_334500_478500_987500_1131500_min_agreg.tif'
raster_fn  = '/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/preproc/DEM/DEM_hydrosheds_3s/dem_hdshds_UTM_334500_478500_987500_1131500_min_agreg.tif'
with rasterio.open(raster_fn) as src:
    transf = src.get_transform()
    
""" Save vegetation as tif """    
vmap,hdr = io.PFB_read('/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2019/veg_map.pfb',1)
drv = gdal.GetDriverByName("GTiff")
ds = drv.Create("/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2019/veg_map.tif", 144,144, 1, gdal.GDT_Float32)
#~ #ds.SetGeoTransform(upper_left_x, x_resolution, x_skew, upper_left_y, y_resolution, y_skew)
ds.SetGeoTransform(transf)
srs = osr.SpatialReference()                 # Establish its coordinate encoding
srs.ImportFromEPSG(32631)                     # This one specifies WGS84 lat long UTM
ds.SetProjection(srs.ExportToWkt() )   # Exports the coordinate system
#ds.SetProjection(ds_tmp.GetProjection())
outBand=ds.GetRasterBand(1)
vmap[np.isnan(vmap)]=-9999.0
outBand.WriteArray(np.swapaxes(vmap[:,::-1,0],1,0)) #checked w Qgis
outBand.SetNoDataValue(-9999.0)
outBand.FlushCache()
ds=None

raster_fn = "/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2019/veg_map.tif"

write_zero_frequencies = True
show_plot = False

shapefile = gpd.read_file(shape_fn)

# extract the geometries in GeoJSON format
geoms = shapefile.geometry.values # list of shapely geometries
records = shapefile.values
with rasterio.open(raster_fn) as src:
    classes = np.unique(src.read(1))

with rasterio.open(raster_fn) as src:
    print('nodata value:', src.nodata)
    idx_area = 0
    for index, row in shapefile.iterrows():
        upslope_area = row['geometry']
        name = row['nom']
        from shapely.geometry import mapping # transform to GeJSON format
        mapped_geom = [mapping(upslope_area)]
        out_image, out_transform = mask(src, mapped_geom, crop=True)# extract the raster values values within the polygon
        no_data=src.nodata #no data values of the original raster
        data = out_image.data[0] #extract the values of the masked array
        clas = np.extract(data != no_data, data) # extract the row, columns of the valid values
        #~ frequencies, class_limits = np.histogram(clas,bins=classes,range=[range_min, range_max])
        frequencies, class_limits = np.histogram(clas,bins=classes)
        if idx_area == 0:
            veg = pd.DataFrame({str(name): frequencies})
            veg.index = class_limits[:-1]
        else:
            veg[str(name)] = frequencies
        idx_area += 1

veg_sta = {'Affon Pont':'AFFON_PONT', 'Aguimo':"AGUIMO", 'Ara':'ARA', 'Aval Sani':"AVAL_SANI", 'Barerou':"BAREROU", 'Beterou':"BETEROU",
       'Bokperou':"BOKPEROU", 'Bori':'BORI', 'Cote 238':"COTE_238", 'Donga Pont':"DONGA_PONT", 'Donga Affon':'DONGA_AFFON',
       'Donga route Kolokonde':"DONGA_ROUTE_DE_KOLOKONDE", 'Igbomakoro':'IGBOMAKORO', 'Koua':"KOUA", 'Nekete':"NEKETE", 'Sani':"SANI",
       'Sarmanga':"SARMANGA", 'Tebou':'TEBOU', 'Wewe':"WEWE", 'Oueme Superieur':'OUEME_SUPERIEUR'}
veg = veg.rename(columns = veg_sta)

veg_prop = veg.apply(lambda x: x[7.0] / x.sum()).rename('Trees_proportion_sim')
Kr = pd.concat([Kr,veg_prop],axis = 1)
tmp1=Kr.loc[:,['Krsim','Trees_proportion_sim']].dropna(how='any')
tmp2=Kr.loc[:,['Kr','Trees_proportion']].dropna(how='any')
model1 = sm.OLS(tmp1.Krsim,sm.add_constant(tmp1.loc[:,'Trees_proportion_sim'])).fit()
model2 = sm.OLS(tmp2.Kr,sm.add_constant(tmp2.loc[:,'Trees_proportion'])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model1, 1, ax=ax)
sm.graphics.plot_fit(model2, 1, ax=ax)
ax.legend(['sim: r2 = %2.2f pval = %f'%(model1.rsquared,model1.pvalues[1]),'fit']) 
ax.legend(['data: r2 = %2.2f pval = %f'%(model2.rsquared,model2.pvalues[1]),'fit']) 
#~ plt.savefig(os.sep.join([simdir,'Figure_compare_Krsim_obs_%s_'%np.str(int(plot_yr))+'.png']),dpi=400,format='png')

fig, ax = plt.subplots()
ax.scatter(tmp1.Trees_proportion_sim,tmp1.Krsim,c='r')
#~ ax.plot(tmp2.Trees_proportion,tmp2.Kr,'k')



tmp=Kr.loc[:,['Kr','area','Mean_slopes','P_mean','Trees_proportionZin20m','Trees_proportion20m','Trees_proportion','Tree_Density']].dropna(how='any')
model = sm.OLS(tmp.Kr,sm.add_constant(tmp.loc[:,['Trees_proportion20m','area','Mean_slopes']])).fit()
fig, ax = plt.subplots()
fig = sm.graphics.plot_fit(model, 1, ax=ax)
ax.legend(['data: r2 = %2.2f pval = %f'%(model.rsquared,model.pvalues[1]),'fit']) 
#~ plt.savefig('/home/hectorb/ADMIN/PROJETS/WorkInProgress/Q_Oueme/figures/Offset_btw_PmET0onset_Qonset_vs_vegetation20m.png')
#~ plt.sav

