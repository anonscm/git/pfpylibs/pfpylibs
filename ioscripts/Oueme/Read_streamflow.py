#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    Anaylse PF outputs for Oueme: Streamflow
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os,glob
from netCDF4 import Dataset
import time
import datetime
import pyproj
import rasterio
from matplotlib.dates import MonthLocator, DateFormatter

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
from procsycz import procGeodata_Gdal


plt.close("all")

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/test_2018_04_24'
simuname = 'test'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2017/oueme2D_KH3high4_2016_12_22'
simuname = 'oueme2D_KH3high4'

#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup_AH/oueme_2018_07_23'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_2018_08_30'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_2018_08_29'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_Kapptot_exp30_CIconst_2018_09_04'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_Kapptot_exp30_Dz6_H20036_2018_09_04'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_Kapptot_exp30_Dz6_H1H2036_6yrs_2018_09_07'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_Kapptot_gauss30_Dz6_H20036_2018_09_04'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_2018_08_30'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_1layer_CIvar_2018_09_05'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_1layer_2018_08_30'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_domain18_2018_08_30'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H1H2036_6yrs_sandH2_2018_09_08'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H20036_CI6_2018_09_09'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H1H2036_CI6_6yrs_2018_09_10'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H20036_CI6_2018_09_11'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI6_2018_09_11'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H20036_CI6_6yrs_2018_09_12'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H20036_CI6_H3ret_2018_09_12'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H20036_CI6_H3ret_4yrs_2018_09_13'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H20036_CI6_6yrs_2018_09_13'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI6_5yrs_2018_09_13'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI4_P3_005_2018_09_15'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI4_P3_005_2018_09_15'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI4_P3_01_2018_09_17'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H20036_CI4_P3_01_2018_09_18'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_H336_CI4_P3_01_2018_09_20'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI4_P3_01_H4clay_tmp_2018_09_20'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_H336_CI8_P3_01_2018_09_21'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZA_2018_09_25'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI4_P3_01_anis_2018_09_21'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_H336_CI4_P3_01_H4clay_2018_09_21'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZAZFA_4yrs_2018_09_28'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZA_stat2_2018_09_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZA_2018_09_26'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZA_H30052_2018_09_26'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_H336_CI8_P3_01_anis_2018_09_26'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H2clay_2018_10_30'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H2clayV2_2018_10_31'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H2clayV3_2018_11_20'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H2imp_2018_11_22'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H2imp_deeprt_2018_11_26'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H2imp_deeprt2_2018_12_21'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_H2imp_deeprt2_2018_12_21'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/NCout/oueme_ADlike_2019_10_01'

simuname = 'oueme'
#~ simuname = 'oueme_results'
#~ simuname = 'oueme_results_1layer_CIvar'
simuname = 'oueme_results_Kapptot_exp30_CIconst'
#~ simuname = 'oueme_results_Kapptot_exp30_Dz6_H20036'
#~ simuname = 'oueme_results_Kapptot_exp30_Dz6_H1H2036_6yrs'
#~ simuname = 'oueme_results_Kapptot_gauss30_Dz6_H20036'
#~ simuname = 'oueme_results_domain18'
simuname = 'oueme_results_H1H2036_6yrs_sandH2'
simuname = 'oueme_results_H20036_CI6'
simuname = 'oueme_results_H1H2036_CI6_6yrs'
simuname = 'oueme_results_ZA_H20036_CI6'
simuname = 'oueme_results_ZA_H236_CI6'
simuname = 'oueme_results_H20036_CI6_6yrs'
simuname = 'oueme_results_ZA_H20036_CI6_H3ret'
simuname = 'oueme_results_ZA_H20036_CI6_H3ret_4yrs'
simuname = 'oueme_results_ZA_H20036_CI6_6yrs'
simuname = 'oueme_results_ZA_H236_CI6_5yrs'
simuname = 'oueme_results_ZA_H236_CI4_P3_005'
simuname = 'oueme_results_ZA_H236_CI4_P3_01_H4clay_tmp'
simuname = 'oueme_results_ZA_H236_CI4_P3_01'
simuname = 'oueme_results_ZA_H20036_CI4_P3_01'
simuname = 'oueme_results_ZA_H236_H336_CI4_P3_01'
simuname = 'oueme_results_ZA_H236_CI4_P3_01_H4clay_tmp'
simuname = 'oueme_results_ZA_H236_H336_CI8_P3_01'
#~ simuname = 'oueme_Amelie_results_ZA'
#~ simuname = 'oueme_results_ZA_H236_CI4_P3_01_anis'
#~ simuname = 'oueme_results_ZA_H236_H336_CI4_P3_01_H4clay'
#~ simuname = 'oueme_Amelie_results_ZAZFA_4yrs'
simuname = 'oueme_Amelie_results_ZA_stat2'
#~ simuname = 'oueme_Amelie_results_ZA'
#~ simuname = 'oueme_Amelie_results_ZA_H30052'
#~ simuname = 'oueme_results_ZA_H236_H336_CI8_P3_01_anis'
#~ simuname = 'oueme_results_H2clay'
#~ simuname = 'oueme_results_H2clayV2'
#~ simuname = 'oueme_results_H2clayV3'
#~ simuname = 'oueme_results_H2imp'
#~ simuname = 'oueme_results_H2imp_deeprt2'
#~ simuname = 'oueme_ADlike'

plot_yr = 2006
method = 'W'
#~ method = 'H'
#~ n_coupled_layers = 5
#~ n_CLM_2Dlayers = 14

statsDay =True
#~ statsDay =False

"""
CLM_var is a dictionnary where keys are local variable names as in PFCLM
values are lists of string[filename, PFCLM variable name, long name, units]
"""
CLM_var={'surf_press':['','h_top','Surface pressure','m']}
""" add the filename in the dic"""        
for k,val in CLM_var.items():
    CLM_var[k][0]=os.sep.join([simdir,''.join([simuname,'_'+val[1]+'.nc'])])

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def minimalist_xldate_as_datetime(xldate, datemode):
    # datemode: 0 for 1900-based, 1 for 1904-based
    return (
        datetime.datetime(1899, 12, 30)
        + datetime.timedelta(days=xldate + 1462 * datemode)
        )
        
def roundDateTime(shakyDT,nearest_minutes=1):
	"""
	"""
	shakyDT += datetime.timedelta(minutes=nearest_minutes/2)
	shakyDT -= datetime.timedelta(minutes=shakyDT.minute % nearest_minutes,
                         seconds=shakyDT.second,
                         microseconds=shakyDT.microsecond)
	return shakyDT
	

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

#### Read & process simulation outputs:
sim = PFout.PFout(simdir,'oueme')
sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_h_top.nc'])]),filetype='CLM')
sim.X0=np.min(sim.x)
sim.Y0=np.min(sim.y)


"""Read PF outputs"""
start_time = time.time()
with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
    sim.satur_array = ds.variables['saturation'][:]
    #~ topmask=np.swapaxes(ds.variables['top_mask'][:],0,1)
"""MASK is a nx x ny boolean mask of the domain
When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
(as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
"""
#~ MASK=topmask>0 
with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
    sim.press_array = ds.variables['succion'][:] 

"""Read CLM outputs"""
for k,val in CLM_var.items():
    try:
        with Dataset(val[0], 'r') as ds:
            print('read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
            exec('sim.%s=np.swapaxes(ds.variables[\'%s\'][:],0,2)'%(k,val[1]))        
    except FileNotFoundError:
            print('CANNOT read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
print("--- %d seconds --- read all PFCLM outputs from NCs" % (time.time() - start_time))

""" Read constant files"""
ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
#watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,1)
sim.mannings_arr = np.zeros((sim.nx,sim.ny))+1e-6
sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,1)
sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,1)
sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,1)
#~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))

""" Read Forcings: """
#~ forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
#~ forc = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')
#~ forc.rename(columns=forcing_headers,inplace=True)
#~ forc.index=pd.date_range('1/1/2005 00:00:00', periods=len(forc), freq='30min')
#~ sim.P=sum(forc['P'])*30*60

"""Get streamflow data"""
rt_dir = r'/home/hectorb/DATA/streamflow/AMMA_Benin/Donga'
rt_dir = r'/home/hectorb/DATA/streamflow/AMMA_Benin/Oueme'
#~ rt_dir = r'/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/analyse/Data/streamflow/Oueme'
suf_pattern = '.csv'
#~ pre_pattern = 'CL.Run_Od-'
pre_pattern = 'CL.Run_O-'
station_list = {'ARA_PONT':2 ,'DONGA_PONT':3,'DONGA_ROUTE_DE_KOLOKONDE':3}
station_list = {'BORI':1,'COTE_238':1,'IGBOMAKORO':1,'SANI_A_SANI':1,'SARMANGA':1,'TEBOU':1,'WEWE':1,'AFFON_PONT':1,'AGUIMO':1,'AVAL-SANI':1,'BAREROU':1,'BETEROU':1}
station_list= {k: 2 for k, v in station_list.items()}
#first col is time then daily average then hourly

proj = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')

df = pd.DataFrame()
stadic ={}
for stationname,data_column in station_list.items():
    """ Create station object for each station """
    sta = rdA.Station(name = stationname)    
    filepattern = os.path.join(rt_dir,'*'.join([''.join([pre_pattern,stationname]),suf_pattern]))        
    """m3/s"""
    sta.read_Q(filepattern, data_col = data_column)
    #~ sta.Q = sta.Q*sta.Q.index.to_series().diff().dt.total_seconds().fillna(0)
    """m3/s -> m3/hr"""
    #~ sta.Q = sta.Q*3600.
    df[stationname]=sta.Q
    print(sta.name)
    filenames = glob.glob(filepattern)    
    sta.read_latlon_from_Qfile(filenames[0])
    sta.x, sta.y = pyproj.transform(geo_system,proj,sta.lon, sta.lat)
    print(sta.lon)
    print(sta.lat)
    print(sta.x)
    print(sta.y)
    stadic[stationname] = sta
    
""" Get stream network:"""
#~ with rasterio.open("/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/inputs/geometries/Donga/data/Donga_streams_segments_1000m_masked_mean_agreg_D4_pitfilled.tif") as src:
#~ with rasterio.open("/homelocal5/phyrev/_PARFLOW/projects/Donga/active/Donga_1km/input/DEM/Rwatershed/streams.tif") as src:
data,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/inputs/geometries/Oueme/data/Oueme_streams_segments_1000m_masked_mean_agreg_D4_pitfilled.tif",nodata_value = 65535)
[lonlon,latlat]=np.meshgrid(londem,latdem)

""" Find the location of data in the PF framework, and calculate simulated Q:"""
xx_masked = np.round(lonlon)
yy_masked = np.round(latlat)
#~ xx_masked[data == 0] = -99999999
#~ yy_masked[data == 0] = -99999999
xx_masked[data[::-1,:] == 0] = -99999999
yy_masked[data[::-1,:] == 0] = -99999999
xx_masked[data == 255] = -99999999
yy_masked[data == 255] = -99999999


#xx_masked, yy_masked and data have shapes [ny,nx] and data[0,:] is the lowest latitude (bottom of map)

Qsim = pd.DataFrame()
#~ x = [sim.X0 + sim.dx*x+sim.dx/2 for x in range(sim.nx)]
#~ y = [sim.Y0 + sim.dy*y+sim.dy/2 for y in range(sim.ny)]
x = [sim.X0 + sim.dx*x for x in range(sim.nx)]
y = [sim.Y0 + sim.dy*y for y in range(sim.ny)]
for stationname,sta in stadic.items():
    print(sta.name)
    #find the closest point in the grid:  
    dist_to_river = np.sqrt((xx_masked - sta.x)**2 + (yy_masked - sta.y)**2)
    dist = np.sqrt((np.round(lonlon)- sta.x)**2 + (np.round(latlat) - sta.y)**2)
    #~ if (dist_to_river.min() <= 2*np.sqrt(sim.dx*sim.dx + sim.dy*sim.dy)):
    if (dist_to_river.min() < np.sqrt(sim.dx*sim.dx + sim.dy*sim.dy)):
        ind_min = np.where(dist_to_river == dist_to_river.min())
        print('river cell found')
    else:
        ind_min = np.where(dist == dist.min())   
        print('no river cell found')
    sta.x_sim = np.round(lonlon[ind_min])
    sta.y_sim = np.round(latlat[ind_min])
    print(sta.x)
    print(sta.y)
    print(sta.x_sim)
    print(sta.y_sim)    
    # now get the outlet location in the PF domain:
    #~ sim.calculate_runoff_from_CLM(x_outlet = np.where(np.array(x) == sta.x_sim),y_outlet = np.where(np.array(y) == sta.y_sim),nlayers = n_coupled_layers+n_CLM_2Dlayers)
    sim.calculate_runoff_from_press(x_outlet = np.where(np.array(x) == sta.x_sim)[0],y_outlet = np.where(np.array(y) == sta.y_sim)[0])    
    print(np.where(np.array(x) == sta.x_sim))
    print(np.where(np.array(y) == sta.y_sim))
    print(np.shape(sim.surfQ))
    # if m3 => m3/s:
    #~ sim.surfQ = [Q/(60*60) for Q in sim.surfQ ]
    """m3/hr (TO CHECK -> seems ok)"""
    #~ sta.Qsim  = sim.create_panda_series(np.reshape(sim.surfQ,sim.n_t_clm),start_date = "1/1/2007", start_hour = "00:00", freq = "1H",periods = sim.n_t_clm)        
    sta.Qsim  = sim.create_panda_series(np.reshape(sim.surfQ,sim.n_t_clm),start_date = "1/1/2005", start_hour = "00:00", freq = "1H",periods = sim.n_t_clm)        
    sta.Qsim[abs(sta.Qsim)>10e10]=np.nan
    """m3/hr -> m3/s"""
    sta.Qsim = sta.Qsim/3600.0
    Qsim[stationname]=sta.Qsim        
    stadic[stationname] = sta


def pdinterpolate(ts, datetime_index):
    """could decide to fill nans !
    but not adapted for calculating scores?
    To be tested"""
    x = pd.concat([ts, pd.Series(index=datetime_index)])
    #~ return x.groupby(x.index).first().sort_index().fillna(method="ffill")[datetime_index]
    return x.groupby(x.index).first().sort_index().interpolate('linear')[datetime_index]
    #~ return x.groupby(x.index).first().sort_index()[datetime_index]


"""
plot station map
#~ https://stackoverflow.com/questions/40899350/python-quiver-and-pcolormesh-not-lining-up-exactly-right     

"""
fig = plt.figure(figsize=(16,8))
ax = fig.add_subplot(111)
ax.set_aspect(1)
[xx,yy]=np.meshgrid(x,y)

""" to check the location with respect to pressure field:"""
#~ p = ax.pcolormesh(xx-sim.dx/2, yy-sim.dy/2, sim.press_array[240,-1,:,:], cmap="viridis", zorder=-1)
""" to check the location, use the stream mask:"""
p = ax.pcolormesh(lonlon-sim.dx/2, latlat-sim.dy/2, data[::-1,:], cmap="viridis", zorder=-1)
cb = plt.colorbar(p)
cb.set_label('P(m)',fontsize=20)
ax.set_xlabel('X utm (m)',fontsize=20)
ax.set_ylabel('Y utm (m)',fontsize=20)

ax.plot([sta.x for stationname,sta in stadic.items()],[sta.y for stationname,sta in stadic.items()],'k+',markersize=12)
ax.plot([sta.x_sim for stationname,sta in stadic.items()],[sta.y_sim for stationname,sta in stadic.items()],'r+',markersize=12)




"""
plot time series
"""
#~ fig,ax =plt.subplots(nrows=len(stadic),figsize=(20,7), squeeze=True)
#~ i=0
#~ for stationname,sta in stadic.items():
    #~ ax[i].plot(sta.Q.loc['2007-1-1':'2007-12-31'].dropna(),'k')
    #~ ax[i].plot(sta.Qsim.loc['2007-1-1':'2007-12-31'],'r')
    #~ ax[i].set_ylabel('$m3/hr')
    #~ ax[i].legend(['%s: obs'%stationname,'%s: sim'%stationname])
    #~ i+=1

fig,ax =plt.subplots(nrows=int(np.ceil(len(stadic)/2)),ncols = 2,figsize=(20,10), squeeze=True)
i=0
j=0


for stationname,sta in stadic.items():
    # interpolation on sim + daily approach:
    obs = sta.Q.dropna().sort_index(axis=0)
    simu = sta.Qsim.dropna().sort_index(axis=0)
    obs = obs.groupby(obs.index).mean()
    obs_int=pd.concat([obs,simu],axis=1).interpolate(method='slinear',limit=50)[0]
    obs_intD = obs_int.resample('D').mean()
    simuD = simu.resample('D').mean()
    merge=pd.concat([obs_intD.rename('obs'),simuD.rename('sim')],axis=1)
    kept = merge.dropna()
    res = obj.kge(kept['obs'].values, kept['sim'].values, return_all=True)
    print(res)  
        #~ else:
            #~ # Interpolation on the closest available obs - sim couples:
            #~ #simuD = simu.resample('D').asfreq()
            #~ #simuD = dsclm['Q'][dsclm.index.hour==12]
            #~ simu = simu[(simu.index.month>=6) & (simu.index.month<=11)]
            #~ new_dates = obs.index.round('H')
            #~ obs2=obs
            #~ obs2.index = new_dates
            #~ obs2 = obs2.groupby(obs2.index).mean()
            #~ merge=pd.concat([obs2,simu],axis=1)
            #~ #merge=pd.concat([obs2,simuD],axis=1)  
  
    
    
    if i>=int(np.ceil(len(stadic)/2)):
        j=1
        i=0
    ax[i][j].plot(sta.Q.dropna().sort_index(axis=0),'k')    
    #~ ax[i][j].plot(sta.Qsim.dropna().sort_index(axis=0),'r')
    ax[i][j].plot(sta.Qsim.dropna().sort_index(axis=0).resample(method).mean(),'r')
    ax[i][j].plot(obs_intD,'b')
    
    #~ ax[i].plot(sta.WTsim.loc['2007-1-1':'2007-12-31'],'r')
    if j==0: ax[i][j].set_ylabel('$m3/s')
    if i==0: ax[i][j].legend(['obs','sim'],fontsize=8,loc='upper right',ncol=2)
    ax[i,j].set_xlim([datetime.datetime(plot_yr,4,1),datetime.datetime(plot_yr,11,1)])
    ax[i,j].set_ylim([0,520])
    ax[i,j].text(datetime.datetime(plot_yr,4,4),480,r'%s: $r^2 = %2.2f$ KGE = %2.2f'%(stationname,res[1]*res[1],res[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})
    
    ax[i,j].tick_params(axis='x', which='both', labelbottom='off', labeltop='off')
    if j ==0: ax[i,j].tick_params(axis='y', which='both', labelright='off', labelleft='on')
    if j ==1: ax[i,j].tick_params(axis='y', which='both', labelright='on', labelleft='off')
    if j ==0: ax[i,j].tick_params(axis='both', which='major', bottom='off',top='off',right='off',left='on')
    if j ==1: ax[i,j].tick_params(axis='both', which='major', bottom='off',top='off',right='on',left='off')
    i+=1
    
for i in range(2*int(np.ceil(len(stadic)/2))):
    #~ fig.axes[i].invert_yaxis()
    fig.subplots_adjust(bottom=0.03, top =0.98,left=0.1,right =0.9,wspace=0.04, hspace=0.001)
    
ax[0,0].tick_params(axis='both', which='major', bottom='off',top='on',right='off',left='on')
ax[0,1].tick_params(axis='both', which='major', bottom='off',top='on',right='on',left='off')
ax[int(np.ceil(len(stadic)/2))-1,0].tick_params(axis='x', which='both', labelbottom='on', labeltop='off')
ax[int(np.ceil(len(stadic)/2))-1,1].tick_params(axis='x', which='both', labelbottom='on', labeltop='off')
plt.savefig(os.sep.join([simdir,'Figure_compare_Q_%s_'%method+np.str(int(plot_yr))+'.png']),dpi=400,format='png')


