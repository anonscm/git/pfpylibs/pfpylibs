#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Calculate budgets for Oueme simulation

    @copyright: 2020 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2020"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os,glob
from netCDF4 import Dataset
import time
import datetime
import pyproj
import rasterio
from matplotlib.dates import MonthLocator, DateFormatter

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_pfb as iopfb
from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
from procsycz import procGeodata_Gdal


plt.close("all")


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI4_P3_005_2018_09_15'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI4_P3_01_H4clay_tmp_2018_09_17'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI4_P3_01_2018_09_17'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI6_5yrs_2018_09_13'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZAZFA_4yrs_2018_09_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZA_stat2_2018_09_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZAZFAlow_2018_09_26'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZA_2018_09_26'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_Amelie_results_ZA_H30052_2018_09_26'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_H336_CI8_P3_01_anis_2018_09_26'
simdir = r'/mnt/hectorb/equipes/phyrev/_PARFLOW/share/Amelie/DATA/ouem_out/Ciment_BH/oueme_Amelie_results_ZA_H30052_4yrs_2018_09_27'
#~ simdir = r'/mnt/hectorb/equipes/phyrev/_PARFLOW/share/Amelie/DATA/ouem_out/Ciment_BH/oueme_Amelie_results_ZA_4yrs_2018_09_27'
#~ simdir = r'/mnt/hectorb/equipes/phyrev/_PARFLOW/share/Amelie/DATA/ouem_out/Ciment_BH/oueme_Amelie_results_ZAZFA_4yrs_2018_09_28'
simuname = 'oueme_results_ZA_H236_CI4_P3_005'
simuname = 'oueme_results_ZA_H236_CI4_P3_01_H4clay_tmp'
simuname = 'oueme_results_ZA_H236_CI4_P3_01'
simuname = 'oueme_results_ZA_H236_CI6_5yrs'
simuname = 'oueme_Amelie_results_ZAZFA_4yrs'
#~ simuname = 'oueme_Amelie_results_ZA_stat2'
#~ simuname = 'oueme_Amelie_results_ZAZFAlow'
simuname = 'oueme_Amelie_results_ZA_H30052_4yrs'
#~ simuname = 'oueme_Amelie_results_ZA_4yrs'
#~ simuname = 'oueme_Amelie_results_ZAZFA_4yrs'
#~ simuname = 'oueme_Amelie_results_ZA_H30052'
#~ simuname = 'oueme_results_ZA_H236_H336_CI8_P3_01_anis'

simdir = r'/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_KWE_AD_H1lowK_rt85_4yrs_2020_04_20'
simuname = 'oueme_KWE_AD_H1lowK_rt85_4yrs'

#FARO
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3_2020_04_29'
simuname = r'oueme_AD_H3lowP_rt10_nvdz_nclm_rtaherb_man3'
#simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/oueme_ZA24_H3015_H2lowKlown_4yrs_2020_04_30'
#simuname = r'oueme_ZA24_H3015_H2lowKlown_4yrs'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_10yrs_2020_05_29'
simuname = r'H2clay_10yrs'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme2020/NCout/H2clay_ZAZFA_100_9yrs_2020_06_03'
simuname = r'H2clay_ZAZFA_100_9yrs'
#~ manning = 1e-6
manning = 3e-6
n_coupled_layers = 8

"""
CLM_var is a dictionnary where keys are local variable names as in PFCLM
values are lists of string[filename, PFCLM variable name, long name, units]
"""
CLM_var={'surf_press':['','h_top','Surface pressure','m'],\
         'evap_tot':['','qflx_evap_tot','total evapotranspiration','mm/s']}
""" add the filename in the dic"""        
for k,val in CLM_var.items():
    CLM_var[k][0]=os.sep.join([simdir,''.join([simuname,'_'+val[1]+'.nc'])])


##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def plot_clustered_stacked(dfall, labels=None,  H="/", **kwargs):
    """Given a list of dataframes, with identical columns and index, create a clustered stacked bar plot. 
labels is a list of the names of the dataframe, used for the legend
title is a string for the title of the plot
H is the hatch used for identification of the different dataframe"""

    n_df = len(dfall)
    n_col = len(dfall[0].columns) 
    n_ind = len(dfall[0].index)
    fig = plt.figure(figsize=(10,4))
    axe = fig.add_subplot(111)

    for df in dfall : # for each data frame
        if isinstance(df, pd.DataFrame):
            axe = df.plot(kind="bar",
            linewidth=0,
            stacked=True,
            ax=axe,
            legend=False,
            grid=False,
            **kwargs)  # make bar plots
        else:
             axe = df.plot(kind="bar",
             linewidth=0,
             stacked=True,
             ax=axe,
             legend=False,
             grid=False,
	     color='b',
             **kwargs)  # make bar plots

    h,l = axe.get_legend_handles_labels() # get the handles we want to modify
    for i in range(0, n_df * n_col, n_col): # len(h) = n_col * n_df
        for j, pa in enumerate(h[i:i+n_col]):
            for rect in pa.patches: # for each index
                rect.set_x(rect.get_x() + 1 / float(n_df + 1) * i / float(n_col))
                rect.set_hatch(H * int(i / n_col)) #edited part     
                rect.set_width(1 / float(n_df + 1))

    axe.set_xticks((np.arange(0, 2 * n_ind, 2) + 1 / float(n_df + 1)) / 2.)
    axe.set_xticklabels(df.index, rotation = 0)

    # Add invisible data to add another legend
    n=[]        
    for i in range(n_df):
        n.append(axe.bar(0, 0, color="gray", hatch=H * i))

    l1 = axe.legend(h[:n_col], l[:n_col], loc=[1.01, 0.5])
    if labels is not None:
        l2 = plt.legend(n, labels, loc=[1.01, 0.1]) 
    axe.add_artist(l1)
    return axe


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

#### Read & process simulation outputs:
sim = PFout.PFout(simdir,'oueme')
sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_h_top.nc'])]),filetype='CLM')
sim.X0=np.min(sim.x)
sim.Y0=np.min(sim.y)


"""Read PF outputs"""
start_time = time.time()
with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
    sim.satur_array = np.transpose(ds.variables['saturation'][:],[3,2,1,0])
    topmask=np.swapaxes(ds.variables['top_mask'][:],0,1)
    #~ topmask = np.swapaxes(ds.variables['mask'][0,:,:],0,1) 
    vegmap=np.swapaxes(ds.variables['veg_map'][:],0,1)   
# for some weird reason sometimes the mask aint in the nc... so pick up another one
#~ ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
#~ topmask = np.swapaxes(ds.variables['mask'][0,:,:],0,1) 


"""MASK is a nx x ny boolean mask of the domain
When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
(as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
"""
MASK=topmask>0 
with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
    sim.press_array = np.transpose(ds.variables['succion'][:],[3,2,1,0])

"""Read CLM outputs"""
for k,val in CLM_var.items():
    try:
        with Dataset(val[0], 'r') as ds:
            print('read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
            exec('sim.%s=np.swapaxes(ds.variables[\'%s\'][:],0,2)'%(k,val[1]))        
    except FileNotFoundError:
            print('CANNOT read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
print("--- %d seconds --- read all PFCLM outputs from NCs" % (time.time() - start_time))

""" Read constant files"""
ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
#watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
#~ sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,1)
sim.dz_mult_arr = np.transpose(ds.variables['var_dz'][:],[2,1,0])
sim.mannings_arr = np.zeros((sim.nx,sim.ny))+ manning 
#~ sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,1)
sim.mask_arr = np.transpose(ds.variables['mask'][:],[2,1,0])
sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
#~ sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,1)
sim.porosity_arr = np.transpose(ds.variables['porosity'][:],[2,1,0])
#~ sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,1)
sim.specific_storage_arr = np.transpose(ds.variables['specific_storage'][:],[2,1,0])
#~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))
sim.dz = np.diff(ds.variables['depth'][:])[0]
sim.z = np.cumsum(sim.dz_mult_arr[0,0,::-1]*sim.dz) - sim.dz_mult_arr[0,0,::-1]*sim.dz/2
sim.z = np.round(sim.z*1000)/1000


"""Prepare Budg datadfram"""
Budg = pd.DataFrame()

#~ Pdir = '/mnt/hectorb/equipes/phyrev/_PARFLOW/share/Amelie/DATA/ouem_in/FORC/APCP'
#~ filepattern = glob.glob(os.path.join(Pdir,'Forc.APCP.*.pfb'))
#~ filepattern.sort()
#~ i = 0
#~ for f in filepattern:
    #~ print('reading file number: %d'%i)
    #~ d,h = PFlibs.io_pfb.PFB_read(f,1)
    #~ if i == 0:
        #~ P = np.zeros([h['nx'],h['ny']])
    #~ else:
        #~ P = np.dstack([P,d])
    #~ i+=1

""" Read Forcings  """
#original dir: 
#Pdir='/mnt/hectorb/srv12/DATA/rain_data'
#~ Pdir='/media/hectorb/Basile/bckp_recherche/PARFLOW/PROJECTS/Oueme/forcings'
#FARO:
Pdir='/homelocal5/phyrev/_PARFLOW/DATA/precip/rain_data'
with Dataset(os.sep.join([Pdir,'rainfall_2005_30mn_utm.nc']), 'r') as ds:
    P1 = np.transpose(ds.variables['rainfall'][:],[2,1,0])
with Dataset(os.sep.join([Pdir,'rainfall_2006_30mn_utm.nc']), 'r') as ds:
    P2 = np.transpose(ds.variables['rainfall'][:],[2,1,0])
P = pd.Series(np.dstack([P1,P2])[MASK].mean(axis=0))
P.index = pd.date_range('1/1/2005 00:00:00', periods=len(P), freq='30MIN')
Budg['P'] = P.resample('1H').sum()



""" GET ET"""
#~ #evap_tot units are mm/s
#~ dsclmD=pd.Series(sim.evap_tot[MASK].mean(axis=0)*3600)
#~ dsclmD.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t_clm, freq='H')
#~ Budg['ET'] = dsclmD

""" Get Q """
#~ sim.calculate_runoff_from_press(x_outlet = int((416000 - 334000)/sim.dx),y_outlet = int((1000000 - 987000)/sim.dy))    
#~ # if m3 => m3/s:
#~ #sim.surfQ = [Q/(60*60) for Q in sim.surfQ ]
"""m3/hr (TO CHECK -> seems ok)"""
#~ Qsim  = sim.create_panda_series(np.reshape(sim.surfQ,sim.n_t_clm),start_date = "1/1/2005", start_hour = "00:00", freq = "1H",periods = sim.n_t_clm)        
#~ Qsim[abs(Qsim)>10e10]=np.nan
catchmentsurf=np.sum(MASK)*sim.dx*sim.dy
#~ Budg['Q'] = Qsim*1000/catchmentsurf

""" Alternative, get overlandsum and evaptranssum from PFBs:
That's to avoid calculating budgets on snapshots (say hourly instead of 30mn actual time step)
but on internally calculated running average.
Note that evaptranssum has to be mult by the vardz of each layer and then P has to be removed from it
Note
"""
overlandsum_array = np.zeros((sim.nx,sim.ny,sim.n_t-1))
evaptranssum_array = np.zeros((sim.nx,sim.ny,sim.n_t-1))
prop_base = os.sep.join([simdir,'overlandsum/oueme.out'])
prop_baseEvap = os.sep.join([simdir,'evaptranssum/oueme.out'])
for t in range(sim.n_t-1):
    # first 24hrs is empty for some reason
    if t==0:
        #~ overlandsum_array[:,:,t]=0.
        evaptranssum_array[:,:,t]=0.
    else:
        # Get all time file directories
        #~ overlandsum_fp = '.'.join([prop_base,"overlandsum",format((t+1)*24,"05.0F"),"pfb"])
        evaptranssum_fp = '.'.join([prop_baseEvap, "evaptranssum",format((t+1)*24,"05.0F"),"pfb"])
        #~ evaptrans_fp = '.'.join([prop_base, "evaptrans", format((t+1)*dump_int,"05.0F"),"pfb"])
        
        # Read pfb
        #~ overlandsum_arr = iopfb.PFB_read(overlandsum_fp,0)
        evaptranssum_arr = iopfb.PFB_read(evaptranssum_fp,0)
        #~ evaptrans_arr = PFB_read(evaptrans_fp,0)
        
        #Fill arrays for each time output
        #~ overlandsum_array[:,:,t]= overlandsum_arr[:,:,0]
        for i in range(n_coupled_layers):
            evaptranssum_array[:,:,t]+=evaptranssum_arr[:,:,i]*(sim.dz*sim.dz_mult_arr[0,0,-(i+1)])

Budg = Budg.resample('D').sum()[0:720] #temporary to match the fact that simulations have only 720 timesteps (daily)
Budg['Q'] = overlandsum_array.sum(axis=0).sum(axis=0)*1000./catchmentsurf
""" Get ET NEW WAY ! ET as obtained from overlandsum doesn't lack data as when obtained from undersampled snapshots! """
#the closest: evaptranssum in pf (evaptranssum.c)is mult by dxdydzdt, just before writing evaptranssum.pfb, so here we devide by dxdy (catchment surf), and divide by dz (0.5 only, assuming in PF they don't take vardz')
# *1000 is to go mm. Upstream, in pf_couple, it is divided by dz*vardz, so we mult by vardz only() -> then we remove the precip.
# actually the flux is divided by each layer dz in pf_couple !!
"""
From pf_couple.F90:
   if (k == 1) then
      clm(t)%pf_flux(k)=(-clm(t)%qflx_tran_veg*clm(t)%rootfr(k)) + clm(t)%qflx_infl + clm(t)%qflx_qirr_inst(k)
!!print*, 'Beta:',(-clm(t)%qflx_tran_veg*clm(t)%rootfr(k)),clm(t)%qflx_infl,saturation(l),pressure(l)
   else  
      clm(t)%pf_flux(k)=(-clm(t)%qflx_tran_veg*clm(t)%rootfr(k)) + clm(t)%qflx_qirr_inst(k)
   endif
   ! copy back to pf, assumes timing for pf is hours and timing for clm is seconds
   ! IMF: replaced drv%dz with clm(t)%dz to allow variable DZ...
   evap_trans(l) = clm(t)%pf_flux(k) * 3.6d0 / clm(t)%dz(k)
enddo

=>> see also solver_richards and evaptranssum.c where evap_trans_sum_ptr[index_evap_trans_sum] += evap_trans_ptr[index_evap_trans] * vol_time;
with vol_time = dx*dy*dz*dt;
=>> so to get the evaptranssum, you need to divide by sim.dz (if you've multiplied by sim.dzxsim.dz_mult already, layer by layer)
"""
Budg['ET'] = -(evaptranssum_array.sum(axis=0).sum(axis=0)*1000./catchmentsurf/sim.dz-Budg['P'])

""" ATTENTION !!!!! create ET & Q !!!"""
#~ Budg = Budg.interpolate()

""" Calculate storage """
stock = np.zeros(sim.n_t)
for t in range(sim.n_t):
    # underground storage
    stock[t] = (sim.dx*sim.dy*sim.dz*sim.dz_mult_arr*sim.porosity_arr*sim.satur_array[:,:,:,t]).sum(axis=0).sum(axis=0).sum(axis=0)
    # poroelastic storage
    tmp = (sim.dx*sim.dy*sim.dz*sim.dz_mult_arr*sim.specific_storage_arr*sim.satur_array[:,:,:,t]*sim.press_array[:,:,:,t])
    # Veery curious... so the specific storage is actually applied in PF even for < pressure: the following need be commented:
    #~ tmp[tmp<0]=0.
    stock[t]+=tmp.sum(axis=0).sum(axis=0).sum(axis=0)
    # surface storage
    tmp = sim.dx*sim.dy*sim.press_array[:,:,-1,t]
    tmp[tmp<0]=0
    stock[t]+=tmp.sum(axis=0).sum(axis=0)

stock = stock * 1000/catchmentsurf
stock = stock-stock[0]
stock = pd.Series(stock)
stock.index=pd.date_range('1/1/2005 00:00:00', periods=sim.n_t, freq='D')
stock = stock.sort_index()
Budg['WSC']  = stock

""" Prepare yearly budget """
Budg = Budg.sort_index()
BudgYR = Budg.resample('Y').sum()

for i, y in enumerate(stock.index.year.unique()):
    if i ==0:
         BudgYR['WSC'][BudgYR.index.year==y] = stock[stock.index.year==y][-1]
    else:
         BudgYR['WSC'][BudgYR.index.year==y] = stock[stock.index.year==y][-1] - stock[stock.index.year==y-1][-1] 
BudgYR.index=BudgYR.index.year
BudgYR2=BudgYR[['ET','Q','WSC']] #reorder
ax=plot_clustered_stacked([BudgYR2,BudgYR['P']])
#~ BudgYR.plot(kind = 'bar')
#~ ax=plot_clustered_stacked([BudgYR,pd.DataFrame()])
ax.set_ylabel('mm')
plt.savefig(os.sep.join([simdir,'Budg_%s.png'%simuname]),dpi=400,format='png')


#~ for y in Budg.index.year.unique():
    #~ for col in ['Q','P','ET']:
        #~ Budg[col][Budg.index.year==y] = np.cumsum(Budg[col][Budg.index.year==y])
    #~ Budg['WSC'][Budg.index.year==y] = Budg['WSC'][Budg.index.year==y] - Budg['WSC'][Budg.index.year==y][0] 
    
yr=[yr for yr in BudgYR.index]
#~ budgerr = [PYR[PYR.index==yr] - (BudgYR2[BudgYR2.index==yr]).sum(axis=1) for yr in BudgYR2.index]
#~ B=pd.DataFrame()
#~ B['P']=PYR
#~ B['ET']=BudgYR2['ET']
#~ B['Q']=BudgYR2['Q']
#~ B['WSC']=BudgYR2['WSC']
BudgYR.to_csv(simdir+'/Budg_%s.csv'%simuname)
