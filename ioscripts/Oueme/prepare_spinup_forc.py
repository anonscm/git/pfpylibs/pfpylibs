#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    SImple script to prepare var_dz and domains.pfb for Oueme

    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np


"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
inputdir = '/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/input/'


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

veg,h = PFlibs.io_pfb.PFB_read(inputdir+'veg_map.pfb',1)
""" mm/yr => in m3 / D"""
DX = 1000
DY = 1000
P_ET_trees = (1301 - 944)*DX*DY/1000/365
P_ET_herb = (1301 - 791)*DX*DY/1000/365

""" mm/yr => in m / D"""
P_ET_trees = (1301 - 944)/1000/365
P_ET_herb = (1301 - 791)/1000/365

DZ = 3.
dz_1st_layer = 0.09

veg[veg == 7] = P_ET_trees/dz_1st_layer
veg[veg == 8] = P_ET_herb/dz_1st_layer
veg[veg == 19] = P_ET_herb/dz_1st_layer

forc = np.dstack([veg,np.zeros([144,144,6])])
h['nz']=7
h['nz']=7
PFlibs.io_pfb.PFB_write(forc[:,:,::-1],h,inputdir+'P_ET_forc_spinup_veg.pfb')


""" Move Ks to m/D"""
#~ K,h2 = PFlibs.io_pfb.PFB_read(inputdir+'KappZA_gauss30_Dz7_1788_H236.pfb',1)
K,h2 = PFlibs.io_pfb.PFB_read(inputdir+'KappZA_gauss30_Dz7_1788_H20036.pfb',1)
K = K * 24
#~ PFlibs.io_pfb.PFB_write(K[:,:,::-1],h2,inputdir+'KappZA_gauss30_Dz7_1788_H236_MD_spinup.pfb')
PFlibs.io_pfb.PFB_write(K[:,:,::-1],h2,inputdir+'KappZA_gauss30_Dz7_1788_H20036_MD_spinup.pfb')

