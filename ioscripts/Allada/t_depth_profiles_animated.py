#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Extract time - depth slices at given location from single simulation

    @copyright: 2019 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2019"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import datetime
import pandas as pd
from netCDF4 import Dataset
import os,glob
import time
import matplotlib.cm as cm
from scipy import interpolate
from matplotlib.animation import FuncAnimation
import matplotlib.animation as animation
"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout

plt.close("all")

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

daynumber = 0

"""
CLM_var is a dictionnary where keys are local variable names as in PFCLM
values are lists of string[filename, PFCLM variable name, long name, units]
"""
CLM_var={'surf_press':['','h_top','Surface pressure','m']}

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


def getSim(simdir,simuname='ref',xloc=0,yloc=0):
    """
    make a function of all the reading part
    """
    sim = PFout.PFout(simdir,'hillslope')
    sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
    sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
    sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')

    """Read PF outputs"""
    start_time = time.time()
    with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
        sim.satur_array = np.transpose(ds.variables['saturation'][:],[3,2,1,0])
    with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
        sim.press_array = np.transpose(ds.variables['succion'][:],[3,2,1,0])

    """ Read constant files"""
    ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
    #watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
    sim.mannings_arr = np.zeros((sim.nx,sim.ny))+5e-6
    sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,1)
    sim.mask_arr = np.transpose(ds.variables['mask'][:])
    topmask = sim.mask_arr[:,:,0]
    MASK=topmask>0     
    sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,2)
    sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,2)
    #~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))
    sim.dz = np.diff(ds.variables['depth'][:])[0]
    sim.dz_mult_arr= np.array([0.05,0.05,0.05,0.05,0.1,0.1,0.1,0.5,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,1])
    sim.dz_mult_arr = sim.dz_mult_arr[::-1]
    sim.z = np.cumsum(sim.dz_mult_arr*sim.dz) - sim.dz_mult_arr*sim.dz/2
    sim.z = np.round(sim.z*1000)/1000
    # need to hard code the following because for 2D simulation dx = nan which sets Q to 0
    sim.dx = 1000.0

    return sim,MASK
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
#~ simuname1 = 'allada'
#~ simuname1 = 'allada_slow'
#~ simuname1 = 'allada_lowR'
#~ simuname1 = 'allada_lowlowR'
#~ simuname1 = 'allada_lowlowlowR'
#~ simuname1 = 'allada_lowlowlowR_1282yrs'
#~ simuname1 = 'allada_lowlowlowR_2564yrs'
#~ simuname1 = 'allada_lowlowlowR_months'
#~ simuname1 = 'allada_20dry_100wet'
#~ simuname1 = 'allada_20dry_100wet_n2_75'
simuname1 = 'allada_m_n2_75'
#~ simuname1 = 'allada_lowlowR_bc2'
#~ simuname1 = 'allada_lowR_highK'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada_2019_07_25'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada_slow_2019_07_25'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada_lowR_2019_07_25'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada_lowlowR_2019_07_25'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada_lowlowlowR_2019_07_26'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada_lowlowlowR_1282yrs_2019_07_26'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada_lowlowlowR_2564yrs_2019_07_26'
simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada_m_n2_75_2019_07_28'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada_lowlowlowR_months_2019_07_27'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada_20dry_100wet_2019_07_28'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada_20dry_100wet_n2_75_2019_07_28'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada_lowlowR_bc2_2019_07_26'
#~ simdir1 = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada_lowR_highK_2019_07_25'
sim,MASK = getSim(simdir1,simuname1)


slope=0.002
catchmentsurf=np.sum(MASK)*sim.dx*sim.dy


domain_thickness=sim.dz_mult_arr.sum()*sim.dz
# pressure at the bottom cell is obtained at half the cell thickness:
offset_for_WTD = domain_thickness - sim.dz_mult_arr[0]*sim.dz/2
WTD=offset_for_WTD-sim.press_array[0,::-1,0,:]
#~ WTD[WTD<0]=0
y = sim.y
dy = sim.dy
ny = sim.ny
#~ z = np.cumsum(sim.dz_mult_arr[::-1]) 
z = np.cumsum(sim.dz_mult_arr[::-1])*sim.dz 
#adapt the plot to the actual slope: 
# 1) interpolate on a finer matrix, the same dimensions as original, define the finness, as a function of the slope
# 2) place each column on a larger dimension (in Z) matrix by shifting each column depending on slope
dz_step = slope * dy
print('dz_step: %f (topographic diff betw one cell and the next one)'%dz_step)
ndiv_dz_step = 5
#~ ndiv_dz_step = 10
#~ ndiv_dz_step = 20
print('ndiv_dz_step: %d (resolution to discretize this topo diff)'%ndiv_dz_step)
#~ Ztot = 24.0-0.
Ztot = 100.0-0.
ncells_Z = np.int(Ztot / (dz_step/ndiv_dz_step) +1)
print('new number of cells (Ztot / (dz_step/ndiv_dz_step): %d'%ncells_Z)
deniv_tot = slope * (np.max(y)-np.min(y))
print('total deniv: %f'%deniv_tot)
ncells_Z_withslope = np.int(ncells_Z + (ny-1) * ndiv_dz_step)
print('number of cells with slope (ncells_Z + (ny-1) * ndiv_dz_step): %d'%ncells_Z_withslope)

#~ z2 = np.linspace(0,24,101)
#~ z2 = np.linspace(0,24,np.int(ncells_Z))
z2 = np.linspace(0,100,np.int(ncells_Z))
#~ z3 = np.linspace(0,24 * 2 ,201)
#~ z3 = np.linspace(0,24 + np.int(dy*ny*slope) ,np.int(ncells_Z_withslope))
z3 = np.linspace(0,100 + np.int(dy*ny*slope) ,np.int(ncells_Z_withslope))

satur_int1 = np.zeros((1,sim.ny,np.size(z2),sim.n_t))
satur_int2 = np.zeros((1,sim.ny,np.size(z2),sim.n_t))
yy,zz = np.meshgrid(y,z)
for i in range(sim.n_t):
    #~ tmp = np.reshape(sim.satur_array[0,:,:,i],(sim.ny,sim.nz))    
    tmp = np.reshape(sim.satur_array[0,:,::-1,i],(sim.ny,sim.nz))    
    #~ tmp2 = np.reshape(sim2.satur_array[0,:,:,i],(sim.ny,sim.nz))        
    f1 = interpolate.interp2d(z,y,tmp,kind='linear')
    #~ f = interpolate.interp2d(zz,yy,tmp,kind='linear')
    satur_int1[0,:,:,i]=f1(z2,y)

satur_int_slope1 = np.empty((1,sim.ny,np.size(z3),sim.n_t))
satur_int_slope1[:]=np.nan
zdeniv = np.zeros(sim.ny)
zdeniv[1:]=-sim.dy*slope

for i in range(sim.ny):
    #~ satur_int_slope1[0,i,200-(i*2)-101:200-i*2,:] = satur_int1[0,i,:,:]
    #~ satur_int_slope2[0,i,200-(i*2)-101:200-i*2,:] = satur_int2[0,i,:,:]
    satur_int_slope1[0,i,(np.int(ncells_Z_withslope))-(i*ndiv_dz_step)-ncells_Z:(ncells_Z_withslope)-(i*ndiv_dz_step),:] = satur_int1[0,i,:,:]
    
#~ satur_slope = np.nan((1,ny,np.size(z2),n_t))

satur_int_slopem1 = np.ma.masked_where(np.isnan(satur_int_slope1),satur_int_slope1)

#~ fig, (ax1, ax2) = plt.subplots(2, 2,figsize=(20,8))
fig, ax = plt.subplots(1, 1,figsize=(20,8))

cmap=cm.jet_r
cmap.set_bad('w',1.)
#~ pcol1 = ax1[0].pcolormesh(np.array(y),np.array(z),sim.satur_array[0,:,:,0].T,cmap=cmap, shading = 'gouraud')
pcol1 = ax.pcolormesh(np.array(y),np.array(z3),satur_int_slopem1[0,::-1,:,daynumber].T,cmap=cmap, shading = 'gouraud')
plt1 = ax.plot(y,WTD[:,daynumber]-np.cumsum(zdeniv),'k-')
fig.axes[0].invert_yaxis()
ax.margins(x=0)
ax.margins(y=0)
leg = plt.cm.ScalarMappable(cmap=cmap)
leg.set_array([0.,1.])
cbar2 = fig.colorbar(leg, ax = ax)
cbar2.ax.set_title('saturation')
ax.set_ylabel('Depth (m)', fontsize=16, color = 'k')
ax.set_xlabel('Y (m)', fontsize=16, color = 'k')
txt = ax.text(100,4,'year : 0',fontsize = 12)

ylim = 0.1



# define animation:
def update(i):
    global plt1,pcol1,txt
    label = 'timestep {0}'.format(i)
    print(label)
    # Update the line and the axes (with a new xlabel). Return a tuple of
    # "artists" that have to be redrawn for this frame.
    plt1[0].remove()
    pcol1.remove()
    txt.remove()
    #~ pcol1 = ax1[0].pcolormesh(np.array(y),np.array(z),sim.satur_array[0,::-1,:,i].T,cmap=cmap, shading = 'gouraud')
    pcol1 = ax.pcolormesh(np.array(y),np.array(z3),satur_int_slopem1[0,::-1,:,i].T,cmap=cmap, shading = 'gouraud')
    plt1 = ax.plot(y,WTD[:,i]-np.cumsum(zdeniv),'k-')
    #~ txt = ax.text(100,-4,'year : %d'%np.int(i*5/365),fontsize = 12)
    #~ txt = ax.text(100,-4,'year : %d'%np.int(i*365/365),fontsize = 12)
    #~ txt = ax.text(100,-4,'year : %d'%np.int(i*30/365),fontsize = 12)
    txt = ax.text(100,-4,'year : %d'%np.int(i*5),fontsize = 12)
    return (ax)

print('prepare the animation')

#animate
Writer = animation.writers['ffmpeg']

#~ writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)
#~ writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)
writer = Writer(fps=15, metadata=dict(artist='Me'),bitrate=900)
#~ from matplotlib.animation import FFMpegFileWriter   
#~ writer = FFMpegFileWriter(fps=15, metadata=dict(artist='Me'), bitrate=1800)
#~ anim = FuncAnimation(fig, update, frames=np.arange(0, 720, 10), interval=20, repeat=False)
#~ anim = FuncAnimation(fig, update, frames=np.arange(0, 7300, 365), interval=2, repeat=False)
#~ anim = FuncAnimation(fig, update, frames=np.arange(0, 14600, 365), interval=2, repeat=False)
#~ anim = FuncAnimation(fig, update, frames=np.arange(0, 29200, 365), interval=2, repeat=False)
#~ anim = FuncAnimation(fig, update, frames=np.arange(0, 29200, 730), interval=2, repeat=False)
#~ anim = FuncAnimation(fig, update, frames=np.arange(0, 58400, 2920), interval=2, repeat=False)
#~ anim = FuncAnimation(fig, update, frames=np.arange(0, 58400, 2920), interval=2, repeat=False)
anim = FuncAnimation(fig, update, frames=np.arange(0, 1440, 12), interval=2, repeat=False)
#~ anim = FuncAnimation(fig, update, frames=np.arange(0, 4000, 100), interval=2, repeat=False)
anim.save(os.sep.join([simdir1,'sim_allada.mp4']), writer=writer)
#~ plt.show()

from PFlibs import VGM
v1 = VGM.VGMp(name = 'set1')
v1=v1.populate_from_a_tcl_and_geometry_name(r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/inputs/test_forc/last_simu_thatsucceeds.tcl','H1')
v2 = VGM.VGMp(name = 'set2')
v2=v2.populate_from_a_tcl_and_geometry_name(r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/inputs/test_forc/simu.tcl','H1')
V = VGM.VGMps()
V[v1.name] = v1
V[v2.name] = v2
#~ V.plot()
V.plot_hth_Kh()
 

