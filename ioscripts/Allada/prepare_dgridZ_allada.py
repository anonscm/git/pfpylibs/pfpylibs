#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS

    Prepare dgridz matrix for VisIt display for 3D ALlada simulations

    @copyright: 2019 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2019"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd


"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from procsycz import readDataAMMA as rdA
from procsycz import procGeodata_Gdal
##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

dzvar= np.array([0.05,0.05,0.05,0.05,0.1,0.1,0.1,0.5,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,1])
dz = 4
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

""" Reads in a DEM"""
#~ DEMfile = r"/home/hectorb/PARFLOW/PROJECTS/Allada/active/preproc/DEM/Allada.dem.out.raster.txt.asc.tiff"
DEMfile = r"/home/hectorb/PARFLOW/PROJECTS/Allada/active/preproc/DEM/Allada_noLaLama.dem.out.raster.tif"
data,lon,lat = procGeodata_Gdal.readRasterWithGdal(DEMfile,nodata_value = 32767)
data[data==32767]=0
data[data<=0]=0
data = data[::-1,:]
[lonlon,latlat]=np.meshgrid(lon,lat)

""" Prepare DgridZ """
zloc = np.cumsum(dzvar*dz) - dz*dzvar/2 #center of cells
final_grid = data[:,:,None] - zloc[None,None,:]
initial_grid = np.zeros(np.shape(data))[:,:,None] + np.arange(24,-1,-1)
dgridz = final_grid-initial_grid
dgridz  = np.swapaxes(dgridz,0,1)

"""Reads in an arbitrary PFB from the simulation"""
#~ pf,hdr = PFlibs.io_pfb.PFB_read(r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/inputs/Allada3D/allada3D.press.4000.pfb',1)
#~ pf,hdr = PFlibs.io_pfb.PFB_read(r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/inputs/Allada3D/Xslopes_allada.pfb',1)
pf,hdr = PFlibs.io_pfb.PFB_read(r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/preproc/DEM/Xslopes_allada_noLaLama.pfb',1)
#~ hdr['ix']= int(hdr['X0'])
#~ hdr['iy']= int(hdr['Y0'])
#~ hdr['ns']= 1
#~ hdr['nx2']= hdr['nx']
hdr['nz']= 25
hdr['nz2']= 25

#~ PFlibs.io_pfb.PFB_write(dgridz[:,:,::-1],hdr,r"/home/hectorb/PARFLOW/PROJECTS/Allada/active/preproc/DEM/dgridZ_noLaLama.pfb")
