#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPT

    Prepare tcl forcings:
    script to prepare the tcl for running parflow only and have time variable BC...
    

    @copyright: 2019 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2019"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime
import os, glob

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from PFlibs import io_tcl


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/inputs/Allada3D'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/inputs'
#~ testdir = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/inputs/TimeVarForcings'

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

"""Read P"""
PrecipOu =  pd.read_csv(r'/home/hectorb/DATA/WT/Oueme/AMMA_CATCH/aux_variables/precip/PrecipOu.csv')
PrecipOu = PrecipOu.set_index(PrecipOu.columns[0])
PrecipOu.index = pd.to_datetime(PrecipOu.index,format='%Y-%m-%d %H:%M:%S')
PrecipOu.index.rename('Date',inplace=True)
""" P have been wrongly associated to 30mn time series while they are daily values. Correct this:"""
corrP = pd.DataFrame()
for yr in PrecipOu.resample('Y').sum().index.year.values:
    PrecipOu.loc[PrecipOu.index.year==yr,:]
    #~ corrP=pd.concat([corrP,pd.DataFrame(data = PrecipOu.loc[PrecipOu.index.year==yr,:].values, columns=PrecipOu.columns, index = pd.Series(pd.date_range(pd.datetime(yr,1,1),pd.datetime(yr,12,31), freq = 'D')))])
    corrP=pd.concat([corrP,pd.DataFrame(data = PrecipOu.loc[PrecipOu.index.year==yr,:].values, columns=PrecipOu.columns, index = pd.Series(pd.date_range(pd.datetime(yr,1,1),periods = PrecipOu.loc[PrecipOu.index.year==yr,:].values.shape[0], freq = 'D')))])

#~ P=corrP.loc[:,'ANANINGA'].resample('M').sum()

#~ P = pd.read_excel('/home/hectorb/PARFLOW/PROJECTS/Allada/data/pluie_annuelle-lite.xlsx')
P = pd.read_excel('/home/hectorb/PARFLOW/PROJECTS/Allada/data/pluie_tt_station_1950-lite.xlsx',sheet_name = 'Cotonou_Aero')
P.set_index(P.columns[0],inplace=True)
P.dropna().resample('Y').sum().rolling(10).mean().plot()
#inspect nans
plt.pcolor(P.isnull())

Pmonth = P.dropna().resample('30D').sum() #beware this will shift 5days every year so 250days after 50 yrs
nmonths = len(Pmonth)
Pday = P.dropna().resample('1D').sum()
ndays = len(Pday)

Rfactor = P.dropna().resample('Y').sum().mean() / 0.51 #0.51mm/yr is the average R rate used to force the spinup
#~ Rfactor = P.dropna().resample('Y').sum().mean() / 5.1 #5.1mm/yr is the average R rate used to force the spinup
#~ Rfactor = P.dropna().resample('Y').sum().mean() / 51.0 #51mm/yr is the average R rate used to force the spinup
#~ Rfactor = P.dropna().resample('Y').sum().mean() / 100.0 #51mm/yr is the average R rate used to force the spinup
#~ Rfactor = P.dropna().resample('Y').sum().mean() / 20.0 #20mm/yr is the average R rate used to force the spinup

Pmonth = Pmonth/Rfactor
Pmonth.rename(columns = {'Pluie journalière':'Pmonth'},inplace = True)
Pday = Pday/Rfactor
Pday.rename(columns = {'Pluie journalière':'Pday'},inplace = True)

n = nmonths
#~ n = ndays

k = "Cycle.rainrec.Names"
v= '"' + ' '.join([str(v) for v in range(n)]) + '"'
dic = {k: v}
#~ for i in range(nmonths):
for i,p in enumerate(Pmonth.Pmonth.sort_index().values):    
#~ for i,p in enumerate(Pday.Pday.sort_index().values):    
    k = 'Cycle.rainrec.%d.Length'%i
    v= 1
    dic[k] = v
    k = 'Patch.z-upper.BCPressure.%d.Value'%i
    v = p / 1000 #mm ->m
    dic[k] = -v

"""Example for creating an artificial seasonal forcings on the south of Benin"""
"""
nmonths = 50*12
nyrs_dry = 20
nyrs_wet = 108

k = "Cycle.rainrec.Names"
v= '"' + ' '.join([str(v) for v in range((nyrs_dry+nyrs_wet)*12)]) + '"'
dic = {k: v}
for i in range((nyrs_dry+nyrs_wet)*12):
    k = 'Cycle.rainrec.%d.Length'%i
    v= 30
    dic[k] = v
for i in range(nyrs_dry):
    for j in range(12):
        k = 'Patch.z-upper.BCPressure.%d.Value'%(i*12+j)
        v= 0
        dic[k] = v
for i in np.arange(nyrs_dry,nyrs_dry+nyrs_wet,1):
    for j in range(3):
        k = 'Patch.z-upper.BCPressure.%d.Value'%(i*12+j)
        v= 0
        dic[k] = v
    for j in np.arange(3,6,1):
        k = 'Patch.z-upper.BCPressure.%d.Value'%(i*12+j)
        v= -0.00000343  # 0.51mm/an
        dic[k] = v
    for j in np.arange(6,8,1):
        k = 'Patch.z-upper.BCPressure.%d.Value'%(i*12+j)
        v= 0
        dic[k] = v
    for j in np.arange(8,10,1):
        k = 'Patch.z-upper.BCPressure.%d.Value'%(i*12+j)
        v= -0.00000343
        dic[k] = v
    for j in np.arange(10,12,1):
        k = 'Patch.z-upper.BCPressure.%d.Value'%(i*12+j)
        v= 0
        dic[k] = v
"""
    
    
#~ tclfile = '/'.join([simdir,'test.tcl'])
tclfile = '/'.join([simdir,'allada2D_month.tcl'])
#~ tclfile = '/'.join([simdir,'allada2D.tcl'])
#~ tclfile = '/'.join([simdir,'allada3D_month.tcl'])
#~ tclfile = '/'.join([simdir,'allada3D_month_R51.tcl'])
#~ tclfile = '/'.join([simdir,'allada3D_month_R20.tcl'])
#~ tclfile = '/'.join([simdir,'allada3D_5d_R100_K5_4.tcl'])
#~ newtcl = tclfile.replace('.tcl', '_new.tcl')
tcllines = io_tcl.TclLines(filename = tclfile)
res=tcllines.copy() 
#~ key ="Cycle.rainrec.0.Length"    
key = "Patch.z-upper.BCPressure.0.Value"
key = "Cycle.rainrec.Names"
        #~ keys = self.setkeys()
        #~ print('change key %s'%key)
        #~ idxs = keys == key
        #~ res = keys[idxs].index
        #~ assert (len(res) == 1) | (len(res) ==0), "Dont know what to do.\n Same key are present.Lines {0} key = %s".format(res)%key
        #~ # Modifs BH. Former GP is simply res = int(res[0]), return res
        #~ if len(res) == 0:
            #~ if create_if_doesnt_exist:
                #~ #the key does not exist, create it (close to the closest key?):
                #~ close_key = difflib.get_close_matches(key,self.setkeys().values)[0]
                #~ if close_key:
                    #~ print('closest key of %s: %s'%(key,close_key))
                    #~ self, close_idx = self.copy().get_idx(close_key)
                    #~ tmp = self.tolist()
                    #~ tmp.insert(close_idx+1,'pfset %s whatever'%key)
                    #~ self = TclLines(pandas.Series(tmp))
                    #~ res = [close_idx+1]
                    #~ print('create key: %s at line %d'%(key,close_idx+1))
                #~ else: print('no close key found... find a way to assign a certain position')
            #~ else: print('problem: the key does not exist, should we create it?')
        #~ res = int(res[0])
#~ res.get_idx(key)
#~ keys = res.setkeys()
#~ idxs = keys == key
#~ res2 = keys[idxs].index
#~ import difflib
#~ close_key = difflib.get_close_matches(key,res.setkeys().values)[0]
#~ idxs2 = keys == close_key   



#~ set_lines = tcllines.is_setline()
#~ set_lines = tcllines.get_setlines()
#~ split_lines = tcllines.to_tclsets()
#~ keytochange = "OverlandFlowSpinUp"
#~ new = tcllines.change_param(keytochange,4)
#~ new.write(filename = newtcl)
#~ ((new.to_series() == tcllines.to_series()) == False).sum()
#~ print("warning if two key are present")

#~ k= "Geom.domain.Perm.TensorValZ"


simuname = os.path.join(simdir,'test_forc')

simus = PF.Simus()
simus = simus.add_simu(simuname, **dic)



### parameters sampling:
#~ sims.populate_SimuDic(gen_run_dir='./test/simtest/sample',
#~ key_tochange='OverlandFlowSpinUp',samplingtype='uniform',
#~ sample_param1=0,sample_param2=12,sample_N=4)

### simulations init & run:
simus.create_rundirs()
simus.create_new_tcls(tclfile)
simus.cp_allinputfiles(tclfile)
#~ simus.run_simulations()


### outputs:
#~ folder_pattern = os.path.join(testdir,'sample*')
#~ tcl_pattern = 'simu.tcl'
#~ simus_out = Simus()

#~ simus_out = simus_out.populate_simus_from_patterns(folder_pattern, tcl_ref = tclfile, tcl_pattern = tcl_pattern)
#~ folder_pattern = '/homelocal5/phyrev/_PARFLOW/PF_lectures/hillslope_course/hillslope2017/SAINTSULPICE/results*'
#~ tcl_ref = '/homelocal5/phyrev/_PARFLOW/PF_lectures/hillslope_course/hillslope2017/SAINTSULPICE/hillslope_ref.tcl'
#~ tcl_pattern = 'hillslope*.tcl'

#~ simus_out = simus_out.populate_simus_from_patterns(folder_pattern, tcl_ref = tclfile, tcl_pattern = tcl_pattern)

