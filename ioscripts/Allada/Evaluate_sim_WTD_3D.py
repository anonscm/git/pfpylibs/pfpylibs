#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS
    Allada:
    
    Evaluate 3D Allada simulation with respect to WTD
    
    @copyright: 2019 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2019"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import datetime
import pandas as pd
from netCDF4 import Dataset
import os,glob
import time
import matplotlib.cm as cm
from scipy import interpolate
from matplotlib.animation import FuncAnimation
import matplotlib.animation as animation
import xarray as xr

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout

plt.close("all")

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

simuname = 'allada3D_n2_75'
simuname = 'allada3D_n2_75_trans'
simuname = 'allada3D_n2_75_R51_trans'
#~ simuname = 'allada3D_R20_trans'
simuname = 'allada3D_R20_poro10_trans'
simuname = 'allada3D_R20_poro10_noLaLama_trans'

#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada_m_n2_75_2019_07_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada_20dry_100wet_n2_75_2019_07_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada3D_n2_75_2019_08_02'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada3D_n2_75_trans_2019_08_02'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada3D_n2_75_R51_trans_2019_08_02'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada3D_R20_trans_2019_08_29'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada3D_R20_poro10_trans_2019_08_29'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada3D_R20_poro10_noLaLama_trans_2019_08_29'
slope=0.002

#this amplification factor is to stretch the amplitude of simulated WTD for visualisation purposes
amplification_factor = 10

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##


sim = PFout.PFout(simdir,'allada3D')
sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')

"""Read PF outputs"""
start_time = time.time()
with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
    sim.satur_array = np.transpose(ds.variables['saturation'][:],[3,2,1,0])
with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
    sim.press_array = np.transpose(ds.variables['succion'][:],[3,2,1,0])

""" Read constant files"""
ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
#watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
sim.mannings_arr = np.zeros((sim.nx,sim.ny))+5e-6
sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,1)
sim.mask_arr = np.transpose(ds.variables['mask'][:])
topmask = sim.mask_arr[:,:,0]
MASK=topmask>0     
sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,2)
sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,2)
#~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))
sim.dz = np.diff(ds.variables['depth'][:])[0]
sim.dz_mult_arr= np.array([0.05,0.05,0.05,0.05,0.1,0.1,0.1,0.5,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,1])
sim.dz_mult_arr = sim.dz_mult_arr[::-1]
sim.z = np.cumsum(sim.dz_mult_arr*sim.dz) - sim.dz_mult_arr*sim.dz/2
sim.z = np.round(sim.z*1000)/1000
# need to hard code the following because for 2D simulation dx = nan which sets Q to 0
sim.dx = 1000.0

catchmentsurf=np.sum(MASK)*sim.dx*sim.dy

"""Forcings"""
P = pd.read_excel('/home/hectorb/PARFLOW/PROJECTS/Allada/data/pluie_tt_station_1950-lite.xlsx',sheet_name = 'Cotonou_Aero')
P.set_index(P.columns[0],inplace=True)

Pmonth = P.dropna().resample('30D').sum() #beware this will shift 5days every year so 250days after 50 yrs
nmonths = len(Pmonth)

#~ Rfactor = P.dropna().resample('Y').sum().mean() / 5.1 #5.1mm/yr is the average R rate used to force the spinup
Rfactor = P.dropna().resample('Y').sum().mean() / 51 #51mm/yr is the average R rate used to force the spinup

Pmonth = Pmonth/Rfactor
Pmonth.rename(columns = {'Pluie journalière':'Pmonth'},inplace = True)


""" Analysis WTD"""
domain_thickness=sim.dz_mult_arr.sum()*sim.dz
# pressure at the bottom cell is obtained at half the cell thickness:
offset_for_WTD = domain_thickness - sim.dz_mult_arr[0]*sim.dz/2
WTD=offset_for_WTD-sim.press_array[:,::-1,0,:]

#~ depth_last_cell = sim.z[-1]
#~ WT = depth_last_cell - sim.press_array[xloc,yloc,0,:]




#~ WTD[WTD<0]=0
y = sim.y
dy = sim.dy
ny = sim.ny
#~ z = np.cumsum(sim.dz_mult_arr[::-1]) 
z = np.cumsum(sim.dz_mult_arr[::-1])*sim.dz 

#~ tsim = pd.date_range('1/1/1900 00:00:00', periods=np.shape(WTD)[1], freq='M')
tsim = Pmonth.index.values
#~ tsim = Pmonth.index.values
#~ tsim = pd.date_range('1/1/0000',periods = 4001,freq = 'Y')
WT = pd.DataFrame()
for i,lon in enumerate(WTD.mean(axis=2).argmax(axis=0)):
    WT = pd.concat([WT,pd.Series(WTD[lon,i,1::],name = i,index = pd.to_datetime(tsim))],axis = 1)
#~ WT.set_index(tsim)
#~ WT = pd.DataFrame(data = WTD.T,columns=y[::-1]/1000,index =tsim)
ax = WT.plot(subplots = True,layout = [10,7],figsize=[20,10])
plt.gcf().subplots_adjust(bottom=0.06, top =0.98,left=0.05,right =0.96,wspace=0.00, hspace=0.000)
for a in ax:
    for b in a:
        b.legend(fontsize=7,loc = 'upper right')
        b.tick_params(axis="y",direction="in",pad = -30,labelsize = 7)
        b.invert_yaxis()
plt.savefig(os.sep.join([simdir,simuname + '_timeseries.png']),dpi=150,format='png')

""" Read in WT data"""

D = pd.read_excel('/home/hectorb/PARFLOW/PROJECTS/Allada/data/Data_Piezo-tt.xlsx',sheet_name = 'Data')
WTloc = pd.read_excel('/home/hectorb/PARFLOW/PROJECTS/Allada/data/site-piezo-meteo.xlsx',sheet_name = 'site-piezo')
WTloc.set_index('Site',inplace = True)


WTd = pd.DataFrame()
for i in range(13):
    print(i)
    WTd = pd.concat([WTd,D.iloc[:,i*3:i*3+2].set_index(D.columns[i*3]).dropna().rename_axis("date")],axis = 1)

# from https://towardsdatascience.com/the-unreasonable-effectiveness-of-method-chaining-in-pandas-15c2109e3c69
fn = lambda x: x.columns.str.replace(r" \(prof\)","") 
WTd.columns = fn(WTd)


######## BEWARE HERE WHEN PLOTTING MULTIPLE COLUMNS OF A DD WITH NANs:
######## DEFAULT PLOT STYLE IS LINE, and only consecutive data points are therefore plotted!!!
######## WORKAROUND: USE OTHER MARKER STYLE OR PLOT EACH COLUMN INDIVIDUALLY USING DROPNA
ax = WTd.plot(figsize = [20,10],marker='o',markersize=2)
ax.invert_yaxis()
ax.set_ylabel('WTD (m)')
ax.set_xlim([datetime.datetime(2007,1,1),datetime.datetime(2019,1,1)])

""" Read in sim outputs using xarray"""
ds = xr.open_dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]),decode_times=False)
tsim_full = np.insert(tsim,0,tsim[0]- (tsim[1]-tsim[0]))
ds = ds.assign_coords(time = pd.to_datetime(tsim_full))
#example:
fig,ax = plt.subplots(1,1,figsize = [7,7])
#~ ds.succion.mean(dim='time').mean(dim='depth').plot(ax=ax)
#~ (ds.succion[:,0,:,:].mean(dim='time')).plot(ax=ax,robust=True)
(offset_for_WTD-ds.succion[:,0,:,:].mean(dim='time')).plot(ax=ax,cmap = 'viridis_r',robust=True, cbar_kwargs={'label':'WTD (m)'})
for station in WTloc.Y.index:
    ax.plot(WTloc.loc[station,'X'],WTloc.loc[station,'Y'],'+','r')
    ax.text(WTloc.loc[station,'X'],WTloc.loc[station,'Y'],station,color ='r')
ax.set_title("")
ax.set_aspect('equal')
plt.savefig(os.sep.join([simdir,simuname + '_WTDmap.png']),dpi=250,format='png')

fig,ax = plt.subplots(WTloc.shape[0],1,figsize = [5,10])
for i,station in enumerate(WTloc.Y.sort_values(ascending = False).index):
    (offset_for_WTD-ds.succion.sel(latitude = WTloc.loc[station,'Y'], longitude = WTloc.loc[station,'X'],depth = 0,method = 'nearest',tolerance =500)).plot(ax = ax[i],color='r')
    WTd.loc[:,station].plot(ax = ax[i],color = 'k',marker='.',markersize=1)
    #~ ax[i].legend(station)
    ax[i].set_title('')
    ax[i].set_xlabel('')
    ax[i].set_ylabel('')
    ax[i].set_ylim([-3,70])
    ax[i].text(ax[i].get_xlim()[0],15,'%s, D. to sea = %2.0f km'%(station,(WTloc.loc[station,'Y']-700520)/1000)) #700520 is approximately coastline location
    ax[i].invert_yaxis()
ax[0].set_ylabel('WTD (m)')    
ax[0].legend(["sim","obs"],ncol=2,loc = 'upper right',fontsize = 8)
plt.gcf().subplots_adjust(bottom=0.05, top =0.95,left=0.2, hspace=0.001,wspace=0.001)
plt.savefig(os.sep.join([simdir,simuname + '_timeseries_vs_Obs.png']),dpi=250,format='png')

fig,ax = plt.subplots(WTloc.shape[0],1,figsize = [5,10])
for i,station in enumerate(WTloc.Y.sort_values(ascending = False).index):
    simtmp = (offset_for_WTD-ds.succion.sel(latitude = WTloc.loc[station,'Y'], longitude = WTloc.loc[station,'X'],depth = 0,method = 'nearest',tolerance =500))
    #magnify the simulation if needed:
    simtmp2 = (simtmp-simtmp.sel(time=slice('2007-01-01', '2019-01-01')).mean(dim='time').values)*amplification_factor
    simtmp2.plot(ax = ax[i],color='r')
    #~ obstmp = WTd.loc[:,station].dropna()
    obstmp = WTd.loc[:,station]
    (obstmp -obstmp.mean()).plot(ax = ax[i],color = 'k',marker='.',markersize=1)
    #~ ax[i].legend(station)
    ax[i].set_title('')
    ax[i].set_xlabel('')
    ax[i].set_ylabel('')
    ax[i].set_xlim([datetime.datetime(2007,1,1),datetime.datetime(2019,1,1)])
    ax[i].set_ylim([-3.7,2])
    ax[i].text(ax[i].get_xlim()[0],-2.5,'%s, D. to sea = %2.0f km'%(station,(WTloc.loc[station,'Y']-700520)/1000)) #700520 is approximately coastline location
    ax[i].invert_yaxis()
    if i<(len(WTloc.index)-1):
        ax[i].set_xticklabels('')
ax[0].set_ylabel('WTD (m)')    
#~ ax[0].legend(["sim x10","obs"],ncol=2)
#~ ax[0].legend(["sim x %1.1f"%amplification_factor,"obs"],ncol=2)
ax[0].legend(["sim","obs"],ncol=2,loc = 'upper right',fontsize = 8)
plt.gcf().subplots_adjust(bottom=0.05, top =0.95,left=0.2, hspace=0.001,wspace=0.001)
plt.savefig(os.sep.join([simdir,simuname + '_timeseries_vs_Obs_rescaled.png']),dpi=250,format='png')

