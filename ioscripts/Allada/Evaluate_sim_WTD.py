#-*- coding: utf-8 -*-
"""
    PFPYLIBS - IOSCRIPTS
    Allada:
    
    Evaluate 2D hillslope simulation with respect to WTD
    
    @copyright: 2019 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2019"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import datetime
import pandas as pd
from netCDF4 import Dataset
import os,glob
import time
import matplotlib.cm as cm
from scipy import interpolate
from matplotlib.animation import FuncAnimation
import matplotlib.animation as animation
"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout

plt.close("all")

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

simuname = 'allada_m_n2_75'
#~ simuname = 'allada_20dry_100wet_n2_75'
simuname = 'allada_20dry_100wet'

#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada_m_n2_75_2019_07_28'
#~ simdir = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada_20dry_100wet_n2_75_2019_07_28'
simdir = r'/home/hectorb/PARFLOW/PROJECTS/Allada/active/NCout/allada_20dry_100wet_2019_07_28'
slope=0.002

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##


sim = PFout.PFout(simdir,'hillslope')
sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')

"""Read PF outputs"""
start_time = time.time()
with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
    sim.satur_array = np.transpose(ds.variables['saturation'][:],[3,2,1,0])
with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
    sim.press_array = np.transpose(ds.variables['succion'][:],[3,2,1,0])

""" Read constant files"""
ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
#watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
sim.mannings_arr = np.zeros((sim.nx,sim.ny))+5e-6
sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,1)
sim.mask_arr = np.transpose(ds.variables['mask'][:])
topmask = sim.mask_arr[:,:,0]
MASK=topmask>0     
sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,2)
sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,2)
#~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))
sim.dz = np.diff(ds.variables['depth'][:])[0]
sim.dz_mult_arr= np.array([0.05,0.05,0.05,0.05,0.1,0.1,0.1,0.5,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,1])
sim.dz_mult_arr = sim.dz_mult_arr[::-1]
sim.z = np.cumsum(sim.dz_mult_arr*sim.dz) - sim.dz_mult_arr*sim.dz/2
sim.z = np.round(sim.z*1000)/1000
# need to hard code the following because for 2D simulation dx = nan which sets Q to 0
sim.dx = 1000.0

catchmentsurf=np.sum(MASK)*sim.dx*sim.dy


domain_thickness=sim.dz_mult_arr.sum()*sim.dz
# pressure at the bottom cell is obtained at half the cell thickness:
offset_for_WTD = domain_thickness - sim.dz_mult_arr[0]*sim.dz/2
WTD=offset_for_WTD-sim.press_array[0,::-1,0,:]

#~ depth_last_cell = sim.z[-1]
#~ WT = depth_last_cell - sim.press_array[xloc,yloc,0,:]


#~ WTD[WTD<0]=0
y = sim.y
dy = sim.dy
ny = sim.ny
#~ z = np.cumsum(sim.dz_mult_arr[::-1]) 
z = np.cumsum(sim.dz_mult_arr[::-1])*sim.dz 

tsim = pd.date_range('1/1/1900 00:00:00', periods=np.shape(WTD)[1], freq='M')
WT = pd.DataFrame(data = WTD.T,columns=y[::-1]/1000,index =tsim)
ax = WT.plot(subplots = True,layout = [10,7],figsize=[20,10])
plt.gcf().subplots_adjust(bottom=0.06, top =0.98,left=0.05,right =0.96,wspace=0.00, hspace=0.000)
for a in ax:
    for b in a:
        b.legend(fontsize=7,loc = 'upper right')
        b.tick_params(axis="y",direction="in",pad = -30,labelsize = 7)
        b.invert_yaxis()
plt.savefig(os.sep.join([simdir,simuname + '_timeseries.png']),dpi=400,format='png')

