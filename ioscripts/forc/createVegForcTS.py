##-*- coding: utf-8 -*-
"""
	PFPREPROC - create vegetation forcings time series

	DESCRIPTION
	Base script to produce vegetation forcings (LAI, SAI, DISPLA, Z0M)
	for use with PF-CLM.
	In the future, parts of the script could be set as functions in a module
	
	Takes LAI at Nalohou (herbaceous) and Bele (Trees), and also precip 
	to get HVEG from an API-like model. Use HVEG to scale LAI
	=> Get LAI, SAI, HVEG for Herbaceous, Trees and Shrubs
	(that's one panda DF that can easily be written out if needed)
	
	=> Then derive SAI, DISPLA and Z0M for Herbaceous, Trees and Shrubs
	(all other classes are set to LAI, SAI of Herbaceous and 
	Z0M, DISPLA from CLM input file (drv_vegp)


	@copyright: 2018 by Basile HECTOR <basile.hector@ird.fr>
	@license: GNU GPL, see COPYING for details.
"""

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import numpy as np
import pandas as pd
import datetime
import matplotlib.pyplot as plt
import os

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

"""path to vegetation/precip time series:"""
sourcedir = "/home/hectorb/DATA/VEG/vegetation_TS/"
sourcedir_rain = "/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/inputs/forcage/"
#~ outdir = './test/tests_io/'
outdir = '/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/vshape2018/'
outdir = '//home/hectorb/PARFLOW/PROJECTS/Ara/resolution/inputs/ara2019/'
# Corrected LAI for forests!!! + ripisylve (as of 29/05/2020)
outdir = '//home/hectorb/PARFLOW/PROJECTS/Hillslope/bele_nalo_tests/forcings_11yrs/'

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


""" Burning dates dictionary for Nalohou key= year , value = [DoY, date, flag(1=local data, 2=arbitrary)]"""
#~ Burning_dates={2005:[335,datetime.datetime(2005,12,1),1],\
				#~ 2006: [335,datetime.datetime(2006,12,1),1],\
				#~ 2007: [336,datetime.datetime(2007,12,2),1],\
				#~ 2008: [305,datetime.datetime(2008,10,31),1],\
				#~ 2009: [352,datetime.datetime(2009,12,18),1],\
				#~ 2010: [344,datetime.datetime(2010,12,10),1],\
				#~ 2011: [335,datetime.datetime(2011,12,1),2],\
				#~ 2012: [336,datetime.datetime(2012,12,1),2],\
				#~ 2013: [335,datetime.datetime(2013,12,1),2],\
				#~ 2014: [335,datetime.datetime(2014,12,1),2],\
				#~ 2015: [335,datetime.datetime(2015,12,1),2]}
Burning_dates={2005:[335,datetime.datetime(2005,12,1),1],\
				2006: [335,datetime.datetime(2006,12,1),1],\
				2007: [336,datetime.datetime(2007,12,2),1],\
				2008: [305,datetime.datetime(2008,10,31),1],\
				2009: [352,datetime.datetime(2009,12,18),1],\
				2010: [344,datetime.datetime(2010,12,10),1],\
				2011: [365,datetime.datetime(2011,12,31),2],\
				2012: [365,datetime.datetime(2012,12,31),2],\
				2013: [365,datetime.datetime(2013,12,31),2],\
				2014: [365,datetime.datetime(2014,12,31),2],\
				2015: [365,datetime.datetime(2015,12,31),2]}
				
vegclasses={1:'evergreen needleleaf forests',2:'evergreen broadleaf forests',\
			3:'deciduous needleleaf forests', 4: 'deciduous broadleaf forests',\
			5: 'mixed forests6 closed shrublands', 6:'closed shrublands',\
			7: 'open shrublands', 8: 'woody savannas',\
			9: 'savannas', 10: 'grasslands',\
			11: 'permanent wetlands', 12: 'croplands',\
			13: 'urban and built-up lands', 14: 'cropland / natural vegetation mosaics',\
			15: 'snow and ice', 16: 'barren or sparsely vegetated',\
			17: 'water bodies', 18: 'bare soil',\
			19: 'herbaceous'}				
			
def readVegForc(filename,\
				ts = pd.date_range('1/1/2005 00:00:00','1/1/2016 00:00:00', freq='30min'),\
				plot=False):
	"""
	Simple function to read a vegetation forcing file
	return a panda series
	"""
	df=pd.read_csv(filename,header=None, delimiter=r"\s+")
	df.rename(columns=lambda x: x+1, inplace=True)
	df = df.drop(df.columns[np.append(np.arange(0,6),np.arange(8,18))],axis=1)
	df.rename(columns=lambda x: vegclasses[x], inplace=True)

	df['time']=ts
	df=df.set_index('time')
	if plot:
		plt.figure()
		df.plot()
		plt.show()

	

def minimalist_xldate_as_datetime(xldate, datemode):
    # datemode: 0 for 1900-based, 1 for 1904-based
    return (
        datetime.datetime(1899, 12, 30)
        + datetime.timedelta(days=xldate + 1462 * datemode)
        )
        
def roundDateTime(shakyDT,nearest_minutes=10):
	"""
	"""
	shakyDT += datetime.timedelta(minutes=nearest_minutes/2)
	shakyDT -= datetime.timedelta(minutes=shakyDT.minute % nearest_minutes,
                         seconds=shakyDT.second,
                         microseconds=shakyDT.microsecond)
	return shakyDT
	
def APImodel_vegheight(rain,a=0.033, b=0.39, dt_api=1):
	"""
	API model for vegetation height:
	dt_api: number of days
	rain time series should be in days if other parameters are in days too
	Many ways to do that, could use a classical np array, or create a new
	shifted column in a df (but that works if the reference to previous index
	is stable through the iteration (i.e. i-2, and not i-k-2 where k is another
	variable (that would imply to make several more shifted columns)
	"""
	API=np.zeros(np.shape(rain))
	h_api=np.zeros(np.shape(rain))
	for j in np.arange(1,len(rain)):
		API[j] = API[j-dt_api]*np.exp(-a*dt_api)+rain[j]
		""" to be similar to MATLAB script which was wrong because taking
		30mn rain instead of daily right here, but produce correct output"""
		if rain[j] > 1000:
			dt_api+=1
		else:
			dt_api=1
		#~ if API[j]>15:
		if API[j]>15:
			h_api[j]=h_api[j-1]+b*np.max([API[j-1]-API[j],0])
			#~ dt_api+=1
		else:
			h_api[j]=h_api[j-1]
			#~ dt_api=1			
	return h_api
    
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##
	
"""get data"""    
filename = sourcedir + 'LAI_fcover_Nalo_composite.csv'
Nalodf = pd.read_csv(filename, sep=';')
Nalodf['date'] = Nalodf['date'].apply(lambda x: minimalist_xldate_as_datetime(x, 0))
Nalodf = Nalodf.set_index('date')
Nalodf = Nalodf.drop(Nalodf.columns[[1,2,3,4]], axis=1)

"""Early LAI process:
!!!!!! BEWARE: that does not currently take into account bissextile years !!!
=> it actually does, when the time series is being interpolated at 30mn

Remove year 2005, replace by 2007 and add some more years by 
duplicating 2007. Also add 01/01/2005 and then interpolate from sparse days to 30mn,
otherwise the TS starts 5th of Jan:"""
Nalodf = Nalodf[Nalodf.index.year!=2005]
baseyr=2007
yrs_to_add = np.append(2005,np.arange(2009,2016))
for yr in yrs_to_add:
	tmp=Nalodf[Nalodf.index.year==baseyr]
	tmp.index=tmp.index.map(lambda t: t.replace(year = yr))
	Nalodf=pd.concat([Nalodf,tmp])
Nalodf.loc[datetime.datetime(2005,1,1)]=0.0 # set it at the end of the df
Nalodf.loc[datetime.datetime(2016,1,1)]=0.0 # set it at the end of the df
LAI = Nalodf.resample('30min').mean().interpolate()
LAI[LAI.columns[0]].name='LAI'

"""
Creating HVEG time series:
- read precip data
- use a calibrated API model to produce vegetation height time series
"""

"""Read precip data"""
df = pd.DataFrame()
print("read precip")
for yr in np.arange(2005,2016):
	if yr>=2013:
		"""Nalohou 3 data only..."""
		tmpdf=pd.read_csv(sourcedir_rain + 'Pluie_30mn_' + np.str(yr) + '_BH_st.txt',header=None, delimiter=r"\s+")
		tmpdf[1]=tmpdf[1]*60*30 #mm/s -> mm
		for i in range(7): tmpdf[2+i]=tmpdf[i+1]#replicate 8times (in total) the Nalohou time series. Should use Bele as well
		for i in np.arange(5,9): tmpdf[i]=tmpdf[i].cumsum() #4th last col are expected to be cumsum of 4 first
	elif yr==2005:#get another yr:
		tmpdf=pd.read_csv(sourcedir_rain + 'Pluie_30mn_' + np.str(2007) + '_st.txt',header=None, delimiter=r"\s+")
	elif yr==2011:#for some reason col 4 has many -9999 while col 8 is ok...!
		tmpdf=pd.read_csv(sourcedir_rain + 'Pluie_30mn_' + np.str(yr) + '_st.txt',header=None, delimiter=r"\s+")
		tmpdf[4] = np.append([0],np.diff(tmpdf[8]))
	else:
		tmpdf=pd.read_csv(sourcedir_rain + 'Pluie_30mn_' + np.str(yr) + '_st.txt',header=None, delimiter=r"\s+")

	if yr==2005:
		tmpdf[0]=tmpdf[0].apply(lambda x: roundDateTime(minimalist_xldate_as_datetime(x, 0)).replace(year = yr))

	else:
		tmpdf[0]=tmpdf[0].apply(lambda x: roundDateTime(minimalist_xldate_as_datetime(x, 0)))	
	tmpdf=tmpdf.set_index(0)	
	"""cleanup january 1st of yr+1 if present"""
	tmpdf=tmpdf[tmpdf.index.year==yr]
	df = pd.concat([df,tmpdf])		

""" Run calibrated API model"""
#~ P = df[1]
# columns are Bele, Bira, Djougou, Nalohou 3
P = df[4]
#~ P[P.index.year==2011] = df[4][df.index.year==2011]
P_d=P.resample('D').sum()
hvegdf = pd.DataFrame()
for yr in np.arange(2005,2016):
	tmp_hveg = APImodel_vegheight(P_d[P_d.index.year == yr].as_matrix(),a=0.033, b=0.39, dt_api=1)
	tmp_hvegdf = pd.DataFrame(tmp_hveg,index=P_d[P_d.index.year == yr].index)
	tmp_hvegdf[0][(tmp_hvegdf.index.year == yr) & (tmp_hvegdf.index >= Burning_dates[yr][1])]=0
	hvegdf = pd.concat([hvegdf,tmp_hvegdf])	
	
""" Process LAI
- affect yearly ratios to scale LAIs: ratios are based on hvegmax(current year)/hvegmax(2007)
- apply min values for LAIs
"""	

"""Affect ratios:"""
for yr in np.arange(2005,2016):	
	LAI[LAI.columns[0]][LAI.index.year == yr]*=\
	hvegdf[0][(hvegdf.index.year==yr) & (hvegdf.index<datetime.datetime(yr,12,31))].max()/hvegdf[0][hvegdf.index.year==2007].max()
	

""" apply min values for LAI"""
LAImin=0.09
LAI[LAI.columns[0]]=LAI[LAI.columns[0]].apply(lambda x: x if x>=LAImin else LAImin)
for yr in np.arange(2005,2016):
	LAI[LAI.columns[0]][(LAI.index.year == yr) & (LAI.index >= Burning_dates[yr][1])] = LAImin


""" Create SAI"""	
LAI['SAI']=0.5
for yr in np.arange(2005,2016):
	#get index of max values of LAI and HVEG
	indmaxLAI = LAI[LAI.columns[0]][LAI.index.year == yr].idxmax()
	indmaxHVEG = hvegdf[hvegdf.columns[0]][hvegdf.index.year==yr].idxmax()
	
	LAI['SAI'][(LAI.index >=indmaxLAI) & (LAI.index <=indmaxHVEG)]=\
	LAI[LAI.columns[0]][LAI.index.year == yr].max()-\
	LAI[LAI.columns[0]][(LAI.index >=indmaxLAI) & (LAI.index <=indmaxHVEG)]+\
	np.linspace(0.5,0,len(LAI[LAI.columns[0]][(LAI.index >=indmaxLAI) & (LAI.index <=indmaxHVEG)]))

"""after the max of LAI, when LAI starts to slowly decrease, but slower
than the np.linspace(0.5,0,npts), that makes the SAI slightly drop below
0.5, so correct this"""
LAI['SAI']=LAI['SAI'].apply(lambda x: x if x>0.5 else 0.5)
LAI[LAI.columns[0]].name='LAI_Herb'
LAI['SAI'].name='SAI_Herb'
LAI.rename(columns={'LAI_grass_Nalo': 'LAI_Herb', 'SAI':'SAI_Herb'},inplace=True)
hvegdf = hvegdf.resample('30min').mean().interpolate()
LAI['HVEG_Herb'] = hvegdf[hvegdf.columns[0]]/100
#~ LAI['Tot']=LAI['LAI_grass_Nalo']+LAI['SAI']

""" BELE """
filename = sourcedir + 'LAI_fcover_Bele_composite.csv'
Beledf=pd.read_csv(filename, sep=',')
Beledf['date']=Beledf['date'].apply(lambda x: minimalist_xldate_as_datetime(x, 0))
Beledf=Beledf.set_index('date')
Beledf = Beledf.drop(Beledf.columns[[1,2,3,4]],axis=1)

"""Early LAI process:
add some more years by 
duplicating 2007. Also add 01/01/2005 and then interpolate from sparse days to 30mn,
otherwise the TS starts 5th of Jan:"""
#~ Beledf = Beledf[Beledf.index.year!=2005]
baseyr=2007
yrs_to_add = np.arange(2009,2016)
for yr in yrs_to_add:
	tmp=Beledf[Beledf.index.year==baseyr]
	tmp.index=tmp.index.map(lambda t: t.replace(year = yr))
	Beledf=pd.concat([Beledf,tmp])
Beledf.loc[datetime.datetime(2005,1,1)]=0.26 # set it at the start of the df
Beledf.loc[datetime.datetime(2016,1,1)]=0.35 # set it at the end of the df
LAI['LAI_Trees'] = Beledf[Beledf.columns[0]].resample('30min').mean().interpolate()
LAI['SAI_Trees'] = 2
LAI['HVEG_Trees'] = 10.6

plt.figure()
#~ Nalodf.plot()	
#~ tmp.plot()
#~ P_d.plot()
#~ hvegdf.plot()
#~ Beledf.plot()
LAI.plot()
plt.show()

LAI['LAI_shrubs'] = 0.5*LAI['LAI_Herb']+0.5*LAI['LAI_Trees']
LAI['SAI_shrubs'] = 0.5*LAI['SAI_Herb'] + 0.4
LAI['HVEG_shrubs'] = LAI['HVEG_Herb'].max()

#~ LAI.to_csv('test.csv', sep='\t')
"""initialize LAI, SAI, DISPLA and Z0M"""
z0mref = np.array([1.00, 2.2, 1.0, 0.8, 0.8, 0.1, 0.1, 0.7, 0.1, 0.03, 0.03, 0.06, 0.5, 0.06, 0.01, 0.05, 0.002, 0.01, 0.0])
displaref = np.array([11.0, 23.0, 11.0, 13.0, 13.0, 0.3, 0.3, 6.5, 0.7, 0.3, 0.3, 0.3, 3.0, 0.3, 0.0, 0.1, 0.0, 0.0, 0.0])
lai = pd.DataFrame(columns=np.arange(1,20),index=LAI.index).apply(lambda x: LAI['LAI_Herb'])
sai = pd.DataFrame(columns=np.arange(1,20),index=LAI.index).apply(lambda x: LAI['SAI_Herb'])
displa = pd.DataFrame(columns=np.arange(1,20),index=LAI.index).apply(lambda x: pd.Series(z0mref[x.name-1],index=LAI.index))
z0m = pd.DataFrame(columns=np.arange(1,20),index=LAI.index).apply(lambda x: pd.Series(displaref[x.name-1],index=LAI.index))

######## Note 2019/08: Class 8 corresponds to woody savannah & 7 to open shrublands: confusingly, open shrublands has actually a denser wood cover (it's closer to the evergreen needleleaf forest')
lai[8] = LAI['LAI_shrubs']
lai[7] = LAI['LAI_Trees']
lai[2] = LAI['LAI_Trees'].max() #ripisylve
lai[18] = 0.

sai[8] = LAI['SAI_shrubs']
sai[7] = LAI['SAI_Trees']
sai[2] = LAI['SAI_Trees'].max() #ripisylve
sai[18] = 0.

z0m[8] = LAI['HVEG_shrubs']*0.097+0.17
z0m[7] = LAI['HVEG_Trees']*0.097+0.17
z0m[2] = LAI['HVEG_Trees'].max()*0.097+0.17 #ripisylve
z0m[18] = 0.
z0m[19] = LAI['HVEG_Herb']*0.097+0.17

displa[8] = LAI['HVEG_shrubs']*0.67
displa[7] = LAI['HVEG_Trees']*0.67
displa[2] = LAI['HVEG_Trees'].max()*0.67 #ripisylve
displa[18] = 0.
displa[19] = LAI['HVEG_Herb']*0.67


lai.to_csv(outdir+'lai.dat',float_format='%2.4f', sep=' ', header=False, index=False)
sai.to_csv(outdir+'sai.dat',float_format='%2.4f', sep=' ', header=False, index=False)
z0m.to_csv(outdir+'z0m.dat',float_format='%2.4f', sep=' ', header=False, index=False)
displa.to_csv(outdir+'displa.dat',float_format='%2.4f', sep=' ', header=False, index=False)

#~ lai = lai/2
#~ lai.to_csv(outdir+'lai_small.dat',float_format='%2.4f', sep=' ', header=False, index=False)
#~ sai.to_csv(outdir+'sai.dat',float_format='%2.4f', sep=' ', header=False, index=False)
#~ z0m.to_csv(outdir+'z0m.dat',float_format='%2.4f', sep=' ', header=False, index=False)
#~ displa.to_csv(outdir+'displa.dat',float_format='%2.4f', sep=' ', header=False, index=False)


"""compare 2 lai files:"""
l1 = pd.read_csv(outdir+'lai.dat', delimiter=r"\s+", header =None)
#~ l1 = pd.read_csv(outdir+'lai_small.dat', delimiter=r"\s+", header =None)
l2 = pd.read_csv('/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/input/lai.dat', header = None, delimiter=r"\s+")
l3=l1.subtract(l2,axis='columns')
plt.figure()
#~ l3.plot()
#~ l3[l3.columns[[1,6,7,18]]].plot()
l1[l1.columns[[1,6,7,18]]].plot()
plt.show()
