##-*- coding: utf-8 -*-
"""
	PFPREPROC- Example script

	Read vegetation forcing files (lai, sai, displa, z0m) for PF

    Class 7 has actually a denser wood cover than class 8
    
	@copyright: 2018 by Basile HECTOR <basile.hector@ird.fr>
	@license: GNU GPL, see COPYING for details.
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

"""
!IGBP Land Cover Types (other classes can be used by changing this file)
!  1 evergreen needleleaf forests
!  2 evergreen broadleaf forests
!  3 deciduous needleleaf forests
!  4 deciduous broadleaf forests
!  5 mixed forests
!  6 closed shrublands
!  7 open shrublands
!  8 woody savannas
!  9 svannas
! 10 grasslands
! 11 permanent wetlands
! 12 croplands
! 13 urban and built-up lands
! 14 cropland / natural vegetation mosaics
! 15 snow and ice
! 16 barren or sparsely vegetated
! 17 water bodies
! 18 bare soil
! 19 doro - herbacé
"""
vegclasses={1:'evergreen needleleaf forests',2:'evergreen broadleaf forests',\
			3:'deciduous needleleaf forests', 4: 'deciduous broadleaf forests',\
			5: 'mixed forests6 closed shrublands', 6:'closed shrublands',\
			7: 'open shrublands', 8: 'woody savannas',\
			9: 'savannas', 10: 'grasslands',\
			11: 'permanent wetlands', 12: 'croplands',\
			13: 'urban and built-up lands', 14: 'cropland / natural vegetation mosaics',\
			15: 'snow and ice', 16: 'barren or sparsely vegetated',\
			17: 'water bodies', 18: 'bare soil',\
			19: 'herbaceous'}


#~ filename = '/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/input/sai.dat'
#~ filename = '/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/input/lai.dat'
filename = '/home/hectorb/PARFLOW/PROJECTS/Ara/resolution2020/inputs/ara100/lai.dat'
#~ filename = '/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/input/lai_new2.dat'
#~ filename = '/home/hectorb/PARFLOW/PROJECTS/Hillslope/bele_nalo_tests/forcings_11yrs/lai.dat'

df=pd.read_csv(filename,header=None, delimiter=r"\s+")
df.rename(columns=lambda x: x+1, inplace=True)
df = df.drop(df.columns[np.append(np.arange(0,6),np.arange(8,18))],axis=1)
df.rename(columns=lambda x: vegclasses[x], inplace=True)

df['time']=pd.date_range('1/1/2005 00:00:00','1/1/2016 00:00:00', freq='30min')
df=df.set_index('time')
plt.figure()
df.plot()
plt.show()
df2 = df
df2['time'] = df2.index.map(lambda x: x.strftime("%m-%d %H:%M"))
#~ df2['month'] = df2.index.month
#~ df2['day'] = df2.index.day
df2.groupby('time').mean().plot()
