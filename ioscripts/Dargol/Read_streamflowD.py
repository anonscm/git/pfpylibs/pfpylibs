#-*- coding: utf-8 -*-

"""
    PFPYLIBS - IOSCRIPTS

    Anaylse PF outputs for Oueme: Streamflow
    
    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
"""Classic imports"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os,glob
from netCDF4 import Dataset
import time
import datetime
import pyproj
import rasterio
from matplotlib.dates import MonthLocator, DateFormatter

"""local imports:"""
import PFlibs
from PFlibs import PFsimus as PF
from PFlibs import PFoutputs as PFout
from procsycz import readDataAMMA as rdA
from procsycz import objfunctions as obj
from procsycz import procGeodata_Gdal


plt.close("all")

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

simdir = r'/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2017/oueme2D_KH3high4_2016_12_22'
simuname = 'oueme2D_KH3high4'

plot_yr = 2015
method = 'W'
#~ n_coupled_layers = 5
#~ n_CLM_2Dlayers = 14

statsDay =True
#~ statsDay =False

"""
CLM_var is a dictionnary where keys are local variable names as in PFCLM
values are lists of string[filename, PFCLM variable name, long name, units]
"""
CLM_var={'surf_press':['','h_top','Surface pressure','m']}
""" add the filename in the dic"""        
for k,val in CLM_var.items():
    CLM_var[k][0]=os.sep.join([simdir,''.join([simuname,'_'+val[1]+'.nc'])])

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def minimalist_xldate_as_datetime(xldate, datemode):
    # datemode: 0 for 1900-based, 1 for 1904-based
    return (
        datetime.datetime(1899, 12, 30)
        + datetime.timedelta(days=xldate + 1462 * datemode)
        )
        
def roundDateTime(shakyDT,nearest_minutes=1):
	"""
	"""
	shakyDT += datetime.timedelta(minutes=nearest_minutes/2)
	shakyDT -= datetime.timedelta(minutes=shakyDT.minute % nearest_minutes,
                         seconds=shakyDT.second,
                         microseconds=shakyDT.microsecond)
	return shakyDT
	

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

#### Read & process simulation outputs:
sim = PFout.PFout(simdir,'oueme')
sim.read_domain_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]))
sim.read_latlon_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),xvarname='longitude',yvarname='latitude')        
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_satur.nc'])]),filetype='PF')
sim.read_time_dimensions_from_NC(filename=os.sep.join([simdir,''.join([simuname,'_h_top.nc'])]),filetype='CLM')
sim.X0=np.min(sim.x)
sim.Y0=np.min(sim.y)


"""Read PF outputs"""
start_time = time.time()
with Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r') as ds:
    sim.satur_array = ds.variables['saturation'][:]
    #~ topmask=np.swapaxes(ds.variables['top_mask'][:],0,1)
"""MASK is a nx x ny boolean mask of the domain
When applied to an nx x ny x nt it will produce an n(true) x nt 2D array
(as in sim.lh_tot[MASK]) => So useful for mean,min,max calc
"""
#~ MASK=topmask>0 
with Dataset(os.sep.join([simdir,''.join([simuname,'_press.nc'])]), 'r') as ds:
    sim.press_array = ds.variables['succion'][:] 

"""Read CLM outputs"""
for k,val in CLM_var.items():
    try:
        with Dataset(val[0], 'r') as ds:
            print('read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
            exec('sim.%s=np.swapaxes(ds.variables[\'%s\'][:],0,2)'%(k,val[1]))        
    except FileNotFoundError:
            print('CANNOT read %s \t\t\t\t\t from file %s'%(val[2],val[0]))
print("--- %d seconds --- read all PFCLM outputs from NCs" % (time.time() - start_time))

""" Read constant files"""
ds=Dataset(os.sep.join([simdir,''.join([simuname,'_satur.nc'])]), 'r')
#watch out for variable dimensions (nz,ny,nx), which is different than in pfb as read using PFout lib:
sim.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,1)
sim.mannings_arr = np.zeros((sim.nx,sim.ny))+1e-6
sim.mask_arr = np.swapaxes(ds.variables['mask'][:],0,1)
sim.slope_x_arr = np.swapaxes(ds.variables['xslope'][:],0,1)
sim.slope_y_arr = np.swapaxes(ds.variables['yslope'][:],0,1)
sim.porosity_arr = np.swapaxes(ds.variables['porosity'][:],0,1)
sim.specific_storage_arr = np.swapaxes(ds.variables['specific_storage'][:],0,1)
#~ sim.domain_arr = sim.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))

""" Read Forcings: """
#~ forcing_headers = {0:'SWin',1:'LWin',2:'P', 3: 'T', 4:'WindX', 5:'WindY',6:'Press', 7:'Hs'}
#~ forc = pd.read_csv(glob.glob(os.sep.join([simdir,'*forc*txt*']))[0],header=None,sep='\t')
#~ forc.rename(columns=forcing_headers,inplace=True)
#~ forc.index=pd.date_range('1/1/2005 00:00:00', periods=len(forc), freq='30min')
#~ sim.P=sum(forc['P'])*30*60

"""Get streamflow data"""
filename = '/home/hectorb/DATA/streamflow/Dargol/data_kakassi.dat'
df = pd.read_csv(filename, comment ='#', sep =' ',na_values=[''])
Q = df['value']
Q.index = pd.to_datetime(df['date'])
Kakassi_catchment_area = 6940*1000*1000
lonkakassi = 1.4667
latkakassi = 13.85

proj = pyproj.Proj(proj='utm', zone=31, ellps='WGS84')
geo_system = pyproj.Proj(proj='latlong')
Xstation, Ystation = pyproj.transform(geo_system,proj,lonkakassi, latkakassi)

   
""" Get stream network:"""
#~ with rasterio.open("/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/inputs/geometries/Donga/data/Donga_streams_segments_1000m_masked_mean_agreg_D4_pitfilled.tif") as src:
#~ with rasterio.open("/homelocal5/phyrev/_PARFLOW/projects/Donga/active/Donga_1km/input/DEM/Rwatershed/streams.tif") as src:
data,londem,latdem = procGeodata_Gdal.readRasterWithGdal("/home/hectorb/PARFLOW/SCRIPTS/scripts_matlab/inputs/geometries/Oueme/data/Oueme_streams_segments_1000m_masked_mean_agreg_D4_pitfilled.tif",nodata_value = 65535)
[lonlon,latlat]=np.meshgrid(londem,latdem)

""" Find the location of data in the PF framework, and calculate simulated Q:"""
xx_masked = np.round(lonlon)
yy_masked = np.round(latlat)
#~ xx_masked[data == 0] = -99999999
#~ yy_masked[data == 0] = -99999999
xx_masked[data[::-1,:] == 0] = -99999999
yy_masked[data[::-1,:] == 0] = -99999999
xx_masked[data == 255] = -99999999
yy_masked[data == 255] = -99999999
#xx_masked, yy_masked and data have shapes [ny,nx] and data[0,:] is the lowest latitude (bottom of map)

Qsim = pd.DataFrame()
#~ x = [sim.X0 + sim.dx*x+sim.dx/2 for x in range(sim.nx)]
#~ y = [sim.Y0 + sim.dy*y+sim.dy/2 for y in range(sim.ny)]
x = [sim.X0 + sim.dx*x for x in range(sim.nx)]
y = [sim.Y0 + sim.dy*y for y in range(sim.ny)]

#find the closest point in the grid:  
dist_to_river = np.sqrt((xx_masked - Xstation)**2 + (yy_masked - Ystation)**2)
dist = np.sqrt((np.round(lonlon)- Xstation)**2 + (np.round(latlat) - Ystation)**2)
#~ if (dist_to_river.min() <= 2*np.sqrt(sim.dx*sim.dx + sim.dy*sim.dy)):
if (dist_to_river.min() < 4*np.sqrt(sim.dx*sim.dx + sim.dy*sim.dy)):
    ind_min = np.where(dist_to_river == dist_to_river.min())
    print('river cell found')
else:
    ind_min = np.where(dist == dist.min())   
    print('no river cell found')
Xstation_sim = np.round(lonlon[ind_min])
Ystation_sim = np.round(latlat[ind_min])
print(Xstation)
print(Ystation)
print(Xstation_sim)
print(Ystation_sim)    
# now get the outlet location in the PF domain:
#~ sim.calculate_runoff_from_CLM(x_outlet = np.where(np.array(x) == Xstation_sim),y_outlet = np.where(np.array(y) == Ystation_sim),nlayers = n_coupled_layers+n_CLM_2Dlayers)
sim.calculate_runoff_from_press(x_outlet = np.where(np.array(x) == Xstation_sim)[0],y_outlet = np.where(np.array(y) == Ystation_sim)[0])    
print(np.where(np.array(x) == Xstation_sim))
print(np.where(np.array(y) == Ystation_sim))
print(np.shape(sim.surfQ))
# if m3 => m3/s:
#~ sim.surfQ = [Q/(60*60) for Q in sim.surfQ ]
"""m3/hr (TO CHECK -> seems ok)"""
#~ sta.Qsim  = sim.create_panda_series(np.reshape(sim.surfQ,sim.n_t_clm),start_date = "1/1/2007", start_hour = "00:00", freq = "1H",periods = sim.n_t_clm)        
Qsim  = sim.create_panda_series(np.reshape(sim.surfQ,sim.n_t_clm),start_date = "1/1/2015", start_hour = "00:00", freq = "1H",periods = sim.n_t_clm)        
Qsim[abs(Qsim)>10e10]=np.nan
"""m3/hr -> m3/s"""
sta.Qsim = sta.Qsim/3600.0
Qsim[stationname]=sta.Qsim        
stadic[stationname] = sta


def pdinterpolate(ts, datetime_index):
    """could decide to fill nans !
    but not adapted for calculating scores?
    To be tested"""
    x = pd.concat([ts, pd.Series(index=datetime_index)])
    #~ return x.groupby(x.index).first().sort_index().fillna(method="ffill")[datetime_index]
    return x.groupby(x.index).first().sort_index().interpolate('linear')[datetime_index]
    #~ return x.groupby(x.index).first().sort_index()[datetime_index]


"""
plot station map
#~ https://stackoverflow.com/questions/40899350/python-quiver-and-pcolormesh-not-lining-up-exactly-right     

"""
fig = plt.figure(figsize=(16,8))
ax = fig.add_subplot(111)
ax.set_aspect(1)
[xx,yy]=np.meshgrid(x,y)

""" to check the location with respect to pressure field:"""
#~ p = ax.pcolormesh(xx-sim.dx/2, yy-sim.dy/2, sim.press_array[240,-1,:,:], cmap="viridis", zorder=-1)
""" to check the location, use the stream mask:"""
p = ax.pcolormesh(lonlon-sim.dx/2, latlat-sim.dy/2, data[::-1,:], cmap="viridis", zorder=-1)
cb = plt.colorbar(p)
cb.set_label('P(m)',fontsize=20)
ax.set_xlabel('X utm (m)',fontsize=20)
ax.set_ylabel('Y utm (m)',fontsize=20)

ax.plot(Xstation,Ystation,'k+',markersize=12)
ax.plot(Xstation_sim ,Ystation_sim ,'r+',markersize=12)




"""
plot time series
"""
#~ fig,ax =plt.subplots(nrows=len(stadic),figsize=(20,7), squeeze=True)
#~ i=0
#~ for stationname,sta in stadic.items():
    #~ ax[i].plot(sta.Q.loc['2007-1-1':'2007-12-31'].dropna(),'k')
    #~ ax[i].plot(sta.Qsim.loc['2007-1-1':'2007-12-31'],'r')
    #~ ax[i].set_ylabel('$m3/hr')
    #~ ax[i].legend(['%s: obs'%stationname,'%s: sim'%stationname])
    #~ i+=1

fig = plt.figure(figsize=(16,8))
ax = fig.add_subplot(111)

# interpolation on sim + daily approach:
obs = Q.dropna().sort_index(axis=0)
simu = Qsim.dropna().sort_index(axis=0)
obs = obs.groupby(obs.index).mean()
obs_int=pd.concat([obs,simu],axis=1).interpolate(method='slinear',limit=50)[0]
obs_intD = obs_int.resample('D').mean()
simuD = simu.resample('D').mean()
merge=pd.concat([obs_intD.rename('obs'),simuD.rename('sim')],axis=1)
kept = merge.dropna()
res = obj.kge(kept['obs'].values, kept['sim'].values, return_all=True)
print(res)  

ax.plot(Q.dropna().sort_index(axis=0),'k')    
ax.plot(Qsim.dropna().sort_index(axis=0).resample(method).mean(),'r')
ax.plot(obs_intD,'b')
ax.set_ylabel('$m3/s')
ax.legend(['obs','sim'],fontsize=8,loc='upper right',ncol=2)
ax.set_xlim([datetime.datetime(plot_yr,4,1),datetime.datetime(plot_yr,11,1)])
ax.set_ylim([0,520])
ax.text(datetime.datetime(plot_yr,4,4),480,r'$r^2 = %2.2f$ KGE = %2.2f'%(res[1]*res[1],res[0]),FontSize=8,bbox = {'facecolor':'white', 'alpha':0.7})

ax.tick_params(axis='x', which='both', labelbottom='on', labeltop='on')
ax.tick_params(axis='y', which='both', labelright='on', labelleft='on')
ax.tick_params(axis='both', which='major', bottom='on',top='on',right='on',left='on')

#~ plt.savefig(os.sep.join([simdir,'Figure_compare_Q_%s_'%method+np.str(int(plot_yr))+'.png']),dpi=400,format='png')


