"""
Read and write tcl files for parflow
"""
__author__     = "PHYREV team"
__copyright__  = "Copyright 2017"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import os
import argparse
import doctest
import pandas
import difflib
#~ import numpy

from PFlibs.config import testfiles

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

_tclset = "pfset"
_nset = len(_tclset)

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def _supr_blank(text):
    """
    Suppr double blank and tabulation in text
    >>> _supr_blank("tr  tr ")
    'tr tr'
    >>> _supr_blank(" a\t b ")
    'a b'
    """
    text = text.replace("\t", " ").strip()
    if "  " in text:
        res = text.replace("  ", " ")
        res = _supr_blank(res)
    else:
        res = text
    
    return res

def tcl_testfile():
    """
    Get the default tcl file
    >>> os.path.isfile(tcl_testfile())
    True
    """
    res = testfiles('tcl')
    return res

##======================================================================================================================##
##                CLASSES                                                                                               ##
##======================================================================================================================##


class TclSet(object):
    """ Class doc
    >>> tclset = TclSet('ks', 1e-6)
    >>> print(tclset)
    pfset ks 1e-06
    >>> tclline = tclset.to_tclline()
    >>> isinstance(tclline, TclLine)
    True
    >>> tclline
    'pfset ks 1e-06'
    >>> tclset2 = tclset.change_value(1e-3)
    >>> isinstance(tclset2, TclSet)
    True
    >>> print(tclset2)
    pfset ks 0.001
    """
    def __init__(self, key, value):
        """ Class initialiser """
        assert isinstance(key, str), "the key is expected to be characters got {0.__class__} : {0}".format(key)
        self.key = key
        self.value = value
        
    def __str__(self):
        """Object representation"""
        res = " ".join([_tclset, self.key, str(self.value)])
        return res
        
    def to_tclline(self):
        """Return a TclLine object"""
        txt = str(self)
        res = TclLine(txt)
        return res
        
    def change_value(self, newval):
        """Return a new object with a different value"""
        res = TclSet(key = self.key, value = newval)
        return res
        

class TclLine(str):
    """ Class doc
    >>> TclLine('pfset Geom.domain.Lower.Z                        0')._treat_setline()
    'Geom.domain.Lower.Z 0'
    >>> tcl1 = TclLine('#pfset Solver.CLM.WriteLogs True')
    >>> tcl2 = TclLine('namespace import Parflow::*')
    >>> tcl3 = TclLine('pfset Solver.CLM.WriteLogs True')
    >>> tcls = [tcl1, tcl2, tcl3]
    >>> for i in tcls:print(isinstance(i, TclLine))
    True
    True
    True
    
    >>> for i in tcls:i.is_setline()
    False
    False
    True
    
    >>> tcl3._treat_setline()
    'Solver.CLM.WriteLogs True'
    >>> tclset = tcl3.to_tclset()
    >>> print(tclset)
    pfset Solver.CLM.WriteLogs True
    >>> tclset.key
    'Solver.CLM.WriteLogs'
    >>> tclset.value
    'True'
    >>> tclset.to_tclline()
    'pfset Solver.CLM.WriteLogs True'
    """
    def __new__(cls, content):
        """Create a new object from str"""
        obj = super(TclLine, cls).__new__(cls,content)
        return obj

    def is_setline(self):
        """True if line begin by pfset"""
        text = str(self)
        res = _tclset == text[:_nset]
        return res

    def _treat_setline(self):
        """ Replace pfset to have key value in text and remove blank and tabs"""
        text = str(self)
        res = text.replace(_tclset, "").strip()
        res = _supr_blank(res)
        return res

    def to_tclset(self):
        """ Return a TclSet object from a TclLine : key and value"""
        text = self._treat_setline()
        texts = text.split(" ")
        ntxt = len(texts)
        if ntxt < 2:
            raise ValueError("exp 2 value got {0}".format(texts))
        elif ntxt == 2:
            texts = tuple(texts)
        else:
            text0 = texts[0]
            text1 = " ".join(texts[1:])
            texts = (text0, text1)
            assert any([ick in text1 for ick in (["'", '"', "[", "]"])])    
        
        assert len(texts) == 2, texts
        key = texts[0]
        val = texts[1]
        res = TclSet(key = key, value = val)
        
        return res


class TclLines(pandas.Series):
    """ Class doc
    >>> tclname = tcl_testfile()
    >>> tcllines = TclLines(filename = tclname)
    >>> tcllines.is_setline().sum()
    236
    >>> setlines = tcllines.get_setlines()
    >>> setlines.index.size
    236
    >>> setlines.head()
    11                    FileVersion 4
    16             Process.Topology.P 1
    17             Process.Topology.Q 1
    18             Process.Topology.R 1
    54    ComputationalGrid.Lower.X 5.0
    dtype: object
    
    >>> tclsets = tcllines.to_tclsets()
    >>> tclsets.loc[11].key
    'FileVersion'
    >>> tclsets.loc[11].value
    '4'
    
    >>> tcllines.setkeys().head()
    11                  FileVersion
    16           Process.Topology.P
    17           Process.Topology.Q
    18           Process.Topology.R
    54    ComputationalGrid.Lower.X
    dtype: object
    
    >>> tcllines.get_idx('ComputationalGrid.Lower.X')
    54
    
    >>> tcllines2 = tcllines.change_param(key = 'ComputationalGrid.Lower.X', newval = 6)
    >>> tcllines2.get_setlines().head()
    11                  FileVersion 4
    16           Process.Topology.P 1
    17           Process.Topology.Q 1
    18           Process.Topology.R 1
    54    ComputationalGrid.Lower.X 6
    dtype: object

    >>> s1 = tcllines.to_series()
    Warnig, some keys have multi values {'ComputationalGrid.NZ'}
    >>> s2 = tcllines2.to_series()
    Warnig, some keys have multi values {'ComputationalGrid.NZ'}
    >>> dif_indx = s1 != s2
    >>> s1[dif_indx]
    ComputationalGrid.Lower.X    5.0
    dtype: object
    >>> s2[dif_indx]
    ComputationalGrid.Lower.X    6
    dtype: object
    
    >>> newtcl = tclname.replace('.tcl', '_new.tcl')
    >>> tcllines2.write(newtcl)
    >>> tcllines3 = TclLines(filename = newtcl)
    >>> (tcllines2 == tcllines3).all()
    True
    >>> len(tcl_testfile())
    2
    >>> os.remove(newtcl)
    >>> os.path.isfile(tcl_testfile())
    True
    """
    
    def __init__(self, *args, **kwargs):
        """ Class initialiser 
        If filename is psecified it reads it
        """
        
        if "filename" in kwargs:
            filename = kwargs.pop("filename")
            assert os.path.isfile(filename), "Filename is not a valid path\n{0}".format(filename)
            with open(filename, "r") as f:
                reads = f.readlines()

            reads = [_supr_blank(i) for i in reads]
            reads = pandas.Series.__init__(self, reads)
        
        else:
            pandas.Series.__init__(self, *args, **kwargs)


    @property
    def _constructor(self):
        """For returning TclLines instead of pandas.Series"""
        return TclLines

    def is_setline(self):
        """ True if tcl setline else False """
        res = self.apply(lambda x : TclLine(x).is_setline())
        return res
        
    def get_setlines(self):
        """ Return a series containing the setlines only """
        indx = self.is_setline()
        res = self[indx]
        res = res.apply(lambda x : TclLine(x)._treat_setline())
        return res
        
        
    def to_tclsets(self):
        """ Return a series of tclset object"""
        res = self.get_setlines().apply(lambda x : TclLine(x).to_tclset())
        return res
        
    def setkeys(self):
        """ Return the key of each setline"""
        res = self.to_tclsets().apply(lambda x : x.key)
        return res
        
    def get_idx(self, key,create_if_doesnt_exist=True):
        """ Return the index of the given key"""
        keys = self.setkeys()
        #~ print('change key %s'%key)
        idxs = keys == key
        res = keys[idxs].index
        assert (len(res) == 1) | (len(res) ==0), "Dont know what to do.\n Same key are present.Lines {0} key = %s".format(res)%key
        # Modifs BH. Former GP is simply res = int(res[0]), return res
        if len(res) == 0:
            if create_if_doesnt_exist:
                #the key does not exist, create it (close to the closest key?):
                close_key = difflib.get_close_matches(key,self.setkeys().values)[0]
                if close_key:
                    #~ print('closest key of %s: %s'%(key,close_key))
                    self, close_idx = self.copy().get_idx(close_key)
                    tmp = self.tolist()
                    tmp.insert(close_idx+1,'pfset %s whatever'%key)
                    self = TclLines(pandas.Series(tmp))
                    res = [close_idx+1]
                    #~ print('create key: %s at line %d'%(key,close_idx+1))
                else: print('no close key found... find a way to assign a certain position')
            else: print('problem: the key does not exist, should we create it?')
        res = int(res[0])
        return self,res
        
    def _change_param(self, key, newval):
        """Change the value of the given key to the given value """
        res = self.copy()
        print('change param %s'%key)
        #~ idx = self.get_idx(key)
        res,idx = res.get_idx(key) #modif BH
        #~ print(idx)
        tochange = res.loc[idx]
        #~ print(tochange)
        newline = str(TclLine(tochange).to_tclset().change_value(newval = newval))
        #~ print(newline)
        res.loc[idx] = newline
        return res
        
    def change_param(self, **kwargs):
        """
        Function doc
        >>> 'afarie'
        """
        res = self.copy()
        for k, v in kwargs.items():
            res = res._change_param(key = k, newval = v)
            
        return res

    def to_series(self):
        """ Return a pandasSeries with keys as index and parameter value as values"""
        lines = self.to_tclsets().values
        dic = {i.key : i.value for i in lines}
        if len(dic) < len(lines):
            keys = [i.key for i in lines]
            print("Warnig, some keys have multi values", set([i for i in keys if keys.count(i) > 1]))
        
        res = pandas.Series(dic)
        return res        
        
        
    def write(self, filename):
        """ Write a tclfile"""
        texts = "\n".join(self.values)
        with open(filename, "w") as fw:
            fw.writelines(texts)
        
        return None
        
    def get_keyval_matching_pattern(self,pat):
        """return a dictionnary with keys as unique strings comprised 
        within a pattern and associated values. Typically to get
        pfset GeomInput.H2.Value			2
        ['H3'] = 3"""
        out = {}
        keys = self.setkeys()
        keys_match_ID = keys[keys.str.contains(pat)].index
        for i in keys_match_ID:
            print(self.to_tclsets()[i].key)
            print(self.to_tclsets()[i].value)
            out[self.to_tclsets()[i].key]=self.to_tclsets()[i].value
        return out
            
     
        
        
#~ def p(*args, **kwargs):
    #~ print(args)
    #~ print(kwargs)
        
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    ARGPARSE                   #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default = False, action = 'store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
        
    else:
        print("Examples")

        tclfile = tcl_testfile()
        newtcl = tclfile.replace('.tcl', '_new.tcl')
        tcllines = TclLines(filename = tclfile)

        set_lines = tcllines.is_setline()
        set_lines = tcllines.get_setlines()
        split_lines = tcllines.to_tclsets()

        keytochange = "OverlandFlowSpinUp"

        new = tcllines.change_param(keytochange,4)
        new.write(filename = newtcl)


        ((new.to_series() == tcllines.to_series()) == False).sum()

        print("warning if two key are present")


        k= "Geom.domain.Perm.TensorValZ"

        os.remove(newtcl)
