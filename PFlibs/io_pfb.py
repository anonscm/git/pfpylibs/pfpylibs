#-*- coding: utf-8 -*-
"""
	PYFLOW - PFlibs - IO utils for PFBs

	IO utils for PFBs
	- Read and write pfb binary files for parflow


	@modified from jgilbert by Savannah & Basile
	@copyright: 2018 by Basile HECTOR <basile.hector@ird.fr>
	@license: GNU GPL, see COPYING for details.
"""
__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
import struct
import numpy
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import os
from mpl_toolkits.axes_grid1 import make_axes_locatable
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##
def PFB_read(pfb_file_path, flag):

    #print("**This code DOES handle subgrids**")
    
    hdr_dict = {}
    
    with open(pfb_file_path,'rb') as pfbf:
        hdr_dict['X0'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['Y0'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['Z0'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['nx'] = struct.unpack('>i', pfbf.read(4))[0]
        hdr_dict['ny'] = struct.unpack('>i', pfbf.read(4))[0]
        hdr_dict['nz'] = struct.unpack('>i', pfbf.read(4))[0]
        hdr_dict['dx'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['dy'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['dz'] = struct.unpack('>d', pfbf.read(8))[0]
        hdr_dict['ns'] = struct.unpack('>i', pfbf.read(4))[0]
        ns = hdr_dict['ns']
        #print(ns)
        nx = hdr_dict['nx']
        ny = hdr_dict['ny']
        nz = hdr_dict['nz']
        
        if nx == 0:
            nx = int(raw_input("What should the NX be?"))
            hdr_dict['nx'] = nx
        if ny == 0:
            ny = int(raw_input("What should the NY be?"))
            hdr_dict['ny'] = ny
        if nz ==0:
            nz =int(raw_input("What should the NZ be?"))
            hdr_dict['nz'] = nz
        
        X0 = hdr_dict['X0']
        #print "len_x, len_y, len_z, nx ny nz --> %f %f %f %i %i %i" %(len_x, len_x, len_x, nx, ny, nz)
        
        pfb_array = numpy.zeros((nx, ny, nz))
        
        for subgrids in range(ns):
            hdr_dict['ix'] = struct.unpack('>i', pfbf.read(4))[0]
            ix = hdr_dict['ix']
            hdr_dict['iy'] = struct.unpack('>i', pfbf.read(4))[0]
            iy = hdr_dict['iy']
            #print(iy)
            hdr_dict['iz'] = struct.unpack('>i', pfbf.read(4))[0]
            iz = hdr_dict['iz']
            hdr_dict['nx2'] = struct.unpack('>i', pfbf.read(4))[0]
            nx2 = hdr_dict['nx2']
            hdr_dict['ny2'] = struct.unpack('>i', pfbf.read(4))[0]
            ny2 = hdr_dict['ny2']
            #print(ny2)
            hdr_dict['nz2'] = struct.unpack('>i', pfbf.read(4))[0]
            nz2 = hdr_dict['nz2']
            hdr_dict['rx'] = struct.unpack('>i', pfbf.read(4))[0]
            rx = hdr_dict['rx']
            hdr_dict['ry'] = struct.unpack('>i', pfbf.read(4))[0]
            ry = hdr_dict['ry']
            hdr_dict['rz'] = struct.unpack('>i', pfbf.read(4))[0]
            rz = hdr_dict['rz']
            #print "ix iy iz nx2 ny2 nz2 rx ry rz --> %i %i %i %i %i %i %i %i %i\n" %(ix,iy, iz, nx2, ny2, nz2, rx,ry, rz)
            for lays in range(nz2):
                for rows in range(ny2):
                    for cols in range(nx2):
                        val = struct.unpack('>d', pfbf.read(8))[0]
                        #q = raw_input(val)
                        pfb_array[(cols+ix), (rows + iy), (nz2-(lays + iz)-1)] = val
                        #pfb_array[(cols+ix-1), (nx2-(iy)-1), (nz2-(lays + iz)-1)] = val
                        #pfb_array[(rows + iy-1), (cols+ix-1), (nz2-(lays + iz)-1)] = val
                        #print val
                        
    if flag ==1:
        return(pfb_array, hdr_dict)
    else:
        return(pfb_array)


def PFB_write(pfarray, pfb_hdr_dict, pfb_fp):
    print("**This code does NOT handle subgrids**")
    with open(pfb_fp,'wb') as pfbf:
        pfbf.write(struct.pack('>d',pfb_hdr_dict['X0']))
        pfbf.write(struct.pack('>d',pfb_hdr_dict['Y0']))
        pfbf.write(struct.pack('>d',pfb_hdr_dict['Z0']))
        pfbf.write(struct.pack('>i',pfb_hdr_dict['nx']))
        pfbf.write(struct.pack('>i',pfb_hdr_dict['ny']))
        pfbf.write(struct.pack('>i',pfb_hdr_dict['nz']))
        pfbf.write(struct.pack('>d', pfb_hdr_dict['dx']))
        pfbf.write(struct.pack('>d', pfb_hdr_dict['dy']))
        pfbf.write(struct.pack('>d', pfb_hdr_dict['dz']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['ns']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['ix']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['iy']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['iz']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['nx']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['ny']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['nz']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['rx']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['ry']))
        pfbf.write(struct.pack('>i', pfb_hdr_dict['rz']))
            
        print ("wrote header")
        nx = pfb_hdr_dict['nx']
        ny = pfb_hdr_dict['ny']
        nz = pfb_hdr_dict['nz']
                
        for lays in range(nz):
            # repeat writing of layer 1 for Noah-WRF layers
            # 1, 2, & 3 (0.10 m, 0.30 m, + 0.60 m dz)
            #print("Writing layer %s") %(lays+1)
            for rows in range(ny):
                   for cols in range(nx):
                       if nz == 1:
                           val = pfarray[cols, rows]
                       else:
                           val = pfarray[cols, rows, lays]
                       pfbf.write(struct.pack('>d', val))
    print("Wrote pfb output")
    return(1)

def write_tif_from_pfb(pfb_filename,tif_filename,crs):
    """read a 2D pfb and write a geotiff """
    from rasterio.transform import Affine
    import rasterio as rio
    d,h = PFB_read(pfb_filename,1) 
    d = d[:,:,0]
    #~ transform = Affine.translation(h['X0'] - h['dx'] / 2, h['Y0'] - h['dy'] / 2) * Affine.scale(h['dx'], h['dy'])
    #or:? 
    #~ transform = Affine.translation(h['X0'], h['Y0']) * Affine.scale(h['dx'], -h['dy'])
    #or:
    transform = Affine.translation(h['X0'],  h['Y0']+(h['ny']-1)*h['dy']) * Affine.scale(h['dx'], -h['dy'])
    d2 = d.swapaxes(0,1)
    d2 = d2[::-1,:]
    with rio.open(tif_filename,'w',driver='GTiff',height=d2.shape[0],width=d2.shape[1],count=1,dtype=d2.dtype,
        crs=crs,transform=transform) as dst:
            dst.write(d2,1)

def get_domain_dims_from_2DPFB(pfb_filename):
	"""
	read a pfb and create XX ans YY based on the header (don't read the 
	file content)
	"""
	hdr_dict = {}
	with open(pfb_filename,'rb') as pfbf:
		hdr_dict['X0'] = struct.unpack('>d', pfbf.read(8))[0]
		hdr_dict['Y0'] = struct.unpack('>d', pfbf.read(8))[0]
		hdr_dict['Z0'] = struct.unpack('>d', pfbf.read(8))[0]
		hdr_dict['nx'] = struct.unpack('>i', pfbf.read(4))[0]
		hdr_dict['ny'] = struct.unpack('>i', pfbf.read(4))[0]
		hdr_dict['nz'] = struct.unpack('>i', pfbf.read(4))[0]
		hdr_dict['dx'] = struct.unpack('>d', pfbf.read(8))[0]
		hdr_dict['dy'] = struct.unpack('>d', pfbf.read(8))[0]
		hdr_dict['dz'] = struct.unpack('>d', pfbf.read(8))[0]
		hdr_dict['ns'] = struct.unpack('>i', pfbf.read(4))[0]
		ns = hdr_dict['ns']
		#print(ns)
		nx = hdr_dict['nx']
		ny = hdr_dict['ny']
		nz = hdr_dict['nz']
		
		if nx == 0:
			nx = int(raw_input("What should the NX be?"))
			hdr_dict['nx'] = nx
		if ny == 0:
			ny = int(raw_input("What should the NY be?"))
			hdr_dict['ny'] = ny
		if nz ==0:
			nz =int(raw_input("What should the NZ be?"))
			hdr_dict['nz'] = nz
		
		X0 = hdr_dict['X0']
		y = [hdr_dict['dy'] *y+hdr_dict['dy'] /2 for y in range(hdr_dict['ny'] )]
		x = [hdr_dict['dx'] *x+hdr_dict['dx'] /2 for x in range(hdr_dict['nx'] )]
		
	return x,y        
		
def plot_2D_PFB(pfb_file_path,plot_fig=True,zlayer=0,crop2D=None,\
logscale=False,colorlabel='',title='',savefig=False,savedir='',\
savename='',vmin=None,vmax=None,width=8,height=4,ticklabels=False,\
cmap='viridis',scale=1.,offset=0.,dpi=400,nanvalue=None,\
threshold_low=None,threshold_high=None):
    """Read and plot a pfb
    Options are
    plot_fig [True]         True to actually plot the figure, otherwise similar to read_PFB with extra meshgrid axes
    zlayer [0]              zlayer: for 3D pfb, tells which z layer to take
    crop2D [None]           if crop2D is not None, give an array of indices to crop the 2D array eg[xmin,xmax,ymin,ymax]
    logscale [False]        logscale for the plot
    colorlabel ['']         label for the colorbar
    title ['']              figure title
    savefig [False]         save figure?
    savedir ['']            output directory
    savename ['']           figure name
    vmin [None]             minimum value to scale the colorbar
    vmax [None]             max value to scale the colorbar
    width [12]              width in inches (float) of the figure
    height [5]              height in inches (float) of the figure
    ticklabels [False]      whether to plot or not x and y tick labels
    cmap ['viridis']        chose the colormap
    outputs: d,xx,yy,fig,ax,cb
    scale                   scaling factor if needed
    offset                  offset amount if needed
    dpi                     dots per inches for saving
    nanvalue                if nanvalue to be specified
    """
    d, h = PFB_read(pfb_file_path, flag=1)
    x=numpy.arange(h['X0'],h['X0']+h['nx']*h['dx'],h['dx'])   
    y=numpy.arange(h['Y0'],h['Y0']+h['ny']*h['dy'],h['dy'])
    [xx,yy] = numpy.meshgrid(x,y)
    d=scale*(numpy.swapaxes(d[:,:,zlayer],1,0)+offset)
    if nanvalue:
        d[d==nanvalue] = numpy.nan
    if threshold_low:
        d[d<threshold_low] = numpy.nan
    if threshold_high:
        d[d>threshold_high] = numpy.nan
    if crop2D is not None:
        d = d[crop2D[2]:crop2D[3],crop2D[0]:crop2D[1]]
        xx = xx[crop2D[2]:crop2D[3],crop2D[0]:crop2D[1]]
        yy = yy[crop2D[2]:crop2D[3],crop2D[0]:crop2D[1]]
    if plot_fig:
        fig = plt.figure(figsize=(width,height))
        ax = fig.add_subplot(111)
        ax.set_aspect(1)
        #colormap 
        #~ cmap = plt.cm.jet
        #~ cmaplist = [cmap(i) for i in range(cmap.N)] 				# extract all colors from the .jet map
        #~ cmaplist[0] = (.5,.5,.5,1.0) 								# force the first color entry to be grey
        #~ cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N) 	# create the new map
        #~ bounds = np.arange(0,18,1) 									# define the bins and normalize
        #~ norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

        #~ p = ax.pcolormesh(xx, yy, d, cmap=cmap, norm=norm, zorder=-1)
        if logscale:
            p = ax.pcolormesh(xx, yy, d,zorder=-1,norm=colors.LogNorm(vmin=vmin,vmax=vmax),cmap=cmap)
        else:
            p = ax.pcolormesh(xx, yy, d,zorder=-1,vmin=vmin,vmax=vmax,cmap=cmap)
        ax.set_title(title,loc='left',fontdict={'fontsize':14, 'fontweight':'bold'})
        if not ticklabels:
            ax.set_xticklabels('')
            ax.set_yticklabels('')
        #~ p2 = ax.scatter(x,y,s=1,c=veg,cmap=cmap, norm=norm)
        #~ cb = plt.colorbar(p,orientation='vertical')
        # https://stackoverflow.com/questions/18195758/set-matplotlib-colorbar-size-to-match-graph
        #~ cb = plt.colorbar(p,orientation='vertical',fraction=0.046, pad=0.04)
        #or
        # create an axes on the right side of ax. The width of cax will be 5%
        # of ax and the padding between cax and ax will be fixed at 0.05 inch.
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        cb = plt.colorbar(p,orientation='vertical',cax=cax)

        
        cb.set_label(colorlabel,fontsize=14)
        #~ cb.set_ticks(np.arange(1,20,1))
        #~ cb.set_ticklabels([vegclasses[tk] for tk in cb.get_ticks()])
        plt.tight_layout()
        #~ plt.show()
        if savefig:
            plt.savefig(os.sep.join([savedir,savename]),dpi=dpi,format='png')

        return d,xx,yy,fig,ax,cb
    else:
        return d,xx,yy

def plot_hist_PFB(pfb_file_path,zlayer=0,bins=None,logscale=False,title='',xlabel='',savefig=False,savedir='',savename='',width=4,height=4):
    """Read and plot histogram of a pfb
    Options are
    zlayer [0]              zlayer: for 3D pfb, tells which z layer to take
    bins [None]             same as bins in the matplotlib hist function
    logscale [False]        logscale for the plot
    title ['']              figure title
    savefig [False]         save figure?
    savedir ['']            output directory
    savename ['']           figure name
    width [12]              width in inches (float) of the figure
    height [5]              height in inches (float) of the figure

    outputs: d,fig,ax
    
    """
    d, h = PFB_read(pfb_file_path, flag=1)
    d=numpy.swapaxes(d[:,:,zlayer],1,0).flatten()
    fig = plt.figure(figsize=(width,height))
    ax = fig.add_subplot(111)
    ax.hist(d,bins=bins,density=True)   
    if logscale:
        ax.set_xscale("log")
    ax.set_xlabel(xlabel,fontsize=14)        
    ax.set_title(title,loc='left',fontdict={'fontsize':14, 'fontweight':'bold'})
    plt.tight_layout()
    if savefig:
        plt.savefig(os.sep.join([savedir,savename]),dpi=400,format='png')
    return d,fig,ax

def plot_boxplots_PFB(pfb_file_path,multiplier=1,logscale=False,crop2D=None,title='',ylabel='',xlabel='',savefig=False,savedir='',savename='',width=6.7,height=3.75,**kwargs):
    """Read and plot histogram of a pfb
    Options are
    zlayer [0]              zlayer: for 3D pfb, tells which z layer to take
    multiplier [1]          used to transform data if needed (eg Ks m/h -> m/s)
    logscale [False]        logscale for the plot
    title ['']              figure title
    xlabel ['']             xlabel
    ylabel ['']             ylabel
    savefig [False]         save figure?
    savedir ['']            output directory
    savename ['']           figure name
    width [12]              width in inches (float) of the figure
    height [5]              height in inches (float) of the figure
    **kwargs                can be used to supply other arguments to plt.boxplot()
                            for instance
                            whis=(1,99),showmeans=True,flierprops=dict(markersize=0.05,linestyle=None)

    outputs: d,fig,ax
    
    """
    d, h = PFB_read(pfb_file_path, flag=1)
    d=numpy.swapaxes(d,1,0) # needed to crop , if wanted
    
    NZ=h['nz']
    data=pd.DataFrame({z:[] for z in range(NZ)})
    for layer in range(NZ):
        if crop2D is not None:
            data[layer]=d[crop2D[2]:crop2D[3],crop2D[0]:crop2D[1],layer].flatten()*multiplier
        else:
            data[layer]=d[:,:,layer].flatten()*multiplier
    fig = plt.figure(figsize=(width,height))
    ax = fig.add_subplot(111)
    data.boxplot(ax=ax,**kwargs)   
    if logscale:
        ax.set_yscale("log")
    ax.set_ylabel(ylabel,fontsize=14)        
    ax.set_xlabel(xlabel,fontsize=14)        
    ax.set_title(title,loc='left',fontdict={'fontsize':14, 'fontweight':'bold'})
    plt.tight_layout()
    if savefig:
        plt.savefig(os.sep.join([savedir,savename]),dpi=400,format='png')
    return d,fig,ax
###############################################################################
## MAIN SECTION : Select Files (pfb) and Select what you want done with them ##
###############################################################################
#~ 
#~ import numpy as np
#~ import os.path
#~ import time
#~ 
#~ 
#~ filename='./test/saturation_example.pfb'
#~ filename_out='./test/saturation_example_recreated.pfb'
#~ 
#~ [test_array, hdr] =  PFB_read(filename,1)      
#~ nz = hdr['nz']
#~ nx = hdr['nx']
#~ ny = hdr['ny']
#~ dx = hdr['dx']
#~ dy = hdr['dy']
#~ dz = hdr['dz']
#~ print('NX = %d',nx)
#~ print('NY = %d',ny)
#~ print('NZ = %d',nz)
#~ print('DX = %d',dx)
#~ print('DY = %d',dy)
#~ print('DZ = %d',dz)
#~ print('PFB[0,0,:]:')
#~ print(test_array[0,0,:])
#~ 
#~ 
#~ PFB_write(test_array, hdr, filename_out)
#~ 
#~ [test_array, hdr] =  PFB_read(filename_out,1)      
#~ nz = hdr['nz']
#~ nx = hdr['nx']
#~ ny = hdr['ny']
#~ dx = hdr['dx']
#~ dy = hdr['dy']
#~ dz = hdr['dz']
#~ print('NX = %d',nx)
#~ print('NY = %d',ny)
#~ print('NZ = %d',nz)
#~ print('DX = %d',dx)
#~ print('DY = %d',dy)
#~ print('DZ = %d',dz)
#~ print('PFB[0,0,:]:')
#~ print(test_array[0,0,:])
