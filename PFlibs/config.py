"""
Python config : path, constant and testfiles
"""
__author__ = "PHYREV team"
__copyright__ = "Copyright 2017"
__license__ = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import os
import doctest
import argparse
import glob

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

DIRNAME = os.path.dirname(__file__)
TESTDIR = os.path.join(DIRNAME, "test")

assert os.path.isdir(TESTDIR), "Package error, no test directory"

def testfiles(ext = None):
    """
    Get the test file present in the test directory
    >>> len(testfiles())
    2
    >>> os.path.isfile(testfiles('tcl'))
    True
    >>> os.path.isfile(testfiles('pfb'))
    True
    """
    if ext is None:
        ext = "*"
    else:
        assert "*" not in ext, "'*' symbol not allowed, got {0}".format(ext)
        ext = "*.{0}".format(ext).replace("..", ".")
    
    pattern = os.path.join(TESTDIR, ext)
    res = glob.glob(pattern)
    if len(res) == 0:
        raise ValueError("No files in testdir")
    elif len(res) == 1:
        res = res[0]
    else:
        pass
    
    return res
    
    


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default = False, action = 'store_true')
    opts = parser.parse_args()
    
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        import doctest
        test_result = doctest.testmod()
        print(test_result)
