"""
Read and write tcl files for parflow
"""
__author__     = "PHYREV team"
__copyright__  = "Copyright 2017"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import os, glob, shutil 
import argparse
import doctest
import pandas as pd
import subprocess
import time
from copy import deepcopy
import numpy as np

from PFlibs import io_tcl
from PFlibs import io_pfb
from PFlibs import PFsimus
from PFlibs.config import testfiles

from netCDF4 import Dataset


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

_tcl_name = "simu.tcl"

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


def calculate_OVFLOW(width,manning,slope,h):
    """
    simple function which return the overland flux from one cell to another
    """
    return (width/manning)*(slope**0.5)*h**(5./3.)
    

def calculate_runoff_maps(htop,slopex,slopey,mannings,nx,ny,dx,dy,nt,mask,KWE='UP',large_array=True,epsilon = 1E-7):
    """
    calculate runoff for each pixel following Rcalc in Priority flow. 
    Two options of flow calc (UP and STAN) and 2 options regarding problem dimensions: using time loops of large matrix)
    
    2D arrays are : htop, slopex, slopey and mannings. first dimension is x (West->East), second is y (South->North). So lower left is [0,0] and upper right is [nx,ny]
    htop is surface pressure
    
    options are:
    - KWE: 'UP' stands for former overland calculation where the slopes are given at the cell centers and upwinded using fluxes
    - KWE: 'Stan' stands for the new OverlandKinematic formulation where the slopes are given at the cell interfaces and upwinded using h
    - large_array: if htop is very large (nx,ny,nt), this is the default, and time loop are being used. Otherwise, a direct 3D matrix method is used (for KWE='UP')
    
    returns:
    outflow: a 3D array (NX,NY,NT)
    
    The comments and R code from Priority flow are included for tracking purposes.
    
    TODO: implement 3D matrix operations for KWE = 'Stan'
    
    """
    outflow = np.zeros((nx,ny,nt))
    if KWE=='UP':
        if large_array==False:
            #if working with large NX NY NT array is doable: 
            qx= -np.sign(np.repeat(slopex[:,:,np.newaxis],nt,axis=2))*np.abs(np.repeat(slopex[:,:,np.newaxis],nt,axis=2))**0.5/np.repeat(mannings[:,:,np.newaxis],nt,axis=2) * np.maximum(htop,np.zeros((nx,ny,nt)))**(5/3) * dy #Units should be l3/t
            qeast= np.maximum(qx[0:(nx-1),:,:],np.zeros((nx-1,ny,nt))) - np.maximum(-qx[1:nx,:,:],np.zeros((nx-1,ny,nt))) # 100s for 2 years oueme domain (144x144)
            qeast=np.concatenate((-np.maximum(-np.reshape(qx[0,:,:],(1,ny,nt)),np.zeros((1,ny,nt))),qeast),axis=0) 
            qeast=np.concatenate((qeast,np.maximum(np.reshape(qx[nx-1,:,:],(1,ny,nt)),np.zeros((1,ny,nt)))),axis=0) 
            qy= -np.sign(np.repeat(slopey[:,:,np.newaxis],nt,axis=2))*np.abs(np.repeat(slopey[:,:,np.newaxis],nt,axis=2))**0.5/np.repeat(mannings[:,:,np.newaxis],nt,axis=2) * np.maximum(htop,np.zeros((nx,ny,nt)))**(5/3) * dx #Units should be l3/t
            qnorth= np.maximum(qy[:,0:(ny-1),:],np.zeros((nx,ny-1,nt))) - np.maximum(-qy[:,1:ny,:],np.zeros((nx,ny-1,nt)))
            qnorth=np.concatenate((-np.maximum(-np.reshape(qy[:,0,:],(nx,1,nt)),np.zeros((nx,1,nt))),qnorth),axis=1) 
            qnorth=np.concatenate((qnorth,np.maximum(np.reshape(qy[:,ny-1,:],(nx,1,nt)),np.zeros((nx,1,nt)))),axis=1) 
            outflow=np.maximum(qeast[1:nx+1,:,:],np.zeros((nx,ny,nt))) + np.maximum(-qeast[0:nx,:,:], np.zeros((nx,ny,nt))) + np.maximum(qnorth[:,1:ny+1,:],np.zeros((nx,ny,nt))) + np.maximum(-qnorth[:,0:ny,:], np.zeros((nx,ny,nt)))
            #otherwise:
        else:
            for i in range(nt):
                ptop = htop[:,:,i]
                ptop[ptop<0]=0
                #####
                #Calculate fluxes across east and north faces
                #First the x direction
                qx= -np.sign(slopex)*np.abs(slopex)**0.5/mannings * ptop**(5/3) * dy #Units should be l3/t

                #Upwinding to get flux across the east face of cells - based in qx[i] if its positive and qx[i+1] if its negative
                #qeast= pmax(qx[1:(nx-1),],zeros[1:(nx-1),]) - pmax(-qx[2:nx,],zeros[2:nx,])
                #TO CHECK max formulaton (axis=0 ?)
                qeast= np.maximum(qx[0:(nx-1),:],np.zeros((nx-1,ny))) - np.maximum(-qx[1:nx,:],np.zeros((nx-1,ny))) # 100s for 2 years oueme domain (144x144)
                #tmp1 =  qx[0:nx-1,:]
                #tmp2 = -qx[1:nx,:]
                #qeast = tmp1.clip(0) - tmp2.clip(0) # 102s for 2 years oueme domain (144x144)
                #qeast = np.where(tmp1>0,tmp1,0) -np.where(tmp2>0,tmp2,0) #  106s for 2 years oueme domain (144x144)
                
                #adding the left boundary - pressures outside domain are 0 so flux across this boundary only occurs when qx[1] is negative
                #qeast= rbind(-pmax(-qx[1,],0), qeast)
                # TO CHECK: 
                qeast=np.concatenate((-np.maximum(-np.reshape(qx[0,:],(1,ny)),np.zeros((1,ny))),qeast),axis=0) 

                #adding the right boundary - pressures outside domain are 0 so flux across this boundary only occurs when qx[nx] is positive
                #qeast= rbind(qeast, pmax(qx[nx,],0))
                qeast=np.concatenate((qeast,np.maximum(np.reshape(qx[nx-1,:],(1,ny)),np.zeros((1,ny)))),axis=0) 

                #####
                #Next the y direction
                #qy= -sign(slopey)*abs(slopey)^0.5/mannings * ptop^(5/3) * dx #Units should be l3/t
                qy= -np.sign(slopey)*np.abs(slopey)**0.5/mannings * ptop**(5/3) * dx #Units should be l3/t

                #Upwinding to get flux across the north face of cells - based in qy[j] if its positive and qy[j+1] if its negative
                #qnorth= pmax(qy[,1:(ny-1)],zeros[,1:(ny-1)]) - pmax(-qy[, 2:ny],zeros[, 2:ny])
                qnorth= np.maximum(qy[:,0:(ny-1)],np.zeros((nx,ny-1))) - np.maximum(-qy[:,1:ny],np.zeros((nx,ny-1)))
                #tmp1 =  qy[:,0:(ny-1)]
                #tmp2 = -qx[:,1:ny]
                #qnorth = tmp1.clip(0) - tmp2.clip(0)
                #qnorth = np.where(tmp1>0,tmp1,0) -np.where(tmp2>0,tmp2,0)
                
                #adding the bottom - pressures outside domain are 0 so flux across this boundary only occurs when qy[1] is negative
                #qnorth= cbind(-pmax(-qy[,1],0), qnorth)
                # TO CHECK: 
                qnorth=np.concatenate((-np.maximum(-np.reshape(qy[:,0],(nx,1)),np.zeros((nx,1))),qnorth),axis=1) 

                #adding the right boundary - pressures outside domain are 0 so flux across this boundary only occurs when qx[nx] is positive
                #qnorth= cbind(qnorth, pmax(qy[,ny],0))
                qnorth=np.concatenate((qnorth,np.maximum(np.reshape(qy[:,ny-1],(nx,1)),np.zeros((nx,1)))),axis=1) 

                #Calculate total outflow
                #Outflow is a postitive qeast[i,j] or qnorth[i,j] or a negative qeast[i-1,j], qnorth[i,j-1]
                #outflow=pmax(qeast[2:(nx+1),],zeros) + pmax(-qeast[1:nx,], zeros) + 
                 #     pmax(qnorth[,2:(ny+1)],zeros) + pmax(-qnorth[, 1:ny], zeros)
                outflow[:,:,i]=np.maximum(qeast[1:nx+1,:],np.zeros((nx,ny))) + np.maximum(-qeast[0:nx,:], np.zeros((nx,ny))) + np.maximum(qnorth[:,1:ny+1],np.zeros((nx,ny))) + np.maximum(-qnorth[:,0:ny], np.zeros((nx,ny)))
                #tmp1 = qeast[1:nx+1,:]
                #tmp2 = -qeast[0:nx,:]
                #tmp3 = qnorth[:,1:ny+1]
                #tmp4 = -qnorth[:,0:ny]
                #outflow[:,:,i] =tmp1.clip(0) + tmp2.clip(0) + tmp3.clip(0) + tmp4.clip(0)
                #outflow[:,:,i]  = np.where(tmp1>0,tmp1,0) + np.where(tmp2>0,tmp2,0) + np.where(tmp3>0,tmp3,0) + np.where(tmp4>0,tmp4,0)
    elif KWE=='Stan':
        for i in range(nt):
            ptop = htop[:,:,i]
            ptop[ptop<0]=0
            #Repeat the slopes on the lower and left boundaries that are inside the domain but outside the mask
            #find indices of all cells that are off the mask but have a neigbor to their right that is on the mask
            #fill.left=which((rbind(mask[2:nx,],rep(0,ny)) - mask[1:nx,]) ==1, arr.ind=T)
            fill_left = np.where((np.concatenate((mask.data[1:nx,:,0],np.zeros((1,ny))),axis=0) - mask.data[:,:,0])==1)                                          
            #get the indices of their neigbors to the right 
            #fill.left2=fill.left
            #fill.left2[,1]=fill.left[,1]+1
            fill_left2 = fill_left
            fill_left2[0][:] = fill_left[0][:]+1
            #pad the slopes to the left with their neigboring cells in the mask 
            #slopex[fill.left]=slopex[fill.left2]
            slopex[fill_left] = slopex[fill_left2]
            #find indices of all cells that are off the mask but have a neigbor above them that is on the mask
            #fill.down=which((cbind(mask[,2:ny],rep(0,nx)) - mask[,1:ny]) ==1, arr.ind=T)
            fill_down = np.where((np.concatenate((mask.data[:,1:ny,0],np.zeros((nx,1))),axis=1) - mask.data[:,:,0])==1)                                          
            #get the indices of their neigbors above
            #fill.down2=fill.down
            #fill.down2[,2]=fill.down[,2]+1
            fill_down2 = fill_down
            fill_down2[1][:]= fill_down[1][:]+1
            #pad the slopes to below  with their neigboring cells in the mask 
            slopey[fill_down]=slopey[fill_down2]

            ####
            #calculate the slope magnitude
            sfmag=np.where((slopex**2+slopey**2)**0.5>epsilon,(slopex**2+slopey**2)**0.5,epsilon)
            #~ sfmag=np.where((slopex*slopex+slopey*slopey)**0.5>epsilon,(slopex*slopex+slopey*slopey)**0.5,epsilon)


            ###
            # IS THIS NEEDED ? (Basile)
            #For OverlandKinematic slopes are face centered and calculated across the upper and right boundaries
            # (i.e. Z[i+1]-Z[i])
            # For cells on the lower and left boundaries its assumed that the slopes repeat 
            # (i.e. repeating the upper and right face boundary for the lower and left for these border cells)
            #slopex.pad=rbind(slopex[1,], slopex)
            #slopey.pad=cbind(slopey[,1], slopey)

            ####
            # upwind the pressure - Note this is for the north and east face of all cells
            # The slopes are calculated across these boundaries so the upper boundary is included in these 
            # calculations and the lower and righ boundary of the domain will be added later
            #pupwindx=pmax(sign(slopex)*rbind(ptop[2:(nx),], rep(0,ny)),0) +
            #       pmax(-sign(slopex)*ptop[1:nx,], 0 )
            #pupwindy=pmax(sign(slopey) * cbind(ptop[,2:ny], rep(0,nx)),0) +
            #       pmax(-sign(slopey) * ptop[, 1:ny],0)
            pupwindx = np.maximum(np.sign(slopex)*np.concatenate((ptop[1:nx,:],np.zeros((1,ny))),axis=0),np.zeros((nx,ny))) + np.maximum(-np.sign(slopex)*ptop[0:nx,:],np.zeros((nx,ny)))
            pupwindy = np.maximum(np.sign(slopey)*np.concatenate((ptop[:,1:ny],np.zeros((nx,1))),axis=1),np.zeros((nx,ny))) + np.maximum(-np.sign(slopey)*ptop[:,0:ny],np.zeros((nx,ny)))
            ###
            # Calculate fluxes across east and north faces
            # First the x direction
            #qeast = -slopex/(sfmag^0.5*mannings) * pupwindx^(5/3) *dy #Units should be l3/t
            #qnorth = -slopey/(sfmag^0.5*mannings) * pupwindy^(5/3) *dx #Units should be l3/t
            qeast = -slopex / ((sfmag**0.5)*mannings) * pupwindx**(5/3) * dy
            qnorth = -slopey / ((sfmag**0.5)*mannings) * pupwindy**(5/3) * dx
            ###
            #Fix the lower x boundary 
            # Use the slopes of the first column with the pressures for cell i 
            #qleft=-slopex[1,]/(sfmag[1,]^0.5*mannings)* (pmax(sign(slopex[1,])*ptop[1,],0))^(5/3) * dy
            #qeast=rbind(qleft,qeast)
            qleft = -slopex[0,:]/((sfmag[0,:]**0.5)*mannings[0,:]) * (np.maximum(np.sign(slopex[0,:])*ptop[0,:],np.zeros((1,ny))))**(5/3) * dy
            qeast = np.concatenate((qleft,qeast),axis=0)
            ###
            # Fix the lower y boundary 
            # Use the slopes of the bottom row with the pressures for cell j
            #qbottom=-slopey[,1]/(sfmag[,1]^0.5*mannings)* (pmax(sign(slopey[,1])*ptop[,1],0))^(5/3) * dx
            #qnorth=cbind(qbottom,qnorth)
            # beware here sfmag[:,0] for instance produce a row vector ! of dimension (144,) and  np.zeros((nx,1)).shape = (144, 1) while np.zeros((nx)).shape = (144,)

            qbottom = -slopey[:,0]/((sfmag[:,0]**0.5)*mannings[:,0]) * (np.maximum(np.sign(slopey[:,0])*ptop[:,0],np.zeros((nx))))**(5/3) * dx
            qnorth = np.concatenate((qbottom[:,np.newaxis],qnorth),axis=1)
            
            outflow[:,:,i]=np.maximum(qeast[1:nx+1,:],np.zeros((nx,ny))) + np.maximum(-qeast[0:nx,:], np.zeros((nx,ny))) + np.maximum(qnorth[:,1:ny+1],np.zeros((nx,ny))) + np.maximum(-qnorth[:,0:ny], np.zeros((nx,ny)))

    return outflow
    

##======================================================================================================================##
##                CLASSES                                                                                               ##
##======================================================================================================================##

class PFout(object):
    """ Class doc
    Single simulation outputs: 
    
    properties: 
    - rundir: 			Simulation directory
    - runname :         Simulation runname
    - n_t :             PF number of timestep
    - n_t_clm:          CLM number of timestep
    - n_t_forc:         Forcing number of timestep
    - dump_int :        PF dump interval
    - dump_int_clm:     CLM dump interval
    - conv :            converting PF units
    - conv_clm:         converting CLM units
    - conv_forc:        converting Forcings units



    """
    def __init__(self, rundir, runname):
        """ Class initialiser """
        self.rundir = rundir
        self.runname = runname
        self.runprefix = os.path.join(rundir, '.'.join([runname, "out"]))


        
    def __str__(self):
        """Object representation"""
        print('rundir: %s'%self.rundir)
        print('runname: %s'%self.runname)
        return self.rundir


    def set_reading_parameters(self,n_t=1,n_t_clm=1,n_t_forc=1,dump_int=1,dump_int_clm=1,conv=1,conv_clm=1,conv_forc=1):
        """
        """
        self.n_t = n_t
        self.n_t_clm = n_t_clm
        self.n_t_forc = n_t_forc
        self.dump_int = dump_int
        self.dump_int_clm = dump_int_clm
        self.conv = conv
        self.conv_clm = conv_clm
        self.conv_forc = conv_forc
    
    def read_domain_dimensions_from_NC(self,filename):
        """
        get domain dimensions
        """
        with Dataset(filename,'r') as ds:
            [t,self.nx,self.ny,self.nz]=[val.size for k,val in ds.dimensions.items()]
        
        
    def read_latlon_from_NC(self,filename,xvarname='x',yvarname='y'):
        """
        get x and y vectors : ! not necessarily cell-centered
        """
        with Dataset(filename,'r') as ds:
            self.x=ds.variables[xvarname][:]
            self.y=ds.variables[yvarname][:]
            self.dx=np.diff(self.x).mean()
            self.dy=np.diff(self.y).mean()
  
    def read_z_from_NC(self, filename):
        """
        read variable DZ and create the Z vector. Currently only if 
        variables 'depth' and 'var_dz' are present in the .nc
        """
        with Dataset(filename, 'r') as ds:
            self.dz_mult_arr = np.swapaxes(ds.variables['var_dz'][:],0,2)
            self.dz = np.diff(ds.variables['depth'][:])[0]
            self.z = np.cumsum(self.dz_mult_arr[0,0,::-1]*self.dz) - self.dz_mult_arr[0,0,::-1]*self.dz/2
            self.z = np.round(self.z*1000)/1000        
        
    def read_time_dimensions_from_NC(self,filename,filetype='PF'):
        """
        filetype is 'PF' or 'CLM' to identify whether output from
        PF or CLM (and hence to derive specific )
        """
        with Dataset(filename,'r') as ds:
            if filetype == 'PF':
                self.n_t = [val.size for k,val in ds.dimensions.items()][0]
            elif filetype == 'CLM':
                self.n_t_clm = [val.size for k,val in ds.dimensions.items()][0]
                

    def read_domain_dimensions_from_PFBs(self):
        """
        Read the first pressure file from the rundir to get domain dimensions from the header
        """
        # +1 car 00000.pfb existe
        if os.path.isfile('.'.join([self.runprefix,"press",format(0,"05.0F"),"pfb"])):
            [test_array, hdr] =  io_pfb.PFB_read('.'.join([self.runprefix,"press",format(0,"05.0F"),"pfb"]),1)                  
            self.nz = hdr['nz']
            self.nx = hdr['nx']
            self.ny = hdr['ny']
            self.dx = hdr['dx']
            self.dy = hdr['dy']       
            self.dz = hdr['dz']
            self.X0 = hdr['X0']
            self.Y0 = hdr['Y0']
            self.Z0 = hdr['Z0']
            self.surf = self.nx*self.dx*self.ny*self.dy
            self.y = [self.dy*y+self.dy/2 for y in range(self.ny)]
            self.x = [self.dx*x+self.dx/2 for x in range(self.nx)]
        else: 
            print('cant find file: %s'%'.'.join([self.runprefix,"press",format(0,"05.0F"),"pfb"]))
        
        
    def read_3DPFoutput_from_PFBs(self,variable,nt):
        """
        read PF outputs from PFBs
        possible variables :
        -press (nt = self.n_t)
        -satur (nt = self.n_t)
        TO DO: make it more flexible
        """
            
        tmp_array = np.zeros((self.nx,self.ny,self.nz,nt))
        for t in range(nt):
            # Get all time file directories
            tmp_fp = '.'.join([self.runprefix,variable,format(t*self.dump_int,"05.0F"),"pfb"])
            # Read pfb
            tmp_arr = io_pfb.PFB_read(tmp_fp,0)           
            #Fill arrays for each time output
            tmp_array[:,:,:,t]=tmp_arr[:,:,:]

        return tmp_array
        
    def read_2DPFoutput_from_PFBs(self,variable,nt):
        """
        read PF outputs from PFBs
        possible variables :
        -overlandsum (nt = self.n_t-1)
        -evaptranssum (nt = self.n_t-1)
        -evaptrans (nt = self.n_t-1)
        TO DO: make it more flexible
        """
            
        tmp_array = np.zeros((self.nx,self.ny,nt))
        for t in range(nt):
            # Get all time file directories
            tmp_fp = '.'.join([self.runprefix,variable,format((t+1)*self.dump_int,"05.0F"),"pfb"])
            # Read pfb
            tmp_arr = io_pfb.PFB_read(tmp_fp,0)           
            #Fill arrays for each time output
            tmp_array[:,:,t]=tmp_arr[:,:,0]

        return tmp_array
     
    
    def read_allPFoutput_from_PFBs(self,v=True):
        """
        read PF outputs from PFBs
        TO DO: make it more flexible
        """
        if v: start_time = time.time()
        self.press_array = self.read_3DPFoutput_from_PFBs('press',self.n_t)
        self.satur_array = self.read_3DPFoutput_from_PFBs('satur',self.n_t)
        self.overlandsum_array = self.read_2DPFoutput_from_PFBs('overlandsum',self.n_t-1)
        self.evaptranssum_array = self.read_2DPFoutput_from_PFBs('evaptranssum',self.n_t-1)
        self.evaptrans_array = self.read_2DPFoutput_from_PFBs('evaptrans',self.n_t-1)
        if v: print("--- %s seconds --- read all PF outputs from PFBs" % (time.time() - start_time))

        
        
    def read_CLMoutput(self, nlayers = 36, v=True):
        """
        read CLM outputs
        TO DO: make it more flexible
        """        
        if v: start_time = time.time()
        # 36 nombre de sorties clm    
        clm_output_array = np.zeros((self.nx,self.ny,nlayers,self.n_t_clm))

        for t in range(self.n_t_clm):        
            # Get all time file directories
            clm_output_fp = '.'.join([self.runprefix,"clm_output",format((t+1)*self.dump_int_clm,"05.0F"),"C.pfb"])
            # Read pfb
            clm_output_arr = io_pfb.PFB_read(clm_output_fp,0)
            #Fill arrays for each time output
            clm_output_array[:,:,:,t]= clm_output_arr[:,:,:]
            
        self.clm_output_array = clm_output_array

        if v: print("--- %s seconds --- read all CLM outputs from PFBs" % (time.time() - start_time))

        

    def extract_CLM_2Doutputs(self, nlayers = 36):
        """
        """

        # latent heat W/m2
        lh_tot = []
        #outgoing lw radiation W/m2
        lwrad_out = []        
         # sensible heat W/m2
        sh_tot = []
        # ground heat flux W/m2
        soil_grnd = []        
        #total evapotranspiration mm/s
        evap_tot=[]
        #ground evaporation without condesation mm/s
        evap_ground = []        
         #soil evaporation mm/s
        evap_soil = []       
        #vegetation evaporation + transpiration mm/s
        evap_veg = []      
        # vegeation transpiration mm/s
        tran_veg = []
        # soil infiltration mm/s
        infl = []
        # Snow Water Equivalent
        swe_out = []
        # Ground temperature
        t_grnd = []
        # Surface pressure
        surf_press = []
        
                
        for i in range(self.n_t_clm) :
            lh_tot.append(self.clm_output_array[:,:,nlayers-1,i].mean())
            lwrad_out.append(self.clm_output_array[:,:,nlayers-2,i].mean())
            sh_tot.append(self.clm_output_array[:,:,nlayers-3,i].mean())
            soil_grnd.append(self.clm_output_array[:,:,nlayers-4,i].mean())            
            evap_tot.append(self.clm_output_array[:,:,nlayers-5,i].mean()*self.conv_clm)
            evap_ground.append(self.clm_output_array[:,:,nlayers-6,i].mean()*self.conv_clm)
            evap_soil.append(self.clm_output_array[:,:,nlayers-7,i].mean()*self.conv_clm)
            evap_veg.append(self.clm_output_array[:,:,nlayers-8,i].mean()*self.conv_clm)
            tran_veg.append(self.clm_output_array[:,:,nlayers-9,i].mean()*self.conv_clm)
            infl.append(self.clm_output_array[:,:,nlayers-10,i].mean()*self.conv_clm)
            #~ swe_out.append(self.clm_output_array[:,:,nlayers-11,i].mean()*self.conv_clm)
            t_grnd.append(self.clm_output_array[:,:,nlayers-12,i].mean()*self.conv_clm)
            surf_press.append(self.clm_output_array[:,:,nlayers-13,i].mean()*self.conv_clm)

        self.lh_tot = lh_tot
        self.lwrad_out = lwrad_out
        self.sh_tot = sh_tot
        self.soil_grnd = soil_grnd
        self.evap_tot = evap_tot        
        self.evap_ground = evap_ground
        self.evap_soil = evap_soil
        self.evap_veg = evap_veg      
        self.tran_veg = tran_veg
        self.infl = infl
        #~ self.swe_out = swe_out
        self.t_grnd = t_grnd
        self.surf_press = surf_press

    def read_domain(self,def_value = 0):
        """
        """
        filenames = glob.glob(os.path.join(self.rundir,'*domain*.pfb'))
        tmp_arr = io_pfb.PFB_read(filenames[0],0)
        return tmp_arr

    def read_constant_outputs(self,variable,def_value):
        """
        read a single constant output, and if not present, replace it by default value
        """
        if os.path.isfile('.'.join([self.runprefix,variable])):
            tmp_fp = '.'.join([self.runprefix,variable])
            tmp_arr = io_pfb.PFB_read(tmp_fp,0)
        else:
            tmp_arr = def_value
        return tmp_arr
            
    def read_allconstant_outputs(self, v=True):
        """
        Generic function for reading a certain number of classical constant output
        should have a pattern matching approach for domains
        """
        if v: start_time = time.time()                        
        self.dz_mult_arr = self.read_constant_outputs('dz_mult.pfb',np.zeros((self.nx,self.ny,self.nz))+1)
        self.mannings_arr = self.read_constant_outputs('mannings.pfb',np.zeros((self.nx,self.ny))+5.5e-6)
        self.mask_arr = self.read_constant_outputs('mask.pfb',np.zeros((self.nx,self.ny,self.nz))+1)
        self.slope_x_arr = self.read_constant_outputs('slope_x.pfb',np.zeros((self.nx,self.ny)))
        self.slope_y_arr = self.read_constant_outputs('slope_y.pfb',np.zeros((self.nx,self.ny)))
        self.porosity_arr = self.read_constant_outputs('porosity.pfb',np.zeros((self.nx,self.ny,self.nz))+1)
        self.perm_x_arr = self.read_constant_outputs('perm_x.pfb',np.zeros((self.nx,self.ny)))
        self.perm_y_arr = self.read_constant_outputs('perm_y.pfb',np.zeros((self.nx,self.ny)))
        self.perm_z_arr = self.read_constant_outputs('perm_z.pfb',np.zeros((self.nx,self.ny)))
        self.specific_storage_arr = self.read_constant_outputs('specific_storage.pfb',np.zeros((self.nx,self.ny,self.nz)))
        self.domain_arr = self.read_constant_outputs('donga_domains.pfb',np.zeros((self.nx,self.ny,self.nz)))
        if v: print("--- %s seconds --- read all constant outputs from PFBs" % (time.time() - start_time))

        
    def mask_PF_arrays(self):
        """
        use self.mask_arr to mask out PF arrays:
        """
        for i in range(len(self.n_t)):
            tmp1 = self.press_array[:,:,:,i]
            tmp2 = self.satur_array[:,:,:,i]
            tmp1[self.mask_arr == 0] = np.nan
            tmp2[self.mask_arr == 0] = np.nan
            self.press_array[:,:,:,i] = tmp1
            self.satur_array[:,:,:,i] = tmp2
            
        
    def read_forcings(self, forcing_file = "forcagePF.txt.0"):
        """
        Read forcing file
        """
        forcages = np.loadtxt(os.path.join(self.rundir,forcing_file))

        self.SW = forcages[0:self.n_t_forc,0]
        self.LW = forcages[0:self.n_t_forc,1]
        self.Pluie = forcages[0:self.n_t_forc,2]
        self.Temp = forcages[0:self.n_t_forc,3]
        self.WindNS = forcages[0:self.n_t_forc,4]
        self.WindEW = forcages[0:self.n_t_forc,5]
        self.Press =forcages[0:self.n_t_forc,6]
        self.Hum = forcages[0:self.n_t_forc,7]
        self.P = sum(self.Pluie)*self.conv_forc
    
    
    def calculate_runoff(self,x_outlet = 0,y_outlet = 0):
       """
       """
       S = self.slope_y_arr[x_outlet,y_outlet,0]
       mannings_runoff = []
       m = self.mannings_arr[x_outlet,y_outlet]
       for i in range(self.n_t):    
           if self.press_array[x_outlet,y_outlet,0,i] >= 0 :
               mannings_runoff.append((self.dx/m)*(S**0.5)*(self.press_array[x_outlet,y_outlet,0,i])**(5./3.)*(1000/self.surf)*self.conv*self.dump_int)
           else :
               mannings_runoff.append(0.)

       self.mannings_runoff = mannings_runoff
       """ Runoff OEVRLANDSUM in mm """
       #~ 
       overland_runoff = []
       overland_runoff_tmp = self.overlandsum_array[x_outlet,y_outlet,:]
       for i in range(len(overland_runoff_tmp)) : overland_runoff.append(overland_runoff_tmp[i]*1000/self.surf)
       self.overland_runoff =overland_runoff
       
    def get_runoff_from_PF(self,x_outlet = 0,y_outlet = 0):
        """
        """
        """ Runoff OEVRLANDSUM in mm """
        overland_runoff = []
        overland_runoff_tmp = self.overlandsum_array[x_outlet,y_outlet,:]
        for i in range(len(overland_runoff_tmp)) : overland_runoff.append(overland_runoff_tmp[i]*1000/self.surf)
        self.overland_runoff =overland_runoff
             
    def calculate_runoff_from_CLM(self,x_outlet = 0,y_outlet = 0, nlayers = 36):
        """
        """
        Sy = self.slope_y_arr[x_outlet,y_outlet]
        Sx = self.slope_x_arr[x_outlet,y_outlet]

        surfQ = []
        m = self.mannings_arr[x_outlet,y_outlet]
        
        for i in range(self.n_t_clm):    
            if self.clm_output_array[x_outlet,y_outlet,nlayers-13,i] >= 0 :
                Qx = calculate_OVFLOW(width = self.dy, manning = m[0], slope = Sx[0],h = self.clm_output_array[x_outlet,y_outlet,nlayers-13,i]) 
                #~ Qx = Qx[0] *(1000/self.surf)*self.conv_clm*self.dump_int_clm
                # m3/hr => m3 
                Qx = Qx[0]
                Qy = calculate_OVFLOW(width = self.dx, manning = m[0], slope = Sy[0],h = self.clm_output_array[x_outlet,y_outlet,nlayers-13,i]) 
                #~ Qy = Qy[0] *(1000/self.surf)*self.conv_clm*self.dump_int_clm    
                Qy = Qy[0] 
                
                surfQ.append(np.sqrt((Qx * Qx) + (Qy * Qy)))
            else :
                surfQ.append(0.)
        self.surfQ = surfQ
        
    def calculate_runoff_from_press(self,x_outlet = 0,y_outlet = 0):
        """
        calculate runoff from self.surf_press as can be read earlier from CLM C file
        (or associated NC file)
        """
        Sy = np.abs(self.slope_y_arr[x_outlet,y_outlet])
        Sx = np.abs(self.slope_x_arr[x_outlet,y_outlet])

        surfQ = []
        m = self.mannings_arr[x_outlet,y_outlet]
        
        for i in range(self.n_t_clm):
            if self.surf_press[x_outlet,y_outlet,i] >= 0 :
                Qx = calculate_OVFLOW(width = self.dy, manning = m, slope = Sx,h = self.surf_press[x_outlet,y_outlet,i]) 
                #~ Qx = Qx[0] *(1000/self.surf)*self.conv_clm*self.dump_int_clm
                # m3/hr => m3 
                Qy = calculate_OVFLOW(width = self.dx, manning = m, slope = Sy,h = self.surf_press[x_outlet,y_outlet,i]) 
                #~ Qy = Qy[0] *(1000/self.surf)*self.conv_clm*self.dump_int_clm    
                
                #~ surfQ.append(np.sqrt((Qx * Qx) + (Qy * Qy)))
                surfQ.append(np.abs(Qx) + np.abs(Qy))
            else :
                surfQ.append(0.)
        self.surfQ = surfQ
        
    def calculate_WSC(self, conv = 1000):
        """
        Conv = convrting metric units: 1000 supposes m -> mm
        """
            
        mailles_array = np.zeros((self.nx,self.ny,self.nz,self.n_t))

        var_stock = []

        for t in range(self.n_t):
            for i in range(self.ny):
                for j in range(self.nz):
                    vol_maille = self.dx*self.dy*self.dz*self.dz_mult_arr[0,i,j]*self.porosity_arr[0,i,j]*self.satur_array[0,i,j,t]
                    #~ if press_array[0,i,j,t] > 0 :
                    if self.press_array[0,i,j,t] > -100000 :
                        mailles_array[0,i,j,t]=vol_maille + self.press_array[0,i,j,t]*self.specific_storage_arr[0,0,0]*(self.satur_array[0,i,j,t]*self.dx*self.dy*self.dz*self.dz_mult_arr[0,i,j])
                    else :
                        mailles_array[0,i,j,t]=vol_maille

        for t in range(self.n_t-1):    
                vol_t_avant = np.sum(mailles_array[:,:,:,t])*conv/self.surf
                vol_t_apres = np.sum(mailles_array[:,:,:,t+1])*conv/self.surf
                diff = vol_t_apres-vol_t_avant
                var_stock.append(diff)
        self.var_stock = var_stock

        #stock simple en mm
        stock_simple = [0]
        for i in range(self.n_t):            
            stock_simple.append(np.sum(mailles_array[:,:,:,i]))


        self.var_tot = np.sum(mailles_array[:,:,:,self.n_t-1])*conv/self.surf - np.sum(mailles_array[:,:,:,0])*conv/self.surf
        self.var_cum = sum(var_stock)

    def calculate_WT(self,xloc=0,yloc=0):
        """
        Extract all water table depths at the location xloc,yloc (indices)
        """
        
        """ extract a T-D slice """
        domain_thickness = np.round(np.cumsum(self.dz_mult_arr[0,0,::-1]*self.dz)[-1]*1000)/1000
        depth_last_cell = self.z[-1]
        WT = domain_thickness - self.press_array[xloc,yloc,0,:]
        WT = depth_last_cell - self.press_array[xloc,yloc,0,:]

        """ Get All Water tables (perched)"""
        #first get a mask of the base layers of each saturated (p>0) contiguous (over Z) unit
        maskWT=np.zeros([self.nz,self.n_t])
        for t in range(self.n_t):
            profile = np.zeros(self.nz)
            #profile[0] = base of domain. if the criteria is p < 0 => 0, you realize there are some weird points at the bottom
            #of perched layers with p>0 but < self.dz ... so as if the cell is not entirely saturated but p>0 ... 
            profile = np.array([0. if self.press_array[xloc,yloc,i,t]<= self.dz_mult_arr[xloc,yloc,i]*self.dz/2 else 1. for i in range(self.nz)])
            for i in range(self.nz-1):
                profile[i+1]=(profile[i]+profile[i+1])*profile[i+1]
            # a=[1,1,1,0,0,0,1,1,0,1,0,0,0,1] => [1, 2, 3, 0, 0, 0, 1, 2, 0, 1, 0, 0, 0, 1]
            profile[profile!=1.]=np.nan
            maskWT[:,t] = profile

        WTs = np.resize(self.z[::-1],[self.n_t,self.nz]).T - (maskWT*self.press_array[xloc,yloc,:,:])
        return WTs        


    def print_budget(self):
        """
        """

        Q = sum(self.overland_runoff)
        dS = sum(self.var_stock)
        E = sum(self.evap_tot)
        Sum_parflow = Q + self.var_cum + E

        print('\n The total rain is {} mm \n'.format(self.P))        
        print('The total PF overland runoff is {} mm \n'.format(Q))
        print('The total overland runoff re-calculated is {} mm \n'.format(sum(self.mannings_runoff)))
        print('The total evapotranspiration is {} mm \n'.format(E))
        print('The total Water Storage Change is {} mm \n'.format(self.var_tot))
        print('Runoff coefficient  {} \n'.format(Q/self.P))
        print('TOTAL  Runoff, infl, evap water is {} mm \n'.format(Sum_parflow))
        print('ERROR PARFLOW P-E-V-R : {} mm \n'.format(self.P-Sum_parflow))        
        
    def calculate_Qr(self):
        """
        """        
        return sum(self.overland_runoff) / self.P


    def create_panda_series(self,variable,start_date = "1/1/2007", start_hour = "00:00", freq = "1D", periods =1):
        """
        """
        # create time series index
        t_ind = pd.date_range(start = ' '.join([start_date, start_hour]), freq = freq , periods = periods)
        # create time series with associated index:
        ts = pd.Series(variable,index = t_ind,dtype=float)
        return ts
        
    def create_some_panda_series(self,start_date = "1/1/2007", start_hour = "00:00", freq_PF = "1D", freq_clm = "1H", freq_forc = "30min"):
        """
        That's really more like an exemple, maybe should not be in this lib...
        
        """
        self.ts_ov = self.create_panda_series(self.overland_runoff,start_date,start_hour,freq_PF,periods = self.n_t-1)
        self.ts_ov_cumsum = self.create_panda_series(np.cumsum(self.overland_runoff),start_date,start_hour,freq_PF,periods = self.n_t-1)
        self.ts_stock = self.create_panda_series(np.cumsum(self.var_stock),start_date,start_hour,freq_PF,periods = self.n_t-1)
        
        self.ts_evap = self.create_panda_series(self.evap_tot,start_date,start_hour,freq_clm,periods = self.n_t_clm)
        self.ts_evap_cumsum = self.create_panda_series(np.cumsum(self.evap_tot),start_date,start_hour,freq_clm,periods = self.n_t_clm)
        self.ts_evap_ground = self.create_panda_series(self.evap_ground,start_date,start_hour,freq_clm,periods = self.n_t_clm)
        self.ts_evap_veg = self.create_panda_series(self.evap_veg,start_date,start_hour,freq_clm,periods = self.n_t_clm)
        self.ts_tran_veg = self.create_panda_series(self.tran_veg,start_date,start_hour,freq_clm,periods = self.n_t_clm)
        self.ts_tran_veg_cumsum = self.create_panda_series(np.cumsum(self.tran_veg),start_date,start_hour,freq_clm,periods = self.n_t_clm)

        self.ts_rain = self.create_panda_series(self.Pluie*self.conv_forc,start_date,start_hour,freq_forc,periods = self.n_t_forc)
        self.ts_rain_cumsum = self.create_panda_series(np.cumsum(self.Pluie*self.conv_forc),start_date,start_hour,freq_forc,periods = self.n_t_forc)


        # create daily average
        self.ts_evap_d = self.ts_evap.resample('D').sum()
        self.ts_evap_ground_d = self.ts_evap_ground.resample('D').sum()
        self.ts_evap_veg_d = self.ts_evap_veg.resample('D').sum()


        #create dataframe object for easy plotting
        self.pf_out_cum = pd.DataFrame({'Q':self.ts_ov,
                                'cumsum(Q)':self.ts_ov_cumsum,
                                'S':self.ts_stock})
        self.clm_out_cum = pd.DataFrame({'cumsum(ETR)':self.ts_evap_cumsum})
        self.forcings_cum = pd.DataFrame({'cumsum(P)':self.ts_rain_cumsum})
        self.pf_out = pd.DataFrame({'Q':self.ts_ov,
                                'S':self.ts_stock,
                                'ETRday':self.ts_evap_d})
        #~ pf_out = pd.DataFrame({'Q':ts_ov,
                                #~ 'S':ts_stock,
                                #~ 'ETRday':ts_evap_d,
                                #~ 'evap_grndday':ts_evap_ground_d,
                                #~ 'evap_vegday':ts_evap_veg_d})                        

        self.pf_out = pd.DataFrame({'Q':self.ts_ov,
                                'ETRday':self.ts_evap_d,
                                'evap_grndday':self.ts_evap_ground_d,
                                'evap_vegday':self.ts_evap_veg_d})                        
                                                  
                                
        self.clm_out = pd.DataFrame({'ETR':self.ts_evap})

        #~ clm_out = pd.DataFrame({'ETR':ts_evap,
                                #~ 'evap_grnd':ts_evap_ground,
                                #~ 'evap_veg':ts_evap_veg,
                                #~ 'tran_veg':ts_tran_veg})

        self.forcings = pd.DataFrame({'P':self.ts_rain})
