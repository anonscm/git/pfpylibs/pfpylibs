# sorties PF: 1/jour, CLM:1/0.5h

# Import the ParFlow TCL package
#
lappend auto_path $env(PARFLOW_DIR)/bin 
package require parflow
namespace import Parflow::*

#-----------------------------------------------------------------------------
# File input version number
#-----------------------------------------------------------------------------
pfset FileVersion 4

#-----------------------------------------------------------------------------
# Process Topology
#-----------------------------------------------------------------------------
pfset Process.Topology.P        1
pfset Process.Topology.Q        1
pfset Process.Topology.R        1

#-----------------------------------------------------------------------------
# Prepare input files
#-----------------------------------------------------------------------------

#file delete -force results
#file mkdir results
#cd results


# FOR 1D
#file copy ../forcagePF.200707.30mn_updated.txt.0 ./forcagePF.txt.0
#file copy ../forcagePF.pluie_35mm.txt.0 ./forcagePF.txt.0
#file copy ../lai.dat ./lai.dat
#file copy ../sai.dat ./sai.dat
#file copy ../z0m.dat ./z0m.dat
#file copy ../displa.dat ./displa.dat
#file copy ../veg_map.pfb ./veg_map.pfb

#file copy ../drv_clmin_200707.dat.0 ./drv_clmin.dat
#file copy ../drv_vegm_hillslope2D.dat ./drv_vegm.dat
#file copy ../drv_vegp.dat ./drv_vegp.dat

#file copy ../Xslope.pfb ./xslope.pfb
#file copy ../Yslope.pfb ./yslope.pfb
#file copy ../var_dz.pfb ./var_dz.pfb
#file copy ../hillslope_sens.tcl ./hillslope_sens.tcl
#file copy ../dgridZ.pfb ./dgridZ.pfb


#-----------------------------------------------------------------------------
# Computational Grid
#-----------------------------------------------------------------------------


pfset ComputationalGrid.Lower.X                	5.0
pfset ComputationalGrid.Lower.Y                	5.0
pfset ComputationalGrid.Lower.Z                	0.

pfset ComputationalGrid.DX	               	10.
pfset ComputationalGrid.DY	                 10. 
pfset ComputationalGrid.DZ	               	0.75

pfset ComputationalGrid.NX	     	       	1
pfset ComputationalGrid.NY	     		50
#compulsory formalism for 2D pfb files:
pfset ComputationalGrid.NZ                  	1 
pfdist Xslope.pfb
pfdist Yslope.pfb
#FOR 1D:
#pfdist veg_map.pfb

pfset ComputationalGrid.NZ                    	32

pfdist var_dz.pfb

puts {pfdist OK}
#-----------------------------------------------------------------------------
# The Names of the GeomInputs
#-----------------------------------------------------------------------------

pfset GeomInput.Names "domain_input H1_input H2_input H3_input H4_input H5_input"

#-----------------------------------------------------------------------------
# Domain Geometry Input
#-----------------------------------------------------------------------------
pfset GeomInput.domain_input.InputType            Box
pfset GeomInput.domain_input.GeomName  domain

pfset Geom.domain.Lower.X                        0
pfset Geom.domain.Lower.Y                        0
pfset Geom.domain.Lower.Z                        0
 
pfset Geom.domain.Upper.X                        10
pfset Geom.domain.Upper.Y                        500

# this upper is synched to computational grid, not linked w/ Z multipliers

pfset Geom.domain.Upper.Z                        24

pfset Geom.domain.Patches             "x-lower x-upper y-lower y-upper z-lower z-upper"

# the vertical coordinate origin is at the bottom
# horizon H1 surface 4cm permet de representer la croute des sol du Niger
pfset GeomInput.H1_input.InputType            Box
pfset GeomInput.H1_input.GeomName             H1

pfset Geom.H1.Lower.X                        0.0 
pfset Geom.H1.Lower.Y                        0.0
pfset Geom.H1.Lower.Z                        23.96

pfset Geom.H1.Upper.X                        10.
pfset Geom.H1.Upper.Y                        500.
pfset Geom.H1.Upper.Z                        24. 

# horizon H2 surface ==> 50cm zone racinaire des herbacees
pfset GeomInput.H2_input.InputType            Box
pfset GeomInput.H2_input.GeomName             H2

pfset Geom.H2.Lower.X                        0.0 
pfset Geom.H2.Lower.Y                        0.0
pfset Geom.H2.Lower.Z                        23.5

pfset Geom.H2.Upper.X                        10.
pfset Geom.H2.Upper.Y                        500.
pfset Geom.H2.Upper.Z                        23.96

# horizon H3  ==> 130 permet de representer le gradient de propriete dans la ZNS 
pfset GeomInput.H3_input.InputType            Box
pfset GeomInput.H3_input.GeomName             H3

pfset Geom.H3.Lower.X                        0.0 
pfset Geom.H3.Lower.Y                        0.0
pfset Geom.H3.Lower.Z                        22.7

pfset Geom.H3.Upper.X                        10.
pfset Geom.H3.Upper.Y                        500.
pfset Geom.H3.Upper.Z                        23.5

# horizon H4  ==> 250 Zone argileuse des sols au benin
pfset GeomInput.H4_input.InputType            Box
pfset GeomInput.H4_input.GeomName             H4

pfset Geom.H4.Lower.X                        0.0 
pfset Geom.H4.Lower.Y                        0.0
pfset Geom.H4.Lower.Z                        21.5

pfset Geom.H4.Upper.X                        10.
pfset Geom.H4.Upper.Y                        500.
pfset Geom.H4.Upper.Z                        22.7


# horizon H5  ==> 24 epaisseur du Regolhite
pfset GeomInput.H5_input.InputType            Box
pfset GeomInput.H5_input.GeomName             H5

pfset Geom.H5.Lower.X                        0.0 
pfset Geom.H5.Lower.Y                        0.0
pfset Geom.H5.Lower.Z                        0.0

pfset Geom.H5.Upper.X                        10.
pfset Geom.H5.Upper.Y                        500.
pfset Geom.H5.Upper.Z                        21.5
#---------------------------------------------------------
# Topo slopes in x-direction (si PFB: doit etre 3D)
#---------------------------------------------------------
#pfset TopoSlopesX.Type 				"Constant"
pfset TopoSlopesX.Type 				PFBFile
pfset TopoSlopesX.GeomNames 			"domain"
#pfset TopoSlopesX.Geom.domain.Value 		0
pfset TopoSlopesX.FileName			Xslope.pfb
#---------------------------------------------------------
# Topo slopes in y-direction (si PFB: doit etre 3D)
#---------------------------------------------------------
#pfset TopoSlopesY.Type 				"Constant"
pfset TopoSlopesY.Type 				PFBFile 
pfset TopoSlopesY.GeomNames 			"domain"
#pfset TopoSlopesY.Geom.domain.Value 		0
pfset TopoSlopesY.FileName			Yslope.pfb
#-----------------------------------------------------------------------------
# Domain
#-----------------------------------------------------------------------------
pfset Domain.GeomName              domain


#-----------------------------------------------------------------------------
# Variable DZ
#-----------------------------------------------------------------------------
pfset Solver.TerrainFollowingGrid                        True
pfset Solver.Nonlinear.VariableDz                        True
pfset dzScale.GeomNames				        domain
pfset dzScale.Type            				PFBFile
pfset Geom.domain.dzScale.FileName			var_dz.pfb
#-----------------------------------------------------------------------------
# Phases
#-----------------------------------------------------------------------------
pfset Phase.Names                       "water"

pfset Phase.water.Density.Type	        Constant
pfset Phase.water.Density.Value	        1.0

pfset Phase.water.Viscosity.Type	Constant
pfset Phase.water.Viscosity.Value	1.0

#-----------------------------------------------------------------------------
# Phase sources:
#-----------------------------------------------------------------------------
pfset PhaseSources.water.Type                         Constant
pfset PhaseSources.water.GeomNames                    domain
pfset PhaseSources.water.Geom.domain.Value            0.0

#-----------------------------------------------------------------------------
# Mobility
#-----------------------------------------------------------------------------
pfset Phase.water.Mobility.Type        Constant
pfset Phase.water.Mobility.Value       1.0

#-----------------------------------------------------------------------------
# Contaminants
#-----------------------------------------------------------------------------
pfset Contaminants.Names		""

#-----------------------------------------------------------------------------
# Gravity
#-----------------------------------------------------------------------------
pfset Gravity				1.0

#-----------------------------------------------------------------------------
# Porosity
#-----------------------------------------------------------------------------
pfset Geom.Porosity.GeomNames      "H1 H2 H3 H4 H5"

pfset Geom.H1.Porosity.Type    	Constant
pfset Geom.H1.Porosity.Value   	0.40

pfset Geom.H2.Porosity.Type    	Constant
pfset Geom.H2.Porosity.Value   	0.40

pfset Geom.H3.Porosity.Type    	Constant
pfset Geom.H3.Porosity.Value   	0.40

pfset Geom.H4.Porosity.Type    	Constant
pfset Geom.H4.Porosity.Value   	0.40

pfset Geom.H5.Porosity.Type    	Constant
pfset Geom.H5.Porosity.Value   	0.30

#-----------------------------------------------------------------------------
# Perm = Ksat ? (m/h)
#-----------------------------------------------------------------------------
# constant
pfset Geom.Perm.Names       "H1 H2 H3 H4 H5"

pfset Geom.H1.Perm.Type            Constant
pfset Geom.H1.Perm.Value           0.000006
#1.10 -7 m/s:


pfset Geom.H2.Perm.Type            Constant
pfset Geom.H2.Perm.Value           0.000066

pfset Geom.H3.Perm.Type            Constant
pfset Geom.H3.Perm.Value           0.00012

pfset Geom.H4.Perm.Type            Constant
pfset Geom.H4.Perm.Value           0.00012

pfset Geom.H5.Perm.Type            Constant
pfset Geom.H5.Perm.Value           0.0036

#pfset Geom.Perm.Names                    "domain"
#pfset Geom.domain.Perm.Type               PFBFile
#pfset Geom.domain.Perm.FileName            Ks.pfb

pfset Perm.TensorType               TensorByGeom

pfset Geom.Perm.TensorByGeom.Names  "domain"

pfset Geom.domain.Perm.TensorValX  1.0
pfset Geom.domain.Perm.TensorValY  1.0
pfset Geom.domain.Perm.TensorValZ  1.0
#~ pfset Geom.domain.Perm.TensorValZ  2.0

#-----------------------------------------------------------------------------
# Specific Storage (/m)???
#-----------------------------------------------------------------------------
# specific storage does not figure into the impes (fully sat) case but we still
# need a key for it

pfset SpecificStorage.Type                 Constant
pfset SpecificStorage.GeomNames            "domain"

pfset Geom.domain.SpecificStorage.Value 1.0e-3

#-----------------------------------------------------------------------------
# Relative Permeability
#-----------------------------------------------------------------------------
pfset Phase.RelPerm.Type               VanGenuchten
pfset Phase.RelPerm.GeomNames          "H1 H2 H3 H4 H5"

pfset Geom.H1.RelPerm.Alpha         3.333
pfset Geom.H1.RelPerm.N             2.2

pfset Geom.H2.RelPerm.Alpha         3.333
pfset Geom.H2.RelPerm.N             2.2

pfset Geom.H3.RelPerm.Alpha         6.667
pfset Geom.H3.RelPerm.N             1.65

pfset Geom.H4.RelPerm.Alpha         6.667
pfset Geom.H4.RelPerm.N             1.65

pfset Geom.H5.RelPerm.Alpha         0.333
pfset Geom.H5.RelPerm.N             2.0

#pfset Phase.RelPerm.GeomNames             "domain"
#pfset Phase.RelPerm.VanGenuchten.File     1
#pfset Geom.domain.RelPerm.Alpha.Filename        alpha.pfb
#pfset Geom.domain.RelPerm.N.Filename            n.pfb

#---------------------------------------------------------
# Saturation
#---------------------------------------------------------
pfset Phase.Saturation.Type              VanGenuchten 
pfset Phase.Saturation.GeomNames         "H1 H2 H3 H4 H5"

pfset Geom.H1.Saturation.Alpha        3.333  
pfset Geom.H1.Saturation.N            2.2
pfset Geom.H1.Saturation.SRes         0.01
pfset Geom.H1.Saturation.SSat         0.9

pfset Geom.H2.Saturation.Alpha        3.333
pfset Geom.H2.Saturation.N            2.2
pfset Geom.H2.Saturation.SRes         0.01
pfset Geom.H2.Saturation.SSat         0.9

pfset Geom.H3.Saturation.Alpha        6.667
pfset Geom.H3.Saturation.N            1.65
pfset Geom.H3.Saturation.SRes         0.1
pfset Geom.H3.Saturation.SSat         0.9

pfset Geom.H4.Saturation.Alpha        6.667
pfset Geom.H4.Saturation.N            1.65
pfset Geom.H4.Saturation.SRes         0.1
pfset Geom.H4.Saturation.SSat         0.9

pfset Geom.H5.Saturation.Alpha        0.333
pfset Geom.H5.Saturation.N            2.0
pfset Geom.H5.Saturation.SRes         0.01
pfset Geom.H5.Saturation.SSat         0.9

#pfset Phase.Saturation.GeomNames            "domain"
#pfset Phase.Saturation.VanGenuchten.File     1
#pfset Geom.domain.Saturation.Alpha.Filename  alpha.pfb
#pfset Geom.domain.Saturation.N.Filename       n.pfb
#pfset Geom.domain.Saturation.SRes.Filename    Sr.pfb
#pfset Geom.domain.Saturation.SSat.Filename    Ssat.pfb


#---------------------------------------------------------
# Mannings coefficient 
#---------------------------------------------------------
pfset Mannings.Type 				"Constant"
pfset Mannings.GeomNames 			"domain"
pfset Mannings.Geom.domain.Value 		5.e-6


#-----------------------------------------------------------------------------
# Wells
#-----------------------------------------------------------------------------
pfset Wells.Names                        ""


#-----------------------------------------------------------------------------2005
# Time Cycles
#-----------------------------------------------------------------------------
#pfset Cycle.Names                       "constant rainrec"
pfset Cycle.Names                       "constant rain"
pfset Cycle.constant.Names              "alltime"
pfset Cycle.constant.alltime.Length      1
pfset Cycle.constant.Repeat             -1

pfset Cycle.rain.Names              "1 2 3 4 5 6 7 8 9 rec"
pfset Cycle.rain.1.Length      5
pfset Cycle.rain.2.Length      5
pfset Cycle.rain.3.Length      5
pfset Cycle.rain.4.Length      5
pfset Cycle.rain.5.Length      5
pfset Cycle.rain.6.Length      5
pfset Cycle.rain.7.Length      5
pfset Cycle.rain.8.Length      5
pfset Cycle.rain.9.Length      5
pfset Cycle.rain.rec.Length    90
pfset Cycle.rain.Repeat        -1


#-----------------------------------------------------------------------------
# Boundary Conditions: Pressure
#-----------------------------------------------------------------------------
pfset BCPressure.PatchNames                   [pfget Geom.domain.Patches]

 ## overland flow boundary condition with very heavy rainfall then recession
pfset Patch.z-upper.BCPressure.Type                   OverlandFlow
#pfset Patch.z-upper.BCPressure.Cycle                  "constant"
#pfset Patch.z-upper.BCPressure.alltime.Value          0.0
pfset Patch.z-upper.BCPressure.Cycle                  "rain"
pfset Patch.z-upper.BCPressure.1.Value	            -0.00425
pfset Patch.z-upper.BCPressure.2.Value	            -0.0085
pfset Patch.z-upper.BCPressure.3.Value	            -0.01275
pfset Patch.z-upper.BCPressure.4.Value	            -0.0085
pfset Patch.z-upper.BCPressure.5.Value	            -0.00425
pfset Patch.z-upper.BCPressure.6.Value	            -0.002
pfset Patch.z-upper.BCPressure.7.Value	            -0.002
pfset Patch.z-upper.BCPressure.8.Value	            -0.002
pfset Patch.z-upper.BCPressure.9.Value	            -0.002
pfset Patch.z-upper.BCPressure.rec.Value	    0.0

#pfset Patch.z-upper.BCPressure.rain.Value	            -0.04
#pfset Patch.z-upper.BCPressure.dry1.Value	            0.00
# pfset Patch.z-upper.BCPressure.dry2.Value	            0.00

pfset Patch.z-lower.BCPressure.Type                   FluxConst
pfset Patch.z-lower.BCPressure.Cycle                  "constant"
pfset Patch.z-lower.BCPressure.alltime.Value          0.0


pfset Patch.x-lower.BCPressure.Type		      FluxConst
pfset Patch.x-lower.BCPressure.Cycle		      "constant"
pfset Patch.x-lower.BCPressure.alltime.Value	      0.0

pfset Patch.y-lower.BCPressure.Type		      FluxConst
pfset Patch.y-lower.BCPressure.Cycle		      "constant"
pfset Patch.y-lower.BCPressure.alltime.Value	      0.0

pfset Patch.x-upper.BCPressure.Type		      FluxConst
pfset Patch.x-upper.BCPressure.Cycle		      "constant"
pfset Patch.x-upper.BCPressure.alltime.Value	      0.0

pfset Patch.y-upper.BCPressure.Type		      FluxConst
pfset Patch.y-upper.BCPressure.Cycle		      "constant"
pfset Patch.y-upper.BCPressure.alltime.Value	      0.0


#---------------------------------------------------------
# Initial conditions: water pressure

#---------------------------------------------------------
pfset ICPressure.Type                                   HydroStaticPatch
pfset ICPressure.GeomNames                              "domain"

pfset Geom.domain.ICPressure.Value                      -10
pfset Geom.domain.ICPressure.RefGeom                    domain
pfset Geom.domain.ICPressure.RefPatch                   z-upper


#for restart:
#pfset ICPressure.Type                                   PFBFile
#pfset ICPressure.GeomNames                              "domain"
#pfset Geom.domain.ICPressure.FileName                   "doro.out.press.00034.pfb"
#-----------------------------------------------------------------------------
# Setup timing info
#-----------------------------------------------------------------------------
pfset TimingInfo.BaseUnit        1
pfset TimingInfo.StartCount      0.0
pfset TimingInfo.StartTime       0.0

pfset TimingInfo.DumpInterval    5
pfset TimeStep.Type              Constant
pfset TimeStep.Value             10

# 5 hours:
pfset TimingInfo.StopTime	 30
# 2 years:
#pfset TimingInfo.StopTime	 35040
# 5 years:
#pfset TimingInfo.StopTime	 43800
# 8 years:
#pfset TimingInfo.StopTime	 70080
#pfset TimingInfo.StopTime	 3600
#pfset TimingInfo.StopTime	 8760

# test 24h00
#pfset TimingInfo.StopTime		48


#-----------------------------------------------------------------------------
# Exact solution specification for error calculations
#-----------------------------------------------------------------------------
pfset KnownSolution                                      NoKnownSolution

#-----------------------------------------------------------------------------
# Set solver parameters
#-----------------------------------------------------------------------------
 
pfset Solver                                             Richards

# pas de temps: 0.5h=1800s, pas de temps min: 0.1s. 1800/2^14=0.1

#This key gives the maximum number of convergence failures allowed.
#Each convergence failure cuts the timestep in half and the solver 
#tries to advance the solution with the reduced timestep.
# should not be higher than 9: time is discretized in terms of the base
#time unit and if the solver begins to take very small timesteps this may
#lead to slightly incorrect times. consider diminishing the timestep if 
#needed.
#When this is reached, ParFlow considers that the problem has been solved!
pfset Solver.MaxConvergenceFailures			 6

#Max number of time steps: maximum simulation duration! Should be large. 
#used to prevent run-away simulations
pfset Solver.MaxIter                                     10000000
# precision machine?
pfset Solver.AbsTol                                      1E-8
# data values greater than the negative of this value and less than the 
# value are treated as 0 and not written.
pfset Solver.Drop                                        1E-20


#max number of convergence avant faute de convergence
pfset Solver.Nonlinear.MaxIter                          200 
#how much the reduction in the non-linear residual should be before 
#non-linear iteration stop. Measured with the l1 (max) norm. In m?
pfset Solver.Nonlinear.ResidualTol                       1e-5
#specifies the tolerance that measures how small the diff. between two 
#consecutive non linear steps can be before nonlinear iteration stops
# (difference entre deux fnorm conscutives?) (tester 10-1)
pfset Solver.Nonlinear.StepTol                           1e-30
#pfset Solver.Nonlinear.StepTol                           1e-7
#The linear system is solved until a relative residual reduction of eta
# is achieved. Linear residual norms are measured in the l2 norm.
#au sein d'une iteration lineaire:
pfset Solver.Nonlinear.EtaChoice                         EtaConstant
pfset Solver.Nonlinear.EtaValue                          0.001
#whether the Jacobian will be used in matrix-vector products or whether a
#matrix-free version of the code will run. Using the Jacobian will most 
#likely decrease the number of nonlinear iterations but require 
#more memory to run.
pfset Solver.Nonlinear.UseJacobian                       True
#This key specifies the value of eps used in approximating the action 
#of the Jacobian on a vector with approximate directional derivatives
# of the nonlinear function. This parameter is only used when the 
# UseJacobian key is False. (def: 10-7...)
pfset Solver.Nonlinear.DerivativeEpsilon                 1e-8

#The choice InexactNewton specifies no global strategy, and the
# choice LineSearch specifies that a line search strategy should be 
#used where the nonlinear step can be lengthened or decreased to satisfy 
#certain criteria
pfset Solver.Nonlinear.Globalization                     LineSearch
#This key specifies the maximum number of vectors to be used in setting up
#the Krylov subspace in the GMRES iterative solver. These vectors are of 
#problem size and it should be noted that large increases in this parameter
#can limit problem sizes. However, increasing this parameter can 
#sometimes help nonlinear solver convergence
pfset Solver.Linear.KrylovDimension                      200
pfset Solver.Linear.MaxRestart                           2
 
#MGSemi specifies a semi-coarsening multigrid algorithm which uses a 
#point relaxation method. The choice SMG specifies a semi-coarsening 
#multigrid algorithm which uses plane relaxations. This method is more 
#robust than MGSemi, but generally requires more memory and compute time. 
#The choice PFMGOctree can be more efficient for problems with large
# numbersof inactive cells

#pfset Solver.Linear.Preconditioner.PCMatrixType          FullJacobian
#pfset Solver.Linear.Preconditioner                       MGSemi
pfset Solver.Linear.Preconditioner                       PFMG
#pfset Solver.Linear.Preconditioner.MGSemi.MaxIter        1
#pfset Solver.Linear.Preconditioner.MGSemi.MaxLevels      10

#pfset Solver.WriteSiloSubsurfData			True
#pfset Solver.WriteSiloPressure 				True
#pfset Solver.WriteSiloSaturation 			True
#pfset Solver.WriteSiloSlopes 			        True
#pfset Solver.WriteSiloMask 			        True
#pfset Solver.WriteSiloMannings 			        True
#pfset Solver.WriteSiloSpecificStorage 			True
#pfset Solver.WriteSiloVelocities 			True
#pfset Solver.WriteSiloEvapTrans 			True
#pfset Solver.WriteSiloEvapTransSum 			True
#pfset Solver.WriteSiloOverlandSum			True
#pfset Solver.WriteSiloDZMultiplier			True
pfset Solver.PrintDZMultiplier				True
pfset Solver.PrintOverlandSum				True
pfset Solver.PrintSlopes				True
pfset Solver.PrintEvapTrans				True
pfset Solver.PrintEvapTransSum				True
pfset Solver.PrintSubsurf                               True

### spinup options:
pfset OverlandFlowSpinUp				0


# just in case this is not overwritten when using clm:
#pfset Patch.z-upper.BCPressure.rain.Value	            0

#pfset Solver.LSM                                         CLM
#pfset Solver.CLM.IstepStart                              1
#pfset Solver.CLM.CLMDumpInterval                         1
# FOR 3D:
#pfset Solver.CLM.MetForcing                              3D
#pfset Solver.CLM.MetFileName                             "Forc"
#pfset Solver.CLM.MetFilePath                             ./FORC
#pfset Solver.CLM.MetFileNT                             	$nt
#FOR 1D:
#pfset Solver.CLM.MetForcing                              1D
#pfset Solver.CLM.MetFileName                             forcagePF.txt.0
#pfset Solver.CLM.MetFilePath                             ./
#pfset Solver.CLM.MetFileNT                             	$nt
#pfset Solver.CLM.ForceVegetation			True
#pfset Solver.CLM.RootZoneNZ				22

#for outputting everything in subfolders (def=True):
#pfset Solver.CLM.BinaryOutDir				False

#for only writing one averaged_over_processor output file/proc (def=false)
#pfset Solver.CLM.Print1dOut				True

#when true: all clm output variables are written as a single file per time step:
# combines the outputs of all clm output variables into a special multilayer pfb
#pfset Solver.CLM.SingleFile				True

#for silo output format (def=False):
#pfset Solver.WriteSiloCLM                                True
#for pfb output format (def=False):
#pfset Solver.PrintCLM					True
#for binary output format (def=True):
#pfset Solver.WriteCLMBinary				False
# in What directory all outputs (clm) are written):
#pfset Solver.CLM.CLMFileDir				"./clm_out/"

#for writting log files per processor (def=True):
#pfset Solver.CLM.WriteLogs				False
#for writing daily restart files (def) or at every time step
#when false
#pfset Solver.CLM.DailyRST					False

#when true: only one file is written/overwritten (def=False)
#pfset Solver.CLM.WriteLastRST					True

#temp (test):
#pfset Solver.CLM.EvapBeta				"none"
#pfset Solver.CLM.ResSat					0.1

#minimal outputs (nothing from clm, basics from parflow:
#pfset Solver.CLM.BinaryOutDir				False
#pfset Solver.WriteCLMBinary				False
#pfset Solver.CLM.WriteLogs				True
#pfset Solver.CLM.WriteLastRST					True
puts {before run OK}
set time_deb [clock seconds]
puts [time {
pfrun tempsim
}]
set time_fin [clock seconds]
puts [expr $time_fin - $time_deb] 
puts {second}

pfundist tempsim
pfundist Xslope.pfb
pfundist Yslope.pfb
pfundist var_dz.pfb
#FOR 1D:
#pfundist veg_map.pfb
#pfundist Ssat.pfb
#pfundist Ks.pfb
#pfundist Sr.pfb
#pfundist n.pfb
#pfundist alpha.pfb
#pfundist var_dz.pfb

set mask [pfload tempsim.out.mask.pfb]
set top [Parflow::pfcomputetop $mask]
pfsave $top -silo "top_mask.silo"
pfsave $top -pfb "top_mask.pfb"

cd ..
