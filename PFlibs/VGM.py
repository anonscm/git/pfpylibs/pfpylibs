#-*- coding: utf-8 -*-
"""
    PFPYLIBS - VGM

    Library for handling and plotting retention parameters & curve
    
    Follows: 
    van Genuchten, M. T.: A Closed-form Equation for Predicting the 
    Hydraulic Conductivity of Unsaturated Soils1, Soil Science Society 
    of America Journal, 44(5), 892, 
    doi:10.2136/sssaj1980.03615995004400050002x, 1980.

    With the formalism of ParFlow:
    Maxwell, R. M., Kollet, S. J., Smith, S. G., Woodward, C. S., 
    Falgout, R. D., Ferguson, I. M., Engdahl, N., Condon, L. E., 
    Hector, B., Lopez, S. R., Gilbert, J., Bearup, L., Jefferson, J., 
    Collins, C., de Graaf, I. E. M., Prubilick, C., Baldwin, C., Bosl, 
    W. J., Hornung, R. and Ashby, S.: ParFlow User’s Manual, 
    Integrated GroundWater Modeling Center Report GWMI., 2016.

    The code includes utilities to read in retention parameters from 
    a ParFlow configuration file (.tcl) and a given geometry, or all 
    geometries, assuming the parameters are hard wired in the .tcl.
    (note that this supposes to load the library io_tcl.py)


    Exemple Usage:
    from PFlibs import VGM                                                                                                                
    exemple_filename = '/home/hectorb/PARFLOW/PROJECTS/Oueme/Oueme_sup2018/output/oueme_results_ZA_H236_CI6_5yrs_2018_09_13/simu.tcl'

    ### plot all retention curves from each geometry coded in a tcl:
    v = VGM.VGMps()
    v=v.populate_from_tcl(exemple_filename)
    v.plot()

    ### plot a single retention curve from a given geometry coded in a tcl:
    v = VGM.VGMp()
    v=v.populate_from_a_tcl_and_geometry_name(exemple_filename,'H1')
    v.plot()

    ### plot a single retention curve from a given set of parameters:
    v = VGM.VGMp()
    v.set_p()
    # OR:
    v['Poro']=0.0
    v['Sr']=0.0
    v['Ssat']=0.0
    v['alpha']=0.0
    v['n']=0.0
    v['m']=0.0
    v['Ks']=0.0
    v.plot()

    ### compare 2 single retention curves from a given set of parameters
    v1 = VGM.VGMp(name = 'set1')
    v1.set_p()
    v2 = VGM.VGMp(name = 'set2')
    v2.set_p()
    V = VGM.VGMps()
    V[v1.name] = v1
    V[v2.name] = v2
    V.plot()

    ### compare 2 single retention curves from two sets of parameters from one tcl and several geom or several tcl:
    v1 = VGM.VGMp(name = 'set1')
    v1=v1.populate_from_a_tcl_and_geometry_name(exemple_filename,'H1')
    v2 = VGM.VGMp(name = 'set2')
    v2=v2.populate_from_a_tcl_and_geometry_name(exemple_filename,'H2')
    V = VGM.VGMps()
    V[v1.name] = v1
    V[v2.name] = v2
    V.plot()


    @copyright: 2018 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2018"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import copy
import matplotlib.pyplot as plt

"""local imports:"""

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##


##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


def read_vals_from_tcl(tcl_filename,pat):
    """
    find the parameters associated to specific geometries in the tcl
    return a dictionnary of VGMp objects
    """
    from PFlibs import io_tcl
    
    tcllines = io_tcl.TclLines(filename = tcl_filename)
    return tcllines.get_keyval_matching_pattern(pat)
    

def calc_retention(h,thr,ths,hg,n):
    """
    """
    return thr+(ths-thr)*(1+(h/hg)**n)**(-(1-1/n))
    
    
def calc_cond(h,Ks,alpha,n):
    """
    """
    m=1-1/n
    return Ks*((1-((h*alpha)**(n-1))/((1+(h*alpha)**n)**m))**2)/((1+(h*alpha)**n)**(m/2))
    
##======================================================================================================================##
##                CLASSES                                                                                               ##
##======================================================================================================================##

class VGMp(dict):
    """ Class doc """
    
    def __init__ (self,name = None):
        """ Class initialiser """
        self.name = name
        self['Poro']=0.0
        self['Sr']=0.0
        self['Ssat']=0.0
        self['alpha']=0.0
        self['n']=0.0
        self['m']=0.0
        self['Ks']=0.5
        pass

    def set_p(self):
        """ set parameters from user input from the screen"""
        self['Poro']=float(input("Poro = "))
        self['Sr']=float(input("Sr = "))
        self['Ssat']=float(input("Ssat = "))
        self['alpha']=float(input("alpha = "))
        self['n']=float(input("n = "))
        self['m']=1-1/self['n']
        self['Ks']=float(input("Ks = "))

    def __str__(self):
        """ replace the print function inherited from the dictionnary"""
        print('Poro:	%1.2f'%self['Poro'])
        print('Sr:	%1.2f'%self['Sr'])
        print('Ssat:	%1.2f'%self['Ssat'])
        print('alpha:	%1.2f'%self['alpha'])
        print('n:	%1.2f'%self['n'])
        print('Ks:	%1.2f'%self['Ks'])
        return '\n'
    

	
    def populate_from_a_tcl_and_geometry_name(self,tcl_filename,geom='H1'):
        """
        """
        res=copy.copy(self)
        res['Poro'] = float(list(read_vals_from_tcl(tcl_filename,pat='Geom.%s.Porosity.Value'%geom).values())[0])
        res['Sr'] = float(list(read_vals_from_tcl(tcl_filename,pat='Geom.%s.Saturation.SRes'%geom).values())[0])
        res['Ssat'] = float(list(read_vals_from_tcl(tcl_filename,pat='Geom.%s.Saturation.SSat'%geom).values())[0])
        res['alpha'] = float(list(read_vals_from_tcl(tcl_filename,pat='Geom.%s.Saturation.Alpha'%geom).values())[0])
        res['n'] = float(list(read_vals_from_tcl(tcl_filename,pat='Geom.%s.Saturation.N'%geom).values())[0])
        res['Ks'] = float(list(read_vals_from_tcl(tcl_filename,pat='Geom.%s.Perm.Value'%geom).values())[0])
        #~ res['Ks'] = read_vals_from_tcl(tcl_filename,pat='Geom.%s.Perm.value'%tmp.name)
        return res  
        
    
    def plot(self):
        """plot a single retention curve"""
        
        
        h=np.concatenate([np.arange(0.001,0.049,0.001),np.arange(0.05,1,0.01),np.arange(1.1,500,0.1)])        
        th = calc_retention(h,thr = self['Poro']*self['Sr'],ths= self['Poro']*self['Ssat'],hg= 1/self['alpha'],n = self['n'])
        
        fig, ax = plt.subplots(1, 1,figsize=(10,10))
        ax.semilogy(th,h)
        ax.set_xlim([0,0.45])
        ax.set_ylim([0.01,1000])
        #~ ax.legend((linesobs,linessim),['obs.','sim.'],loc='lower left',fontsize=28,ncol=1)
        ax.set_ylabel('|h| (m)', fontsize=20, color = 'k')
        ax.set_xlabel(r'$\theta$', fontsize=20, color = 'k')
        ax.tick_params(axis='both', which='major', labelsize=20)

        plt.show()
        return ax
        
    def plot_hth_Kh(self):
        """plot a single retention curve and a K(h) curve"""
        
        
        h=np.concatenate([np.arange(0.001,0.049,0.001),np.arange(0.05,1,0.01),np.arange(1.1,500,0.1)])        
        th = calc_retention(h,thr = self['Poro']*self['Sr'],ths= self['Poro']*self['Ssat'],hg = 1/self['alpha'],n = self['n'])
        K = calc_cond(h,self['Ks'],alpha=self['alpha'],n = self['n'])
        fig, ax = plt.subplots(1, 2,figsize=(20,10))
        ax[0].semilogy(th,h)
        ax[0].set_xlim([0,0.45])
        ax[0].set_ylim([0.01,1000])
        ax[0].set_ylabel('|h| (m)', fontsize=20, color = 'k')
        ax[0].set_xlabel(r'$\theta$', fontsize=20, color = 'k')
        ax[0].tick_params(axis='both', which='major', labelsize=20)
        
        ax[1].semilogy(th,K)
        ax[1].set_ylim([0.00001,10])
        ax[1].set_xlim([0,0.45])
        ax[1].set_ylabel('K', fontsize=20, color = 'k')
        ax[1].set_xlabel(r'$\theta$', fontsize=20, color = 'k')
        ax[1].tick_params(axis='both', which='major', labelsize=20)

        plt.show()
        return ax        

class VGMps(dict):
    """ A dictionnary of VGM objects """
    
    def __init__ (self):
        """ Class initialiser """
        pass
            

    def populate_from_tcl(self,tcl_filename):
        """read a tcl, get the unique domains and populate the VGMps obj
        Works in the case of constant VGM parameters type"""
        res = copy.copy(self)
        domains = read_vals_from_tcl(tcl_filename,pat='Geom.*.Porosity.Value')
        for key,val in domains.items():
            tmp = VGMp(name = key.split('.')[1])
            tmp = tmp.populate_from_a_tcl_and_geometry_name(tcl_filename,geom=tmp.name)
            res[tmp.name]=tmp
        return res

    def plot(self):
        """plot all retention curve"""
        
        h=np.concatenate([np.arange(0.001,0.049,0.001),np.arange(0.05,1,0.01),np.arange(1.1,500,0.1)])        
        
        fig, ax = plt.subplots(1, 1,figsize=(10,10))
        l=[]
        names=[]
        for key,val in self.items():
            th = calc_retention(h,thr = val['Poro']*val['Sr'],ths= val['Poro']*val['Ssat'],hg= 1/val['alpha'],n = val['n'])
            tmp, = ax.semilogy(th,h)
            l.append(tmp)
            names.append(key)
            
        
        ax.set_xlim([0,0.45])
        ax.set_ylim([0.01,1000])
        ax.legend(l,names,loc='upper right',fontsize=20,ncol=1)
        ax.set_ylabel('|h| (m)', fontsize=20, color = 'k')
        ax.set_xlabel(r'$\theta$', fontsize=20, color = 'k')
        ax.tick_params(axis='both', which='major', labelsize=20)

        plt.show()
        return ax

    def plot_hth_Kh(self):
        """plot all retention curve and K(theta) curves"""
        
        h=np.concatenate([np.arange(0.001,0.049,0.001),np.arange(0.05,1,0.01),np.arange(1.1,500,0.1)])        
        
        fig, ax = plt.subplots(1, 2,figsize=(20,10))
        l1=[]
        l2=[]
        names=[]
        for key,val in self.items():
            th = calc_retention(h,thr = val['Poro']*val['Sr'],ths= val['Poro']*val['Ssat'],hg= 1/val['alpha'],n = val['n'])
            K = calc_cond(h,val['Ks'],alpha=val['alpha'],n = val['n'])
            tmp1, = ax[0].semilogy(th,h)
            tmp2, = ax[1].semilogy(th,K)
            l1.append(tmp1)
            l2.append(tmp2)
            names.append(key)
            
        
        ax[0].set_xlim([0,0.45])
        ax[0].set_ylim([0.01,1000])
        ax[0].legend(l1,names,loc='upper right',fontsize=20,ncol=1)
        ax[0].set_ylabel('|h| (m)', fontsize=20, color = 'k')
        ax[0].set_xlabel(r'$\theta$', fontsize=20, color = 'k')
        ax[0].tick_params(axis='both', which='major', labelsize=20)
        
        ax[1].set_ylim([0.00001,10])
        ax[1].set_xlim([0,0.45])
        ax[1].legend(l2,names,loc='upper right',fontsize=20,ncol=1)
        ax[1].set_ylabel('K', fontsize=20, color = 'k')
        ax[1].set_xlabel(r'$\theta$', fontsize=20, color = 'k')
        ax[1].tick_params(axis='both', which='major', labelsize=20)


        plt.show()
        return ax

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

