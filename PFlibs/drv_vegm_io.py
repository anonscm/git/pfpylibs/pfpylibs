#-*- coding: utf-8 -*-
"""
	PFPREPROC - DRV_VEGM

	TODO :write_vegm
	
	handling drv_vgm files
	io functions for CLM drv_vegm files:
	drv_vegm files are (x,y,veg_ind) triplets where veg_ind is vegetation type following:
	% !IGBP Land Cover Types (other classes can be used by changing this file)
	% !  1 doro - woody savannas
	% !  2 evergreen broadleaf forests
	% !  3 deciduous needleleaf forests
	% !  4 deciduous broadleaf forests
	% !  5 mixed forests
	% !  6 closed shrublands
	% !  7 open shrublands
	% !  8 woody savannas
	% !  9 svannas
	% ! 10 grasslands
	% ! 11 permanent wetlands
	% ! 12 croplands
	% ! 13 urban and built-up lands
	% ! 14 cropland / natural vegetation mosaics
	% ! 15 snow and ice
	% ! 16 barren or sparsely vegetated
	% ! 17 water bodies
	% ! 18 bare soil
	% ! 19 doro - herbacé
	
	format is latitude by latitude from N to S in one latitude, the data points
	are arranged from west to east, starting from 0° long
	X, Y must be indices of values (hence integers)

	header:
	fprintf(fid,' x  y  lat    lon    sand clay color  fractional coverage of grid by vegetation class (Must/Should Add to 1.0)\n');
	fprintf(fid,'       (Deg) (Deg)  (prct/100)   index  1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18   19\n');


	@copyright: 2018 by Basile HECTOR <basile.hector@ird.fr>
	@license: GNU GPL, see COPYING for details.
	
	
"""

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import os


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

inputdrvvegm='/home/hectorb/PARFLOW/PROJECTS/Vshape/Vshape_exp_20m/input/drv_vegm_vshape.dat'
outdir='./test/tests_io/'
vegclasses={1:'evergreen needleleaf forests',2:'evergreen broadleaf forests',\
			3:'deciduous needleleaf forests', 4: 'deciduous broadleaf forests',\
			5: 'mixed forests6 closed shrublands', 6:'closed shrublands',\
			7: 'open shrublands', 8: 'woody savannas',\
			9: 'savannas', 10: 'grasslands',\
			11: 'permanent wetlands', 12: 'croplands',\
			13: 'urban and built-up lands', 14: 'cropland / natural vegetation mosaics',\
			15: 'snow and ice', 16: 'barren or sparsely vegetated',\
			17: 'water bodies', 18: 'bare soil',\
			19: 'herbaceous'}	

hdr={'X0': 0			,'Y0':0				,'Z0':0				,\
		'nx':10				,'ny':10			,'nz':1 			,\
		'dx':10				,'dy':10			,'dz':10			,\
		'ns':1,\
		'ix':10				,'iy':10			,'iz':10			,\
		'rx':10				,'ry':10			,'rz':10			}
##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


def read_vegm(filename, plot=False,cmap = plt.cm.jet):
	"""
	simple function to read a drv_vegm - like file
	"""
	with open(filename, "r") as f:
		reads = f.readlines()

	print(reads[0])
	print(reads[1])
	reads[0:2] = []
	x=np.array([])
	y=np.array([])
	veg=np.array([])
	for r in reads:
		curr_line=np.array([float(x) for x in r.strip().split()])
		x=np.append(x,curr_line[0])
		y=np.append(y,curr_line[1])
		veg=np.append(veg,np.argmax(curr_line[7::])+1)
	if plot:
		fig = plt.figure(figsize=(12,12))
		ax = fig.add_subplot(111)
		ax.set_aspect(1)
		#colormap for regolith thickness
		cmaplist = [cmap(i) for i in range(cmap.N)] 				# extract all colors from the .jet map
		cmaplist[0] = (.5,.5,.5,1.0) 								# force the first color entry to be grey
		cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N) 	# create the new map
		bounds = np.arange(0,18,1) 									# define the bins and normalize
		norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
		p = ax.scatter(x,y,c=veg,cmap=cmap, norm=norm)
		cb = plt.colorbar(p,orientation='vertical')
		cb.set_label('vegetation class',fontsize=14)
		cb.set_ticks(np.arange(1,20,1))
		cb.set_ticklabels([vegclasses[tk] for tk in cb.get_ticks()])
		plt.show()
		
	return x,y, veg
	
	
def veg_to_vegveg(x,y,veg):
	"""
	based on 1D vectors for x,y and veg, as obtained from read_vegm
	try to meshgrid it assuming all values of the domain are listed and following
	PF rules (see header of this script)
	"""
	X=np.sort(np.unique(x))
	Y=np.sort(np.unique(y))
	[XX,YY] = np.meshgrid(X,Y)
	VEGVEG = np.zeros(np.shape(XX))
	for ind in range(len(veg)):
		ix = ind % len(X)
		iy = len(Y) -1 - (ind // len(X))
		VEGVEG[iy,ix] = veg[ind]
	return XX, YY, VEGVEG

def vegveg_to_veg(XX,YY,VEGVEG):
	"""
	from a meshgridded XX,YY and VEGVEG, produce 1D vectors (x,y,veg)
	to be printed as drv_vegm
	"""
	N=np.prod(np.shape(XX))
	x=np.zeros(N)
	y=np.zeros(N)
	veg=np.zeros(N)
	for ind in range(N):
		ix = ind % np.shape(XX)[1]
		iy = ind // np.shape(XX)[1]
		x[ind] = XX[iy,ix]
		y[ind] = YY[iy,ix]
		veg[ind] = VEGVEG[iy,ix]
	return x,y,veg

def write_vegm(x,y,veg,filename):
	"""
	simple function to write a drv_vegm file
	
	np.array([(i,j) for i,j in zip(x, y)])
	"""
	tmp=np.zeros([1,19+7])
	datas=np.zeros([len(x),19+7])
	for i in range(len(x)):
		tmp[0][0] = np.int(x[i])
		tmp[0][1] = np.int(y[i])
		tmp[0][2] = 9.74
		tmp[0][3] = 1.6
		tmp[0][4] = 0.58
		tmp[0][5] = 0.27
		tmp[0][6] = 6
		tmp[0][6+np.int(veg[i])] = 1
		datas[i]=tmp
		tmp=np.zeros([1,19+7])
	
	#~ texts = '\n'.join([' '.join(['%1.2f'%x for x in d]) for d in datas])
	texts = '\n'.join([' '.join([' '.join(['%i'%x for x in d[0:2]]),' '.join(['%1.2f'%x for x in d[2:6]]),' '.join(['%i'%x for x in d[6:19+7]])]) for d in datas])
	with open(filename, "w") as fw:
		fw.writelines('x  y  lat    lon    sand clay color  fractional coverage of grid by vegetation class (Must/Should Add to 1.0\n')
		fw.writelines('       (Deg) (Deg)  (prct/100)   index  1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18   19\n')
		fw.writelines(texts)
		#to be checked:(13/09/2018)
		fw.writelines('\n')
		

	
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
	
	"""
	example of applying those functions
	"""
	
	[x,y,veg]=read_vegm(inputdrvvegm,plot=False)
	#~ [x,y,veg]=read_vegm(inputdrvvegm,plot=True)
	[XX,YY,vegveg]=veg_to_vegveg(x,y,veg)
	[x2,y2,veg2]=vegveg_to_veg(XX,YY,vegveg)
	write_vegm(x2,y2,veg2,os.sep.join([outdir,'test.vegm.dat']))
	#~ [x3,y3,veg3]=read_vegm(os.sep.join([outdir,'test.vegm.dat']),plot=True)
	[x3,y3,veg3]=read_vegm(os.sep.join([outdir,'test.vegm.dat']),plot=False)
	
	
	""" If pfb writing is needed:"""
	hdr['nx'] = np.shape(XX)[1]
	hdr['ny'] = np.shape(XX)[0]
	from PFlibs import io_pfb
	io_pfb.PFB_write(np.swapaxes(vegveg,0,1), hdr, pfb_fp=os.sep.join([outdir,'veg_map.pfb']))
	

	#~ fig = plt.figure(figsize=(12,12))
	#~ ax = fig.add_subplot(111)
	#~ ax.set_aspect(1)
	#~ #colormap for regolith thickness
	#~ cmap = plt.cm.jet
	#~ cmaplist = [cmap(i) for i in range(cmap.N)] 				# extract all colors from the .jet map
	#~ cmaplist[0] = (.5,.5,.5,1.0) 								# force the first color entry to be grey
	#~ cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N) 	# create the new map
	#~ bounds = np.arange(0,18,1) 									# define the bins and normalize
	#~ norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

	#~ p = ax.pcolormesh(XX, YY, vegveg, cmap=cmap, norm=norm, zorder=-1)
	#~ p2 = ax.scatter(x,y,s=1,c=veg,cmap=cmap, norm=norm)
	#~ cb = plt.colorbar(p,orientation='vertical')
	#~ cb.set_label('vegetation class',fontsize=14)
	#~ cb.set_ticks(np.arange(1,20,1))
	#~ cb.set_ticklabels([vegclasses[tk] for tk in cb.get_ticks()])
	#~ plt.show()



