"""
Read and write tcl files for parflow
"""
__author__     = "PHYREV team"
__copyright__  = "Copyright 2017"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import os, glob, shutil 
import argparse
import doctest
#~ from pandas import Series
#~ from pandas import DataFrame
#~ from pandas import concat
import pandas
import subprocess

#only for get_PF_grid_from_Gtiff(filename) :
from osgeo import gdal 
#

from copy import deepcopy
import numpy as np

from PFlibs import io_tcl
from PFlibs import io_pfb
from PFlibs.config import testfiles

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

_tcl_name = "simu.tcl"

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def get_PF_grid_from_Gtiff(filename):
    """get the grid coordinates from a gtiff
    according to AD
    then if meshgrid
    xx, yy = np.meshgrid(vecx, vecy[::-1]) 
    """
    ds = gdal.Open(filename)
    ny = ds.RasterYSize
    nx = ds.RasterXSize
    b = ds.GetGeoTransform() #bbox, interval
    vecx = np.arange(nx)*b[1]+b[0]
    vecy = np.arange(ny)*b[5]+b[3]
    hdr={}
    hdr['X0'] = b[0]
    hdr['Y0'] = b[3]
    hdr['nx'] = nx
    hdr['ny'] = ny
    hdr['dx'] = b[1]
    hdr['dy'] = b[5]
    return hdr,vecx,vecy
##======================================================================================================================##
##                CLASSES                                                                                               ##
##======================================================================================================================##


class Simu(object):
    """ Class doc
    One simulation: 
    
    properties: 
    - rundir: 			Simulation directory
    - tcl_generic:  	the generic tcl that will be modified
                        An instance of a TclLines object (see io_tcl.py)
    - tcl_new:          the modified tcl
                        An instance of a TclLines object (see io_tcl.py)
    - tcl_new_name:     the name of the new tcl (without its path)
    - keyval_tochange   key values of the tcl to change
    
    """
    def __init__(self, rundir, tcl_ref = None):
        """ Class initialiser """
        #~ assert os.path.isdir(rundir), "Exp directory, got {0}".format(rundir)
        #~ assert tcl_ref is None or os.path.isfile(tcl_ref), "Exp file, got {0}".format(tcl_ref)
        #~ assert tcl_new is None or os.path.isdir(os.path.dirname(tcl_new))
        self.rundir = rundir
        self.tcl_ref = tcl_ref #io_tcl.TclLines()
        #~ self.tcl_new = io_tcl.TclLines()
        self.tcl_new = self._tcl_new()
        #~ self.keyval_tochange = {}
        
    def __str__(self):
        """Object representation"""
        return self.rundir
        
    def _tcl_new(self):
        """
        Function doc
        >>> 'afarie'
        """
        res = os.path.join(self.rundir, _tcl_name)
        return res
        
        
    def create_rundir(self):
        """
        Function doc
        >>> 'afarie'
        """
        print(self.rundir)
        if os.path.isdir(self.rundir):
            print("Rundir exists")
        else:
            os.mkdir(self.rundir)
            
        return None
        
    #~ def check_tcl_new(self):
        #~ """
        #~ Function doc
        #~ >>> 'afarie'
        #~ """
        #~ assert os.path.samefile(os.path.dirname(self.tcl_new), self.rundir)
        #~ return None
        
    def get_ref_dir(self):
        """
        Function doc
        >>> 'afarie'
        """
        res = os.path.dirname(self.tcl_ref)
        # check if inputdir exist:
        assert os.path.isdir(res), "Input dir. is not a valid path\n{0}".format(res)
        return res
        
    def get_tcllines(self, which='ref'):
        """
        Function doc
        >>> 'afarie'
        """
        if which == 'ref':
            path = self.tcl_ref
        elif which == "new":
            path = self.tcl_new
        else:
            raise ValueError("Exp 'ref' or 'new' got{0}".format(which))
            
        res = io_tcl.TclLines(filename = path)
        return res
        
    def create_new_tcl(self, **kwargs):
        """
        Function doc
        >>> 'afarie'
        """
        self.create_rundir()
        #~ self.check_tcl_new()
        
        tcl_ref = self.get_tcllines(which='ref')
        res = tcl_ref.change_param(**kwargs)
        res.write(filename = self.tcl_new)
        return res
        
        
    def cp_inputfiles(self, *args):
        """ Copy input files from a given dir to rundir """
        ##### TO BE UPDATED !
        if len(args) == 0:
            patterns =  ("*.pfb", "*.pfsol","*.dat","*.dat.0","*.txt","*.txt.0")
        else:
            patterns = args
        
        inputdir = self.get_ref_dir()
        
        files = []
        for pat in patterns:
            files += glob.glob(os.path.join(inputdir, pat))
            
        for ifile in files:
            assert os.path.isfile(ifile), "Dont know what to do with {0}".format(ifile)
            shutil.copy2(ifile, self.rundir)
                
        #~ self.tcl_new_name = new_tcl_name        
        #~ self.tcl_new.write(filename = '/'.join([self.rundir,self.tcl_new_name]))
        
        return None
        
    def scan_rundir(self):
        """
        Function doc
        >>> 'afarie'
        """
        res = glob.glob(os.path.join(self.rundir, "*"))
        return res
        
    def run(self):
        """run PF """
        
        #get current dir:
        curr_dir =  os.getcwd()
        os.chdir(self.rundir)
        print(" ".join(["run the command: tclsh",self.tcl_new]))
        
        p=subprocess.Popen(["tclsh", self.tcl_new], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        for line in p.stdout:
            print(line)
            
        p.wait()
        
        if p.returncode != 0:
            print("Non zero exit status")
            for line in p.stderr:
                print(line)
        
        os.chdir(curr_dir)
        
    def new(self, rundir = None, tcl_ref = None):
        """
        Function doc
        >>> 'afarie'
        """
        assert rundir is not None or tcl_ref is not None
        
class Simus(pandas.DataFrame):
#~ class Simus(DataFrame):
    """ Class doc
    A set of simulations
    
    properties:
    - simudic              a dictionnary of Simu object: keys are number
                           values are Simu objects
    - tcl_generic:  	the generic tcl that will be modified
                        An instance of a TclLines object (see io_tcl.py)
    
    """
    def __init__(self, *args, **kwargs):
        """ Class initialiser """
        #~ if "rundirs" in kwargs:
            #~ rundirs = kwargs.pop("rundirs")
            
            #~ pandas.DataFrame.__init__(self, *args, **kwargs)
        
        #~ else:
        pandas.DataFrame.__init__(self, *args, **kwargs)
        #~ DataFrame.__init__(self, *args, **kwargs)
        
    #~ def __str__(self):
        #~ """Object representation"""
        
        
    @property
    def _constructor(self):
        """For returning Simus instead of DataFrame"""
        return Simus
        
    def add_simu(self, rundir, **kwargs):
        """
        Function doc
        >>> 'afarie'
        """
        # typically: value (e.g. 1e-5), index(row) = rundir, columns = keys (e.g. Geom.H3.Perm.Value) 
        new = pandas.DataFrame(kwargs, index = [rundir], columns = sorted(kwargs.keys()))
        #~ new = DataFrame(kwargs, index = [rundir], columns = sorted(kwargs.keys()))
        #~ res = concat([self, new])
        res = pandas.concat([self, new])
        res = Simus(res)
        return res
        
    def simu_series(self, tcl_ref = None):
        """
        Function doc
        Create a pandaSeries object where index (rows) are rundirs and values are Simu objects (with a rundir and a tcl_ref)
        >>> 'afarie'
        """
        res = {rdir : Simu(rundir = rdir, tcl_ref = tcl_ref) for rdir in self.index}
        res = pandas.Series(res)
        #~ res = Series(res)
        return res
        
        
    def create_rundirs(self):
        """
        Function doc
        Get each simu as simu object and apply the create_rundir method
        >>> 'afarie'
        """
        res = self.simu_series().apply(lambda x : x.create_rundir())
        return res
        
    def create_new_tcls(self, tcl_ref):
        """
        Function doc
        >>> 'afarie'
        """
        ts = self.simu_series(tcl_ref = tcl_ref)
        for irow in self.index:
            simu = ts.loc[irow]
            dic = self.loc[irow].dropna()
            dic = dict(dic)
            simu.create_new_tcl(**dic)

        return None
        
    def cp_allinputfiles(self, tcl_ref = None):
        """
        Function doc
        Current version reads the reference dir from the tcl_ref name.
        Maybe another option to specify a specifc dir would be more flexible to allow
        different tcl / dir locations   
        >>> 'afarie'
        """
        res = self.simu_series(tcl_ref = tcl_ref).apply(lambda x : x.cp_inputfiles())
        return res

    
    def run_simulations(self):
        """
        Function doc
        >>> 'afarie'
        """
        res = self.simu_series().apply(lambda x : x.run())
        return res
        
        
    def populate_simus_from_patterns(self,folder_pattern, tcl_ref, tcl_pattern = '*.tcl'):
        """
        Function doc
        One method to read simulation outputs by specififying a folder pattern and a tcl file pattern
        
        >>> 'afarie'
        """ 
        simus_out=Simus()
        ref = io_tcl.TclLines(filename = tcl_ref).to_series()
        
        for dirname in glob.glob(folder_pattern):
            if len(glob.glob(os.path.join(dirname,tcl_pattern)))>1:
                print('more than one tcl in  %s: take the first one...'%dirname)
                
            curr = io_tcl.TclLines(filename = glob.glob(os.path.join(dirname,tcl_pattern))[0]).to_series()    
            #~ dic = {key: float(val) for key, val in curr[curr != ref].to_dict().items()}                
            dic = curr[curr != ref].to_dict()
            simus_out = simus_out.add_simu(dirname, **dic)            
        return simus_out
                    
            
        
        
    def populate_SimuDic(self,gen_run_dir,key_tochange,samplingtype='uniform',sample_param1=0,sample_param2=1,sample_N=4):
        """
        TO BE UPDATED
        populate the simudic properties: create as many Simu object as sample numbers 
        (sample_N), and set their new value relative to the given tcl
        key_tochange, according to a sampling method defined by its type
        and several parameters (currently only 2 param, for the uniform
        distribution)
        """
        
        
        
        if samplingtype == 'uniform':
            tmp=np.random.uniform(low=sample_param1,high = sample_param2, size = sample_N)
            
        for i in range(sample_N):
            tmpdir= ''.join([gen_run_dir,'_%d'%i])
            print(tmpdir)
            #Create a Simu object:
            self.simudic[i]=Simu(rundir = tmpdir)            
            # set the keyval to change for the current Simu obj:
            self.simudic[i].keyval_tochange={key_tochange:tmp[i]}
            

    def create_tcls(self,inputdir=None,tclfile=None,new_tcl_name='testtemp.tcl'):
        """
        create the tcl + dir for each simulation
        according to a root tcl
        """

        # check if inputdir exist:
        assert os.path.isdir(inputdir), "Input dir. is not a valid path\n{0}".format(inputdir)
        
        # get the tcl      
        self.tcl_ref = io_tcl.TclLines(filename = tclfile)
        set_lines = self.tcl_ref.is_setline()
        set_lines = self.tcl_ref.get_setlines()
        split_lines = self.tcl_ref.to_tclsets()
        
        # cp currently any PF-like files in this folder
        ##### TO BE UPDATED !
        files = glob.glob(os.path.join(inputdir, "*.pfb"))
        files = files +glob.glob(os.path.join(inputdir, "*.pfsol"))
        
        for key, val in self.simudic.items():
            self.simudic[key].tcl_generic=deepcopy(self.tcl_ref)            
            self.simudic[key].tcl_new=deepcopy(self.tcl_ref)
            # change key values:
            for tclkey, tclval in self.simudic[key].keyval_tochange.items():
                #beware: new.change_param(key,val) only is not enough
                self.simudic[key].tcl_new=self.simudic[key].tcl_new.change_param(tclkey,tclval)
        
            # check if rundir exist, if not: make it
            if os.path.isdir(self.simudic[key].rundir):
                print('rundir exist...')
            else:
                os.mkdir(self.simudic[key].rundir)
            
            # cp currently any PF-like files in this folder
            ##### TO BE UPDATED !
            for file in files:  
                if os.path.isfile(file):
                    shutil.copy2(file, self.simudic[key].rundir)
                    
            self.simudic[key].tcl_new_name = new_tcl_name        
            self.simudic[key].tcl_new.write(filename = '/'.join([self.simudic[key].rundir,self.simudic[key].tcl_new_name]))        
        
        
        def run_simus(self):
            """
            run all simulations from the simdic
            """
            for key, val in self.simudic.items():
                val.run()

        def workinprogress():
            """
            Function doc
            >>> 'afarie'
            """
            #~ df = ts.apply(lambda x : x.get_tcllines('new').to_series())
            #~ df
            #~ df.columns
            #~ for col in df.columns:df[col]
            #~ df[col]
            #~ s=df[col]
            #~ s.value_counts()
            #~ int(s.value_counts())
            #~ df
            #~ df.apply?
            #~ df.apply(numpy.sum, axis = 0)
            #~ df.apply(lambda x : x.value_counts(), axis = 0)
            #~ df
            #~ df.apply(lambda x : x.sum(), axis = 0)
            #~ df.apply(lambda x : set(x.values), axis = 0)
            #~ df.apply(lambda x : len(set(x.values)), axis = 0)
            #~ df.apply(lambda x : len(set(x.values)), axis = 0)
            #~ set(['r', 'r','t'])
            #~ df.apply(lambda x : len(set(x.values)>1), axis = 0)
            #~ df.apply(lambda x : len(set(x.values)), axis = 0) > 1
            #~ idx = df.apply(lambda x : len(set(x.values)), axis = 0) > 1
            #~ df.loc[idx]
            #~ df[idx]
            #~ idx
            #~ idx = df.apply(lambda x : len(set(x.values)), axis = 0) > 1
            #~ idx[idx]
            #~ idx = df.apply(lambda x : len(set(x.values)), axis = 0) > 1
        
            
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    ARGPARSE                   #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default = False, action = 'store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    _dir = os.path.dirname(__file__)
    testdir = os.path.join(_dir,'test/simtest/')
    tclfile = '/'.join([testdir,'test.tcl'])
    simunames = [os.path.join(testdir, "sample__{0}".format(i)) for i in range(5)]
    
    simus = Simus()
    
    ### parameters sampling:
    #~ sims.populate_SimuDic(gen_run_dir='./test/simtest/sample',
    #~ key_tochange='OverlandFlowSpinUp',samplingtype='uniform',
    #~ sample_param1=0,sample_param2=12,sample_N=4)
    
    for ii, irundir in enumerate(simunames):
        k = "Geom.H{0}.Perm.Value".format(ii + 1)
        v = (ii +1) * 1e-5
        dic = {k: v}
        print(dic)
        simus = simus.add_simu(irundir, **dic)
    
    ### simulations init & run:
    simus.create_rundirs()
    simus.create_new_tcls(tclfile)
    simus.cp_allinputfiles(tclfile)
    #~ simus.run_simulations()

    
    ### outputs:
    folder_pattern = os.path.join(testdir,'sample*')
    tcl_pattern = 'simu.tcl'
    simus_out = Simus()

    simus_out = simus_out.populate_simus_from_patterns(folder_pattern, tcl_ref = tclfile, tcl_pattern = tcl_pattern)
    folder_pattern = '/homelocal5/phyrev/_PARFLOW/PF_lectures/hillslope_course/hillslope2017/SAINTSULPICE/results*'
    tcl_ref = '/homelocal5/phyrev/_PARFLOW/PF_lectures/hillslope_course/hillslope2017/SAINTSULPICE/hillslope_ref.tcl'
    tcl_pattern = 'hillslope*.tcl'
    
    simus_out = simus_out.populate_simus_from_patterns(folder_pattern, tcl_ref = tclfile, tcl_pattern = tcl_pattern)
    

    
    # then remove all files to close the test:        
    #~ for key,val in sims.simudic.items():
        #~ shutil.rmtree(val.rundir)

