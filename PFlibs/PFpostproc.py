#-*- coding: utf-8 -*-

"""
    PFPYLIBS - PFLIBS

    Utility functions to postproc PF outputs

    @copyright: 2021 by PHyREV (Basile HECTOR) <basile.hector@ird.fr>
    @license: GNU GPL, see COPYING for details.
"""

__author__     = "PHyREV (Basile HECTOR)"
__copyright__  = "Copyright 2021"
__license__    = "GNU GPL"

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

"""classical imports:"""
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
import copy,os
import pyproj
from osgeo import gdal,ogr
from osgeo.gdalconst import *
import fiona
from shapely.geometry import Point, mapping,GeometryCollection
from shapely.geometry import shape as shplyshp
from pysheds.grid import Grid
import geopandas as gpd


"""local imports:"""

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##


def readRasterWithGdal(Rasterfile,nodata_value=-9999.0,datatype = np.int):
	"""
    Function to simplify classic Raster opening
    output an array, and x & y vectors (sorted in increasing order)
    masking is possible if nodata_value is provided

    Meshgrid can be used afterwards:
    [xx,yy]=np.meshgrid(x,y)

    Parameters:
        Rasterfile (str): filename of the raster to open
        nodata_value (float): value for the nodata field
        datatype (dtype): if you wish to specify data type to be read
        
    Returns: 
        data (numpy.array): matrix data from the raster
        lon (numpy.array): x coordinates
        lat (numpy.array): x coordinates
        
    .. note::
    gdal.Open don't work with "with X as x", because a gdal.Dataset has no .__enter__ method

    Some infos here:
    https://geoinformaticstutorial.blogspot.fr/2012/09/reading-raster-data-with-python-and-gdal.html

    Read data:
    data = band.ReadAsArray(0, 0, ds.RasterXSize, ds.RasterYSize)

    One has  to be careful not confusing column and rows! Matrix is 
    value = data[row, column], and it starts with '0', so the 
    value -8.30476 is located at y=row=3501 and x=column=4001. 

    that's the transformation to get from pixel units to real world units:
    transf = ds.GetGeoTransform()

    adfGeoTransform[0] /* top left x */
    adfGeoTransform[1] /* w-e pixel resolution */
    adfGeoTransform[2] /* 0 */
    adfGeoTransform[3] /* top left y */
    adfGeoTransform[4] /* 0 */
    adfGeoTransform[5] /* n-s pixel resolution (negative value) */

    T0 = Affine.from_gdal(*ds.GetGeoTransform())
    # If rather use center of cell instead of the pixel corner: translate by 50%
    T1 = T0 * Affine.translation(0.5, 0.5)
    #to transform from pixel coordinates to world coordinates, multiply the coordinates with the matrix,
    rc2xy = lambda r, c: (c, r) * T1
    print(rc2xy(0, 1))
    #if you need to get the pixel coordinate from a world coordinate, you can use an inverted affine transformation matrix, ~T0.

    If a raster needs be reprojected:
    gdal.Warp(output_raster,input_raster,dstSRS='EPSG:4326')

    transform points
    xx_a, yy_a = pyproj.transform(geo_system,proj,lonlon_tmp, latlat_tmp)
	"""
    
	ds = gdal.Open(Rasterfile, GA_ReadOnly)
	if ds is None:
		print('Failed open file')
		sys.exit(1)
	cols = ds.RasterXSize
	rows = ds.RasterYSize
	bands = ds.RasterCount #1
	band = ds.GetRasterBand(1)
	bandtype = gdal.GetDataTypeName(band.DataType) #Int16
	driver = ds.GetDriver().LongName #'GeoTIFF'
	# that's the transformation to get from pixel units to real world units
	transf = ds.GetGeoTransform()
	transfInv = gdal.InvGeoTransform(transf)
	#~ data = band.ReadAsArray(0, 0, cols, rows).astype(np.int)
	data = band.ReadAsArray(0, 0, cols, rows).astype(datatype)
	if nodata_value:
		masked_data = np.ma.masked_where(data == nodata_value, data)
	print("Origin = ({}, {})".format(transf[0], transf[3]))
	print("Pixel Size = ({}, {})".format(transf[1], transf[5]))

	########" prepare y x arrays:
	# lon0, lat0 = top left corner:
	reslon = transf[1]
	reslat = transf[5] #negative value if it's upper left corner
	lon0 = transf[0]+reslon/2.
	lat0 = transf[3]+reslat/2.
	lon = np.arange(lon0,lon0+reslon*cols,reslon) 
	lat = np.arange(lat0,lat0+reslat*rows,reslat)
	#make it in order:
	lat = lat[::-1]
	return data,lon,lat


def find_station_location_on_PF(D,PFgrid, streamfile=None,plot_streams=False,drainage_file = None, box_size=None,plot_drainage=False):
    """
    function to find station locations on a PF grid
    
    Parameters:
        D (pandas.DataFrame): a pd.DataFrame: must contain at least columns 'x'and 'y'
                if a drainage area map is specified, D must contain a column
                "catch_area" with the 'real' catchment area (in km2)
        PFgrid (dict): must contain X0;Y0;nx;ny;dx;dy as keys
        streamfile (str): filename for geotiff of the stream network matching the PF-grid 
        plot_streams (bool): plot a figure with the streamnetwork and stations
        drainage_file (str): filename for geotiff of draining area matching the PF-grid 
        box_size (int): 1/2 side of a rectangle around the station to look for similar
                draining area
        plot_drainage (bool): : plot a figure with the drainage area and stations
        
    Returns:
        Bool: The return value. True for success, False otherwise.
        
    .. note:: 
        To extract a point efficiently:
        in the example below all 3 stations should have the PF coordinate of the center 
        of cell so if a transformation of PF coordinate allow the llcorners 
        coordinates to increment to unity, the same transf can be applied to the 
        data points to get their cell values directly. 
        eg. data 1 = [18565,88552], data2 =[18120,89053],data4=[19100,89000]
        PF dx,dy = 1000
        center of cell is 18300,88700, so ll is [17800,88200]
        so we do x offset = +200 & y_offset = -200
        ll becomes [18000,88000]
        data1 becomes [18765,88352]
        data2 becomes [18320,88853]
        data4 becomes [19300,88800]
         ___________
        |         .3|
        |  .2       |  .4
        |    _|_    |
        |     |     |
        |         .1|
        |___________|
        ll corner

        cell coordinate (data1) = [floor(data[0]/1000)*1000-x_offset+dx/2, floor(data[1]/1000)*1000-y_offset+dy/2        
        
        To match station location and a stream network:
        (optional) step: load a stream network and find station location 
        on the streams. 
        stations already on streams: flag = 1
        stations that needs be moved to less < 1km to be on a strem cell: flag = 2
        
        To match station location and a similar drainage area (nearby):
        if a drainage area map is specified, D must contain a column
        "catch_area" with the 'real' catchment area (in km2)        
        stations that needs be moved within <box_size> to match a similar drainage area: flag = 3

    """
    """ step 3 convert data to PF projection & find PF coordinates"""
    nx = PFgrid['nx']; ny = PFgrid['ny']; dx = PFgrid['dx']; dy = PFgrid['dy']
    x0 = PFgrid['X0']; y0 = PFgrid['Y0']
    x = np.arange(nx)*dx+x0
    y = np.arange(ny)*dy+y0

    #option possible when steps increments unitarily in coordinates (or transformation is possible)
    xlowerleft = x-dx/2 #that's made to go from say x0 = -1425000.0 to x0 ll = -1424500.0
    ylowerleft = y-dy/2
    # make a vector which increments unitarily (this will depend on resolution)
    xoffset = - dx/2 #(could be + too: that's made to go from say x0ll = -1424500.0 to x0 ll = -1424000.0 to have it rounded)
    yoffset = - dy/2
    xlowerleftround = xlowerleft + xoffset 
    ylowerleftround = ylowerleft + yoffset
    #can we replace 1000 by dx here?
    D['xPF'] = np.floor((D['x'] + xoffset)/1000)*1000 - xoffset +dx/2
    D['yPF'] = np.floor((D['y'] + yoffset)/1000)*1000 - yoffset +dy/2
    D['iPF'] = ((D['xPF'] - x0)/dx).apply(lambda x:int(x))
    D['jPF'] = ((D['yPF'] - y0)/dy).apply(lambda x:int(x))
    # drop stations outside the domain:
    D = D.loc[(D.iPF>0) & (D.iPF<nx) & (D.jPF>0) & (D.jPF<ny)] 

    """ step 4 (optional) load a stream network and find station location 
    on the streams. Flag stations that needs be moved """

    """ step 4.a : load stream network"""
    if streamfile :
        stream,xstream,ystream = readRasterWithGdal(streamfile)
        [xx,yy]=np.meshgrid(xstream-dx/2,ystream+dy/2) # no idea why xstream[0] is -1424500., and ystream[0] -779500.,while x0 is -1425000.0 and y0 is -779000.0
        stream = stream.astype(float)
        stream[stream==0] = np.nan
        stream = stream[::-1,:]
        #tmp save for thierry
        #~ lonlon,latlat = pyproj.transform(projAEA,geo_system,xx,yy)
        #~ pd.DataFrame({'x':xx.flatten(),'y':yy.flatten(),'lon':np.round(lonlon.flatten()*1000)/1000,'lat':np.round(latlat.flatten()*1000)/1000}).to_csv('/home/hectorb/PARFLOW/PROJECTS/CONWA/active/CONWA_AD/PF_grid_albers_to_geo.csv')
        #stream is meshgrid-like, that is [ny,nx], ny being correctly in increasing order. So pF-compatible version is:
        streamPF = np.swapaxes(stream,0,1)


        """ step 4.b :  Flag points already on major streams: FLAG = 1 """
        D = D.assign(onStream= D.apply(lambda x: 1.0 if not np.isnan(stream[x.jPF,x.iPF]) else np.nan,axis=1).values)
        D = D.assign(xStream= D.apply(lambda x: x.xPF if x.onStream == 1.  else np.nan,axis=1).values)
        D = D.assign(yStream= D.apply(lambda x: x.yPF if x.onStream == 1.  else np.nan,axis=1).values)
        D = D.assign(iStream= D.apply(lambda x: x.iPF if x.onStream == 1.  else np.nan,axis=1).values)
        D = D.assign(jStream= D.apply(lambda x: x.jPF if x.onStream == 1.  else np.nan,axis=1).values)

        print('%d stations out of %d are already located on a stream'%(len(D.onStream.dropna()),len(D)))

        """ step 4.c :  identify points with rivers nearby (<1km) FLAG = 2 """
        ## move points that are nearby major streams: FLAG = 2
        for index, row in D.iterrows():
            if np.isnan(row.onStream):
                # crop a window 25km radius
                stream_tmp = stream[D.loc[index,'jPF'].item() - 25 : D.loc[index,'jPF'].item() + 25 , D.loc[index,'iPF'].item() - 25 : D.loc[index,'iPF'].item() + 25]
                xx_tmp = xx[D.loc[index,'jPF'].item() - 25 : D.loc[index,'jPF'].item() + 25 , D.loc[index,'iPF'].item() - 25 : D.loc[index,'iPF'].item() + 25]
                yy_tmp = yy[D.loc[index,'jPF'].item() - 25 : D.loc[index,'jPF'].item() + 25 , D.loc[index,'iPF'].item() - 25 : D.loc[index,'iPF'].item() + 25]

                # find the matching stream cell: 
                xx_tmp2 = copy.deepcopy(xx_tmp)
                yy_tmp2 = copy.deepcopy(yy_tmp)
                xx_tmp2[np.isnan(stream_tmp)]=np.nan       
                yy_tmp2[np.isnan(stream_tmp)]=np.nan       
                
                dist_to_river = np.sqrt((xx_tmp2 - D.loc[index,'x'].item())**2 + (yy_tmp2 - D.loc[index,'y'].item())**2)
                
                if dist_to_river.size:
                    if np.nanmin(dist_to_river) <= 2*np.sqrt(dx*dx + dy*dy):
                        ind_min = np.where(dist_to_river == np.nanmin(dist_to_river))
                        D.loc[index,'onStream'] = 2 #flag for modifying cell according to PF river nearby
                        D.loc[index,'xStream'] = xx_tmp2[ind_min][0]
                        D.loc[index,'yStream'] = yy_tmp2[ind_min][0]
                        D.loc[index,'iStream'] = (xx_tmp2[ind_min][0]- x0)/dx
                        D.loc[index,'jStream'] = (yy_tmp2[ind_min][0]- y0)/dy

        print('%d stations out of %d are less than 1 km of a stream cell'%(sum(D.onStream==2),len(D)))

        if plot_streams:
            """  plot the whole map: """
            fig = plt.figure(figsize=(10,8))
            ax = fig.add_subplot(111)
            ax.set_aspect(1)
            p0 = ax.pcolormesh(xx-dx/2, yy-dy/2, stream)
            l1, = ax.plot(D.xPF.to_numpy(),D.yPF.to_numpy(),'ro',markersize=1,markeredgewidth=1)
            l2, = ax.plot(D.x.to_numpy(),D.y.to_numpy(),'b+',markersize=1,markeredgewidth=1)
            l3, = ax.plot(D.loc[D.onStream==1,'xPF'].to_numpy(),D.loc[D.onStream==1,'yPF'].to_numpy(),'go',markersize=3,markeredgewidth=3)
            l4, = ax.plot(D.loc[D.onStream==2,'xStream'].to_numpy(),D.loc[D.onStream==2,'yStream'].to_numpy(),'bo',markersize=3,markeredgewidth=3)

            props = dict(boxstyle='round', facecolor='white', alpha=0.5)
            for index, row in D.iterrows():
                ax.text(row.x,row.y,row.Name.split(sep=' ')[0],fontsize = 6, bbox=props)
            ax.legend((l1,l2,l3,l4),('PF station','True station','station on stream','station moved to nearby stream'),fontsize=6,loc='upper left',ncol=2)

    if drainage_file:
        """ step 5 (optionnal) load a drainage area map and find catchment area
        in PF for each station """
        area,xarea,yarea = readRasterWithGdal(drainage_file)
        area = area.astype(float)
        area = area[::-1,:]

        D = D.assign(PFarea = D.apply(lambda x: area[x.jPF,x.iPF] if x.onStream == 1.  else np.nan,axis=1).values)
        D = D.assign(PFarea = D.apply(lambda x: area[int(x.jStream),int(x.iStream)] if x.onStream==2  else x.PFarea,axis=1).values)

        diff_rel_catch_area = abs(D.PFarea - D.loc[:,'catch_area'] )/D.loc[:,'catch_area']    
        # plot the relative difference in catchment area: the plot is bounded between 0 and 1 but there are some high values probably due to Niger & endoreism areas
        fig,ax = plt.subplots(1,1)
        diff_rel_catch_area.hist(bins = np.arange(0,1,0.02)) 
        ax.set_xlabel('$|\dfrac{PFarea - Realarea}{Realarea}|$')

        """ step 5.b : for remaining stations, find the nearby pixel (within say 10km where draining area closely match PF's)"""
        for index, row in D.iterrows():
            if np.isnan(row.onStream):
                # crop a window 
                area_tmp = area[D.loc[index,'jPF'].item() - box_size : D.loc[index,'jPF'].item() + box_size , D.loc[index,'iPF'].item() - box_size : D.loc[index,'iPF'].item() + box_size]
                xx_tmp = xx[D.loc[index,'jPF'].item() - box_size : D.loc[index,'jPF'].item() + box_size , D.loc[index,'iPF'].item() - box_size : D.loc[index,'iPF'].item() + box_size]
                yy_tmp = yy[D.loc[index,'jPF'].item() - box_size : D.loc[index,'jPF'].item() + box_size , D.loc[index,'iPF'].item() - box_size : D.loc[index,'iPF'].item() + box_size]

                # find the matching draining area cell: 
                closest_area = abs(area_tmp-D.loc[index,'catch_area'])
                closest_area_location = np.where(closest_area == closest_area.min())
               
                D.loc[index,'onStream'] = 3 #flag for modifying cell according to PF river nearby
                D.loc[index,'xStream'] = xx_tmp[closest_area_location][0]
                D.loc[index,'yStream'] = yy_tmp[closest_area_location][0]
                D.loc[index,'iStream'] = (xx_tmp[closest_area_location][0]- x0)/dx
                D.loc[index,'jStream'] = (yy_tmp[closest_area_location][0]- y0)/dy

        print('%d stations out of %d have been moved to a nearby cell with similar draining area'%(sum(D.onStream==3),len(D)))

        if plot_drainage:
            """  plot again the whole map: """
            fig = plt.figure(figsize=(15,12))
            ax = fig.add_subplot(111)
            ax.set_aspect(1)
            #~ p1 = ax.pcolormesh(xx-dx/2, yy-dy/2, area,vmin=100,vmax=5000)
            p0 = ax.pcolormesh(xx-dx/2, yy-dy/2, stream)
            l1, = ax.plot(D.xPF.to_numpy(),D.yPF.to_numpy(),'ro',markersize=1,markeredgewidth=1)
            l2, = ax.plot(D.x.to_numpy(),D.y.to_numpy(),'b+',markersize=1,markeredgewidth=1)
            l3, = ax.plot(D.loc[D.onStream==1,'xPF'].to_numpy(),D.loc[D.onStream==1,'yPF'].to_numpy(),'go',markersize=3,markeredgewidth=3)
            l4, = ax.plot(D.loc[D.onStream==2,'xStream'].to_numpy(),D.loc[D.onStream==2,'yStream'].to_numpy(),'bo',markersize=3,markeredgewidth=3)
            l5, = ax.plot(D.loc[D.onStream==3,'xStream'].to_numpy(),D.loc[D.onStream==3,'yStream'].to_numpy(),'co',markersize=3,markeredgewidth=3)

            props = dict(boxstyle='round', facecolor='white', alpha=0.5)
            for index, row in D.iterrows():
                ax.text(row.x,row.y,row.Name.split(sep=' ')[0],fontsize = 5, bbox=props)
            ax.legend((l1,l2,l3,l4,l5),('PF station','True station','station on stream','station moved to nearby stream','station moved to similar draining area'),fontsize=6,loc='upper left',ncol=2)


    """ step 6 (optionnal) flag stations with obvious mismatch of catchment size """

    """ step 7 (optionnal) inspect individual stations and correct location
    manually if needed"""

    """ step 8 save outputs """
    return D


def compute_catchments_from_PF_stations(D,direction_file,drainage_file,crs,dirmap=(3,6,4,7,1,8,2,9),outdir=None, stream_acc_threshold = 100):
    """ function to compute and save catchment shapefiles, outlet shapefiles and river networks
    from station coordinates in PF grid, a PFgrid, direction and accumulation files

    Parameters:
        D (pandas.DataFrame): a pd.DataFrame: must contain at least columns iStream and jStream
                which are indexes of outlets on the PF grid. also xStream and yStream which
                are directly outlet coordinates
        direction_file (str): filename for geotiff of the direction file
                matching the PF-grid and as created with priority flow. Otherwise 
                (d8 or other processing tool), provide the dirction vector
        drainage_file (str): filename for geotiff of draining area matching the PF-grid 
                crs (str): coordinate reference system to add to the geopandas (eg
                '+proj=aea +lat_1=16.12 +lat_2=6.58 +lat_0=11.35 +lon_0=-4.3 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs'
                for current CONWA AEA)
        dirmap (tuple): flow direction values: values indicating where the water goes from one cell to another. d8 reference
                the format is (N,NE,E,SE,S,SW,W,NW). For d4, add dumb values for NE, SE, NW, SW. ParFlow has usually flowdir
                maps being processed using priority flow (https://github.com/lecondon/PriorityFlow). In this case, the d4 
                goes like   (3,10,4,10,1,10,2,10)
        outdir (str): output directory: if river networks are to be saved as individual tif files
                from the drainage raster, provides the directory where to save tifs
        stream_acc_threshold (int): threshold to define the river network by masking the drainage raster
                in pixel counts units

    Returns:
        gDcatchment (geoDataFrame): geoDataFrame containing the original data + the shape of the catchment
        gDoutlet (geoDataFrame): geoDataFrame containing the original data + the shape of the outlet
    """
    
    grid = Grid()

    #define the grid: beware: grid.affine or grid.bbox or grid.extent are
    # pretty clear: the bounding box is given similar to values in the .asc header
    # so for CONWA: its offsets of 1000, not 500 (so when saved then as raster, 
    # and opened into qgis, center of cells will be in ...500.)
    grid.from_raster(direction_file,data_name='dir')
    #~ grid.from_ascii(direction_file,data_name='dir')

    grid.read_raster(direction_file, data_name='dir')
    #~ grid.read_ascii(direction_file, data_name='dir')
    grid.dir[grid.dir ==9999] = np.nan
    #~ plt.imshow(grid.dir)  

    #optionnal:
    grid.read_raster(drainage_file, data_name='acc')
    
    gDcatchment = copy.deepcopy(gpd.GeoDataFrame(D))
    gDoutlet = copy.deepcopy(gpd.GeoDataFrame(D))
    #initialise the geometry column of the geodataframe:
    gDcatchment['geometry'] = [GeometryCollection() for a in range(len(D))] 
    gDoutlet['geometry'] = [GeometryCollection() for a in range(len(D))] 
    for index, row in D.iterrows():
        #clip back again to the whole domain:
        grid.clip_to('dir')  
        x = int(row.iStream)
        y = int(grid.dir.shape[0] - row.jStream)-1
        
        # compute catchment:
        grid.catchment(data='dir', x=x, y=y, dirmap=dirmap, out_name=str(index),
                       recursionlimit=15000, xytype='index', nodata_out=0)
        grid.clip_to(str(index))
        
        # get catchment polygons:
        shapes = grid.polygonize()
        geometry = []
        for sh, value in shapes:
            geometry.append(shplyshp(sh))
            gDcatchment.loc[index,'geometry'] = geometry

        # get outlet points:
        point = Point(float(row['xStream']), float(row['yStream']))            
        gDoutlet.loc[index,'geometry'] = point

    gDcatchment.crs = crs
    gDoutlet.crs = crs
    
    if outdir: 
        # save rasters of river network
        for index, row in gDcatchment.iterrows():
            grid.clip_to(str(index))
            grid.acc2 = grid.view('acc')
            grid.acc2[grid.acc2<stream_acc_threshold] = np.nan
            grid.acc2[grid.acc2>=stream_acc_threshold] = 1
            grid.to_raster('acc2', outdir+'rivers_%s.tif'%index, blockxsize=16, blockysize=16)

    return gDcatchment,gDoutlet

