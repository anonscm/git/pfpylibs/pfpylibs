https://rogerdudler.github.io/git-guide/index.fr.html

# https://services.renater.fr/sourcesup/git/comment_travailler_avec_git_en_local_sur_son_poste_de_travail
# https://services.renater.fr/sourcesup/git/comment_travailler_avec_git_et_la_plate-forme_sourcesup_partage_de_votre_projet
# https://services.renater.fr/sourcesup/sshkey/index
# Git tuto: https://try.github.io/levels/1/challenges/1
#(delay time < 1hr after uploading public keys)
#current key is similar to ciment

#When PFpylibs is on Git (could take some time after the creation on sourcesup):
git clone git+ssh://git@git.renater.fr:2222/pfpylibs.git
#changed to :
git clone ssh://hectorb@git.renater.fr:2222/scmrepos/git/pfpylibs/pfpylibs.git

# or from public access
git clone git://git.renater.fr/pfpylibs.git 
git clone https://git.renater.fr/anonscm/git/pfpylibs/pfpylibs.git


#(don't forget to upload public keys on sourcesup when you change computer (see links above)
#local pphrse = prvt#1 . srv12pphrse =prvt#2noacc1stwrd

cp pfpylibs
#(cp files in it)
# could edit .gitignore file to list files which you don't want to commit
git add .
# First commit (add name and email (maybe?)):
git commit -m "first commit" 
# -u option tells to remember parameters, so then we can do git push only
git push ssh://git@git.renater.fr:2222/pfpylibs.git
# git pull is to checkout other changes that have been made on the repo on the server
# IF there are changes, when a new pull is done, to former local files:
# In this case we want the diff of our most recent commit, which we can refer to using the HEAD pointer.
git diff HEAD
# create new branch:
git branch new_branch
# make it active:
git checkout new_branch
# if you want to merge it back again to master, you should move to master (git checkout master) and then
git merge new_branch
# To delete a file from a repo (without completely remove it from the history:
git rm file.txt
# then should commit


# to push : just use 
git push
# passphrase is usually the prevert for Renater



####### how to use pfpylibs in developer mode on different machines ?
https://softwareengineering.stackexchange.com/questions/156309/working-with-git-on-multiple-machines

http://www.deanbodenham.com/learn/using-git-to-sync-different-computers.html

pour mettre à jour
git pull origin master
ou 
git fetch origin master
et 
	git merge origin/master

to identify unmerged files:
git status

One thing I can do is see specifically which commits are conflicting:
git log --merge

you can edit the conflicting files:
https://stackoverflow.com/questions/161813/how-to-resolve-merge-conflicts-in-git
https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/resolving-a-merge-conflict-using-the-command-line
HEAD is git slang for “the branch you’re currently working on”
or (meld as mergetool is pretty cool, you can choose with config)
 READ THIS: when using meld, left is your file, right is the one you're merging with, MIDDLE IS THE MERGED ONE !
git mergetool 


on faro there's only vimdiff as mergetool, so checkout

https://www.rosipov.com/blog/use-vimdiff-as-git-mergetool/
https://gist.github.com/mattratleph/4026987
https://gist.github.com/karenyyng/f19ff75c60f18b4b8149

##### do a new branch
git branch syncbranch
git push --set-upstream origin syncbranch

sur l'autre ordi
git pull origin syncbranch
git checkout syncbranch (cree une nouvelle branche)




